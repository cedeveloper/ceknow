<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$document = JFactory::getDocument();
$document->addStyleSheet('modules/mod_teaser/assets/css/teaser.css');
$document->addStyleSheet('https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css');
$showIcon = $params->get('showIcon');
$pTop 		= $params->get('positiontop');
$width 		= $params->get('width');
$height 	= $params->get('height');
$animate 	= $params->get('animate');
if($animate):
	$animate = 'animate__animated '.$animate; 
endif;
$delay 	= $params->get('delay');
if($delay):
	$delay = $delay; 
endif;
$duration 	= $params->get('duration');
if($duration):
	$duration = $duration; 
endif;
$border 	= $params->get('border');
if($border):
	$borderColor = $params->get('borderColor');
	$borderWidth = $params->get('borderWidth');
	$border = 'border: '.$borderWidth.'px solid '.$borderColor;
endif;
$radius 	= $params->get('borderRadius');
if($radius):
	$borderRadius = $params->get('borderRadius');
	$radius = 'border-radius: '.$borderRadius.'px';
endif;
$shadow 	= $params->get('borderShadow');
if($shadow):
	$borderShadow = $params->get('borderShadow');
	$shadow = 'box-shadow: 6px 6px 4px #000000';
endif;
$bg 		= $params->get('background');
if($bg):
	$bg = $params->get('background');
	$bg = 'background-color: '.$bg.'!important';
endif;
$bgI 		= $params->get('backgroundImage');
if($bgI):
	$bgI = $params->get('backgroundImage');
	$bgI = 'background-image: url('.$bgI.');background-size: cover';
endif;
$class = '';
if($params->get('template') != '' && $params->get('template') != 0):
	$class = $params->get('template');
	$border = '';
	$radius = '';
	$shadow = '';
	$bg = '';
endif;
$text 		= $params->get('text');
$link = $params->get('menulink');
 ?>
<div id="teaser" class="<?php echo $class;?> <?php echo $animate;?> <?php echo $delay;?> <?php echo $duration;?>" 
	style="
	top: <?php echo $pTop.'px!important'?>;
	width: <?php echo $width.'px!important'?>;
	height: <?php echo $height.'px!important'?>;
	<?php echo $border;?>;
	<?php echo $radius;?>;
	<?php echo $shadow;?>;
	<?php echo $bgI;?>;
	<?php echo $bg;?>;">
	<?php echo $text;?>
</div>
<script>
	$(document).ready(function() {
		var w1 = $('.container').width();
		var w2 = $('#teaser').width();
		var w3 = ((w1 - w2) / 2);
		console.log(w1 + ' ' + w2 + ' ' + w3);
		$('#teaser').css('left', w3 + 'px');
	});
</script>
