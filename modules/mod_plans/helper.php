<?php

class modplansHelper
{
	public static function getPlans() {

		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_plans','a'));
        $query->where('a.price <> 0');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}

}

?>

