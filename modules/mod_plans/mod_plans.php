<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_banners
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the banners functions only once
JLoader::register('ModPlansHelper', __DIR__ . '/helper.php');

$input 	= JFactory::getApplication()->input;
$deno 	= $input->get('deno', '', 'INT');
$planID = $input->get('pid', '', 'INT');
$plans 	= &ModPlansHelper::getPlans();

require JModuleHelper::getLayoutPath('mod_plans', $params->get('layout', 'default'));
