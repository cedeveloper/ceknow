<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$document = JFactory::getDocument();
$document->addStyleSheet('modules/mod_plans/assets/css/plans.css');
$document->addStyleSheet('https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css');
$user 	= JFactory::getUser();
$input 	= JFactory::getApplication()->input;
$deno 	= $input->get('deno','','INT');
 ?>
<div id="plans" class="row">
	<div id="biggestSize" class="col-12" style="display: none;">
		<div class="alert alert-danger">
			<a class="btn btn-primary mr-5" href="my-account"><i class="fa fa-chevron-left"></i> Zurück zu <b>Mein Konto</b></a> 
			Sie haben bereits den höchsten Plan gebucht.
		</div>
	</div>
	<div class="col-12 mb-3">
		<div class="custom-control custom-switch">
		  <input type="checkbox" class="custom-control-input" id="switchAbo">
		  <label class="custom-control-label" for="switchAbo">Zahlungsweise ändern</label>
		</div>
		<div id="abo" class="badge badge-primary text-white mt-2">Monatliche Abrechnung</div>
	</div>
	<?php
	if($plans):
		foreach($plans as $plan):
			if($planID):
				$link = 'newplan?n='.$plan->id.'&r=3&uid='.$user->id.'&deno='.$deno;
			else:
				$link = 'newplan?n='.$plan->id;
			endif;
			echo '<div class="col-12 col-sm-4 mb-2 sm-mb-0">';
			echo '<div id="plan'.$plan->id.'" class="card">';
			echo '<div class="card-body">';
		    echo '<h4 class="card-title">'.$plan->title.'</h4>';
		    echo '<h2 class="card-title mb-0"><span id="price_'.$plan->id.'">'.number_format($plan->price,2,',','.').'</span>€*</h2>';
		    echo '<span class="teaser" style="display: none;">Sie sparen 25%</span>';
		    echo '<p class="price_description"></p>';
		    echo '<p class="details mb-2 text-muted">'.$plan->description_f.'</p>';
		    echo '<a href="'.$link.'" class="btn btn-primary bt-lg text-white">Auswählen</a>';
		  	echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '<input type="hidden" id="price" data-id="'.$plan->id.'" class="price" value="'.$plan->price.'">';
		endforeach;
	endif;
	?>
	<div class="col-12 mb-3">
		<small>* Preise verstehen sich zzgl. gesetzl. MwSt.</small><br>
		<small>** max. 40 MB pro Learningroom</small>
	</div>
</div>
<input type="hidden" id="deno" value="<?php echo $deno;?>" />
<input type="hidden" id="planID" value="<?php echo $planID;?>" />
<script>
	$(document).ready(function() {
		$('#switchAbo').prop('checked', false);
		if($('#deno').val() == 1) {
			$('.demoaccount_end').css('display', 'none');
		}
		if($('#planID').val() == 1) {
			$('#plan1').addClass('disabled');
		}
		if($('#planID').val() == 2) {
			$('#plan1').addClass('disabled');
			$('#plan2').addClass('disabled');
		}
		if($('#planID').val() == 3) {
			$('#plan1').addClass('disabled');
			$('#plan2').addClass('disabled');
			$('#plan3').addClass('disabled');
			$('#biggestSize').css('display','block');
		}
	});
	$('#switchAbo').on('change', function() {
		if($(this).is(':checked')) {
			$('#abo').html('Jährliche Zahlungsweise');
			$('.price').each(function() {
				var id 		= $(this).attr('data-id');
				var price 	= $(this).val();
				var nPrice 	= (price * 75 / 100)
				nPrice = nPrice.toFixed(2);
				nPrice = nPrice.replace('.',',');
				$('#price_' + id).html(nPrice);
				$('.teaser').slideDown(200);
				$('.price_description').html('netto pro Monat bei einem Jahresabo und jährlicher Abrechnung');
			});
		} else {
			$('#abo').html('Monatliche Zahlungsweise');
			$('.price').each(function() {
				var id 		= $(this).attr('data-id');
				var price 	= $(this).val();
				price = price.replace('.',',');
				$('#price_' + id).html(price);
				$('.teaser').fadeOut(200);
				$('.price_description').html('netto pro Monat bei einem Jahresabo und monatlicher Abrechnung');
			});
		}
	});
</script>

