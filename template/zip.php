<?php
define( '_JEXEC',1);
define( 'DS', DIRECTORY_SEPARATOR );
define( 'JPATH_BASE',dirname(__FILE__));
ini_set('default_socket_timeout', 600); // 900 Seconds = 15 Minutes Added by CS

require_once( JPATH_BASE. DS .'includes' . DS . 'defines.php' );
require_once( JPATH_BASE. DS .'includes' . DS . 'framework.php' );
require_once( JPATH_BASE. DS .'libraries' . DS . 'src' . DS . 'Factory.php' );
$mainframe = JFactory::getApplication('site');
$mainframe->initialise();
doSomething();

function doSomething() {
	$session = JFactory::getSession();
	$id 	= $session->get('tmp_lrid');
	$xml 	= $session->get('tmpxml');
	$zip = new ZipArchive;
	if ($zip->open('tmp/saveLR'.$id.'.zip', ZipArchive::CREATE) === TRUE)
	{
		$zip->addEmptyDir('LR'.$id);
		$zip->addFromString('LR'.$id.'/save.json', file_get_contents($xml));
		$path 	= JPATH_BASE.'/images/learningrooms/LR'.$id.'/material';
		$files 	= array_diff(scandir($path), array('.', '..'));
		if($files):
			foreach($files as $file):
				$zip->addFromString('LR'.$id.'/material/'.$file, file_get_contents($path.'/'.$file));
			endforeach;
		endif;
		$zip->addEmptyDir('LR'.$id.'/material_students');
		$path 	= JPATH_BASE.'/images/learningrooms/LR'.$id.'/material_students';
		$files 	= array_diff(scandir($path), array('.', '..'));
		if($files):
			foreach($files as $file):
				$zip->addFromString('LR'.$id.'/material_students/'.$file, file_get_contents($path.'/'.$file));
			endforeach;
		endif;
		$zip->addEmptyDir('LR'.$id.'/reserve');
		$path 	= JPATH_BASE.'/images/learningrooms/LR'.$id.'/reserve';
		$files 	= array_diff(scandir($path), array('.', '..'));
		if($files):
			foreach($files as $file):
				$zip->addFromString('LR'.$id.'/reserve/'.$file, file_get_contents($path.'/'.$file));
			endforeach;
		endif;
		$zip->addEmptyDir('LR'.$id.'/units');
		$path 	= JPATH_BASE.'/images/learningrooms/LR'.$id.'/units';
		$files 	= array_diff(scandir($path), array('.', '..'));
		if($files):
			foreach($files as $file):
				$zip->addFromString('LR'.$id.'/units/'.$file, file_get_contents($path.'/'.$file));
			endforeach;
		endif;
	    $zip->close();
	    $session->set('tmpzip', 'tmp/saveLR'.$id.'.zip');
	}
}