
jQuery(function($) {

	var h1 = $(document).height();
    $('#sp-main-body').css('min-height',h1 - 186);
	$('select.select2-max').select2({
		width: '100%'
	});
	$('select.select2-600').select2({
		width: '600px'
	});
	$('select.select2').select2({
		width: '300px'
	});
	$('select.select2-sm').select2({
		width: '80px'
	});
	$( ".datepicker" ).datepicker({
	    firstDay: 1,
	    prevText: '&#x3c;zurück', prevStatus: '',
	    prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
	    nextText: 'Vor&#x3e;', nextStatus: '',
	    nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
	    currentText: 'heute', currentStatus: '',
	    todayText: 'heute', todayStatus: '',
	    clearText: '-', clearStatus: '',
	    closeText: 'schließen', closeStatus: '',
	    monthNames: ['Januar','Februar','März','April','Mai','Juni',
	    'Juli','August','September','Oktober','November','Dezember'],
	    monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun',
	    'Jul','Aug','Sep','Okt','Nov','Dez'],
	    dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
	    dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
	    dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
	    dateFormat: 'dd.mm.yy'
	});
});