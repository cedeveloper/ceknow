<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Kunde table class.
 *
 * @package     Auditum
 * @subpackage  Tables
 */
class JclassroomTableLogs extends JTable {
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__jclassroom_logfiles', 'id', $db);
	}
	public function store($updateNulls = false)
	{


		return parent::store($updateNulls);
	}
	/**
     * Overloaded check function
     */
    public function check()
	{
		
		return parent::store($updateNulls);
	}
	
}
?>