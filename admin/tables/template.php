<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Kunde table class.
 *
 * @package     Auditum
 * @subpackage  Tables
 */
class JclassroomTableTemplate extends JTable {
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__jclassroom_templates', 'id', $db);
	}
	public function store($updateNulls = false)
	{

		$date   = JFactory::getDate()->toSql();
		$userId = JFactory::getUser()->id;

		$this->scripts = $this->id;

		return parent::store($updateNulls);
	}
	/**
     * Overloaded check function
     */
    public function check()
	{
		$user = JFactory::getUser();
		$session 			= JFactory::getSession();
		$this->customerID 	= $session->get('customerID');
		if($this->id):
			$this->modified 	= date('Y-m-d H:i:s');
			$this->modified_by 	= $user->id;
		else:
			if (!(int) $this->created)
			{
				$this->created = date('Y-m-d H:i:s');
			}

			if (empty($this->created_by))
			{
				$this->created_by = $user->id;
			}
		endif;
		return parent::store($updateNulls);
	}
	
}
?>