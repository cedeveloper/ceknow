<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Kunde table class.
 *
 * @package     Auditum
 * @subpackage  Tables
 */
class JclassroomTableConfiguration extends JTable {
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__jclassroom_configuration', 'id', $db);
	}
	public function store($updateNulls = false)
	{

		$date   = JFactory::getDate()->toSql();
		$userId = JFactory::getUser()->id;

		return parent::store($updateNulls);
	}
	/**
     * Overloaded check function
     */
    public function check()
	{
		
		return parent::store($updateNulls);
	}
	
}
?>