<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Kunde table class.
 *
 * @package     Auditum
 * @subpackage  Tables
 */
class JclassroomTableClassroom_student_quizz extends JTable {
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__jclassroom_classroom_student_quizz', 'id', $db);
	}
	public function store($updateNulls = false)
	{

		$date   = JFactory::getDate()->toSql();
		$userId = JFactory::getUser()->id;

		return parent::store($updateNulls);
	}
	/**
     * Overloaded check function
     */
    public function check()
	{
		if($this->id):
			$this->modified = date('Y-m-d H:i:s');
			$this->modified_by = JFactory::getUser()->id;
		else:
			$this->created = date('Y-m-d H:i:s');
			$session = JFactory::getSession();
        	if($session->get('group') == 'superuser'):
				$this->created_by = 0;
			else:
				$this->created_by = JFactory::getUser()->id;
			endif;
		endif;
		return parent::store($updateNulls);
	}
	
}
?>