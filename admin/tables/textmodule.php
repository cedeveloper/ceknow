<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Kunde table class.
 *
 * @package     Auditum
 * @subpackage  Tables
 */
class JclassroomTableTextmodule extends JTable {
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__jclassroom_textmodules', 'id', $db);
	}
	public function store($updateNulls = false)
	{

		$date   = JFactory::getDate()->toSql();
		$userId = JFactory::getUser()->id;

		return parent::store($updateNulls);
	}
	/**
     * Overloaded check function
     */
    public function check()
	{
		if($this->id):
			$this->modified = date('Y-m-d H:i:s');
			$this->modified_by = JFactory::getUser()->id;
		else:
			$this->created = date('Y-m-d H:i:s');
			$this->created_by = JFactory::getUser()->id;
		endif;
		return parent::store($updateNulls);
	}
	
}
?>