<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerDashboard_students extends JControllerForm
{
	function verification() {
		$input 	= JFactory::getApplication()->input;
		$user 	= JFactory::getUser();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$clr 	= $input->get('clr',0,'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom_verification','JclassroomTable',array());
		$load 	= array('studentID' => $user->id);
		$check 	= $table->load($load);
		if($check):
			$table->datenschutz 	= $input->get('cD',0,'INT');
			$table->agb 			= $input->get('cA',0,'INT');
			$table->aufzeichnung 	= $input->get('cF',0,'INT');
			$table->modified 		= date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data 					= array();
			$data['classroomID'] 	= $clr;
			$data['studentID'] 		= $user->id;
			$data['datenschutz'] 	= $input->get('cD',0,'INT');
			$data['agb'] 			= $input->get('cA',0,'INT');
			$data['aufzeichnung'] 	= $input->get('cF',0,'INT');
			$data['created'] 		= strval($datum);
			$data['created_by'] 	= $user->id;
			$table->bind($data);
			$table->store();
		endif;
        echo 'OK';
        exit();
	}
	function loadOrderpositions() {
		$input 	= JFactory::getApplication()->input;
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id, a.amount, b.title'));
        $query->from($db->quoteName('#__jclassroom_order_positions','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_hardware', 'b') . ' ON (' . $db->quoteName('a.hardwareID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where('a.orderID = '.$input->get('id', 0, 'INT'));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$html = '';
		if($result):
			foreach($result as $item):
				$html .= '<tr id="position_'.$item->id.'" class="positions">';
				$html .= '<td>'.$item->title.'</td>';
				$html .= '<td class="text-right">
				<input type="number" min="0" onchange="checkPositionValue('.$item->id.');" id="position'.$item->id.'" class="text-right float-right" style="width: 80px;height: 25px;" value="'.$item->amount.'" />
				</td>';
				$html .= '<td class="text-center">
					<span class="badge badge-danger text-white"><i onclick="deletePosition('.$item->id.');" class="fa fa-trash-o"></i></span>
				</td>';
				$html .= '</tr>';
			endforeach;
		endif;
        echo $html;
        exit();
	}
	function savePositions() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Orderposition','JclassroomTable',array());
		$table->load($input->get('positionID',0,'INT'));
		$table->amount 		= $input->get('value', 0, 'INT');
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by = $user->id;
		$table->store();
        echo 'OK';
        exit();
	}
}
?>