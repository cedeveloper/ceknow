<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerCustomer extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'manager-administrator/customers';
				break;
			case 'trainer':
				$retour = JURI::Root().'celearning/students';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'customer-edit?&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = 'index.php?option=com_plans&view=customer&layout=edit&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$post 	= $input->get('jform', array(), 'array');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer','JclassroomTable',array());
		$user 	= JFactory::getUser();
		$data = array();
		$data['name'] 				= $post['name'];
		$data['company_name'] 		= $post['company_name'];
		$data['customer_number'] 	= $post['customer_number'];
		$data['address'] 			= $post['address'];
		$data['postcode'] 			= $post['postcode'];
		$data['city'] 				= $post['city'];
		$data['email'] 				= $post['email'];
		$data['web'] 				= $post['web'];
		$data['planID'] 			= $post['plan'];
		$data['plan_from'] 			= date('Y-m-d', strtotime($post['plan_from']));
		$data['plan_to'] 			= date('Y-m-d', strtotime($post['plan_to']));
		$data['company_size'] 		= $post['company_size'];
		$data['ustid'] 				= $post['ustid'];
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= $post['published'];
		$table->bind($data);
		$table->store();
		$tblCustomerID 		= $table->id;
		// Save the Plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_plan','JclassroomTable',array());
		$load 	= array('customerID' => $input->get('id', 0, 'INT'));
		$check 	= $table->load($load);
		if(!$check):
			$data = array();
			$data['customerID'] 	= $tblCustomerID;
			$data['planID'] 		= $post['plan'];
			$data['valid_from'] 	= '2020-01-01';
			$data['valid_to'] 		= '2020-12-31';
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= $post['published'];
			$table->bind($data);
			$table->store();
		endif;
		// Save the Payments
		/*if($formData->getInt('paymentID')):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
			$load 	= array('customerID' => $input->get('id', 0, 'INT'));
			$check 	= $table->load($load);
			if(!$check):
				$data = array();
				$data['customerID'] 	= $tblCustomerID;
				$data['paymentID'] 		= $formData->getInt('paymentID');
				$data['account_owner'] 	= $formData->getStr('account_owner');
				$data['account_number'] = $formData->getStr('account_number');
				$data['iban'] 		= $formData->getStr('iban');
				$data['bic'] 		= $formData->getStr('bic');
				$data['bank_code'] 	= $formData->getStr('bank_code');
				$data['valid_from'] 		= date('Y-m-d',strtotime($formData->getStr('valid_from')));
				$data['valid_to'] 			= date('Y-m-d',strtotime($formData->getStr('valid_to')));
				$data['creditcard_bank'] 	= $formData->getStr('creditcard_bank');
				$data['creditcard_number'] 	= $formData->getInt('creditcard_number');
				$data['creditcard_check_number']= $formData->getInt('creditcard_check_number');
				$data['creditcard_valid_to'] 	= date('Y-m-d',strtotime($formData->getStr('creditcard_valid_to')));
				$data['invoice_title'] 		= $formData->getStr('invoice_title');
				$data['invoice_adress'] 	= $formData->getStr('invoice_adress');
				$data['invoice_postcode'] 	= $formData->getInt('invoice_postcode');
				$data['invoice_city'] 		= $formData->getStr('invoice_city');
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by']	= $user->id;
				$data['published'] 	= $formData->getInt('published');
				$table->bind($data);
				$table->store();
			endif;
		endif;*/
		return $tblCustomerID;
	}
	public function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($input->get('id', 0, 'INT'));
		$table->name 		= $formData->getStr('name');
		$table->company_name = $formData->getStr('company_name');
		$table->customer_number 	= $formData->getStr('customer_number');
		$table->address 	= $formData->getStr('address');
		$table->postcode 	= $formData->getStr('postcode');
		$table->city		= $formData->getStr('city');
		$table->email		= $formData->getStr('email');
		$table->web			= $formData->getStr('web');
		$table->planID		= $formData->getStr('plan');
		$table->plan_from	= date('Y-m-d', strtotime($formData->getStr('plan_from')));
		$table->plan_to		= date('Y-m-d', strtotime($formData->getStr('plan_to')));
		$table->ustid 		= $formData->getStr('ustid');
		$table->company_size= $formData->getStr('company_size');
		$table->published 	= $formData->getInt('published');
		$table->modified	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		// Save the Plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_plan','JclassroomTable',array());
		$load 	= array('customerID' => $input->get('id', 0, 'INT'));
		$check 	= $table->load($load);
		if(!$check):
			$data = array();
			$data['customerID'] 	= $input->get('id', 0, 'INT');
			$data['planID'] 		= $formData->getInt('plan');
			$data['valid_from'] 	= '2020-01-01';
			$data['valid_to'] 		= '2020-12-31';
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= $formData->getInt('published');
			$table->bind($data);
			$table->store();
		else:
			$table->planID 		= $formData->getInt('plan');
			$table->modified	= date('Y-m-d H:i:s');
			$table->modified_by	= $user->id;
			$table->store();
		endif;
		// Save the Payments
		if($formData->getInt('paymentID')):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
			$load 	= array('customerID' => $input->get('id', 0, 'INT'));
			$check 	= $table->load($load);
			if(!$check):
				$data = array();
				$data['customerID'] 	= $input->get('id', 0, 'INT');
				$data['paymentID'] 		= $formData->getInt('paymentID');
				$data['account_owner'] 	= $formData->getStr('account_owner');
				$data['account_number'] = $formData->getStr('account_number');
				$data['iban'] 		= $formData->getStr('iban');
				$data['bic'] 		= $formData->getStr('bic');
				$data['bank_code'] 	= $formData->getStr('bank_code');
				$data['valid_from'] 		= date('Y-m-d',strtotime($formData->getStr('valid_from')));
				$data['valid_to'] 			= date('Y-m-d',strtotime($formData->getStr('valid_to')));
				$data['creditcard_bank'] 	= $formData->getStr('creditcard_bank');
				$data['creditcard_number'] 	= $formData->getInt('creditcard_number');
				$data['creditcard_check_number']= $formData->getInt('creditcard_check_number');
				$data['creditcard_valid_to'] 	= date('Y-m-d',strtotime($formData->getStr('creditcard_valid_to')));
				$data['invoice_title'] 		= $formData->getStr('invoice_title');
				$data['invoice_adress'] 	= $formData->getStr('invoice_adress');
				$data['invoice_postcode'] 	= $formData->getInt('invoice_postcode');
				$data['invoice_city'] 		= $formData->getStr('invoice_city');
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by']	= $user->id;
				$data['published'] 	= $formData->getInt('published');
				$table->bind($data);
				$table->store();
			else:
				$table->paymentID 			= $formData->getInt('paymentID');
				$table->account_owner 		= $formData->getStr('account_owner');
				$table->account_number 		= $formData->getStr('account_number');
				$table->iban 				= $formData->getStr('iban');
				$table->bic 				= $formData->getStr('bic');
				$table->bank_code 			= $formData->getStr('bank_code');
				$table->valid_from 		= date('Y-m-d',strtotime($formData->getStr('valid_from')));
				$table->valid_to 		= date('Y-m-d',strtotime($formData->getStr('valid_to')));
				$table->creditcard_bank 		= $formData->getStr('creditcard_bank');
				$table->creditcard_number 		= $formData->getInt('creditcard_number');
				$table->creditcard_check_number = $formData->getInt('creditcard_check_number');
				$table->creditcard_valid_to 	= date('Y-m-d',strtotime($formData->getStr('creditcard_valid_to')));
				$table->invoice_title 			= $formData->getStr('invoice_title');
				$table->invoice_adress 			= $formData->getStr('invoice_adress');
				$table->invoice_postcode 		= $formData->getInt('invoice_postcode');
				$table->invoice_city 			= $formData->getStr('invoice_city');
				$table->modified	= date('Y-m-d H:i:s');
				$table->modified_by	= $user->id;
				$table->store();
			endif;
		endif;
		return $tblResellerID;
	}
	function checkEmail() {
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('User','JclassroomTable',array());
		$load 	= array('email' => $input->get('email','','STR'));
		$check 	= $table->load($load);
		if($check):
			echo 'NOK_Die E-Mail-Adresse '.$email.' ist bereits registriert.';
		else:
			echo 'OK_';
		endif;
		exit();
	}
	function createAccount() {
		$session 	= JFactory::getSession();
		$input 		= JFactory::getApplication()->input;
		$email 		= $input->get('email','','STR');
		$functionA 	= $input->get('functionA','','STR');
		$p 			= $input->get('p', '', 'STR');
		$first_name = $input->get('first_name', '', 'STR');
		$last_name 	= $input->get('last_name', '', 'STR');
		$planID 	= $input->get('planID', 0, 'INT');
		$plan_from 	= date('Y-m-d');
		$plan_to 	= new Datetime($plan_from);
		$plan_to->add(new Dateinterval('P30D'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer','JclassroomTable',array());
		$data = array();
		$data['name'] 			= $first_name.' '.$last_name;
		$data['company_name'] 	= '';
		$data['customer_number'] = 0;
		$data['email'] 			= $email;
		$data['planID'] 		= $planID;
		$data['plan_from'] 		= $plan_from;
		$data['plan_to'] 		= $plan_to->format('Y-m-d');
		$data['company_size'] 	= 0;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= 0;
		$data['published'] 	= 0;
		$table->bind($data);
		$table->store();
		$newCustomerID = $table->id;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$data = array();
		$data['salutation'] 	= '';
		$data['first_name'] 	= $first_name;
		$data['last_name'] 		= $last_name;
		$data['phone'] 			= '';
		$data['email'] 			= $email;
		$data['function'] 		= $functionA;
		$data['customerID'] 	= $newCustomerID;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= 0;
		$data['published'] 	= 0;
		$table->bind($data);
		$table->store();
		$customerID = $table->id;
		// Create the Plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_plan','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $newCustomerID;
		$data['planID'] 		= $planID;
		$data['valid_from'] 	= $plan_from;
		$data['valid_to'] 		= $plan_to->format('Y-m-d');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= 0;
		$data['published'] 	= 0;
		$table->bind($data);
		$table->store();
		// Create the User
		$password = JUserHelper::hashPassword($p);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$data = array();
		$data['name'] 		= $first_name.' '.$last_name;
		$data['username'] 	= $email;
		$data['email'] 		= $email;
		$data['password'] 	= $password;
		$data['block'] 		= 1;
		$data['sendEmail'] 	= 0;
		$data['registerDate'] 	= date('Y-m-d H:i:s');
		$data['lastvisitDate'] 	= date('Y-m-d H:i:s');
		$data['activation'] 	= 0;
		$table->bind($data);
		$table->store();
		$tblUserID = $table->id;
		//Tabelle Usergroup-Map
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Usergroup','JclassroomTable',array());
		$data = array();
		$data['user_id'] 	= $tblUserID;
		$data['group_id'] 	= 13;
		$table->bind($data);
		$table->store();
		// SAVE the user ID
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$table->load($customerID);
		$table->userID = $tblUserID;
		$table->store();
		// SAVE the customer_number
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($newCustomerID);
		$table->customer_number = 'CU'.str_pad($newCustomerID,8,'0', STR_PAD_LEFT);
		$table->created_by 		= $tblUserID;
		$table->store();
		$customer_number = 'CU'.str_pad($newCustomerID,8,'0', STR_PAD_LEFT);
		//Send E-Mail for Double-Opt-In
		//Write to Emails
		//Email to customer
		/*JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Email','JclassroomTable',array());
		$data 		= array();
		$data['typ'] = 5;
		$data['send'] 	= date('Y-m-d H:i:s');
		$data['send_by'] 	= $user->id;
		$data['send_to'] 	= $tblUserID;
		$data['classroomID'] = 0;
		$table->bind($data);
		$table->store();*/
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemname');
		$table->load($load);
		$systemName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'subject_confirm');
		$table->load($load);
		$subject = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode');
		$table->load($load);
		$testmode = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode_email');
		$table->load($load);
		$testmode_email = $table->wert;
		
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Textmodule','JclassroomTable',array());
		$load 		= array('section' => 1, 'title' => 'Bestätigungsmail Planbestellung -> Register');
		$table->load($load);
		$template = $table->theText;

		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 1):
			$mailer->addRecipient($testmode_email);
		else:
			$mailer->addRecipient($email);
		endif;
		$template = str_replace('{first_name}',$first_name, $template);
		$template = str_replace('{last_name}',$last_name, $template);
		$template = str_replace('{email}',$email, $template);
		$template = str_replace('{systemname}',$systemName, $template);
		$template = str_replace('{activationlink}', '<a style="text-decoration:none;color: #ff3600;" href="'.$systemLink.'/newplan?r=3&SMA='.base64_encode($tblUserID).'">Bestätigungslink</a>', $template);
		$body 	= $template;
		$mailer->setSubject('Bestätigen Sie Ihre E-Mail-Adresse');
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		//Email to systemadmin
		$mailerA = JFactory::getMailer();
		$senderA = array($emailSender,$emailSenderName);
		$mailerA->setSender($senderA);
		if($testmode == 1):
			$mailerA->addRecipient($testmode_email);
		else:
			$mailerA->addRecipient($emailSender);
		endif;
		$mailerA->setSubject('ADMINMAIL: Benutzer '.$customer_number.', '.$first_name.' '.$last_name.' hat neues Konto erzeugt.');
		$mailerA->isHtml(true);
		$mailerA->setBody($body);
		$sendA = $mailerA->Send();
		if($testmode == 0):
			// 4. Create the Logfile
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Logs','JclassroomTable',array());
			$data = array();
			$data['customerID'] 	= $newCustomerID;
			$data['userID'] 		= $id;
			$data['parameter'] 		= 'Create Account';
			$data['wert'] 			= 'Der Benutzer '.$first_name.' '.$last_name.', Kundennummer: '.$customer_number.'  hat ein Konto angelegt.';
			$data['created'] 		= date('Y-m-d H:i:s');
			$data['created_by'] 	= $id;
			$table->bind($data);
			$table->store();
		endif;
		echo $systemlink.'/newplan?r=5';
		exit();
	}
	function createDemo() {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$plan_from 	= date('Y-m-d');
		$plan_to 	= new Datetime($plan_from);
		$plan_to->add(new Dateinterval('P30D'));
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer','JclassroomTable',array());
		$data = array();
		$data['name'] 			= $formData->getStr('register_first_name').' '.$formData->getStr('register_last_name');
		$data['company_name'] 	= '';
		$data['customer_number'] = 0;
		$data['email'] 			= $formData->getStr('register_email');
		$data['planID'] 		= 4;
		$data['plan_from'] 		= $plan_from;
		$data['plan_to'] 		= $plan_to->format('Y-m-d');
		$data['company_size'] 	= $formData->getStr('size');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$newCustomerID = $table->id;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$data = array();
		$data['salutation'] 	= '';
		$data['first_name'] 	= $formData->getStr('register_first_name');
		$data['last_name'] 		= $formData->getStr('register_last_name');
		$data['phone'] 			= $formData->getStr('register_phone');
		$data['email'] 			= $formData->getStr('register_email');
		$data['function'] 		= $formData->getStr('register_function');
		$data['customerID'] 	= $newCustomerID;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$customerID = $table->id;
		// Create the Plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_plan','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $newCustomerID;
		$data['planID'] 		= 4;
		$data['valid_from'] 	= $plan_from;
		$data['valid_to'] 		= $plan_to->format('Y-m-d');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		// Create the User
		$password = JUserHelper::hashPassword($formData->getStr('register_password'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$data = array();
		$data['name'] 		= $formData->getStr('register_first_name').' '.$formData->getStr('register_last_name');
		$data['username'] 	= $formData->getStr('register_email');
		$data['email'] 		= $formData->getStr('register_email');
		$data['password'] 	= $password;
		$data['block'] 		= 0;
		$data['sendEmail'] 	= 0;
		$data['registerDate'] 	= date('Y-m-d H:i:s');
		$data['lastvisitDate'] 	= date('Y-m-d H:i:s');
		$data['activation'] 	= 0;
		$table->bind($data);
		$table->store();
		$tblUserID = $table->id;
		//Tabelle Usergroup-Map
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Usergroup','JclassroomTable',array());
		$data = array();
		$data['user_id'] 	= $tblUserID;
		$data['group_id'] 	= 13;
		$table->bind($data);
		$table->store();
		// SAVE the user ID
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$table->load($customerID);
		$table->userID = $tblUserID;
		$table->store();
		// SAVE the customer_number
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($newCustomerID);
		$table->customer_number = 'CU'.str_pad($newCustomerID,8,'0', STR_PAD_LEFT);
		$table->store();
		//Send E-Mail for Double-Opt-In
		//Write to Emails
		//Email to customer
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Email','JclassroomTable',array());
		$data 		= array();
		$data['typ'] = 5;
		$data['send'] 	= date('Y-m-d H:i:s');
		$data['send_by'] 	= $user->id;
		$data['send_to'] 	= $tblUserID;
		$data['classroomID'] = 0;
		$table->bind($data);
		$table->store();
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemname');
		$table->load($load);
		$systemName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode');
		$table->load($load);
		$testmode = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode_email');
		$table->load($load);
		$testmode_email = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'subject_confirm');
		$table->load($load);
		$subject = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'text_confirm');
		$table->load($load);
		$template = $table->wert;

		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 0):
			$mailer->addRecipient($formData->getStr('register_email'));
		else:
			$mailer->addRecipient($testmode_email);
		endif;
		$template = str_replace('{first_name}',$formData->getStr('register_first_name'), $template);
		$template = str_replace('{last_name}',$formData->getStr('register_last_name'), $template);
		$template = str_replace('{email}',$formData->getStr('register_email'), $template);
		$template = str_replace('{systemname}',$systemName, $template);
		$template = str_replace('{activationlink}', '<a style="text-decoration:none;color: #ff3600;" href="'.$systemLink.'/doi?SMA='.base64_encode($newCustomerID.'__'.$tblUserID).'">Bestätigungslink</a>', $template);
		$body 	= $template;
		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		//Email to systemadmin
		$mailerA = JFactory::getMailer();
		$senderA = array('training@ceknow.de','ceKnow');
		$mailerA->setSender($senderA);
		if($testmode == 0):
			$mailerA->addRecipient($emailSender);
		else:
			$mailerA->addRecipient($testmode_email);
		endif;
		$mailerA->setSubject('ADMINMAIL: Neuer Demozugang -> '.$subject);
		$mailerA->isHtml(true);
		$mailerA->setBody($body);
		$sendA = $mailerA->Send();
		if($testmode == 0):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Logs','JclassroomTable',array());
			$data = array();
			$data['customerID'] 	= $customerID;
			$data['userID'] 		= $tblUserID;
			$data['parameter'] 		= 'Create Demo';
			$data['wert'] 			= 'Der Benutzer '.$formData->getStr('register_first_name').' '.$formData->getStr('register_last_name').' hat einen Demozugang angelegt.';
			$data['created'] 		= date('Y-m-d H:i:s');
			$data['created_by'] 	= $tblUserID;
			$table->bind($data);
			$table->store();
		endif;
		//JFactory::getApplication()->enqueueMessage('Ihr Demozugang wurde erstellt.', 'Message');
		$this->setRedirect(JRoute::_(JURI::Root().'checkDemo', false));
	}
	function setAccount() {
		$input 		= JFactory::getApplication()->input;
		$planID 	= $input->get('pid', '', 'INT');
		$this->setRedirect(JRoute::_(JURI::Root().'login?set=1&pid='.$planID, false));
	}
	function completeAccount() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		if($user->id):
			$session->set('setAccount', 1);
		else:
			$session->set('setAccount', 0);
		endif;
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($input->get('customerID', 0, 'INT'));
		$table->address 	= $input->get('invoice_adress', '', 'STR');
		$table->postcode 	= $input->get('invoice_postcode', '', 'STR');
		$table->city 		= $input->get('invoice_city', '', 'STR');
		$table->planID 		= $input->get('planID', '', 'INT');
		$table->plan_from 	= date('Y-m-d');
		$plan_to 			= date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 1 year"));
		$table->plan_to 	= $plan_to;
 		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by = 0;
		$table->store();
		$email = $table->email;
		// Create the Plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
		$load = array('customerID' => $input->get('customerID', 0, 'INT'));
		$check = $table->load($load);
		if(!$check):
			$data = array();
			if($input->get('paymentAbo', 0, 'STR') == 'monthly'):
				$paymentAbo = 1;
			else:	
				$paymentAbo = 2;
			endif;
			if($input->get('paymentType', 0, 'STR') == 'invoice'):
				$paymentType = 1;
			else:	
				$paymentType = 2;
			endif;
			$data['customerID'] 	= $input->get('customerID', 0, 'INT');
			$data['paymentAbo'] 	= $paymentAbo;
			$data['paymentID'] 		= $paymentType;
			if($input->get('alt_invoice',0, 'INT') == 1):
				$data['alt_invoice'] 	= 1;
				$data['invoice_first_name'] 	= $input->get('invoice_first_name', 0, 'STR');
				$data['invoice_last_name'] 		= $input->get('invoice_last_name', 0, 'STR');
				$data['invoice_function'] 	= $input->get('invoice_function', 0, 'STR');
				$data['invoice_title'] 	= $input->get('invoice_company', 0, 'STR');
				$data['invoice_adress'] = $input->get('invoice_alt_adress', 0, 'STR');
				$data['invoice_postcode'] = $input->get('invoice_alt_plz', 0, 'STR');
				$data['invoice_city'] = $input->get('invoice_alt_city', 0, 'STR');
			else:
				$data['invoice_title'] 	= $input->get('invoice_company', 0, 'STR');
				$data['invoice_adress'] = $input->get('invoice_adress', 0, 'STR');
				$data['invoice_postcode'] = $input->get('invoice_postcode', 0, 'STR');
				$data['invoice_city'] = $input->get('invoice_city', 0, 'STR');
			endif;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= 0;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// Create the Planorder
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Planorders','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $input->get('customerID', 0, 'INT');
		$data['orderDate'] 		= date('Y-m-d H:i:s');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= 0;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$planorderID = $table->id;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Planorders_positions','JclassroomTable',array());
		$data = array();
		if($input->get('paymentAbo', 0, 'STR') == 'monthly'):
			$paymentAbo = 1;
			$discount = 0;
		else:	
			$paymentAbo = 2;
			$discount = 25;
		endif;
		if($input->get('paymentType', 0, 'STR') == 'invoice'):
			$paymentType = 1;
		else:	
			$paymentType = 2;
		endif;
		$data['planorderID'] 	= $planorderID;
		$data['planID'] 		= $input->get('planID', 0, 'INT');
		$data['paymentAbo'] 	= $paymentAbo;
		$data['paymentID'] 		= $paymentType;
		// Load the plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$plan 	= JTable::getInstance('Plan','JclassroomTable',array());
		$plan->load($input->get('planID', 0, 'INT'));
		$data['price'] 		= $plan->price;
		$data['discount']	= $discount;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= 0;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		// 1. Activate the Useraccount
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$ca 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$load = array('customerID' => $input->get('customerID', 0, 'INT'));
		$ca->load($load);
		$userID = $ca->userID;
		// 1. Activate the Useraccount
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$us 	= JTable::getInstance('User','JclassroomTable',array());
		$us->load($userID);
		$us->block = 0;
		$us->store();
		//Create Confirmmail
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemname');
		$table->load($load);
		$systemName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode');
		$table->load($load);
		$testmode = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode_email');
		$table->load($load);
		$testmode_email = $table->wert;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Textmodule','JclassroomTable',array());
		$load 		= array('section' => 1, 'title' => 'Bestätigungsmail Planbestellung -> Final');
		$table->load($load);
		$template 	= $table->theText;
		$subject 	= $table->theSubject;
		// Load the customer
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$customerC 	= JTable::getInstance('Customer','JclassroomTable',array());
		$customerC->load($input->get('customerID', 0, 'INT'));
		// Load the order
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$customerA 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$load = array('customerID' => $input->get('customerID', 0, 'INT'));
		$customerA->load($load);
		// Load the payment
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$customerP 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
		$load = array('customerID' => $input->get('customerID', 0, 'INT'));
		$customerP->load($load);
		// Load the order
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$planorder 	= JTable::getInstance('Planorders','JclassroomTable',array());
		$planorder->load($planorderID);

		switch($customerP->paymentAbo):
			case 1: 
				$paymentAbo = 'Monatlich';
				$price_complete = $plan->price;
				$discountT = '';
				break;
			case 2: 
				$paymentAbo = 'Jährlich';
				$price_complete = (($plan->price * (100 - $discount)) / 100) * 12;
				$discountT 	= '(abzgl. '.$discount.'% Rabatt)';
				break;
		endswitch;
		$tax = ($price_complete * 19) / 100;
		$price_tax = ($price_complete * 119) / 100;
		switch($customerP->paymentID):
			case 1: 
				$paymentType = 'Rechnung';
				break;
			case 2: 
				$paymentType = 'Bankeinzug';
				break;
		endswitch;
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 1):
			$mailer->addRecipient($testmode_email);
		else:
			$mailer->addRecipient($customerC->email);
		endif;
		$template = str_replace('{first_name}',$customerA->first_name, $template);
		$template = str_replace('{last_name}',$customerA->last_name, $template);
		$template = str_replace('{email}',$email, $template);
		$template = str_replace('{systemname}',$systemName, $template);
		$template = str_replace('{customer_number}',$customerC->customer_number, $template);
		$template = str_replace('{company}',$customerP->invoice_title, $template);
		$template = str_replace('{invoice_adress}',$customerP->invoice_adress, $template);
		$template = str_replace('{invoice_postcode}',$customerP->invoice_postcode, $template);
		$template = str_replace('{invoice_city}',$customerP->invoice_city, $template);
		$template = str_replace('{department}',$customerP->invoice_function, $template);
		$template = str_replace('{planname}',$plan->title, $template);
		$template = str_replace('{price}',number_format($plan->price,2,',','.').'€', $template);
		$template = str_replace('{price_complete}',number_format($price_complete,2,',','.').'€', $template);
		$template = str_replace('{tax}',number_format($tax,2,',','.').'€', $template);
		$template = str_replace('{price_tax}',number_format($price_tax,2,',','.').'€', $template);
		$template = str_replace('{paymentAbo}',$paymentAbo, $template);
		$template = str_replace('{paymentType}',$paymentType, $template);
		$template = str_replace('{ordernumber}','B01-'.str_pad($planorder->id,8,'0', STR_PAD_LEFT), $template);
		$template = str_replace('{orderdate}',date('d.m.Y', strtotime($planorder->orderDate)), $template);
		$template = str_replace('{duration}','Unbefristet', $template);
		$template = str_replace('{discount}',$discountT, $template);
		$template = str_replace('{support_link}','<a href="www.ce.de/kontakt">ce.de</a>', $template);
		$body 	= $template;
		$subject = str_replace('{first_name}',$customerA->first_name, $subject);
		$subject = str_replace('{last_name}',$customerA->last_name, $subject);
		$subject = str_replace('{ordernumber}','B01-'.str_pad($planorder->id,8,'0', STR_PAD_LEFT), $subject);
		$subject = str_replace('{orderdate}',date('d.m.Y', strtotime($planorder->orderDate)), $subject);
		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		// Create Adminmail
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 1):
			$mailer->addRecipient($testmode_email);
		else:
			$mailer->addRecipient($emailSender);
		endif;
		$mailer->setSubject('ADMINMAIL: Bestellbestätigung für Bestellung bei '.$systemName.' Kunde: '.$customerC->customer_number.' ('.$customerA->first_name.' '.$customerA->last_name.')');
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		if($testmode == 0):
			// 4. Create the Logfile
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Logs','JclassroomTable',array());
			$data = array();
			$data['customerID'] 	= $customerC->customer_number;
			$data['userID'] 		= $id;
			$data['parameter'] 		= 'Execute Account';
			$data['wert'] 			= 'Der Benutzer '.$customerA->first_name.' '.$customerA->last_name.', Kundennummer: '.$customerC->customer_number.'  hat eine Bestellung für Plan '.$plan->title.' ausgelöst.';
			$data['created'] 		= date('Y-m-d H:i:s');
			$data['created_by'] 	= $id;
			$table->bind($data);
			$table->store();
		endif;
		echo $systemlink.'/newplan?r=6&pid='.$planorderID.'&SMA='.base64_encode($customerC->id);
		exit();	
	}
	function changeAccount() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($input->get('customerID', 0, 'INT'));
		$table->address 	= $input->get('invoice_adress', '', 'STR');
		$table->postcode 	= $input->get('invoice_postcode', '', 'STR');
		$table->city 		= $input->get('invoice_city', '', 'STR');
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by = $user->id;
		$table->planID 		= $input->get('planID', 0, 'INT');
		$table->plan_from 	= date('Y-m-d');
		$plan_from 	= date('Y-m-d');
		$plan_to 	= new Datetime($plan_from);
		$plan_to->add(new Dateinterval('P1Y'));
		$table->plan_to 		= $plan_to->format('Y-m-d');
		$table->store();
		// Create the Plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
		$load = array('customerID' => $input->get('customerID', 0, 'INT'));
		$check = $table->load($load);
		if(!$check):
			$data = array();
			if($input->get('paymentAbo', 0, 'STR') == 'monthly'):
				$paymentAbo = 1;
			else:	
				$paymentAbo = 2;
			endif;
			if($input->get('paymentType', 0, 'STR') == 'invoice'):
				$paymentType = 1;
			else:	
				$paymentType = 2;
			endif;
			$data['customerID'] 	= $input->get('customerID', 0, 'INT');
			$data['paymentAbo'] 	= $paymentAbo;
			$data['paymentID'] 		= $paymentType;
			if($input->get('alt_invoice',0, 'INT') == 1):
				$data['alt_invoice'] 	= 1;
				$data['invoice_first_name'] 	= $input->get('invoice_first_name', 0, 'STR');
				$data['invoice_last_name'] 		= $input->get('invoice_last_name', 0, 'STR');
				$data['invoice_function'] 	= $input->get('invoice_function', 0, 'STR');
				$data['invoice_title'] 	= $input->get('invoice_company', 0, 'STR');
				$data['invoice_adress'] = $input->get('invoice_alt_adress', 0, 'STR');
				$data['invoice_postcode'] = $input->get('invoice_alt_plz', 0, 'STR');
				$data['invoice_city'] = $input->get('invoice_alt_city', 0, 'STR');
			else:
				$data['invoice_title'] 	= $input->get('invoice_company', 0, 'STR');
				$data['invoice_adress'] = $input->get('invoice_adress', 0, 'STR');
				$data['invoice_postcode'] = $input->get('invoice_postcode', 0, 'STR');
				$data['invoice_city'] = $input->get('invoice_city', 0, 'STR');
			endif;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= 0;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		else:
			if($input->get('paymentAbo', 0, 'STR') == 'monthly'):
				$paymentAbo = 1;
			else:	
				$paymentAbo = 2;
			endif;
			if($input->get('paymentType', 0, 'STR') == 'invoice'):
				$paymentType = 1;
			else:	
				$paymentType = 2;
			endif;
			$table->paymentAbo 	= $paymentAbo;
			$table->paymentID 	= $paymentType;
			if($input->get('alt_invoice',0, 'INT') == 1):
				$table->alt_invoice 	= 1;
				$table->invoice_first_name 	= $input->get('invoice_first_name', 0, 'STR');
				$table->invoice_last_name 	= $input->get('invoice_last_name', 0, 'STR');
				$table->invoice_function 	= $input->get('invoice_function', 0, 'STR');
				$table->invoice_title		= $input->get('invoice_company', 0, 'STR');
				$table->invoice_adress 		= $input->get('invoice_alt_adress', 0, 'STR');
				$table->invoice_postcode 	= $input->get('invoice_alt_plz', 0, 'STR');
				$table->invoice_city 		= $input->get('invoice_alt_city', 0, 'STR');
			else:
				$table->invoice_title 		= $input->get('invoice_company', 0, 'STR');
				$table->invoice_adress 		= $input->get('invoice_adress', 0, 'STR');
				$table->invoice_postcode 	= $input->get('invoice_postcode', 0, 'STR');
				$table->invoice_city 		= $input->get('invoice_city', 0, 'STR');
			endif;
			$table->modified	= date('Y-m-d H:i:s');
			$table->modified_by	= $user->id;
			$table->store();
		endif;
		// Create the Planorder
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Planorders','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $input->get('customerID', 0, 'INT');
		$data['orderDate'] 		= date('Y-m-d H:i:s');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= 0;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$planorderID = $table->id;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Planorders_positions','JclassroomTable',array());
		$data = array();
		if($input->get('paymentAbo', 0, 'STR') == 'monthly'):
			$paymentAbo = 1;
			$discount = 0;
		else:	
			$paymentAbo = 2;
			$discount = 25;
		endif;
		if($input->get('paymentType', 0, 'STR') == 'invoice'):
			$paymentType = 1;
		else:	
			$paymentType = 2;
		endif;
		$data['planorderID'] 	= $planorderID;
		$data['planID'] 		= $input->get('planID', 0, 'INT');
		$data['paymentAbo'] 	= $paymentAbo;
		$data['paymentID'] 		= $paymentType;
		// Load the plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$plan 	= JTable::getInstance('Plan','JclassroomTable',array());
		$plan->load($input->get('planID', 0, 'INT'));
		$data['price'] 		= $plan->price;
		$data['discount']	= $discount;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= 0;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		// 1. Activate the Useraccount
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$ca 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$load = array('customerID' => $input->get('customerID', 0, 'INT'));
		$ca->load($load);
		$userID = $ca->userID;
		// 1. Activate the Useraccount
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$us 	= JTable::getInstance('User','JclassroomTable',array());
		$us->load($userID);
		$us->block = 0;
		$us->store();
		//Create Confirmmail
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemname');
		$table->load($load);
		$systemName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode');
		$table->load($load);
		$testmode = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode_email');
		$table->load($load);
		$testmode_email = $table->wert;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Textmodule','JclassroomTable',array());
		$load 		= array('section' => 2, 'title' => 'Bestätigungsmail Planwechsel Upgrade');
		$table->load($load);
		$template 	= $table->theText;
		$subject 	= $table->theSubject;
		// Load the customer
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$customerC 	= JTable::getInstance('Customer','JclassroomTable',array());
		$customerC->load($input->get('customerID', 0, 'INT'));
		// Load the order
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$customerA 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$load = array('customerID' => $input->get('customerID', 0, 'INT'));
		$customerA->load($load);
		// Load the payment
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$customerP 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
		$load = array('customerID' => $input->get('customerID', 0, 'INT'));
		$customerP->load($load);
		// Load the plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$plan 	= JTable::getInstance('Plan','JclassroomTable',array());
		$plan->load($customerC->planID);
		// Load the order
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$planorder 	= JTable::getInstance('Planorders','JclassroomTable',array());
		$planorder->load($planorderID);

		switch($customerP->paymentAbo):
			case 1: 
				$paymentAbo = 'Monatlich';
				$price_complete = $plan->price;
				$discountT = '';
				break;
			case 2: 
				$paymentAbo = 'Jährlich';
				$price_complete = (($plan->price * (100 - $discount)) / 100) * 12;
				$discountT 	= '(abzgl. '.$discount.'% Rabatt)';
				break;
		endswitch;
		$tax = ($price_complete * 19) / 100;
		$price_tax = ($price_complete * 119) / 100;
		switch($customerP->paymentID):
			case 1: 
				$paymentType = 'Rechnung';
				break;
			case 2: 
				$paymentType = 'Bankeinzug';
				break;
		endswitch;
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 1):
			$mailer->addRecipient($testmode_email);
		else:
			$mailer->addRecipient($email);
		endif;
		$template = str_replace('{first_name}',$customerA->first_name, $template);
		$template = str_replace('{last_name}',$customerA->last_name, $template);
		$template = str_replace('{email}',$email, $template);
		$template = str_replace('{systemname}',$systemName, $template);
		$template = str_replace('{customer_number}',$customerC->customer_number, $template);
		$template = str_replace('{company}',$customerP->invoice_title, $template);
		$template = str_replace('{invoice_adress}',$customerP->invoice_adress, $template);
		$template = str_replace('{invoice_postcode}',$customerP->invoice_postcode, $template);
		$template = str_replace('{invoice_city}',$customerP->invoice_city, $template);
		$template = str_replace('{department}',$customerP->invoice_function, $template);
		$template = str_replace('{planname}',$plan->title, $template);
		$template = str_replace('{price}',number_format($plan->price,2,',','.').'€', $template);
		$template = str_replace('{price_complete}',number_format($price_complete,2,',','.').'€', $template);
		$template = str_replace('{tax}',number_format($tax,2,',','.').'€', $template);
		$template = str_replace('{price_tax}',number_format($price_tax,2,',','.').'€', $template);
		$template = str_replace('{paymentAbo}',$paymentAbo, $template);
		$template = str_replace('{paymentType}',$paymentType, $template);
		$template = str_replace('{ordernumber}','B01-'.str_pad($planorder->id,8,'0', STR_PAD_LEFT), $template);
		$template = str_replace('{orderdate}',date('d.m.Y', strtotime($planorder->orderDate)), $template);
		$template = str_replace('{duration}','Unbefristet', $template);
		$template = str_replace('{discount}',$discountT, $template);
		$template = str_replace('{support_link}','<a href="www.ce.de/kontakt">ce.de</a>', $template);
		$body 	= $template;
		$subject = str_replace('{first_name}',$customerA->first_name, $subject);
		$subject = str_replace('{last_name}',$customerA->last_name, $subject);
		$subject = str_replace('{email}',$email, $subject);
		$subject = str_replace('{systemname}',$systemName, $subject);
		$subject = str_replace('{customer_number}',$customerC->customer_number, $subject);
		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		// Create Adminmail
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 1):
			$mailer->addRecipient($testmode_email);
		else:
			$mailer->addRecipient($emailSender);
		endif;
		$mailer->setSubject('ADMINMAIL: '.$subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		if($testmode == 0):
			// 4. Create the Logfile
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Logs','JclassroomTable',array());
			$data = array();
			$data['customerID'] 	= $customerC->customer_number;
			$data['userID'] 		= $id;
			$data['parameter'] 		= 'Execute Account';
			$data['wert'] 			= 'Der Benutzer '.$customerA->first_name.' '.$customerA->last_name.', Kundennummer: '.$customerC->customer_number.'  hat eine Bestellung für Plan '.$plan->title.' ausgelöst.';
			$data['created'] 		= date('Y-m-d H:i:s');
			$data['created_by'] 	= $id;
			$table->bind($data);
			$table->store();
		endif;
		echo $systemlink.'/newplan?r=6&SMA='.base64_encode($customerC->id).'&pid='.$planorderID;
		exit();
		
	}
	function downgradePlan() {
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$planID 	= $input->get('planID', 0, 'INT');
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		//Create Confirmmail
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemname');
		$table->load($load);
		$systemName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode');
		$table->load($load);
		$testmode = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode_email');
		$table->load($load);
		$testmode_email = $table->wert;
		// Load the order
		require_once(JPATH_COMPONENT_SITE.'/helpers/loadPersons.php');
        $customerData = LoadpersonsHelper::loadCustomeradmin($user->id);
        // Get the Plan
        JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$plan 		= JTable::getInstance('Plan','JclassroomTable',array());
		$plan->load($planID);

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Textmodule','JclassroomTable',array());
		$load 		= array('section' => 2, 'title' => 'Bestätigungsmail Planwechsel Downgrade');
		$table->load($load);
		$template 	= $table->theText;
		$subject 	= $table->theSubject;
		$subject = str_replace('{first_name}',$customerData->first_name, $subject);
		$subject = str_replace('{last_name}',$customerData->last_name, $subject);
		$subject = str_replace('{customer_number}',$customerData->customer_number, $subject);
		$subject = str_replace('{plan_name}',$plan->title.'plan', $subject);
		// Usermail
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 1):
			$mailer->addRecipient($testmode_email);
		else:
			$mailer->addRecipient($customerData->email);
		endif;
		$template = str_replace('{first_name}',$customerData->first_name, $template);
		$template = str_replace('{last_name}',$customerData->last_name, $template);
		$template = str_replace('{customer_number}',$customerData->customer_number, $template);
		$template = str_replace('{plan_name}',$plan->title.'plan', $template);
		$template = str_replace('{plan_description}',$plan->description, $template);

		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($template);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		// Adminmail
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 1):
			$mailer->addRecipient($testmode_email);
		else:
			$mailer->addRecipient($emailSender);
		endif;
		$mailer->setSubject('ADMINMAIL: '.$subject);
		$mailer->isHtml(true);
		$mailer->setBody($template);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		if($testmode == 0):
			// 4. Create the Logfile
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Logs','JclassroomTable',array());
			$data = array();
			$data['customerID'] 	= $customerID;
			$data['userID'] 		= $id;
			$data['parameter'] 		= 'Change Plan';
			$data['wert'] 			= 'Der Benutzer '.$customerData->first_name.' '.$customerData->last_name.', Kundennummer: '.$customerID.'  hat ein Downgrade seines Plans auf Plan: '.$plan->title.' angefragt.';
			$data['created'] 		= date('Y-m-d H:i:s');
			$data['created_by'] 	= $id;
			$table->bind($data);
			$table->store();
		endif;
		echo $return;
		exit();
	}
	function cancelPlan() {
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$user 		= JFactory::getUser();
		//Create Confirmmail
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemname');
		$table->load($load);
		$systemName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode');
		$table->load($load);
		$testmode = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'testmode_email');
		$table->load($load);
		$testmode_email = $table->wert;
		// Load the order
		require_once(JPATH_COMPONENT_SITE.'/helpers/loadPersons.php');
        $customerData = LoadpersonsHelper::loadCustomeradmin($user->id);

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Textmodule','JclassroomTable',array());
		$load 		= array('section' => 2, 'title' => 'Kündigungsmail Plan');
		$table->load($load);
		$template 	= $table->theText;
		$subject 	= $table->theSubject;
		$subject = str_replace('{first_name}',$customerData->first_name, $subject);
		$subject = str_replace('{last_name}',$customerData->last_name, $subject);
		$subject = str_replace('{customer_number}',$customerData->customer_number, $subject);
		// Usermail
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 1):
			$mailer->addRecipient($testmode_email);
		else:
			$mailer->addRecipient($customerData->customer_email);
		endif;
		$template = str_replace('{first_name}',$customerData->first_name, $template);
		$template = str_replace('{last_name}',$customerData->last_name, $template);
		$template = str_replace('{customer_number}',$customerData->customer_number, $template);

		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($template);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		// Adminmail
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 1):
			$mailer->addRecipient($testmode_email);
		else:
			$mailer->addRecipient($emailSender);
		endif;
		$mailer->setSubject('ADMINMAIL: '.$subject);
		$mailer->isHtml(true);
		$mailer->setBody($template);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		if($testmode == 0):
			// 4. Create the Logfile
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Logs','JclassroomTable',array());
			$data = array();
			$data['customerID'] 	= $customerID;
			$data['userID'] 		= $id;
			$data['parameter'] 		= 'Delete Account';
			$data['wert'] 			= 'Der Benutzer '.$customerData->first_name.' '.$customerData->last_name.', Kundennummer: '.$customerID.'  hat seinen Plan gekündigt.';
			$data['created'] 		= date('Y-m-d H:i:s');
			$data['created_by'] 	= $id;
			$table->bind($data);
			$table->store();
		endif;
		echo $return;
		exit();
	}
	function newPlan() {
		$user 		= JFactory::getUser();
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$p 			= $formData->getInt('planID');
		$paymentID	= $formData->getInt('paymentIDPlan');
		$plan_from 	= date('Y-m-d');
		$plan_to 	= new Datetime($plan_from);
		$plan_to->add(new Dateinterval('P1Y'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer','JclassroomTable',array());
		$data = array();
		$data['name'] 			= $formData->getStr('first_name').' '.$formData->getStr('last_name');
		$data['company_name'] 	= '';
		$data['customer_number'] = 0;
		$data['email'] 			= $formData->getStr('email');
		$data['address'] 		= $formData->getStr('address');
		$data['postcode'] 		= $formData->getStr('postcode');
		$data['city'] 			= $formData->getStr('city');
		$data['planID'] 		= $formData->getInt('planID');
		$data['plan_from'] 		= $plan_from;
		$data['plan_to'] 		= $plan_to->format('Y-m-d');
		$data['company_size'] 	= $formData->getStr('size');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$newCustomerID = $table->id;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$data = array();
		$data['salutation'] 	= '';
		$data['first_name'] 	= $formData->getStr('first_name');
		$data['last_name'] 		= $formData->getStr('last_name');
		$data['phone'] 			= $formData->getStr('phone');
		$data['mobile'] 		= $formData->getStr('mobile');
		$data['email'] 			= $formData->getStr('email');
		$data['function'] 		= $formData->getStr('function');
		$data['customerID'] 	= $newCustomerID;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$customerID = $table->id;
		// Create the Plan
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_plan','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $newCustomerID;
		$data['planID'] 		= $formData->getInt('planID');
		$data['valid_from'] 	= $plan_from;
		$data['valid_to'] 		= $plan_to->format('Y-m-d');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		// Create the Payment
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $newCustomerID;
		$data['playmentID'] 	= $formData->getInt('planID');
		$data['invoice_title'] 		= $formData->getStr('company');
		$data['invoice_adress'] 	= $formData->getStr('address');
		$data['invoice_postcode'] 	= $formData->getStr('postcode');
		$data['invoice_city'] 		= $formData->getStr('city');
		$data['account_owner'] 		= $formData->getStr('account_owner');
		$data['iban'] 		= $formData->getStr('iban');
		$data['bic'] 		= $formData->getStr('bic');
		$data['valid_from'] 	= $plan_from;
		$data['valid_to'] 		= $plan_to->format('Y-m-d');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		// Create the User
		$password = JUserHelper::hashPassword($formData->getStr('p1'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$data = array();
		$data['name'] 		= $formData->getStr('first_name').' '.$formData->getStr('last_name');
		$data['username'] 	= $formData->getStr('email');
		$data['email'] 		= $formData->getStr('email');
		$data['password'] 	= $password;
		$data['block'] 		= 0;
		$data['sendEmail'] 	= 0;
		$data['registerDate'] 	= date('Y-m-d H:i:s');
		$data['lastvisitDate'] 	= date('Y-m-d H:i:s');
		$data['activation'] 	= 0;
		$table->bind($data);
		$table->store();
		$tblUserID = $table->id;
		echo $tblUserID;
		//Tabelle Usergroup-Map
		/*JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Usergroup','JclassroomTable',array());
		$data = array();
		$data['user_id'] 	= $tblUserID;
		$data['group_id'] 	= 13;
		$table->bind($data);
		$table->store();
		// SAVE the user ID
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$table->load($customerID);
		$table->userID = $tblUserID;
		$table->store();
		// SAVE the customer_number
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($newCustomerID);
		$table->customer_number = 'CU'.str_pad($newCustomerID,8,'0', STR_PAD_LEFT);
		$table->store();
		// Load the plans
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableP = JTable::getInstance('Plan','JclassroomTable',array());
		$tableP->load($formData->getInt('planID'));
		$planname = $tableP->title;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Logs','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $newCustomerID;
		$data['userID'] 		= $tblUserID;
		$data['parameter'] 		= 'Create Plan';
		$data['wert'] 			= 'Der Benutzer '.$formData->getStr('first_name').' '.$formData->getStr('last_name').' hat den Plan '.$planname.' angelegt.';
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $tblUserID;
		$table->bind($data);
		$table->store();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableL 	= JTable::getInstance('Logs','JclassroomTable',array());
		$data = array();
		$data['userID'] 		= $user->id;
		$data['parameter'] 		= 'Consent';
		$data['wert'] 			= 'Der Benutzer '.$formData->getStr('first_name').' '.$formData->getStr('last_name') .' hat sich mit der Datenschutzerklärung und den Nutzungsbedingungen einverstanden erklärt.';
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$tableL->bind($data);
		$tableL->store();
		/*$this->sendMail($newCustomerID, $formData->getInt('planID'), 'confirmPlanNew');
		JFactory::getApplication()->enqueueMessage('Ihr Konto wurde erfolgreich angelegt und der gewählte Plan aktiviert.', 'Message');
		$this->setRedirect(JRoute::_('index.php?option=com_jclassroom&view=customer&r=2', false));*/
	}
	function editPlan() {
		$user 		= JFactory::getUser();
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$p 			= $formData->getInt('planID');
		$paymentID	= $formData->getInt('paymentIDPlan');
		$plan_from 	= date('Y-m-d');
		$plan_to 	= new Datetime($plan_from);
		$plan_to->add(new Dateinterval('P1Y'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($customerID);
		$table->planID 		= $p;
		$table->plan_from 	= $planFrom;
		$table->plan_to 	= $plan_to->format('Y-m-d');
		$table->store();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableA 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
		$load = array('customerID' => $customerID);
		$tableA->load($load);
		$tableA->paymentID 	= $formData->getInt('paymentIDPlan');
		$tableA->invoice_title 	= $formData->getStr('invoice_title');
		$tableA->invoice_adress 	= $formData->getStr('invoice_adress');
		$tableA->invoice_postcode 	= $formData->getStr('invoice_postcode');
		$tableA->invoice_city 	= $formData->getStr('invoice_city');
		$tableA->account_owner 	= $formData->getStr('account_owner');
		$tableA->iban 	= $formData->getStr('iban');
		$tableA->bic 	= $formData->getStr('bic');
		$tableA->modified 		= date('Y-m-d H:i:s');
		$tableA->modified_by 	= $user->id;
		$tableA->store();
		$this->sendMail($customerID, $planID, 'confirmPlanChange');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableP 	= JTable::getInstance('Plan','JclassroomTable',array());
		$tableP->load($p);
		$planname = $tableP->title;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableL 	= JTable::getInstance('Logs','JclassroomTable',array());
		$data = array();
		$data['userID'] 		= $user->id;
		$data['parameter'] 		= 'Planchange';
		$data['wert'] 			= 'Der Benutzer '.$table->name.' ('.$table->company_name.' : '.$table->customer_number.' ) hat seinen Plan auf '.$planname.' geändert.';
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$tableL->bind($data);
		$tableL->store();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableL 	= JTable::getInstance('Logs','JclassroomTable',array());
		$data = array();
		$data['userID'] 		= $user->id;
		$data['parameter'] 		= 'Consent';
		$data['wert'] 			= 'Der Benutzer '.$table->name.' ('.$table->company_name.' : '.$table->customer_number.' ) hat sich mit der Datenschutzerklärung und den Nutzungsbedingungen einverstanden erklärt.';
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$tableL->bind($data);
		$tableL->store();
		JFactory::getApplication()->enqueueMessage('Ihr Plan wurde erfolgreich gewechselt.', 'Message');
		$this->setRedirect(JRoute::_('index.php?option=com_jclassroom&view=customer&r=1', false));
	}
	function sendMail($customerID, $planID, $mailType) {
		$session 	= JFactory::getSession();
		$group 		= $session->get('group');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($customerID);
		$email 	= $table->email;
		$name 	= $table->name; 
		$planID = $table->planID;
		$globalTestmode 		= 0;
		$globalTestmodeEmail 	= '';
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		if($group == 'superuser'):
			$load 		= array('customerID' => 0, 'parameter' => 'testmode');
			$check = $table->load($load);
			if($check && $table->wert == 1):
				$globalTestmode = 1;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableA 	= JTable::getInstance('Configuration','JclassroomTable',array());
				$load 		= array('customerID' => 0, 'parameter' => 'testmode_email');
				$tableA->load($load);
				$globalTestmodeEmail = $tableA->wert;
				$email 		= $tableA->wert; 
			endif;
		endif;
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemname');
		$table->load($load);
		$systemName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		//If mailtype is Demozugang
		if($mailType == 'confirmDemoaccount'):
			$load 		= array('customerID' => 0, 'parameter' => 'subject_confirm');
			$table->load($load);
			$subject = $table->wert;
			$load 		= array('customerID' => 0, 'parameter' => 'text_confirm');
			$table->load($load);
			$template = $table->wert;
			$planname = '';
			$plandescription = '';
		endif;
		//If mailtype is Planchange
		if($mailType == 'confirmPlanChange'):
			$load 		= array('customerID' => 0, 'parameter' => 'subject_plan');
			$table->load($load);
			$subject = $table->wert;
			$load 		= array('customerID' => 0, 'parameter' => 'text_plan');
			$table->load($load);
			$template = $table->wert;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 		= JTable::getInstance('Plan','JclassroomTable',array());
			$table->load($planID);
			$planname = $table->titel;
			$plandescription = $table->description;
		endif;
		//If mailtype is Plannew
		if($mailType == 'confirmPlanNew'):
			$load 		= array('customerID' => 0, 'parameter' => 'subject_plan');
			$table->load($load);
			$subject = $table->wert;
			$load 		= array('customerID' => 0, 'parameter' => 'text_plan');
			$table->load($load);
			$template = $table->wert;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 		= JTable::getInstance('Plan','JclassroomTable',array());
			$table->load($planID);
			$planname = $table->titel;
			$plandescription = $table->description;
		endif;
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		$mailer->addRecipient($email);
		//$mailer->addRecipient('ts@torstenscheel.de');
		$template = str_replace('{first_name}',$name, $template);
		$template = str_replace('{last_name}','', $template);
		$template = str_replace('{email}',$email, $template);
		$template = str_replace('{systemname}',$systemName, $template);
		$template = str_replace('{planname}',$planname, $template);
		$template = str_replace('{plandescription}',$plandescription, $template);
		$body 	= $template;
		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail wurde erfolgreich versendet';
		}
		//Email to systemadmin
		$mailerA = JFactory::getMailer();
		$senderA = array('training@ceknow.de','ceKnow');
		$mailerA->setSender($senderA);
		$mailerA->addRecipient($emailAdmin);
		$mailerA->setSubject('ADMINMAIL: Neue Planbuchung ceKNOW -> '.$subject);
		$mailerA->isHtml(true);
		$mailerA->setBody($body);
		$sendA = $mailerA->Send();
	}
	function addContact() {
		$input 		= JFactory::getApplication()->input;
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Contact','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $input->get('customerID',0,'INT');
		$data['salutation'] 	= $input->get('salutation','','STR');
		$data['first_name'] 	= $input->get('first_name','','STR');
		$data['last_name'] 		= $input->get('last_name','','STR');
		$data['phone'] 			= $input->get('phone','','STR');
		$data['email'] 			= $input->get('email','','STR');
		$data['function'] 		= $input->get('functionS','','STR');
		$data['type'] 			= 1;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		echo $table->id;
		exit();
	}
	function editContact() {
		$input 		= JFactory::getApplication()->input;
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Contact','JclassroomTable',array());
		$table->load($input->get('contactID', 0,'INT'));
		$table->salutation 		= $input->get('salutation','','STR');
		$table->first_name 		= $input->get('first_name','','STR');
		$table->last_name 		= $input->get('last_name','','STR');
		$table->phone 			= $input->get('phone','','STR');
		$table->email 			= $input->get('email','','STR');
		$table->function 		= $input->get('functionS','','STR');
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->published 	= 1;
		$table->store();
		echo $table->id;
		exit();
	}
	function getContact() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0,'INT');
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_contacts','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('id', 0, 'INT')));
		$db->setQuery($query);
		$contact = $db->loadObject();
		echo json_encode($contact);
		exit();
	}
	function deleteContact() {
		$input 	= JFactory::getApplication()->input;
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Contact','JclassroomTable',array());
		$table->delete($input->get('id', 0, 'INT'));
		echo 'OK';
		exit();
	}
	function addAdmin() {
		$input 		= JFactory::getApplication()->input;
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $input->get('customerID','','INT');
		$data['salutation'] 	= $input->get('salutation','','STR');
		$data['first_name'] 	= $input->get('first_name','','STR');
		$data['last_name'] 		= $input->get('last_name','','STR');
		$data['phone'] 			= $input->get('phone','','STR');
		$data['email'] 			= $input->get('email','','STR');
		$data['function'] 		= $input->get('functionS','','STR');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$customerID = $table->id;
		$password = JUserHelper::hashPassword($input->get('password','','STR'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$data = array();
		$data['name'] 		= $input->get('first_name','','STR').' '.$input->get('last_name','','STR');
		$data['username'] 	= $input->get('email','','STR');
		$data['email'] 		= $input->get('email','','STR');
		$data['password'] 	= $password;
		$data['block'] 		= 0;
		$data['sendEmail'] 	= 0;
		$data['registerDate'] 	= date('Y-m-d H:i:s');
		$data['lastvisitDate'] 	= date('Y-m-d H:i:s');
		$data['activation'] 	= 0;
		$table->bind($data);
		$table->store();
		$tblUserID = $table->id;
		//Tabelle Usergroup-Map
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Usergroup','JclassroomTable',array());
		$data = array();
		$data['user_id'] 	= $tblUserID;
		$data['group_id'] 	= 13;
		$table->bind($data);
		$table->store();
		// SAVE the user ID
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$table->load($customerID);
		$table->userID = $tblUserID;
		$table->store();
		echo $table->id;
		exit();
	}
	function editAdmin() {
		$input 		= JFactory::getApplication()->input;
		$user 		= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$table->load($input->get('administratorID', 0, 'INT'));
		$userID = $table->userID;
		$table->salutation 		= $input->get('salutation','','STR');
		$table->first_name 		= $input->get('first_name','','STR');
		$table->last_name 		= $input->get('last_name','','STR');
		$table->phone 			= $input->get('phone','','STR');
		$table->email 			= $input->get('email','','STR');
		$table->function 		= $input->get('funktion','','STR');
		$table->modified 		= date('Y-m-d H:i:s');
		$table->modified_by 	= $user->id;
		$table->store();
		if($input->get('password','','STR')):
			$password = JUserHelper::hashPassword($input->get('password','','STR'));
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('User','JclassroomTable',array());
			$table->load($userID);
			$table->password = $password;
			$table->store();
		endif;
		echo $table->id;
		exit();
	}
	function deleteAdmin() {
		$input 		= JFactory::getApplication()->input;
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$table->load($input->get('id','','INT'));
		$userID 	= $table->userID;
		$table->delete($input->get('id', 0, 'INT'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('User','JclassroomTable',array());
		$table->delete($userID);
		$db 		= JFactory::getDbo(); 
		$query 		= $db->getQuery(true);
		$sql		= 'DELETE FROM jcr_user_usergroup_map WHERE user_id = '.$userID.' AND group_id = 13';
		$db->setQuery($sql);
		$db->execute();
		echo 'OK';
		exit();
	}
	function getAdmin() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0,'INT');
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('id', 0, 'INT')));
		$db->setQuery($query);
		$admin = $db->loadObject();
		echo json_encode($admin);
		exit();
	}
	function loadPlanData() {
		$input 		= JFactory::getApplication()->input;
		$planID 	= $input->get('planID', 0,'INT');
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_plans','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($planID));
		$db->setQuery($query);
		$plan = $db->loadObject();
		echo json_encode($plan);
		exit();
	}
	function getSecure() {
		$data 	= array('1_8_9','3_7_10','2_9_11','6_4_10','7_5_12','8_4_12','5_7_12','9_4_13','2_7_9','4_8_12');
		$gData 	= rand(0,9);
		$result = $data[$gData].'_'.$gData;
		echo $result;
		exit();
	}
	function compareSecure() {
		$input 		= JFactory::getApplication()->input;
		$secureID 	= $input->get('secureID', 0,'INT');
		$data 	= array('1_8_9','3_7_10','2_9_11','6_4_10','7_5_12','8_4_12','5_7_12','9_4_13','2_7_9','4_8_12');
		$getRes = $data[$secureID];
		echo $getRes;
		exit();
	}
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'customer';

	protected $view_list = 'customers';

}
?>