<?php
/**
 * @author     
 * @copyright  
 * @license    
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Fragenkatalog item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerStage_classroom extends JControllerForm
{
	function next() {
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$step 	= $input->get('st', 0, 'INT');
		$db 		= JFactory::getDbo();
		$query 		= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		$query->where('a.classroomID = '.$id);
		$query->where('a.published = 1');
		$db->setQuery($query);
		$db->execute();
		$numRows = $db->getNumRows();
		if($step + 1 > $numRows):
			JFactory::getApplication()->enqueueMessage('Die letzte Sektion ist erreicht.', 'Message');
		else:
			$session= JFactory::getSession();
			$session->set('stepKatalog', $step + 1);
		endif;
		$this->setRedirect(JRoute::_(JURI::Root().'stage-classroom?id='.$id.'&dir=forward', false));
	}
	function prev() {
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$step 	= $input->get('st', 0, 'INT');
		
		if($step == 1):
			JFactory::getApplication()->enqueueMessage('Die erste Sektion ist erreicht.', 'Message');
		else:
			$session= JFactory::getSession();
			$session->set('stepKatalog', $step - 1);
		endif;
		$this->setRedirect(JRoute::_(JURI::Root().'stage-classroom?id='.$id.'&dir=back', false));
	}
	function stop() {
		$session= JFactory::getSession();
		$session->set('stepKatalog', 0);
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = 'manager-administrator';;
				break;
			case 'customer':
				$retour = 'manager-customer';
				break;
			case 'trainer':
				$retour = 'manager-trainer';
				break;
			case 'student':
				$retour = 'dashboard-students';
				break;
		}
		$this->setRedirect(JRoute::_(JURI::Root().$retour, false));
	}
	function writeChat() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Chats','JclassroomTable',array());
		$user 	= JFactory::getUser();
		$data = array();
		$data['classroomID'] 		= $input->get('classroomID', 0, 'INT');
		$data['userID'] 			= $user->id;
		$data['content'] 			= $input->get('content', '', 'STR');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$table->bind($data);
		$table->store();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('User','JclassroomTable',array());
		$table->load($user->id);
		$back = '';
		$back .= '<div class="chatContainer">';
		$back .= '<div class="chat fromme">';
		$back .= '<div class="chatinfo">';
        $back .= '<span>'.$table->name.'</span>';
        $back .= '<span>'.date('d.m.Y H.i:s').'</span>';
        $back .= '</div>';
        $back .= $input->get('content', '', 'STR');
        $back .= '</div>';
        $back .= '</div>';
		echo $back;
		exit();
	}
	public function loadChat() {
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$db 		= JFactory::getDbo();
		$query 		= $db->getQuery(true);
		$query->select(array('a.*,b.name'));
        $query->from($db->quoteName('#__jclassroom_chats','a'));
         $query->join('LEFT', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('a.userID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where('a.classroomID = '.$input->get('classroomID', 0, 'INT'));
		$query->order('a.created ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$back = '';
		if($result):
			foreach($result as $item):
				$back .= '<div class="chatContainer">';
				if($user->id == $item->created_by):
					$back .= '<div class="chat fromme">';
				else:
					$back .= '<div class="chat fromother">';
				endif;
				$back .= '<div class="chatinfo">';
		        $back .= '<span>'.$item->name.'</span>';
		        $back .= '<span>'.date('d.m.Y H.i:s', strtotime($item->created)).'</span>';
		        $back .= '</div>';
		        $back .= $item->content;
		        $back .= '</div>';
		        $back .= '</div>';
			endforeach;
		endif;
		echo $back;
		exit();
	}
   	public function printCertificate() {
     	$input = JFactory::getApplication()->input;
      	$id = $input->get('id','','INT');
      	require_once JPATH_SITE. '/components/com_jclassroom/library/certificateLR.php';
      	$printAudit = new printCertificate();
      	$printAudit->certificate($id);
   	}
}
?>