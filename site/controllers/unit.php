<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Fragenkatalog item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerUnit extends JControllerForm
{
	//LOAD SOMETHING
	public function library() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0, 'INT');
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($id);
		$xml['quizz'] = array(
			'customerID'	=> $table->customerID,
			'title' 		=> $table->title, 
			'description' 	=> $table->description,
			'type' 			=> $table->type,
			'showTo' 		=> $table->showTo,
			'showToTrainer' => $table->showToTrainer,
			'sendToMail' 	=> $table->sendMail,
			'bgColor' 		=> $table->bgColor,
			'calculate' 	=> $table->calculate,
			'chart' 		=> $table->chart,
			'bestof_list'  	=> $table->bestof_list
		);
		$db = JFactory::getDbo();
		// Load the groups
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where('a.quizzID = '.$id);
		$query->where('a.type = 14');
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$groups[] 	= array(
					'id' 		=> $item->id,
					'groupID' 	=> $item->groupID,
					'type' 		=> $item->type,
					'title' 		=> $item->title,
					'points' 		=> $item->points,
					'content' 		=> $item->content,
					'infotextPositiv'	=> $item->infotextPositiv,
					'infotextNegativ'	=> $item->infotextNegativ,
					'todo' 			=> $item->todo,
					'testfield' 	=> $item->testfield,
					'theme' 		=> $item->theme,
					'infotext' 		=> $item->infotext,
					'kotext' 		=> $item->kotext,
					'countAnswers' 	=> $item->countAnswers,
					'correctAnswers'=> $item->correctAnswers,
					'chart' 		=> $item->chart,
					'ordering' 		=> $item->ordering,
					'groupOrdering'	=> $item->groupOrdering
				);
			endforeach;
		endif;
		$xml['groups']	= $groups;
		// Load the units
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where('a.quizzID = '.$id);
		$query->where('a.type <> 14');
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$positionsA = array();
				$positionsL = array();
				$positionsC = array();
				if($item->type == 3):
					// Load the answers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					$query->where('a.questionID = '.$item->id);
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$answers = $db->loadObjectList();
					if($answers):
						foreach($answers as $answer):
							$positionsA[] 	= array(
								'questionID' 	=> $answer->questionID,
								'title' 		=> $answer->title,
								'correct' 		=> $answer->correct,
								'ordering' 		=> $answer->ordering
							);
						endforeach;
					endif;
				endif;
				if($item->type == 12):
					// Load the answers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
					$query->where('a.questionID = '.$item->id);
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$lists = $db->loadObjectList();
					if($lists):
						foreach($lists as $list):
							$positionsL[] 	= array(
								'questionID' 	=> $list->questionID,
								'title' 		=> $list->title,
								'correct' 		=> $list->correct,
								'ordering' 		=> $list->ordering
							);
						endforeach;
					endif;
				endif;
				if($item->type == 13):
					// Load the answers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
					$query->where('a.questionID = '.$item->id);
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$checks = $db->loadObjectList();
					if($checks):
						foreach($checks as $check):
							$positionsC[] 	= array(
								'questionID' 	=> $check->questionID,
								'title' 		=> $check->title,
								'correct' 		=> $check->correct,
								'ordering' 		=> $check->ordering
							);
						endforeach;
					endif;
				endif;
				$positions[] 	= array(
					'groupID' 	=> $item->groupID,
					'type' 		=> $item->type,
					'title' 		=> $item->title,
					'points' 		=> $item->points,
					'content' 		=> $item->content,
					'infotextPositiv'	=> $item->infotextPositiv,
					'infotextNegativ'	=> $item->infotextNegativ,
					'todo' 			=> $item->todo,
					'testfield' 	=> $item->testfield,
					'theme' 		=> $item->theme,
					'infotext' 		=> $item->infotext,
					'kotext' 		=> $item->kotext,
					'countAnswers' 	=> $item->countAnswers,
					'correctAnswers'=> $item->correctAnswers,
					'chart' 		=> $item->chart,
					'ordering' 		=> $item->ordering,
					'groupOrdering'	=> $item->groupOrdering,
					'answers' 		=> $positionsA,
					'lists' 		=> $positionsL,
					'checkboxes' 	=> $positionsC
				);
				
			endforeach;
		endif;
		$xml['questions']	= $positions;
		$save = json_encode($xml);
		// Save data to database
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_library','JclassroomTable',array());
		$data 		= array();
		$data['structure'] 		= json_encode($xml);
		$data['title'] 			= $xml['quizz']['title'];
		$data['description'] 	= $xml['quizz']['description'];
		$data['type'] 			= $xml['quizz']['type'];
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by']		= $user->id;
		$data['published'] 		= 1;
		$table->bind($data);
		$table->store();
		$quizzID = $table->id;
		JFactory::getApplication()->enqueueMessage('Das Quizz wurde erfolgreich als Vorlage gespeichert', 'Message');
		$this->setRedirect(JURI::Root().'quizz-edit?layout=edit&id='.$id);
	}
	public function getTemplate() {
		$input 		= JFactory::getApplication()->input;
		$type 		= $input->get('type', 0, 'INT');
		$quizzID 	= $input->get('quizzID', 0, 'INT');
		$sectionID 	= $input->get('sectionID', 0, 'INT');
		$groupID 	= $input->get('groupID', 0, 'INT');
		$isGroup  	= $input->get('isGroup',0,'INT');
		// Get the last ordering for unit without grouping
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->where($db->quoteName('a.type').' <> '.$db->quote(14));
		$query->order('a.ordering DESC');
		//$query->setLimit(1);
		$db->setQuery($query);
		$db->execute();
		$numRows = $db->getNumRows();
		$ordering = $db->loadObject();
		if($numRows == 0):
			$ordering = 0;
		else:
			$ordering = $ordering->ordering + 1;
		endif;
		if($groupID != 0):
			// Get the last ordering for unit with grouping
			$query = $db->getQuery(true);
			$query->select(array('a.ordering'));
		    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
			$query->where($db->quoteName('a.groupID').' = '.$db->quote($groupID));
			$query->order('a.ordering DESC');
			$query->setLimit(1);
			$db->setQuery($query);
			$db->execute();
			$numRows = $db->getNumRows();
			$getOrdering = $db->loadObject();
			$ordering = 0;
			if(!$numRows) {
				$query = $db->getQuery(true);
				$query->select(array('a.ordering'));
			    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.id').' = '.$db->quote($groupID));
				$db->setQuery($query);
				$getOrdering = $db->loadObject();
				$ordering = $getOrdering->ordering + 1;
			} else {
				if($numRows == 0):
					$ordering = 0;
				else:
					$ordering = $getOrdering->ordering + 1;
				endif;
			}
		endif;
		// Get the last ordering for group
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->where($db->quoteName('a.type').' = '.$db->quote(14));
		$query->order('a.ordering DESC');
		//$query->setLimit(1);
		$db->setQuery($query);
		$db->execute();
		$numRowsG = $db->getNumRows();
		$orderingG = $db->loadObject();
		if($numRowsG == 0):
			$orderingG = 0;
		else:
			$orderingG = $ordering->groupOrdering + 1;
		endif;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$data 		= array();
		$data['quizzID']		= $quizzID;
		$data['groupID'] 		= $groupID;
		$data['type']			= $type;
		$data['content']		= $content;
		$data['title']			= $title;
		$data['points']			= $points;
		if($isGroup == 1):
			$data['ordering']		= 0;
			$data['groupOrdering'] 	= $orderingG;
		else:
			$data['ordering']		= $ordering;
			$data['groupOrdering'] 	= 0;
		endif;
		$data['published'] 		= 1;
		$table->bind($data);
		$table->store();
		JLoader::register('SectionsHelper',JPATH_COMPONENT_SITE.'/helpers/sections.php');
        $template = new SectionsHelper();
        $html = $template->getTemplate($sectionID, $type, '', $table->id,'',0);
        //$this->orderPlus($quizzID,$ordering,$table->id);
        $this->reorderQuestions($quizzID,$ordering,$table->id);
        echo $html;
        exit();
	}
	function newGroup() {
		$input 		= JFactory::getApplication()->input;
		$type 		= $input->get('type', 0, 'INT');
		$quizzID 	= $input->get('quizzID', 0, 'INT');
		$sectionID 	= $input->get('sectionID', 0, 'INT');
		$isGroup  	= $input->get('isGroup',0,'INT');
		// Get the last ordering
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->order('a.ordering DESC');
		//$query->setLimit(1);
		$db->setQuery($query);
		$db->execute();
		$numRows = $db->getNumRows();
		$ordering = $db->loadObject();
		if($numRows == 0):
			$ordering = 0;
		else:
			$ordering = $ordering->ordering + 1;
		endif;
		// Get the last ordering for group
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->where($db->quoteName('a.type').' = '.$db->quote(14));
		$query->order('a.ordering DESC');
		//$query->setLimit(1);
		$db->setQuery($query);
		$db->execute();
		$numRowsG = $db->getNumRows();
		$orderingG = $db->loadObject();
		if($numRowsG == 0):
			$orderingG = 0;
		else:
			$orderingG = $ordering->groupOrdering + 1;
		endif;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$data 		= array();
		$data['quizzID']		= $quizzID;
		$data['type']			= 14;
		$data['content']		= '';
		$data['title']			= $title;
		$data['points']			= 0;
		if($isGroup == 1):
			$data['ordering']		= $ordering;
			$data['groupOrdering'] 	= $orderingG;
		else:
			$data['ordering']		= $ordering;
			$data['groupOrdering'] 	= 0;
		endif;
		$data['published'] 		= 1;
		$table->bind($data);
		$table->store();
		$quizzID 	= $table->id;
		JLoader::register('SectionsHelper',JPATH_COMPONENT_SITE.'/helpers/sections.php');
        $template = new SectionsHelper();
        $html = $template->getTemplate($sectionID, 14, '', $quizzID,'',0);
        
        echo $html;
        exit();
	}
	//COPY SOMETHING
	public function copyQuestion() {
		$input 		= JFactory::getApplication()->input;
		$quizzID 	= $input->get('quizzID', 0, 'INT');
		$questionID = $input->get('questionID', 0, 'INT');
		// Get the last ordering
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->order('a.ordering DESC');
		//$query->setLimit(1);
		$db->setQuery($query);
		$db->execute();
		$numRows = $db->getNumRows();
		$ordering = $db->loadObject();
		if($numRows == 0):
			$ordering = 0;
		else:
			$ordering = $ordering->ordering + 1;
		endif;
		// GET THE ORIGINAL QUESTION
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$original 	= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$original->load($questionID);
		// SAVE THE COPY
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$data 		= array();
		$data['quizzID']		= $original->quizzID;
		$data['type']			= $original->type;
		$data['content']		= $original->content;
		$data['title']			= $original->title;
		$data['points']			= $original->points;
		$data['ordering']		= $ordering;
		$data['published'] 		= 1;
		$table->bind($data);
		$table->store();
		$newQuestionID 	= $table->id;
		if($original->type == 13):
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
		    $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
			$db->setQuery($query);
			$nf = $db->loadObjectList();
			if($nf):
				foreach($nf as $n):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Quizz_checkboxes','JclassroomTable',array());
					$data 		= array();
					$data['questionID']		= $newQuestionID;
					$data['title']			= $n->title;
					$data['correct']		= $n->correct;
					$data['ordering']		= $n->ordering;
					$table->bind($data);
					$table->store();
				endforeach;
			endif;
		endif;
		JLoader::register('SectionsHelper',JPATH_COMPONENT_SITE.'/helpers/sections.php');
        $template = new SectionsHelper();
        $html = $template->getTemplate(0, $original->type, '', $newQuestionID, 0, 0);
        
        echo $html;
        exit();
	}
	public function copyQuizz() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		// Get the origin of the quizz
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($id);
		// Save the copy of the quizz
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableN 		= JTable::getInstance('Unit','JclassroomTable',array());
		$data = array();
		$data['customerID'] 		= $table->customerID;
		$data['title'] 				= $table->title;
		$data['description'] 		= $table->description;
		$data['type'] 				= $table->type;
		$data['showTo'] 			= $table->showTo;
		$data['showToTrainer'] 		= $table->showToTrainer;
		$data['sendToMail'] 		= $table->sendToMail;
		$data['bgColor'] 			= $table->bgColor;
		$data['calculate'] 			= $table->calculate;
		$data['chart'] 				= $table->chart;
		$data['bestof_list'] 		= $table->bestof_list;
		$data['published'] 	= 1;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['showType'] 	= $table->showType;
		$data['readRights'] 		= $table->readRights;
		$data['writeRights'] 		= $table->writeRights;
		$data['showto'] 		= $table->showto;
		$data['editto'] 		= $table->editto;
		$data['chooseableto'] 	= $table->chooseableto;
		$tableN->bind($data);
		$tableN->store();
		$newUnitID 	= $tableN->id;

		$db = JFactory::getDbo();
		// Load the groups
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where('a.quizzID = '.$id);
		$query->where('a.type = 14');
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableN 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$data = array();
				$data['quizzID'] 	= $newUnitID;
				$data['groupID'] 	= $item->groupID;
				$data['type'] 		= $item->type;
				$data['title'] 		= $item->title;
				$data['points'] 	= $item->points;
				$data['content'] 	= $item->content;
				$data['infotextPositiv'] 	= $item->infotextPositiv;
				$data['infotextNegativ'] 	= $item->infotextNegativ;
				$data['todo'] 		= $item->todo;
				$data['testfield'] 	= $item->testfield;
				$data['theme'] 		= $item->theme;
				$data['infotext'] 	= $item->infotext;
				$data['kotext'] 	= $item->kotext;
				$data['countAnswers'] 		= $item->countAnswers;
				$data['correctAnswers'] 	= $item->correctAnswers;
				$data['chart'] 		= $item->chart;
				$data['ordering'] 		= $item->ordering;
				$data['groupOrdering'] 	= $item->groupOrdering;
				$tableN->bind($data);
				$tableN->store();
				$newGPosID 	= $tableN->id;
			endforeach;
		endif;
		// Load the positions != group
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where('a.quizzID = '.$id);
		$query->where('a.type <> 14');
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableN 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$data = array();
				$data['quizzID'] 	= $newUnitID;
				$data['groupID'] 	= $item->groupID;
				$data['type'] 		= $item->type;
				$data['title'] 		= $item->title;
				$data['points'] 	= $item->points;
				$data['content'] 	= $item->content;
				$data['infotextPositiv'] 	= $item->infotextPositiv;
				$data['infotextNegativ'] 	= $item->infotextNegativ;
				$data['todo'] 		= $item->todo;
				$data['testfield'] 	= $item->testfield;
				$data['theme'] 		= $item->theme;
				$data['infotext'] 	= $item->infotext;
				$data['kotext'] 	= $item->kotext;
				$data['countAnswers'] 		= $item->countAnswers;
				$data['correctAnswers'] 	= $item->correctAnswers;
				$data['chart'] 		= $item->chart;
				$data['ordering'] 		= $item->ordering;
				$data['groupOrdering'] 	= $item->groupOrdering;
				$tableN->bind($data);
				$tableN->store();
				$newPosID 	= $tableN->id;
				if($item->type == 3 || $item->type == 7):
					// Load the answers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					$query->where('a.questionID = '.$item->id);
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$answers = $db->loadObjectList();
					if($answers):
						foreach($answers as $answer):
							JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
							$tableN 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
							$data = array();
							$data['questionID'] 	= $newPosID;
							$data['title'] 			= $answer->title;
							$data['correct'] 		= $answer->correct;
							$data['ordering'] 		= $answer->ordering;
							$tableN->bind($data);
							$tableN->store();
						endforeach;
					endif;
				endif;
				if($item->type == 12):
					// Load the answers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
					$query->where('a.questionID = '.$item->id);
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$lists = $db->loadObjectList();
					if($lists):
						foreach($lists as $list):
							JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
							$tableN 		= JTable::getInstance('Quizz_options','JclassroomTable',array());
							$data = array();
							$data['questionID'] 	= $newPosID;
							$data['title'] 			= $list->title;
							$data['correct'] 		= $list->correct;
							$data['ordering'] 		= $list->ordering;
							$tableN->bind($data);
							$tableN->store();
						endforeach;
					endif;
				endif;
				if($item->type == 13):
					// Load the answers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
					$query->where('a.questionID = '.$item->id);
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$checks = $db->loadObjectList();
					if($checks):
						foreach($checks as $check):
							JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
							$tableN = JTable::getInstance('Quizz_checkboxes','JclassroomTable',array());
							$data 	= array();
							$data['questionID'] 	= $newPosID;
							$data['title'] 			= $check->title;
							$data['correct'] 		= $check->correct;
							$data['ordering'] 		= $check->ordering;
							$tableN->bind($data);
							$tableN->store();
						endforeach;
					endif;
				endif;	
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Das Quizz wurde erfolgreich kopiert. Die neue QuizzID ist '.$newUnitID, 'Message');
		$this->setRedirect(JURI::Root().'quizze');
	}
	// DELETE SOMETHING
	public function deleteTheQuestion() {
		$input 		= JFactory::getApplication()->input;
		$questionID = $input->get('questionID', 0, 'INT');
		$quizzID  	= $input->get('quizzID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$table->delete($questionID);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.id'));
	    $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$nf = $db->loadObjectList();
		if($nf):
			foreach($nf as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
				$table->delete($item->id);
			endforeach;
		endif;
		$query = $db->getQuery(true);
		$query->select(array('a.id'));
	    $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$nf = $db->loadObjectList();
		if($nf):
			foreach($nf as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_checkboxes','JclassroomTable',array());
				$table->delete($item->id);
			endforeach;
		endif;
		//Reorder the questions in quizzpositions
		$query = $db->getQuery(true);
		$query->select(array('a.id'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$positions = $db->loadObjectList();
		if($positions):
			$ordering = 0;
			foreach($positions as $position):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$table->load($position->id);
				$table->ordering = $ordering;
				$table->store();
				$ordering++;
			endforeach;
		endif;
        echo 'OK';
        exit();
	}
	function moveQuestionToGroup() {
		$input 		= JFactory::getApplication()->input;
		$questionToMove = $input->get('questionToMove', 0, 'INT');
		$groupID 		= $input->get('groupID', 0, 'INT');
		// Load the ordering of last position in group element
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.ordering'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.groupID').' = '.$db->quote($groupID));
		$query->order('a.ordering DESC');
		$query->setLimit(1);
		$db->setQuery($query);
		$lastOrder = $db->loadResult();
		if(!$lastOrder) {
			$query = $db->getQuery(true);
			$query->select(array('a.ordering'));
		    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.id').' = '.$db->quote($groupID));
			$query->order('a.ordering DESC');
			$query->setLimit(1);
			$db->setQuery($query);
			$lastOrder = $db->loadResult();
		}
		$lastOrder = $lastOrder + 1;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$table->load($questionToMove);
		$quizzID 			= $table->quizzID;
		$table->groupID 	= $groupID;
		$table->ordering 	= $lastOrder;
		$table->store();
		echo $lastOrder.' '.$table->id;
		//$this->orderPlus($quizzID,$lastOrder,$table->id);
		$this->reorderQuestions($quizzID,$lastOrder,$table->id);
		$this->justOrder($quizzID);
		echo 'OK';
		exit();
	}
	function moveQuestionToMainfield() {
		$input 		= JFactory::getApplication()->input;
		$questionToMove = $input->get('questionToMove', 0, 'INT');
		// Load the ordering of group element
		/*JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$table->load($groupID);
		$lastOrder = $table->ordering + 1;*/
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$table->load($questionToMove);
		$quizzID 			= $table->quizzID;
		$table->groupID 	= 0;
		$table->ordering 	= $lastOrder;
		$table->store();
		//$this->orderPlus($quizzID,$lastOrder,$table->id);
		$this->reorderQuestions($quizzID);
		$this->justOrder($quizzID);
		echo 'OK';
		exit();
	}
	public function deleteAnswer() {
		$input 		= JFactory::getApplication()->input;
		$answerID = $input->get('answerID', 0, 'INT');

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
		$table->delete($answerID);
        echo 'OK';
        exit();
	}
	public function deleteOption() {
		$input 		= JFactory::getApplication()->input;
		$optionID = $input->get('optionID', 0, 'INT');

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_options','JclassroomTable',array());
		$table->delete($optionID);
        echo 'OK';
        exit();
	}
	public function deleteCheckbox() {
		$input 		= JFactory::getApplication()->input;
		$checkboxID = $input->get('checkboxID', 0, 'INT');

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_checkboxes','JclassroomTable',array());
		$table->delete($checkboxID);
        echo 'OK';
        exit();
	}
	public function saveQuestion() {
		$input 		= JFactory::getApplication()->input;
		$questionID = $input->get('questionID', 0, 'INT');
		$title 		= $input->get('title', 0, 'STR');
		$points 	= $input->get('points', 0, 'INT');
		$textT7 	= $input->get('textT7', 0, 'RAW');
		$chart 		= $input->get('calculates', '', 'STR');
		$content 	= $input->get('content', 0, 'RAW');
		$infotextPositiv 	= $input->get('infotextPositiv', 0, 'STR');
		$infotextNegativ 	= $input->get('infotextNegativ', 0, 'STR');
		$todo 		= $input->get('todo', 0, 'STR');
		$testfield 	= $input->get('testfield', 0, 'STR');
		$theme 		= $input->get('theme', 0, 'STR');
		$infotext 	= $input->get('infotext', 0, 'STR');
		$kotext 	= $input->get('kotext', 0, 'STR');
		$answers 	= $input->get('answers', 0, 'ARRAY');
		$options 	= $input->get('options', 0, 'ARRAY');
		$checkboxes = $input->get('checkboxes', 0, 'ARRAY');
		$sectionCorrects = $input->get('sectionCorrects', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$table->load($questionID);
		$table->title 	= $title;
		$table->points 	= $points;
		$table->chart 	= $chart;
		$table->content = htmlentities($content);
		$table->infotextPositiv = $infotextPositiv;
		$table->infotextNegativ = $infotextNegativ;
		$table->todo 	= $todo;
		$table->testfield 	= $testfield;
		$table->theme 	= $theme;
		$table->infotext= $infotext;
		$table->kotext= $kotext;
		if($input->get('sectionCorrects')):
			$table->correctAnswers = $input->get('sectionCorrects', 0, 'INT');
		else:
			$table->correctAnswers = $input->get('correctAnswers', 0, 'INT');
		endif;
		$table->store();
        if($answers):
        	foreach($answers as $answer):
        		$splitAnswer = explode('_', $answer);
        		if($splitAnswer[0] == 3):
	        		$answerID = str_replace('answerTyp3row','', $splitAnswer[1]);
	        		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
					$table->load($answerID);
					$table->title = $splitAnswer[2];
					$table->correct = $splitAnswer[3];
					$table->store();
				endif;
				if($splitAnswer[0] == 5):
	        		$answerID = str_replace('answerTyp3row','', $splitAnswer[1]);
	        		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
					$load = array('questionID' => $questionID, 'correct' => 1);
					$check = $table->load($load);
					if(!$check):
						$data 		= array();
						$data['questionID']		= $questionID;
						$data['title']			= $splitAnswer[1];
						$data['correct']		= 1;
						$data['ordering']		= 0;
						$table->bind($data);
						$table->store();
					else:
						$table->title 			= $splitAnswer[1];
						$table->store();
					endif;
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
					$load = array('questionID' => $questionID, 'correct' => 0);
					$check = $table->load($load);
					if(!$check):
						$data 		= array();
						$data['questionID']		= $questionID;
						$data['title']			= $splitAnswer[2];
						$data['correct']		= 0;
						$data['ordering']		= 0;
						$table->bind($data);
						$table->store();
					else:
						$table->title 			= $splitAnswer[2];
						$table->store();
					endif;
				endif;
				if($splitAnswer[0] == 3):
	        		$answerID = str_replace('answerTyp3row','', $splitAnswer[1]);
	        		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
					$table->load($answerID);
					$table->title = $splitAnswer[2];
					$table->correct = $splitAnswer[3];
					$table->store();
				endif;
			endforeach;
		endif;
		if($options):
        	foreach($options as $option):
        		$splitAnswer = explode('_', $option);
				if($splitAnswer[0] == 12):
					echo $splitAnswer[1].' '.$splitAnswer[2];
	        		$optionID = $splitAnswer[1];
	        		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Quizz_options','JclassroomTable',array());
					$table->load($optionID);
					$table->title 	= $splitAnswer[2];
					$table->correct = $splitAnswer[3];
					$table->store();
				endif;
        	endforeach;
        endif;
        if($checkboxes):
        	foreach($checkboxes as $checkbox):
        		$splitAnswer = explode('*/*', $checkbox);
				if($splitAnswer[0] == 13):
	        		$checkboxID = $splitAnswer[1];
	        		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Quizz_checkboxes','JclassroomTable',array());
					$table->load($checkboxID);
					$table->title = htmlentities($splitAnswer[2]);
					$table->correct = $splitAnswer[3];
					$table->store();
				endif;
        	endforeach;
        endif;
        if($textT7):
    		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
			$load = array('questionID' => $questionID);
			$check = $table->load($load);
			if(!$check):
				$data 		= array();
				$data['questionID']		= $questionID;
				$data['title']			= $textT7;
				$data['correct']		= 0;
				$data['ordering']		= 0;
				$table->bind($data);
				$table->store();
			else:
				$table->title 			= $textT7;
				$table->store();
			endif;
        endif;
        echo 'OK';
        exit();
	}
	public function loadAnswerTyp3() {
		$input 		= JFactory::getApplication()->input;
		$questionID = $input->get('questionID', 0, 'INT');

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
		$data 		= array();
		$data['questionID']		= $questionID;
		$data['title']			= $title;
		/*$data['content']		= $content;
		$data['title']			= $title;
		$data['points']			= $points;
		$data['ordering']		= $ordering;
		$data['published'] 		= 1;*/
		$table->bind($data);
		$table->store();
		$answerID 	= $table->id;
		JLoader::register('SectionsHelper',JPATH_COMPONENT_SITE.'/helpers/sections.php');
        $template = new SectionsHelper();
        $html = $template->loadAnswer($questionID,$answerID);
        
        echo $html;
        exit();
	}
	public function loadOption() {
		$input 		= JFactory::getApplication()->input;
		$questionID = $input->get('questionID', 0, 'INT');

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_options','JclassroomTable',array());
		$data 		= array();
		$data['questionID']		= $questionID;
		$data['title']			= $title;
		/*$data['content']		= $content;
		$data['title']			= $title;
		$data['points']			= $points;
		$data['ordering']		= $ordering;
		$data['published'] 		= 1;*/
		$table->bind($data);
		$table->store();
		$optionID 	= $table->id;
		JLoader::register('SectionsHelper',JPATH_COMPONENT_SITE.'/helpers/sections.php');
        $template = new SectionsHelper();
        $html = $template->loadOption($optionID, $questionID);
        
        echo $html;
        exit();
	}
	public function loadCheckbox() {
		$input 		= JFactory::getApplication()->input;
		$questionID = $input->get('questionID', 0, 'INT');

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_checkboxes','JclassroomTable',array());
		$data 		= array();
		$data['questionID']		= $questionID;
		$data['title']			= $title;
		$table->bind($data);
		$table->store();
		$checkboxID 	= $table->id;
		JLoader::register('SectionsHelper',JPATH_COMPONENT_SITE.'/helpers/sections.php');
        $template = new SectionsHelper();
        $html = $template->loadCheckbox($checkboxID,$questionID);
        
        echo $html;
        exit();
	}
	// Scripts for sorting something
	function writeSortable() {
		$input = JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		// The id of the dragged element
		$id 		= $input->get('id', 0, 'INT');
		// Check if the dragged element is a group
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$table->load($id);
		$quizzID = $table->quizzID;
		if($orders):
			$ordering 		= 0;
			$groupOrdering  = 0;
			foreach($orders as $order):
				$unitID 	= explode('_', $order);
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$table->load($unitID[1]);
				if($table->type == 14):
					$table->ordering 		= $ordering;
					$table->groupOrdering 	= $groupOrdering;
					$table->store();
					$ordering++;
					$groupOrdering++;
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query->select(array('a.id'));
				    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
					$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
					$query->where($db->quoteName('a.groupID').' = '.$db->quote($unitID[1]));
					$query->order('a.ordering ASC');
					$db->setQuery($query);
					$elements = $db->loadObjectList();
					if($elements):
						foreach($elements as $element):
							JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
							$tableE 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
							$tableE->load($element->id);
							$tableE->ordering = $ordering;
							$tableE->store();
							$ordering++;
						endforeach;
					endif;
				else:
					$table->ordering 	= $ordering;
					$table->store();
					$ordering++;
				endif;
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function writeSortableGroup() {
		$input = JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		$groupID 	= $input->get('groupID', 0, 'INT');
		$quizzID 	= $input->get('quizzID', 0, 'INT');
		// Load the ordering of group element
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$table->load($groupID);
		$table->ordering;
		$lastOrder 	= $table->ordering + 1;
		echo $lastOrder;
		if($orders):
			foreach($orders as $order):
				$unitID 	= explode('_', $order);
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$table->load($unitID[1]);
				$table->ordering 	= $lastOrder;
				$table->store();
				$lastOrder++;
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function reorderQuestions($quizzID, $startFrom = false, $except = false) {
		$i = 0;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.id'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		if($except && $startFrom):
			$query->where($db->quoteName('a.ordering').' >= '.$db->quote($startFrom));
			$query->where($db->quoteName('a.id').' <> '.$db->quote($except));
			$i = $startFrom + 1;
		endif;
		$query->order('a.ordering ASC');
		$db->setQuery($query);
		$orderings = $db->loadObjectList();
		print_r($ordering);
		foreach($orderings as $ordering):
			echo $i.'<br/>';
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
			$table->load($ordering->id);
			$table->ordering 	= $i;
			$table->store();
			$i++;
		endforeach;
	}
	function justOrder($quizzID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.id'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->order('a.ordering ASC');
		$db->setQuery($query);
		$orderings = $db->loadObjectList();
		$order = 0;
		foreach($orderings as $ordering):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
			$table->load($ordering->id);
			$table->ordering 	= $order;
			$table->store();
			$order++;
		endforeach;
	}
	function orderPlus($quizzID,$orderFrom,$currentElement) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.id'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->where($db->quoteName('a.ordering').' >= '.$db->quote($orderFrom));
		$query->where($db->quoteName('a.id').' <> '.$db->quote($currentElement));
		$query->order('a.ordering ASC');
		$db->setQuery($query);
		$orderings = $db->loadObjectList();
		$orderFrom++;
		foreach($orderings as $ordering):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
			$table->load($ordering->id);
			$table->ordering 	= $orderFrom;
			$table->store();
			$orderFrom++;
		endforeach;
	}
	function setRights() {
		$input 		= JFactory::getApplication()->input;
		$quizzID 	= $input->get('quizzID', 0, 'INT');
		$userID 	= $input->get('userID', 0, 'INT');
		$type 		= $input->get('type', '', 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($quizzID);
		if($type == 'read'):
			$users = $table->readRights;
			if(!$users):
				$users 		= $userID;
			else:
				$users 		= explode(',', $table->readRights);
				$users[] 	= $userID;
				$users 		= implode(',',$users);
			endif;
			$table->readRights = $users;
		endif;
		if($type == 'write'):
			$users = $table->writeRights;
			if(!$users):
				$users 		= $userID;
			else:
				$users 		= explode(',', $table->writeRights);
				$users[] 	= $userID;
				$users 		= implode(',',$users);
			endif;
			$table->writeRights = $users;
		endif;
		$table->store();
		echo 'OK';
		exit();
	}
	function setNoRights() {
		$input 		= JFactory::getApplication()->input;
		$quizzID 	= $input->get('quizzID', 0, 'INT');
		$userID 	= $input->get('userID', 0, 'INT');
		$type 		= $input->get('type', '', 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($quizzID);
		if($type == 'read'):
			$users 		= explode(',',$table->readRights);
			$i = 0;
			foreach($users as $user):
				if($user == $userID):
					unset($users[$i]);
				endif;
				$i++;
			endforeach;
			/*$users 		= str_replace($userID,'', $users);
			$users 		= str_replace(',,',',', $users);
			$set1 		= substr($users,0,1);
			if($set1 == ','):
				$users = substr($users,1);
			endif;
			$setL = strlen($users);
			$set2 = substr($users,$setL -1, 1);
			if($set2 == ','):
				$users = substr($users, 0, $setL -1);
			endif;*/
			$users = implode(',', $users);
			$table->readRights = $users;
		endif;
		if($type == 'write'):
			$users 		= $table->writeRights;
			$users 		= str_replace($userID,'', $users);
			$users 		= str_replace(',,',',', $users);
			$set1 		= substr($users,0,1);
			if($set1 == ','):
				$users = substr($users,1);
			endif;
			$setL = strlen($users);
			$set2 = substr($users,$setL -1, 1);
			if($set2 == ','):
				$users = substr($users, 0, $setL -1);
			endif;
			$table->writeRights = $users;
		endif;
		$table->store();
		echo 'OK';
		exit();
	}
}
?>