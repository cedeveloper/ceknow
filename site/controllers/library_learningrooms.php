<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

class JclassroomControllerLibrary_learningrooms extends JControllerAdmin
{
	function createLearningroom() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$originID 	= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableO 		= JTable::getInstance('Classroom_library','JclassroomTable',array());
		$tableO->load($originID);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom','JclassroomTable',array());
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['companyID'] 			= '';
		$data['title'] 				= $tableO->title.' (Dublicate from Library LR'.$originID.')';
		$data['description'] 		= $tableO->description;
		$data['main_trainer'] 		= $tableO->main_trainer;
		$data['co_trainer'] 		= $tableO->co_trainer;
		$data['showTo'] 			= $tableO->showTo;
		$data['showToUser'] 		= $tableO->showToUser;
		$data['email_template'] 		= $tableO->email_template;
		$data['verification_template'] 	= $tableO->verification_template;
		$data['remember_template'] 		= $tableO->remember_template;
		$data['invitations'] 			= $tableO->invitations;
		$data['countRememberMails'] 	= $tableO->countRememberMails;
		$data['offsetRememberMails'] 	= $tableO->offsetRememberMails;
		$data['invitationsBy'] 			= $tableO->invitationsBy;
		$data['certificate'] 			= $tableO->certificate;
		$data['student_feedback'] 		= $tableO->student_feedback;
		$data['student_feedback_anonym'] 		= $tableO->student_feedback_anonym;
		$data['trainer_feedback'] 		= $tableO->trainer_feedback;
		$data['published'] 	= 1;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$table->bind($data);
		$table->store();
		$newClassroomID 	= $table->id;
		//Create imagefolders for new LR
		$pathFL 	= JPATH_SITE.'/images/learningrooms/LR'.$newClassroomID;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');

		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_library','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($originID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		if($days):
			foreach($days as $day):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_days','JclassroomTable',array());
				$data = array();
				$data['classroomID'] 	= $newClassroomID;
				$data['day'] 			= $day->day;
				$data['title'] 			= $day->title;
				$data['dayID'] 			= $day->dayID;
				$data['ordering'] 		= $day->ordering;
				$data['published'] 		= 1;
				$table->bind($data);
				$table->store();
				$newDayID = $table->id;
				$this->copyModules($day->id, $newDayID, $newClassroomID, $session->get('customerID'), $user);
			endforeach;
		endif;
		echo $newClassroomID;
		exit();
	}
	function copyModules($oldDayID, $newDayID, $newLearningRoomID, $customerID, $user) {
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
        $query->where($db->quotename('a.dayID').' = '.$db->quote($oldDayID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$modules = $db->loadObjectList();
		if($modules):
			foreach($modules as $module):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_modules','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $customerID;
				$data['classroomID']= $newLearningRoomID;
				$data['dayID'] 		= $newDayID;
				$data['title'] 		= $module->title;
				$data['description'] = $module->description;
				$data['ordering'] 	= $module->ordering;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $module->published;
				$tableD->bind($data);
				$tableD->store();
				$this->copyUnits($module->id, $tableD->id, $newLearningRoomID, $customerID, $user);
			endforeach;
		endif;
	}
	function copyUnits($oldModuleID, $newModuleID, $newLearningRoomID, $customerID, $user) {
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
        $query->where($db->quotename('a.moduleID').' = '.$db->quote($oldModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $customerID;
				$data['classroomID']= $newLearningRoomID;
				$data['dayID'] 		= 0;
				$data['moduleID'] 	= $newModuleID;
				$data['unitID'] 	= $unit->unitID;
				$data['unitType'] 	= $unit->unitType;
				$data['ordering'] 	= $unit->ordering;
				$data['duration'] 	= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $unit->published;
				$tableD->bind($data);
				$tableD->store();
				$newUnitID = $tableD->id;
				// Copy files
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$pathLibraryToMove = '/images/learningrooms/LR'.$newLearningRoomID.'/units/';
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableF = JTable::getInstance('File','JclassroomTable',array());
						$data = array();
						$data['type'] 		= $file->type;
						$data['classroomID']= $newLearningRoomID;
						$data['unitID'] 	= $newUnitID;
						$data['studentID'] 	= 0;
						$data['folder'] 	= '';
						$data['filename'] 	= $file->filename;
						$data['path'] 		= $pathLibraryToMove.$file->filename;
						$data['created'] 	= date('Y-m-d H:i:s');
						$data['created_by'] = $user->id;
						$data['published'] 	= $file->published;
						$tableF->bind($data);
						$tableF->store();
						copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.$file->filename);
					endforeach;
				endif;
			endforeach;
		endif;
	}
	function removeTemplate() {
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom_library','JclassroomTable',array());
		$table->delete($id);

		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.id');
        $query->from($db->quoteName('#__jclassroom_classroom_days_library','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($id));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		if($days):
			foreach($days as $day):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD 	= JTable::getInstance('Classroom_days_library','JclassroomTable',array());
				$tableD->delete($day->id);

				$query 	= $db->getQuery(true);
				$query->select('a.id');
		        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
		        $query->where($db->quotename('a.classroomID').' = '.$db->quote($id));
		        $query->order('a.ordering ASC');
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				if($modules):
					foreach($modules as $module):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableM 	= JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
						$tableM->delete($module->id);

						$query 	= $db->getQuery(true);
						$query->select('a.id');
				        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
				        $query->where($db->quotename('a.moduleID').' = '.$db->quote($module->id));
				        $query->order('a.ordering ASC');
						$db->setQuery($query);
						$units = $db->loadObjectList();
						if($units):
							foreach($units as $unit):
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$tableU 	= JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
								$tableU->delete($unit->id);
							endforeach;
						endif;
					endforeach;
				endif;
			endforeach;
		endif;
		JFolder::delete(JPATH_ROOT.'/images/library/learningrooms/LR'.$id);
		echo 'OK';
		exit();
	}
	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'students';
	
	/**
	 * Get the admin model and set it to default
	 *
	 * @param   string           $name    Name of the model.
	 * @param   string           $prefix  Prefix of the model.
	 * @param   array			 $config  The model configuration.
	 */
	public function getModel($name = 'Student', $prefix='JclassroomModel', $config = array())
	{
		$config['ignore_request'] = true;
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>