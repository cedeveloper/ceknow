<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Fragenkatalog item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerPublish_quizz extends JControllerForm
{
	function save() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Quizz','JclassroomTable',array());
		if($input->get('id',0,'INT')):
			$table->load($input->get('id', 0,'INT'));
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$tableU 	= JTable::getInstance('Unit','JclassroomTable',array());
			$tableU->load($formData->getInt('quizzID'));
			if(!$formData->getStr('title')):
				$title = $tableU->title;
			else:
				$title = $formData->getStr('title');
			endif;
			$table->title 			= $title;
			$table->description 	= $formData->getRaw('description');
			$table->internalDescription 	= $formData->getRaw('internalDescription');
			$table->quizzID 		= $formData->getInt('quizzID');
			$table->quizzType 		= $formData->getInt('quizzType');
			$table->companyID 		= $formData->getInt('companyID');
			$table->classroomID 	= $formData->getInt('classroomID');
			if($formData->getInt('openQuizz') == 1):
				$from 	= $formData->getStr('publish_up').' '.$formData->getStr('publish_up_time');
				$to 	= $formData->getStr('publish_down').' '.$formData->getStr('publish_down_time');
				$table->publish_up 		= date('Y-m-d H:i:s', strtotime($from));
				$table->publish_down 	= date('Y-m-d H:i:s', strtotime($to));
			endif;
			$table->openQuizz 		= $formData->getInt('openQuizz');
			$table->modified 		= date('Y-m-d H:i:s');
			$table->modified_by		= $user->id;
			$table->link 			= 'stage?sT=quizz&qID='.$table->id.'&q=-1';
			$table->published 		= $formData->getInt('published');
			$table->store();
			$quizzID 				= $input->get('id', 0, 'INT');
		else:
			$data = array();
			$data['customerID'] 		= $session->get('customerID');
			$data['companyID'] 			= $formData->getInt('companyID');
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$tableU 	= JTable::getInstance('Unit','JclassroomTable',array());
			$tableU->load($formData->getInt('quizzID'));
			if(!$formData->getStr('title')):
				$title = $tableU->title;
			else:
				$title = $formData->getStr('title');
			endif;
			$data['title'] 				= $title;
			$data['description'] 		= $formData->getRaw('description');
			$data['internalDescription'] 	= $formData->getRaw('internalDescription');
			$data['quizzID'] 			= $formData->getInt('quizzID');
			$data['classroomID'] 		= $formData->getInt('classroomID');
			$data['quizzType'] 			= $formData->getInt('quizzType');
			$data['co_trainer'] 		= $formData->getInt('co_trainer');
			$data['openQuizz'] 			= $formData->getInt('openQuizz');
			if($formData->getInt('openQuizz') == 1):
				$from 	= $formData->getStr('publish_up').' '.$formData->getStr('publish_up_time');
				$to 	= $formData->getStr('publish_down').' '.$formData->getStr('publish_down_time');
				$data['publish_up'] 		= date('Y-m-d H:i:s', strtotime($from));
				$data['publish_down'] 		= date('Y-m-d H:i:s', strtotime($to));
			endif;
			$data['published'] 	= $formData->getInt('published');
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$table->bind($data);
			$table->store();
			$quizzID = $table->id;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Quizz','JclassroomTable',array());
			$table->load($quizzID);
			$table->link		= 'stage?sT=quizz&qID='.$quizzID.'&q=-1';
			$table->store();
		endif;
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$this->setRedirect(JRoute::_(JURI::Root().'publish-quizz?layout=edit&id='.$quizzID, false));
	}
}
?>