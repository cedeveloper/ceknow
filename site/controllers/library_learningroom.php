<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerLibrary_learningroom extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'library-learningrooms';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id    = $input->get('id','','INT');
		$uid   = $input->get('uid','','INT');
		$clr   = $input->get('clr','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'library-learningroom-edit?&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$user 	= JFactory::getUser();
		$session= JFactory::getSession();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$post 	= $input->get('jform', array(), 'array');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom_library','JclassroomTable',array());
		$user 	= JFactory::getUser();
		$data = array();
		$data['customerID'] 		= $post['customerID'];
		$data['userID'] 			= $post['userID'];
		$data['classroomID'] 		= $post['classroomID'];
		$data['deliveryDate'] 		= $post['deliveryDate'];
		$data['salutation'] 		= $post['salutation'];
		$data['first_name'] 		= $post['first_name'];
		$data['last_name'] 			= $post['last_name'];
		$data['adress'] 			= $post['adress'];
		$data['postcode'] 			= $post['postcode'];
		$data['city'] 				= $post['city'];
		$data['phone'] 				= $post['phone'];
		$data['mobile'] 			= $post['mobile'];
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= $post['published'];
		$table->bind($data);
		$table->store();
		$tblOrderID 		= $table->id;
		return $tblOrderID;
	}
	
	public function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['title_image'];
		if($file && $file['name'] != '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_library','JclassroomTable',array());
		$table->load($formData->getInt('id'));
		$table->title 				= $formData->getStr('title');
		$table->title_description 	= $formData->getRaw('title_description');
		if($file && $file['name'] != ''):
			$table->title_image 		= $filename;
		endif;
		$table->freeLR 	= $formData->getInt('freeLR');
		$table->price 	= $formData->getDec('price');
		$table->modified	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		
		return $tblResellerID;
	}
}
?>