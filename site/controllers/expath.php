<?php
/**
 * @author     
 * @copyright  
 * @license    
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Fragenkatalog item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerExpath extends JControllerForm
{

   public function nextQuestion() {
      $user       = JFactory::getUser();
      $session    = JFactory::getSession();
      $cart   		= $session->get('quizzes');
      $input      = JFactory::getApplication()->input;
      $quizzID    = $input->get('quizzID', 0, 'INT');
      $publishedQuizzID    = $input->get('publishedQuizzID', 0, 'INT');
      $questionID = $input->get('questionID', 0, 'INT');
      $q          = $input->get('q', 0, 'INT');
      $preview    = $input->get('preview', 0, 'INT');
      $stageType  = $input->get('stageType', 0, 'STR');
      $type       = $input->get('type', 0, 'INT');
      $classroomID= $input->get('clr', 0, 'INT');
      $answers    = $input->get('answers', 0, 'ARARY');
      $indAnswers = $input->get('indAnswers', 0, 'ARARY');
      $options    = $input->get('options', 0, 'ARARY');
      $smiley     = $input->get('smiley', '', 'STR');
      $checkboxes = $input->get('checkboxes', 0, 'ARARY');
      $exeptionText     = $input->get('exeptionText', '', 'STR');
      $exeptionPoints   = $input->get('exeptionPoints', 0, 'INT');
      $session->set('quizzID', $quizzID);
      // After the intro? Load the first question
      if($q == -1):
         if($stageType == 'quizz'):
            if($preview == 1):
               echo 'https://www.ceknow.de/expath.php?sT=quizz&qID='.$quizzID.'&q='.($q + 1).'&preview=1';
            else:
               echo 'stage?sT=quizz&qID='.$publishedQuizzID.'&q='.($q + 1);
            endif;
         else:
            echo 'stage?sT=lr&unitID='.$quizzID.'&clr='.$classroomID.'&q='.($q + 1);
         endif;
         exit();
      endif;
      if($answers):
      	$content = $answers;
      endif;
      if($indAnswers):
      	$content = $indAnswers;
      endif;
      if($answers):
      	$content = $answers;
      endif;
      if($options):
      	$content = $options;
      endif;
      if($checkboxes):
      	$content = $checkboxes;
      endif;
      if($smiley):
      	$content = $smiley;
      endif;
      //Get Answers correct
      $key = 0;
      if($cart):
         foreach($cart as $item):
            if($item['questionID'] == $questionID):
               unset($cart[$key]);
               $cart = array_values($cart);
            endif;
            $key++;
         endforeach;
      endif;
      $cart[]  = array(
         'classroomID'        => $classroomID,
         'userID'             => $user->id, 
         'quizzID'            => $quizzID, 
         'publishedQuizzID'   => $publishedQuizzID,
         'questionID'         => $questionID,
         'type' 	            => $type,
         'content'            => $content,
         'exeptionText'       => $exeptionText,
         'exeptionPoints'     => $exeptionPoints
      );
      // Check, if the result of the question should be shown directly after the answer
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quizz   = JTable::getInstance('Quizz','JclassroomTable',array());
      $quizz->load($publishedQuizzID);
      $showResultAfter = $quizz->showResultAfter;
      // if show the result after answer, get the answers
      if($showResultAfter == 1):
         // if there is just one answer
         if($answers):
            foreach($answers as $answer):
               $splitAnswer = explode('_', $answer);
               JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
               $ans   = JTable::getInstance('Quizz_answers','JclassroomTable',array());
               $ans->load($splitAnswer[0]);
               JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
               $correct  = JTable::getInstance('Quizz_answers','JclassroomTable',array());
               $load = array('questionID' => $questionID, 'correct' => 1);
               $correct->load($load);
               if($ans->correct == $correct->correct):
                  JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
                  $info  = JTable::getInstance('Quizz_positions','JclassroomTable',array());
                  $info->load($questionID);
                  if($info->infotextPositiv):
                     $setInfoPositiv = $info->infotextPositiv;
                  else:
                     $setInfoPositiv = '';
                  endif;
                  $html = '<span class="badge bg-success text-white" style="font-size: 16px;">Ihre Antwort <i>'.$correct->title.'</i> war richtig!</span>'.$setInfoPositiv;
               else:
                  JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
                  $info  = JTable::getInstance('Quizz_positions','JclassroomTable',array());
                  $info->load($questionID);
                  if($info->infotextNegativ):
                     $setInfoNegativ = $info->infotextNegativ;
                  else:
                     $setInfoNegativ = '';
                  endif;
                  $html = '<span class="badge bg-danger text-white" style="font-size: 16px;">Leider falsch. Die richtige Antwort wäre <i>'.$correct->title.'</i> gewesen.</span>'.$setInfoNegativ;
               endif;
            endforeach;
         endif;
         // if there is more the one answer
      else:   

      endif;
      $session->set('quizzes', $cart);
      if($stageType == 'quizz'):
         if($showResultAfter == 1):
            echo 'SRA_'.$theResultID.'_'.$html;
         endif;
         if($showResultAfter == 0):
             // check if current question is the last in quizz
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('count(a.id)'));
            $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
            $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
            $db->setQuery($query);
            $theAnswersCorrect   = $db->loadResult();
            if($preview == 1):
               // Is the last qestion, return to result
               if($theAnswersCorrect == ($q + 1)):
                  echo 'https://www.ceknow.de/expath.php?q=0&lid=10&qID='.$quizzID.'&preview=1';
               else:
                  echo 'https://www.ceknow.de/expath.php?sT=quizz&qID='.$quizzID.'&q='.($q + 1).'&preview=1';
               endif;
            else:
               // Is the last qestion, return to result
               if($theAnswersCorrect == ($q + 1)):
                  $theResultID = $this->saveTheResult($classroomID, $quizzID, $publishedQuizzID);
                  echo 'https://www.ceknow.de/expath.php?rID='.$theResultID.'&lid=10&qid='.$publishedQuizzID;
               else:
                  echo 'https://www.ceknow.de/expath.php?qID='.$publishedQuizzID.'&q='.($q + 1);
               endif;
            endif;
         endif;
      else:
         // check if current question is the last in quizz
         $db   = JFactory::getDbo();
         $query   = $db->getQuery(true);
         $query->select(array('count(a.id)'));
         $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
         $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
         $db->setQuery($query);
         $theAnswersCorrect   = $db->loadResult();
         // Is the last qestion, return to result
         if($theAnswersCorrect == ($q + 1)):
            $theResultID = $this->saveTheResult($classroomID, $quizzID, 0);
            echo 'https://www.ceknow.de/expath.php?rID='.$theResultID.'&q=-1&lid=10';
         else:
            echo 'https://www.ceknow.de/expath.php?sT=lr&unitID='.$quizzID.'&clr='.$classroomID.'&q='.($q + 1);
         endif;
      endif;
      exit();
   }
   function moveNextQuestion() {
      $input      = JFactory::getApplication()->input;
      $q          = $input->get('q', 0, 'INT');
      $classroomID   = $input->get('clr', 0, 'INT');
      $publishedQuizzID    = $input->get('publishedQuizzID', 0, 'INT');
      $quizzID    = $input->get('quizzID', 0, 'INT');
      $theResultID= $input->get('theResultID', 0, 'INT');
      // check if current question is the last in quizz
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('count(a.id)'));
      $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
      $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
      $db->setQuery($query);
      $theAnswersCorrect   = $db->loadResult();
      // Is the last qestion, return to result
      if($theAnswersCorrect == ($q + 1)):
         $theResultID = $this->saveTheResult($classroomID, $quizzID, $publishedQuizzID);
         echo 'stage?layout=edit&rID='.$theResultID.'&q=-1&lid=10&pqid='.$publishedQuizzID;
      else:
         echo 'stage?sT=quizz&qID='.$publishedQuizzID.'&q='.($q + 1);
      endif;
      exit();
   }
   function movePrevQuestion() {
      $input      = JFactory::getApplication()->input;
      $q          = $input->get('q', 0, 'INT');
      $publishedQuizzID    = $input->get('publishedQuizzID', 0, 'INT');
      $quizzID    = $input->get('quizzID', 0, 'INT');
      $theResultID= $input->get('theResultID', 0, 'INT');
      $stageType  = $input->get('stageType', '', 'STR');
      $clr        = $input->get('clr', 0, 'INT');
      $preview    = $input->get('preview', 0, 'INT');
      // check if current question is the last in quizz
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('count(a.id)'));
      $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
      $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
      $db->setQuery($query);
      $theAnswersCorrect   = $db->loadResult();
      // Is the last qestion, return to result
      if($theAnswersCorrect == ($q - 1)):
         echo 'stage?layout=edit&rID='.$theResultID.'&q=-1&lid=10&pqid='.$publishedQuizzID;
      else:
         if($stageType == 'quizz'):
            if($preview == 1):
               echo 'stage?sT='.$input->get('stageType','','STR').'&qID='.$quizzID.'&q='.($q - 1).'&preview=1';
            else:
               echo 'stage?sT='.$input->get('stageType','','STR').'&qID='.$publishedQuizzID.'&q='.($q - 1);
            endif;
         endif;
         if($stageType == 'lr'):
            echo 'stage?sT='.$input->get('stageType','','STR').'&unitID='.$quizzID.'&clr='.$clr.'&q='.($q - 1);
         endif;
      endif;
      exit();
   }
   function restartQuizz() {
      $session    = JFactory::getSession();
      $cart       = $session->get('quizzes');
      $stageType  = $session->get('quizztype');
      $session->clear('quizzes');
      $session->clear('quizztype');
      $input      = JFactory::getApplication()->input;
      $quizzID    = $input->get('quizzID', 0, 'INT');
      $publishedQuizzID    = $input->get('publishedQuizzID', 0, 'INT');
      $preview    = $input->get('preview', 0, 'INT');
      if($preview == 1):
         $preview = '&preview=1';
      else:
         $preview = '';
      endif;
      if($stageType == 'quizz'):
         if($publishedQuizzID):
            echo 'stage?sT=quizz&qID='.$publishedQuizzID.'&q=-1';
         else:
            echo 'stage?sT=quizz&qID='.$quizzID.'&q=-1'.$preview;
         endif;
      else:
         echo 'stage?layout=edit&sT=lr&unitID='.$quizzID.'&clr='.$classroomID.'&q=-1';
      endif;
      exit();
   }
   function quitQuizz() {
      $session    = JFactory::getSession();
      $stageType  = $session->get('quizztype');
      $classroomID= $session->get('classroomID');
      if($stageType == 'lr'):
         echo JURI::Root().'stage-classroom?layout=edit&id='.$classroomID;
      endif; 
      if($stageType == 'quizz'):
         echo JURI::Root();
      endif; 
      $session->clear('quizztype');
      $session->clear('classroomID');
      exit();
   }
   function saveTheResult($classroomID, $quizzID, $publishedQuizzID) {
      $user    = JFactory::getUser();
      $session = JFactory::getSession();
      $cart    = $session->get('quizzes');
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $table   = JTable::getInstance('Theresults','JclassroomTable',array());
      $data                = array();
      $data['classroomID'] = $classroomID;
      $data['quizzID']     = $quizzID;
      $data['publishedQuizzID'] = $publishedQuizzID;
      $data['created']     = date('Y-m-d H:i:d');
      $data['created_by']  = $user->id;
      $table->bind($data);
      $table->store();
      $theResultID = $table->id;
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $table   = JTable::getInstance('Quizz_results','JclassroomTable',array());
      $load    = array('theResultID' => $theResultID);
      $check   = $table->load($load);
      foreach($cart as $item):
         if($item['content']):
            if($item['type'] == 4):
               $contentF = explode('_', $item['content']);
               JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
               $table   = JTable::getInstance('Quizz_results','JclassroomTable',array());
               $data                = array();
               $data['theResultID'] = $theResultID;
               $data['questionID']  = $item['questionID'];
               $data['quizzID']     = $item['quizzID'];
               $data['publishedQuizzID'] = $publishedQuizzID;
               $data['type']        = $item['type'];
               $data['content']     = $contentF[1];
               $data['answerID']    = $contentF[0];
               $data['exeptionText']   = $item['exeptionText'];
               $data['exeptionPoints'] = $item['exeptionPoints'];
               $data['created']     = date('Y-m-d H:i:d');
               $data['created_by']  = $user->id;
               $table->bind($data);
               $table->store();
            else:
               foreach($item['content'] as $conten):
                  $contentF = explode('_', $conten);
                  JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
                  $table   = JTable::getInstance('Quizz_results','JclassroomTable',array());
                  $data                = array();
                  $data['theResultID'] = $theResultID;
                  $data['questionID']  = $item['questionID'];
                  $data['quizzID']     = $item['quizzID'];
                  $data['publishedQuizzID'] = $publishedQuizzID;
                  $data['type']        = $item['type'];
                  if($item['type'] == 6):
                     $data['content']     = $contentF[0];
                     $data['answerID']    = 0;
                  else:
                     $data['content']     = $contentF[1];
                     $data['answerID']    = $contentF[0];
                  endif;
                  $data['exeptionText']   = $item['exeptionText'];
                  $data['exeptionPoints'] = $item['exeptionPoints'];
                  $data['created']     = date('Y-m-d H:i:d');
                  $data['created_by']  = $user->id;
                  $table->bind($data);
                  $table->store();
               endforeach;
            endif;
         endif;
      endforeach;
      //$session->clear('quizzes');
      // IF RESULT SHOULD BE SEND TO ANYONE
      // Load the Quizz
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quizz   = JTable::getInstance('Unit','JclassroomTable',array());
      $quizz->load($quizzID);
      if($quizz->sendToMail):
         $mailer = JFactory::getMailer();
         $sender = array('training@celearning.de','ceLearning');
         $mailer->setSender($sender);
         $mailer->addRecipient($quizz->sendToMail);
         $body    = json_encode($cart);
         $mailer->setSubject('Auswertung Quizz '. $quizz->titel .' für Teilnehmer '.$user->name);
         $mailer->isHtml(true);
         $mailer->setBody($body);
         $send = $mailer->Send();
      endif;
      return $theResultID; 
   }
   function getAnswerText() {
      $input         = JFactory::getApplication()->input;
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $answer   = JTable::getInstance('Quizz_answers','JclassroomTable',array());
      $answer->load($input->get('id',0,'INT'));
      echo $answer->title;
      exit();
   }
   function loadDay() {
      $input         = JFactory::getApplication()->input;
      $dayID         = $input->get('dayID','','STR');
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $day   = JTable::getInstance('Classroom_days','JclassroomTable',array());
      $day->load($dayID);
      if($day->title):
         $title   = $day->title;
      else:
         $title   = date('l, d.m.Y', strtotime($day->day));
      endif;
      $html = '';
      $html .= '<div class="dayrow row mb-1">';
      $html .= '<div class="col-12">';
      $html .= '<div class="day card bg-light" style="height: 100%;">';
      $html .= '<div class="card-header">';
      $html .= '<h3>'.$title.'</h3>';
      $html .= '</div>';
      $html .= '<div class="card-body">';
      $modules = $this->getModules($input->get('classroomID',0,'INT'), $input->get('dayID', 0, 'INT'));
      if($modules):
         foreach($modules as $module):
            $html .= '<div id="module'.$module->id.'" class="module mb-5">';
               $html .= '<div onclick="openModule('.$module->id.');" class="moduleHeader d-flex" style="justify-content: space-between;">';
                  $html .= '<h4 >'.$module->title.'<span style="font-size: 12px;"> (Modul)</span></h4>
                     <i title="Modul öffnen/schließen" style="font-size: 24px; padding-top: 5px;cursor: pointer;" class="fa fa-chevron-right"></i>';
               $html .= '</div>';
            $html .= '<div id="moduleContent'.$module->id.'" class="moduleContent hide">';
            $units = $this->getUnits($input->get('classroomID',0,'INT'), $module->id);
            if($units):
               foreach($units as $unit):
                  $files = $this->getFiles($input->get('classroomID',0,'INT'), $unit->id);
                  switch($unit->unitType):
                     case 1:
                        $html.= '<div id="unit'.$unit->id.'" class="card p-1 mt-1" >';
                           $html.= '<div class="card-header d-inline-block" style="background-color: palegreen;">';
                           $html .= '<div onclick="openUnit('.$unit->id.');" class="unitHeader d-flex" style="justify-content:space-between;">';
                              $html.= '<h4 class="stageUnitHeader">'.$unit->title.'<i style="font-size:12px;"> (Freitext)</i></h4>';
                              $html.= '<div class="duration">';
                              if($unit->duration != 0):
                                 $html .= '<i class="fa fa-clock-o"></i> '.$unit->duration .' Minuten';
                              endif;
                              $html .= '<i style="font-size: 18px; margin-left: 10px;cursor: pointer;" class="fa fa-chevron-right"></i>';
                              $html .= '</div>';
                           $html.= '</div>';
                           $html.= '</div>';
                           $html.= '<div class="card-body hide d-inline-block">';
                              $html.= $unit->content;
                              $html.= '<div class="mt-3">';
                              foreach($files as $file):
                                 if($file->unitID == $unit->id):
                                    $html.= '<p class="mt-1 mb-1" style="width:300px;">
                                    <a href="'.JURI::Root().$file->path.'" target="_blank" style="width:100%;" class="btn btn-primary">'.$file->filename.'</a>
                                    </p>';
                                 endif;
                              endforeach;
                              $html.= '</div>';
                           $html .= '</div>';
                        $html.= '</div>';
                        break;
                     case 2:
                        $html.= '<div id="unit'.$unit->id.'" class="card p-1 mt-1">';
                        $html.= '<div class="card-header d-inline-block w-100" style="background-color: #00c9ff;">';
                        $html .= '<div onclick="openUnit('.$unit->id.');" class="unitHeader d-flex" style="justify-content: space-between;">';
                        $html.= '<h4 class="stageUnitHeader">'.$unit->title.'<span style="font-size: 12px;"> (Virtual Classroom)</span></h4>';
                        $html.= '<div class="duration">';
                        if($unit->duration != 0):
                           $html .= '<i class="fa fa-clock-o"></i> '.$unit->duration .' Minuten';
                        endif;
                        $html .= '<i style="font-size: 18px; margin-left: 10px;cursor: pointer;" class="fa fa-chevron-right"></i>';
                        $html .= '</div>';
                        $html.= '</div>';
                        $html.= '<a target="_blank" class="btn btn-primary btn-sm" href="'.$unit->link.'">Zum virtual classroom</a>';
                        $html.= '</div>';
                        $html.= '<div class="card-body hide d-inline-block">';
                        $html.= $unit->content;
                        if($unit->link):
                        $html.= '<br/>Teilnehmen unter diesem Link: <br/>
                        <br/><a target="_blank" class="btn btn-primary btn-sm" href="'.$unit->link.'">Zum virtual classroom</a><br/>';
                        endif;
                        $html.= '<div class="mt-3">';
                        foreach($files as $file):
                           if($file->unitID == $unit->id):
                              $html.= '<p class="mt-1 mb-1" style="width:300px;">
                              <a href="'.JURI::Root().$file->path.'" target="_blank" style="width:100%;" class="btn btn-primary">'.$file->filename.'</a>
                              </p>';
                           endif;
                        endforeach;
                        $html.= '</div>';
                        $html.= '</div>';
                        $html.= '</div>';
                        break;
                     case 3:
                        $html.= '<div id="unit'.$unit->id.'" class="card p-1 mt-1" style="background-color: silver;">';
                        $html.= '<div onclick="openUnit('.$unit->id.');" class="unitHeader card-body d-inline-block">';
                        $html.= '<h4 class="stageUnitHeader"><b>PAUSE</b></h4>';
                        if($unit->duration != 0):
                        $html.= '<div class="duration"><i class="fa fa-clock-o"></i> '.$unit->duration.' Minuten</div>';
                        endif;
                        $html.= $unit->content;
                        $html.= '</div>';
                        $html.= '</div>';
                        break;
                     case 4:
                        $html.= '<div id="unit'.$unit->id.'" class="card p-1 mt-1">';
                        $html.= '<div class="card-header d-inline-block" style="background-color: darkorange;">';
                        $html .= '<div onclick="openUnit('.$unit->id.');" class="unitHeader d-flex" style="justify-content:space-between;">';
                        $html.= '<h4 class="stageUnitHeader">'.$unit->title.'<span style="font-size: 12px;"> (Quizz)</span></h4>';
                        $html.= '<div class="duration">';
                        if($unit->duration != 0):
                           $html .= '<i class="fa fa-clock-o"></i> '.$unit->duration .' Minuten';
                        endif;
                        $html .= '<i style="font-size: 18px; margin-left: 10px;cursor: pointer;" class="fa fa-chevron-right"></i>';
                        $html .= '</div>';
                        $html.= '</div>';
                        $html .= '<a class="btn btn-primary text-white btn-sm" target="_blank" href="stage?layout=edit&sT=lr&unitID='.$unit->quizzID.'&clr='.$input->get('classroomID',0,'INT').'&q=-1">Zum Quizz</a>';
                        $html.= '</div>';
                        $html.= '<div class="card-body hide d-inline-block">';
                        $html.= $unit->content;
                        $html.= '<br/>Quizz im neuen Fenster öffnen: <br/><a class="btn btn-primary text-white btn-sm" target="_blank" href="stage?layout=edit&sT=lr&unitID='.$unit->quizzID.'&clr='.$input->get('classroomID',0,'INT').'&q=-1">Zum Quizz</a><br/>';
                        //$html.= '<a id="stageWebinar" class="d-inline-block text-white btn btn-primary btn-sm">Freigeben</a>';
                        $html.= '</div>';
                        $html.= '</div>';
                        break;
                     case 5:
                        $html.= '<div id="unit'.$unit->id.'" class="card p-1 mt-1">';
                           $html.= '<div class="card-header d-inline-block"  style="background-color: #ff3600;">';
                              $html .= '<div onclick="openUnit('.$unit->id.');" class="unitHeader d-flex" style="justify-content:space-between;">';
                              $html.= '<h4 class="stageUnitHeader">'.$unit->title.'<span style="font-size: 12px;"> (Einzelübung)</span></h4>';
                              $html.= '<div class="duration">';
                              if($unit->duration != 0):
                                 $html .= '<i class="fa fa-clock-o"></i> '.$unit->duration .' Minuten';
                              endif;
                              $html .= '<i style="font-size: 18px; margin-left: 10px;cursor: pointer;" class="fa fa-chevron-right"></i>';
                              $html .= '</div>';
                              $html.= '</div>';
                           $html.= '</div>';
                           $html.= '<div class="card-body hide d-inline-block">';
                           $html.= $unit->content;
                           $html.= '<div class="mt-3">';
                           if($files):
                              $html .='<h6>Dateien</h6>';
                              foreach($files as $file):
                                 if($file->unitID == $unit->id):
                                    $html.= '<p class="mt-1 mb-1" style="width:300px;">
                                    <a href="'.JURI::Root().$file->path.'" target="_blank" style="width:100%;" class="btn btn-primary">'.$file->filename.'</a>
                                    </p>';
                                 endif;
                              endforeach;
                           endif;
                           $html.= '</div>';
                           $html.= '</div>';
                        $html.= '</div>';
                        break;
                     case 6:
                        $html.= '<div id="unit'.$unit->id.'" class="card p-1 mt-1">';
                        $html.= '<div class="card-header d-inline-block" style="background-color: mediumseagreen;">';
                        $html .= '<div onclick="openUnit('.$unit->id.');" class="unitHeader d-flex" style="justify-content:space-between;">';
                        $html.= '<h4 class="stageUnitHeader">'.$unit->title.'<span style="font-size:12px;"> (Gruppenübung)</span></h4>';
                        $html.= '<div class="duration">';
                        if($unit->duration != 0):
                           $html .= '<i class="fa fa-clock-o"></i> '.$unit->duration .' Minuten';
                        endif;
                        $html .= '<i style="font-size: 18px; margin-left: 10px;cursor: pointer;" class="fa fa-chevron-right"></i>';
                        $html .= '</div>';
                        $html.= '</div>';
                        $html.= '</div>';
                        $html.= '<div class="card-body hide d-inline-block">';
                        $html.= $unit->content;
                        $html.= '<div class="mt-3">';
                              foreach($files as $file):
                                 if($file->unitID == $unit->id):
                                    $html.= '<p class="mt-1 mb-1" style="width:300px;">
                                    <a href="'.JURI::Root().$file->path.'" target="_blank" style="width:100%;" class="btn btn-primary">'.$file->filename.'</a>
                                    </p>';
                                 endif;
                              endforeach;
                              $html.= '</div>';
                           $html.= '</div>';
                        $html.= '</div>';
                        break;
                     case 7:
                        $html.= '<div id="unit'.$unit->id.'" class="card p-1 mt-1">';
                        $html.= '<div class="card-header d-inline-block" style="background-color: lightsalmon;">';
                        $html .= '<div onclick="openUnit('.$unit->id.');" class="unitHeader d-flex" style="justify-content:space-between;">';
                        $html.= '<h4 class="stageUnitHeader">'.$unit->title.'<span style="font-size: 12px;"> (Video)</span></h4>';
                        $html.= '<div class="duration">';
                        if($unit->duration != 0):
                           $html .= '<i class="fa fa-clock-o"></i> '.$unit->duration .' Minuten';
                        endif;
                        $html .= '<i style="font-size: 18px; margin-left: 10px;cursor: pointer;" class="fa fa-chevron-right"></i>';
                        $html .= '</div>';
                        $html.= '</div>';
                        $html .= '<a class="btn btn-primary btn-sm text-white" target="_blank" href="'.$unit->link.'">Video öffnen</a>';
                        $html.= '</div>';
                        $html.= '<div class="card-body hide d-inline-block">';
                        $html.= $unit->content;
                        $html.= '<br/><a class="btn btn-primary btn-sm text-white" target="_blank" href="'.$unit->link.'">Video öffnen</a><br/>';
                        $html.= '</div>';
                        $html.= '</div>';
                        break;
                     case 8:
                        $html.= '<div id="unit'.$unit->id.'" class="card p-1 mt-1">';
                           $html.= '<div class="card-header d-inline-block" style="background-color: crimson;">';
                           $html .= '<div onclick="openUnit('.$unit->id.');" class="unitHeader d-flex" style="justify-content:space-between;">';
                              $html.= '<h4 class="stageUnitHeader"><b><i>Feedback: </i></b>'.$unit->title.'</h4>';
                              $html.= '<div class="duration">';
                              if($unit->duration != 0):
                                 $html .= '<i class="fa fa-clock-o"></i> '.$unit->duration .' Minuten';
                              endif;
                              $html .= '<i style="font-size: 18px; margin-left: 10px;cursor: pointer;" class="fa fa-chevron-right"></i>';
                              $html .= '</div>';
                           $html.= '</div>';
                           $html.= '</div>';
                           $html.= '<div class="card-body hide d-inline-block">';
                              $html.= $unit->content;
                              $html.= '<div class="mt-3">';
                              foreach($files as $file):
                                 if($file->unitID == $unit->id):
                                    $html.= '<p class="mt-1 mb-1" style="width:300px;">
                                    <a href="'.JURI::Root().$file->path.'" target="_blank" style="width:100%;" class="btn btn-primary">'.$file->filename.'</a>
                                    </p>';
                                 endif;
                              endforeach;
                              $html.= '</div>';
                           $html.= '</div>';
                        $html.= '</div>';
                        break;
                     case 9:
                        $html.= '<div id="unit'.$unit->id.'" class="card p-1 mt-1" >';
                           $html.= '<div class="card-header d-inline-block" style="background-color: PaleTurquoise;">';
                           $html .= '<div onclick="openUnit('.$unit->id.');" class="unitHeader d-flex" style="justify-content:space-between;">';
                              $html.= '<h4 class="stageUnitHeader">'.$unit->title.'<i style="font-size:12px;"> (Aufgabe)</i></h4>';
                              $html.= '<div class="duration">';
                              if($unit->duration != 0):
                                 $html .= '<i class="fa fa-clock-o"></i> '.$unit->duration .' Minuten';
                              endif;
                              $html .= '<i style="font-size: 18px; margin-left: 10px;cursor: pointer;" class="fa fa-chevron-right"></i>';
                              $html .= '</div>';
                           $html.= '</div>';
                           $html.= '</div>';
                           $html.= '<div class="card-body hide d-inline-block">';
                              $html.= $unit->content;
                              $html.= '<div class="mt-3">';
                              foreach($files as $file):
                                 if($file->unitID == $unit->id):
                                    $html.= '<p class="mt-1 mb-1" style="width:300px;">
                                    <a href="'.JURI::Root().$file->path.'" target="_blank" style="width:100%;" class="btn btn-primary">'.$file->filename.'</a>
                                    </p>';
                                 endif;
                              endforeach;
                              $html.= '</div>';
                           $html .= '</div>';
                        $html.= '</div>';
                        break;
                     endswitch;
               endforeach;
            endif;
            $html .= '</div>';
            $html .= '</div>';
         endforeach;
      endif;
      $html .= '</div>';
      $html .= '</div>';
      $html .= '</div>';
      $html .= '</div>';
      echo $html;
      exit();
   }
   function getModules($classroomID, $dayID) {
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
      $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
      $query->where($db->quoteName('a.classroomID').' = '.$db->quote($classroomID));
      $query->where($db->quoteName('a.dayID').' = '.$db->quote($dayID));
      $query->where($db->quoteName('a.published').' = 1');
      $query->order('a.ordering ASC');
      $db->setQuery($query);
      $modules   = $db->loadObjectList();
      return $modules;
   }
   function getUnits($classroomID, $moduleID) {
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
      $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
      $query->where($db->quoteName('a.classroomID').' = '.$db->quote($classroomID));
      $query->where($db->quoteName('a.moduleID').' = '.$db->quote($moduleID));
      $query->where($db->quoteName('a.published').' = 1');
      $query->order('a.ordering ASC');
      $db->setQuery($query);
      $units   = $db->loadObjectList();
      return $units;
   }
   function getFiles($classroomID, $unitID) {
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
      $query->from($db->quoteName('#__jclassroom_files','a'));
      $query->where($db->quoteName('a.classroomID').' = '.$db->quote($classroomID));
      $query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
      $query->order('a.id ASC');
      $db->setQuery($query);
      $files   = $db->loadObjectList();
      return $files;
   }
   //IMPORT DER KAPITEL
   function importKapitel($fkaID) {
      jimport('joomla.filesystem.folder');
      jimport('joomla.filesystem.file');
      $input         = JFactory::getApplication()->input;
      $formData      = new JInput($input->get('jform', '', 'array'));
      $files         = $input->files->get('jform');
      $file          = $files['kapitel'];
      if($files && $file['name'] != '') {
         $filename      = JFile::makeSafe($file['name']);
         $src        = $file['tmp_name'];
         $dest          = JPATH_ROOT . "/images/auditum/kapitel/" . $filename;
         JFile::upload($src, $dest);
         $count = $this->parseKapitelEXE($fkaID,$dest,";","\"");
         //$import = $this->importCSVContent($fkaID);
         $app = JFactory::getApplication();
      }
      //$app->redirect('index.php?option=com_auditum&view=auditkatalog&layout=edit&id='.$auditkatalogID,'Der Import wurde erfolgreich gespeichert. '.$count.' Header gefunden. '.$import.' Fragen importiert.','message');
   }
   public function printCertificate() {
      $input = JFactory::getApplication()->input;
      $id = $input->get('rID','','INT');
      require_once JPATH_SITE. '/components/com_jclassroom/library/certificate.php';
      $printAudit = new printCertificate();
      $printAudit->certificate($id);
   }
}
?>