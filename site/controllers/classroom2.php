<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerClassroom2 extends JControllerForm
{
	public function add() {

		$retour = JURI::Root().'classroom-edit?layout=basicdata';
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveBasic($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editBasicToDatabase($id);
		} else {
			$id = $this->saveBasicToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSaveBasic($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editBasicToDatabase($id);
		} else {
			$id = $this->saveBasicToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'classroom-edit?layout=basicdata&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'classroom-edit?layout=basicdata&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'classroom-edit?layout=basicdata&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveBasicToDatabase() {
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$user 	= JFactory::getUser();
		
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['title'] 				= $formData->getStr('title');
		$data['description'] 		= $formData->getRaw('description');
		$data['main_trainer'] 		= $formData->getInt('main_trainer');
		$data['co_trainer'] 		= $formData->getInt('co_trainer');
		$data['presentation'] 		= $formData->getInt('presentation');
		$data['hardware_order'] 	= $formData->getInt('hardware_order');
		$data['showChat'] 			= $formData->getInt('showChat');
		$data['companyID'] 			= $formData->getInt('companyID');
		$data['published'] 	= 1;
		$data['created'] 	= date('Y-m-d H:i:s');
		$createdBy = $user->id;
		if($formData->getInt('created_by')):
			$createdBy = $formData->getInt('created_by');
		endif;
		$data['created_by']	= $createdBy;
		$data['showto']		= 3;
		$data['editto']		= 3;
		$data['choosableto']	= 1;
		$data['visibleTo'] 		= $formData->getInt('visibleTo');
		$data['testmode'] 		= $formData->getInt('testmode');
		$data['email_testmode'] = $formData->getStr('email_testmode');
		$table->bind($data);
		$table->store();
		$newClassroomID 	= $table->id;
		// Create the Folder for learningroom
		$pathFL 	= JPATH_SITE.'/images/learningrooms/LR'.$newClassroomID;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');
		
		return $newClassroomID;
	}
	public function editBasicToDatabase($id) {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		$table->title				= $formData->getStr('title');
		$table->description 		= $formData->getRaw('description');
		$table->presentation 		= $formData->getInt('presentation');
		$table->visibleTo 		= $formData->getInt('visibleTo');
		$table->main_trainer 	= $formData->getInt('main_trainer');
		$table->co_trainer 		= $formData->getInt('co_trainer');
		$table->companyID 		= $formData->getInt('companyID');
		$table->published 	= $formData->getStr('published');
		$table->created_by	= $formData->getInt('created_by');
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->testmode 	= $formData->getInt('testmode');
		$table->email_testmode 	= $formData->getStr('email_testmode');
		$table->store();
	}
	// Saving the version
	public function saveVersion($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editVersionToDatabase($id);
		} else {
			$id = $this->saveVersionToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSaveVersion($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editVersionToDatabase($id);
		} else {
			$id = $this->saveVersionToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'classroom-edit?layout=version&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'classroom-edit?layout=version&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'classroom-edit?layout=version&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveVersionToDatabase() {
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$user 	= JFactory::getUser();
		
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['title'] 				= $formData->getStr('title');
		$data['description'] 		= $formData->getRaw('description');
		$data['main_trainer'] 		= $formData->getInt('main_trainer');
		$data['co_trainer'] 		= $formData->getInt('co_trainer');
		$data['presentation'] 		= $formData->getInt('presentation');
		$data['hardware_order'] 	= $formData->getInt('hardware_order');
		$data['showChat'] 			= $formData->getInt('showChat');
		$data['companyID'] 			= $formData->getInt('companyID');
		$data['published'] 	= 1;
		$data['created'] 	= date('Y-m-d H:i:s');
		$createdBy = $user->id;
		if($formData->getInt('created_by')):
			$createdBy = $formData->getInt('created_by');
		endif;
		$data['created_by']	= $createdBy;
		$data['showto']		= 3;
		$data['editto']		= 3;
		$data['choosableto']	= 1;
		$data['visibleTo'] 		= $formData->getInt('visibleTo');
		$data['testmode'] 		= $formData->getInt('testmode');
		$data['email_testmode'] = $formData->getStr('email_testmode');
		$table->bind($data);
		$table->store();
		$newClassroomID 	= $table->id;
		// Create the Folder for learningroom
		$pathFL 	= JPATH_SITE.'/images/learningrooms/LR'.$newClassroomID;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');
		
		return $newClassroomID;
	}
	public function editVersionToDatabase($id) {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		$table->version			= $formData->getStr('version');
		$table->changelog 		= $formData->getRaw('changelog');
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
	}
	// Saving the communications
	public function saveCommunication($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editCommunicationToDatabase($id);
		} else {
			//$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSaveCommunication($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editCommunicationToDatabase($id);
		} else {
			//$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'classroom-edit?layout=communication&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'classroom-edit?layout=communication&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'classroom-edit?layout=communication&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveCommunicationToDatabase() {
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$formData 	= new JInput($input->get('jform', '', 'array'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$datum 	= JFactory::getDate();
		$date 	= new JDate($datum);
		$datum 	= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 	= JFactory::getUser();
		
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['title'] 				= $formData->getStr('title');
		$data['description'] 		= $formData->getRaw('description');
		$data['main_trainer'] 		= $formData->getInt('main_trainer');
		$data['co_trainer'] 		= $formData->getInt('co_trainer');
		$data['presentation'] 		= $formData->getInt('presentation');
		$data['hardware_order'] 	= $formData->getInt('hardware_order');
		$data['showChat'] 			= $formData->getInt('showChat');
		$data['companyID'] 			= $formData->getInt('companyID');
		$fromDate 	= new Datetime($formData->getStr('fromDate'));
		$toDate 	= new DateTime($formData->getStr('toDate'));
		$data['fromDate'] 		= $fromDate->format('Y-m-d');
		$data['toDate'] 		= $toDate->format('Y-m-d');
		$data['inventa_kursID'] 	= $formData->getInt('inventa_kursID');
		$data['inventa_terminID'] 	= $formData->getInt('inventa_terminID');
		$data['showTo'] 		= $formData->getInt('showTo');
		$data['showToUser'] 	= json_encode($formData->getStr('showToUser'));
		$data['visibleTo'] 		= $formData->getInt('visibleTo');
		$data['email_template'] = $formData->getInt('email_template');
		$data['verification_template'] = $formData->getInt('verification_template');
		$data['remember_template'] = $formData->getInt('remember_template');
		$data['invitations'] 		= $formData->getInt('invitations');
		$data['offsetRememberMails']= $formData->getInt('offsetRememberMails');
		$data['invitationsBy'] 		= $formData->getStr('invitationsBy');
		$data['certificate'] 		= $formData->getInt('certificate');
		$data['student_feedback'] 	= $formData->getInt('student_feedback');
		$data['student_feedback_anonym'] 	= $formData->getInt('student_feedback_anonym');
		$data['trainer_feedback'] 	= $formData->getInt('trainer_feedback');
		$data['published'] 	= $formData->getInt('published');
		$data['created'] 	= strval($date);
		$createdBy = $user->id;
		if($formData->getInt('created_by')):
			$createdBy = $formData->getInt('created_by');
		endif;
		$data['created_by']	= $createdBy;
		$data['logo'] 		= $savename;
		$table->bind($data);
		$table->store();
		$tblClassroomID 	= $table->id;
		// Create the Folder for learningroom
		$pathFL 	= JPATH_SITE.'/images/learningrooms/LR'.$id;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$classroomMaterial = array();
		$classroomMaterialStudent = array();
		if($files['material']): 
			foreach($files['material'] as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					$filename	= $pathFL.'/material/'.str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$tblClassroomID.'/material/'.str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file['tmp_name'], $dest);
					$classroomMaterial[] = array('name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		if($files['reserve']): 
			foreach($files['reserve'] as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					$filename	= $pathFL.'/material/reserve/'.str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$tblClassroomID.'/material/reserve/'.str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file['tmp_name'], $dest);
					$classroomReserve[] = array('name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		if($files['material_student']): 
			foreach($files['material_student'] as $key => $file):
				if($file[0]['name']):
					$filenameRaw 	= JFile::makeSafe($file[0]['name']);
					$filename	= $pathFL.'/material/'.str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$tblClassroomID.'/material/'.str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file[0]['tmp_name'], $dest);
					$classroomMaterialStudent[] = array('studentID' => $key, 'name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		// Save the files
		if($classroomMaterial):
			foreach($classroomMaterial as $material):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['type'] 			= 'material';
				$data['classroomID'] 	= $tblClassroomID;
				$data['folder'] 		= '';
				$data['filename'] 		= $material['name'];
				$data['path'] 			= $material['path'];
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$table->bind($data);
				$table->store();
			endforeach;
		endif;
		if($classroomReserve):
			foreach($classroomReserve as $material):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['classroomID'] 	= $tblClassroomID;
				$data['type'] 			= 'reserve';
				$data['folder'] 		= '';
				$data['filename'] 		= $material['name'];
				$data['path'] 			= $material['path'];
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$data['published'] 		= 0;
				$table->bind($data);
				$table->store();
			endforeach;
		endif;
		if($classroomMaterialStudent):
			foreach($classroomMaterial as $material):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['type'] 			= 'material';
				$data['classroomID'] 	= $tblClassroomID;
				$data['studentID'] 		= $material['studentID'];
 				$data['folder'] 		= '';
				$data['filename'] 		= $material['name'];
				$data['path'] 			= $material['path'];
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$table->bind($data);
				$table->store();
			endforeach;
		endif;
		return $tblClassroomID;
	}
	public function editCommunicationToDatabase($id) {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		$table->invitations 	= $formData->getInt('invitations');
		$table->invitationsBy 	= $formData->getInt('invitationsBy');
		$table->offsetRememberMails = $formData->getStr('offsetRememberMails');
		$table->certificate 	= $formData->getInt('certificate');
		$table->student_feedback 		= $formData->getInt('student_feedback');
		$table->student_feedback_anonym = $formData->getInt('student_feedback_anonym');
		$table->trainer_feedback		= $formData->getInt('trainer_feedback');
		$table->hardware_order 		= $formData->getInt('hardware_order');
		$table->showChat 			= $formData->getInt('showChat');
		$table->email_template 		= $formData->getInt('email_template');
		$table->remember_template 	= $formData->getInt('remember_template');
		$table->verification_template = $formData->getInt('verification_template');
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
	}
	// Saving the Rights
	public function saveRights($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editRightsToDatabase($id);
		} else {
			$id = $this->saveRightsToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'classroom-edit?layout=global&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSaveRights($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editRightsToDatabase($id);
		} else {
			$id = $this->saveRightsToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'classroom-edit?layout=rights&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'classroom-edit?layout=rights&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'classroom-edit?layout=rights&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveRightsToDatabase() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0, 'INT');
		$formData 	= new JInput($input->get('jform', '', 'array'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom','JclassroomTable',array());
		$user 		= JFactory::getUser();
		
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['title'] 				= $formData->getStr('title');
		$data['description'] 		= $formData->getRaw('description');
		$data['main_trainer'] 		= $formData->getInt('main_trainer');
		$data['co_trainer'] 		= $formData->getInt('co_trainer');
		$data['presentation'] 		= $formData->getInt('presentation');
		$data['hardware_order'] 	= $formData->getInt('hardware_order');
		$data['showChat'] 			= $formData->getInt('showChat');
		$data['companyID'] 			= $formData->getInt('companyID');
		$fromDate 	= new Datetime($formData->getStr('fromDate'));
		$toDate 	= new DateTime($formData->getStr('toDate'));
		$data['fromDate'] 		= $fromDate->format('Y-m-d');
		$data['toDate'] 		= $toDate->format('Y-m-d');
		$data['inventa_kursID'] 	= $formData->getInt('inventa_kursID');
		$data['inventa_terminID'] 	= $formData->getInt('inventa_terminID');
		$data['showTo'] 		= $formData->getInt('showTo');
		$data['showToUser'] 	= json_encode($formData->getStr('showToUser'));
		$data['visibleTo'] 		= $formData->getInt('visibleTo');
		$data['email_template'] = $formData->getInt('email_template');
		$data['verification_template'] = $formData->getInt('verification_template');
		$data['remember_template'] = $formData->getInt('remember_template');
		$data['invitations'] 		= $formData->getInt('invitations');
		$data['offsetRememberMails']= $formData->getInt('offsetRememberMails');
		$data['invitationsBy'] 		= $formData->getStr('invitationsBy');
		$data['certificate'] 		= $formData->getInt('certificate');
		$data['student_feedback'] 	= $formData->getInt('student_feedback');
		$data['student_feedback_anonym'] 	= $formData->getInt('student_feedback_anonym');
		$data['trainer_feedback'] 	= $formData->getInt('trainer_feedback');
		$data['published'] 	= $formData->getInt('published');
		$data['created'] 	= strval($date);
		$createdBy = $user->id;
		if($formData->getInt('created_by')):
			$createdBy = $formData->getInt('created_by');
		endif;
		$data['created_by']	= $createdBy;
		$data['logo'] 		= $savename;
		$table->bind($data);
		$table->store();
		$tblClassroomID 	= $table->id;
		// Create the Folder for learningroom
		$pathFL 	= JPATH_SITE.'/images/learningrooms/LR'.$id;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$classroomMaterial = array();
		$classroomMaterialStudent = array();
		if($files['material']): 
			foreach($files['material'] as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					$filename	= $pathFL.'/material/'.str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$tblClassroomID.'/material/'.str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file['tmp_name'], $dest);
					$classroomMaterial[] = array('name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		if($files['reserve']): 
			foreach($files['reserve'] as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					$filename	= $pathFL.'/material/reserve/'.str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$tblClassroomID.'/material/reserve/'.str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file['tmp_name'], $dest);
					$classroomReserve[] = array('name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		if($files['material_student']): 
			foreach($files['material_student'] as $key => $file):
				if($file[0]['name']):
					$filenameRaw 	= JFile::makeSafe($file[0]['name']);
					$filename	= $pathFL.'/material/'.str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$tblClassroomID.'/material/'.str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file[0]['tmp_name'], $dest);
					$classroomMaterialStudent[] = array('studentID' => $key, 'name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		// Save the files
		if($classroomMaterial):
			foreach($classroomMaterial as $material):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['type'] 			= 'material';
				$data['classroomID'] 	= $tblClassroomID;
				$data['folder'] 		= '';
				$data['filename'] 		= $material['name'];
				$data['path'] 			= $material['path'];
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$table->bind($data);
				$table->store();
			endforeach;
		endif;
		if($classroomReserve):
			foreach($classroomReserve as $material):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['classroomID'] 	= $tblClassroomID;
				$data['type'] 			= 'reserve';
				$data['folder'] 		= '';
				$data['filename'] 		= $material['name'];
				$data['path'] 			= $material['path'];
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$data['published'] 		= 0;
				$table->bind($data);
				$table->store();
			endforeach;
		endif;
		if($classroomMaterialStudent):
			foreach($classroomMaterial as $material):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['type'] 			= 'material';
				$data['classroomID'] 	= $tblClassroomID;
				$data['studentID'] 		= $material['studentID'];
 				$data['folder'] 		= '';
				$data['filename'] 		= $material['name'];
				$data['path'] 			= $material['path'];
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$table->bind($data);
				$table->store();
			endforeach;
		endif;
		return $tblClassroomID;
	}
	public function editRightsToDatabase($id) {
		$input 		= JFactory::getApplication()->input;
		echo '<pre>';
		print_r($input);
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		$table->showto 	= $formData->getInt('showto');
		$table->editto 	= $formData->getInt('editto');
		$table->choosableto = $formData->getInt('choosableto');
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
	}
	public function setRead() {
		$input 		= JFactory::getApplication()->input;
		$user 		= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom_rights','JclassroomTable',array());
		$load 	= array(
			'classroomID' 	=> $input->get('classroomID', 0, 'INT'),
			'userID' 		=> $input->get('id', 0, 'INT'),
			'rightType' 	=> $input->get('right', '', 'INT')
		); 
		$check = $table->load($load);
		if($input->get('right', 0, 'INT') == 1):
			if(!$check):
				$data = array();
				$data['classroomID'] = $input->get('classroomID', 0, 'INT');
				$data['userID'] 	= $input->get('id', 0, 'INT');
				$data['rightType'] 	= 1;
				$data['rightValue'] = 1;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$table->bind($data);
				$table->store();
			else:
				$table->classroomID = $input->get('classroomID', 0, 'INT');
				$table->userID 		= $input->get('id', 0, 'INT');
				$table->rightType 	= 1;
				$table->rightValue 	= 1;
				$table->created 	= date('Y-m-d H:i:s');
				$table->created_by	= $user->id;
				$table->store();
			endif;
		endif;
		if($input->get('right', 0, 'INT') == 0):
			$load = array(
				'classroomID' => $input->get('classroomID', 0, 'INT'),
				'userID' => $input->get('id', 0, 'INT'),
				'rightType' => 1
			);
			$table->load($load);
			$id = $table->id;
			$table->delete($id);
		endif;
		echo 'OK';
		exit();
	}
	public function setWrite() {
		$input 		= JFactory::getApplication()->input;
		$user 		= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_rights','JclassroomTable',array());
		if($input->get('right', 0, 'INT') == 1):
			$table->load($id);
			$table->classroomID 	= $input->get('classroomID', 0, 'INT');
			$table->userID 		= $input->get('id', 0, 'INT');
			$table->rightType 	= 2;
			$table->rightValue 	= 1;
			$table->created 	= date('Y-m-d H:i:s');
			$table->created_by	= $user->id;
			$table->store();
		endif;
		if($input->get('right', 0, 'INT') == 0):
			$load = array(
				'classroomID' => $input->get('classroomID', 0, 'INT'),
				'userID' => $input->get('id', 0, 'INT'),
				'rightType' => 2
			);
			$check = $table->load($load);
			if($check):
				$id = $table->id;
				$table->delete($id);
			endif;
		endif;
		echo 'OK';
		exit();
	}
	// Load all persons of the choosen company
	function loadPersonsOfCompany() {
		$input	= JFactory::getApplication()->input;
		// Check, if prefix is "A" (then load persons of customer)
		$cA 		= substr($input->get('companyID', 0, 'STR'),0,1);
		$persons 	= array();
		$db 	= JFactory::getDbo();
		if($cA == "A"):
			$companyID 	= str_replace('A','',$input->get('companyID',0,'STR'));
			$query 	= $db->getQuery(true);
			$query->select(array('
				a.*
				'));
	        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
	        $query->where('a.customerID = '.$companyID);
			$query->order('a.last_name ASC');
			$db->setQuery($query);
			$admins = $db->loadObjectList();
			if($admins):
				foreach($admins as $admin):
					$persons[] = array(
						'id' 	=> $admin->userID,
						'name'	=> $admin->first_name.' '.$admin->last_name,
						'function' => 'Administrator'
					);
				endforeach;
			endif;
			$query 	= $db->getQuery(true);
			$query->select(array('
				a.*
				'));
	        $query->from($db->quoteName('#__jclassroom_trainers','a'));
	        $query->where('a.customerID = '.$companyID);
	        $query->where('a.published = 1');
			$query->order('a.last_name ASC');
			$db->setQuery($query);
			$admins = $db->loadObjectList();
			if($admins):
				foreach($admins as $admin):
					$persons[] = array(
						'id' 	=> $admin->tblUserID,
						'name'	=> $admin->first_name.' '.$admin->last_name,
						'function' => 'Trainer'
					);
				endforeach;
			endif;
		else:
			$query 	= $db->getQuery(true);
			$query->select(array('
				a.*
				'));
	        $query->from($db->quoteName('#__jclassroom_students','a'));
	        $query->where('a.companyID = '.$input->get('companyID', 0, 'INT'));
	        $query->where('a.published = 1');
			$query->order('a.last_name ASC');
			$db->setQuery($query);
			$students = $db->loadObjectList();
			if($students):
				foreach($students as $student):
					$persons[] = array(
						'id' 	=> $student->tblUserID,
						'name'	=> $student->first_name.' '.$student->last_name,
						'function' => 'Student'
					);
				endforeach;
			endif;
		endif;
		echo json_encode($persons);
		exit();
	}

	function deleteFolder() {
		JFolder::delete(JPATH_SITE.'/images/learningrooms/LR83/reserve');
		echo 'OK';
		exit();
	}
	function createFolderTree() {
		$path = JPATH_SITE.'/images/learningrooms/LR76';
		$tree = JFolder::listFolderTree($path, $filter, $maxLevel = 4, $level = 0, $parent = 0);
		echo json_encode($tree);
		exit();
	}
	function writeUnit() {
		$input	= JFactory::getApplication()->input;
	}
	function addDays() {
		$input	= JFactory::getApplication()->input;
		$start 	= $input->get('start', '', 'STR');
		$end 	= $input->get('end', '', 'STR');
		$date 	= $start;
		$start 	= new DateTime($start);
		$end 	= new DateTime($end);
		$date 	= new DateTime($date);
		$interval = $start->diff($end);
		//$interval = $interval + 1;
		$html = '';
		$intervalDays 	= $interval->days;
		for($i = 0; $i <= $intervalDays; $i++) {
			$html .= '<div id="cardFor' .$date->format('d').'" class="card bg-light mb-2">';
			$html .= '<div class="card-header d-flex" style="justify-content: space-between">';
			$html .= '<i onclick="openDay(&quot;'.$date->format('d').'&quot;);" class="open fa fa-chevron-right"></i>';
			$html .= '<h2>'.$date->format('l, d.m.Y').'</h2>';
			$html .= '<a id="addFor'.$date->format('d').'" onclick="addUnit(&quot;'.$date->format('d').'&quot;);"><i style="font-size: 24px;border-radius: 50%;" class="fa fa-plus bg-success text-white p-1"></i></a>';
			$html .= '</div>';
			$html .= '<div class="card-body">';
			$html .= '</div>';
			$html .= '</div>';
			$date->add(new DateInterval('P1D'));
		}
		echo $html;
		exit();
	}
	function loadInvitationTemplate() {
		$input = JFactory::getApplication()->input;

		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('invitationTemplate', 0, 'INT')));
		$db->setQuery($query);
		$template = $db->loadobject();
		echo $template->text;
		exit();
	}
	function getTemplate() {
		$session 	= JFactory::getSession();
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$typ 			= $input->get('typ', 0, 'INT');
		$classroomID 	= $input->get('classroomID', '', 'INT');
		$dayID 			= $input->get('dayID', '', 'STR');
		$moduleID 		= $input->get('moduleID', '', 'INT');
		$unitCounter 	= $input->get('unitCounter', 0, 'INT');
		$library 		= $input->get('library', 0, 'INT');
		$unitID 		= $input->get('unitID', 0, 'INT');
		if($library == 0):
			//Save Unit 
			// 1. Load Max Ordering 
			$db 	= JFactory::getDbo(); 
			$query 	= $db->getQuery(true);
			$query->select('max(a.ordering)');
	        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->where($db->quotename('a.dayID').' = '.$db->quote($dayID));
	        $query->where($db->quotename('a.moduleID').' = '.$db->quote($moduleID));
			$db->setQuery($query);
			$nextOrder = $db->loadResult();
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
			$data = array();
			$data['customerID'] = $session->get('customerID');
			$data['classroomID'] = $classroomID;
			$data['dayID'] 		= $dayID;
			$data['moduleID'] 	= $moduleID;
			$data['unitID'] 	= $typ;
			$data['unitType'] 	= $typ;
			$data['content'] 	= '';
			$data['ordering'] 	= $nextOrder + 1;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by'] = $user->id;
			$data['published']	= 1;
			$table->bind($data);
			$table->store();
			$unitID = $table->id;
		endif;
		$this->reorderUnits($classroomID);
		JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
        $template = new UnitsHelper();
        $template = $template->getTemplate('structure', $classroomID, $dayID, $typ, $unitID);

        echo $template;
        exit();
	}
	// Scripts for saving something
	function saveModule() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$moduleID 		= $input->get('moduleID', 0, 'INT');
		$title	 		= $input->get('title', '', 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$table->load($moduleID);
		$table->title 	= $title;
		$table->store();
		echo 'OK';
		exit();
	}
	function saveUnit() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		$title	 		= $input->get('title', '', 'STR');
		$duration	 	= $input->get('duration', 0, 'INT');
		$content	 	= $input->get('content', '', 'RAW');
		$link	 		= $input->get('link', '', 'STR');
		$quizz	 		= $input->get('quizz', '', 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$table->load($unitID);
		$table->title 	= $title;
		$table->duration 	= $duration;
		$table->content 	= $content;
		$table->link 		= $link;
		$table->quizzID 	= $quizz;
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		echo 'OK';
		exit();
	}
	function saveDayLibrary() {
		// Be sure, the neccassary folder exit
		$pathFL 	= JPATH_SITE.'/images/library';
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/themes');
		$session = JFactory::getSession();
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$dayID 		= $input->get('dayID', 0, 'INT');
		$title 		= $input->get('title', 0, 'STR');
		$description= $input->get('desc', 0, 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$day = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$day->load($dayID);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$library = JTable::getInstance('Classroom_days_library','JclassroomTable',array());
		$data 			= array();
		$data['customerID'] 	= $session->get('customerID');
		$data['classroomID'] 	= 0;
		$data['title'] 			= $title;
		$data['description']	= $description;
		$data['day'] 	= $day->day;
		$data['dayID'] 	= $day->dayID;
		$data['created']	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$library->bind($data);
		$library->store();
		$newDayID = $library->id;
		$pathLibrary = $pathFL.'/themes/theme_'.$newDayID;
		$pathLibraryToMove = '/images/library/themes/theme_'.$newDayID;
		JFolder::create($pathLibrary);
		$this->copyModules($day->id,$newDayID, $day->classroomID);
		
		echo 'OK';
		exit();
	}
	function saveModuleLibrary() {
		// Be sure, the neccassary folder exit
		$pathFL 	= JPATH_SITE.'/images/library';
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/modules');
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$moduleID 		= $input->get('moduleID', 0, 'INT');
		$title	 		= $input->get('title', '', 'STR');
		$description	= $input->get('description', '', 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$module = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$module->load($moduleID);
		$classroomID = $module->classroomID;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$library = JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
		$data 			= array();
		$data['customerID'] 	= $session->get('customerID');
		$data['title'] 			= $title;
		$data['description'] 	= $description;
		$data['ordering'] 		= 0;
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$data['published'] 		= 1;
		$library->bind($data);
		$library->store();
		$newModuleID = $library->id;
		// Copy the units
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		$query->where('a.moduleID = '.$moduleID);
		$query->where('a.published = 1');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$unit = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
				$data 			= array();
				$data['customerID'] = $session->get('customerID');
				$data['moduleID'] 	= $library->id;
				$data['unitType'] 	= $item->unitType;
				$data['ordering'] 	= $item->ordering;
				$data['duration'] 	= $item->duration;
				$data['title'] 		= $item->title;
				$data['content'] 	= $item->content;
				$data['link'] 		= $item->link;
				$data['quizzID'] 	= $item->quizzID;
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$data['published'] 		= 1;
				$unit->bind($data);
				$unit->store();
				$newUnitID = $unit->id;
				$pathLibrary = $pathFL.'/modules/module_'.$newModuleID.'/units/unit_'.$newUnitID;
				$pathLibraryToMove = '/images/library/modules/module_'.$newModuleID.'/units/unit_'.$newUnitID;
				JFolder::create($pathLibrary);
				// Copy the files
				$db 	= JFactory::getDbo(); 
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($item->id));
		        $query->order('a.id ASC');
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$file_library = JTable::getInstance('File','JclassroomTable',array());
						$data 			= array();
						$data['customerID'] = $session->get('customerID');
						$data['type'] 		= $file->type;
						$data['classroomID']= $classroomID;
						$data['unitID'] 	= $newUnitID;
						$data['studentID'] 		= 0;
						$data['folder'] 		= '';
						$data['filename'] 		= $file->filename;
						$data['path'] 			= $pathLibraryToMove.'/'.$file->filename;
						$data['ordering'] 		= 0;
						$data['created'] 		= date('Y-m-d H:i:s');
						$data['created_by'] 	= $user->id;
						$data['published'] 		= 1;
						$file_library->bind($data);
						$file_library->store();
						copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.'/'.$file->filename);
					endforeach;
				endif;
			endforeach;
		endif;
		echo $module->title;
		exit();
	}
	function saveUnitLibrary() {
		// Be sure, the neccassary folder exit
		$pathFL 	= JPATH_SITE.'/images/library';
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/units');
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$unitID = $input->get('unitID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$unit 	= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$unit->load($unitID);
		/*preg_match_all('/<img[^>]+>/i',$unit->content, $result);
		$img = array();
		echo '<pre>';
		$i = 0;
		foreach( $result[0] as $img_tag):
		    preg_match_all('/(src)=("[^"]*")/i',$img_tag, $img[$i]);
			$i++;
		endforeach;
		if($img):
			foreach($img as $im):
				foreach($im[2] as $ia):
					$ia = str_replace('"','', $ia);
					$filename = explode('/',$ia);
					print_r($filename);
					$cF = count($filename);
					echo $cF;
					echo $filename[$cF - 1];
					if($ia):
						//$content = file_get_contents($ia);
						//file_put_contents(JPATH_SITE.'/images/library/'.$filename[$cF - 1], $content);
						//copy($ia, JPATH_SITE.'/images/library/'.$filename[$cF - 1]);
						$ch = curl_init($ia);
						$fp = fopen(JPATH_SITE.'/images/library/'.$filename[$cF - 1], 'wb');
						curl_setopt($ch, CURLOPT_FILE, $fp);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_exec($ch);
						curl_close($ch);
						fclose($fp);
					endif;
					/*if(copy($ia,JPATH_SITE.'/images/library')):
						echo $ia.'<br>';
					else:
						echo $ia.'NIX<br>';
					endif;
				endforeach;
			endforeach;
		endif;*/
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$library 	= JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
		$data 		= array();
		$data['customerID'] 	= $session->get('customerID');
		$data['unitType'] 		= $unit->unitType;
		$data['title'] 			= $input->get('title', '','STR');
		$data['description'] 	= $input->get('description', '','STR');
		$data['content'] 		= $unit->content;
		$data['duration'] 		= $unit->duration;
		$data['link'] 			= $unit->link;
		$data['quizzID'] 		= $unit->quizzID;
		$data['ordering'] 		= 0;
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$data['published'] 		= $unit->published;
		$library->bind($data);
		$library->store();
		$newUnitID = $library->id;
		$pathLibrary = $pathFL.'/units/unit_'.$newUnitID;
		$pathLibraryToMove = '/images/library/units/unit_'.$newUnitID;
		JFolder::create($pathLibrary);
		$pathLibraryUnits = $pathFL.'/units/unit_'.$newUnitID.'/material';
		JFolder::create($pathLibraryUnits);
		// Copy the files
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_files','a'));
        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitID));
        $query->order('a.id ASC');
		$db->setQuery($query);
		$files = $db->loadObjectList();
		if($files):
			foreach($files as $file):
				$file_library = JTable::getInstance('File','JclassroomTable',array());
				$data 			= array();
				$data['customerID'] = $session->get('customerID');
				$data['type'] 		= $file->type;
				$data['classroomID']= 0;
				$data['unitID'] 	= $newUnitID;
				$data['studentID'] 		= 0;
				$data['folder'] 		= '';
				$data['filename'] 		= $file->filename;
				$data['path'] 			= $pathLibraryToMove.'/'.$file->filename;
				$data['ordering'] 		= 0;
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$data['published'] 		= 1;
				$file_library->bind($data);
				$file_library->store();
				copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.'/'.$file->filename);
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function copyModules($oldDayID, $newDayID, $newLearningRoomID) {
		$pathFL = JPATH_SITE.'/images/library';
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
        $query->where($db->quotename('a.dayID').' = '.$db->quote($oldDayID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$modules = $db->loadObjectList();
		if($modules):
			foreach($modules as $module):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $session->get('customerID');
				$data['classroomID']= 0;
				$data['dayID'] 		= $newDayID;
				$data['title'] 		= $module->title;
				$data['description'] = $module->description;
				$data['ordering'] 	= $module->ordering;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $module->published;
				$tableD->bind($data);
				$tableD->store();
				$newModuleID = $tableD->id;
				$pathLibrary = $pathFL.'/themes/theme_'.$newDayID.'/modules/module_'.$newModuleID;
				$pathLibraryToMove = '/images/library/themes/theme_'.$newDayID.'/modules/module_'.$newModuleID;
				JFolder::create($pathLibrary);
				$this->copyUnits($module->id, $newModuleID, $newLearningRoomID, $newDayID);
			endforeach;
		endif;
	}
	function copyUnits($oldModuleID, $newModuleID, $newLearningRoomID, $newDayID) {
		$session = JFactory::getSession();
		$pathFL = JPATH_SITE.'/images/library';
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
        $query->where($db->quotename('a.moduleID').' = '.$db->quote($oldModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $session->get('customerID');
				$data['classroomID']= 0;
				$data['dayID'] 		= $newDayID;
				$data['moduleID'] 		= $newModuleID;
				$data['unitID'] 		= $unit->unitID;
				$data['unitType'] 		= $unit->unitType;
				$data['ordering'] 		= $unit->ordering;
				$data['duration'] 		= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $unit->published;
				$tableD->bind($data);
				$tableD->store();
				$newUnitID = $tableD->id;
				$pathLibrary = $pathFL.'/themes/theme_'.$newDayID.'/modules/module_'.$newModuleID.'/units/unit_'.$newUnitID;
				$pathLibraryToMove = '/images/library/themes/theme_'.$newDayID.'/modules/module_'.$newModuleID.'/units/unit_'.$newUnitID;
				JFolder::create($pathLibrary);
				// Copy the files
				$db 	= JFactory::getDbo(); 
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
		        $query->order('a.id ASC');
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$file_library 	= JTable::getInstance('File_library','JclassroomTable',array());
						$data 			= array();
						$data['customerID'] = $session->get('customerID');
						$data['type'] 		= 'unit';
						$data['classroomID']= 0;
						$data['unitID'] 	= $newUnitID;
						$data['studentID'] 		= 0;
						$data['folder'] 		= '';
						$data['filename'] 		= $file->filename;
						$data['path'] 			= $pathLibraryToMove.'/'.$file->filename;
						$data['ordering'] 		= 0;
						$data['created'] 		= date('Y-m-d H:i:s');
						$data['created_by'] 	= $user->id;
						$data['published'] 		= 1;
						$file_library->bind($data);
						$file_library->store();
						copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.'/'.$file->filename);
					endforeach;
				endif;
			endforeach;
		endif;
	}
	// Scripts for adding something
	function addNewDay() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$day 			= $input->get('day', 0, 'STR');
		$title 			= $input->get('title', 0, 'STR');
		//Get max ordering for classroom days
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.ordering');
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->order('a.ordering DESC');
        $query->setlimit(1);
		$db->setQuery($query);
		$lastOrdering = $db->loadResult();

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$load = array('classroomID' => $classroomID, 'day' => date('Y-m-d', strtotime($day)));
		$check = $table->load($load);
		if(!$check):
			$data = array();
			$data['classroomID'] 	= $classroomID;
			if($day):
				$data['day'] 			= date('Y-m-d', strtotime($day));
				$data['dayID'] 			= date('d', strtotime($day));
			endif;
			$data['title'] 			= $title;
			$data['ordering'] 		= $lastOrdering +1;
			$data['published']		= 1;
			$table->bind($data);
			$table->store();
			$dayID = $table->id;
			//Load Template

			echo $dayID;
		else:
			echo 'Fehler: Das Datum '.date('d.m.Y', strtotime($day)).' wurde für diesen Learningroom bereits angelegt.';
		endif;
		exit();
	}
	function addModule() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$classroomID 		= $input->get('classroomID', 0, 'INT');
		$dayID 				= $input->get('dayID', 0, 'INT');
		$saveFromLibrary 	= $input->get('saveFromLibrary', 0, 'INT');
		$copyFromModuleID 	= $input->get('copyFromModuleID', 0, 'INT');
		if($saveFromLibrary == 0):
			// Add the module to database
			JLoader::register('AddSomethingHelper',JPATH_COMPONENT_SITE.'/helpers/addSomething.php');
	    	$addModule 		= new AddSomethingHelper();
	    	$addModuleID 	= $addModule->addModule($saveFromLibrary, $classroomID, $dayID);
	    endif;
	    if($saveFromLibrary == 1):
			// Add the module to database
			// Returns the ID of the the new module
			JLoader::register('AddSomethingHelper',JPATH_COMPONENT_SITE.'/helpers/addSomething.php');
	    	$addModule 		= new AddSomethingHelper();
	    	$addModuleID 	= $addModule->addModuleFromLibrary($saveFromLibrary, $classroomID, $dayID, $copyFromModuleID);
	    	// Check if the dublicated module has some units to copy
	    	// Returns the complete template to insert it into the module
			JLoader::register('AddSomethingHelper',JPATH_COMPONENT_SITE.'/helpers/addSomething.php');
	    	$addUnits 		= new AddSomethingHelper();
	    	$unitsTemplate 	= $addUnits->addUnitsForModuleFromLibrary($classroomID, $dayID, $copyFromModuleID, $addModuleID);
	    endif;
		// Render the module
		JLoader::register('ModulesHelper',JPATH_COMPONENT_SITE.'/helpers/modules.php');
    	$template = new ModulesHelper();
    	$moduleTemplate = $template->getTemplate($classroomID, $dayID, $addModuleID, '');
    	// Render the units
    	$moduleTemplate = str_replace('{units}', $unitsTemplate, $moduleTemplate);
    	echo $moduleTemplate;
		exit();
	}
	function addDayFromLibrary() {
		$user 	= JFactory::getUser();
		$session = JFactory::getSession();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.id,a.title,a.description'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_library','a'));

        if($session->get('group') == 'customer'):
        	$query->where($db->quotename('a.customerID').' = '.$db->quote($session->get('customerID')));
    	endif;
    	if($session->get('group') == 'trainer'):
        	$query->where($db->quotename('a.customerID').' = '.$db->quote($session->get('customerID')));
    	endif;
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$TID = '[TID: '.$item->id.' ]';
				$return[] = array('id' => $item->id, 'title' => $item->title, 'description' => $item->description,'TID' => $TID);
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	function addModuleFromLibrary() {
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$user 	= JFactory::getUser();
		$groups = JAccess::getGroupsByUser($user->id);
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.id,a.title,a.description'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
        //$query->where($db->quotename('a.dayID').' = '.$db->quote(0));
        if(in_array(8,$groups)):
        	//$query->where($db->quotename('a.created_by').' = '.$db->quote($user->id));
    	endif;
    	if(in_array(12,$groups)):
        	$query->where($db->quotename('a.customerID').' = '.$db->quote($customerID).' OR a.customerID = 0');
    	endif;
    	if(in_array(13,$groups)):
        	$query->where($db->quotename('a.customerID').' = '.$db->quote($customerID).' OR a.customerID = 0');
    	endif;
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$return[] = array('id' => $item->id, 'title' => $item->title, 'description' => $item->description);
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	function addUnitFromLibrary() {
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.id,a.title,a.description,a.unitType'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
        $query->where('a.customerID = '.$session->get('customerID'));
        $query->order('a.title ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				switch($item->unitType):
					case 1: 
						$type = 'Freitext';
						break;
					case 2:
						$type = 'virtual classroom';
						break;
					case 3:
						$type = 'Pause';
						break;
					case 4:
						$type = 'Quizz';
						break;
					case 5:
						$type = 'Einzelübung';
						break;
					case 6:
						$type = 'Gruppenübung';
						break;
					case 7:
						$type = 'Video';
						break;
					case 8:
						$type = 'Feedback';
						break;
					case 9:
						$type = 'Aufgabe';
						break;
				endswitch;
				$return[] = array(
					'title' => $item->title,
					'BUID'	=> '[BUID: '.$item->id.' ]',
					'unitType' => $type,
					'id' 	=> $item->id
				);
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	// Scripts for delete something
	function deleteDay() {
		$input 	= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->delete($dayID);
		//Delete Timeblock, if exist
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_timeblocks_themes','JclassroomTable',array());
		$load = array('classroomID' => $classroomID, 'themeID' => $dayID);
		$check = $table->load($load);
		if($check):
			$id = $table->id;
			$table->delete($id);
		endif;
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.dayID = '.$dayID);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
				// Load the days
		        $query = $db->getQuery(true);
				$query->select(array('a.id'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				$query->where('a.moduleID = '.$item->id);
				$db->setQuery($query);
				$units = $db->loadObjectList();
				if($units):
					foreach($units as $unit):
						$query 	= $db->getQuery(true);
						$query->select('a.*');
				        $query->from($db->quoteName('#__jclassroom_files','a'));
				        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
						$db->setQuery($query);
						$files = $db->loadObjectList();
						if($files):
							foreach($files as $file):
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$tableF = JTable::getInstance('File','JclassroomTable',array());
								$tableF->delete($file->id);
							endforeach;
						endif;
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableU = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
						$tableU->delete($unit->id);
					endforeach;
				endif;
				$table->delete($item->id);
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function deleteModule() {
		$input = JFactory::getApplication()->input;
		$moduleID 		= $input->get('moduleID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$table->delete($moduleID);
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		$query->where('a.moduleID = '.$moduleID);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item): 
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($item->id));
		        $query->order('a.id ASC');
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$table = JTable::getInstance('File','JclassroomTable',array());
						$table->delete($file->id);
						JFile::delete($file->path);
					endforeach;
				endif;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$table->delete($item->id);
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function deleteUnit() {
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$table->load($unitID);
		$classroomID = $table->classroomID;
		$table->delete($unitID);
		// Search for Images and delete them, if necassary
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_files','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitID));
		$db->setQuery($query);
		$files = $db->loadObjectList();
		if($files):
			foreach($files as $file):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('File','JclassroomTable',array());
				$table->delete($file->id);
				unlink(JPATH_SITE.$file->path);
			endforeach;
		endif;
		$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	// Scripts for inserting something
	function insertDayFromLibrary() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		$insertDayID 	= $input->get('insertDayID', 0, 'INT');
		$session 		= JFactory::getSession();
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_library','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($insertDayID));
		$db->setQuery($query);
		$theme = $db->loadObject();
		if($theme):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$library = JTable::getInstance('Classroom_days','JclassroomTable',array());
			$data 			= array();
			$data['customerID'] 	= $session->get('customerID');
			$data['classroomID'] 	= $classroomID;
			$data['title'] 			= $theme->title;
			$data['description']	= $theme->description;
			$data['day'] 	= $theme->day;
			$data['dayID'] 	= $theme->dayID;
			$data['ordering'] 	= $theme->ordering;
			$data['created']	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$library->bind($data);
			$library->store();
			$newDayID = $library->id;
			$query 	= $db->getQuery(true);
			$query->select('a.*');
	        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
	        $query->where($db->quotename('a.dayID').' = '.$db->quote($insertDayID));
	        $query->order('a.ordering ASC');
			$db->setQuery($query);
			$modules = $db->loadObjectList();
			if($modules):
				foreach($modules as $module):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$tableD = JTable::getInstance('Classroom_modules','JclassroomTable',array());
					$data = array();
					$data['customerID'] = $session->get('customerID');
					$data['classroomID']= $classroomID;
					$data['dayID'] 		= $newDayID;
					$data['title'] 		= $module->title;
					$data['description'] = $module->description;
					$data['ordering'] 	= $module->ordering;
					$data['created'] 	= date('Y-m-d H:i:s');
					$data['created_by'] = $user->id;
					$data['published'] 	= $module->published;
					$tableD->bind($data);
					$tableD->store();
					$newModuleID = $tableD->id;
					$this->insertUnitsFromLibrary($module->id, $newModuleID, $classroomID, $newDayID);
				endforeach;
			endif;
		endif;
		echo $newDayID.'_'.$theme->title;
		exit();
	}
	function insertUnitsFromLibrary($libraryModuleID, $newModuleID, $classroomID, $dayID) {
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
        $query->where($db->quotename('a.moduleID').' = '.$db->quote($libraryModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableU = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $session->get('customerID');
				$data['classroomID'] = $classroomID;
				$data['dayID'] 		= $dayID;
				$data['moduleID'] 	= $newModuleID;
				$data['unitType'] 	= $unit->unitType;
				$data['ordering'] 	= $unit->ordering;
				$data['duration'] 	= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $unit->published;
				$tableU->bind($data);
				$tableU->store();
				$newUnitID = $tableU->id;
				// check if the library-unit has some Files. If true, then copy the files, too.
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$pathToMove = '/images/learningrooms/LR'.$classroomID.'/units/';
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableFC = JTable::getInstance('File','JclassroomTable',array());
						$data = array();
						$data['type'] 			= $file->type;
						$data['classroomID'] 	= $classroomID;
						$data['unitID'] 	= $newUnitID;
						$data['folder'] 	= $file->folder;
						$data['filename'] 	= $file->filename;
						$data['path'] 		= $pathToMove.$file->filename;
						$data['created'] 	= date('Y-m-d H:i:s');
						$data['created_by'] = $user->id;
						$data['published'] 	= $file->published;
						$tableFC->bind($data);
						$tableFC->store();
						copy(JPATH_SITE.$file->path,JPATH_SITE.$pathToMove.$file->filename);
					endforeach;
				endif;
			endforeach;
		endif;
	}
	function insertModuleFromLibrary() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		$insertModuleID = $input->get('insertModuleID', 0, 'INT');
		$newModuleID 	= $input->get('newModuleID', 0, 'INT');
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($insertModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			$db 	= JFactory::getDbo(); 
			$query 	= $db->getQuery(true);
			$query->select('max(a.ordering)');
	        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->where($db->quotename('a.dayID').' = '.$db->quote($dayID));
			$db->setQuery($query);
			$nextOrder = $db->loadResult();
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableU = JTable::getInstance('Classroom_modules','JclassroomTable',array());
				$data = array();
				$data['classroomID'] = $classroomID;
				$data['dayID'] 		= $dayID;
				$data['title'] 		= $unit->title;
				$data['description']= $unit->description;
				$data['ordering'] 	= $nextOrder + 1;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= 1;
				$tableU->bind($data);
				$tableU->store();
				$return = array('moduleID' => $newModuleID,'typeID' => $unit->unitType,'unitID' => $tableU->id);
			endforeach;
		endif;
		//echo json_encode($return);
		echo $tableU->id;
		exit();
	}
	function insertUnitFromLibrary() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		$insertUnitID 	= $input->get('insertUnitID', 0, 'INT');
		$newModuleID 	= $input->get('newModuleID', 0, 'INT');
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($insertUnitID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			$db 	= JFactory::getDbo(); 
			$query 	= $db->getQuery(true);
			$query->select('max(a.ordering)');
	        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->where($db->quotename('a.dayID').' = '.$db->quote($dayID));
			$db->setQuery($query);
			$nextOrder = $db->loadResult();
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableU = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$data = array();
				$data['customerID']	= $unit->customerID;
				$data['classroomID'] = $classroomID;
				$data['dayID'] 		= $dayID;
				$data['moduleID'] 	= $newModuleID;
				$data['unitType'] 	= $unit->unitType;
				$data['ordering'] 	= $nextOrder + 1;
				$data['duration'] 	= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= 1;
				$tableU->bind($data);
				$tableU->store();
				switch ($unit->unitType) {
					case 1:
						$type = 'Freitext';
						break;
					case 2:
						$type = 'virtual Classroom';
						break;
					case 3:
						$type = 'Pause';
						break;
					case 4:
						$type = 'Quizz';
						break;
					case 5:
						$type = 'Einzelübung';
						break;
					case 6:
						$type = 'Gruppenübung';
						break;
					case 7:
						$type = 'Video';
						break;
					case 8:
						$type = 'Feedback';
						break;
					case 9:
						$type = 'Aufgabe';
						break;
					default:
						# code...
						break;
				}
				$return = array(
					'moduleID' => $newModuleID,
					'typeID' => $unit->unitType,
					'type' => $type,
					'unitID' => $tableU->id);
				// Load File From Library
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
				$db->setQuery($query);
				$libFiles = $db->loadObjectList();
				if($libFiles):
					foreach($libFiles as $file):
						$pathLibraryToMove = '/images/learningrooms/LR'.$classroomID.'/units/';
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableF = JTable::getInstance('File','JclassroomTable',array());
						$data = array();
						$data['type'] 		= $file->type;
						$data['classroomID']= $classroomID;
						$data['unitID'] 	= $tableU->id;
						$data['studentID'] 	= 0;
						$data['folder'] 	= $file->folder;
						$data['filename'] 	= $file->filename;
						$data['path'] 		= $pathLibraryToMove.$file->filename;
						$data['created'] 	= date('Y-m-d H:i:s');
						$data['created_by'] = $user->id;
						$data['published'] 	= 1;
						$tableF->bind($data);
						$tableF->store();
						copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.$file->filename);
					endforeach;
				endif;
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	// Scripts for hiding something
	function hideDay() {
		$input = JFactory::getApplication()->input;
		$dayID 		= $input->get('dayID', 0, 'INT');
		$published 	= $input->get('published', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->load($dayID);
		$table->published 	= $published;
		$table->store();
		echo 'OK';
		exit();
	}
	function hideModule() {
		$input = JFactory::getApplication()->input;
		$moduleID 		= $input->get('moduleID', 0, 'INT');
		$published 		= $input->get('published', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$table->load($moduleID);
		$table->published 	= $published;
		$table->store();
		echo 'OK';
		exit();
	}
	function hideUnit() {
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		$published 		= $input->get('published', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$table->load($unitID);
		$table->published 	= $published;
		$table->store();
		echo 'OK';
		exit();
	}
	// Scripts for loading something
	function loadAvailableDays() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$currentDay 	= $input->get('currentDay', '', 'STR');
		// LOAD THE DAYS
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id,a.title'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.id <> '.$currentDay);
		$query->order('a.day asc');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		if($days):
			foreach($days as $day):
				$return[] = array('id' => $day->id,'title' => $day->title);
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	function loadDay() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->load($dayID);
		$theDate = $table->day;
		JLoader::register('DayHelper',JPATH_COMPONENT_SITE.'/helpers/day.php');
        $template = new DayHelper();
        $day = $template->getTemplate($theDate, $dayID);
        // LOAD THE MODULES
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id,a.title'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.dayID = '.$dayID);
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$modules = $db->loadObjectList();
		if($modules):
			$moduleTemplate = '';
			foreach($modules as $module):
	        	JLoader::register('ModulesHelper',JPATH_COMPONENT_SITE.'/helpers/modules.php');
	        	$template = new ModulesHelper();
	        	$moduleTemplate .= $template->getTemplate($classroomID, $dayID, $module->id, $module->title);
	        	// LOAD THE UNITS
		        $query = $db->getQuery(true);
				$query->select(array('a.id,a.unitType'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				$query->where('a.classroomID = '.$classroomID);
				$query->where('a.moduleID = '.$module->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$units = $db->loadObjectList();
				if($units):
					foreach($units as $unit):
						$query = $db->getQuery(true);
						$query->select(array('a.*'));
				        $query->from($db->quoteName('#__jclassroom_files','a'));
						$query->where('a.classroomID = '.$classroomID);
						$query->where('a.unitID = '.$unit->id);
						$query->where('a.type = "unit"');
						$db->setQuery($query);
						$files = $db->loadObjectList();
	        			JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
						$template = new UnitsHelper();
						$unitTemplate .= $template->getTemplate('structure', $classroomID, $dayID, $unit->unitType,$unit->id);
					endforeach;
				endif;
				$moduleTemplate = str_replace('{units}', $unitTemplate, $moduleTemplate);
				$unitTemplate = '';
				$html = str_replace('{modules}', $moduleTemplate, $day);
			endforeach;
        else:
        	$html = str_replace('{modules}', '', $day);
        endif;
       	echo $html;

       	exit();
	}
	function loadTermine() {
		$now 	= date('Y-m-d');
		$input 	= JFactory::getApplication()->input;
		// Load Data from INVENTA
		$option = array(); 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede01';    	// User for database authentication
		$option['password'] = 'sbZ78p5etz';   // Password for database authentication
		$option['database'] = 'cede01';    	// Database name
		$option['prefix']   = 'web3_';     // Database prefix (may be empty)
		$return = array();
		$db = JDatabaseDriver::getInstance( $option );
		$query = $db->getQuery(true);
		$query->select(array('a.id,a.termin_beginn, a.termin_ende'));
        $query->from($db->quoteName('#__inventa_termine','a'));
        $query->where($db->quotename('a.kurs').' = '.$db->quote($input->get('kursID')));
        $query->where($db->quotename('a.termin_beginn').' >= '.$db->quote($now));
		$query->where('a.aktiv = 1');
		$query->order('a.termin_beginn asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$beginn = new datetime($item->termin_beginn);
				$ende = new datetime($item->termin_ende);
				$return[] = array('title' => '', 'id' => $item->id, 'beginn' => $beginn->format('d.m.Y'), 'ende' => $ende->format('d.m.Y'));
			endforeach;
		else:
			$return[] = array('title' => 'Keine Termine gefunden','id' => 0, 'beginn' => '', 'ende' => '');
		endif;

		echo json_encode($return);
		exit();
	}
	function loadKursData() {
		$input 	= JFactory::getApplication()->input;
		// Load Data from INVENTA
		$option = array(); 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede01';    	// User for database authentication
		$option['password'] = 'sbZ78p5etz';   // Password for database authentication
		$option['database'] = 'cede01';    	// Database name
		$option['prefix']   = 'web3_';     // Database prefix (may be empty)
		$return = array();
		$db = JDatabaseDriver::getInstance( $option );
		$query = $db->getQuery(true);
		$query->select(array('a.inhalt'));
        $query->from($db->quoteName('#__inventa_kurse','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('kursID')));
		$db->setQuery($query);
		$result = $db->loadResult();

		echo $result;
		exit();
	}
	function loadQuizzResult() {
		$input 		= JFactory::getApplication()->input;
		$theResultID= $input->get('theResultID', 0, 'INT');
		$classroomID= $input->get('classroomID', 0, 'INT');
		$unitID 	= $input->get('unitID', 0, 'INT');
		// Get the result
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Theresults','JclassroomTable',array());
		$result->load($theResultID);
		// Get Username
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$user = JTable::getInstance('User','JclassroomTable',array());
		$user->load($result->created_by);
		// Get Quizz
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quizz = JTable::getInstance('Unit','JclassroomTable',array());
		$quizz->load($result->quizzID);
		JLoader::register('StageResultHelper',JPATH_COMPONENT_SITE.'/helpers/stageResult.php');
		$result = new stageResultHelper();
		//getResultPDF($rID, $fromLR, $userID, $completeEvaluation, $classroomUD, $publishedQuizzID, $unitID, $pdf)
		$html = $result->getResult($theResultID, 1, 0, 0, $classroomID, 0, $unitID,0);
		$return = array(
			'quizzTitle' => $quizz->title,
			'quizzDescription' => $quizz->description,
			'username' 	=> $user->name,
			'html' 		=> $html
		);
		echo json_encode($return);
		exit();
	}
	function loadQuizzCompleteResult() {
		$input 	= JFactory::getApplication()->input;
		$classroomID = $input->get('classroomID', 0, 'INT');
		$quizzID 	= $input->get('unitID', 0, 'INT');
		$unitPosID 	= $input->get('unitPosID', 0, 'INT');

		// Load the quizz to get the chart
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($quizzID);
		$chartID 	= $table->chart;
		
		JLoader::register('QuizzResultHelper',JPATH_COMPONENT_SITE.'/helpers/quizzResult.php');
		$result = new quizzResultHelper();
		$theQuestions = $result->loadQuizzResultComplete($classroomID, $unitPosID, false, $quizzID, $chartID,'open');

		/*//if($chartID == 2 || $chartID == 3 || $chartID == 4):
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(array('
				c.title,
				c.content,
				c.chart,
				c.type,
				c.id as questionID,
				c.correctAnswers,
				c.ordering
			'));
	        $query->from($db->quoteName('#__jclassroom_theresults','a'));
	        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.quizzID') . ')');
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
	        $query->where($db->quotename('a.quizzID').' = '.$db->quote($unitID));
	        $query->where('('.$db->quotename('c.type').' <> '.$db->quote(1).' AND '.$db->quotename('c.type').' <> '.$db->quote(14).')');
	        $query->order('c.ordering asc ');
	        $query->group('c.id');
			$db->setQuery($query);
			$questions = $db->loadObjectList();
			if($questions):
				foreach($questions as $question):
					$type 		= $quesion->type;
					$labels 	= array();
					$datas 		= array();
					$indiAnswers= '';
					if($question->chart && $question->chart != 'wordCloud'):
						// WW-Fragen
						if($question->type == 3):
							$cCount = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.title,a.id
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$titles     = strip_tags($answer->title);
					            $titles     = htmlspecialchars_decode($titles);
					            $labels[]   = $titles;
								$theReal = $this->getUsersResult($question->questionID, $answer->id, $classroomID, $unitPosID);
								$datas[] 	= $theReal['users'];
								$cCount 	+= $theReal['users'];
							endforeach;
							if($question->correctAnswers == 2):
								$theRealActives = $this->getTheActiveUsersOfQuestion($question->questionID, $answer->id, $classroomID, $unitPosID, 3);
								$cCount  	= $theRealActives;
							endif;
						endif;
						if($question->type == 4):
							//echo $classroomID.' '.$unitPosID.'<br/>';
							$answersIS = 0;
							$labels 	= array('Sehr schlecht', 'Schlecht','Nicht schlecht','Gut','Sehr gut');
							$cCount = 0;
							for($i = 1;$i <= 5;$i++) {
								$query = $db->getQuery(true);
								$query->select(array('
									count(b.answerID) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quotename('b.answerID').' = '.$db->quote($i));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								if($answersIS):
									$datas[] 	= $answersIS;
									$cCount += $answersIS;
								else:
									$datas[] 	= 0;
								endif;
							}
						endif;
						// Ja/Nein-Fragen
						if($question->type == 5):
							$cCount = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as cJ
							'));
							$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
					        $query->where($db->quotename('a.content').' = '.$db->quote('answerJ'));
					        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
							$db->setQuery($query);
							$answersIS 	= $db->loadObject();
							$datas[] 	= $answersIS->cJ;
							$labels[] 	= 'Ja';
							$cCount += $answersIS->cJ;
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as cN
							'));
							$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
					        $query->where($db->quotename('a.content').' = '.$db->quote('answerN'));
					        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
							$db->setQuery($query);
							$answersIS 	= $db->loadObject();
							$datas[] 	= $answersIS->cN;
							$labels[] 	= 'Nein';
							$cCount += $answersIS->cN;
						endif;
						// Eingabefeld mit 1 Auswertung
						if($question->type == 7):
							$cCount = 0;
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= strip_tags($answer->title);
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
						        $query->where($db->quotename('b.content').' = '.$db->quote($answer->title));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
						        $query->order('a.created DESC');
						        $query->setLimit(1);
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
								$cCount 	+= $answersIS;
							endforeach;
							$labels[] 		= 'Andere Antwort';
							$theRealActives = $this->getTheActiveUsersOfQuestion($question->questionID, '', $classroomID, $unitPosID, 7);
							$cCount  	= $theRealActives;
						endif;
						// Auswahlliste
						if($question->type == 12):
							$cCount = 0;
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= strip_tags($answer->title);
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
						        $query->order('a.created DESC');
						        $query->setLimit(1);
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
								$cCount 	+= $answersIS;
							endforeach;
							if($question->correctAnswers == 2):
								$theRealActives = $this->getTheActiveUsersOfQuestion($question->questionID,'', $classroomID, $unitPosID,12);
								$cCount  	= $theRealActives;
							endif;
						endif;
						// Checkboxen
						if($question->type == 13):
							$cCount 	= 0;
							$answersIS 	= 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= $answer->title;
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
						        $query->order('a.created DESC');
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
								$cCount 	+= $answersIS;
							endforeach;
							if($question->correctAnswers == 2):
								$theRealActives = $this->getTheActiveUsersOfQuestion($question->questionID, $answer->id, $classroomID, $unitPosID, 13);
								$cCount  	= $theRealActives;
							endif;
						endif;
					endif;
					if($question->chart && $question->chart == 'wordCloud'):
						$query = $db->getQuery(true);
						$query->select(array('
							count(a.id) as data,a.content
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
				        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
				        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
				        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
				        if($question->type == 4):
				        	$query->group('a.answerID');
				        endif;
						$db->setQuery($query);
						$answersIS = $db->loadObjectList();
						if($answersIS):
							foreach($answersIS as $answer):
								$datas[] 	= array(
									'text' 		=> $answer->content, 
									'weight' 	=> $answer->data,
									'color'		=> '#ff3600'
								);
							endforeach;
						endif;
					endif;
					// If there is no charttype
					if($question->chart == '' || $question->chart == '0'):
						$cCount = 0;
						// WWM-Fragen
						if($question->type == 3):
							$cCount = 0;
							$query 	= $db->getQuery(true);
							$query->select(array('
								a.title,a.id
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= strip_tags($answer->title);
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as cc
								'));
								$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						        $query->where($db->quotename('a.answerID').' = '.$db->quote($answer->id));
						        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
						        $query->order('a.created DESC');
								$db->setQuery($query);
								$answersIS 	= $db->loadObject();
								if($answersIS):
									
									$datas[] 		= $answersIS->cc;
									$cCount 		+= $answersIS->cc;
								else:
									$datas[] 	= 0;
								endif;
								$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.strip_tags($answer->title).'</span> Anzahl: '.$answersIS->cc.'</p>'; 
							endforeach;
						endif;
						if($question->type == 4):
							$answersIS = 0;
							$labels 	= array('','Sehr schlecht', 'Schlecht','Nicht schlecht','Gut','Sehr gut');
							$cCount = 0;
							for($i = 1;$i <= 5;$i++) {
								$query = $db->getQuery(true);
								$query->select(array('
									count(b.answerID) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quotename('b.answerID').' = '.$db->quote($i));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								if($answersIS):
									$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.$labels[$i].'</span> Anzahl: '.$answersIS.'</p>'; 
									$datas[] 		= $answersIS;
									$cCount 		+= $answersIS;
								else:
									$datas[] 	= 0;
								endif;
							}
						endif;
						if($question->type == 5):
							$cCount = 0;
							$labels[] 	= array('Ja', 'Nein');
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as cJ
							'));
							$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
					        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
					        $query->where($db->quotename('a.content').' = '.$db->quote('answerJ'));
							$db->setQuery($query);
							$db->execute();
							$answersISJ 	= $db->loadObject();
							if($answersISJ):
								$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">Ja</span> Anzahl: '.$answersISJ->cJ.'</p>'; 
								$cCount 		+= $answersISJ->cJ;
							else:
								$datas[] 	= 0;
							endif;
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as cN
							'));
							$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
					        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
					        $query->where($db->quotename('a.content').' = '.$db->quote('answerN'));
							$db->setQuery($query);
							$db->execute();
							$answersISN 	= $db->loadObject();
							if($answersISN):
								$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">Nein</span> Anzahl: '.$answersISN->cN.'</p>'; 
								$cCount 		+= $answersISN->cN;
							else:
								$datas[] 	= 0;
							endif;
						endif;
						// Eingabefeld mit 1 Auswertung
						if($question->type == 7):
							$cCount = 0;
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= strip_tags($answer->title);
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
						        $query->where($db->quotename('b.content').' = '.$db->quote($answer->title));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
						        $query->order('a.created DESC');
						        $query->setLimit(1);
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
								$cCount 	+= $answersIS;
							endforeach;
							$labels[] 		= 'Andere Antwort';
							$indiAnswers 	= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.strip_tags($answer->title).'</span> Anzahl: '.$answersIS.'</p>';
							$delta = $cCount - $answersIS; 
							$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">Andere Antwort</span> Anzahl: '.$delta.'</p>';
						endif;
						// Texteingabefeld or Textarea
						if($question->type == 10 || $question->type == 11):
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								group_concat(b.content SEPARATOR "<br/>") as indiAnswers,
								count(a.id) as cCount
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
					        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
					        $query->where('b.content <> ""');
							$db->setQuery($query);
							$answersIS = $db->loadObject();
							if($answersIS):
								$indiAnswers 	= $answersIS->indiAnswers;
							else:
								$indiAnswers 	= '';
							endif;
							$cCount = $answersIS->cCount;
						endif;
						// List
						if($question->type == 12):
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= html_entity_decode($answer->title);
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
						        $query->order('a.created DESC');
						        $query->setLimit(1);
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
								$cCount 	+= $answersIS;
								$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.$answer->title.'</span> Anzahl: '.$answersIS.'</p>'; 
							endforeach;
						endif;
						// Checkboxen
						if($question->type == 13):
							$cCount 	= 0;
							$answersIS 	= 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= $answer->title;
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
						        $query->order('a.created DESC');
						        $query->setLimit(1);
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
								$cCount 	+= $answersIS;
								$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.$answer->title.'</span> Anzahl: '.$answersIS.'</p>'; 
							endforeach;
						endif;
					endif;
					$theQuestions[] = array(
						'questionType' => $question->type,
						'questionID'=> $question->questionID,
						'question' 	=> $question->title,
						'content'	=> $question->content,
						'chart' 	=> $question->chart,
						'labels' 	=> $labels,
						'datas' 	=> $datas,
						'indiAnswers'	=> $indiAnswers,
						'cCount' 	=> $cCount,
						'type' 		=> $type
					);
				endforeach;
			endif;
		//endif;*/
		echo json_encode($theQuestions);
		exit();
	}
	function getUsersResult($questionID, $answerID, $classroomID, $unitPosID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.id
		'));
		$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
        $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
        $query->where($db->quotename('a.answerID').' = '.$db->quote($answerID));
        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
        $query->order('a.created DESC');
		$db->setQuery($query);
		$db->execute();
		$numRows 	= $db->getNumRows();
		$return 	= array('users' => $numRows);

		return $return;
	}
	function getTheActiveUsersOfQuestion($questionID, $answerID, $classroomID, $unitPosID, $type) {
		//echo $questionID.' '.$answerID.' '.$classroomID.' '.$unitPosID.'<br>';
		// Calculate the active students
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.id
		'));
		$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
        $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
        if($answerID):
        	$query->where($db->quotename('a.answerID').' = '.$db->quote($answerID));
        endif;
        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
        if($type == 12 || $type == 13):
        	$query->group('a.created_by');
        endif;
        $query->order('a.created DESC');
		$db->setQuery($query);
		$db->execute();
		$numRows 	= $db->getNumRows();

		return $numRows;
	}
	function printResult() {
		$input 	= JFactory::getApplication()->input;
		$rID 	= $input->get('theResultID', 0, 'INT');
		$completeEvaluation = $input->get('completeEvaluation', 0, 'INT');
		$classroomID = $input->get('classroomID', 0, 'INT');
		$unitID = $input->get('unitID', 0, 'INT');
      	require_once JPATH_SITE. '/components/com_jclassroom/library/quizz.php';
      	$printResult = new printQuizzEngine();
      	//printQuizz($rID, $save, $fromLR, $completeEvaluation, $classroomID, $publishedQuizzID, $unitID)
      	$printResult->printQuizz($rID, 1, 1,$completeEvaluation,$classroomID,0, $unitID);
      	echo '/images/evaluates/Quizzreport'.$rID.'.pdf';
		exit();
	}
	function deleteTheFolder() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id','','STR');
		$pathFL 	= JPATH_SITE.'/images/evaluates/LR'.$input->get('classroomID',0,'INT').'unit'.$input->get('unitID',0,'INT');
		JFolder::delete($pathFL);
		echo 'OK';
		exit();
	}
	function saveChartImage() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id','','STR');
		$pathFL 	= JPATH_SITE.'/images/evaluates/LR'.$input->get('classroomID',0,'INT').'unit'.$input->get('unitID',0,'INT');
		JFolder::create($pathFL);
		$data = $input->get('image','','RAW');
		echo $data;
		//list($type, $data) = explode(';', $data);
		//list(, $data)      = explode(',', $data);
		$base64Image = trim($data);
    	$base64Image = str_replace('data:image/png;base64,', '', $base64Image);
		$data = base64_decode($base64Image);
		echo $data;
		file_put_contents($pathFL.'/'.$id.'.png', $data);
		echo 'OK';
		exit();
	}
	// Load result for Stage Quizz
	function loadQuizzStageCompleteResult() {
		$input 	= JFactory::getApplication()->input;
		// Load the quizz to get the chart
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($input->get('quizzResultID',0,'INT'));
		$chartID 	= $table->chart;
		
		//if($chartID == 2 || $chartID == 3 || $chartID == 4):
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(array('
				c.title,
				c.chart,
				c.type,
				c.id as questionID
			'));
	        $query->from($db->quoteName('#__jclassroom_theresults','a'));
	        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.quizzID') . ')');
	        $query->where($db->quotename('a.quizzID').' = '.$db->quote($input->get('quizzResultID',0,'INT')));
	        $query->group('c.id');
			$db->setQuery($query);
			$questions = $db->loadObjectList();
			if($questions):
				foreach($questions as $question):
					$labels 	= array();
					$datas 		= array();
					if($question->chart && $question->chart != 'wordCloud'):
						if($question->type == 3 || $question->type == 5):
							$query = $db->getQuery(true);
							$query->select(array('
								a.title,a.id
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= $answer->title;
								$query = $db->getQuery(true);
								$query->select(array('
									a.id
								'));
								$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
						        $query->where($db->quotename('a.answerID').' = '.$db->quote($answer->id));
								$db->setQuery($query);
								$db->execute();
								$numRows = $db->getNumRows();
								$answersIS 	= $db->loadObject();
								$datas[] 	= $numRows;
							endforeach;
						endif;
						if($question->type == 13):
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= $answer->title;
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
						        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						        $query->where($db->quotename('a.answerID').' = '.$db->quote($answer->id));
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
							endforeach;
						endif;
					endif;
					if($question->chart && $question->chart == 'wordCloud'):
						$query = $db->getQuery(true);
						$query->select(array('
							count(a.id) as data,a.content
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
				        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersIS = $db->loadObjectList();
						if($answersIS):
							foreach($answersIS as $answer):
								$datas[] 	= array(
									'text' 		=> $answer->content, 
									'weight' 	=> $answer->data,
									'color'		=> '#ff3600'
								);
							endforeach;
						endif;
					endif;
					$theQuestions[] = array(
						'questionID' => $question->questionID,
						'question' => $question->title,
						'chart' => $question->chart,
						'labels' => $labels,
						'datas' => $datas
					);
				endforeach;
			endif;
		//endif;
		echo json_encode($theQuestions);
		exit();
	}
	function loadSingleChart() {
		$input 		= JFactory::getApplication()->input;
		$theResultID= $input->get('theResultID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table   = JTable::getInstance('Theresults','JclassroomTable',array());
		$table->load($theResultID);
		// Load the Quizzdata
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$unit   = JTable::getInstance('Unit','JclassroomTable',array());
		$unit->load($table->quizzID);
		// The Result in details
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.classroomID,
			a.quizzID,
			a.created as resultCreated,
			a.created_by as resultBy,
			a.id as resultID,
			c.title as quizzTitle,
			d.id as questionID,
			d.title as questionTitle,
			d.type as questionType,
			d.points
		'));
		$query->from($db->quoteName('#__jclassroom_theresults','a'));
		//$query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_units', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'd') . ' ON (' . $db->quoteName('c.id') . ' = ' . $db->quoteName('d.quizzID') . ')');
		$query->where($db->quotename('a.id').' = '.$db->quote($theResultID));
		//$query->where($db->quotename('b.questionID').' = '.$db->quote(108));
		$query->order('d.ordering ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$sumMax = 0;

      	foreach($result as $item):
         	$query   = $db->getQuery(true);
	        if(
	         	$item->questionType == 3 || 
	         	$item->questionType == 5 || 
	         	$item->questionType == 7 || 
	         	$item->questionType == 12 || 
	         	$item->questionType == 13):
	            switch($item->questionType):
	               	case 13:
						$query->select(array('a.id,a.title,a.correct'));
						$query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
						$query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
						break;
	               	case 3:
						$query->select(array('a.id,a.title,a.correct'));
						$query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
						$query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
						break;
	               	case 5:
						$query->select(array('a.id,a.title,a.correctAnswers as correct'));
						$query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
						$query->where($db->quotename('a.id').' = '.$db->quote($item->questionID));
						break;
					case 7:
						$query->select(array('a.id,a.title,a.correct'));
						$query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
						$query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
						break;
					case 12:
						$query->select(array('a.id,a.title,a.correct'));
						$query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
						$query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
						break;
	            endswitch;
	            $db->setQuery($query);
	            $answersSoll = $db->loadObjectList();
            	if($answersSoll):
	               	$countCAS = 0;
	               	$countCAI = 0;
	               	foreach($answersSoll as $answer):
	                  	$correct = 0;
	                  	if($item->questionType == 5):
		                    $countCAS++;
	                  	endif;
	                  	if($item->questionType == 7):
	                  		$correct = $answer->title;
		                    $countCAS++;
	                  	endif;
	                  	if($item->questionType == 3 || $item->questionType == 12 || $item->questionType == 13):
		                  	if($answer->correct == 1):
		                    	$correct = 1;
		                     	$countCAS++;
		                  	endif;
		                 endif;
	                  	$db = JFactory::getDbo();
						$query = $db->getQuery(true);
						$query->select(array('
							a.answerID,
							a.type,
							a.content
						'));
						$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						$query->where($db->quotename('a.theResultID').' = '.$db->quote($theResultID));
						$query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
						$db->setQuery($query);
						$as = $db->loadObjectList();
	                  	foreach($as as $answerIS):
                     		switch($answerIS->type):
                     			case 5:
                     				if($correct == 0 && $answerIS->content == 'answerJ'):
                     					$countCAI += 1;
                     				endif;
                     				if($correct == 1 && $answerIS->content == 'answerN'):
                     					$countCAI += 1;
                     				endif;
                     				break;
                     			case 7:
                     				if($correct == $answerIS->content):
                     					$countCAI += 1;
                     				endif;
                     				break;
                     			default:
                     				if(
                     					($item->questionType == $answerIS->type) && 
                     					$answer->id == $answerIS->answerID):
			                        	if($correct == 1):
			                           		$countCAI++;
			                        	endif;
			                        endif;
		                        	break;
		                    endswitch;
	                  	endforeach;
               		endforeach;
            	endif;
         	endif;
         	if(
	         	$item->questionType == 3 || 
	         	$item->questionType == 5 || 
	         	$item->questionType == 7 || 
	         	$item->questionType == 12 || 
	         	$item->questionType == 13):
				if($unit->calculate == 2):
					echo $item->questionType.' '.$points.' '.$sumReal.'<br>';
					$sumMax  += $item->points;	
					$points  = ($countCAI * $item->points) / $countCAS;
					$sumReal += $points;
				endif;
			endif;
      	endforeach;
      	echo $sumReal.' '.$sumMax;
      	$labels[] = 'Richtig';
      	$labels[] = 'Falsch';
      	$datas[] = round(($sumReal * 100) / $sumMax);
      	$datas[] = round((($sumMax - $sumReal) * 100) / $sumMax);
      	$return = array(
      		'labels' => $labels,
      		'datas' => $datas,
      		'chart' => 'pie',
      		'calculate' => $unit->calculate
      	);
      	echo json_encode($return);
      	exit();
	}
	function getQuizzData() {
		$input 	= JFactory::getApplication()->input;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_units','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('unitID',0,'INT')));
		$db->setQuery($query);
		$result = $db->loadObject();
		echo json_encode($result);
		exit();
	}
	function deleteQuizzResult() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Quizz_results','JclassroomTable',array());
		$table->delete($input->get('quizzResultID',0,'INT'));
		echo 'OK';
		exit();
	}
	// Scripts for sorting something
	function writeSortableTimeblocks() {
		$input = JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		$classroomID= $input->get('classroomID', 0,'INT');
		if($orders):
			$i = 1;
			foreach($orders as $order):
				$themeID 	= substr($order,5);
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_timeblocks_themes','JclassroomTable',array());
				$load = array('classroomID' => $classroomID, 'themeID' => $themeID);
				$table->load($load);
				$table->ordering 	= $i;
				$table->store();
				$i++;
			endforeach;
		endif;
		//$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	function writeSortableThemes() {
		$input = JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		$classroomID= 0;
		if($orders):
			$i = 1;
			foreach($orders as $order):
				$dayID 	= substr($order,7);
				echo $dayID;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_days','JclassroomTable',array());
				$table->load($dayID);
				$table->ordering 	= $i;
				$table->store();
				$classroomID = $table->classroomID;
				$i++;
			endforeach;
		endif;
		$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	function writeSortableUnits() {
		$input = JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		$classroomID= 0;
		if($orders):
			foreach($orders as $order):
				$unitID 	= explode('_', $order);
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$table->load($unitID[1]);
				$table->ordering 	= $unitID[0];
				$table->store();
				$classroomID = $table->classroomID;
			endforeach;
		endif;
		$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	function writeSortableModules() {
		$input 		= JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		$classroomID= 0;
		if($orders):
			foreach($orders as $order):
				$unitID 	= explode('_', $order);
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_modules','JclassroomTable',array());
				$table->load($unitID[1]);
				$table->ordering 	= $unitID[0];
				$table->store();
				$classroomID = $table->classroomID;
			endforeach;
		endif;
		$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	function reorderUnits($classroomID) {
		$db = JFactory::getDbo();
		$i = 1;
		$query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		foreach($days as $day):
			// Load the modules
			$query = $db->getQuery(true);
			$query->select(array('a.id'));
	        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
	        $query->where($db->quotename('a.dayID').' = '.$db->quote($day->id));
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->order('a.ordering ASC');
			$db->setQuery($query);
			$modules = $db->loadObjectList();
			foreach($modules as $module):
				$query = $db->getQuery(true);
				$query->select(array('a.id'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		        $query->where($db->quotename('a.moduleID').' = '.$db->quote($module->id));
		        $query->order('a.ordering ASC');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				foreach($result as $item):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
					$table->load($item->id);
					$table->ordering = $i;
					$table->store();
					$i++;
				endforeach;
			endforeach;
		endforeach;
	}
	// Scripts for moving something
	function moveDayToDay() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$dayToMove 		= $input->get('dayToMove', 0, 'INT');
		$moveDayToDay 	= $input->get('moveDayToDay', 0, 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->load($dayToMove);
		$table->day 		= date('Y-m-d', strtotime($moveDayToDay));
		$table->dayID		= date('d', strtotime($moveDayToDay));
		$table->store();

		echo date('d.m.Y', strtotime($moveDayToDay));
		exit();
	}
	function moveModuleToDay() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$moduleToMove 		= $input->get('moduleToMove', 0, 'INT');
		$moveModuleToDay 	= $input->get('moveModuleToDay', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$table->load($moduleToMove);
		$table->dayID		= $moveModuleToDay;
		$table->store();

		echo 'OK';
		exit();
	}
	function moveUnitToModule() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$unitID 	= $input->get('unitToMove', 0, 'INT');
		$newModuleID 	= $input->get('newModuleID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$table->load($unitID);
		$table->moduleID 	= $newModuleID;
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by = $user->id;
		$table->store();

		echo 'OK';
		exit();
	}
	function deleteStudent() {
		$input 	= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$studentID 		= $input->get('studentID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$load 	= array('classroomID' => $classroomID, 'userID' => $studentID);
		$table->load($load);
		$studentID = $table->id;
		$table->delete($studentID);

		echo $result;
		exit();
	}
	// Checksystem for Emails
	function checkTemplateExist() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$type 			= $input->get('template', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($classroomID);
		$return = 'NOK';
		if($type == 1):
			if($table->verification_template && $table->verification_template != 0):
				$return = 'OK';
			endif;
		endif;
		if($type == 2):
			if($table->email_template && $table->email_template != 0):
				$return = 'OK';
			endif;
		endif;
		if($type == 3):
			if($table->remember_template && $table->remember_template != 0):
				$return = 'OK';
			endif;
		endif;
		echo $return;
		exit();
	}
	function checkTemplates($type, $classroomID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_templates', 'b') . ' ON (' . $db->quoteName('a.verification_template') . ' = ' . $db->quoteName('b.id') . ')');
        /*$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('b.id') . ' = ' . $db->quoteName('c.user_id') . ')');
        $query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_students', 'e') . ' ON (' . $db->quoteName('e.tblUserID') . ' = ' . $db->quoteName('a.userID') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'f') . ' ON (' . $db->quoteName('e.companyID') . ' = ' . $db->quoteName('f.id') . ')');*/
        $query->where('b.type = '.$type);
		$query->where('a.id = '.$classroomID);
		$db->setQuery($query);
		$result = $db->loadObject();
		$return = $result;

		return $return;
	}
	function verificationToOne() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$studentID 		= $input->get('studentID', 0, 'INT');
		$session 		= JFactory::getSession();
		$return 		= '';
		// Check first, if templates are available
		$cT = $this->checkTemplates(2, $classroomID);
		if($cT):
			$db 	= JFactory::getDbo();
			$query 	= $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->where($db->quotename('a.userID').' = '.$db->quote($studentID));
			$db->setQuery($query);
			$student 	= $db->loadObject();
			$set = $this->vertifying($classroomID, $student);
			$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">';
			$return .= $set.'<br/>';
			$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
	    		<span aria-hidden="true">&times;</span>
	  			</button>';
			$return .= '</div>'; 
			$back = array(
				'success' => 1,
				'html' => $return,
				'created' => date('d.m.Y H:i:s')
			);
			echo json_encode($back);
		else:
			$return .= '<div id="message" class="alert alert-danger mt-2" role="alert" style="font-size: 14px;">';
			$return .= 'Kein Template für den Versand von Verifikationsmails gefunden. E-Mail(s) wurden nicht versendet.<br/>';
			$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
	    		<span aria-hidden="true">&times;</span>
	  			</button>';
			$return .= '</div>';
			$back = array(
				'success' => 0,
				'html' => $return,
				'created' => date('d.m.Y H:i:s')
			);
			echo json_encode($back);
		endif;
		exit();
	}
	function verificationToAll() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$students 	= $db->loadObjectList();
		$return 	= '';
		if($students):
			$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
			foreach($students as $student):
				$set = $this->vertifying($classroomID, $student);
				$return .= $set.'<br/>';
			endforeach;
			$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
			$return .= '</div>'; 
		endif; 
		$back = array(
			'html' => $return,
			'created' => date('d.m.Y H:i:s')
		);
		echo json_encode($back);
		exit();
	}
	function vertifying($classroomID, $student) {
		$user 	= JFactory::getUser();
		//Load the Student, if possible
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$studentU 	= JTable::getInstance('Student','JclassroomTable',array());
		$load 		= array('tblUserID' => $student->userID);
		$checkStudent = $studentU->load($load);
		if($checkStudent):
			$usergroup 		= 'student';
			$salutation 	= $studentU->salutation;
			$first_name 	= $studentU->first_name;
			$last_name 		= $studentU->last_name;
			$recipient 		= $studentU->email;
			$userID 		= $studentU->tblUserID;
		endif;

		//Load the Trainer, if possible
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainerU 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load 		= array('tblUserID' => $student->userID);
		$checkTrainer = $trainerU->load($load);
		if($checkTrainer):
			$usergroup = 'trainer';
			$salutation 	= $trainerU->salutation;
			$first_name 	= $trainerU->first_name;
			$last_name 		= $trainerU->last_name;
			$recipient 		= $trainerU->email;
			$userID 		= $trainerU->tblUserID;
		endif;
		//Load the Administrator, if possible
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$adminU 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$load 		= array('userID' => $student->userID);
		$checkAdmin = $adminU->load($load);
		if($checkAdmin):
			$usergroup = 'administrator';
			$salutation 	= $adminU->salutation;
			$first_name 	= $adminU->first_name;
			$last_name 		= $adminU->last_name;
			$recipient 		= $adminU->email;
			$userID 		= $adminU->tblUserID;
		endif;
		if(!$recipient): 
			//Load the User
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$userU 	= JTable::getInstance('User','JclassroomTable',array());
			$load 	= array('id' => $student->userID);
			$userU->load($load);
			$salutation 	= '';
			$first_name 	= '';
			$last_name 		= $userU->name;
			$recipient 		= $userU->email;
			$userID 		= $userU->tblUserID;
		endif;
		//Load the classroom
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$classroom 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$load 	= array('id' => $classroomID);
		$classroom->load($load);
		$verification_template = $classroom->verification_template;
		$customerID 	= $classroom->customerID;
		$testmode 		= $classroom->testmode;
		$email_testmode = $classroom->email_testmode;
		//Load the template
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.text,a.subject'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.id').' = '.$db->quote($verification_template));
		$db->setQuery($query);
		$template = $db->loadObject();
		$subject  = $template->subject;
		//Write to Emails
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Email','JclassroomTable',array());
		$data 		= array();
		$data['typ'] = 2;
		$data['send'] 	= date('Y-m-d H:i:s');
		$data['send_by'] 	= $user->id;
		$data['send_to'] 	= $userID;
		$data['classroomID'] = $classroomID;
		$table->bind($data);
		$table->store();
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$mailer = JFactory::getMailer();
		//$sender = array('training@ce.de','ceLearning');
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 0):
			$mailer->addRecipient($recipient);
			$email = $recipient;
		else:
			$mailer->addRecipient($email_testmode);
			$subject .= ' !TESTMODUS!';
			$email = $email_testmode;
		endif;
		//Load the day
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('Min(a.title) as first_day,MAX(a.title) as last_day'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$day 	= $db->loadObject();
		//Load the days
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$days 	= $db->loadObjectList();
		$all_days = '';
		if($days):
			foreach($days as $dayS1):
				$all_days .= date('d.m.Y', strtotime($dayS1->title)).'<br/>';
			endforeach;
		endif;
		//Load the trainer
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainer 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load = array('tblUserID' => $classroom->co_trainer);
		$trainer->load($load);
		$co_trainer = $trainer->salutation.' '.$trainer->first_name.' '.$trainer->last_name;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainer 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load 		= array('tblUserID' => $classroom->main_trainer);
		$check 		= $trainer->load($load);
		if($check):
			$main_trainer = $trainer->first_name.' '.$trainer->last_name;
		else:
			// Try to load a customeradmin as trainer
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 		= array('userID' => $classroom->main_trainer);
			$check2 	= $ST->load($load);
			if($check2):
				$main_trainer 	= $ST->first_name.' '.$ST->last_name;
			endif; 	
		endif;

		$template = str_replace('{first_name}',$first_name, $template->text);
		$template = str_replace('{last_name}',$last_name, $template);
		$template = str_replace('{salutation}',$salutation, $template);
		$template = str_replace('{email}',$email, $template);
		$template = str_replace('{classroomname}',$classroom->title, $template);
		$template = str_replace('{lr_name}',$classroom->title, $template);
		$template = str_replace('{main_trainer}',$main_trainer, $template);
		$template = str_replace('{co_trainer}',$co_trainer, $template);
		$template = str_replace('{first_day}',date('d.m.Y', strtotime($day->first_day)), $template);
		$template = str_replace('{last_day}',date('d.m.Y', strtotime($day->last_day)), $template);
		$template = str_replace('{all_days}',$all_days, $template);
		$template = str_replace('{activationlink}', '<a style="color: #ff3600;" href="'.$systemLink.'/vertify?SMA='.base64_encode($userID).'">hier</a>', $template);
		$body 	= $template;
		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail an Teilnehmer '.$first_name.' '.$last_name.' ('.$email.') erfolgreich versendet.';
		}
		return $return;
	}
	function invitationToAll() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$students 	= $db->loadObjectList();
		$return 	= '';
		if($students):
			$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
			foreach($students as $student):
				$set = $this->invitation($classroomID, $student);
				$return .= $set.'<br/>';
			endforeach;
			$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
			$return .= '</div>'; 
		endif; 
		$back = array(
			'html' => $return,
			'created' => date('d.m.Y H:i:s')
		);
		echo json_encode($back);
		exit();
	}
	function invitationToOne() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$studentID 		= $input->get('studentID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('a.userID').' = '.$db->quote($studentID));
		$db->setQuery($query);
		$student 	= $db->loadObject();
		$return 	= '';
		$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
		$set = $this->invitation($classroomID, $student);
		$return .= $set.'<br/>';
		$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
		$return .= '</div>'; 
		$back = array(
			'html' => $return,
			'created' => date('d.m.Y H:i:s')
		);
		echo json_encode($back);
		exit();
	}
	function invitation($classroomID, $student) {
		$user 	= JFactory::getUser();
        $groups = JAccess::getGroupsByUser($student->userID);
        // Students
        if(in_array(11,$groups)) {
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Student','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        // Customer
        if(in_array(12,$groups)) {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 		= array('userID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        // Trainer
        if(in_array(10,$groups)) {
           	JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Trainer','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        if(!$recipient): 
			//Load the User
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$userU 	= JTable::getInstance('User','JclassroomTable',array());
			$load 	= array('id' => $student->userID);
			$userU->load($load);
			$salutation 	= '';
			$first_name 	= '';
			$last_name 		= $userU->name;
			$recipient 		= $userU->email;
		endif;
		//Load the classroom
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$classroom 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$classroom->load($classroomID);
		$invitationTemplate = $classroom->email_template;
		$customerID 	= $classroom->customerID;
		$testmode 		= $classroom->testmode;
		$email_testmode = $classroom->email_testmode;
		//Load the trainer
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainer 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load = array('tblUserID' => $classroom->co_trainer);
		$trainer->load($load);
		$co_trainer = $trainer->salutation.' '.$trainer->first_name.' '.$trainer->last_name;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainer 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load 		= array('tblUserID' => $classroom->main_trainer);
		$check 		= $trainer->load($load);
		if($check):
			$main_trainer = $trainer->first_name.' '.$trainer->last_name;
		else:
			// Try to load a customeradmin as trainer
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 		= array('userID' => $classroom->main_trainer);
			$check2 	= $ST->load($load);
			if($check2):
				$main_trainer 	= $ST->first_name.' '.$ST->last_name;
			endif; 	
		endif;
		//Load the template
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.text,a.subject'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.id').' = '.$db->quote($invitationTemplate));
		$db->setQuery($query);
		$template 	= $db->loadObject();
		$subject 	= $template->subject;
		//Load the day
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('Min(a.title) as first_day,MAX(a.title) as last_day'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroom->id));
		$db->setQuery($query);
		$day 	= $db->loadObject();
		//Load the days
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroom->id));
		$db->setQuery($query);
		$days 	= $db->loadObjectList();
		if($days):
			foreach($days as $dayS1):
				$all_days .= date('d.m.Y', strtotime($dayS1->title)).'<br/>';
			endforeach;
		endif;
		//Write to Emails
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Email','JclassroomTable',array());
		$data 		= array();
		$data['typ'] = 1;
		$data['send'] 	= date('Y-m-d H:i:s');
		$data['send_by'] 	= $user->id;
		$data['send_to'] 	= $student->userID;
		$data['classroomID'] = $classroomID;
		$table->bind($data);
		$table->store();
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		if($testmode == 0):
			$mailer->addRecipient($recipient);
			$email = $recipient;
		else:
			$mailer->addRecipient($email_testmode);
			$email = $email_testmode;
		endif;
		$template = str_replace('{first_name}',$first_name, $template->text);
		$template = str_replace('{salutation}',$salutation, $template);
		$template = str_replace('{last_name}',$last_name, $template);
		$template = str_replace('{email}',$email, $template);
		$template = str_replace('{classroomname}',$classroom->title, $template);
		$template = str_replace('{maintrainer}',$main_trainer, $template);
		$template = str_replace('{first_day}',date('d.m.Y', strtotime($day->first_day)), $template);
		$template = str_replace('{last_day}',date('d.m.Y', strtotime($day->last_day)), $template);
		$template = str_replace('{all_days}', $all_days, $template);
		$template = str_replace('{lr_name}',$classroom->title, $template);
		$template = str_replace('{main_trainer}',$main_trainer, $template);
		$template = str_replace('{co_trainer}',$co_trainer, $template);
		$template = str_replace('{activationlink}', '<a style="color: #ff3600;" href="'.$systemLink.'/vertify?SMA='.base64_encode($student->userID).'">hier</a>', $template);
		$body 	= $template;
		$subject = str_replace('{classroomname}',$classroom->title, $subject);
		$subject = str_replace('{first_day}',date('d.m.Y', strtotime($day->first_day)), $subject);

		$body 	= $template;
		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail an Teilnehmer '.$first_name.' '.$last_name.' (E-Mail: '.$email.') erfolgreich versendet.';
		}
		return $return;
	}
	function rememberToAll() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$students 	= $db->loadObjectList();
		$return 	= '';
		if($students):
			$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
			foreach($students as $student):
				$set = $this->remember($classroomID, $student);
				$return .= $set.'<br/>';
			endforeach;
			$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
			$return .= '</div>'; 
		endif; 
		$back = array(
			'html' => $return,
			'created' => date('d.m.Y H:i:s')
		);
		echo json_encode($back);
		exit();
	}
	function rememberToOne() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$studentID 		= $input->get('studentID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('a.userID').' = '.$db->quote($studentID));
		$db->setQuery($query);
		$student 	= $db->loadObject();
		$return 	= '';
		$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
		$set = $this->remember($classroomID, $student);
		$return .= $set.'<br/>';
		$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
		$return .= '</div>'; 
		$back = array(
			'html' => $return,
			'created' => date('d.m.Y H:i:s')
		);
		echo json_encode($back);
		exit();
	}
	function remember($classroomID, $student) {
		$user = JFactory::getUser();
        $groups = JAccess::getGroupsByUser($student->userID);
        // Students
        if(in_array(11,$groups)) {
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Student','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        // Reseller
        if(in_array(12,$groups)) {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 		= array('userID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        // Trainer
        if(in_array(10,$groups)) {
           	JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Trainer','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        if(!$recipient): 
			//Load the User
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$userU 	= JTable::getInstance('User','JclassroomTable',array());
			$load 	= array('id' => $student->userID);
			$userU->load($load);
			$salutation 	= '';
			$first_name 	= '';
			$last_name 		= $userU->name;
			$recipient 		= $userU->email;
		endif;
		//Load the classroom
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$classroom 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$classroom->load($classroomID);
		$invitationTemplate = $classroom->remember_template;
		$customerID 		= $classroom->customerID;
		$testmode 		= $classroom->testmode;
		$email_testmode = $classroom->email_testmode;
		//Load the trainer
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainer 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load = array('tblUserID' => $classroom->co_trainer);
		$trainer->load($load);
		$co_trainer = $trainer->salutation.' '.$trainer->first_name.' '.$trainer->last_name;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainer 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load 		= array('tblUserID' => $classroom->main_trainer);
		$check 		= $trainer->load($load);
		if($check):
			$main_trainer = $trainer->first_name.' '.$trainer->last_name;
		else:
			// Try to load a customeradmin as trainer
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 		= array('userID' => $classroom->main_trainer);
			$check2 	= $ST->load($load);
			if($check2):
				$main_trainer 	= $ST->first_name.' '.$ST->last_name;
			endif; 	
		endif;
		//Load the template
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.text,a.subject'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.id').' = '.$db->quote($invitationTemplate));
		$db->setQuery($query);
		$template 	= $db->loadObject();
		$subject 	= $template->subject;
		//Load the day
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('Min(a.title) as first_day,MAX(a.title) as last_day'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroom->id));
		$db->setQuery($query);
		$day 	= $db->loadObject();
		//Load the days
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroom->id));
		$db->setQuery($query);
		$days 	= $db->loadObjectList();
		if($days):
			foreach($days as $dayS1):
				$all_days .= date('d.m.Y', strtotime($dayS1->title)).'<br/>';
			endforeach;
		endif;
		//Write to Emails
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Email','JclassroomTable',array());
		$data 		= array();
		$data['typ'] = 3;
		$data['send'] 	= date('Y-m-d H:i:s');
		$data['send_by'] 	= $user->id;
		$data['send_to'] 	= $student->userID;
		$data['classroomID'] = $classroomID;
		$table->bind($data);
		$table->store();
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$fireMail = 1;
		if(!$emailSender || !$emailSenderName):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
			$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
			$table->load($load);
			$emailSender = $table->wert;
			$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
			$table->load($load);
			$emailSenderName = $table->wert;
			
			$fireMail = 1;
		endif;
		if($fireMail == 1 && $template->text && $subject):
			$mailer = JFactory::getMailer();
			$sender = array($emailSender,$emailSenderName);
			$mailer->setSender($sender);
			if($testmode == 0):
				$mailer->addRecipient($recipient);
				$email = $recipient;
			else:
				$mailer->addRecipient($email_testmode);
				$email = $email_testmode;
			endif;
			$template = str_replace('{first_name}',$first_name, $template->text);
			$template = str_replace('{salutation}',$salutation, $template);
			$template = str_replace('{last_name}',$last_name, $template);
			$template = str_replace('{email}',$email, $template);
			$template = str_replace('{classroomname}',$classroom->title, $template);
			$template = str_replace('{maintrainer}',$main_trainer, $template);
			$template = str_replace('{first_day}',date('d.m.Y', strtotime($day->first_day)), $template);
			$template = str_replace('{last_day}',date('d.m.Y', strtotime($day->last_day)), $template);
			$template = str_replace('{all_days}', $all_days, $template);
			$template = str_replace('{lr_name}',$classroom->title, $template);
			$template = str_replace('{main_trainer}',$main_trainer, $template);
			$template = str_replace('{co_trainer}',$co_trainer, $template);
			$template = str_replace('{activationlink}', '<a style="color: #ff3600;" href="'.$systemLink.'/vertify?SMA='.base64_encode($student->userID).'">hier</a>', $template);
			$body 	= $template;
			$subject = str_replace('{classroomname}',$classroom->title, $subject);
			$subject = str_replace('{first_day}',date('d.m.Y', strtotime($day->first_day)), $subject);
			$mailer->setSubject($subject);
			$mailer->isHtml(true);
			$mailer->setBody($body);
			$send = $mailer->Send();
			if ( $send !== true ) {
			    return 'Error sending email: ';
			} else {
			    return 'Mail wurde erfolgreich an '.$first_name.' '.$last_name.' ('.$email.') versendet';
			}
		else:
			return "Keine Absenderdaten, Template oder Betreffdaten gefunden. E-Mail wurde nicht versendet.";
		endif;
	}
	function stageFile() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('File','JclassroomTable',array());
		$table->load($id);
		$table->published = 1;
		$table->store();
		echo 'Datei wurde freigegeben.';
		exit();
	}
	function unstageFile() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('File','JclassroomTable',array());
		$table->load($id);
		$table->published = 0;
		$table->store();
		echo 'Datei wurde versteckt.';
		exit();
	}
	public function newStudent() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		//First check if student-email exisit
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('User','JclassroomTable',array());
		$load = array('email' => $input->get('studentEmail',0,'STR'));
		$check = $table->load($load);
		/*if($check):
			echo 'Es gibt bereits einen Teilnehmer mit dieser E-Mail-Adresse.';
			exit();
		endif;*/
		$id 		= $input->get('classroomID', 0, 'INT');
		$studentSalutation   = $input->get('studentSalutation',0,'STR');
		$studentFirstname    = $input->get('studentFirstname',0,'STR');
		$studentLastname     = $input->get('studentLastname',0,'STR');
		$studentCompany      = $input->get('studentCompany',0,'STR');
		$studentAdress       = $input->get('studentAdress',0,'STR');
		$studentPostcode     = $input->get('studentPostcode',0,'STR');
		$studentCity         = $input->get('studentCity',0,'STR');
		$studentPhone        = $input->get('studentPhone',0,'STR');
		$studentEmail        = $input->get('studentEmail',0,'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Student','JclassroomTable',array());
		$data 		= array();
		$data['customerID'] = $session->get('customerID');
		$data['companyID'] 	= $input->get('studentCompanyID',0,'INT');
		$data['salutation'] = $studentSalutation;
		$data['first_name'] = $studentFirstname;
		$data['last_name'] 	= $studentLastname;
		$data['company'] 	= $studentCompany;
		$data['adress'] 	= $studentAdress;
		$data['postcode'] 	= $studentPostcode;
		$data['city'] 		= $studentCity;
		$data['phone'] 		= $studentPhone;
		$data['username'] 	= $studentEmail;
		$data['password'] 	= '';
		$data['email'] 		= $studentEmail;
		$data['created'] 	= strval($datum);
		$data['created_by'] = $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$userID = $table->tblUserID;
		$userID = $this->writeClassroom2User($data, $table->id, $password);
		$return 	= array(
			'userID' 	=> $userID,
			'badge' 	=> 'badge-success',
			'usergroup' => 'Teilnehmer',
			'userstate' => '<i class="fa fa-ban text-danger"></i>',
			'created' 	=> date('d.m.Y H:i')
		);
		//$password = JUserHelper::hashPassword('#mytraining1602');
		
		$this->writeClassroom2Student($id, $table->id, $userID);
		echo json_encode($return);
		exit();
	}
	public function chooseStudent() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
        $groups 	= JAccess::getGroupsByUser($user->id);
        // Students
        if(in_array(11,$groups)) {
        	$group 	= 'student';
        }
        // Customer
        if(in_array(12,$groups)) {
            $group 	= 'customer';
        }
        // Customeradmin
        if(in_array(13,$groups)) {
            $group 	= 'customeradmin';
        }
        // Trainer
        if(in_array(10,$groups)) {
            $group 	= 'trainer';
        }
        // Trainer
        if(in_array(8,$groups)) {
            $group 	= 'superuser';
        }
		$input 		= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$students 		= $input->get('students', '', 'ARRAY');
		$return = array();
		if($students):
			foreach($students as $student):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$st 		= JTable::getInstance('Student','JclassroomTable',array());
				$load 		= array('tblUserID' => $student);
				$checkST 	= $st->load($load);
				if($checkST):
					$studentID 		= $st->id;
					$studentName 	= $st->first_name.' '.$st->last_name;
					$email 			= $st->email;
					$studentType	= 'Teilnehmer';
				endif;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tr 		= JTable::getInstance('Trainer','JclassroomTable',array());
				$load 		= array('tblUserID' => $student);
				$checkTR 	= $tr->load($load);
				if($checkTR):
					$studentID 		= $tr->id;
					$studentName 	= $tr->first_name.' '.$tr->last_name;
					$email 			= $tr->email;
					$studentType	= 'Trainer';
				endif;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$ad 		= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
				$load 		= array('userID' => $student);
				$checkAD 	= $ad->load($load);
				if($checkAD):
					$studentID 		= $ad->id;
					$studentName 	= $ad->first_name.' '.$ad->last_name;
					$email 			= $ad->email;
					$studentType	= 'Administrator';
				endif;
				if(!$checkST && !$checkTR && !$checkAD):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$us 		= JTable::getInstance('User','JclassroomTable',array());
					$load 		= array('id' => $student);
					$checkUS 	= $us->load($load);
					if($checkUS):
						$studentID 		= $us->id;
						$studentName 	= $us->name;
						$email 			= $us->email;
						$studentType	= 'Systemadministrator';
					endif;
				endif;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
				$load = array('classroomID' => $classroomID, 'userID' => $student);
				$check = $table->load($load);
				if(!$check):
					$data 		= array();
					$data['classroomID'] 	= $classroomID;
					//$data['studentID'] 		= $studentID;
					$data['userID'] 		= $student;
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
 					$table->bind($data);
					$table->store();
				else:
					$table->modified 		= date('Y-m-d H:i:s');
					$table->modified_by 	= $user->id;
					$table->store();;
				endif;
				$return[] = array(
					'id' 		=> $studentID,
					'email' 	=> $email,
					'tblUserID' => $student,
					'name' 		=> $studentName,
					'studentType' => $studentType,
					'vertify' 	=> '<i class="fa fa-check text-success"></i>',
					'dateOfVertify' 	=> '',
					'dateOfInvitation' 	=> '',
					'dateOfRemember' 	=> '',
					'dateOfBooking' 	=> date('d.m.Y H:i')
				);
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	public function loadFileM() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$files  	= $input->files->get('inputMaterial');
		if($files): 
			foreach($files as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					//echo $filenameRaw.'<br/>';
					$filename	= '/images/learningrooms/LR'.$id.'/material/'.str_replace(' ','-',$filenameRaw);
					$savename	= str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
			        JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['unitID'] 		= '';
					$data['type'] 			= 'material';
					$data['folder'] 		= '';
					$data['filename'] 		= $savename;
					$data['path'] 			= $filename;
					$data['size'] 			= $file['size'];
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$fileID = $table->id;
				endif;
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Die Datei(en) wurden importiert'.'<br/>'.$message, 'Message');
		$this->setRedirect(JURI::Root().'classroom-edit?layout=material&id='.$id);
	}
	public function loadFileR() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$files  	= $input->files->get('inputReserve');
		if($files): 
			foreach($files as $file):
				if($file['name']):
					echo $file['name'];
					$filenameRaw 	= JFile::makeSafe($file['name']);
					echo $filenameRaw.'<br/>';
					$filename	= '/images/learningrooms/LR'.$id.'/reserve/'.str_replace(' ','-',$filenameRaw);
					$savename	= str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
			        JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['unitID'] 		= 0;
					$data['type'] 			= 'reserve';
					$data['folder'] 		= '';
					$data['filename'] 		= $savename;
					$data['path'] 			= $filename;
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 0;
					$table->bind($data);
					$table->store();
					$fileID = $table->id;
				endif;
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Die Datei(en) wurden importiert'.'<br/>'.$message, 'Message');
		$this->setRedirect(JURI::Root().'classroom-edit?layout=material&id='.$id);
	}
	public function loadFileS() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$studentID 	= $input->get('uploadStudentUserID', 0, 'INT');
		$files  	= $input->files->get('inputStudent');
		if($files): 
			foreach($files as $file):
				if($file['name']):
					echo $file['name'];
					$filenameRaw 	= JFile::makeSafe($file['name']);
					echo $filenameRaw.'<br/>';
					$filename	= '/images/learningrooms/LR'.$id.'/material_students/'.str_replace(' ','-',$filenameRaw);
					$savename	= str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
			        JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['unitID'] 		= 0;
					$data['studentID'] 		= $studentID;
					$data['type'] 			= 'student';
					$data['folder'] 		= '';
					$data['filename'] 		= $savename;
					$data['path'] 			= $filename;
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$fileID = $table->id;
				endif;
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Die Datei(en) wurden importiert'.'<br/>'.$message, 'Message');
		$this->setRedirect(JURI::Root().'classroom-edit?layout=material&id='.$id);
	}
	public function loadFile() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$unitID 	= $input->get('unitID','','STR');
		$file  		= $input->files->get('uploadUnit', array(), 'raw');
		if($file): 
			//foreach($files as $file):
				if($file['name']):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$fileID = $table->id;

					$filenameRaw 	= JFile::makeSafe($fileID.'_'.$file['name']);
					$filename	= '/images/learningrooms/LR'.$id.'/units/'.str_replace(' ','-',$filenameRaw);
					$savename	= str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file['tmp_name'], $dest, false, true);
					$table = JTable::getInstance('File','JclassroomTable',array());
			        $table->load($fileID);	
					$table->classroomID 	= $id;
					$table->unitID 		= $unitID;
					$table->type 			= 'unit';
					$table->folder 			= '';
					$table->filename 		= $savename;
					$table->path 			= $filename;
					$table->created 		= date('Y-m-d H:i:s');
					$table->created_by 		= $user->id;
					$table->published 		= 1;
					$table->store();
				endif;
			//endforeach;
		endif;
        echo $fileID.'__'.$filename;
        exit();
		//JFactory::getApplication()->enqueueMessage('Die Datei(en) wurden importiert'.'<br/>'.$message, 'Message');
		//$this->setRedirect(JURI::Root().'classroom-edit?layout=edit&id='.$id);
	}
	public function loadImageFile() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$unitID 	= $input->get('unitID','','STR');
		$file  		= $input->files->get('uploadImageUnit');
		if($file): 
			//foreach($files as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					//echo $filenameRaw.'<br/>';
					$filename	= '/images/learningrooms/LR'.$id.'/units/'.str_replace(' ','-',$filenameRaw);
					$savename	= str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
			        JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['unitID'] 		= $unitID;
					$data['type'] 			= 'imageUnit';
					$data['folder'] 		= '';
					$data['filename'] 		= $savename;
					$data['path'] 			= $filename;
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$fileID = $table->id;
				endif;
			//endforeach;
		endif;
        echo $fileID.'__'.$filename;
        exit();
		//JFactory::getApplication()->enqueueMessage('Die Datei(en) wurden importiert'.'<br/>'.$message, 'Message');
		//$this->setRedirect(JURI::Root().'classroom-edit?layout=edit&id='.$id);
	}
	public function deleteFile() {
		$input 		= JFactory::getApplication()->input;
		$fileID 	= $input->get('fileID','','INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('File','JclassroomTable',array());
		$table->load($fileID);
		$path = $table->path;
		$table->delete($fileID);
		unlink(JPATH_SITE.'/'.$path);
		echo str_replace('.','',$table->filename);
		exit();
	}
	public function deleteFileFD() {
		$input 		= JFactory::getApplication()->input;
		$path 		= $input->get('path','','STR');
		$id 		= $input->get('id','','INT');
		if($id):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('File','JclassroomTable',array());
			$table->load($id);
			$filename = $table->path;
			$table->delete($id);
		endif;
		unlink($path);
		echo 'OK';
		exit();
	}
	public function deleteFolderFD() {
		$input 		= JFactory::getApplication()->input;
		$fileID 	= $input->get('fileID','','INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('File','JclassroomTable',array());
		$table->load($fileID);
		$filename = $table->path;
		$table->delete($fileID);
		unlink(JPATH_SITE.'/'.$filename);
		echo $filename;
		exit();
	}
	public function testCSV() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$file  		= $input->files->get('uploadCSVT');
		$fileName   = $file['name'];
		$src        = $file['tmp_name'];
        $dest       = JPATH_SITE.'/images/jclassroom/import/'.$fileName;
        JFile::upload($src, $dest);
        
        if(($handle = fopen($dest, "r")) !== FALSE):
			$i = 1;
			while(($line = fgetcsv($handle, 0, ';', "\n")) !== false) {
				foreach($line as $key => $value) {
                    if ($i === 1) {
                        $keys[$key] = $value;
                    } else {
                        $out[$i][$key] = $value;
                    }
                }
                $i++;
			}
		endif;
		$html = '';
		$f1 = 0;
		if(count($keys) != 12):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Falsche Anzahl Datenfelder -> Soll = 12.<br/>';
			$f1 = 1;
		endif;
		/*if($keys[1] != 'Kunden-Nr.'):
			$html .= $kdnr.'<i class="fa fa-exclamation-circle"></i> Fehler: Feld Kunden-Nr. fehlt oder ist nicht an Position 2 (B)<br/>';
			$f1 = 1;
		endif;*/
		/*if($keys[0] != 'Anrede'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld Anrede fehlt oder ist nicht an Position 1 (A)<br/>';
			$f1 = 1;
		endif;*/
		if($keys[3] != 'Vorname'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld Vorname fehlt oder ist nicht an Position 4 (D)<br/>';
			$f1 = 1;
		endif;
		if($keys[4] != 'Nachname'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld Nachname fehlt oder ist nicht an Position 5 (E)<br/>';
			$f1 = 1;
		endif;
		/*if($keys[5] != 'Firma'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld Firma fehlt oder ist nicht an Position 6 (F)<br/>';
			$f1 = 1;
		endif;*/
		/*if($keys[3] != 'Adresse'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld Adresse fehlt oder ist nicht an Position 4 (D)<br/>';
			$f1 = 1;
		endif;
		if($keys[4] != 'PLZ'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld PLZ fehlt oder ist nicht an Position 5 (E)<br/>';
			$f1 = 1;
		endif;
		if($keys[5] != 'Ort'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld Ort fehlt oder ist nicht an Position 6 (F)<br/>';
			$f1 = 1;
		endif;
		if($keys[6] != 'Telefonnummer'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld Telefonnummer fehlt oder ist nicht an Position 7 (G)<br/>';
			$f1 = 1;
		endif;*/
		/*if($keys[10] != 'Mobilnummer'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld Mobilnummer fehlt oder ist nicht an Position 11 (K)<br/>';
			$f1 = 1;
		endif;*/
		if($keys[11] != 'E-Mail'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: Feld E-Mail fehlt oder ist nicht an Position 12 (L)<br/>';
			$f1 = 1;
		endif;
		if($out):
			foreach($out as $item):
				$return[] = array(
					'Nr.' 		=> $item[0],
					'Kunden-Nr.' 	=> $item[1], 
					'Anrede' 	=> $item[2],
					'Vorname' 	=> $item[3], 
					'Nachname' 	=> $item[4],
					'Firma' 		=> $item[5], 
					'Adresse' 		=> $item[6], 
					'PLZ' 			=> $item[7],
					'Ort' 			=> $item[8], 
					'Telefonnummer' => $item[9], 
					'Mobilnummer' 	=> $item[10],
					'E-Mail' 		=> $item[11]
				);
			endforeach;
		endif;
		if(!$return):
			JFactory::getApplication()->enqueueMessage('Keine importierbaren Zeilen gefunden (Zeilen in der CSV-Datei = 0)', 'Message');
			$this->setRedirect(JURI::Root().'classroom-edit?layout=students&id='.$id);
		endif;
		if($return):
			$c1 = 1;
			$c2 = 1;
			$c3 = 1;
			$c4 = 1;
			$c5 = 1;
			$c6 = 1;
			$c7 = 1;
			$c8 = 1;
			$c9 = 1;
			foreach($return as $item):
				/*if($item['Anrede'] == ''):
					$c1 = 0;
					$f1 = 1;
				endif;*/
				if($item['Vorname'] == ''):
					$c2 = 0;
					$f1 = 1;
				endif;
				if($item['Nachname'] == ''):
					$c3 = 0;
					$f1 = 1;
				endif;
				/*if($item['Adresse'] == ''):
					$c4 = 0;
					$f1 = 1;
				endif;
				if($item['PLZ'] == ''):
					$c5 = 0;
					$f1 = 1;
				endif;
				if($item['Ort'] == ''):
					$c6 = 0;
					$f1 = 1;
				endif;
				if($item['Telefonnummer'] == ''):
					$c7 = 0;
					$f1 = 1;
				endif;
				/*if($item['Mobilnummer'] == ''):
					$c8 = 0;
					$f1 = 1;
				endif;*/
				if($item['E-Mail'] == ''):
					$c8 = 0;
					$f1 = 1;
				endif;
			endforeach;
		endif;
		if($c1 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: In mind. einer Zeile ist das Feld Anrede nicht ausgefüllt oder konnte der Position 1 nicht zugeordnet werden.<br/>';
		endif;
		if($c2 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: In mind. einer Zeile ist das Feld Vorname nicht ausgefüllt oder konnte der Position 2 nicht zugeordnet werden.<br/>';
		endif;
		if($c3 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: In mind. einer Zeile ist das Feld Nachname nicht ausgefüllt oder konnte der Position 3 nicht zugeordnet werden.<br/>';
		endif;
		if($c4 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: In mind. einer Zeile ist das Feld Adresse nicht ausgefüllt oder konnte der Position 4 nicht zugeordnet werden.<br/>';
		endif;
		if($c5 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: In mind. einer Zeile ist das Feld PLZ nicht ausgefüllt oder konnte der Position 5 nicht zugeordnet werden.<br/>';
		endif;
		if($c6 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: In mind. einer Zeile ist das Feld Ort nicht ausgefüllt oder konnte der Position 6 nicht zugeordnet werden.<br/>';
		endif;
		if($c7 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: In mind. einer Zeile ist das Feld Telefonnummer nicht ausgefüllt oder konnte der Position 7 nicht zugeordnet werden.<br/>';
		endif;
		/*if($c8 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: In mind. einer Zeile ist das Feld Mobilnummer nicht ausgefüllt oder konnte der Position 11 nicht zugeordnet werden.<br/>';
		endif;*/
		if($c8 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Fehler: In mind. einer Zeile ist das Feld E-Mail nicht ausgefüllt oder konnte der Position 8 nicht zugeordnet werden.<br/>';
		endif;
		if($return):
			$html .= '<br/>';
			$html .= '<h2>Ergebnis der Importprüfung</h2>';
			$html .= '<table class="table table-striped">';
			$html .= '<thead>';
			$html .= '<tr>';
			foreach($keys as $key => $value):
				$html .= '<th>'.$value.'</th>';
			endforeach;
			$html .= '</tr>';
			$html .= '</thead>';
			foreach($return as $item):
				$html .= '<tr>';
				foreach($keys as $key => $value):
					$html .= '<td>'.$item[$value].'</td>';
				endforeach;
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
		endif;
		$html .= '<h2><i class="fa fa-lightbulb-o"></i> Hinweis: stellen Sie sicher, dass Sie die CSV-Datei im Format CSV UTF-8 exportiert haben, um Probleme mit Umlauten und Sonerzeichen zu vermeiden.</h2><br/>';
		if($f1 == 1):
			$html .= '<h2><i class="fa fa-exclamation-circle"></i> Kein Import möglich! Zuvor müssen die Fehler in der CSV-Datei beseitigt werden.</h2><br/>';
		else:
			$html .= '<h2><i class="fa fa-thumbs-up"></i> OK, Import möglich! Laden Sie die geprüfte CSV-Datei über den Button <i>Teilnehmer über CSV-Upload erfassen</i> auf den Server.</h2><br/>';
		endif;
		if($f1 == 1):
			$html .= '<br/>';
			$html .= '<h2>Beispiel korrekter Import</h2>';
			$html .= '<table class="table table-striped">';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<th>Anrede</th>';
			$html .= '<th>Vorname</th>';
			$html .= '<th>Nachname</th>';
			$html .= '<th>Adresse</th>';
			$html .= '<th>PLZ</th>';
			$html .= '<th>Ort</th>';
			$html .= '<th>Telefonnummer</th>';
			$html .= '<th>E-Mail</th>';
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tr>';
			$html .= '<td>Herr</td>';
			$html .= '<td>Max</td>';
			$html .= '<td>Mustermann</td>';
			$html .= '<td>Teststraße 1</td>';
			$html .= '<td>55667</td>';
			$html .= '<td>Teststadt</td>';
			$html .= '<td>0123/123456</td>';
			$html .= '<td>max@meinedomain.de</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>Herr</td>';
			$html .= '<td>Fred</td>';
			$html .= '<td>Mustermann</td>';
			$html .= '<td>Teststraße 2</td>';
			$html .= '<td>55667</td>';
			$html .= '<td>Teststadt</td>';
			$html .= '<td>0123/1234567</td>';
			$html .= '<td>fred@meinedomain.de</td>';
			$html .= '</tr>';
			$html .= '</table>';
		endif;
		JFactory::getApplication()->enqueueMessage($html, 'Message');
		$this->setRedirect(JURI::Root().'classroom-edit?layout=students&id='.$id);
	}
	public function loadCSV() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$file  		= $input->files->get('uploadCSV');
		$fileName   = $file['name'];
		$src        = $file['tmp_name'];
        $dest       = JPATH_SITE.'/images/jclassroom/import/'.$fileName;
        JFile::upload($src, $dest);
        $keys 		= array();
        if(($handle = fopen($dest, "r")) !== FALSE):
			$i = 1;
			while(($line = fgetcsv($handle, 0, ';', "\n")) !== false) {
				foreach($line as $key => $value) {
                    if ($i === 1) {
                    	if($key == 0):
                    		$theFirst = $value;
                    	endif;
                        $keys[$key] = $value;
                    } else {
                        $out[$i][$key] = $value;
                    }
                }
                $i++;
			}
		endif;
		$f1 	= 0;
		$fa0 	= 0;
		$html 	= '';
		if(count($keys) != 12):
			$html .= '<i class="fa fa-exclamation-circle"></i> Anzahl der Spalten ist nicht 12 (Gefunden: '.count($keys).')<br/>';
			$f1 = 1;
		endif;	
		/*if($keys[1] != 'Kunden-Nr.'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld B Kunden-Nr. nicht gefunden (Gefunden: '.$keys[1].')<br/>';
			$f1 = 1;
		endif;*/
		/*if($keys[0] != 'Anrede'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld A Anrede nicht gefunden (Gefunden: '.$keys[0].')<br/>';
			$f1 = 1;
		endif;*/
		if($keys[3] != 'Vorname'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld D Vorname nicht gefunden (Gefunden: '.$keys[3].')<br/>';
			$f1 = 1;
		endif;
		if($keys[4] != 'Nachname'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld E Nachname nicht gefunden (Gefunden: '.$keys[4].')<br/>';
			$f1 = 1;
		endif;
		/*if($keys[3] != 'Firma'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld D Firma nicht gefunden (Gefunden: '.$keys[3].')<br/>';
			$f1 = 1;
		endif;
		if($keys[6] != 'Adresse'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld G Adresse nicht gefunden (Gefunden: '.$keys[6].')<br/>';
			$f1 = 1;
		endif;
		if($keys[7] != 'PLZ'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld H PLZ nicht gefunden (Gefunden: '.$keys[7].')<br/>';
			$f1 = 1;
		endif;
		if($keys[8] != 'Ort'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld I Ort nicht gefunden (Gefunden: '.$keys[8].')<br/>';
			$f1 = 1;
		endif;
		if($keys[9] != 'Telefonnummer'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld J Telefonnummer nicht gefunden (Gefunden: '.$keys[9].')<br/>';
			$f1 = 1;
		endif;
		if($keys[10] != 'Mobilnummer'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld K Mobilnummer nicht gefunden (Gefunden: '.$keys[10].')<br/>';
			$f1 = 1;
		endif;*/
		if($keys[11] != 'E-Mail'):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld L E-Mail nicht gefunden (Gefunden: '.$keys[11].')<br/>';
			$f1 = 1;
		endif;
		if($f1 == 1):
			JFactory::getApplication()->enqueueMessage($html, 'Message');
			$this->setRedirect(JURI::Root().'classroom-edit?layout=students&id='.$id);
		endif;	
		if($out):
			foreach($out as $item):
				$return[] = array( 
					'nr' 	=> $item[0],
					'customer_number' 	=> $item[1],
					'salutation' 	=> $item[2],
					'first_name' 	=> $item[3], 
					'last_name' 	=> $item[4], 
					'company' 		=> $item[5], 
					'adress' 		=> $item[6], 
					'postcode' 		=> $item[7],
					'city' 			=> $item[8], 
					'phone' 		=> $item[9],
					'mobile' 		=> $item[10], 
					'email' 		=> $item[11]
				);
			endforeach;
		endif;
		if(!$return):
			JFactory::getApplication()->enqueueMessage('<i class="fa fa-exclamation-circle"></i> Keine importierbaren Zeilen gefunden (Zeilen in der CSV-Datei = 0)', 'Message');
			$this->setRedirect(JURI::Root().'classroom-edit?layout=students&id='.$id);
		endif;
		if($return):
			$c1 = 1;
			$c2 = 1;
			$c3 = 1;
			$c4 = 1;
			$c5 = 1;
			$c6 = 1;
			$c7 = 1;
			$c8 = 1;
			$c9 = 1;
			foreach($return as $item):
				/*if($item['salutation'] == ''):
					$c1 = 0;
				endif;*/
				if($item['first_name'] == ''):
					$c2 = 0;
				endif;
				if($item['last_name'] == ''):
					$c3 = 0;
				endif;
				/*if($item['adress'] == ''):
					$c4 = 0;
				endif;
				if($item['zip'] == ''):
					$c5 = 0;
				endif;
				if($item['city'] == ''):
					$c6 = 0;
				endif;
				if($item['phone'] == ''):
					$c7 = 0;
				endif;
				if($item['mobile'] == ''):
					$c8 = 0;
				endif;*/
				if($item['email'] == ''):
					$c9 = 0;
				endif;
			endforeach;
		endif;
		$f1 = 0;
		/*if($c1 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld A Anrede nicht ausgefüllt<br/>';
			$f1 = 1;
		endif;*/
		if($c2 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld D Vorname nicht ausgefüllt<br/>';
			$f1 = 1;
		endif;
		if($c3 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld E Nachname nicht ausgefüllt<br/>';
			$f1 = 1;
		endif;
		/*if($c4 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld G Adresse nicht ausgefüllt<br/>';
			$f1 = 1;
		endif;
		if($c5 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld H PLZ nicht ausgefüllt<br/>';
			$f1 = 1;
		endif;
		if($c6 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld I Ort nicht ausgefüllt<br/>';
			$f1 = 1;
		endif;
		if($c7 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld J Telefonnummer nicht ausgefüllt<br/>';
			$f1 = 1;
		endif;
		if($c8 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld K Mobilnummer nicht ausgefüllt<br/>';
			$f1 = 1;
		endif;*/
		if($c9 == 0):
			$html .= '<i class="fa fa-exclamation-circle"></i> Feld L E-Mail nicht ausgefüllt<br/>';
			$f1 = 1;
		endif;
		if($f1 == 1):
			JFactory::getApplication()->enqueueMessage($html, 'Message');
			$this->setRedirect(JURI::Root().'classroom-edit?layout=students&id='.$id);
		endif;
		if($return && $f1 == 0):
			$back = '';
			foreach($return as $item):
				$check = $this->checkPeople($item['email']);
				if($check == false) {
					// Check if email exist
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('User','JclassroomTable',array());
					$load = array('email' => $item['email']);
					$check = $table->load($load);
					if(!$check):
						//Load the classroom
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$cl 		= JTable::getInstance('Classroom','JclassroomTable',array());
						$cl->load($id);
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$table 		= JTable::getInstance('Student','JclassroomTable',array());
						$data 		= array();
						$data['customerID'] = $session->get('customerID');
						$data['companyID'] 	= $cl->companyID;
						$data['customer_number'] = $item['customer_number'];
						$data['salutation'] = $item['salutation'];
						$data['first_name'] = $item['first_name'];
						$data['last_name'] 	= $item['last_name'];
						$data['company'] 	= $item['company'];
						$data['adress'] 	= $item['adress'];
						$data['postcode'] 	= $item['postcode'];
						$data['city'] 		= $item['city'];
						$data['phone'] 		= $item['phone'];
						$data['mobile'] 	= $item['mobile'];
						$data['email'] 		= $item['email'];
						$data['created'] 	= strval($datum);
						$data['created_by'] = $user->id;
						$data['published'] 	= 1;
						$table->bind($data);
						$table->store();
						$userID = $this->writeClassroom2User($data, $table->id,'');
						$this->writeClassroom2Student($id, $table->id, $userID);
						$back .= 'Der Teilnehmer <b>'.$item['first_name'].' '.$item['last_name'].'</b> wurde importiert.<br/>';
					else:
						$back[] = 'Der Teilnehmer '.$item['first_name'].' '.$item['last_name'].' ist bereits registriert.';
					endif;
				} else {
					$back .= 'Der Teilnehmer <b>'.$item['first_name'].' '.$item['last_name'].' E-Mail: '.$item['email'].'</b> ist bereits im System vorhanden.<br/>';
				}
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage($back, 'Message');
		$this->setRedirect(JURI::Root().'classroom-edit?layout=students&id='.$id);
	}
	public function saveCSV() {
		$input 	= JFactory::getApplication()->input;
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*,b.*'));
       	$query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->join('LEFT', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('a.userID') . ' = ' . $db->quoteName('b.id') . ')');
       	$query->where('a.classroomID = ' . $db->quote($input->get('lr',0,'INT')));
		$db->setQuery((string)$query);
		$result = $db->loadObjectList();
		$pfad = JPATH_SITE.'/images/jclassroom/export/ceKnowImportMasterV1.csv';
		$file = fopen($pfad,'w');
		$header = array(
			'nr' 			=> 'Nr.',
			'customer_number' => 'Kunden-Nr.',
			'salutation' 	=> 'Anrede',
			'first_name' 	=> 'Vorname',
			'last_name' 	=> 'Nachname',
			'company'		=> 'Firma',
			'adress' 		=> 'Adresse',
			'postcode' 		=> 'PLZ',
			'city' 			=> 'Ort',
			'phone' 		=> 'Telefonnummer',
			'mobile' 		=> 'Mobilnummer',
			'email' 		=> 'E-Mail'
		);
		fputcsv($file, $header,';');
		/*if($result) {
			$ia = 1;
			foreach($result as $item) {
				$export = array(
					'nr' 				=> $ia,
					'customer_number' 	=> $item->customer_number,
					'salutation' 		=> $item->salutation,
					'first_name' 		=> $item->first_name,
					'last_name' 		=> $item->last_name,
					'company' 			=> $item->company,
					'adress' 			=> $item->adress,
					'postcode' 		=> $item->postcode,
					'city' 			=> $item->city,
					'phone' 			=> $item->phone_number,
					'mobile' 		=> $item->mobile_number,
					'email' 			=> $item->email,
				);
				fputcsv($file, $export,';');
				$ia++;
			}
		}
		fclose($file);*/
		$app = JFactory::getApplication();
		$app->redirect(JURI::root().'images/jclassroom/export/ceKnowImportMasterV1.csv');
	}
	function writeClassroom2User($studentData, $studentID, $password = false) {
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('User','JclassroomTable',array());
		$load = array('email' => $studentData['email']);
		$check = $table->load($load);
		if(!$check):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('User','JclassroomTable',array());
			$data = array();
			$data['name'] 		= $studentData['first_name'].' '.$studentData['last_name'];
			$data['username'] 	= $studentData['email'];
			$data['email'] 		= $studentData['email'];
			$data['password'] 	= '';
			$data['block'] 		= 0;
			$data['sendEmail'] 	= 0;
			$data['registerDate'] 	= date('Y-m-d H:i:s');
			$data['lastvisitDate'] 	= date('Y-m-d H:i:s');
			$data['activation'] 	= 0;
			$table->bind($data);
			$table->store();
			$tblUserID = $table->id;
			//Tabelle Usergroup-Map
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('Usergroup','JclassroomTable',array());
			$data = array();
			$data['user_id'] 	= $tblUserID;
			$data['group_id'] 	= 11;
			$table->bind($data);
			$table->store();
			// SAVE the user ID
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('Student','JclassroomTable',array());
			$table->load($studentID);
			$table->tblUserID = $tblUserID;
			$table->store();
		endif;
		return $tblUserID;
	}
	function writeClassroom2Student($classroomID, $studentID, $userID) {
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$load = array('classroomID' => $classroomID, 'studentID' => $studentID);
		$check = $table->load($load);
		if(!$check):
			$data 		= array();
			$data['classroomID'] 	= $classroomID;
			$data['studentID'] 		= $studentID;
			$data['userID'] 		= $userID;
			$data['created'] 		= date('Y-m-d H:i:s');
			$data['created_by'] 	= $user->id;
			$data['published'] 		= 1;
			$table->bind($data);
			$table->store();
		endif;
	}
	function checkPeople($email) {
		echo $email.'<br/>';
		$return 	= false;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('User','JclassroomTable',array());
		$load 		= array('email' => $email);
		$check 		= $table->load($load);
		if($check):
			$return = true;
		endif;
		echo $return.'<br/>';
		return $return;
	}
	function checkEmail() {
		$return = false;
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('User','JclassroomTable',array());
		$load 		= array('email' => $input->get('email' ,'', 'STR'));
		$check 		= $table->load($load);
		if($check):
			$return = true;
		endif;
		echo $return;
		exit();
	}
	function loadTab() {
		$input 		= JFactory::getApplication()->input;
		$tab 		= $input->get('tab','', 'STR');
		switch($tab) {
			case 'basic':
				$qresult = require_once(JPATH_COMPONENT.'/views/classroom/tmpl/basic.php');
				break;
		}
		echo $qresult;
		exit();
	}
	function getClassroomData($classroomID, $dayID) {
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.published = 1');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->cards = $result;
		// Load the modules
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.classroomID = '.$classroomID);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->modules = $result;
		// Load the units
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		$query->where('a.classroomID = '.$classroomID);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->units = $result;
		// Load the reservefiles
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_files','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.type = "reserve"');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->reserve = $result;
		// Load the regular Files
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_files','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.type = "unit"');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->unitFiles = $result;

		return $item;
	}
	function loadTheThemesOfClassroom() {
		$input 	= JFactory::getApplication()->input;
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.id, a.title'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$input->get('classroomID', 0, 'INT'));
		$query->where('a.published = 1');
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$return[] = array('type' => 'optiongroupS', 'title' => $item->title, 'id' => 0);
				$query = $db->getQuery(true);
				$query->select(array('a.id, a.title'));
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
				$query->where('a.classroomID = '.$input->get('classroomID', 0, 'INT'));
				$query->where('a.dayID = '.$item->id);
				$query->where('a.published = 1');
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				foreach($modules as $module):
					$return[] = array('type' => 'option', 'title' => $module->title, 'id' => $module->id);
				endforeach;
				$return[] = array('type' => 'optiongroupE', 'title' => $item->title, 'id' => 0);
			endforeach;
		endif;
		echo json_encode($return);

		exit();
	}
	function exportLearningroom() {
		$input 		= JFactory::getApplication()->input;
		$classroomID= $input->get('classroomID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$table->load($classroomID);
		$xml['learningroom'] = array(
			'title' => $table->title, 
			'description' => $table->description
		);
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.published = 1');
		$query->order('a.day asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
				$query->where('a.dayID = '.$item->id);
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				
				$days[] 	= array('day' => $item->day,'modules' => json_encode($modules));
			endforeach;
		endif;
		$xml['days']	= array($days);
		print_r($xml);
		//file_put_contents(JURI::Root().'images/learningrooms/LR'.$table->id.'.xml',$xml);
		echo $xml;
		exit();
	}
	function libraryLearningroom() {
		$session 	= JFactory::getSession();
		if($session->get('customerID')):
			$customerID = $session->get('customerID');
		else:
			$customerID = 0;
		endif;
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		// Load origin
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		// Create copy
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$lrCopy 	= JTable::getInstance('Classroom_library','JclassroomTable',array());
		$data 		= array();
		$data['customerID'] 	= $customerID;
		$data['companyID'] 		= 0;
		$data['title'] 			= $table->title;
		$data['description'] 	= $table->description;
		$data['main_trainer'] 	= 0;
		$data['co_trainer'] 	= 0;
		$data['fromDate'] 		= '0000-00-00';
		$data['toDate'] 		= '0000-00-00';
		$data['presentation'] 	= $table->presentation;
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$data['published'] 		= 1;
		$lrCopy->bind($data);
		$lrCopy->store();
		$newLearningRoomID = $lrCopy->id;
		//Create imagefolders for new LR
		$pathFL 	= JPATH_SITE.'/images/library/learningrooms/LR'.$newLearningRoomID;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');
		// Load the origin days
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$id);
		$query->order('a.day asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				// Create copy
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$dayCopy 	= JTable::getInstance('Classroom_days_library','JclassroomTable',array());
				$data 		= array();
				$data['customerID'] 	= $session->get('customerID');
				$data['classroomID'] 	= $newLearningRoomID;
				$data['title'] 			= $item->title;
				$data['description'] 	= $item->description;
				$data['day'] 			= $item->day;
				$data['dayID'] 			= 0;
				$data['ordering'] 		= $item->ordering;
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$data['published'] 		= 1;
				$dayCopy->bind($data);
				$dayCopy->store();
				$newDayID = $dayCopy->id;
				// Load origin modules
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
				$query->where('a.classroomID = '.$id);
				$query->where('a.dayID = '.$item->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				if($modules):
					foreach($modules as $module):
						// Create copy
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$moduleCopy 	= JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
						$data 		= array();
						$data['customerID'] 	= $session->get('customerID');
						$data['classroomID'] 	= $newLearningRoomID;
						$data['dayID'] 			= $newDayID;
						$data['title'] 			= $module->title;
						$data['description'] 	= $module->description;
						$data['ordering'] 		= $module->ordering;
						$data['created'] 		= date('Y-m-d H:i:s');
						$data['created_by'] 	= $user->id;
						$data['published'] 		= 1;
						$moduleCopy->bind($data);
						$moduleCopy->store();
						$newModuleID = $moduleCopy->id;
						// Load origin units
						$query = $db->getQuery(true);
						$query->select(array('a.*'));
				        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
						$query->where('a.moduleID = '.$module->id);
						$query->order('a.ordering asc');
						$db->setQuery($query);
						$units = $db->loadObjectList();
						if($units):
							foreach($units as $unit):
								// Create copy
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$unitCopy 	= JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
								$data 		= array();
								$data['title'] 			= $unit->title;
								$data['description'] 	= $unit->description;
								$data['moduleID'] 	= $newModuleID;
								$data['unitType'] 	= $unit->unitType;
								$data['ordering'] 	= $unit->ordering;
								$data['duration'] 	= $unit->duration;
								$data['content'] 	= $unit->content;
								$data['link'] 		= $unit->link;
								$data['quizzID'] 	= $unit->quizzID;
								$data['created'] 		= date('Y-m-d H:i:s');
								$data['created_by'] 	= $user->id;
								$data['published'] 		= 1;
								$unitCopy->bind($data);
								$unitCopy->store();
								$newUnitID = $unitCopy->id;
								// Copy files
								$query 	= $db->getQuery(true);
								$query->select('a.*');
						        $query->from($db->quoteName('#__jclassroom_files','a'));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
								$db->setQuery($query);
								$files = $db->loadObjectList();
								if($files):
									foreach($files as $file):
										$pathLibraryToMove = '/images/library/learningrooms/LR'.$newLearningRoomID.'/units/';
										JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
										$tableF = JTable::getInstance('File','JclassroomTable',array());
										$data = array();
										$data['type'] 		= $file->type;
										$data['classroomID']= $newLearningRoomID;
										$data['unitID'] 	= $newUnitID;
										$data['studentID'] 	= 0;
										$data['folder'] 	= '';
										$data['filename'] 	= $file->filename;
										$data['path'] 		= $pathLibraryToMove.$file->filename;
										$data['created'] 	= date('Y-m-d H:i:s');
										$data['created_by'] = $user->id;
										$data['published'] 	= $file->published;
										$tableF->bind($data);
										$tableF->store();
										copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.$file->filename);
									endforeach;
								endif;
							endforeach;
						endif;
					endforeach;
				endif;
			endforeach;
		endif;

		echo 'OK';
		exit();
	}
	function addTimeblock() {
		$session 	= JFactory::getSession();
		$input 	= JFactory::getApplication()->input;
		//Get the last ordering of timeblock
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.ordering'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
		$query->where('a.classroomID = '.$input->get('classroomID',0,'INT'));
		$query->order('a.ordering DESC');
		$query->setLimit(1);
		$db->setQuery($query);
		$lastOrdering = $db->loadResult();
		//Save the new timeblock
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks','JclassroomTable',array());
		$data 		= array();
		$data['customerID'] 	= $session->get('customerID');
		$data['classroomID'] 	= $input->get('classroomID',0,'INT');
		$data['title'] 		=	'[NEU]';
		$data['ordering'] 	= $lastOrdering + 1;
		$data['background_color'] 	= '#eee';
		$timeblocks->bind($data);
		$timeblocks->store();
		$id = $timeblocks->id;
		echo $id;
		exit();
	}
	function addThemeToTimeblock() {
		$input 			= JFactory::getApplication()->input;
		$timeblockID 	= $input->get('droppedTo','','STR');
		$themeID 	 	= $input->get('droppedID','','STR');
		//Get the last ordering of timeblock
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.ordering'));
        $query->from($db->quoteName('#__jclassroom_timeblocks_themes','a'));
		$query->where('a.classroomID = '.$input->get('classroomID',0,'INT'));
		$query->order('a.ordering DESC');
		$query->setLimit(1);
		$db->setQuery($query);
		$lastOrdering = $db->loadResult();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks_themes','JclassroomTable',array());
		$data 		= array();
		$data['classroomID'] 	= $input->get('classroomID',0,'INT');
		$data['timeblockID'] 	= substr($timeblockID,9);
		$data['themeID'] 		= substr($themeID,5);
		$data['ordering'] 	= $lastOrdering + 1;
		$timeblocks->bind($data);
		$timeblocks->store();
		$id = $timeblocks->id;
		echo $id;
		exit();
	}
	function deleteThemeFromTimeblock() {
		$input 			= JFactory::getApplication()->input;
		$timeblockID 	= $input->get('droppedTo','','STR');
		$themeID 	 	= $input->get('droppedID','','STR');
		//Get the last ordering of timeblock
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_timeblocks_themes','a'));
		$query->where('a.classroomID = '.$input->get('classroomID',0,'INT'));
		//$query->where('a.timeblockID = '.substr($timeblockID,9));
		$query->where('a.themeID = '.substr($themeID,5));
		$db->setQuery($query);
		$deleteID = $db->loadResult();
		echo $deleteID;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks_themes','JclassroomTable',array());
		$timeblocks->delete($deleteID);
		echo 'OK';
		exit();
	}
	function deleteTimeblock() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks','JclassroomTable',array());
		$timeblocks->delete($input->get('timeblockID',0,'INT'));
		echo 'OK';
		exit();
	}
	function saveEditTimeblock() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks','JclassroomTable',array());
		$timeblocks->load($input->get('timeblockID', 0, 'INT'));
		$timeblocks->title = date('Y-m-d', strtotime($input->get('dayDate', '', 'STR')));
		$timeblocks->background_color = $input->get('color', '', 'STR');
		$timeblocks->store();
		echo 'OK';
		exit();
	}
	function reorderRoom() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID',0,'INT');
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id,a.title'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->order('a.ordering ASC');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		echo '<pre>';
		if($days):
			$i = 1;
			foreach($days as $day):
				echo 'DAY: '.$day->title.'<br/>';
				$query = $db->getQuery(true);
				$query->select(array('a.id,a.title'));
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
				$query->where('a.classroomID = '.$classroomID);
				$query->where('a.dayID = '.$day->id);
				$query->order('a.ordering ASC');
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				if($modules):
					foreach($modules as $module):
						echo 'MODULE: '.$module->title.'<br/>';
						$query = $db->getQuery(true);
						$query->select(array('a.id,a.title'));
				        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
						$query->where('a.classroomID = '.$classroomID);
						$query->where('a.moduleID = '.$module->id);
						$query->order('a.ordering ASC');
						$db->setQuery($query);
						$units = $db->loadObjectList();
						if($units):
							
							foreach($units as $unit):
								echo 'Unit: '.$unit->title.'<br/>';
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$table 		= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
								$table->load($unit->id);
								$table->ordering 	= $i;
								$table->store();
								$i++;
							endforeach;
						endif;
					endforeach;
				endif;
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function saveComment() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$load 		= array('classroomID' => $input->get('classroomID',0,'INT'), 'userID' => $input->get('id',0,'INT'));
		$table->load($load);
		$table->comment = $input->get('comment', '', 'STR');
		$table->store();
		echo 'OK';
		exit();
	}
	function saveCertificate() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$load 		= array(
			'classroomID' => $input->get('classroomID',0,'INT'),
			'userID' => $input->get('id',0,'INT'));
		$table->load($load);
		$table->certificate = 1;
		$table->certificate_on = date('Y-m-d H:i:s');
		$table->store();
		echo date('d.m.Y H:i');
		exit();
	}
	function saveCertificateDe() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$load 		= array(
			'classroomID' => $input->get('classroomID',0,'INT'),
			'userID' => $input->get('id',0,'INT'));
		$table->load($load);
		$table->certificate = 0;
		$table->certificate_on = date('Y-m-d H:i:s');
		$table->store();
		echo date('d.m.Y H:i');
		exit();
	}
	function saveDayTitle() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->load($input->get('id',0,'INT'));
		$table->day = $input->get('date', '', 'STR');
		$table->title = $input->get('title', '', 'STR');
		$table->store();
		echo 'OK';
		exit();
	}
	function exportTemplate() {
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		JLoader::register('CopyLearningroomHelper',JPATH_COMPONENT_SITE.'/helpers/copyLearningroom.php');
      	$result     = new CopyLearningroomHelper();
      	$newLID    = $result->copyLearningroom($id);
		/*$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		$xml['classroom'] = array(
			'id' 			=> $table->id,
			'customerID'	=> $table->customerID,
			'companyID'		=> $table->companyID,
			'title' 		=> $table->title, 
			'description' 	=> $table->description,
			'main_trainer' 	=> $table->main_trainer,
			'co_trainer' 	=> $table->co_trainer,
			'showTo' 		=> $table->showTo,
			'showToUser' 	=> $table->showToUser,
			'email_template' 		=> $table->email_template,
			'verification_template' => $table->verification_template,
			'remember_template' 	=> $table->remember_template,
			'invitations'  	=> $table->invitations,
			'countRememberMails'  	=> $table->countRememberMails,
			'offsetRememberMails'  	=> $table->offsetRememberMails,
			'invitationsBy'  	=> $table->invitationsBy,
			'certificate'  		=> $table->certificate,
			'student_feedback'  	=> $table->student_feedback,
			'student_feedback_anonym'  	=> $table->student_feedback_anonym,
			'trainer_feedback'  	=> $table->trainer_feedback

		);
		$db = JFactory::getDbo();
		// Load the groups
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$id);
		$query->order('a.day asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$days[] 	= array(
					'id' 		=> $item->id,
					'classroomID' 	=> $item->classroomID,
					'day' 			=> $item->day,
					'title' 		=> $item->title,
					'dayID' 		=> $item->dayID
				);
			endforeach;
		endif;
		$xml['days']	= $days;
		// Load the units
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.classroomID = '.$id);
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$positionsU = array();
				$positionsF = array();
				// Load the units
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				$query->where('a.moduleID = '.$item->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$units = $db->loadObjectList();
				if($units):
					foreach($units as $unit):
						$positionsU[] 	= array(
							'id' 			=> $unit->id,
							'customerID' 	=> $unit->customerID,
							'classroomID' 	=> $unit->classroomID,
							'dayID' 		=> $unit->dayID,
							'moduleID' 		=> $unit->moduleID,
							'unitID' 		=> $unit->unitID,
							'unitType' 		=> $unit->unitType,
							'ordering' 		=> $unit->ordering,
							'duration' 		=> $unit->duration,
							'title' 		=> $unit->title,
							'content' 		=> $unit->content,
							'link' 			=> $unit->link,
							'quizzID' 		=> $unit->quizzID
						);
					endforeach;
				endif;
				// Load the files
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_files','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$positionsF[] 	= array(
							'type' 			=> $file->type,
							'classroomID' 	=> $file->classroomID,
							'unitID' 		=> $file->unitID,
							'studentID' 	=> $file->studentID,
							'folder' 		=> $file->folder,
							'filename' 		=> $file->filename,
							'path' 			=> $file->path
						);
					endforeach;
				endif;
				$modules[] 	= array(
					'id' 		=> $item->id,
					'customerID' 	=> $item->customerID,
					'classroomID' 	=> $item->classroomID,
					'dayID' 		=> $item->dayID,
					'title' 		=> $item->title,
					'description' 	=> $item->description,
					'ordering' 		=> $item->ordering,
					'units' 		=> $positionsU,
					'files' 		=> $positionsF
				);
				
			endforeach;
		endif;
		$xml['modules']	= $modules;
		$save = json_encode($xml);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_library','JclassroomTable',array());
		$data 		= array();
		$data['structure'] 		= $save;
		$data['companyID'] 		= 0;
		$data['title'] 			= $xml['classroom']['title'];
		$data['description'] 	= $xml['classroom']['description'];
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$data['published'] 		= 1;
		$table->bind($data);
		$table->store();
		$newLearningRoomID = $table->id;
		/*$rand = rand(100,10000);
		$pathSave = JPATH_SITE."/images/jclassroom/temp/xml_".$rand.".json";
		$downloadPath = JURI::Root()."images/jclassroom/temp/xml_".$rand.".json";
		file_put_contents($pathSave,$save);
		echo $downloadPath;*/
		echo $newLID;
		exit();
	}
	public function printStudentlist() {
      $input = JFactory::getApplication()->input;
      $id = $input->get('id','','INT');
      require_once JPATH_SITE. '/components/com_jclassroom/library/studentslist.php';
      $printAudit = new printList();
      $printAudit->studentlist($id);
   }
   	function deleteResult() {
   		$input 	= JFactory::getApplication()->input;
      	$rID 	= $input->get('rID','','INT');
      	$db = JFactory::getDbo();
		// Load the groups
        $query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
		$query->where('a.theResultID = '.$rID);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
				$table->delete($item->id);
			endforeach;
		endif;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Theresults','JclassroomTable',array());
		$table->delete($rID);
      	echo $rID;
      	exit();
   	}
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'classroom';

	protected $view_list = 'classrooms';

}
?>