<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerAccount extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input 		= JFactory::getApplication()->input;
		$id   		= $input->get('id','','INT');
		$usertype 	= $input->get('usertype','', 'STR');
		$this->editToDatabase($usertype);
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'manager-administrator';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer';
				break;
			case 'trainer':
				$retour = JURI::Root().'manager-trainer';
				break;
			case 'student':
				$retour = JURI::Root().'dashboard-students';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input 		= JFactory::getApplication()->input;
		$id   		= $input->get('id','','INT');
		$usertype 	= $input->get('usertype','', 'STR');
		$this->editToDatabase($usertype);
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'my-account';
				break;
			case 'trainer':
				$retour = JURI::Root().'my-account';
				break;
			case 'student':
				$retour = JURI::Root().'my-account';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function editToDatabase($usertype) {
		$input 		= JFactory::getApplication()->input;
		$customerID = $input->get('id',0,'INT');
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		switch($usertype):
			case 'student':
				echo $formData->getStr('salutation');
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Student','JclassroomTable',array());
				$table->load($formData->getInt('id'));
				$table->salutation 		= $formData->getStr('salutation');
				$table->first_name 		= $formData->getStr('first_name');
				$table->last_name 		= $formData->getStr('last_name');
				$table->company 	= $formData->getStr('company');
				$table->adress 		= $formData->getStr('adress');
				$table->postcode 	= $formData->getStr('postcode');
				$table->city		= $formData->getStr('city');
				$table->phone 		= $formData->getStr('phone');
				$table->fax 		= $formData->getStr('fax');
				$table->email		= $formData->getStr('email');
				$table->web 		= $formData->getStr('web');
				$table->password 	= $formData->getStr('password');
				$table->published 	= $formData->getInt('published');
				$table->modified	= date('Y-m-d');
				$table->modified_by	= $user->id;
				$table->store();
				
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('User','JclassroomTable',array());
				$table->load($input->get('userID',0,'INT'));
				$table->name 		= $formData->getStr('first_name').' '.$formData->getStr('last_name');
				$table->username 	= $formData->getStr('email');
				if($formData->getStr('password')):
					$password 			= JUserHelper::hashPassword($formData->getStr('password'));
					$table->password 	= $password;
				endif;
				$table->email 		= $formData->getStr('email');
				$table->store();
				break;
			case 'customer':
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Customer_administratoren','JclassroomTable',array());
				$table->load($formData->getInt('customerAdministratorID'));
				$table->salutation 		= $formData->getStr('salutation');
				$table->first_name 		= $formData->getStr('first_name');
				$table->last_name 		= $formData->getStr('last_name');
				$table->phone 			= $formData->getStr('phone');
				$table->fax 			= $formData->getStr('fax');
				$table->email 			= $formData->getStr('email');
				$table->store();
				$administratorUserID 	= $table->userID;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Customer','JclassroomTable',array());
				$table->load($formData->getInt('customerID'));
				$table->company_name 		= $formData->getStr('company_name');
				$table->address 		= $formData->getStr('address');
				$table->postcode 		= $formData->getStr('postcode');
				$table->city 			= $formData->getStr('city');
				$table->store();
				// Save alternate invoicedata, if checkbox is checked
				if($formData->getInt('alt_invoice') == 1):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
					$load 	= array('customerID' => $formData->getInt('customerID'));
					$check = $table->load($load);
					if($check):
						$table->alt_invoice 		= $formData->getInt('alt_invoice');
						$table->paymentID 			= $formData->getInt('paymentID');
						$table->invoice_first_name 		= $formData->getStr('invoice_first_name');
						$table->invoice_last_name 		= $formData->getStr('invoice_last_name');
						$table->invoice_function 		= $formData->getStr('invoice_function');
						$table->invoice_title 		= $formData->getStr('invoice_title');
						$table->invoice_adress 		= $formData->getStr('invoice_adress');
						$table->invoice_postcode 	= $formData->getInt('invoice_postcode');
						$table->invoice_city 		= $formData->getStr('invoice_city');
						$table->valid_from 		= date('Y-m-d', strtotime($formData->getStr('valid_from')));
						$table->valid_to 		= date('Y-m-d', strtotime($formData->getStr('valid_to')));
						$table->modified 		= date('Y-m-d H:i:s');
						$table->modified_by 	= $user->id;
						$table->store();
					else:
						$data = array();
						$data['customerID'] 	= $formData->getInt('customerID');
						$data['paymentID'] 		= $formData->getInt('paymentID');
						$data['alt_invoice'] 		= $formData->getInt('alt_invoice');
						$data['valid_from'] 		= date('Y-m-d',strtotime($formData->getStr('valid_from')));
						$data['valid_to'] 			= date('Y-m-d',strtotime($formData->getStr('valid_to')));
						$data['invoice_first_name'] 	= $formData->getStr('invoice_first_name');
						$data['invoice_last_name'] 		= $formData->getStr('invoice_last_name');
						$data['invoice_function'] 		= $formData->getStr('invoice_function');
						$data['invoice_title'] 		= $formData->getStr('invoice_title');
						$data['invoice_adress'] 	= $formData->getStr('invoice_adress');
						$data['invoice_postcode'] 	= $formData->getInt('invoice_postcode');
						$data['invoice_city'] 		= $formData->getStr('invoice_city');
						$data['created'] 	= date('Y-m-d H:i:s');
						$data['created_by']	= $user->id;
						$data['published'] 	= 1;
						$table->bind($data);
						$table->store();
					endif;
				else:
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
					$load 	= array('customerID' => $formData->getInt('customerID'));
					$check = $table->load($load);
					if($check):
						$table->alt_invoice = 0;
						$table->store();
					endif;
				endif;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('User','JclassroomTable',array());
				$table->load($administratorUserID);
				$table->name 		= $formData->getStr('first_name').' '.$formData->getStr('last_name');
				$table->username 	= $formData->getStr('email');
				if($formData->getStr('password')):
					$password 			= JUserHelper::hashPassword($formData->getStr('password'));
					$table->password 	= $password;
				endif;
				$table->email 		= $formData->getStr('email');
				$table->store();
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('Logs','JclassroomTable',array());
				$data = array();
				$data['userID'] 		= $user->id;
				$data['parameter'] 		= 'New Password';
				$data['wert'] 			= 'Der Benutzer '.$formData->getStr('first_name').' '.$formData->getStr('last_name').' ('.$formData->getStr('email').') hat sein Passwort neu vergeben.';
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$table->bind($data);
				$table->store();
				break;
			case 'trainer':
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('Trainer','JclassroomTable',array());
				$load 	= array('tblUserID' => $formData->getInt('id'));
				$table->load($load);
				$table->salutation 		= $formData->getStr('salutation');
				$table->first_name 		= $formData->getStr('first_name');
				$table->last_name 		= $formData->getStr('last_name');
				$table->adress 			= $formData->getStr('adress');
				$table->postcode 		= $formData->getStr('postcode');
				$table->city 			= $formData->getStr('city');
				$table->phone 			= $formData->getStr('phone');
				$table->email 		= $formData->getStr('email');
				$table->web 		= $formData->getStr('web');
				$table->store();
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('User','JclassroomTable',array());
				$table->load($input->get('userID',0,'INT'));
				$table->name 		= $formData->getStr('first_name').' '.$formData->getStr('last_name');
				$table->username 	= $formData->getStr('email');
				if($formData->getStr('password')):
					$password 			= JUserHelper::hashPassword($formData->getStr('password'));
					$table->password 	= $password;
				endif;
				$table->email 		= $formData->getStr('email');
				$table->store();
				break;
		endswitch;
		
		return $tblResellerID;
	}
	
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'student';

	protected $view_list = 'students';

}
?>