<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerLibrary_module extends JControllerForm
{
	function saveModule() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$moduleID 		= $input->get('moduleID', 0, 'INT');
		$title	 		= $input->get('title', '', 'STR');
		$description	= $input->get('description', '', 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
		$table->load($moduleID);
		$table->title 		= $title;
		$table->description = $description;
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by = $user->id;
		$table->store();
		echo 'OK';
		exit();
	}

	function saveUnit() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		$title	 		= $input->get('title', '', 'STR');
		$description	= $input->get('description', '', 'STR');
		$duration	 	= $input->get('duration', 0, 'INT');
		$content	 	= $input->get('content', '', 'RAW');
		$link	 		= $input->get('link', '', 'STR');
		$quizz	 		= $input->get('quizz', '', 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
		$table->load($unitID);
		$table->title 		= $title;
		$table->description = $description;
		$table->duration 	= $duration;
		$table->content 	= $content;
		$table->link 		= $link;
		$table->quizzID 	= $quizz;
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		echo 'OK';
		exit();
	}
	function deleteUnit() {
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
		$table->delete($unitID);
		echo 'OK';
		exit();
	}
	// Scripts for sorting something
	function writeSortableUnits() {
		$input = JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		if($orders):
			foreach($orders as $order):
				$unitID 	= explode('_', $order);
				echo $unitID[1];
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
				$table->load($unitID[1]);
				$table->ordering 	= $unitID[0];
				$table->store();
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function getTemplate() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$typ 			= $input->get('typ', 0, 'INT');
		$classroomID 	= $input->get('classroomID', '', 'INT');
		$dayID 			= $input->get('dayID', '', 'STR');
		$moduleID 		= $input->get('moduleID', '', 'INT');
		$unitCounter 	= $input->get('unitCounter', 0, 'INT');
		$library 		= $input->get('library', 0, 'INT');
		$unitID 		= $input->get('unitID', 0, 'INT');
		if($library == 0):
			//Save Unit 
			// 1. Load Max Ordering 
			$db 	= JFactory::getDbo(); 
			$query 	= $db->getQuery(true);
			$query->select('max(a.ordering)');
	        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
	        $query->where($db->quotename('a.moduleID').' = '.$db->quote($moduleID));
			$db->setQuery($query);
			$nextOrder = $db->loadResult();
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
			$data = array();
			$data['classroomID'] = $classroomID;
			$data['dayID'] 		= $dayID;
			$data['moduleID'] 	= $moduleID;
			$data['unitID'] 	= $typ;
			$data['unitType'] 	= $typ;
			$data['content'] 	= '';
			$data['ordering'] 	= $nextOrder + 1;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by'] = $user->id;
			$data['published']	= 1;
			$table->bind($data);
			$table->store();
			$unitID = $table->id;
		endif;
		JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
        $template = new UnitsHelper();
        $template = $template->getTemplate('lib', $classroomID, $dayID, $typ, '', '', $unitID);

        echo $template;
        exit();
	}
}
?>