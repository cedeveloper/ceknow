<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

class JclassroomControllerPlans extends JControllerAdmin
{
	public function delete() {
		$input = JFactory::getApplication()->input;
		$id = $input->get('id', 0, 'INT');
		$delete = $input->get('cid',array(), 'array');
		if($delete) {
			foreach($delete as $del) {
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Plan','JclassroomTable',array());
				$table->delete($del);	
			}
		}
		JFactory::getApplication()->enqueueMessage('Die Datensätze wurden gelöscht', 'Message');
		$this->setRedirect(JURI::Root().'manager-administrator/plans');
	}
	public function loadCSV() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$file  		= $input->files->get('uploadCSV');
		$fileName   = $file['name'];
		$src        = $file['tmp_name'];
        $dest       = JPATH_BASE.'/images/jclassroom/import/'.$fileName;
        JFile::upload($src, $dest);
        if(($handle = fopen($dest, "r")) !== FALSE):
			$i = 1;
			while(($line = fgetcsv($handle, 0, ';', "\n")) !== false) {
				foreach($line as $key => $value) {
                    if ($i === 1) {
                        $keys[$key] = $value;
                    } else {
                        $out[$i][$key] = $value;
                    }
                }
                $i++;
			}
		endif;
		if($out):
			foreach($out as $item):
				$return[] = array(
					'customer_number' 	=> $item[0], 
					'first_name' 	=> $item[1], 
					'last_name' 	=> $item[2],
					'company' 		=> $item[3], 
					'adress' 		=> $item[4], 
					'postcode' 		=> $item[5], 
					'city' 			=> $item[6], 
					'phone' 		=> $item[7], 
					'mobile' 		=> $item[8],
					'email' 		=> $item[9]
				);
			endforeach;
		endif;
		if($return):
			$message = '';
			foreach($return as $item):
				$check = $this->checkPeople($item['email']);
				if($check == false && $item['email'] != '') {
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Student','JclassroomTable',array());
					$data 		= array();
					$data['customer_number'] = $item['customer_number'];
					$data['first_name'] = $item['first_name'];
					$data['last_name'] 	= $item['last_name'];
					$data['company'] 	= $item['company'];
					$data['adress'] 	= $item['adress'];
					$data['postcode'] 	= $item['postcode'];
					$data['city'] 		= $item['city'];
					$data['phone'] 		= $item['phone'];
					$data['mobile'] 	= $item['mobile'];
					$data['email'] 		= $item['email'];
					$data['created'] 	= strval($datum);
					$data['created_by'] = $user->id;
					$data['published'] 	= 1;
					$table->bind($data);
					$table->store();
				} else {
					$message .= 'Die Person <b>'.$item['first_name'].' '.$item['last_name'].'</b> mit der E-Mail-Adresse <b>'.$item['email'].'</b> ist bereits im Stammdatensatz vorhanden.<br/>';
				}
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde importiert'.'<br/>'.$message, 'Message');
		$this->setRedirect(JURI::Root().'celearning/students');
	}
	function checkPeople($email) {
		$return 	= false;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Student','JclassroomTable',array());
		$load 		= array('email' => $email);
		$check 		= $table->load($load);
		if($check):
			$return = true;
		endif;

		return $return;
	}
	public function saveCSV() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		$query 		= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_students','a'));
		$query->order('a.id asc');
		$db->setQuery((string)$query);
		$result = $db->loadObjectList();
		$pfad = JPATH_SITE.'/images/jclassroom/export/students'.$user->id.'.csv';
		$file = fopen($pfad,'w');
		$header = array(
			'customer_number' => 'Kunden-Nr.',
			'first_name' 	=> 'Vorname',
			'last_name' 	=> 'Nachname',
			'company'		=> 'Firma',
			'adress' 		=> 'Adresse',
			'postcode' 		=> 'PLZ',
			'city' 			=> 'Ort',
			'phone' 		=> 'Telefonnummer',
			'mobile' 		=> 'Mobilnummer',
			'email' 		=> 'E-Mail',
		);
		fputcsv($file, $header,';');
		if($result) {
			foreach($result as $item) {
				$export = array(
					'customer_number' 	=> $this->convertForExcel($item->customer_number),
					'first_name' 		=> $this->convertForExcel($item->first_name),
					'last_name' 		=> $this->convertForExcel($item->last_name),
					'company' 			=> $this->convertForExcel($item->company),
					'adress' 			=> $this->convertForExcel($item->adress),
					'postcode' 		=> $this->convertForExcel($item->postcode),
					'city' 			=> $this->convertForExcel($item->city),
					'phone' 			=> $this->convertForExcel($item->phone_number),
					'mobile' 		=> $this->convertForExcel($item->mobile_number),
					'email' 			=> $this->convertForExcel($item->email),
				);
				fputcsv($file, $export,';');
			}
		}
		fclose($file);
		$app = JFactory::getApplication();
		$app->redirect(JURI::root().'images/jclassroom/export/students'.$user->id.'.csv');
	}
	function convertForExcel($str) {
		return mb_convert_encoding($str, "Windows-1252", mb_detect_encoding($str, "UTF-8, ISO-8859-1, ISO-8859-15",true));
	}
	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'students';
	
	/**
	 * Get the admin model and set it to default
	 *
	 * @param   string           $name    Name of the model.
	 * @param   string           $prefix  Prefix of the model.
	 * @param   array			 $config  The model configuration.
	 */
	public function getModel($name = 'Student', $prefix='JclassroomModel', $config = array())
	{
		$config['ignore_request'] = true;
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>