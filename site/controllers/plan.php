<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerPlan extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'manager-administrator/plans';
				break;
			case 'trainer':
				$retour = JURI::Root().'celearning/students';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'plan-edit?layout=edit&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = 'index.php?option=com_plans&view=plan&layout=edit&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$post 	= $input->get('jform', array(), 'array');
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Plan','JclassroomTable',array());
		$user 	= JFactory::getUser();
		$data = array();
		$data['title'] 				= $post['title'];
		$data['detail'] 			= $post['detail'];
		$data['description'] 		= $post['description'];
		$data['description_f'] 		= $post['description_f'];
		$data['count_trainer'] 		= $post['count_trainer'];
		$data['count_admin'] 		= $post['count_admin'];
		$data['count_learningrooms']= $post['count_learningrooms'];
		$data['count_modules'] 		= $post['count_modules'];
		$data['count_units'] 		= $post['count_units'];
		$data['price'] 				= $post['price'];
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= $post['published'];
		$table->bind($data);
		$table->store();
		$tblCustomerID 		= $table->id;

		return $tblCustomerID;
	}
	
	public function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Plan','JclassroomTable',array());
		$table->load($input->get('id', 0, 'INT'));
		$table->title 		= $formData->getStr('title');
		$table->detail 		= $formData->getStr('detail');
		$table->description = $formData->getRaw('description');
		$table->description_f = $formData->getRaw('description_f');
		$table->count_admin 	= $formData->getStr('count_admin');
		$table->count_trainer 		= $formData->getStr('count_trainer');
		$table->count_learningrooms = $formData->getStr('count_learningrooms');
		$table->count_modules		= $formData->getStr('count_modules');
		$table->count_units 		= $formData->getStr('count_units');
		$table->price 		= $formData->getInt('price');
		$table->published 	= $formData->getInt('published');
		$table->modified	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		
		return $tblResellerID;
	}
	
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'plan';

	protected $view_list = 'plans';

}
?>