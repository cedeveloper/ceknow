<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerInvoice extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'manager-administrator/invoices';
				break;
			case 'trainer':
				$retour = JURI::Root().'celearning/students';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer/companies-customer';
				break;
		}
		//$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'invoice-edit?&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'company-edit?&id='.$id;
				break;
		}
		//$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$user 	= JFactory::getUser();
		$session= JFactory::getSession();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$post 	= $input->get('jform', array(), 'array');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Invoice','JclassroomTable',array());
		$user 	= JFactory::getUser();
		$data = array();
		$data['customerID'] 		= $post['customerID'];
		$data['invoice_number'] 	= $post['invoice_number'];
		$data['invoice_date'] 		= $post['invoice_date'];
		$data['rate'] 				= $post['rate'];
		$data['tax_rate'] 			= $post['tax_rate'];
		$data['tax'] 				= $post['tax'];
		$data['invoice_title'] 		= $post['invoice_title'];
		$data['invoice_intro'] 		= $post['invoice_intro'];
		$data['invoice_paymentterms'] 	= $post['invoice_paymentterms'];
		$data['invoice_outro'] 		= $post['invoice_outro'];
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= $post['published'];
		$data['performance_type'] 	= $post['performance_type'];
		if($post['performance_type'] == 1 || $post['performance_type'] == 2):
			$pf = $post['performance_date3'];
			$data['effective_date'] = $pf;
		else:
			$pf = $post['performance_date1'].'-'.$post['performance_date2'];
			$data['effective_date'] = $pf;
		endif;
		$table->bind($data);
		$table->store();
		$tblCustomerID 		= $table->id;

		return $tblCustomerID;
	}
	public function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		if($input->get('performance_type',0,'INT') == 1 || $input->get('performance_type',0,'INT') == 2):
			$pf = $formData->getStr('performance_date3');
		else:
			$pf = $formData->getStr('performance_date1').'-'.$formData->getStr('performance_date2');
		endif;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Invoice','JclassroomTable',array());
		$table->load($input->get('id', 0, 'INT'));
		$table->invoice_number 	= $formData->getStr('invoice_number');
		$table->invoice_date 	= date('Y-m-d', strtotime($formData->getStr('invoice_date')));
		$table->effective_date 	= $pf;
		$table->performance_type = $formData->getInt('performance_type');
		$table->invoice_title 	= $formData->getStr('invoice_title');
		$table->invoice_intro 	= $formData->getStr('invoice_intro');
		$table->invoice_paymentterms 	= $formData->getStr('invoice_paymentterms');
		$table->invoice_outro 	= $formData->getStr('invoice_outro');
		$table->published 	= $formData->getInt('published');
		$table->modified	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		
		return $tblResellerID;
	}

	function addPosition() {
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$discount 	= $input->get('discount','','FLOAT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$plan 	= JTable::getInstance('Plan','JclassroomTable',array());
		$plan->load($input->get('articleID',0,'INT'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Invoiceposition','JclassroomTable',array());
		$data = array();
		$data['invoiceID'] 	= $input->get('invoiceID',0,'INT');
		$data['planID'] 	= $input->get('articleID',0,'INT');
		$data['plan'] 		= $input->get('article',0,'STR');
		$data['ep'] 		= $input->get('price','','FLOAT');
		$data['amount'] 	= $input->get('amount','','INT');
		$data['tax'] 		= $input->get('tax','','INT');
		$data['unit'] 		= $input->get('einheitF','','STR');
		$data['discount'] 	= $discount;
		$gp = $input->get('price','','FLOAT') * $input->get('amount','','INT');
		if($discount && $discount > 0):
			$gp = ($gp * (100 - $discount)) / 100;
		endif;
		$data['gp'] 		= $gp;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		echo $table->id;
		exit();
	}
	function addFreetext() {
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Invoiceposition','JclassroomTable',array());
		$data = array();
		$data['invoiceID'] 	= $input->get('invoiceID',0,'INT');
		$data['planID'] 	= 0;
		$data['plan'] 		= $input->get('text','','RAW');
		$data['ep'] 		= 0;
		$data['amount'] 	= 0;
		$data['tax'] 		= 0;
		$data['unit'] 		= '';
		$data['discount'] 	= 0;
		$data['gp'] 		= 0;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		echo $table->id;
		exit();
	}
	function deletePosition() {
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Invoiceposition','JclassroomTable',array());
		$table->delete($input->get('id', 0, 'INT'));
		echo 'OK';
		exit();
	}
	function loadPrice() {
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Plan','JclassroomTable',array());
		$table->load($input->get('planID', 0,'INT'));
		echo number_format($table->price,2,',','.') .' €';
		exit();
	}
	function loadCustomerData() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0,'INT');
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_customer','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('customerID', 0, 'INT')));
		$db->setQuery($query);
		$result = $db->loadObject();
		echo json_encode($result);
		exit();
	}
	public function printPDF() {
      $input = JFactory::getApplication()->input;
      $id = $input->get('id','','INT');
      require_once JPATH_SITE. '/components/com_jclassroom/library/invoice.php';
      $printAudit = new printPDF();
      $printAudit->invoice($id);
   }
}
?>