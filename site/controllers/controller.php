<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class InpageController extends JControllerLegacy {

    public function display($cachable = false, $urlparams = false) {
        require_once JPATH_COMPONENT . '/helpers/inpage.php';

        $view = JFactory::getApplication()->input->getCmd('view', 'Inpage');
        JFactory::getApplication()->input->set('view', $view);

        parent::display($cachable, $urlparams);

        return $this;
    }

}
