<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerLibrary_unit extends JControllerForm
{
	function saveUnit() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		$title	 		= $input->get('title', '', 'STR');
		$description	= $input->get('description', '', 'STR');
		$duration	 	= $input->get('duration', 0, 'INT');
		$content	 	= $input->get('content', '', 'RAW');
		$link	 		= $input->get('link', '', 'STR');
		$quizz	 		= $input->get('quizz', '', 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
		$table->load($unitID);
		$table->title 		= $title;
		$table->description = $description;
		$table->duration 	= $duration;
		$table->content 	= $content;
		$table->link 		= $link;
		$table->quizzID 	= $quizz;
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		echo 'OK';
		exit();
	}
	protected $view_item = 'library_unit';

	protected $view_list = 'library_units';

}
?>