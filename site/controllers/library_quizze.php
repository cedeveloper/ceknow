<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

class JclassroomControllerLibrary_quizze extends JControllerAdmin
{
	function createQuizz() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_library','JclassroomTable',array());
		$table->load($input->get('id', 0, 'INT'));
        $xml = json_decode($table->structure);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Unit','JclassroomTable',array());
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['title'] 				= $xml->quizz->title;
		$data['description'] 		= $xml->quizz->description;
		$data['type'] 				= $xml->quizz->type;
		$data['sendToMail'] 		= $xml->quizz->sendToMail;
		$data['bgColor'] 			= $xml->quizz->bgColor;
		$data['calculate'] 			= $xml->quizz->calculate;
		$data['chart'] 				= $xml->quizz->chart;
		$data['bestof_list'] 		= $xml->quizz->bestof_list;
		$data['published'] 			= 1;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['showType'] 	= $session->get('group');
		$data['showto'] 			= 3;
		$data['editto'] 			= 3;
		$data['choosableto'] 		= 1;
		$table->bind($data);
		$table->store();
		$newUnitID 	= $table->id;
		$newGroup = array();
		if($xml->groups):
			foreach($xml->groups as $question):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$data = array();
				$data['quizzID'] 		= $newUnitID;
				$data['groupID'] 		= 0;
				$data['type'] 			= $question->type;
				$data['title'] 			= $question->title;
				$data['points'] 		= $question->points;
				$data['content'] 		= $question->content;
				$data['infotextPositiv'] 		= $question->infotextPositiv;
				$data['infotextNegativ'] 		= $question->infotextNegativ;
				$data['todo'] 			= $question->todo;
				$data['testfield'] 		= $question->testfield;
				$data['theme'] 			= $question->theme;
				$data['infotext'] 		= $question->infotext;
				$data['kotext'] 		= $question->kotext;
				$data['countAnswers'] 	= $question->countAnswers;
				$data['chart'] 			= $question->chart;
				$data['ordering'] 		= $question->ordering;
				$data['groupOrdering'] 	= $question->groupOrdering;
				$table->bind($data);
				$table->store();
				$newGroup[] = array('oldGroupID' => $question->id, 'newGroupID' => $table->id);
			endforeach;
		endif;
		if($xml->questions):
			foreach($xml->questions as $question):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$data = array();
				$data['quizzID'] 		= $newUnitID;
				foreach($newGroup as $part):

					if($question->groupID == $part['oldGroupID']):
						$data['groupID'] 	= $part['newGroupID'];
					endif;
				endforeach;
				$data['type'] 				= $question->type;
				$data['title'] 				= $question->title;
				$data['points'] 			= $question->points;
				$data['content'] 			= $question->content;
				$data['infotextPositiv'] 		= $question->infotextPositiv;
				$data['infotextNegativ'] 		= $question->infotextNegativ;
				$data['todo'] 				= $question->todo;
				$data['testfield'] 			= $question->testfield;
				$data['theme'] 				= $question->theme;
				$data['infotext'] 			= $question->infotext;
				$data['kotext'] 			= $question->kotext;
				$data['countAnswers'] 		= $question->countAnswers;
				$data['chart'] 				= $question->chart;
				$data['ordering'] 			= $question->ordering;
				$data['groupOrdering'] 		= $question->groupOrdering;
				$table->bind($data);
				$table->store();
				$newQuestionID = $table->id;
				if($question->checkboxes):
					foreach($question->checkboxes as $checkbox):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$cb 		= JTable::getInstance('Quizz_checkboxes','JclassroomTable',array());
						$dataCB = array();
						$dataCB['questionID'] 	= $newQuestionID;
						$dataCB['title'] 		= $checkbox->title;
						$dataCB['correct'] 		= $checkbox->correct;
						$dataCB['ordering'] 	= $checkbox->ordering;
						$cb->bind($dataCB);
						$cb->store();
					endforeach;
				endif;
				if($question->answers):
					foreach($question->answers as $answer):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$an 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
						$dataAN = array();
						$dataAN['questionID'] 	= $newQuestionID;
						$dataAN['title'] 		= $answer->title;
						$dataAN['correct'] 		= $answer->correct;
						$dataAN['ordering'] 	= $answer->ordering;
						$an->bind($dataAN);
						$an->store();
					endforeach;
				endif;
				if($question->lists):
					foreach($question->lists as $list):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$li 		= JTable::getInstance('Quizz_options','JclassroomTable',array());
						$dataLI = array();
						$dataLI['questionID'] 	= $newQuestionID;
						$dataLI['title'] 		= $list->title;
						$dataLI['correct'] 		= $list->correct;
						$dataLI['ordering'] 	= $list->ordering;
						$li->bind($dataLI);
						$li->store();
					endforeach;
				endif;
			endforeach;
		endif;
		echo $newUnitID;
		exit();
	}
	
	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'students';
	
	/**
	 * Get the admin model and set it to default
	 *
	 * @param   string           $name    Name of the model.
	 * @param   string           $prefix  Prefix of the model.
	 * @param   array			 $config  The model configuration.
	 */
	public function getModel($name = 'Student', $prefix='JclassroomModel', $config = array())
	{
		$config['ignore_request'] = true;
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>