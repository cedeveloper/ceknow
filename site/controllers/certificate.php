<?php
defined('_JEXEC') or die;

class printCertificate {
	
	public function certificate($id) {
		echo $id;
		require_once(JPATH_SITE.'/components/com_jclassroom/controllers/tcpdf_include.php');
		require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('dats | Druckagentur Torsten Scheel');
		$pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE,PDF_HEADER_STRING);
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		// set margins
		$pdf->SetMargins(10, 50, 10,true);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->SetFont('helvetica', 'R', 14, '', false);
		$pdf->AddPage();
		//$html = $this->getResult();
		$pdf->writeHTML($html, true, false, true, false, '');
		ob_end_clean();
		$pdf->Output('components/com_inclure/pdf/Dispo1_BLG'.$id.'.pdf', 'I');
	}

	function getResult() {
		$counter 	= 1;
		$input 		= JFactory::getApplication()->input;
		$resultID 	= $input->get('rID',0,'INT');
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		// Load the result of the quizz
		$query 		= $db->getQuery(true);
		$query->select(array('
			a.*, 
			a.id as theResultID,
			b.id as resultPositionID,
			b.*
		'));
        $query->from($db->quoteName('#__jclassroom_theresults','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('b.questionID') . ' = ' . $db->quoteName('c.id') . ')');
		$query->where($db->quoteName('a.id').' = '.$db->quote($resultID));
		$query->group('b.questionID');
		$query->order('c.ordering');
		$db->setQuery($query);
		$questions 	= $db->loadObjectList();
		$quizzID 	= $questions[0]->quizzID;
		$theResultID= $questions[0]->theResultID;
		//Load the quizz
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quizz 		= JTable::getInstance('Unit','JclassroomTable',array());
		$quizz->load($quizzID);
		$html 		= '';
		if($questions):
			$html .= '<table border="0" width="100%" style="font-size: 12px;">';
			$html .= '<tbody>';
			$html .= '<tr>';
			$html .= '<td>Herzlichen Glückwunsch! Sie haben unser Quizz erfolgreich absolviert. Nachfolgend finden Sie Ihr individuelles Ergebnis.</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td height="10"></td>';
			$html .= '</tr>';
			$html .= '</tbody>';
			$html .= '</table>';
			$html .= '<table border="0" width="100%" style="font-size: 11px;">';
			$html .= '<thead>';
			$html .= '<tr style="background-color: #e1e1e1;">';
			$html .= '<th style="border: 0.2px solid #000;" width="30%">Frage</th>';
			if($quizz->calculate == 2):
				$html .= '<th style="border: 0.2px solid #000;" width="30%">Antwortmöglichkeiten</th>';
				$html .= '<th style="border: 0.2px solid #000;" width="30%">Ihre Antwort</th>';
				$html .= '<th style="border: 0.2px solid #000;text-align: center;" width="10%">Punkte</th>';
			else:
				$html .= '<th width="70%" style="text-center;border: 0.2px solid #000;">Ihre Antwort</th>';
			endif;
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';

			foreach($questions as $question):
				$html .= '<tr>';
				//Get the Question
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$quest 	= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$quest->load($question->questionID);
				$points = $quest->points;
				$html 	.= '<td width="30%" style="border: 0.2px solid #000;font-size: 10px;"><h5><b>'.$counter.'. '.$quest->title.'</b></h5></td>';
				// WWM
				if($quest->type == 3):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$ans 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
					$ans->load($question->answerID);
					if($quizz->calculate == 2):
						$query 	= $db->getQuery(true);
						$query->select(array('a.*'));
				        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
						$query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$corrects 	= $db->loadObjectList();
						if($corrects):
							$correctSoll 	= 0;
							$correctIst 	= 0;
							$html .= '<td width="30%" style="border: 0.2px solid #000;">';
							$html .= '<table style="width: 100%; font-size: 9px;">';
							foreach($corrects as $correct):
								if($correct->correct == 1):
									$bgS = 'forestgreen';
									$correctSoll++;
								else:
									$bgS = 'firebrick';
								endif;
								$html .= '<tr>';
								$html .= '<td style="border: 0.5px solid #000;color: #fff;background-color: '.$bgS.'">'.$correct->title.'</td>';
								$html .= '</tr>';
							endforeach;
							$html .= '</table>';
							$html .= '</td>';
							$html .= '<td width="30%" style="border: 0.2px solid #000;">';
							$html .= '<table style="width: 100%;font-size: 9px;">';
							foreach($corrects as $correct):
								$query 	= $db->getQuery(true);
								$query->select(array('a.*'));
						        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						        $query->where($db->quoteName('a.theResultID').' = '.$db->quote($question->theResultID));
						        $query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quoteName('a.answerID').' = '.$db->quote($correct->id));
								$db->setQuery($query);
								$result 	= $db->loadObject();
								$html .= '<tr>';
								if($correct->correct == 1 && $result->id):
									$html .= '<td style="border: 0.5px solid #000;color: #fff;background-color: forestgreen;">'.$correct->title.'</td>';
									$correctIst++;
								endif;
								if($correct->correct == 1 && !$result->id):
									$html .= '<td></td>';
								endif;
								if($correct->correct == 0 && $result->id):
									$html .= '<td style="border: 0.5px solid #000;color: #fff;background-color: firebrick;">'.$correct->title.'</td>';
								endif;
								if($correct->correct == 0 && !$result->id):
									$html .= '<td></td>';
								endif;
								$html .= '</tr>';
							endforeach;

							$html .= '</table>';
							$html .= '</td>';
						endif;
						if($correctSoll == $correctIst):
							$html .= '<td width="10%" style="border: 0.2px solid #000;text-align: center;">'.$points.'</td>';
							$gesPoints += $points;
						else:
							$html .= '<td width="10%" style="border: 0.2px solid #000;text-align: center;">0</td>';
						endif;
						$maxPoints += $quest->points;
					endif;
				endif;
				// Smileys
				if($quest->type == 4):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
					$result->load($question->resultPositionID);
					if($quizz->calculate == 2):
						$points = 0;
						$html .= '<td width="30%" style="border: 0.2px solid #000;">';
						$html .= '<table style="width: 100%;">';
						$html .= '<tr>';
						$html .= '<td style="border: 0.2px solid #000;background-color: #BC1A3D; color:#fff;font-size: 9px;text-align: center;">Sehr schlecht</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="border: 0.2px solid #000;background-color: #DD4610;color:#fff;font-size: 9px;text-align: center;">Schlecht</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="border: 0.2px solid #000;background-color: #F4D532;color:#fff;font-size: 9px;text-align: center;">Nicht schlecht</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="border: 0.2px solid #000;background-color: #A9CB44;color:#fff;font-size: 9px;text-align: center;">Gut</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="border: 0.2px solid #000;background-color: #538930;color:#fff;font-size: 9px;text-align: center;">Sehr gut</td>';
						$html .= '</tr>';
						$html .= '</table>';
						$html .= '</td>';
						$html .= '<td width="30%">';
						$html .= '<table style="width: 100%;font-size: 11px;">';
						$html .= '<tr>';
						if($result->answerID == '1'):
							$html .= '<td style="border: 0.2px solid #000;background-color: #BC1A3D; color:#fff;font-size: 9px;text-align: center;">Sehr schlecht</td>';
							$points 	= ($quest->points / 5) * 1;
						else:	
							$html .= '<td style="font-size: 9px;"></td>';
						endif;
						$html .= '</tr>';
						$html .= '<tr>';
						if($result->answerID == '2'):
							$html .= '<td style="border: 0.2px solid #000;background-color: #DD4610;color:#fff;font-size: 9px;text-align: center;">Schlecht</td>';
							$points 	= ($quest->points / 5) * 2;
						else:		
							$html .= '<td style="font-size: 9px;"></td>';
						endif;
						$html .= '</tr>';
						$html .= '<tr>';
						if($result->answerID == '3'):
							$html .= '<td style="border: 0.2px solid #000;background-color: #F4D532;color:#fff;font-size: 9px;text-align: center;">Nicht schlecht</td>';
							$points 	= ($quest->points / 5) * 3;
						else:		
							$html .= '<td style="font-size: 9px;"></td>';
						endif;
						$html .= '</tr>';
						$html .= '<tr>';
						if($result->answerID == '4'):
							$html .= '<td style="border: 0.2px solid #000;background-color: #A9CB44;color:#fff;font-size: 9px;text-align: center;">Gut</td>';
							$points 	= ($quest->points / 5) * 4;
						else:		
							$html .= '<td style="font-size: 9px;"></td>';
						endif;
						$html .= '</tr>';
						$html .= '<tr>';
						if($result->answerID == '5'):
							$html .= '<td style="border: 0.2px solid #000;background-color: #538930;color:#fff;font-size: 9px;text-align: center;">Sehr gut</td>';
							$points 	= $quest->points;
						else:		
							$html .= '<td style="font-size: 9px;"></td>';
						endif;
						$html .= '</tr>';
						$html .= '</table>';
						$html .= '</td>';	
						$html .= '<td width="10%" style="border: 0.2px solid #000;text-align: center;">'.$points.'</td>';
						$gesPoints += $points;
					else:
						$html .= '<td class="p-1 bg bg-success text-center">'.$answI5.'</td>';
					endif;
					$maxPoints += $quest->points;
				endif;
				// Ja/Nein
				if($quest->type == 5):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
					$result->load($question->resultPositionID);
					// 0 = Ja, 1 = Nein
					$correctAnswer 	= $quest->correctAnswer;
					switch($correctAnswer):
						case 0:
							$answS5 	= 'Ja';
							break;
						case 1:
							$answS5 	= 'Nein';
							break;
					endswitch;
					switch($result->content):
						case 'answerJ':
							$answI5 	= 'Ja';
							break;
						case 'answerN':
							$answI5 	= 'Nein';
							break;
					endswitch;
					if($quizz->calculate == 2):
						$points = 0;
						if($answS5 == $answI5):
							$points = $quest->points;
							$gesPoints += $points;
							$bgR 	= 'forestgreen';
						else:
							$bgR 	= 'firebrick';
						endif;
						$html .= '<td style="border: 0.2px solid #000;">';
						$html .= '<table>';
						$html .= '<tr>';
						$html .= '<td style="background-color: forestgreen;color: #fff;border: 0.2px solid #000;text-align: center; font-size: 9px;">'.$answS5.'</td>';
						$html .= '</tr>';
						$html .= '</table>';
						$html .= '</td>';
						$html .= '<td style="border: 0.2px solid #000;">';
						$html .= '<table>';
						$html .= '<tr>';
						$html .= '<td style="background-color: '.$bgR.';color: #fff;border: 0.2px solid #000;text-align: center; font-size: 9px;">'.$answI5.'</td>';
						$html .= '</tr>';
						$html .= '</table>';
						$html .= '</td>';
						$html .= '<td style="text-align: center;border: 0.2px solid #000;font-size: 11px;">'.$points.'</td>';
					else:
						$html .= '<td>'.$answI5.'</td>';
					endif;
					$maxPoints += $quest->points;
				endif;
				// Audit Frage
				if($quest->type == 6):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
					$result->load($question->resultPositionID);
					switch($result->content):
						case 'A':
							$answI5 	= 'Ja';
							$bgI 		= 'forestgreen';
							break;
						case 'AA':
							$answI5 	= 'Ja, mit Einschränkung';
							$bgI 		= 'orange';
							break;
						case 'N':
							$answI5 	= 'Nein';
							$bgI 		= 'firebrick';
							break;
					endswitch;
					if($quizz->calculate == 2):
						$points = 0;
						$html .= '<td style="border: 0.2px solid #000;" width="30%">';
						$html .= '<table style="width: 100%;font-size: 9px;">';
						$html .= '<tr>';
						$html .= '<td style="border: 0.2px solid #000;background-color:forestgreen;color:#fff;font-size: 9px;text-align: center;">Ja</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="border: 0.2px solid #000;background-color:orange;color:#fff;font-size: 9px;text-align: center;">Ja, mit Einschränkung</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="border: 0.2px solid #000;background-color:firebrick;color:#fff;font-size: 9px;text-align: center;">Nein</td>';
						$html .= '</tr>';
						$html .= '</table>';
						$html .= '</td>';
						$html .= '<td style="border: 0.2px solid #000;" width="30%">';
						$html .= '<table style="width: 100%;font-size: 11px;">';
						$html .= '<tr>';
						if($answI5 == 'Ja'):
							$html .= '<td style="border: 0.2px solid #000;background-color:forestgreen;color:#fff;font-size: 9px;text-align: center;">Ja</td>';
							$points 	= $quest->points;
						else:	
							$html .= '<td style="font-size: 9px;"></td>';
						endif;
						$html .= '</tr>';
						$html .= '<tr>';
						if($answI5 == 'Ja, mit Einschränkung'):
							$html .= '<td style="border: 0.2px solid #000;background-color:orange;color:#fff;font-size: 9px;text-align: center;">Ja, mit Einschränkung</td>';
							if($result->exeptionPoints):
								$points 	= $result->exeptionPoints;
							else:
								$points 	= $quest->points;
							endif;
						else:		
							$html .= '<td style="font-size: 9px;"></td>';
						endif;
						$html .= '</tr>';
						$html .= '<tr>';
						if($answI5 == 'Nein'):
							$html .= '<td style="border: 0.2px solid #000;background-color:firebrick;color:#fff;font-size: 14px;text-align: center;">Nein</td>';
						else:		
							$html .= '<td style="font-size: 9px;"></td>';
						endif;
						$html .= '</tr>';
						$html .= '</table>';
						$html .= '</td>';	
						$html .= '<td width="10%" style="border: 0.2px solid #000;text-align: center;">'.$points.'</td>';
						$gesPoints += $points;
					else:
						$html .= '<td width="10%" style="border: 0.2px solid #000;text-align: center;">'.$answI5.'</td>';
					endif;
					$maxPoints += $quest->points;
				endif;
				// Textfeld Frage
				if($quest->type == 10):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
					$result->load($question->resultPositionID);
					if($quizz->calculate == 2):
						if($result->content):
							$points 	= $quest->points;
							$gesPoints += $points;
							$maxPoints += $quest->points;
						else:
							$points = 10;
						endif;
						$html .= '<td style="border: 0.2px solid #000;"></td>';
						$html .= '<td style="border: 0.2px solid #000;font-size: 9px;">'.$result->content.'</td>';
						$html .= '<td style="text-align: center;border: 0.2px solid #000;">'.$points.'</td>';
						
					else:
						$html .= '<td class="p-1 bg bg-success text-center">'.$result->content.'</td>';
					endif;
				endif;
				// Textarea Frage
				if($quest->type == 11):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
					$result->load($question->resultPositionID);
					if($quizz->calculate == 2):
						if($result->content):
							$points 	= $quest->points;
							$gesPoints += $points;
							$maxPoints += $quest->points;
						else:
							$points = 0;
						endif;
						$html .= '<td style="border: 0.2px solid #000;"></td>';
						$html .= '<td style="border: 0.2px solid #000;font-size: 9px;">'.$result->content.'</td>';
						$html .= '<td style="text-align: center;border: 0.2px solid #000;">'.$points.'</td>';
					else:
						$html .= '<td class="p-1 bg bg-success text-center">'.$result->content.'</td>';
					endif;
				endif;
				// Auswahlliste
				if($quest->type == 12):
					$query 	= $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
					$query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
					$db->setQuery($query);
					$corrects 	= $db->loadObjectList();
					if($quizz->calculate == 2):
						if($corrects):
							$correctSoll 	= 0;
							$correctIst 	= 0;
							$html .= '<td style="border: 0.2px solid #000;">';
							$html .= '<table style="width: 100%;">';
							foreach($corrects as $correct):
								if($correct->correct == 1):
									$bgS = 'forestgreen';
									$correctSoll++;
								else:
									$bgS = 'firebrick';
								endif;
								$html .= '<tr>';
								$html .= '<td style="border: 0.2px solid #000;color: white; background-color:'.$bgS.';font-size: 9px;">'.$correct->title.'</td>';
								$html .= '</tr>';
							endforeach;
							$html .= '</table>';
							$html .= '</td>';
							$html .= '<td style="border: 0.2px solid #000;">';
							$html .= '<table style="width: 100%;">';
							foreach($corrects as $correct):
								$query 	= $db->getQuery(true);
								$query->select(array('a.*'));
						        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						        $query->where($db->quoteName('a.theResultID').' = '.$db->quote($question->theResultID));
						        $query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quoteName('a.answerID').' = '.$db->quote($correct->id));
								$db->setQuery($query);
								$result 	= $db->loadObject();
								$html .= '<tr>';
								if($correct->correct == 1 && $result):
									$bgI = 'bg-success';
									$html .= '<td style="border: 0.2px solid #000;color: #fff;background-color: forestgreen;font-size: 9px;">'.$correct->title.'</td>';
									$correctIst++;
								endif;
								if($correct->correct == 1 && !$result):
									$html .= '<td style="font-size: 9px;"></td>';
								endif;
								if($correct->correct == 0 && !$result):
									$html .= '<td style=";font-size: 9px;"></td>';
								endif;
								if($correct->correct == 0 && $result):
									$html .= '<td style="border: 0.2px solid #000;color: #fff;background-color: firebrick;font-size: 9px;">'.$correct->title.'</td>';
								endif;
								$html .= '</tr>';
							endforeach;
							$html .= '</table>';
							$html .= '</td>';
							
						endif;
						if($correctSoll == $correctIst):
							$html .= '<td style="border: 0.2px solid #000;text-align: center;">'.$points.'</td>';
							$gesPoints += $points;
						else:
							$html .= '<td style="border: 0.2px solid #000;text-align: center;">0</td>';
						endif;
						$maxPoints += $quest->points;
					else:
						$html .= '<td width="70%" style="border: 0.2px solid #000;padding: 0px;">';
						$html .= '<table>';
						foreach($corrects as $correct):
							$query 	= $db->getQuery(true);
							$query->select(array('a.*'));
					        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					        $query->where($db->quoteName('a.theResultID').' = '.$db->quote($question->theResultID));
					        $query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quoteName('a.answerID').' = '.$db->quote($correct->id));
							$db->setQuery($query);
							$result 	= $db->loadObject();
							if($result):	
								$html .= '<tr>';
								$html .= '<td style="border: 0.2px solid #000;background-color: lightsteelblue;">'.$correct->title.'</td>';
								$html .= '</tr>';
							endif;
						endforeach;
						$html .= '</table>';
						$html .='</td>';
					endif;
				endif;
				// Checkboxen
				if($quest->type == 13):
					$query 	= $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
					$query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
					$db->setQuery($query);
					$corrects 	= $db->loadObjectList();
					if($quizz->calculate == 2):
						if($corrects):
							$correctSoll 	= 0;
							$correctIst 	= 0;
							$html .= '<td width="30%" style="border: 0.2px solid #000;">';
							$html .= '<table style="width: 100%;">';
							foreach($corrects as $correct):
								if($correct->correct == 1):
									$bgS = 'forestgreen';
									$correctSoll++;
								else:
									$bgS = 'firebrick';
								endif;
								$html .= '<tr>';
								$html .= '<td style="border: 0.2px solid #000;background-color: '.$bgS.';color: #fff;font-size: 9px;">'.$correct->title.'</td>';
								$html .= '</tr>';
							endforeach;
							$html .= '</table>';
							$html .= '</td>';
							$html .= '<td width="30%" style="border: 0.2px solid #000;">';
							$html .= '<table style="width: 100%;">';
							foreach($corrects as $correct):
								$query 	= $db->getQuery(true);
								$query->select(array('a.*'));
						        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						        $query->where($db->quoteName('a.theResultID').' = '.$db->quote($question->theResultID));
						        $query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quoteName('a.answerID').' = '.$db->quote($correct->id));
								$db->setQuery($query);
								$result 	= $db->loadObject();
								$html .= '<tr>';
								if($correct->correct == 1 && $result):
									$html .= '<td style="border: 0.2px solid #000;background-color: forestgreen;color: #fff;font-size: 9px;">'.$correct->title.'</td>';
									$correctIst++;
								endif;
								if($correct->correct == 1 && !$result):
									$html .= '<td style="font-size: 9px;"></td>';
								endif;
								if($correct->correct == 0 && !$result):
									$html .= '<td style="font-size: 9px;"></td>';
								endif;
								if($correct->correct == 0 && $result):
									$html .= '<td style="border: 0.2px solid #000;background-color: firebrick;color: #fff;font-size: 9px;">'.$correct->title.'</td>';
								endif;
								$html .= '</tr>';
							endforeach;
							$html .= '</table>';
							$html .= '</td>';
							if($correctSoll == $correctIst):
								$html .= '<td width="10%" style="text-align: center;border: 0.2px solid #000;" class="text-center">'.$points.'</td>';
								$gesPoints += $points;
							else:
								$html .= '<td width="10%" style="text-align: center;border: 0.2px solid #000;" class="text-center">0</td>';
							endif;
							$maxPoints += $quest->points;
						endif;
					else:
						$html .= '<td width="70%" style="border: 0.2px solid #000;padding: 0px;">';
						$html .= '<table style="width:100%;">';
						foreach($corrects as $correct):
							$query 	= $db->getQuery(true);
							$query->select(array('a.*'));
					        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					        $query->where($db->quoteName('a.theResultID').' = '.$db->quote($question->theResultID));
					        $query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quoteName('a.answerID').' = '.$db->quote($correct->id));
							$db->setQuery($query);
							$result 	= $db->loadObject();
							if($result):	
								$html .= '<tr>';
								$html .= '<td style="border: 0.2px solid #000;background-color: lightsteelblue;">'.$correct->title.'</td>';
								$html .= '</tr>';
							endif;
						endforeach;
						$html .= '</table>';
						$html .='</td>';
					endif;
				endif;
				$html .= '</tr>';
				$counter++;
			endforeach;
			$html .= '</tbody>';
			if($quizz->calculate == 2):
				$html .= '<tfoot>';
				$html .= '<tr>';
				$html .= '<td style="border: 0.2px solid #000; font-size: 8px;"><b>Ihre erreicht Punktzahl (von maximal '.$maxPoints.')</b></td>';
				$html .= '<td style="border: 0.2px solid #000;" colspan="2"></td>';
				$html .= '<td style="border: 0.2px solid #000;" align="center" class="text-center"><b>'.$gesPoints.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
				$html .= '<td style="border: 0.2px solid #000;font-size: 8px;"><b>Dies entspricht einem Score von</b></td>';
				$html .= '<td style="border: 0.2px solid #000;" colspan="2"></td>';
				$score = $gesPoints * 100/ $maxPoints;
				$html .= '<td style="border: 0.2px solid #000;" align="center" class="text-center"><b>'.round($score,2).'%</b></td>';
				$html .= '</tr>';
				$html .= '</tfoot>';
			endif;
			$html .= '</table>';
		endif;
		return $html;
	}
}
require_once(JPATH_SITE.'/components/com_jclassroom/controllers/tcpdf_include.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');

class MYPDF extends TCPDF {

	public function Header() {
		$this->Image(JURI::Root().'components/com_jclassroom/library/logo_ceKNOW.png', 10, 10, 70, 26, 'PNG','' , '', true, 300, '', false, false, 0, false, false, false);
		$this->writeHTMLCell(90, 30, 110,17.5, '<h1 style="font-size: 38pt;">Zertifikat</h1>', 0, 0, 0, 0, 'R', true, true, 0, false, true, 30, 'M', true);
	}
	public function Footer() {
		$now = JFactory::getDate();
		$date = new JDate($now);
		$now = $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$datumSTR = $now->format('d.m.Y');
		$datum = explode(' ',$now);
		$this->SetXY(10,-15);
		// Set font
		$this->SetFont('helvetica', '', 7);
		// Page number
		$this->Cell(0, 10, $mandant, 0, false, 'L', 0, '', 0, false, 'T', 'M');
		$this->MultiCell(50, 5, 'ceKnow (c) '.date('Y'), '0', 'L', 0, 1, '10', '285', true);
		$this->MultiCell(50, 5, 'Druck am: '.$datumSTR.' '.$datum[1], '', 'L', 0, 1, '10', '288', true);
		$this->MultiCell(50, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), '0', 'R', 0, 1, '160', '288', true);
	}
}

