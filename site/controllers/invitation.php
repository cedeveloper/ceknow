<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Kunde item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerInvitation extends JControllerForm
{
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'invitation&id=1';

	protected $view_list = 'invitation&id=1';
}
?>