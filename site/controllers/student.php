<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerStudent extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'students';
				break;
			case 'trainer':
				$retour = JURI::Root().'students';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer/students-customer';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$session 	= JFactory::getSession();
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$post 	= $input->get('jform', array(), 'array');
		$password 	= JUserHelper::hashPassword($post['password']);
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Student','JclassroomTable',array());
		$datum 	= JFactory::getDate();
		$date 	= new JDate($datum);
		$datum 	= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 	= JFactory::getUser();
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['companyID'] 			= $post['companyID'];
		$data['customer_number'] 	= $post['customer_number'];
		$data['salutation'] 		= $post['salutation'];
		$data['first_name'] 		= $post['first_name'];
		$data['last_name'] 			= $post['last_name'];
		$data['company'] 			= $post['company'];
		$data['adress'] 			= $post['adress'];
		$data['postcode'] 			= $post['postcode'];
		$data['city'] 				= $post['city'];
		$data['phone'] 				= $post['phone'];
		$data['fax'] 				= $post['fax'];
		$data['mobile'] 			= $post['mobile'];
		$data['email'] 				= $post['email'];
		$data['asp_name'] 			= $post['asp_name'];
		$data['asp_phone'] 			= $post['asp_phone'];
		$data['asp_email'] 			= $post['asp_email'];
		$data['username'] 	= $post['username'];
		$data['password'] 	= $post['password'];
		$data['published'] 	= $post['published'];
		$data['created'] 	= strval($date);
		$data['created_by']	= $user->id;
		$data['tblUserID'] 	= 0;
		$data['logo'] 		= $savename;
		$table->bind($data);
		$table->store();
		$tblCustomerID 		= $table->id;
		if($post['username'] && $post['password']):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('User','JclassroomTable',array());
			$data = array();
			$data['name'] 		= $post['first_name'].' '.$post['last_name'];
			$data['username'] 	= $post['email'];
			$data['email'] 		= $post['email'];
			$data['password'] 	= $password;
			$data['block'] 		= 0;
			$data['sendEmail'] 	= 0;
			$data['registerDate'] 	= strval($date);
			$data['lastvisitDate'] 	= strval($date);
			$data['activation'] 	= 0;
			$table->bind($data);
			$table->store();
			$tblUserID = $table->id;
			//Tabelle Usergroup-Map
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('Usergroup','JclassroomTable',array());
			$data = array();
			$data['user_id'] 	= $tblUserID;
			$data['group_id'] 	= 11;
			$table->bind($data);
			$table->store();
			// SAVE the user ID
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('Student','JclassroomTable',array());
			$table->load($tblCustomerID);
			$table->tblUserID = $tblUserID;
			$table->store();
		endif;

		return $tblCustomerID;
	}
	public function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$tblResellerID 		= $input->get('id',0,'INT');
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Student','JclassroomTable',array());
		$load = array('tblUserID' => $formData->getInt('id'));
		$table->load($load);
		$table->salutation 	= $formData->getStr('salutation');
		$table->companyID 	= $formData->getInt('companyID');
		$table->salutation 	= $formData->getStr('salutation');
		$table->first_name 		= $formData->getStr('first_name');
		$table->last_name 		= $formData->getStr('last_name');
		$table->customer_number = $formData->getStr('customer_number');
		$table->company 	= $formData->getStr('company');
		$table->adress 		= $formData->getStr('adress');
		$table->postcode 	= $formData->getStr('postcode');
		$table->city		= $formData->getStr('city');
		$table->phone 		= $formData->getStr('phone');
		$table->fax 		= $formData->getStr('fax');
		$table->mobile 		= $formData->getStr('mobile');
		$table->email		= $formData->getStr('email');
		$table->web 		= $formData->getStr('web');
		if($savename) {
			$table->logo 		= $savename;
		}
		$table->asp_name 	= $formData->getStr('asp_name');
		$table->asp_phone	= $formData->getStr('asp_phone');
		$table->asp_email	= $formData->getStr('asp_email');
		$table->username 	= $formData->getStr('username');
		$table->password 	= $formData->getStr('password');
		$table->published 	= $formData->getInt('published');
		$table->modified	= strval($date);
		$table->modified_by	= $user->id;
		$table->store();
		if($formData->getStr('username')):
			$password 			= JUserHelper::hashPassword($formData->getStr('password'));
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('User','JclassroomTable',array());
			$table->load($formData->getInt('id'));
			$table->name 		= $formData->getStr('first_name').' '.$formData->getStr('last_name');
			$table->username 	= $formData->getStr('email');
			if($formData->get('password')):
				$table->password 	= $password;
			endif;
			$table->email 		= $formData->getStr('email');
			$table->store();
		endif;
		
		return $tblResellerID;
	}
	function deleteImage() {
		$input 		= JFactory::getApplication()->input;
		$userID 	= $input->get('userID',0,'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Student','JclassroomTable',array());
		$load = array('tblUserID' => $userID);
		$table->load($load);
		$path = $table->logo;
		$table->logo = '';
		$table->store();
		if($path) {
			JFile::delete(JPATH_ROOT.'/'.$path);
		}
	}
	
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'student';

	protected $view_list = 'students';

}
?>