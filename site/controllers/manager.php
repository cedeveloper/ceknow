<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerManager extends JControllerForm
{
	
	public function vertify() {
		$input 		= JFactory::getApplication()->input;
		$uid 		= $input->get('uid','', 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$load = array('id' => $uid);
		$table->load($load);
		//if($table->password == ''):
		$password 			= JUserHelper::hashPassword($input->get('passwort','', 'STR'));
		$table->password 	= $password;
		$table->store();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Verified','JclassroomTable',array());
		$data = array();
		$data['classroomID'] 	= 0;
		$data['studentID'] 		= $uid;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $uid;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$this->setRedirect('index.php?option=com_jclassroom&view=vertify&layout=hello', false);
		//else:
			//JFactory::getApplication()->enqueueMessage('Ihr Konto ist bereits aktiviert', 'Message');
			//$this->setRedirect(JURI::Root(), false);
		//endif;

		echo $password;
	}
	function quitt() {
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$uid 		= $input->get('id','', 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Logs','JclassroomTable',array());
		$table->load($uid);
		$table->quitt = date('Y-m-d H:i:s');
		$table->quitt_by = $user->id;
		$table->store();
		echo 'OK';
		exit();
	}

}
?>