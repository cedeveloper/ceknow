<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Fragenkatalog item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerUnit_config extends JControllerForm
{
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'unit';

	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'units';

	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = 'manager';
				$retour = JURI::Root().'quizze';
				break;
			case 'trainer':
				$retour = JURI::Root().'quizze';
				break;
			case 'customer':
				$retour = JURI::Root().'quizze';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'section-config?layout=edit&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'section-config?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'section-config?layout=edit&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	function saveToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$sectionIDS 	= $input->get('sectionID','','INT');
		$types 		= $input->get('type','','INT');
		$contents 	= $input->get('content','','RAW');
		if($sectionIDS):
			foreach($sectionIDS as $keySection => $section):
				echo $keySection.' '.$section.'<br/>';
			endforeach;
		endif;
	}
	function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0, 'INT');
		$sectionIDS 	= $input->get('sectionID','','STR');
		$types 		= $input->get('type','','STR');
		$contents 	= $input->get('content','','RAW');
		$yeses 		= $input->get('section5Yes','','STR');
		$noes 		= $input->get('section5No','','STR');
		$points 	= $input->get('section5Points','','STR');
		$titles 	= $input->get('section3Title','','STR');
		$points3 	= $input->get('section3Points','','STR');
		$answers3 	= $input->get('answerTyp3','','STR');
		$corrects3 	= $input->get('correctTyp3','','STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Unit','JclassroomTable',array());
		$check = $table->load($id);
		if($check):
			if($sectionIDS):
				$return = array();
				foreach($sectionIDS as $keySection => $section):
					$setType 	= '';
					$setContent = '';
					$setNo 		= '';
					$setYes 	= '';
					$setPoint 	= '';
					$setTitle 	= '';
					$setAnswer 	= '';
					$setCorrect = '';
					if($types):
						foreach($types as $keyType => $type):
							if($keySection == $keyType):
								$setType = $type[0];
							endif;
						endforeach;
					endif;
					if($contents):
						foreach($contents as $keyContent => $content):
							if($keySection == $keyContent):
								$setContent = $content[0];
							endif;
						endforeach;
					endif;
					if($noes):
						foreach($noes as $keyNo => $no):
							if($keySection == $keyNo):
								$setNo = $no[0];
							endif;
						endforeach;
					endif;
					if($yeses):
						foreach($yeses as $keyYes => $yes):
							if($keySection == $keyYes):
								$setYes = $yes[0];
							endif;
						endforeach;
					endif;
					if($points):
						foreach($points as $keyPoint => $point):
							if($keySection == $keyPoint):
								$setPoint = $point[0];
							endif;
						endforeach;
					endif;
					if($points3):
						foreach($points3 as $keyPoint => $point):
							if($keySection == $keyPoint):
								$setPoint = $point[0];
							endif;
						endforeach;
					endif;
					if($titles):
						foreach($titles as $keyTitles => $title):
							if($keySection == $keyTitles):
								$setTitle = $title[0];
							endif;
						endforeach;
					endif;
					if($answers3):
						foreach($answers3 as $keyA => $answer3):
							if($keySection == $keyA):
								foreach($answer3 as $keyA2 => $answer):
									$setCorrect = '';
									if($corrects3):
										foreach($corrects3 as $keyCorrect => $correct3):
											if($keySection == $keyCorrect):
												foreach($correct3 as $keyCorrect2 => $correct):
													if($keyCorrect2 == $keyA2):
														$setCorrect = $correct[0];
													endif;
												endforeach;
											endif;
										endforeach;
									endif;
									$setAnswer[] = array('answer' => $answer[0], 'correct' => $setCorrect);
								endforeach;
							endif;
						endforeach;
					endif;
					$return[] = array(
						'sectionID' => $keySection,
						'sectionType' => $setType,
						'content' => $setContent,
						'no' => $setNo,
						'yes' => $setYes,
						'points' => $setPoint,
						'title' => $setTitle,
						'answers' => $setAnswer );
				endforeach;
			endif;
			//print_r($return);
			$table->structure = json_encode($return);
			$table->store();
		endif;
	}
	public function saveSection() {
		$input = JFactory::getApplication()->input;
		$sectionID 	= $input->get('sectionID','','INT');
		$type 		= $input->get('type','','INT');
		$content 	= $input->get('content','','RAW');
		$title 		= $input->get('title','','STR');
		$points 	= $input->get('points',0,'INT');
		$answers 	= $input->get('answers','','RAW');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum_ad/tables');
		$table 		= JTable::getInstance('Unit_config','Auditum_adTable',array());
		if($sectionID != 0) {
			$check = $table->load($sectionID);
			if($check) {
				$table->content = $content;
				$table->title 	= $title;
				$table->points 	= $points;
				$table->store();
			} else {
				$ordering 	= $this->countSectionsinUnit($input->get('unitID', 0, 'INT'));
				$data 		= array();
				$data['unitID']				= $input->get('unitID', 0, 'INT');
				$data['type']				= $type;
				$data['content']			= $content;
				$data['title']				= $title;
				$data['points']				= $points;
				$data['ordering']			= $ordering;
				$data['published'] 			= 1;
				$table->bind($data);
				$table->store();
				$sectionID = $table->id;
			}
		} else {
			$ordering 	= $this->countSectionsinUnit($input->get('unitID', 0, 'INT'));
			$data 		= array();
			$data['unitID']				= $input->get('unitID', 0, 'INT');
			$data['type']				= $type;
			$data['content']			= $content;
			$data['title']				= $title;
			$data['points']				= $points;
			$data['ordering']			= $ordering;
			$data['published'] 			= 1;
			$table->bind($data);
			$table->store();
			$sectionID = $table->id;
		}
		if($answers) {
			$this->writeAnswers($sectionID, $answers, $type);
		}
		echo 'OK';
		exit();
	}
	function writeAnswers($sectionID, $answers, $type) {

		foreach($answers as $answer) {
			$set 			= explode('_',$answer);
			if($type == 3) {
				if($set[2] == 'false') {
					$setC = 0;
				} else {
					$setC = 1;
				}
				$case = '';
			}
			if($type == 5) {
				if($set[2] == 'false') {
					$setC = 0;
					$case = 'no';
				} else {
					$setC = 0;
					$case = 'yes';
				}
			}
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum_ad/tables');
			$table 		= JTable::getInstance('Unit_section_answer','Auditum_adTable',array());
			if($set[0] != 0) {
				$answerID 	= (int) $set[0];
				$check 		= $table->load($answerID);
				if($check) {
					$table->load($answerID);
					$table->answer 				= htmlentities($set[1]);
					$table->correct 			= $setC;
					$table->aCase 				= $case;
					$table->store();
				} else {
					$data 						= array();
					$data['sectionID']			= strval($sectionID);
					$data['answer']				= htmlentities($set[1]);
					$data['correct']			= $setC;
					$data['aCase']				= $case;
					$table->bind($data);
					$table->store();
				}
			} else {
				if($set[1]) {
					$data 						= array();
					$data['sectionID']			= strval($sectionID);
					$data['answer']				= htmlentities($set[1]);
					$data['correct']			= $setC;
					$data['aCase']				= $case;
					$table->bind($data);
					$table->store();
				}
			}
		}
	}
	function countSectionsinUnit($unitID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('MAX(a.ordering)'));
	    $query->from($db->quoteName('#__audit_unit_sections','a'));
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
		$db->setQuery($query);
		$nf = $db->loadResult() +  1;
		return $nf;
	}
}
?>