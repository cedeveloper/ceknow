<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerCompany extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'companys';
				break;
			case 'trainer':
				$retour = JURI::Root().'celearning/students';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer/companies-customer';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'company-edit?&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'company-edit?&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$user 	= JFactory::getUser();
		$session= JFactory::getSession();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$post 	= $input->get('jform', array(), 'array');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Company','JclassroomTable',array());
		$user 	= JFactory::getUser();
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['customer_number'] 	= $post['customer_number'];
		$data['name'] 				= $post['name'];
		$data['address'] 			= $post['address'];
		$data['postcode'] 			= $post['postcode'];
		$data['city'] 				= $post['city'];
		$data['phone'] 				= $post['phone'];
		$data['email'] 				= $post['email'];
		$data['web'] 				= $post['web'];
		$data['showVertifyAGB'] 	= $post['showVertifyAGB'];
		$data['showVertifyDS'] 		= $post['showVertifyDS'];
		$data['showVertifyFilm'] 	= $post['showVertifyFilm'];
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= $post['published'];
		$table->bind($data);
		$table->store();
		$tblCustomerID 		= $table->id;

		return $tblCustomerID;
	}
	public function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Company','JclassroomTable',array());
		$table->load($input->get('id', 0, 'INT'));
		$table->name 			= $formData->getStr('name');
		$table->customer_number = $formData->getStr('customer_number');
		$table->address 		= $formData->getStr('address');
		$table->postcode 	= $formData->getStr('postcode');
		$table->city		= $formData->getStr('city');
		$table->email		= $formData->getStr('email');
		$table->phone		= $formData->getStr('phone');
		$table->web			= $formData->getStr('web');
		$table->showVertifyAGB	= $formData->getInt('showVertifyAGB');
		$table->showVertifyDS	= $formData->getInt('showVertifyDS');
		$table->showVertifyFilm	= $formData->getInt('showVertifyFilm');
		$table->published 	= $formData->getInt('published');
		$table->modified	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		
		return $tblResellerID;
	}
	function addContact() {
		$session 	= JFactory::getSession();
		$input 		= JFactory::getApplication()->input;
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Contact','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $session->get('customerID');
		$data['companyID'] 		= $input->get('customerID',0,'INT');
		$data['salutation'] 	= $input->get('salutation','','STR');
		$data['first_name'] 	= $input->get('first_name','','STR');
		$data['last_name'] 		= $input->get('last_name','','STR');
		$data['phone'] 			= $input->get('phone','','STR');
		$data['email'] 			= $input->get('email','','STR');
		$data['function'] 		= $input->get('functionS','','STR');
		$data['type'] 			= 2;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		echo $table->id;
		exit();
	}
	function editContact() {
		$input 		= JFactory::getApplication()->input;
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Contact','JclassroomTable',array());
		$table->load($input->get('contactID', 0,'INT'));
		$table->salutation 		= $input->get('salutation','','STR');
		$table->first_name 		= $input->get('first_name','','STR');
		$table->last_name 		= $input->get('last_name','','STR');
		$table->phone 			= $input->get('phone','','STR');
		$table->email 			= $input->get('email','','STR');
		$table->function 		= $input->get('functionS','','STR');
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->published 	= 1;
		$table->store();
		echo $table->id;
		exit();
	}
	function getContact() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0,'INT');
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_contacts','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('id', 0, 'INT')));
		$db->setQuery($query);
		$contact = $db->loadObject();
		echo json_encode($contact);
		exit();
	}
	function deleteContact() {
		$input 	= JFactory::getApplication()->input;
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Contact','JclassroomTable',array());
		$table->delete($input->get('id', 0, 'INT'));
		echo 'OK';
		exit();
	}
	
}
?>