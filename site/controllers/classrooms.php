<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

class JclassroomControllerClassrooms extends JControllerAdmin
{
	public function delete() {
		$input = JFactory::getApplication()->input;
		$id = $input->get('id', 0, 'INT');
		$delete = $input->get('cid',array(), 'array');
		if($delete) {
			foreach($delete as $del) {
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Classroom','JclassroomTable',array());
				$table->delete($del);	
				$db 	= JFactory::getDbo(); 
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		        $query->where($db->quotename('a.classroomID').' = '.$db->quote($del));
				$db->setQuery($query);
				$days = $db->loadObjectList();
				foreach($days as $day):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$tableD = JTable::getInstance('Classroom_days','JclassroomTable',array());
					$tableD->delete($day->id);
				endforeach;
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		        $query->where($db->quotename('a.classroomID').' = '.$db->quote($del));
		        $query->order('a.ordering ASC');
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				foreach($modules as $module):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$tableD = JTable::getInstance('Classroom_modules','JclassroomTable',array());
					$tableD->delete($module->id);
				endforeach;
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		        $query->where($db->quotename('a.classroomID').' = '.$db->quote($del));
		        $query->order('a.ordering ASC');
				$db->setQuery($query);
				$units = $db->loadObjectList();
				foreach($units as $unit):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$tableD = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
					$tableD->delete($unit->id);
				endforeach;
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
		        $query->where($db->quotename('a.classroomID').' = '.$db->quote($del));
				$db->setQuery($query);
				$units = $db->loadObjectList();
				foreach($units as $unit):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$tableD = JTable::getInstance('Classroom_student','JclassroomTable',array());
					$tableD->delete($unit->id);
				endforeach;
				// DELETE FROM TBL_FILES
				$query 	= $db->getQuery(true);
				$query->select('a.id');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.classroomID').' = '.$db->quote($del));
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableF = JTable::getInstance('File','JclassroomTable',array());
						$tableF->delete($file->id);
					endforeach;
				endif;
				JFolder::delete(JPATH_ROOT.'/images/learningrooms/LR'.$del);	
			}
		}
		$app = JFactory::getApplication();
		JFactory::getApplication()->enqueueMessage('Die ausgewählten Datensätze wurden gelöscht', 'message');
		$app->redirect(JURI::Root().'classrooms');
	}
	function copyLearningroom() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id = $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$classroom = JTable::getInstance('Classroom','JclassroomTable',array());
		$classroom->load($id);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableN = JTable::getInstance('Classroom','JclassroomTable',array());
		$data = array();
		$data['customerID'] 		= $classroom->customerID;
		$data['companyID'] 			= $classroom->companyID;
		$data['title'] 				= $classroom->title.' (copy from LR '.$id.')';
		$data['description'] 		= $classroom->description;
		$data['main_trainer'] 		= $classroom->main_trainer;
		$data['co_trainer'] 		= $classroom->co_trainer;
		$data['fromDate'] 			= '';
		$data['toDate'] 			= '';
		$data['inventa_terminID'] 	= $classroom->inventa_terminID;
		$data['inventa_kursID'] 	= $classroom->inventa_kursID;
		$data['email_template'] 	= $classroom->email_template;
		$data['verification_template'] 	= $classroom->verification_template;
		$data['remember_template'] 	= $classroom->remember_template;
		$data['published'] 	= $classroom->published;
		$data['created'] 	= date('Y-m-d H.i:s');
		$data['created_by']	= $user->id;
		$data['logo'] 		= $classroom->logo;
		$data['files'] 			= $classroom->files;
		$data['files_students'] 	= $classroom->files_students;
		$data['published'] 	= $classroom->published;
		$data['showto'] 	= $classroom->showto;
		$data['editto'] 	= $classroom->editto;
		$data['choosableto'] 	= $classroom->choosableto;
		$tableN->bind($data);
		$tableN->store();
		$newLearningRoomID = $tableN->id;
		//Create imagefolders for new LR
		$pathFL 	= JPATH_SITE.'/images/learningrooms/LR'.$newLearningRoomID;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');
		// Copy files
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_files','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($id));
        $query->where('('.$db->quotename('a.type').' = '.$db->quote('material').' OR '.$db->quotename('a.type').' = '.$db->quote('reserve').')');
		$db->setQuery($query);
		$files = $db->loadObjectList();
		if($files):
			foreach($files as $file):
				$pathLibraryToMove = '/images/learningrooms/LR'.$newLearningRoomID;
				if($file->type == 'material'):
					$tile = 'material';
				endif;
				if($file->type == 'reserve'):
					$tile = 'reserve';
				endif;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableF = JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['type'] 		= $file->type;
				$data['classroomID']= $newLearningRoomID;
				$data['unitID'] 	= $newUnitID;
				$data['studentID'] 	= $file->studentID;
				$data['folder'] 	= '';
				$data['filename'] 	= $file->filename;
				$data['path'] 		= $pathLibraryToMove.'/'.$tile.'/'.$file->filename;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $file->published;
				$tableF->bind($data);
				$tableF->store();
				copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.'/'.$tile.'/'.$file->filename);
			endforeach;
		endif;
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($id));
        $query->order('a.day ASC');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		if($days):
			foreach($days as $day):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_days','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $day->customerID;
				$data['classroomID']= $newLearningRoomID;
				$data['day'] 		= $day->day;
				$data['title'] 		= $day->title;
				$data['ordering'] 	= $day->ordering;
				$data['dayID'] 		= $day->dayID;
				$data['published'] 	= $day->published;
				$tableD->bind($data);
				$tableD->store();
				$newDayID = $tableD->id;
				$this->copyModules($day->id,$newDayID, $newLearningRoomID);
			endforeach;
		endif;
		exit();
	}
	function copyModules($oldDayID, $newDayID, $newLearningRoomID) {
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
        $query->where($db->quotename('a.dayID').' = '.$db->quote($oldDayID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$modules = $db->loadObjectList();
		if($modules):
			foreach($modules as $module):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_modules','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $module->customerID;
				$data['classroomID']= $newLearningRoomID;
				$data['dayID'] 		= $newDayID;
				$data['title'] 		= $module->title;
				$data['description'] = $module->description;
				$data['ordering'] 	= $module->ordering;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $module->published;
				$tableD->bind($data);
				$tableD->store();
				$this->copyUnits($module->id, $tableD->id, $newLearningRoomID);
			endforeach;
		endif;
	}
	function copyUnits($oldModuleID, $newModuleID, $newLearningRoomID) {
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
        $query->where($db->quotename('a.moduleID').' = '.$db->quote($oldModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $unit->customerID;
				$data['classroomID']= $newLearningRoomID;
				$data['dayID'] 		= 0;
				$data['moduleID'] 	= $newModuleID;
				$data['unitID'] 	= $unit->unitID;
				$data['unitType'] 	= $unit->unitType;
				$data['ordering'] 	= $unit->ordering;
				$data['duration'] 	= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $unit->published;
				$tableD->bind($data);
				$tableD->store();
				$newUnitID = $tableD->id;
				// Copy files
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$pathLibraryToMove = '/images/learningrooms/LR'.$newLearningRoomID.'/units/';
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableF = JTable::getInstance('File','JclassroomTable',array());
						$data = array();
						$data['type'] 		= $file->type;
						$data['classroomID']= $newLearningRoomID;
						$data['unitID'] 	= $newUnitID;
						$data['studentID'] 	= 0;
						$data['folder'] 	= '';
						$data['filename'] 	= $file->filename;
						$data['path'] 		= $pathLibraryToMove.'/'.$file->filename;
						$data['created'] 	= date('Y-m-d H:i:s');
						$data['created_by'] = $user->id;
						$data['published'] 	= $file->published;
						$tableF->bind($data);
						$tableF->store();
						copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.'/'.$file->filename);
					endforeach;
				endif;
			endforeach;
		endif;
	}
	function import() {
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote(27));
		$db->setQuery($query);
		$classroom = $db->loadObject();
		// Copy Learningroom
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom','JclassroomTable',array());
		$data = array();
		$data['mandant'] 			= $classroom->mandant;
		$data['title'] 				= $classroom->title;
		$data['description'] 		= $classroom->description;
		$data['main_trainer'] 		= $classroom->main_trainer;
		$data['co_trainer'] 		= $classroom->co_trainer;
		$data['fromDate'] 			= '';
		$data['toDate'] 			= '';
		$data['inventa_kursID'] 	= $classroom->inventa_kursID;
		$data['inventa_terminID'] 	= $classroom->inventa_terminID;
		$data['published'] 	= $classroom->published;
		$data['created'] 	= date('Y-m-d H.i:s');
		$data['created_by']	= $user->id;
		$data['logo'] 		= $classroom->logo;
		$data['files'] 			= $classroom->files;
		$data['files_students'] 	= $classroom->files_students;
		//$table->bind($data);
		//$table->store();
		//$classroomID = $table->id;
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote(27));
		$db->setQuery($query);
		$days = $db->loadObjectList();
		echo '<pre>';
		if($days):
			foreach($days as $day):
				echo $day->id.' '.$day->day.'<br/>';
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
				$data = array();
				$data['classroomID'] 	= 32;
				$data['day'] 			= $day->day;
				$data['dayID'] 			= $day->dayID;
				$data['published']		= 1;
				//$table->bind($data);
				//$table->store();
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		        $query->where($db->quotename('a.classroomID').' = '.$db->quote(27));
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				if($modules):
					foreach($modules as $module):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
						$data = array();
						$data['classroomID'] 	= 32;
						$data['dayID'] 			= $day->id;
						$data['title'] 			= $module->title;
						$data['description'] 	= '';
						$data['created']		= date('Y-m-d H:i:s');
						$data['created_by']		= $user->id;
						$data['published']		= 1;
						//$table->bind($data);
						//$table->store();
						$query 	= $db->getQuery(true);
						$query->select('a.*');
				        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				        $query->where($db->quotename('a.classroomID').' = '.$db->quote(27));
				        //$query->where($db->quotename('a.dayID').' = '.$db->quote($day->dayID));
				        //$query->where($db->quotename('a.moduleID').' = '.$db->quote($module->id));
						$db->setQuery($query);
						$units = $db->loadObjectList();
						if($units):
							foreach($units as $unit):
								print_r($units);
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
								$data = array();
								$data['classroomID'] = 32;
								$data['dayID'] 		= 98;
								$data['moduleID'] 	= $moduleID;
								$data['unitID'] 	= $unit->unitID;
								$data['unitType'] 	= $unit->unitType;
								$data['duration'] 	= $unit->duration;
								$data['title'] 	= $unit->title;
								$data['link'] 	= $unit->link;
								$data['quizzID'] 	= $unit->quizzID;
								$data['content'] 	= $unit->content;
								$data['ordering'] 	= $unit->ordering;
								$data['created'] 	= date('Y-m-d H:i:s');
								$data['created_by'] = $user->id;
								$data['published']	= 1;
								$table->bind($data);
								$table->store();
							endforeach;
						endif;
					endforeach;
				endif;
			endforeach;
		endif;
	}
	function export() {
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$session->set('tmp_lrid', $id);
		
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		$xml['classroom'] = array(
			'id' 			=> $table->id,
			'customerID'	=> $table->customerID,
			'companyID'		=> $table->companyID,
			'title' 		=> $table->title, 
			'description' 	=> $table->description,
			'main_trainer' 	=> $table->main_trainer,
			'co_trainer' 	=> $table->co_trainer,
			'showTo' 		=> $table->showTo,
			'showToUser' 	=> $table->showToUser,
			'email_template' 		=> $table->email_template,
			'verification_template' => $table->verification_template,
			'remember_template' 	=> $table->remember_template,
			'invitations'  	=> $table->invitations,
			'countRememberMails'  	=> $table->countRememberMails,
			'offsetRememberMails'  	=> $table->offsetRememberMails,
			'invitationsBy'  	=> $table->invitationsBy,
			'certificate'  		=> $table->certificate,
			'student_feedback'  	=> $table->student_feedback,
			'student_feedback_anonym'  	=> $table->student_feedback_anonym,
			'trainer_feedback'  	=> $table->trainer_feedback

		);
		$db = JFactory::getDbo();
		// Load the groups
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$id);
		$query->order('a.day asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$days[] 	= array(
					'id' 		=> $item->id,
					'classroomID' 	=> $item->classroomID,
					'day' 			=> $item->day,
					'title' 		=> $item->title,
					'dayID' 		=> $item->dayID
				);
			endforeach;
		endif;
		$xml['days']	= $days;
		// Load the units
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.classroomID = '.$id);
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$positionsU = array();
				$positionsF = array();
				// Load the units
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				$query->where('a.moduleID = '.$item->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$units = $db->loadObjectList();
				if($units):
					foreach($units as $unit):
						$positionsU[] 	= array(
							'id' 			=> $unit->id,
							'customerID' 	=> $unit->customerID,
							'classroomID' 	=> $unit->classroomID,
							'dayID' 		=> $unit->dayID,
							'moduleID' 		=> $unit->moduleID,
							'unitID' 		=> $unit->unitID,
							'unitType' 		=> $unit->unitType,
							'ordering' 		=> $unit->ordering,
							'duration' 		=> $unit->duration,
							'title' 		=> $unit->title,
							'content' 		=> $unit->content,
							'link' 			=> $unit->link,
							'quizzID' 		=> $unit->quizzID
						);
					endforeach;
				endif;
				// Load the files
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_files','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$positionsF[] 	= array(
							'type' 			=> $file->type,
							'classroomID' 	=> $file->classroomID,
							'unitID' 		=> $file->unitID,
							'studentID' 	=> $file->studentID,
							'folder' 		=> $file->folder,
							'filename' 		=> $file->filename,
							'path' 			=> $file->path
						);
					endforeach;
				endif;
				$modules[] 	= array(
					'id' 		=> $item->id,
					'customerID' 	=> $item->customerID,
					'classroomID' 	=> $item->classroomID,
					'dayID' 		=> $item->dayID,
					'title' 		=> $item->title,
					'description' 	=> $item->description,
					'ordering' 		=> $item->ordering,
					'units' 		=> $positionsU,
					'files' 		=> $positionsF
				);
				
			endforeach;
		endif;
		$xml['modules']	= $modules;
		$save = json_encode($xml);
		$rand = rand(100,10000);
		$pathSave = JPATH_SITE."/images/jclassroom/temp/xml_".$rand.".json";
		$downloadPath = JURI::Root()."images/jclassroom/temp/xml_".$rand.".json";
		file_put_contents($pathSave,$save);
		$session->set('tmpxml',$pathSave);
		require_once(JPATH_BASE.'/zip.php');
		$zip = $session->get('tmpzip');
		echo $zip;
		exit();
	}
	function loadJSON() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$file  		= $input->files->get('uploadJSON');
		$fileName   = $file['name'];
		$src        = $file['tmp_name'];
        $dest       = JPATH_BASE.'/images/jclassroom/temp/'.$fileName;
        JFile::upload($src, $dest);
        $get = file_get_contents($dest);
        $xml = json_decode($get);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom','JclassroomTable',array());
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['companyID'] 			= '';
		$data['title'] 				= $xml->classroom->title;
		$data['description'] 		= $xml->classroom->description;
		$data['main_trainer'] 		= $xml->classroom->main_trainer;
		$data['co_trainer'] 		= $xml->classroom->co_trainer;
		$data['showTo'] 			= $xml->classroom->showTo;
		$data['showToUser'] 		= $xml->classroom->showToUser;
		$data['email_template'] 		= $xml->classroom->email_template;
		$data['verification_template'] 	= $xml->classroom->verification_template;
		$data['remember_template'] 		= $xml->classroom->remember_template;
		$data['invitations'] 			= $xml->classroom->invitations;
		$data['countRememberMails'] 	= $xml->classroom->countRememberMails;
		$data['offsetRememberMails'] 	= $xml->classroom->offsetRememberMails;
		$data['invitationsBy'] 			= $xml->classroom->invitationsBy;
		$data['certificate'] 			= $xml->classroom->certificate;
		$data['student_feedback'] 		= $xml->classroom->student_feedback;
		$data['student_feedback_anonym'] 		= $xml->classroom->student_feedback_anonym;
		$data['trainer_feedback'] 		= $xml->classroom->trainer_feedback;
		$data['published'] 	= 1;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$table->bind($data);
		$table->store();
		$newClassroomID 	= $table->id;
		$newDay = array();
		if($xml->days):
			foreach($xml->days as $day):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_days','JclassroomTable',array());
				$data = array();
				$data['classroomID'] 	= $newClassroomID;
				$data['day'] 			= $day->day;
				$data['title'] 			= $day->title;
				$data['dayID'] 			= $day->dayID;
				$data['published'] 		= 1;
				$table->bind($data);
				$table->store();
				$newDayID = $table->id;
				$newDay[] = array('oldDayID' => $day->id, 'newDayID' => $table->id);
			endforeach;
		endif;
		if($xml->modules):
			foreach($xml->modules as $module):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_modules','JclassroomTable',array());
				$data = array();
				$data['customerID'] 		= $module->customerID;
				$data['classroomID'] 		= $newClassroomID;
				foreach($newDay as $part):
					if($module->dayID == $part['oldDayID']):
						$data['dayID'] 	= $part['newDayID'];
					endif;
				endforeach;
				$data['title'] 				= $module->title;
				$data['description'] 		= $module->content;
				$data['ordering'] 			= $module->ordering;
				$data['groupOrdering'] 		= $module->groupOrdering;
				$data['published'] 			= 1;
				$data['created'] 			= date('Y-m-d H:i:s');
				$data['created_by']			= $user->id;
				$table->bind($data);
				$table->store();
				$newModuleID = $table->id;
				if($module->units):
					foreach($module->units as $unit):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$cu 	= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
						$dataCU = array();
						$dataCU['customerID'] 	= $unit->customerID;
						$dataCU['classroomID'] 	= $newClassroomID;
						foreach($newDay as $part):
							if($unit->dayID == $part['oldDayID']):
								$dataCU['dayID'] 	= $part['newDayID'];
							endif;
						endforeach;
						$dataCU['moduleID'] 	= $newModuleID;
						$dataCU['unitID'] 		= $unit->unitID;
						$dataCU['unitType'] 	= $unit->unitType;
						$dataCU['ordering'] 	= $unit->ordering;
						$dataCU['duration'] 	= $unit->duration;
						$dataCU['title'] 		= $unit->title;
						$dataCU['content'] 		= $unit->content;
						$dataCU['link'] 		= $unit->link;
						$dataCU['quizzID'] 		= $unit->quizzID;
						$dataCU['published'] 	= 1;
						$dataCU['created'] 		= date('Y-m-d H:i:s');
						$dataCU['created_by']	= $user->id;
						$cu->bind($dataCU);
						$cu->store();
					endforeach;
				endif;
				/*if($question->answers):
					foreach($question->answers as $answer):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$an 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
						$dataAN = array();
						$dataAN['questionID'] 	= $newQuestionID;
						$dataAN['title'] 		= $answer->title;
						$dataAN['correct'] 		= $answer->correct;
						$dataAN['ordering'] 	= $answer->ordering;
						$an->bind($dataAN);
						$an->store();
					endforeach;
				endif;
				if($question->lists):
					foreach($question->lists as $list):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$li 		= JTable::getInstance('Quizz_options','JclassroomTable',array());
						$dataLI = array();
						$dataLI['questionID'] 	= $newQuestionID;
						$dataLI['title'] 		= $list->title;
						$dataLI['correct'] 		= $list->correct;
						$dataLI['ordering'] 	= $list->ordering;
						$li->bind($dataLI);
						$li->store();
					endforeach;
				endif;*/
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Der Learningroom wurde erfolgreich angelegt', 'Message');
		$this->setRedirect(JURI::Root().'classrooms');
	}
	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'classrooms';
	
	/**
	 * Get the admin model and set it to default
	 *
	 * @param   string           $name    Name of the model.
	 * @param   string           $prefix  Prefix of the model.
	 * @param   array			 $config  The model configuration.
	 */
	public function getModel($name = 'Classroom', $prefix='JclassroomModel', $config = array())
	{
		$config['ignore_request'] = true;
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>