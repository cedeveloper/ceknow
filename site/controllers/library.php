<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerLibrary extends JControllerForm
{
	function addThemeToLibrary() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$themeID 	= $input->get('dayID', 0, 'INT'); 
		// Be sure, the neccassary folder exit
		$pathFL 	= JPATH_SITE.'/images/library';
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/themes');
		
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$day = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$day->load($themeID);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$library = JTable::getInstance('Classroom_days_library','JclassroomTable',array());
		$data 			= array();
		$data['customerID'] 	= $session->get('customerID');
		$data['classroomID'] 	= 0;
		$data['title'] 			= $day->title;
		$data['description']	= $day->description;
		$data['day'] 	= $day->day;
		$data['dayID'] 	= $day->dayID;
		$data['ordering'] 	= $day->ordering;
		$data['created']	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$library->bind($data);
		$library->store();
		$newDayID = $library->id;
		$pathLibrary = $pathFL.'/themes/theme_'.$newDayID;
		$pathLibraryToMove = '/images/library/themes/theme_'.$newDayID;
		JFolder::create($pathLibrary);
		$this->copyModules($day->id,$newDayID, $day->classroomID);
		echo '<pre>';
		echo $themeID;
		exit();
	}
	function copyModules($oldDayID, $newDayID, $newLearningRoomID) {
		$pathFL = JPATH_SITE.'/images/library';
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
        $query->where($db->quotename('a.dayID').' = '.$db->quote($oldDayID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$modules = $db->loadObjectList();
		if($modules):
			foreach($modules as $module):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $session->get('customerID');
				$data['classroomID']= 0;
				$data['dayID'] 		= $newDayID;
				$data['title'] 		= $module->title;
				$data['description'] = $module->description;
				$data['ordering'] 	= $module->ordering;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $module->published;
				$tableD->bind($data);
				$tableD->store();
				$newModuleID = $tableD->id;
				$pathLibrary = $pathFL.'/themes/theme_'.$newDayID.'/modules/module_'.$newModuleID;
				$pathLibraryToMove = '/images/library/themes/theme_'.$newDayID.'/modules/module_'.$newModuleID;
				JFolder::create($pathLibrary);
				$this->copyUnits($module->id, $newModuleID, $newLearningRoomID, $newDayID);
			endforeach;
		endif;
	}
	function copyUnits($oldModuleID, $newModuleID, $newLearningRoomID, $newDayID) {
		$session = JFactory::getSession();
		$pathFL = JPATH_SITE.'/images/library';
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
        $query->where($db->quotename('a.moduleID').' = '.$db->quote($oldModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $session->get('customerID');
				$data['classroomID']= 0;
				$data['dayID'] 		= $newDayID;
				$data['moduleID'] 		= $newModuleID;
				$data['unitID'] 		= $unit->unitID;
				$data['unitType'] 		= $unit->unitType;
				$data['ordering'] 		= $unit->ordering;
				$data['duration'] 		= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $unit->published;
				$tableD->bind($data);
				$tableD->store();
				$newUnitID = $tableD->id;
				$pathLibrary = $pathFL.'/themes/theme_'.$newDayID.'/modules/module_'.$newModuleID.'/units/unit_'.$newUnitID;
				$pathLibraryToMove = '/images/library/themes/theme_'.$newDayID.'/modules/module_'.$newModuleID.'/units/unit_'.$newUnitID;
				JFolder::create($pathLibrary);
				// Copy the files
				$db 	= JFactory::getDbo(); 
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
		        $query->order('a.id ASC');
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$file_library 	= JTable::getInstance('File','JclassroomTable',array());
						$data 			= array();
						$data['customerID'] = $session->get('customerID');
						$data['type'] 		= $file->type;
						$data['classroomID']= 0;
						$data['unitID'] 	= $newUnitID;
						$data['studentID'] 		= 0;
						$data['folder'] 		= '';
						$data['filename'] 		= $file->filename;
						$data['path'] 			= $pathLibraryToMove.'/'.$file->filename;
						$data['ordering'] 		= 0;
						$data['created'] 		= date('Y-m-d H:i:s');
						$data['created_by'] 	= $user->id;
						$data['published'] 		= 1;
						$file_library->bind($data);
						$file_library->store();
						copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.'/'.$file->filename);
					endforeach;
				endif;
			endforeach;
		endif;
	}
}
?>