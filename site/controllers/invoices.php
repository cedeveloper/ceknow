<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

class JclassroomControllerInvoices extends JControllerAdmin
{
	public function delete() {
		$input = JFactory::getApplication()->input;
		$id = $input->get('id', 0, 'INT');
		$delete = $input->get('cid',array(), 'array');
		if($delete) {
			foreach($delete as $del) {
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Invoice','JclassroomTable',array());
				$table->delete($del);	
			}
		}
		JFactory::getApplication()->enqueueMessage('Die Datensätze wurden gelöscht', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour 	= 'manager-administrator/invoices';
				break;
			case 'customer':
				$retour 	= 'manager-customer/companies-customer';
				break;
			case 'trainer':

				break;
			case 'student':

				break;
		}
		$this->setRedirect(JURI::Root().$retour);
	}
	
	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'invoices';
	
	/**
	 * Get the admin model and set it to default
	 *
	 * @param   string           $name    Name of the model.
	 * @param   string           $prefix  Prefix of the model.
	 * @param   array			 $config  The model configuration.
	 */
	public function getModel($name = 'Student', $prefix='JclassroomModel', $config = array())
	{
		$config['ignore_request'] = true;
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>