<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerTemplate extends JControllerForm
{
	function sendTemplate() {
		$doctype 	= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
		$htmlPre 	= '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">';
		$bodyPost 	= '</body>';
		$htmlPost 	= '</html>';
		$datum 	= JFactory::getDate();
		$date 	= new JDate($datum);
		$datum 	= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$reciever 	= $input->get('reciever', '', 'STR');
		//Load the template
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($id));
		$db->setQuery($query);
		$template = $db->loadobject();
		$body 	= $doctype;
		$body  .= $htmlPre;
		$body  .= $template->head;
		$body  .= '<style>';
		$body  .= $template->styles;
		$body  .= '</style>';
		$body  .= $template->body;
		$body  .= $template->text;
		$body  .= $bodyPost;
		$body  .= $htmlPost;
		$mailer = JFactory::getMailer();
		$sender = array('info@ceLearning.de','ceLearning');
		$mailer->setSender($sender);
		$mailer->addRecipient($reciever);
		$mailer->setSubject($template->subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    echo 'Error sending email: ';
		} else {
		    echo 'Mail sent';
		}
		JFactory::getApplication()->enqueueMessage('Das Template wurde an '.$reciever.' versendet.');
		$this->setRedirect(JURI::Root().'template-edit?layout=stage&id='.$id);
	}
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'template';

	protected $view_list = 'templates';

}
?>