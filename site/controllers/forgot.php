<?php
/**
 *  	ce corporate education GmbH
 * 	  	JClassroom 2.1
 * 	 	(C) 2021. All Rights reserved.
 * 		25. Januar 2021
 *		Developer: Torsten Scheel
 */
 
// No direct access to this file

class JclassroomControllerForgot extends JControllerForm
{
	function reset() {
		$input 	= JFactory::getApplication()->input;
		$email 	= $input->get('email', '', 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemName');
		$table->load($load);
		$systemName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		// 1. Try to identify the user
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('User','JclassroomTable',array());
		$load = array('email' => $email);
		$table->load($load);
		$theUser = $table->load($load);
		$theUserID = $table->id;
		if(!$theUser):
			JFactory::getApplication()->enqueueMessage('Ihre E-Mail-Adresse ist uns nicht bekannt. Bitte prüfen Sie die korrekte Schreibweise.<br/>Wenn Sie noch keinen Zugang zu <b>'.$systemName.'</b> haben, finden Sie weitere Informationen unter <a href="ceknow-kostenlos-testen">Testen</a>', 'Error');
			$this->setRedirect(JRoute::_('login', false));
		else:
			$db 	= JFactory::getDbo();
			// 2. Try to identify the usergroup of the user
			$query 	= $db->getQuery(true);
			$query->select(array('b.id,b.title'));
	        $query->from($db->quoteName('#__user_usergroup_map','a'));
	        $query->join('LEFT', $db->quoteName('#__usergroups', 'b') . ' ON (' . $db->quoteName('a.group_id') . ' = ' . $db->quoteName('b.id') . ')');
	        $query->where($db->quotename('a.user_id').' = '.$db->quote($table->id));
			$db->setQuery($query);
			$usergroup = $db->loadObject();
			// 3. Try to find out the customerID, if there is no customerID, use the system default.
			$customerID = false;
			switch($usergroup->title):
				case 'Super Users':
					$query 	= $db->getQuery(true);
					$query->select(array('a.name'));
					$query->from($db->quoteName('#__users','a'));
					$query->where($db->quotename('a.id').' = '.$db->quote($table->id));
					$db->setQuery($query);
					$customerID = $db->loadObject();
					$salutation = '';
					$first_name = $customerID->name;
					$last_name 	= '';
				case 'Customer':
					$query 	= $db->getQuery(true);
					$query->select(array('a.customerID,a.salutation,a.first_name,a.last_name'));
					$query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
					$query->where($db->quotename('a.userID').' = '.$db->quote($table->id));
					$db->setQuery($query);
					$customerID = $db->loadObject();
					$salutation = $customerID->salutation;
					$first_name = $customerID->first_name;
					$last_name 	= $customerID->last_name;
					break;
				case 'Customeradmin':
					$query 	= $db->getQuery(true);
					$query->select(array('a.customerID,a.salutation,a.first_name,a.last_name'));
					$query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
					$query->where($db->quotename('a.userID').' = '.$db->quote($table->id));
					$db->setQuery($query);
					$customerID = $db->loadObject();
					$salutation = $customerID->salutation;
					$first_name = $customerID->first_name;
					$last_name 	= $customerID->last_name;
					break;
				case 'Trainer':
					$query 	= $db->getQuery(true);
					$query->select(array('a.customerID,a.salutation,a.first_name,a.last_name'));
					$query->from($db->quoteName('#__jclassroom_trainers','a'));
					$query->where($db->quotename('a.tblUserID').' = '.$db->quote($table->id));
					$db->setQuery($query);
					$customerID = $db->loadObject();
					$salutation = $customerID->salutation;
					$first_name = $customerID->first_name;
					$last_name 	= $customerID->last_name;
					break;
				case 'Students':
					$query 	= $db->getQuery(true);
					$query->select(array('a.customerID,a.salutation,a.first_name,a.last_name'));
					$query->from($db->quoteName('#__jclassroom_students','a'));
					$query->where($db->quotename('a.tblUserID').' = '.$db->quote($table->id));
					$db->setQuery($query);
					$customerID = $db->loadObject();
					$salutation = $customerID->salutation;
					$first_name = $customerID->first_name;
					$last_name 	= $customerID->last_name;
					break;
				default:
					$query 	= $db->getQuery(true);
					$query->select(array('a.name'));
					$query->from($db->quoteName('#__users','a'));
					$query->where($db->quotename('a.id').' = '.$db->quote($table->id));
					$db->setQuery($query);
					$customerID = $db->loadObject();
					$salutation = '';
					$first_name = $customerID->name;
					$last_name 	= '';
					break;
			endswitch;
			// 4. Try to load the template for the customerID
			$query 	= $db->getQuery(true);
			$query->select(array('a.text,a.subject'));
	        $query->from($db->quoteName('#__jclassroom_templates','a'));
	        $query->where($db->quotename('a.type').' = '.$db->quote(5));
	        if($customerID):
	        	$query->where($db->quotename('a.customerID').' = '.$db->quote($customerID->customerID));
	        else:
	        	$query->where($db->quotename('a.customerID').' = '.$db->quote(0));
	        endif;
			$db->setQuery($query);
			$template = $db->loadObject();
			if(!$template):
				$query 	= $db->getQuery(true);
				$query->select(array('a.text,a.subject'));
		        $query->from($db->quoteName('#__jclassroom_templates','a'));
		        $query->where($db->quotename('a.type').' = '.$db->quote(5));
		        $query->where($db->quotename('a.customerID').' = '.$db->quote(0));
				$db->setQuery($query);
				$template = $db->loadObject();
			endif;
			$subject  = $template->subject;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$tableC 		= JTable::getInstance('Configuration','JclassroomTable',array());
			if($customerID):
				$load 		= array('customerID' => $customerID->customerID, 'parameter' => 'emailSender');
				$tableC->load($load);
				$emailSender = $tableC->wert;
				$load 		= array('customerID' => $customerID->customerID, 'parameter' => 'emailSenderName');
				$tableC->load($load);
				$emailSenderName = $tableC->wert;
				$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
				$tableC->load($load);
				$systemLink = $tableC->wert;
			else:
				$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
				$tableC->load($load);
				$emailSender = $tableC->wert;
				$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
				$tableC->load($load);
				$emailSenderName = $tableC->wert;
				$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
				$tableC->load($load);
				$systemLink = $tableC->wert;
			endif;
			$mailer = JFactory::getMailer();
			$sender = array($emailSender,$emailSenderName);
			$mailer->setSender($sender);
			$mailer->addRecipient($email);
			//$mailer->addRecipient('ts@torstenscheel.de');
			$mailer->setSubject($subject);
			$mailer->isHtml(true);
			$template = str_replace('{first_name}',$first_name, $template->text);
			$template = str_replace('{last_name}',$last_name, $template);
			$template = str_replace('{salutation}',$salutation, $template);
			$template = str_replace('{email}',$email, $template);
			$template = str_replace('{classroomname}',$classroom->title, $template);
			$template = str_replace('{activationlink}', '<a style="color: #ff3600;" href="'.$systemLink.'/index.php?option=com_jclassroom&view=forgot&layout=reset&SMA='.base64_encode($theUserID).'">Passwort zurücksetzen</a>', $template);
			$template = str_replace('{systemname}', $systemName, $template);
			$body 	= $template;
			$mailer->setBody($body);
			$send = $mailer->Send();
			if ( $send !== true ) {
			    $return = 'Error sending email: ';
			} else {
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('Logs','JclassroomTable',array());
				$data = array();
				$data['userID'] 		= $theUserID;
				$data['parameter'] 		= 'Reset Passwort';
				$data['wert'] 			= 'Der Benutzer '.$first_name.' '.$last_name.' ('.$email.') hat das Zurücksetzen des Passworts angefordert';
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $theUserID;
				$table->bind($data);
				$table->store();
			    $return = 'An die E-Mail-Adresse <b>'.$email.'</b> wurde eine E-Mail versendet. <br/>Folgen Sie den Anweisungen in der E-Mail, um Ihr Passwort zurückzusetzen.';
			    JFactory::getApplication()->enqueueMessage($return, 'Message');
				//$this->setRedirect(JRoute::_('index.php?option=com_jclassroom&view=forgot&layout=reset', false));
			}
		endif;
	}
	function resetgo() {
		$input 	= JFactory::getApplication()->input;
		$id     = base64_decode($input->get('mark', '', 'STR'));
		echo $id;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$table->load($id);
		$name 	= $table->name;
		$email 	= $table->email;
		if($input->get('p1','','STR')):
			$password 			= JUserHelper::hashPassword($input->get('p1','','STR'));
			$table->password 	= $password;
		endif;
		$table->store();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Logs','JclassroomTable',array());
		$data = array();
		$data['userID'] 		= $id;
		$data['parameter'] 		= 'Reset Passwort';
		$data['wert'] 			= 'Der Benutzer '.$name.' ('.$email.') hat sein Passwort erfolgreich zurückgesetzt.';
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $id;
		$table->bind($data);
		$table->store();
		JFactory::getApplication()->enqueueMessage('Ihr Passwort wurde erfolgreich zurückgesetzt. Bitte melden Sie sich nun mit Ihrem Benutzernamen und Ihrem neuen Passwort an.', 'Message');
		$this->setRedirect(JRoute::_('login', false));
	}
}
?>