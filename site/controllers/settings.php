<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

class JclassroomControllerSettings extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'students';
				break;
			case 'trainer':
				$retour = JURI::Root().'students';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer/settings-customer';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer/settings-customer';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$session 	= JFactory::getSession();
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		echo '<pre>';
		print_r($input);
		$id 	= $input->get('id', 0, 'INT');
		$formData 	= new JInput($input->get('jform', '', 'array'));
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		if($formData->getStr('emailSender')):
			$this->saveToConfig($session->get('customerID'), 'emailSender', $formData->getStr('emailSender'));
		endif;
		if($formData->getStr('emailSenderName')):
			$this->saveToConfig($session->get('customerID'), 'emailSenderName', $formData->getStr('emailSenderName'));
		endif;
		
	}
	function saveToConfig($customerID, $parameter, $wert) {
		echo $parameter;
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('customerID' => $customerID, 'parameter' => $parameter);
		$check 	= $table->load($load);
		if(!$check):
			$data = array();
			$data['customerID'] 		= $customerID;
			$data['parameter'] 			= $parameter;
			$data['wert'] 				= $wert;
			$data['published'] 	= 1;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$table->bind($data);
			$table->store();
		else:
			$table->wert 			= $wert;
			$table->modified 		= date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		endif;
		

		return $table->id;
	}
	public function editToDatabase() {
		/*$input 		= JFactory::getApplication()->input;
		$tblResellerID 		= $input->get('id',0,'INT');
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Student','JclassroomTable',array());
		$load = array('tblUserID' => $formData->getInt('id'));
		$table->load($load);
		$table->salutation 	= $formData->getStr('salutation');
		$table->companyID 	= $formData->getInt('companyID');
		$table->salutation 	= $formData->getStr('salutation');
		$table->first_name 		= $formData->getStr('first_name');
		$table->last_name 		= $formData->getStr('last_name');
		$table->customer_number = $formData->getStr('customer_number');
		$table->company 	= $formData->getStr('company');
		$table->adress 		= $formData->getStr('adress');
		$table->postcode 	= $formData->getStr('postcode');
		$table->city		= $formData->getStr('city');
		$table->phone 		= $formData->getStr('phone');
		$table->fax 		= $formData->getStr('fax');
		$table->mobile 		= $formData->getStr('mobile');
		$table->email		= $formData->getStr('email');
		$table->web 		= $formData->getStr('web');
		if($savename) {
			$table->logo 		= $savename;
		}
		$table->asp_name 	= $formData->getStr('asp_name');
		$table->asp_phone	= $formData->getStr('asp_phone');
		$table->asp_email	= $formData->getStr('asp_email');
		$table->username 	= $formData->getStr('username');
		$table->password 	= $formData->getStr('password');
		$table->published 	= $formData->getInt('published');
		$table->modified	= strval($date);
		$table->modified_by	= $user->id;
		$table->store();
		if($formData->getStr('username')):
			$password 			= JUserHelper::hashPassword($formData->getStr('password'));
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('User','JclassroomTable',array());
			$table->load($formData->getInt('id'));
			$table->name 		= $formData->getStr('first_name').' '.$formData->getStr('last_name');
			$table->username 	= $formData->getStr('email');
			if($formData->get('password')):
				$table->password 	= $password;
			endif;
			$table->email 		= $formData->getStr('email');
			$table->store();
		endif;
		
		return $tblResellerID;*/
	}
	
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'student';

	protected $view_list = 'students';
}
?>