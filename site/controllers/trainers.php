<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
use Joomla\CMS\User\User;
/**
 * Kunden list controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerTrainers extends JControllerAdmin
{
	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'trainers';
	
	public function delete() {
		$input = JFactory::getApplication()->input;
		$cid = $input->get('cid', '', 'STR');
		if($cid) {
			foreach($cid as $delCid) {
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Trainer','JclassroomTable',array());
				$table->load($delCid);
				$tblUserID 	= $table->tblUserID;
				$logo 		= $table->logo;
				$table->delete($delCid);
				$user = User::getInstance($tblUserID);
				$user->delete();
				$db 		= JFactory::getDbo();
				$query 		= $db->getQuery(true);
				$sql 		= 'DELETE FROM jcr_user_usergroup_map WHERE user_id = '.$tblUserID;
				$db->setQuery($sql);
				$db->execute();
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableU 		= JTable::getInstance('User','JclassroomTable',array());
				$tableU->delete($tblUserID);
				
				if($logo) {
					JFile::delete(JPATH_ROOT.'/'.$logo);
				}
			}
		}
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = 'manager-administrator/trainer';
				break;
			case 'customer':
				$retour = 'manager-customer/trainer-customer';
				break;
			case 'trainer':
				$retour = 'manager-trainer';
				break;
		}
		JFactory::getApplication()->enqueueMessage('Die Datensätze wurden gelöscht', 'Message');
		$this->setRedirect(JRoute::_(JURI::Root().$retour, false));
	}
	function loadContent() {
		$input 		= JFactory::getApplication()->input;
		$trainerID 	= $input->get('trainerID', 0, 'INT'); 
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load 	= array('tblUserID' => $trainerID);
		$table->load($load);
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
        $query->where($db->quotename('a.created_by').' = '.$db->quote($trainerID));
		$db->setQuery($query);
		$classrooms = $db->loadObjectList();
		$html = '';
		if($classrooms):
			$html .= '<p style="font-weight: bold">Für den Trainer <i>'.$table->first_name.' '.$table->last_name.'</i> wurden folgende <span style="color: #ff3600;">Learningrooms</span> gefunden.</p>';
			foreach($classrooms as $classroom):
				$clr 	= '<b>(LR'.str_pad($classroom->id, 8,'0', STR_PAD_LEFT).')</b>'; 
				$html .= '<p>'.$clr.' '.$classroom->title.'</p>';
			endforeach;
		endif;
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_units','a'));
        $query->where($db->quotename('a.created_by').' = '.$db->quote($trainerID));
		$db->setQuery($query);
		$classrooms = $db->loadObjectList();
		if($classrooms):
			$html .= '<p style="font-weight: bold">Für den Trainer <i>'.$table->first_name.' '.$table->last_name.'</i> wurden folgende <span style="color: #ff3600;">Quizze</span> gefunden.</p>';
			foreach($classrooms as $classroom):
				$clr 	= '<b>(UID'.str_pad($classroom->id, 8,'0', STR_PAD_LEFT).')</b>'; 
				$html .= '<p>'.$clr.' '.$classroom->title.'</p>';
			endforeach;
		endif;
		echo $html;
		exit();
	}
	/**
	 * Get the admin model and set it to default
	 *
	 * @param   string           $name    Name of the model.
	 * @param   string           $prefix  Prefix of the model.
	 * @param   array			 $config  The model configuration.
	 */
	public function getModel($name = 'Trainer', $prefix='JclassroomModel', $config = array())
	{
		$config['ignore_request'] = true;
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>