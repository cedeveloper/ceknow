<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Fragenkataloge list controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerUnits extends JControllerAdmin
{
	public function delete() {
		$input = JFactory::getApplication()->input;
		$id = $input->get('id', 0, 'INT');
		$delete = $input->get('cid',array(), 'array');
		$db = JFactory::getDbo();
		if($delete) {
			foreach($delete as $del) {
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Unit','JclassroomTable',array());
				$table->load($del);
				$query = $db->getQuery(true);
				$query->select(array('a.id'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where('a.quizzID = '.$table->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				if($result):
					foreach($result as $item):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$pos = JTable::getInstance('Quizz_positions','JclassroomTable',array());
						$pos->load($item->id);
						$query = $db->getQuery(true);
						$query->select(array('a.id'));
				        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
						$query->where('a.questionID = '.$pos->id);
						$db->setQuery($query);
						$answers = $db->loadObjectList();
						if($answers):
							foreach($answers as $answer):
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$an = JTable::getInstance('Quizz_answers','JclassroomTable',array());
								$an->delete($answer->id);
							endforeach;
						endif;
						$query = $db->getQuery(true);
						$query->select(array('a.id'));
				        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
						$query->where('a.questionID = '.$pos->id);
						$db->setQuery($query);
						$answers = $db->loadObjectList();
						if($answers):
							foreach($answers as $answer):
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$an = JTable::getInstance('Quizz_options','JclassroomTable',array());
								$an->delete($answer->id);
							endforeach;
						endif;
						$query = $db->getQuery(true);
						$query->select(array('a.id'));
				        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
						$query->where('a.questionID = '.$pos->id);
						$db->setQuery($query);
						$answers = $db->loadObjectList();
						if($answers):
							foreach($answers as $answer):
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$an = JTable::getInstance('Quizz_checkboxes','JclassroomTable',array());
								$an->delete($answer->id);
							endforeach;
						endif;
						$pos->delete($item->id);
					endforeach;
				endif;
				$table->delete($del);
			}
		}
		$app = JFactory::getApplication();
		JFactory::getApplication()->enqueueMessage('Die ausgewählten Datensätze wurden gelöscht', 'message');
		$app->redirect(JURI::Root().'quizze');
	}
	function loadContent() {
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('unitID', 0, 'INT'); 
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($unitID);
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*,b.title'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_classrooms', 'b') . ' ON (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('b.id') . ')');
        $query->where($db->quotename('a.quizzID').' = '.$db->quote($unitID));
		$db->setQuery($query);
		$classrooms = $db->loadObjectList();
		$html = '';
		if($classrooms):
			$html .= '<p style="font-weight: bold">Für das Quizz <i>'.$table->title.'</i> wurden folgende <span style="color: #ff3600;">Learningrooms</span> gefunden.</p>';
			foreach($classrooms as $classroom):
				$clr 	= '<b>(LR'.str_pad($classroom->classroomID, 8,'0', STR_PAD_LEFT).')</b>'; 
				$html .= '<p>'.$clr.' '.$classroom->title.'</p>';
			endforeach;
		endif;
		echo $html;
		exit();
	}
	function export() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($id);
		$xml['quizz'] = array(
			'customerID'	=> $table->customerID,
			'title' 		=> $table->title, 
			'description' 	=> $table->description,
			'type' 			=> $table->type,
			'showTo' 		=> $table->showTo,
			'showToTrainer' => $table->showToTrainer,
			'sendToMail' 	=> $table->sendMail,
			'bgColor' 		=> $table->bgColor,
			'calculate' 	=> $table->calculate,
			'chart' 		=> $table->chart,
			'bestof_list'  	=> $table->bestof_list
		);
		$db = JFactory::getDbo();
		// Load the groups
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where('a.quizzID = '.$id);
		$query->where('a.type = 14');
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$groups[] 	= array(
					'id' 		=> $item->id,
					'groupID' 	=> $item->groupID,
					'type' 		=> $item->type,
					'title' 		=> $item->title,
					'points' 		=> $item->points,
					'content' 		=> $item->content,
					'infotextPositiv'	=> $item->infotextPositiv,
					'infotextNegativ'	=> $item->infotextNegativ,
					'todo' 			=> $item->todo,
					'testfield' 	=> $item->testfield,
					'theme' 		=> $item->theme,
					'infotext' 		=> $item->infotext,
					'kotext' 		=> $item->kotext,
					'countAnswers' 	=> $item->countAnswers,
					'correctAnswers'=> $item->correctAnswers,
					'chart' 		=> $item->chart,
					'ordering' 		=> $item->ordering,
					'groupOrdering'	=> $item->groupOrdering
				);
			endforeach;
		endif;
		$xml['groups']	= $groups;
		// Load the units
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where('a.quizzID = '.$id);
		$query->where('a.type <> 14');
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$positionsA = array();
				$positionsL = array();
				$positionsC = array();
				if($item->type == 3):
					// Load the answers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					$query->where('a.questionID = '.$item->id);
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$answers = $db->loadObjectList();
					if($answers):
						foreach($answers as $answer):
							$positionsA[] 	= array(
								'questionID' 	=> $answer->questionID,
								'title' 		=> $answer->title,
								'correct' 		=> $answer->correct,
								'ordering' 		=> $answer->ordering
							);
						endforeach;
					endif;
				endif;
				if($item->type == 12):
					// Load the answers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
					$query->where('a.questionID = '.$item->id);
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$lists = $db->loadObjectList();
					if($lists):
						foreach($lists as $list):
							$positionsL[] 	= array(
								'questionID' 	=> $list->questionID,
								'title' 		=> $list->title,
								'correct' 		=> $list->correct,
								'ordering' 		=> $list->ordering
							);
						endforeach;
					endif;
				endif;
				if($item->type == 13):
					// Load the answers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
					$query->where('a.questionID = '.$item->id);
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$checks = $db->loadObjectList();
					if($checks):
						foreach($checks as $check):
							$positionsC[] 	= array(
								'questionID' 	=> $check->questionID,
								'title' 		=> $check->title,
								'correct' 		=> $check->correct,
								'ordering' 		=> $check->ordering
							);
						endforeach;
					endif;
				endif;
				$positions[] 	= array(
					'groupID' 	=> $item->groupID,
					'type' 		=> $item->type,
					'title' 		=> $item->title,
					'points' 		=> $item->points,
					'content' 		=> $item->content,
					'infotextPositiv'	=> $item->infotextPositiv,
					'infotextNegativ'	=> $item->infotextNegativ,
					'todo' 			=> $item->todo,
					'testfield' 	=> $item->testfield,
					'theme' 		=> $item->theme,
					'infotext' 		=> $item->infotext,
					'kotext' 		=> $item->kotext,
					'countAnswers' 	=> $item->countAnswers,
					'correctAnswers'=> $item->correctAnswers,
					'chart' 		=> $item->chart,
					'ordering' 		=> $item->ordering,
					'groupOrdering'	=> $item->groupOrdering,
					'answers' 		=> $positionsA,
					'lists' 		=> $positionsL,
					'checkboxes' 	=> $positionsC
				);
				
			endforeach;
		endif;
		$xml['questions']	= $positions;
		$save = json_encode($xml);
		$rand = rand(100,10000);
		$pathSave = JPATH_SITE."/images/jclassroom/temp/xml_".$rand.".json";
		$downloadPath = JURI::Root()."images/jclassroom/temp/xml_".$rand.".json";
		file_put_contents($pathSave,$save);
		echo $downloadPath;
		exit();
	}
	function loadJSON() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$file  		= $input->files->get('uploadJSON');
		$fileName   = $file['name'];
		$src        = $file['tmp_name'];
        $dest       = JPATH_BASE.'/images/jclassroom/temp/'.$fileName;
        JFile::upload($src, $dest);
        $get = file_get_contents($dest);
        $xml = json_decode($get);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Unit','JclassroomTable',array());
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['title'] 				= $xml->quizz->title;
		$data['description'] 		= $xml->quizz->description;
		$data['type'] 				= $xml->quizz->type;
		$data['showTo'] 			= $xml->quizz->showTo;
		$data['showToTrainer'] 		= $xml->quizz->showToTrainer;
		$data['sendToMail'] 		= $xml->quizz->sendToMail;
		$data['bgColor'] 			= $xml->quizz->bgColor;
		$data['calculate'] 			= $xml->quizz->calculate;
		$data['chart'] 				= $xml->quizz->chart;
		$data['bestof_list'] 		= $xml->quizz->bestof_list;
		$data['published'] 	= 1;
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['showType'] 	= $session->get('group');
		$table->bind($data);
		$table->store();
		$newUnitID 	= $table->id;
		$newGroup = array();
		if($xml->groups):
			foreach($xml->groups as $question):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$data = array();
				$data['quizzID'] 		= $newUnitID;
				$data['groupID'] 		= 0;
				$data['type'] 			= $question->type;
				$data['title'] 			= $question->title;
				$data['points'] 		= $question->points;
				$data['content'] 		= $question->content;
				$data['infotextPositiv'] 		= $question->infotextPositiv;
				$data['infotextNegativ'] 		= $question->infotextNegativ;
				$data['todo'] 			= $question->todo;
				$data['testfield'] 		= $question->testfield;
				$data['theme'] 			= $question->theme;
				$data['infotext'] 		= $question->infotext;
				$data['kotext'] 		= $question->kotext;
				$data['countAnswers'] 	= $question->countAnswers;
				$data['chart'] 			= $question->chart;
				$data['ordering'] 		= $question->ordering;
				$data['groupOrdering'] 	= $question->groupOrdering;
				$table->bind($data);
				$table->store();
				$newGroup[] = array('oldGroupID' => $question->id, 'newGroupID' => $table->id);
			endforeach;
		endif;
		if($xml->questions):
			foreach($xml->questions as $question):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
				$data = array();
				$data['quizzID'] 		= $newUnitID;
				foreach($newGroup as $part):

					if($question->groupID == $part['oldGroupID']):
						echo 'oldGroupID '.$question->groupID.' '.$part['oldGroupID'].' '.$part['newGroupID'].'<br/>';
						$data['groupID'] 	= $part['newGroupID'];
					endif;
				endforeach;
				$data['type'] 				= $question->type;
				$data['title'] 				= $question->title;
				$data['points'] 			= $question->points;
				$data['content'] 			= $question->content;
				$data['infotextPositiv'] 		= $question->infotextPositiv;
				$data['infotextNegativ'] 		= $question->infotextNegativ;
				$data['todo'] 				= $question->todo;
				$data['testfield'] 			= $question->testfield;
				$data['theme'] 				= $question->theme;
				$data['infotext'] 			= $question->infotext;
				$data['kotext'] 			= $question->kotext;
				$data['countAnswers'] 		= $question->countAnswers;
				$data['chart'] 				= $question->chart;
				$data['ordering'] 			= $question->ordering;
				$data['groupOrdering'] 		= $question->groupOrdering;
				$table->bind($data);
				$table->store();
				$newQuestionID = $table->id;
				if($question->checkboxes):
					foreach($question->checkboxes as $checkbox):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$cb 		= JTable::getInstance('Quizz_checkboxes','JclassroomTable',array());
						$dataCB = array();
						$dataCB['questionID'] 	= $newQuestionID;
						$dataCB['title'] 		= $checkbox->title;
						$dataCB['correct'] 		= $checkbox->correct;
						$dataCB['ordering'] 	= $checkbox->ordering;
						$cb->bind($dataCB);
						$cb->store();
					endforeach;
				endif;
				if($question->answers):
					foreach($question->answers as $answer):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$an 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
						$dataAN = array();
						$dataAN['questionID'] 	= $newQuestionID;
						$dataAN['title'] 		= $answer->title;
						$dataAN['correct'] 		= $answer->correct;
						$dataAN['ordering'] 	= $answer->ordering;
						$an->bind($dataAN);
						$an->store();
					endforeach;
				endif;
				if($question->lists):
					foreach($question->lists as $list):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$li 		= JTable::getInstance('Quizz_options','JclassroomTable',array());
						$dataLI = array();
						$dataLI['questionID'] 	= $newQuestionID;
						$dataLI['title'] 		= $list->title;
						$dataLI['correct'] 		= $list->correct;
						$dataLI['ordering'] 	= $list->ordering;
						$li->bind($dataLI);
						$li->store();
					endforeach;
				endif;
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Das Quizz wurde erfolgreich angelegt', 'Message');
		$this->setRedirect(JURI::Root().'quizze');
	}
	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'units';
	
	/**
	 * Get the admin model and set it to default
	 *
	 * @param   string           $name    Name of the model.
	 * @param   string           $prefix  Prefix of the model.
	 * @param   array			 $config  The model configuration.
	 */
	public function getModel($name = 'Unit', $prefix='JclassroomModel', $config = array())
	{
		$config['ignore_request'] = true;
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>