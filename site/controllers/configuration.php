<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerConfiguration extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input 		= JFactory::getApplication()->input;
		$id   		= $input->get('id','','INT');
		$usertype 	= $input->get('usertype','', 'STR');
		$this->editToDatabase($usertype);
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'configuration';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer';
				break;
			case 'trainer':
				$retour = JURI::Root().'manager-trainer';
				break;
			case 'student':
				$retour = JURI::Root().'dashboard-students';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input 		= JFactory::getApplication()->input;
		$id   		= $input->get('id','','INT');
		$usertype 	= $input->get('usertype','', 'STR');
		$this->editToDatabase($usertype);
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'configuration';
				break;
			case 'customer':
				$retour = JURI::Root().'my-account';
				break;
			case 'trainer':
				$retour = JURI::Root().'my-account';
				break;
			case 'student':
				$retour = JURI::Root().'my-account';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function editToDatabase($usertype) {
		$session 	= JFactory::getSession();
		$group 		= $session->get('group');
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		if($group == 'superuser'):
			// SAVE TESTMODE
			$load 	= array('parameter' => 'testmode','customerID' => 0);
			$check 	= $table->load($load);
			$testmode = 0;
			if($formData->getStr('testmode')):
				$testmode = 1;
			endif;
			if($check):
				$table->wert 	 = $testmode;
				$table->modified = date('Y-m-d H:i:s');
				$table->modified_by 	= $user->id;
				$table->store();
			else:
				$data = array();
				$data['parameter'] 	= 'testmode';
				$data['wert'] 		= $testmode;
				$data['customerID'] = 0;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by']	= $user->id;
				$data['published'] 	= 1;
				$table->bind($data);
				$table->store();
			endif;
			// SAVE EMAIL TESTMODE
			$load 	= array('parameter' => 'testmode_email');
			$check 	= $table->load($load);
			if($check):
				$table->wert 	 = $formData->getStr('testmode_email');
				$table->modified = date('Y-m-d H:i:s');
				$table->modified_by 	= $user->id;
				$table->store();
			else:
				$data = array();
				$data['parameter'] 	= 'testmode_email';
				$data['wert'] 		= $formData->getStr('testmode_email');
				$data['customerID'] = 0;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by']	= $user->id;
				$data['published'] 	= 1;
				$table->bind($data);
				$table->store();
			endif;
		endif;
		// SAVE SYSTEMVERSION
		$load 	= array('parameter' => 'systemversion');
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('systemversion');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'systemversion';
			$data['wert'] 		= $formData->getStr('systemversion');
			$data['customerID'] = 0;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE SYSTEMDATE
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'systemdate');
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('systemdate');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'systemdate';
			$data['wert'] 		= $formData->getStr('systemdate');
			$data['customerID'] = 0;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE SYSTEMNAME
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'systemname');
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('systemname');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'systemname';
			$data['wert'] 		= $formData->getStr('systemname');
			$data['customerID'] = 0;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		$customerID = $session->get('customerID');
		// SAVE SYSTEMLINK
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'systemLink','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('systemLink');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'systemLink';
			$data['wert'] 		= $formData->getRaw('systemLink');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE EMAILSENDER
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailSender','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('emailSender');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'emailSender';
			$data['wert'] 		= $formData->getStr('emailSender');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE EMAILSENDERNAME
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailSenderName','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('emailSenderName');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'emailSenderName';
			$data['wert'] 		= $formData->getStr('emailSenderName');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE EMAILBODY
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailBody','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('emailBody');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'emailBody';
			$data['wert'] 		= $formData->getRaw('emailBody');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE EMAILSTYLES
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailStyles','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('emailStyles');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'emailStyles';
			$data['wert'] 		= $formData->getRaw('emailStyles');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE EMAILSCRIPTS
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailScripts','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('emailScripts');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'emailScripts';
			$data['wert'] 		= $formData->getRaw('emailScripts');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE TEMPLATE CONFIRM
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'description_confirm','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('description_confirm_mail');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'description_confirm';
			$data['wert'] 		= $formData->getStr('description_confirm_mail');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE TEMPLATE CONFIRM
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'subject_confirm','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('subject_confirm_mail');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'subject_confirm';
			$data['wert'] 		= $formData->getStr('subject_confirm_mail');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE TEMPLATE CONFIRM
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'text_confirm','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getRaw('text_confirm_mail');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'text_confirm';
			$data['wert'] 		= $formData->getRaw('text_confirm_mail');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE TEMPLATE PLAN
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'description_plan','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('description_plan_mail');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'description_plan';
			$data['wert'] 		= $formData->getStr('description_plan_mail');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE TEMPLATE CONFIRM
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'subject_plan','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getStr('subject_plan_mail');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'subject_plan';
			$data['wert'] 		= $formData->getStr('subject_plan_mail');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE TEMPLATE CONFIRM
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'text_plan','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getRaw('text_plan_mail');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'text_plan';
			$data['wert'] 		= $formData->getRaw('text_plan_mail');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		// SAVE ASP 
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'asp_text','customerID' => $customerID);
		$check 	= $table->load($load);
		if($check):
			$table->wert 	 = $formData->getRaw('asp_text');
			$table->modified = date('Y-m-d H:i:s');
			$table->modified_by 	= $user->id;
			$table->store();
		else:
			$data = array();
			$data['parameter'] 	= 'asp_text';
			$data['wert'] 		= $formData->getRaw('asp');
			$data['customerID'] = $customerID;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by']	= $user->id;
			$data['published'] 	= 1;
			$table->bind($data);
			$table->store();
		endif;
		return $tblResellerID;
	}
	function loadBasicTemplate() {
		$basic_template = file_get_contents(JURI::Root().'images/jclassroom/configuration/basic_template.txt');
        echo $basic_template;
        exit();
	}
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'student';

	protected $view_list = 'students';

}
?>