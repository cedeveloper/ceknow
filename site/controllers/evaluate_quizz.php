<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerEvaluate_quizz extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'manager-administrator/companys-admin';
				break;
			case 'trainer':
				$retour = JURI::Root().'celearning/students';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'company-edit?&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'company-edit?&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$user 	= JFactory::getUser();
		$session= JFactory::getSession();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$post 	= $input->get('jform', array(), 'array');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Company','JclassroomTable',array());
		$user 	= JFactory::getUser();
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['name'] 				= $post['name'];
		$data['address'] 			= $post['address'];
		$data['postcode'] 			= $post['postcode'];
		$data['city'] 				= $post['city'];
		$data['email'] 				= $post['email'];
		$data['web'] 				= $post['web'];
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= $post['published'];
		$table->bind($data);
		$table->store();
		$tblCustomerID 		= $table->id;

		return $tblCustomerID;
	}
	
	public function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Company','JclassroomTable',array());
		$table->load($input->get('id', 0, 'INT'));
		$table->name 		= $formData->getStr('name');
		$table->address 		= $formData->getStr('address');
		$table->postcode 	= $formData->getStr('postcode');
		$table->city		= $formData->getStr('city');
		$table->email		= $formData->getStr('email');
		$table->web			= $formData->getStr('web');
		$table->published 	= $formData->getInt('published');
		$table->modified	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		
		return $tblResellerID;
	}
	// Load result for Stage Quizz
	function loadQuizzCompleteResult() {
		$input 	= JFactory::getApplication()->input;
		$publishedQuizzID = $input->get('publishedQuizzID', 0, 'INT');
		// Load the published-quizz to get the quizzID
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Quizz','JclassroomTable',array());
		$table->load($publishedQuizzID);
		$quizzID = $table->quizzID;
		// Load the quizz to get the chart
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($quizzID);
		$chartID 	= $table->chart;
		JLoader::register('QuizzResultHelper',JPATH_COMPONENT_SITE.'/helpers/quizzResult.php');
		$result = new quizzResultHelper();
		$theQuestions = $result->loadQuizzResultComplete(false, false, $publishedQuizzID, $quizzID, $chartID,'open');
		echo json_encode($theQuestions);
		exit();
	}
	function saveChartImage() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id','','STR');
		$pathFL 	= JPATH_SITE.'/images/evaluates/eva'.$input->get('publishedQuizzID',0,'INT');
		JFolder::create($pathFL);
		$data = $input->get('image','','RAW');
		echo $data;
		//list($type, $data) = explode(';', $data);
		//list(, $data)      = explode(',', $data);
		$base64Image = trim($data);
    	$base64Image = str_replace('data:image/png;base64,', '', $base64Image);
		$data = base64_decode($base64Image);
		echo $data;
		file_put_contents($pathFL.'/'.$id.'.png', $data);
		echo 'OK';
		exit();
	}
	function savePreface() {
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Quizz','JclassroomTable',array());
		$table->load($input->get('publishedQuizzID', 0, 'INT'));
		$table->preface = $input->get('content', '', 'RAW');
		$table->store();
		echo 'OK';
		exit();
	}
	public function printQuizz() {
		$input = JFactory::getApplication()->input;
		$publishedQuizzID = $input->get('rID','','INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      	$unit      = JTable::getInstance('Quizz','JclassroomTable',array());
      	$unit->load($publishedQuizzID);
		require_once JPATH_SITE. '/components/com_jclassroom/library/quizz.php';
		$printResult = new printQuizzEngine();
		$printResult->printQuizz($unit->quizzID, 0, 0, 1, 0, $publishedQuizzID, $unit->quizzID);
	}
	public function printAudit() {
		$input = JFactory::getApplication()->input;
		$rID = $input->get('rID','','INT');
		require_once JPATH_SITE. '/components/com_jclassroom/library/quizz.php';
		$printAudit = new printQuizzEngine();
		$printAudit->printQuizz($rID, 0, 0, 0, 0);
	}
	public function printUmfrage() {
		$input = JFactory::getApplication()->input;
		$rID = $input->get('rID','','INT');
		require_once JPATH_SITE. '/components/com_jclassroom/library/quizz.php';
		$printAudit = new printQuizzEngine();
		$printAudit->printQuizz($rID, 0, 0, 0, 0);
	}
	
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'company';

	protected $view_list = 'companys-admin';

}
?>