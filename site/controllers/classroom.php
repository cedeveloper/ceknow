<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerClassroom extends JControllerForm
{
	public function add() {

		$retour = JURI::Root().'classroom-edit?layout=edit';
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = 'manager';
				$retour = JURI::Root().'classrooms';
				break;
			case 'trainer':
				$retour = JURI::Root().'classrooms';
				break;
			case 'customer':
				$retour = JURI::Root().'classrooms';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'classroom-edit?layout=edit&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'classroom-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'classroom-edit?layout=edit&id='.$id;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$formData 	= new JInput($input->get('jform', '', 'array'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$datum 	= JFactory::getDate();
		$date 	= new JDate($datum);
		$datum 	= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 	= JFactory::getUser();
		
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['title'] 				= $formData->getStr('title');
		$data['description'] 		= $formData->getRaw('description');
		$data['main_trainer'] 		= $formData->getInt('main_trainer');
		$data['co_trainer'] 		= $formData->getInt('co_trainer');
		$data['presentation'] 		= $formData->getInt('presentation');
		$data['hardware_order'] 	= $formData->getInt('hardware_order');
		$data['showChat'] 			= $formData->getInt('showChat');
		$data['companyID'] 			= $formData->getInt('companyID');
		$fromDate 	= new Datetime($formData->getStr('fromDate'));
		$toDate 	= new DateTime($formData->getStr('toDate'));
		$data['fromDate'] 		= $fromDate->format('Y-m-d');
		$data['toDate'] 		= $toDate->format('Y-m-d');
		$data['inventa_kursID'] 	= $formData->getInt('inventa_kursID');
		$data['inventa_terminID'] 	= $formData->getInt('inventa_terminID');
		$data['showTo'] 		= $formData->getInt('showTo');
		$data['showToUser'] 	= json_encode($formData->getStr('showToUser'));
		$data['visibleTo'] 		= $formData->getInt('visibleTo');
		$data['email_template'] = $formData->getInt('email_template');
		$data['verification_template'] = $formData->getInt('verification_template');
		$data['remember_template'] = $formData->getInt('remember_template');
		$data['invitations'] 		= $formData->getInt('invitations');
		$data['offsetRememberMails']= $formData->getInt('offsetRememberMails');
		$data['invitationsBy'] 		= $formData->getStr('invitationsBy');
		$data['certificate'] 		= $formData->getInt('certificate');
		$data['student_feedback'] 	= $formData->getInt('student_feedback');
		$data['student_feedback_anonym'] 	= $formData->getInt('student_feedback_anonym');
		$data['trainer_feedback'] 	= $formData->getInt('trainer_feedback');
		$data['published'] 	= $formData->getInt('published');
		$data['created'] 	= strval($date);
		$createdBy = $user->id;
		if($formData->getInt('created_by')):
			$createdBy = $formData->getInt('created_by');
		endif;
		$data['created_by']	= $createdBy;
		$data['logo'] 		= $savename;
		$table->bind($data);
		$table->store();
		$tblClassroomID 	= $table->id;
		// Create the Folder for learningroom
		$pathFL 	= JPATH_SITE.'/images/learningrooms/LR'.$id;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$classroomMaterial = array();
		$classroomMaterialStudent = array();
		if($files['material']): 
			foreach($files['material'] as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					$filename	= $pathFL.'/material/'.str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$tblClassroomID.'/material/'.str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file['tmp_name'], $dest);
					$classroomMaterial[] = array('name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		if($files['reserve']): 
			foreach($files['reserve'] as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					$filename	= $pathFL.'/material/reserve/'.str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$tblClassroomID.'/material/reserve/'.str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file['tmp_name'], $dest);
					$classroomReserve[] = array('name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		if($files['material_student']): 
			foreach($files['material_student'] as $key => $file):
				if($file[0]['name']):
					$filenameRaw 	= JFile::makeSafe($file[0]['name']);
					$filename	= $pathFL.'/material/'.str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$tblClassroomID.'/material/'.str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
					JFile::upload($file[0]['tmp_name'], $dest);
					$classroomMaterialStudent[] = array('studentID' => $key, 'name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		// Save the files
		if($classroomMaterial):
			foreach($classroomMaterial as $material):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['type'] 			= 'material';
				$data['classroomID'] 	= $tblClassroomID;
				$data['folder'] 		= '';
				$data['filename'] 		= $material['name'];
				$data['path'] 			= $material['path'];
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$table->bind($data);
				$table->store();
			endforeach;
		endif;
		if($classroomReserve):
			foreach($classroomReserve as $material):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['classroomID'] 	= $tblClassroomID;
				$data['type'] 			= 'reserve';
				$data['folder'] 		= '';
				$data['filename'] 		= $material['name'];
				$data['path'] 			= $material['path'];
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$data['published'] 		= 0;
				$table->bind($data);
				$table->store();
			endforeach;
		endif;
		if($classroomMaterialStudent):
			foreach($classroomMaterial as $material):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['type'] 			= 'material';
				$data['classroomID'] 	= $tblClassroomID;
				$data['studentID'] 		= $material['studentID'];
 				$data['folder'] 		= '';
				$data['filename'] 		= $material['name'];
				$data['path'] 			= $material['path'];
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$table->bind($data);
				$table->store();
			endforeach;
		endif;
		return $tblClassroomID;
	}
	public function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$id 		= $formData->getInt('id');
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		// Create the Folder for learningroom
		$pathFL 	= JPATH_SITE.'/images/learningrooms/LR'.$id;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$classroomMaterial = array();
		$classroomMaterialStudent = array();
		if($files['material']): 
			if(!JFolder::exists($pathFL.'/material')):
				JFolder::create($pathFL.'/material');
			endif;
			foreach($files['material'] as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					$filename	= str_replace(' ','-',$filenameRaw);
					$savename	= $pathFL.'/material/'.str_replace(' ','-',$filenameRaw);
					$pathTEMP 	= 'images/learningrooms/LR'.$id;
					$dest		= $savename;
					JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['type'] 			= 'material';
					$data['folder'] 		= '';
					$data['filename'] 		= $filename;
					$data['path'] 			= $pathTEMP.'/material/'.str_replace(' ','-',$filenameRaw);
					$data['created'] 		= strval($datum);
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$classroomMaterial[] = array('name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		if($files['reserve']): 
			if(!JFolder::exists($pathFL.'/reserve')):
				JFolder::create($pathFL.'/reserve');
			endif;
			foreach($files['reserve'] as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					$filename	= str_replace(' ','-',$filenameRaw);
					$savename	= $pathFL.'/reserve/'.str_replace(' ','-',$filenameRaw);
					$pathTEMP 	= 'images/learningrooms/LR'.$id;
					$dest		= $savename;
					JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['type'] 			= 'reserve';
					$data['folder'] 		= '';
					$data['filename'] 		= $filename;
					$data['path'] 			= $pathTEMP.'/reserve/'.str_replace(' ','-',$filenameRaw);
					$data['created'] 		= strval($datum);
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 0;
					$table->bind($data);
					$table->store();
					$classroomReserve[] = array('name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		if($files['material_student']): 
			JFolder::create($pathFL.'/material_students');
			foreach($files['material_student'] as $key => $file):
				if($file[0]['name']):
					$filenameRaw 	= JFile::makeSafe($file[0]['name']);
					$filename	= str_replace(' ','-',$filenameRaw);
					$savename	= 'images/learningrooms/LR'.$id.'/material_students/'.str_replace(' ','-',$filenameRaw);
					$dest		= $savename;
					JFile::upload($file[0]['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['studentID'] 		= $key;
					$data['type'] 			= 'student';
					$data['folder'] 		= '';
					$data['filename'] 		= $filename;
					$data['path'] 			= $savename;
					$data['created'] 		= strval($datum);
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$classroomMaterialStudent[] = array('studentID' => $key, 'name' => $filenameRaw,'path' => $savename);
				endif;
			endforeach;
		endif;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		$table->title				= $formData->getStr('title');
		$table->description 		= $formData->getRaw('description');
		$table->presentation 		= $formData->getInt('presentation');
		$table->hardware_order 		= $formData->getInt('hardware_order');
		$table->showChat 			= $formData->getInt('showChat');
		$table->main_trainer 		= $formData->getInt('main_trainer');
		$table->co_trainer 		= $formData->getInt('co_trainer');
		$table->companyID 		= $formData->getInt('companyID');
		$fromDate 	= new Datetime($formData->getStr('fromDate'));
		$toDate 	= new DateTime($formData->getStr('toDate'));
		$table->fromDate 	= $fromDate->format('Y-m-d');
		$table->toDate 		= $toDate->format('Y-m-d');
		$table->inventa_kursID 		= $formData->getInt('inventa_kursID');
		$table->inventa_terminID 	= $formData->getInt('inventa_terminID');
		/*if($classroomMaterial):
			$table->files 			= json_encode($classroomMaterial);
		endif;
		if($classroomReserve):
			$table->files_reserve 	= json_encode($classroomReserve);
		endif;
		if($classroomMaterialStudent):
			$table->files_students 	= json_encode($classroomMaterialStudent);
		endif;*/
		$table->showTo 		= $formData->getInt('showTo');
		$table->showToUser 	= json_encode($formData->getStr('showToUser'));
		$table->visibleTo 	= $formData->getInt('visibleTo');
		$table->email_template = $formData->getInt('email_template');
		$table->remember_template = $formData->getInt('remember_template');
		$table->verification_template = $formData->getInt('verification_template');
		$table->invitations = $formData->getInt('invitations');
		$table->offsetRememberMails = $formData->getStr('offsetRememberMails');
		$table->invitationsBy 	= $formData->getInt('invitationsBy');
		$table->certificate 	= $formData->getInt('certificate');
		$table->student_feedback 	= $formData->getInt('student_feedback');
		$table->student_feedback_anonym = $formData->getInt('student_feedback_anonym');
		$table->trainer_feedback	= $formData->getInt('trainer_feedback');
		$table->published 	= $formData->getStr('published');
		$table->created_by	= $formData->getInt('created_by');
		$table->modified 	= strval($date);
		$table->modified_by	= $user->id;
		$table->store();
	}
	function deleteFolder() {
		JFolder::delete(JPATH_SITE.'/images/learningrooms/LR83/reserve');
		echo 'OK';
		exit();
	}
	function createFolderTree() {
		$path = JPATH_SITE.'/images/learningrooms/LR76';
		$tree = JFolder::listFolderTree($path, $filter, $maxLevel = 4, $level = 0, $parent = 0);
		echo json_encode($tree);
		exit();
	}
	function writeUnit() {
		$input	= JFactory::getApplication()->input;
	}
	function addDays() {
		$input	= JFactory::getApplication()->input;
		$start 	= $input->get('start', '', 'STR');
		$end 	= $input->get('end', '', 'STR');
		$date 	= $start;
		$start 	= new DateTime($start);
		$end 	= new DateTime($end);
		$date 	= new DateTime($date);
		$interval = $start->diff($end);
		//$interval = $interval + 1;
		$html = '';
		$intervalDays 	= $interval->days;
		for($i = 0; $i <= $intervalDays; $i++) {
			$html .= '<div id="cardFor' .$date->format('d').'" class="card bg-light mb-2">';
			$html .= '<div class="card-header d-flex" style="justify-content: space-between">';
			$html .= '<i onclick="openDay(&quot;'.$date->format('d').'&quot;);" class="open fa fa-chevron-right"></i>';
			$html .= '<h2>'.$date->format('l, d.m.Y').'</h2>';
			$html .= '<a id="addFor'.$date->format('d').'" onclick="addUnit(&quot;'.$date->format('d').'&quot;);"><i style="font-size: 24px;border-radius: 50%;" class="fa fa-plus bg-success text-white p-1"></i></a>';
			$html .= '</div>';
			$html .= '<div class="card-body">';
			$html .= '</div>';
			$html .= '</div>';
			$date->add(new DateInterval('P1D'));
		}
		echo $html;
		exit();
	}
	function loadInvitationTemplate() {
		$input = JFactory::getApplication()->input;

		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('invitationTemplate', 0, 'INT')));
		$db->setQuery($query);
		$template = $db->loadobject();
		echo $template->text;
		exit();
	}
	function getTemplate() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$typ 			= $input->get('typ', 0, 'INT');
		$classroomID 	= $input->get('classroomID', '', 'INT');
		$dayID 			= $input->get('dayID', '', 'STR');
		$moduleID 		= $input->get('moduleID', '', 'INT');
		$unitCounter 	= $input->get('unitCounter', 0, 'INT');
		$library 		= $input->get('library', 0, 'INT');
		$unitID 		= $input->get('unitID', 0, 'INT');
		if($library == 0):
			//Save Unit 
			// 1. Load Max Ordering 
			$db 	= JFactory::getDbo(); 
			$query 	= $db->getQuery(true);
			$query->select('max(a.ordering)');
	        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->where($db->quotename('a.dayID').' = '.$db->quote($dayID));
	        $query->where($db->quotename('a.moduleID').' = '.$db->quote($moduleID));
			$db->setQuery($query);
			$nextOrder = $db->loadResult();
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
			$data = array();
			$data['classroomID'] = $classroomID;
			$data['dayID'] 		= $dayID;
			$data['moduleID'] 	= $moduleID;
			$data['unitID'] 	= $typ;
			$data['unitType'] 	= $typ;
			$data['content'] 	= '';
			$data['ordering'] 	= $nextOrder + 1;
			$data['created'] 	= date('Y-m-d H:i:s');
			$data['created_by'] = $user->id;
			$data['published']	= 1;
			$table->bind($data);
			$table->store();
			$unitID = $table->id;
		endif;
		$this->reorderUnits($classroomID);
		JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
        $template = new UnitsHelper();
        $template = $template->getTemplate('structure', $classroomID, $dayID, $typ, $unitID);

        echo $template;
        exit();
	}
	// Scripts for saving something
	function saveModule() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$moduleID 		= $input->get('moduleID', 0, 'INT');
		$title	 		= $input->get('title', '', 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$table->load($moduleID);
		$table->title 	= $title;
		$table->store();
		echo 'OK';
		exit();
	}
	function saveUnit() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		$title	 		= $input->get('title', '', 'STR');
		$duration	 	= $input->get('duration', 0, 'INT');
		$content	 	= $input->get('content', '', 'RAW');
		$link	 		= $input->get('link', '', 'STR');
		$quizz	 		= $input->get('quizz', '', 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$table->load($unitID);
		$table->title 	= $title;
		$table->duration 	= $duration;
		$table->content 	= $content;
		$table->link 		= $link;
		$table->quizzID 	= $quizz;
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		echo 'OK';
		exit();
	}
	function saveDayLibrary() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$dayID 		= $input->get('dayID', 0, 'INT');
		$title 		= $input->get('title', 0, 'STR');
		$description= $input->get('desc', 0, 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$day = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$day->load($dayID);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$library = JTable::getInstance('Classroom_days_library','JclassroomTable',array());
		$data 			= array();
		$data['classroomID'] 	= 0;
		$data['title'] 			= $title;
		$data['description']	= $description;
		$data['day'] 	= $day->day;
		$data['dayID'] 	= $day->dayID;
		$data['created']	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$library->bind($data);
		$library->store();
		$newDayID = $library->id;
		$this->copyModules($day->id,$newDayID, $day->classroomID);
		
		echo 'OK';
		exit();
	}
	function saveModuleLibrary() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$moduleID 		= $input->get('moduleID', 0, 'INT');
		$title	 		= $input->get('title', '', 'STR');
		$description	= $input->get('description', '', 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$module = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$module->load($moduleID);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$library = JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
		$data 			= array();
		$data['title'] 			= $title;
		$data['description'] 	= $description;
		$data['ordering'] 		= 0;
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$data['published'] 		= 1;
		$library->bind($data);
		$library->store();
		// Copy the units
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		$query->where('a.moduleID = '.$moduleID);
		$query->where('a.published = 1');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$unit = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
				$data 			= array();
				$data['moduleID'] 	= $library->id;
				$data['unitType'] 	= $item->unitType;
				$data['ordering'] 	= $item->ordering;
				$data['duration'] 	= $item->duration;
				$data['title'] 		= $item->title;
				$data['content'] 	= $item->content;
				$data['link'] 		= $item->link;
				$data['quizzID'] 	= $item->quizzID;
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$data['published'] 		= 1;
				$unit->bind($data);
				$unit->store();
				$newUnitID = $unit->id;
				// Copy the files
				$db 	= JFactory::getDbo(); 
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($item->id));
		        $query->order('a.id ASC');
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$file_library = JTable::getInstance('File_library','JclassroomTable',array());
						$data 			= array();
						$data['type'] 		= 'unit';
						$data['classroomID']= 0;
						$data['unitID'] 	= $newUnitID;
						$data['studentID'] 		= 0;
						$data['folder'] 		= '';
						$data['filename'] 		= $file->filename;
						$data['path'] 			= $file->path;
						$data['ordering'] 		= 0;
						$data['created'] 		= date('Y-m-d H:i:s');
						$data['created_by'] 	= $user->id;
						$data['published'] 		= 1;
						$file_library->bind($data);
						$file_library->store();
					endforeach;
				endif;
			endforeach;
		endif;
		echo $module->title;
		exit();
	}
	function saveUnitLibrary() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$unit = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$unit->load($unitID);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$library = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
		$data 			= array();
		$data['unitType'] 		= $unit->unitType;
		$data['title'] 			= $input->get('title', '','STR');
		$data['description'] 	= $input->get('description', '','STR');
		$data['content'] 		= $unit->content;
		$data['duration'] 		= $unit->duration;
		$data['link'] 			= $unit->link;
		$data['quizzID'] 		= $unit->quizzID;
		$data['ordering'] 		= 0;
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$data['published'] 		= 1;
		$library->bind($data);
		$library->store();
		$newUnitID = $library->id;
		// Copy the files
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_files','a'));
        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitID));
        $query->order('a.id ASC');
		$db->setQuery($query);
		$files = $db->loadObjectList();
		if($files):
			foreach($files as $file):
				$file_library = JTable::getInstance('File_library','JclassroomTable',array());
				$data 			= array();
				$data['type'] 		= 'unit';
				$data['classroomID']= 0;
				$data['unitID'] 	= $newUnitID;
				$data['studentID'] 		= 0;
				$data['folder'] 		= '';
				$data['filename'] 		= $file->filename;
				$data['path'] 			= $file->path;
				$data['ordering'] 		= 0;
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$data['published'] 		= 1;
				$file_library->bind($data);
				$file_library->store();
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function copyModules($oldDayID, $newDayID, $newLearningRoomID) {
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
        $query->where($db->quotename('a.dayID').' = '.$db->quote($oldDayID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$modules = $db->loadObjectList();
		if($modules):
			foreach($modules as $module):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
				$data = array();
				$data['classroomID']= 0;
				$data['dayID'] 		= $newDayID;
				$data['title'] 		= $module->title;
				$data['description'] = $module->description;
				$data['ordering'] 	= $module->ordering;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $module->published;
				$tableD->bind($data);
				$tableD->store();
				$newModuleID = $tableD->id;
				$this->copyUnits($module->id, $newModuleID, $newLearningRoomID, $newDayID);
			endforeach;
		endif;
	}
	function copyUnits($oldModuleID, $newModuleID, $newLearningRoomID, $newDayID) {
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
        $query->where($db->quotename('a.moduleID').' = '.$db->quote($oldModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
				$data = array();
				$data['classroomID']= 0;
				$data['dayID'] 		= $newDayID;
				$data['moduleID'] 		= $newModuleID;
				$data['unitID'] 		= $unit->unitID;
				$data['unitType'] 		= $unit->unitType;
				$data['ordering'] 		= $unit->ordering;
				$data['duration'] 		= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= 1;
				$tableD->bind($data);
				$tableD->store();
			endforeach;
		endif;
	}
	// Scripts for adding something
	function addNewDay() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$day 			= $input->get('day', 0, 'STR');
		$title 			= $input->get('title', 0, 'STR');
		//Get max ordering for classroom days
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.ordering');
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->order('a.ordering DESC');
        $query->setlimit(1);
		$db->setQuery($query);
		$lastOrdering = $db->loadResult();

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$load = array('classroomID' => $classroomID, 'day' => date('Y-m-d', strtotime($day)));
		$check = $table->load($load);
		if(!$check):
			$data = array();
			$data['classroomID'] 	= $classroomID;
			if($day):
				$data['day'] 			= date('Y-m-d', strtotime($day));
				$data['dayID'] 			= date('d', strtotime($day));
			endif;
			$data['title'] 			= $title;
			$data['ordering'] 		= $lastOrdering +1;
			$data['published']		= 1;
			$table->bind($data);
			$table->store();
			$dayID = $table->id;
			//Load Template

			echo $dayID;
		else:
			echo 'Fehler: Das Datum '.date('d.m.Y', strtotime($day)).' wurde für diesen Learningroom bereits angelegt.';
		endif;
		exit();
	}
	function addModule() {
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$classroomID 		= $input->get('classroomID', 0, 'INT');
		$dayID 				= $input->get('dayID', 0, 'INT');
		$saveFromLibrary 	= $input->get('saveFromLibrary', 0, 'INT');
		$copyFromModuleID 	= $input->get('copyFromModuleID', 0, 'INT');
		if($saveFromLibrary == 0):
			// Add the module to database
			JLoader::register('AddSomethingHelper',JPATH_COMPONENT_SITE.'/helpers/addSomething.php');
	    	$addModule 		= new AddSomethingHelper();
	    	$addModuleID 	= $addModule->addModule($saveFromLibrary, $classroomID, $dayID);
	    endif;
	    if($saveFromLibrary == 1):
			// Add the module to database
			// Returns the ID of the the new module
			JLoader::register('AddSomethingHelper',JPATH_COMPONENT_SITE.'/helpers/addSomething.php');
	    	$addModule 		= new AddSomethingHelper();
	    	$addModuleID 	= $addModule->addModuleFromLibrary($saveFromLibrary, $classroomID, $dayID, $copyFromModuleID);
	    	// Check if the dublicated module has some units to copy
	    	// Returns the complete template to insert it into the module
			JLoader::register('AddSomethingHelper',JPATH_COMPONENT_SITE.'/helpers/addSomething.php');
	    	$addUnits 		= new AddSomethingHelper();
	    	$unitsTemplate 	= $addUnits->addUnitsForModuleFromLibrary($classroomID, $dayID, $copyFromModuleID, $addModuleID);
	    endif;
		// Render the module
		JLoader::register('ModulesHelper',JPATH_COMPONENT_SITE.'/helpers/modules.php');
    	$template = new ModulesHelper();
    	$moduleTemplate = $template->getTemplate($classroomID, $dayID, $addModuleID, '');
    	// Render the units
    	$moduleTemplate = str_replace('{units}', $unitsTemplate, $moduleTemplate);
    	echo $moduleTemplate;
		exit();
	}
	function addDayFromLibrary() {
		$user 	= JFactory::getUser();
		$groups = JAccess::getGroupsByUser($user->id);
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.id,a.title,a.description'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_library','a'));
        if(in_array(8,$groups)):
        	//$query->where($db->quotename('a.created_by').' = '.$db->quote($user->id));
    	endif;
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$return[] = array('id' => $item->id, 'title' => $item->title, 'description' => $item->description);
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	function addModuleFromLibrary() {
		$user 	= JFactory::getUser();
		$groups = JAccess::getGroupsByUser($user->id);
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.id,a.title,a.description'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
        $query->where($db->quotename('a.dayID').' = '.$db->quote(0));
        if(in_array(8,$groups)):
        	//$query->where($db->quotename('a.created_by').' = '.$db->quote($user->id));
    	endif;
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$return[] = array('id' => $item->id, 'title' => $item->title, 'description' => $item->description);
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	function addUnitFromLibrary() {
		$session = JFactory::getSession();
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.id,a.title,a.description'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
        $query->where('a.customerID = '.$session->get('customerID'));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		echo json_encode($result);
		exit();
	}
	// Scripts for delete something
	function deleteDay() {
		$input 	= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->delete($dayID);
		//Delete Timeblock, if exist
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_timeblocks_themes','JclassroomTable',array());
		$load = array('classroomID' => $classroomID, 'themeID' => $dayID);
		$check = $table->load($load);
		if($check):
			$id = $table->id;
			$table->delete($id);
		endif;
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.dayID = '.$dayID);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
				// Load the days
		        $query = $db->getQuery(true);
				$query->select(array('a.id'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				$query->where('a.moduleID = '.$item->id);
				$db->setQuery($query);
				$units = $db->loadObjectList();
				if($units):
					foreach($units as $unit):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
						$table->delete($unit->id);
					endforeach;
				endif;
				$table->delete($item->id);
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function deleteModule() {
		$input = JFactory::getApplication()->input;
		$moduleID 		= $input->get('moduleID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$table->delete($moduleID);
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		$query->where('a.moduleID = '.$moduleID);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$table->delete($item->id);
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function deleteUnit() {
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$table->load($unitID);
		$classroomID = $table->classroomID;
		$table->delete($unitID);
		// Search for Images and delete them, if necassary
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_files','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitID));
		$db->setQuery($query);
		$files = $db->loadObjectList();
		if($files):
			foreach($files as $file):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('File','JclassroomTable',array());
				$table->delete($file->id);
				unlink(JPATH_SITE.$file->path);
			endforeach;
		endif;
		$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	// Scripts for inserting something
	function insertDayFromLibrary() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		$insertDayID 	= $input->get('insertDayID', 0, 'INT');

		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
        $query->where($db->quotename('a.dayID').' = '.$db->quote($insertDayID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$modules = $db->loadObjectList();
		if($modules):
			foreach($modules as $module):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_modules','JclassroomTable',array());
				$data = array();
				$data['classroomID']= $classroomID;
				$data['dayID'] 		= $dayID;
				$data['title'] 		= $module->title;
				$data['description'] = $module->description;
				$data['ordering'] 	= $module->ordering;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $module->published;
				$tableD->bind($data);
				$tableD->store();
				$newModuleID = $tableD->id;
				$this->insertUnitsFromLibrary($module->id, $newModuleID, $classroomID, $dayID);
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function insertUnitsFromLibrary($libraryModuleID, $newModuleID, $classroomID, $dayID) {
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
        $query->where($db->quotename('a.moduleID').' = '.$db->quote($libraryModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableU = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$data = array();
				$data['classroomID'] = $classroomID;
				$data['dayID'] 		= $dayID;
				$data['moduleID'] 	= $newModuleID;
				$data['unitType'] 	= $unit->unitType;
				$data['ordering'] 	= $unit->ordering;
				$data['duration'] 	= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$tableU->bind($data);
				$tableU->store();
				$newUnitID = $tableU->id;
				// check if the library-unit has some Files. If true, then copy the files, too.
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableF = JTable::getInstance('File_library','JclassroomTable',array());
				$load 	= array('unitID' => $unit->id);
				$check 	= $tableF->load($load);
				if($check):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$tableFC = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['type'] 			= $tableF->type;
					$data['classroomID'] 	= $classroomID;
					$data['unitID'] 	= $newUnitID;
					$data['folder'] 	= $tableF->folder;
					$data['filename'] 	= $tableF->filename;
					$data['path'] 		= $tableF->path;
					$data['created'] 	= date('Y-m-d H:i:s');
					$data['created_by'] = $user->id;
					$tableFC->bind($data);
					$tableFC->store();
				endif;
			endforeach;
		endif;
	}
	function insertModuleFromLibrary() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		$insertModuleID = $input->get('insertModuleID', 0, 'INT');
		$newModuleID 	= $input->get('newModuleID', 0, 'INT');
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($insertModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			$db 	= JFactory::getDbo(); 
			$query 	= $db->getQuery(true);
			$query->select('max(a.ordering)');
	        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->where($db->quotename('a.dayID').' = '.$db->quote($dayID));
			$db->setQuery($query);
			$nextOrder = $db->loadResult();
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableU = JTable::getInstance('Classroom_modules','JclassroomTable',array());
				$data = array();
				$data['classroomID'] = $classroomID;
				$data['dayID'] 		= $dayID;
				$data['title'] 		= $unit->title;
				$data['description']= $unit->description;
				$data['ordering'] 	= $nextOrder + 1;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= 1;
				$tableU->bind($data);
				$tableU->store();
				$return = array('moduleID' => $newModuleID,'typeID' => $unit->unitType,'unitID' => $tableU->id);
			endforeach;
		endif;
		//echo json_encode($return);
		echo $tableU->id;
		exit();
	}
	function insertUnitFromLibrary() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		$insertUnitID 	= $input->get('insertUnitID', 0, 'INT');
		$newModuleID 	= $input->get('newModuleID', 0, 'INT');
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($insertUnitID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			$db 	= JFactory::getDbo(); 
			$query 	= $db->getQuery(true);
			$query->select('max(a.ordering)');
	        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->where($db->quotename('a.dayID').' = '.$db->quote($dayID));
			$db->setQuery($query);
			$nextOrder = $db->loadResult();
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableU = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$data = array();
				$data['classroomID'] = $classroomID;
				$data['dayID'] 		= $dayID;
				$data['moduleID'] 	= $newModuleID;
				$data['unitType'] 	= $unit->unitType;
				$data['ordering'] 	= $nextOrder + 1;
				$data['duration'] 	= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= 1;
				$tableU->bind($data);
				$tableU->store();
				$return = array('moduleID' => $newModuleID,'typeID' => $unit->unitType,'unitID' => $tableU->id);
				// Load File From Library
				$query 	= $db->getQuery(true);
				$query->select('*');
		        $query->from($db->quoteName('#__jclassroom_files_library','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
		        $query->where($db->quotename('a.type').' = "unit"');
				$db->setQuery($query);
				$libFiles = $db->loadObjectList();
				if($libFiles):
					foreach($libFiles as $libfile):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableF = JTable::getInstance('File','JclassroomTable',array());
						$data = array();
						$data['type'] 		= 'unit';
						$data['classroomID']= $classroomID;
						$data['unitID'] 	= $tableU->id;
						$data['studentID'] 	= 0;
						$data['folder'] 	= $libfile->folder;
						$data['filename'] 	= $libfile->filename;
						$data['path'] 		= $libfile->path;
						$data['created'] 	= date('Y-m-d H:i:s');
						$data['created_by'] = $user->id;
						$data['published'] 	= 1;
						$tableF->bind($data);
						$tableF->store();
					endforeach;
				endif;
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	// Scripts for hiding something
	function hideDay() {
		$input = JFactory::getApplication()->input;
		$dayID 		= $input->get('dayID', 0, 'INT');
		$published 	= $input->get('published', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->load($dayID);
		$table->published 	= $published;
		$table->store();
		echo 'OK';
		exit();
	}
	function hideModule() {
		$input = JFactory::getApplication()->input;
		$moduleID 		= $input->get('moduleID', 0, 'INT');
		$published 		= $input->get('published', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$table->load($moduleID);
		$table->published 	= $published;
		$table->store();
		echo 'OK';
		exit();
	}
	function hideUnit() {
		$input = JFactory::getApplication()->input;
		$unitID 		= $input->get('unitID', 0, 'INT');
		$published 		= $input->get('published', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$table->load($unitID);
		$table->published 	= $published;
		$table->store();
		echo 'OK';
		exit();
	}
	// Scripts for loading something
	function loadAvailableDays() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$currentDay 	= $input->get('currentDay', '', 'STR');
		// LOAD THE DAYS
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id,a.title'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.id <> '.$currentDay);
		$query->order('a.day asc');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		if($days):
			foreach($days as $day):
				$return[] = array('id' => $day->id,'title' => $day->title);
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	function loadDay() {
		$input = JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$dayID 			= $input->get('dayID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->load($dayID);
		$theDate = $table->day;
		JLoader::register('DayHelper',JPATH_COMPONENT_SITE.'/helpers/day.php');
        $template = new DayHelper();
        $day = $template->getTemplate($theDate, $dayID);
        // LOAD THE MODULES
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id,a.title'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.dayID = '.$dayID);
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$modules = $db->loadObjectList();
		if($modules):
			$moduleTemplate = '';
			foreach($modules as $module):
	        	JLoader::register('ModulesHelper',JPATH_COMPONENT_SITE.'/helpers/modules.php');
	        	$template = new ModulesHelper();
	        	$moduleTemplate .= $template->getTemplate($classroomID, $dayID, $module->id, $module->title);
	        	// LOAD THE UNITS
		        $query = $db->getQuery(true);
				$query->select(array('a.id,a.unitType'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				$query->where('a.classroomID = '.$classroomID);
				$query->where('a.moduleID = '.$module->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$units = $db->loadObjectList();
				if($units):
					foreach($units as $unit):
						$query = $db->getQuery(true);
						$query->select(array('a.*'));
				        $query->from($db->quoteName('#__jclassroom_files','a'));
						$query->where('a.classroomID = '.$classroomID);
						$query->where('a.unitID = '.$unit->id);
						$query->where('a.type = "unit"');
						$db->setQuery($query);
						$files = $db->loadObjectList();
	        			JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
						$template = new UnitsHelper();
						$unitTemplate .= $template->getTemplate('structure', $classroomID, $dayID, $unit->unitType,$unit->id);
					endforeach;
				endif;
				$moduleTemplate = str_replace('{units}', $unitTemplate, $moduleTemplate);
				$unitTemplate = '';
				$html = str_replace('{modules}', $moduleTemplate, $day);
			endforeach;
        else:
        	$html = str_replace('{modules}', '', $day);
        endif;
       	echo $html;

       	exit();
	}
	function loadTermine() {
		$now 	= date('Y-m-d');
		$input 	= JFactory::getApplication()->input;
		// Load Data from INVENTA
		$option = array(); 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede01';    	// User for database authentication
		$option['password'] = 'sbZ78p5etz';   // Password for database authentication
		$option['database'] = 'cede01';    	// Database name
		$option['prefix']   = 'web3_';     // Database prefix (may be empty)
		$return = array();
		$db = JDatabaseDriver::getInstance( $option );
		$query = $db->getQuery(true);
		$query->select(array('a.id,a.termin_beginn, a.termin_ende'));
        $query->from($db->quoteName('#__inventa_termine','a'));
        $query->where($db->quotename('a.kurs').' = '.$db->quote($input->get('kursID')));
        $query->where($db->quotename('a.termin_beginn').' >= '.$db->quote($now));
		$query->where('a.aktiv = 1');
		$query->order('a.termin_beginn asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$beginn = new datetime($item->termin_beginn);
				$ende = new datetime($item->termin_ende);
				$return[] = array('title' => '', 'id' => $item->id, 'beginn' => $beginn->format('d.m.Y'), 'ende' => $ende->format('d.m.Y'));
			endforeach;
		else:
			$return[] = array('title' => 'Keine Termine gefunden','id' => 0, 'beginn' => '', 'ende' => '');
		endif;

		echo json_encode($return);
		exit();
	}
	function loadKursData() {
		$input 	= JFactory::getApplication()->input;
		// Load Data from INVENTA
		$option = array(); 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede01';    	// User for database authentication
		$option['password'] = 'sbZ78p5etz';   // Password for database authentication
		$option['database'] = 'cede01';    	// Database name
		$option['prefix']   = 'web3_';     // Database prefix (may be empty)
		$return = array();
		$db = JDatabaseDriver::getInstance( $option );
		$query = $db->getQuery(true);
		$query->select(array('a.inhalt'));
        $query->from($db->quoteName('#__inventa_kurse','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('kursID')));
		$db->setQuery($query);
		$result = $db->loadResult();

		echo $result;
		exit();
	}
	function loadQuizzResult() {
		$input 		= JFactory::getApplication()->input;
		$theResultID= $input->get('theResultID', 0, 'INT');
		// Get the result
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Theresults','JclassroomTable',array());
		$result->load($theResultID);
		// Get Username
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$user = JTable::getInstance('User','JclassroomTable',array());
		$user->load($result->created_by);
		// Get Quizz
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quizz = JTable::getInstance('Unit','JclassroomTable',array());
		$quizz->load($result->quizzID);
		JLoader::register('ResultHelper',JPATH_COMPONENT_SITE.'/helpers/result.php');
		$result = new ResultHelper();
		$html = $result->getResult($theResultID);
		$return = array(
			'quizzTitle' => $quizz->title,
			'quizzDescription' => $quizz->description,
			'username' 	=> $user->name,
			'html' 		=> $html
		);
		echo json_encode($return);
		exit();
	}
	function loadQuizzCompleteResult() {
		$input 	= JFactory::getApplication()->input;
		// Load the quizz to get the chart
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($input->get('quizzResultID',0,'INT'));
		$chartID 	= $table->chart;
		
		//if($chartID == 2 || $chartID == 3 || $chartID == 4):
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(array('
				c.title,
				c.content,
				c.chart,
				c.type,
				c.id as questionID
			'));
	        $query->from($db->quoteName('#__jclassroom_theresults','a'));
	        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.quizzID') . ')');
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($input->get('classroomID',0,'INT')));
	        $query->where($db->quotename('a.unitID').' = '.$db->quote($input->get('unitID',0,'INT')));
	        $query->where($db->quotename('a.quizzID').' = '.$db->quote($input->get('quizzResultID',0,'INT')));
	        $query->where($db->quotename('c.type').' <> '.$db->quote(1));
	        $query->group('c.id');
			$db->setQuery($query);
			$questions = $db->loadObjectList();
			//print_r($questions);
			if($questions):
				foreach($questions as $question):
					$labels 	= array();
					$datas 		= array();
					$indiAnswers= '';
					if($question->chart && $question->chart != 'wordCloud'):
						if($question->type == 3 || $question->type == 5):
							$query = $db->getQuery(true);
							$query->select(array('
								a.title,a.id
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= $answer->title;
								$query = $db->getQuery(true);
								$query->select(array('
									a.id
								'));
								$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						        $query->where($db->quotename('a.answerID').' = '.$db->quote($answer->id));
						        $query->where($db->quotename('b.classroomID').' = '.$db->quote($input->get('classroomID',0,'INT')));
						        $query->where($db->quotename('b.unitID').' = '.$db->quote($input->get('unitID',0,'INT')));
						        $query->order('a.created DESC');
								$db->setQuery($query);
								$db->execute();
								$numRows = $db->getNumRows();
								$answersIS 	= $db->loadObject();
								$datas[] 	= $numRows;
							endforeach;
						endif;
						if($question->type == 4):
							$answersIS = 0;
							$labels 	= array('Sehr schlecht', 'Schlecht','Nicht schlecht','Gut','Sehr gut');
							for($i = 1;$i <= 5;$i++) {
								$query = $db->getQuery(true);
								$query->select(array('
									count(b.answerID) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quotename('b.answerID').' = '.$db->quote($i));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($input->get('classroomID',0,'INT')));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($input->get('unitID',0,'INT')));
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								if($answersIS):
									$datas[] 	= $answersIS;
								else:
									$datas[] 	= 0;
								endif;
							}
						endif;
						if($question->type == 12):
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= $answer->title;
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($input->get('classroomID',0,'INT')));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($input->get('unitID',0,'INT')));
						        $query->order('a.created DESC');
						        $query->setLimit(1);
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
							endforeach;
						endif;
						if($question->type == 13):
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= $answer->title;
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_theresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
						        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
						        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
								$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
						        $query->where($db->quotename('a.classroomID').' = '.$db->quote($input->get('classroomID',0,'INT')));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($input->get('unitID',0,'INT')));
						        $query->order('a.created DESC');
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
							endforeach;
						endif;
					endif;
					if($question->chart && $question->chart == 'wordCloud'):
						$query = $db->getQuery(true);
						$query->select(array('
							count(a.id) as data,a.content
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
				        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
				        $query->where($db->quotename('b.classroomID').' = '.$db->quote($input->get('classroomID',0,'INT')));
				        $query->where($db->quotename('a.unitID').' = '.$db->quote($input->get('unitID',0,'INT')));
						$db->setQuery($query);
						$answersIS = $db->loadObjectList();
						if($answersIS):
							foreach($answersIS as $answer):
								$datas[] 	= array(
									'text' 		=> $answer->content, 
									'weight' 	=> $answer->data,
									'color'		=> '#ff3600'
								);
							endforeach;
						endif;
					endif;
					if($question->chart == ''):
						if($question->type == 11):
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								group_concat(b.content SEPARATOR "<br/>") as indiAnswers
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
					        $query->where($db->quotename('a.classroomID').' = '.$db->quote($input->get('classroomID',0,'INT')));
					        $query->where($db->quotename('a.unitID').' = '.$db->quote($input->get('unitID',0,'INT')));
					        $query->where('b.content <> ""');
							$db->setQuery($query);
							$answersIS = $db->loadObject();
							if($answersIS):
								$indiAnswers 	= $answersIS->indiAnswers;
							else:
								$indiAnswers 	= '';
							endif;
	
						endif;
					endif;
					$theQuestions[] = array(
						'question' 	=> $question->title,
						'content'	=> $question->content,
						'chart' 	=> $question->chart,
						'labels' 	=> $labels,
						'datas' 	=> $datas,
						'indiAnswers'	=> $indiAnswers
					);
				endforeach;
			endif;
		//endif;
		echo json_encode($theQuestions);
		exit();
	}
	// Load result for Stage Quizz
	function loadQuizzStageCompleteResult() {
		$input 	= JFactory::getApplication()->input;
		// Load the quizz to get the chart
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Unit','JclassroomTable',array());
		$table->load($input->get('quizzResultID',0,'INT'));
		$chartID 	= $table->chart;
		
		//if($chartID == 2 || $chartID == 3 || $chartID == 4):
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(array('
				c.title,
				c.chart,
				c.type,
				c.id as questionID
			'));
	        $query->from($db->quoteName('#__jclassroom_theresults','a'));
	        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.quizzID') . ')');
	        $query->where($db->quotename('a.quizzID').' = '.$db->quote($input->get('quizzResultID',0,'INT')));
	        $query->group('c.id');
			$db->setQuery($query);
			$questions = $db->loadObjectList();
			if($questions):
				foreach($questions as $question):
					$labels 	= array();
					$datas 		= array();
					if($question->chart && $question->chart != 'wordCloud'):
						if($question->type == 3 || $question->type == 5):
							$query = $db->getQuery(true);
							$query->select(array('
								a.title,a.id
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= $answer->title;
								$query = $db->getQuery(true);
								$query->select(array('
									a.id
								'));
								$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
						        $query->where($db->quotename('a.answerID').' = '.$db->quote($answer->id));
								$db->setQuery($query);
								$db->execute();
								$numRows = $db->getNumRows();
								$answersIS 	= $db->loadObject();
								$datas[] 	= $numRows;
							endforeach;
						endif;
						if($question->type == 13):
							$answersIS = 0;
							$query = $db->getQuery(true);
							$query->select(array('
								a.id,
								a.title
							'));
					        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
							$db->setQuery($query);
							$answersSOLL = $db->loadObjectList();
							foreach($answersSOLL as $answer):
								$labels[] 	= $answer->title;
								$query = $db->getQuery(true);
								$query->select(array('
									count(a.id) as data
								'));
						        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
						        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						        $query->where($db->quotename('a.answerID').' = '.$db->quote($answer->id));
								$db->setQuery($query);
								$answersIS = $db->loadResult();
								$datas[] 	= $answersIS;
							endforeach;
						endif;
					endif;
					if($question->chart && $question->chart == 'wordCloud'):
						$query = $db->getQuery(true);
						$query->select(array('
							count(a.id) as data,a.content
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
				        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersIS = $db->loadObjectList();
						if($answersIS):
							foreach($answersIS as $answer):
								$datas[] 	= array(
									'text' 		=> $answer->content, 
									'weight' 	=> $answer->data,
									'color'		=> '#ff3600'
								);
							endforeach;
						endif;
					endif;
					$theQuestions[] = array(
						'question' => $question->title,
						'chart' => $question->chart,
						'labels' => $labels,
						'datas' => $datas
					);
				endforeach;
			endif;
		//endif;
		echo json_encode($theQuestions);
		exit();
	}
	function loadSingleChart() {
		$input 		= JFactory::getApplication()->input;
		$theResultID= $input->get('theResultID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table   = JTable::getInstance('Theresults','JclassroomTable',array());
		$table->load($theResultID);
		// Load the Quizzdata
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$unit   = JTable::getInstance('Unit','JclassroomTable',array());
		$unit->load($table->quizzID);
		// The Result in details
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.classroomID,
			a.quizzID,
			a.created as resultCreated,
			a.created_by as resultBy,
			a.id as resultID,
			c.title as quizzTitle,
			d.id as questionID,
			d.title as questionTitle,
			d.type as questionType,
			d.points
		'));
		$query->from($db->quoteName('#__jclassroom_theresults','a'));
		//$query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_units', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'd') . ' ON (' . $db->quoteName('c.id') . ' = ' . $db->quoteName('d.quizzID') . ')');
		$query->where($db->quotename('a.id').' = '.$db->quote($theResultID));
		//$query->where($db->quotename('b.questionID').' = '.$db->quote(108));
		$query->order('d.ordering ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
      	foreach($result as $item):
         	$query   = $db->getQuery(true);
	         if($item->questionType == 3 || $item->questionType == 5 || $item->questionType == 12 || $item->questionType == 13):
	            //echo 'QUESTIONTYPE: '.$item->questionType.'<br/>';
	            switch($item->questionType):
	               	case 13:
						$query->select(array('a.id,a.title,a.correct'));
						$query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
						$query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
						break;
	               	case 3:
						$query->select(array('a.id,a.title,a.correct'));
						$query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
						$query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
						break;
	               	case 5:
						$query->select(array('a.id,a.title,a.correctAnswers as correct'));
						$query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
						$query->where($db->quotename('a.id').' = '.$db->quote($item->questionID));
						break;
					case 12:
						$query->select(array('a.id,a.title,a.correct'));
						$query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
						$query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
						break;
	            endswitch;
	            $db->setQuery($query);
	            $answersSoll = $db->loadObjectList();
            	if($answersSoll):
	               	$countCAS = 0;
	               	$countCAI = 0;
	               	foreach($answersSoll as $answer):
	                  	$correct = 0;
	                  	if($answer->correct == 1):
	                    	$correct = 1;
	                     	$countCAS++;
	                  	endif;
	                  	$db = JFactory::getDbo();
						$query = $db->getQuery(true);
						$query->select(array('
							a.answerID,
							a.type,
							a.content
						'));
						$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
						$query->where($db->quotename('a.theResultID').' = '.$db->quote($theResultID));
						$query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
						$db->setQuery($query);
						$as = $db->loadObjectList();
	                  	foreach($as as $answerIS):
                     		switch($answerIS->type):
                     			case 5:
                     				if($correct == 0 && $answerIS->content == 'answerJ'):
                     					$countCAI += 1;
                     				endif;
                     				if($correct == 1 && $answerIS->content == 'answerN'):
                     					$countCAI += 1;
                     				endif;
                     				break;
                     			default:
                     				if(($item->questionType == $answerIS->type) && $answer->id == $answerIS->answerID):
			                        	if($correct == 1):
			                           		$countCAI++;
			                        	endif;
			                        endif;
		                        	break;
		                    endswitch;
	                  	endforeach;
               		endforeach;
            	endif;
         	endif;
			if($unit->calculate == 2):
				$sumMax  += $item->points;
				$points  = ($countCAI * $item->points) / $countCAS;
				$sumReal += $points;
			endif;
      	endforeach;
      	$labels[] = 'Richtig';
      	$labels[] = 'Falsch';
      	$datas[] = round(($sumReal * 100) / $sumMax);
      	$datas[] = round((($sumMax - $sumReal) * 100) / $sumMax);
      	$return = array('labels' => $labels,'datas' => $datas,'chart' => 'pie','calculate' => $unit->calculate);
      	echo json_encode($return);
      	exit();
	}
	function getQuizzData() {
		$input 	= JFactory::getApplication()->input;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_units','a'));
        $query->where($db->quotename('a.id').' = '.$db->quote($input->get('quizzResultID',0,'INT')));
		$db->setQuery($query);
		$result = $db->loadObject();
		echo json_encode($result);
		exit();
	}
	function deleteQuizzResult() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Quizz_results','JclassroomTable',array());
		$table->delete($input->get('quizzResultID',0,'INT'));
		echo 'OK';
		exit();
	}
	// Scripts for sorting something
	function writeSortableTimeblocks() {
		$input = JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		$classroomID= $input->get('classroomID', 0,'INT');
		if($orders):
			$i = 1;
			foreach($orders as $order):
				$themeID 	= substr($order,5);
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_timeblocks_themes','JclassroomTable',array());
				$load = array('classroomID' => $classroomID, 'themeID' => $themeID);
				$table->load($load);
				$table->ordering 	= $i;
				$table->store();
				$i++;
			endforeach;
		endif;
		//$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	function writeSortableThemes() {
		$input = JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		$classroomID= 0;
		if($orders):
			$i = 1;
			foreach($orders as $order):
				$dayID 	= substr($order,7);
				echo $dayID;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_days','JclassroomTable',array());
				$table->load($dayID);
				$table->ordering 	= $i;
				$table->store();
				$classroomID = $table->classroomID;
				$i++;
			endforeach;
		endif;
		$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	function writeSortableUnits() {
		$input = JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		$classroomID= 0;
		if($orders):
			foreach($orders as $order):
				$unitID 	= explode('_', $order);
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$table->load($unitID[1]);
				$table->ordering 	= $unitID[0];
				$table->store();
				$classroomID = $table->classroomID;
			endforeach;
		endif;
		$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	function writeSortableModules() {
		$input 		= JFactory::getApplication()->input;
		$orders 	= $input->get('order', 0, 'STR');
		$classroomID= 0;
		if($orders):
			foreach($orders as $order):
				$unitID 	= explode('_', $order);
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_modules','JclassroomTable',array());
				$table->load($unitID[1]);
				$table->ordering 	= $unitID[0];
				$table->store();
				$classroomID = $table->classroomID;
			endforeach;
		endif;
		$this->reorderUnits($classroomID);
		echo 'OK';
		exit();
	}
	function reorderUnits($classroomID) {
		$db = JFactory::getDbo();
		$i = 1;
		$query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		foreach($days as $day):
			// Load the modules
			$query = $db->getQuery(true);
			$query->select(array('a.id'));
	        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
	        $query->where($db->quotename('a.dayID').' = '.$db->quote($day->id));
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->order('a.ordering ASC');
			$db->setQuery($query);
			$modules = $db->loadObjectList();
			foreach($modules as $module):
				$query = $db->getQuery(true);
				$query->select(array('a.id'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		        $query->where($db->quotename('a.moduleID').' = '.$db->quote($module->id));
		        $query->order('a.ordering ASC');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				foreach($result as $item):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
					$table->load($item->id);
					$table->ordering = $i;
					$table->store();
					$i++;
				endforeach;
			endforeach;
		endforeach;
	}
	// Scripts for moving something
	function moveDayToDay() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$dayToMove 		= $input->get('dayToMove', 0, 'INT');
		$moveDayToDay 	= $input->get('moveDayToDay', 0, 'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->load($dayToMove);
		$table->day 		= date('Y-m-d', strtotime($moveDayToDay));
		$table->dayID		= date('d', strtotime($moveDayToDay));
		$table->store();

		echo date('d.m.Y', strtotime($moveDayToDay));
		exit();
	}
	function moveModuleToDay() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$moduleToMove 		= $input->get('moduleToMove', 0, 'INT');
		$moveModuleToDay 	= $input->get('moveModuleToDay', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$table->load($moduleToMove);
		$table->dayID		= $moveModuleToDay;
		$table->store();

		echo 'OK';
		exit();
	}
	function moveUnitToModule() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$unitID 	= $input->get('unitToMove', 0, 'INT');
		$newModuleID 	= $input->get('newModuleID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
		$table->load($unitID);
		$table->moduleID 	= $newModuleID;
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by = $user->id;
		$table->store();

		echo 'OK';
		exit();
	}
	function deleteStudent() {
		$input 	= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$studentID 		= $input->get('studentID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$load 	= array('classroomID' => $classroomID, 'userID' => $studentID);
		$table->load($load);
		$studentID = $table->id;
		$table->delete($studentID);

		echo $result;
		exit();
	}
	function verificationToOne() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$studentID 		= $input->get('studentID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('a.userID').' = '.$db->quote($studentID));
		$db->setQuery($query);
		$student 	= $db->loadObject();
		$return 	= '';
		$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
		$set = $this->vertifying($classroomID, $student);
		$return .= $set.'<br/>';
		$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
		$return .= '</div>'; 
		echo $return;
		exit();
	}
	function verificationToAll() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$students 	= $db->loadObjectList();
		$return 	= '';
		if($students):
			$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
			foreach($students as $student):
				$set = $this->vertifying($classroomID, $student);
				$return .= $set.'<br/>';
			endforeach;
			$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
			$return .= '</div>'; 
		endif; 
		echo $return;
		exit();
	}
	function vertifying($classroomID, $student) {
		$session = JFactory::getSession();
		$customerID = $session->get('customerID');
		$datum 	= JFactory::getDate();
		$date 	= new JDate($datum);
		$datum 	= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 	= JFactory::getUser();
		//Load the Student, if possible
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$studentU 	= JTable::getInstance('Student','JclassroomTable',array());
		$load 		= array('tblUserID' => $student->userID);
		$checkStudent = $studentU->load($load);
		if($checkStudent):
			$usergroup 		= 'student';
			$salutation 	= $studentU->salutation;
			$first_name 	= $studentU->first_name;
			$last_name 		= $studentU->last_name;
			$recipient 		= $studentU->email;
			$userID 		= $studentU->tblUserID;
		endif;
		//Load the Trainer, if possible
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainerU 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load 		= array('tblUserID' => $student->userID);
		$checkTrainer = $trainerU->load($load);
		if($checkTrainer):
			$usergroup = 'trainer';
			$salutation 	= $trainerU->salutation;
			$first_name 	= $trainerU->first_name;
			$last_name 		= $trainerU->last_name;
			$recipient 		= $studentU->email;
			$userID 		= $trainerU->tblUserID;
		endif;
		//Load the Administrator, if possible
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$adminU 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
		$load 		= array('userID' => $student->userID);
		$checkAdmin = $adminU->load($load);
		if($checkAdmin):
			$usergroup = 'administrator';
			$salutation 	= $adminU->salutation;
			$first_name 	= $adminU->first_name;
			$last_name 		= $adminU->last_name;
			$recipient 		= $studentU->email;
			$userID 		= $adminU->tblUserID;
		endif;
		if(!$recipient): 
			//Load the User
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$userU 	= JTable::getInstance('User','JclassroomTable',array());
			$load 	= array('id' => $student->userID);
			$userU->load($load);
			$salutation 	= '';
			$first_name 	= '';
			$last_name 		= $userU->name;
			$recipient 		= $userU->email;
			$userID 		= $userU->tblUserID;
		endif;
		//Load the classroom
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$classroom 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$load 	= array('id' => $classroomID);
		$classroom->load($load);
		$verification_template = $classroom->verification_template;
		//Load the template
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.text,a.subject'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.id').' = '.$db->quote($verification_template));
		$db->setQuery($query);
		$template = $db->loadObject();
		$subject  = $template->subject;
		//Write to Emails
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Email','JclassroomTable',array());
		$data 		= array();
		$data['typ'] = 2;
		$data['send'] 	= date('Y-m-d H:i:s');
		$data['send_by'] 	= $user->id;
		$data['send_to'] 	= $userID;
		$data['classroomID'] = $classroomID;
		$table->bind($data);
		$table->store();
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		if($customerID):
			$load 		= array('customerID' => $customerID, 'parameter' => 'emailSender');
			$table->load($load);
			$emailSender = $table->wert;
			$load 		= array('customerID' => $customerID, 'parameter' => 'emailSenderName');
			$table->load($load);
			$emailSenderName = $table->wert;
			$load 		= array('customerID' => $customerID, 'parameter' => 'systemLink');
			$table->load($load);
			$systemLink = $table->wert;
		else:
			$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
			$table->load($load);
			$emailSender = $table->wert;
			$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
			$table->load($load);
			$emailSenderName = $table->wert;
			$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
			$table->load($load);
			$systemLink = $table->wert;
		endif;
		
		$mailer = JFactory::getMailer();
		//$sender = array('training@ce.de','ceLearning');
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		$mailer->addRecipient($recipient);
		//$mailer->addRecipient('ts@torstenscheel.de');
		$template = str_replace('{first_name}',$first_name, $template->text);
		$template = str_replace('{last_name}',$last_name, $template);
		$template = str_replace('{email}',$recipient->email, $template);
		$template = str_replace('{classroomname}',$classroom->title, $template);
		$template = str_replace('{aktivierungslink}', '<a style="color: #ff3600;" href="'.$systemLink.'/vertify?SMA='.base64_encode($studentU->tblUserID).'">hier</a>', $template);
		$body 	= $template;
		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    $return = 'Error sending email: ';
		} else {
		    $return = 'Mail an Teilnehmer: '.$first_name.' '.$last_name.' erfolgreich versendet.';
		}
		return $return;
	}
	function invitationToAll() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$students 	= $db->loadObjectList();
		$return 	= '';
		if($students):
			$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
			foreach($students as $student):
				$set = $this->invitation($classroomID, $student);
				$return .= $set.'<br/>';
			endforeach;
			$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
			$return .= '</div>'; 
		endif; 
		echo $return;
		exit();
	}
	function invitationToOne() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$studentID 		= $input->get('studentID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('a.userID').' = '.$db->quote($studentID));
		$db->setQuery($query);
		$student 	= $db->loadObject();
		$return 	= '';
		$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
		$set = $this->invitation($classroomID, $student);
		$return .= $set.'<br/>';
		$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
		$return .= '</div>'; 
		echo $return;
		exit();
	}
	function invitation($classroomID, $student) {
		$user = JFactory::getUser();
        $groups = JAccess::getGroupsByUser($student->userID);
        // Students
        if(in_array(11,$groups)) {
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Student','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        // Customer
        if(in_array(12,$groups)) {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 		= array('userID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        // Trainer
        if(in_array(10,$groups)) {
           	JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Trainer','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        if(!$recipient): 
			//Load the User
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$userU 	= JTable::getInstance('User','JclassroomTable',array());
			$load 	= array('id' => $student->userID);
			$userU->load($load);
			$salutation 	= '';
			$first_name 	= '';
			$last_name 		= $userU->name;
			$recipient 		= $userU->email;
		endif;
		//Load the classroom
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$classroom 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$classroom->load($classroomID);
		$invitationTemplate = $classroom->email_template;
		$customerID 	= $classroom->customerID;
		//Load the trainer
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainer 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$trainer->load($classroom->main_trainer);
		//Load the template
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.text,a.subject'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.id').' = '.$db->quote($invitationTemplate));
		$db->setQuery($query);
		$template 	= $db->loadObject();
		$subject 	= $template->subject;
		//Load the day
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('Min(a.title) as first_day,MAX(a.title) as last_day'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroom->id));
		$db->setQuery($query);
		$day 	= $db->loadObject();
		//Load the days
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroom->id));
		$db->setQuery($query);
		$days 	= $db->loadObjectList();
		if($days):
			foreach($days as $dayS1):
				$all_days .= date('d.m.Y', strtotime($dayS1->title)).'<br/>';
			endforeach;
		endif;
		//Write to Emails
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Email','JclassroomTable',array());
		$data 		= array();
		$data['typ'] = 1;
		$data['send'] 	= date('Y-m-d H:i:s');
		$data['send_by'] 	= $user->id;
		$data['send_to'] 	= $student->userID;
		$data['classroomID'] = $classroomID;
		$table->bind($data);
		$table->store();
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		$mailer->addRecipient($recipient);
		//$mailer->addRecipient('ts@torstenscheel.de');
		$template = str_replace('{first_name}',$first_name, $template->text);
		$template = str_replace('{last_name}',$last_name, $template);
		$template = str_replace('{email}',$student->email, $template);
		$template = str_replace('{classroomname}',$classroom->title, $template);
		$template = str_replace('{maintrainer}',$trainer->name, $template);
		$template = str_replace('{first_day}',date('d.m.Y', strtotime($day->first_day)), $template);
		$template = str_replace('{last_day}',date('d.m.Y', strtotime($day->last_day)), $template);
		$template = str_replace('{all_days}', $all_days, $template);
		$body 	= $template;
		$subject = str_replace('{classroomname}',$classroom->title, $subject);
		$subject = str_replace('{first_day}',date('d.m.Y', strtotime($day->first_day)), $subject);
		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    return 'Error sending email: ';
		} else {
		    return 'Mail wurde erfolgreich an '.$first_name.' '.$last_name.' versendet';
		}
	}
	function rememberToAll() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$students 	= $db->loadObjectList();
		$return 	= '';
		if($students):
			$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
			foreach($students as $student):
				$set = $this->remember($classroomID, $student);
				$return .= $set.'<br/>';
			endforeach;
			$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
			$return .= '</div>'; 
		endif; 
		echo $return;
		exit();
	}
	function rememberToOne() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$studentID 		= $input->get('studentID', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('a.userID').' = '.$db->quote($studentID));
		$db->setQuery($query);
		$student 	= $db->loadObject();
		$return 	= '';
		$return .= '<div id="message" class="alert alert-success mt-2" role="alert" style="font-size: 14px;">'; 
		$set = $this->remember($classroomID, $student);
		$return .= $set.'<br/>';
		$return .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: absolute;top: 5px;right: 10px;">
    		<span aria-hidden="true">&times;</span>
  			</button>';
		$return .= '</div>'; 
		echo $return;
		exit();
	}
	function remember($classroomID, $student) {
		$user = JFactory::getUser();
        $groups = JAccess::getGroupsByUser($student->userID);
        // Students
        if(in_array(11,$groups)) {
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Student','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        // Reseller
        if(in_array(12,$groups)) {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 		= array('userID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        // Trainer
        if(in_array(10,$groups)) {
           	JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Trainer','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->userID);
			$ST->load($load);
			$salutation = $ST->salutation;
			$first_name = $ST->first_name;
			$last_name 	= $ST->last_name;
			$recipient 	= $ST->email;
        }
        if(!$recipient): 
			//Load the User
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$userU 	= JTable::getInstance('User','JclassroomTable',array());
			$load 	= array('id' => $student->userID);
			$userU->load($load);
			$salutation 	= '';
			$first_name 	= '';
			$last_name 		= $userU->name;
			$recipient 		= $userU->email;
		endif;
		//Load the classroom
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$classroom 	= JTable::getInstance('Classroom','JclassroomTable',array());
		$classroom->load($classroomID);
		$invitationTemplate = $classroom->remember_template;
		$customerID 		= $classroom->customerID;
		//Load the trainer
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$trainer 	= JTable::getInstance('Trainer','JclassroomTable',array());
		$load 		= array('tblUserID' => $classroom->main_trainer);
		$check 		= $trainer->load($load);
		if($check):
			$maintrainer = $trainer->first_name.' '.$trainer->last_name;
		else:
			// Try to load a customeradmin as trainer
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$ST 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 		= array('userID' => $classroom->main_trainer);
			$check2 	= $ST->load($load);
			if($check2):
				$maintrainer 	= $ST->first_name.' '.$ST->last_name;
			endif; 	
		endif;
		//Load the template
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.text,a.subject'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.id').' = '.$db->quote($invitationTemplate));
		$db->setQuery($query);
		$template 	= $db->loadObject();
		$subject 	= $template->subject;
		//Load the day
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('Min(a.title) as first_day,MAX(a.title) as last_day'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroom->id));
		$db->setQuery($query);
		$day 	= $db->loadObject();
		//Load the days
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroom->id));
		$db->setQuery($query);
		$days 	= $db->loadObjectList();
		if($days):
			foreach($days as $dayS1):
				$all_days .= date('d.m.Y', strtotime($dayS1->title)).'<br/>';
			endforeach;
		endif;
		//Write to Emails
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Email','JclassroomTable',array());
		$data 		= array();
		$data['typ'] = 3;
		$data['send'] 	= date('Y-m-d H:i:s');
		$data['send_by'] 	= $user->id;
		$data['send_to'] 	= $student->userID;
		$data['classroomID'] = $classroomID;
		$table->bind($data);
		$table->store();
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSender');
		$table->load($load);
		$emailSender = $table->wert;
		$load 		= array('customerID' => $customerID, 'parameter' => 'emailSenderName');
		$table->load($load);
		$emailSenderName = $table->wert;
		$mailer = JFactory::getMailer();
		$sender = array($emailSender,$emailSenderName);
		$mailer->setSender($sender);
		$mailer->addRecipient($recipient);
		//$mailer->addRecipient('ts@torstenscheel.de');
		$template = str_replace('{first_name}',$first_name, $template->text);
		$template = str_replace('{last_name}',$last_name, $template);
		$template = str_replace('{email}',$student->email, $template);
		$template = str_replace('{classroomname}',$classroom->title, $template);
		$template = str_replace('{maintrainer}',$maintrainer, $template);
		$template = str_replace('{first_day}',date('d.m.Y', strtotime($day->first_day)), $template);
		$template = str_replace('{last_day}',date('d.m.Y', strtotime($day->last_day)), $template);
		$template = str_replace('{all_days}', $all_days, $template);
		$body 	= $template;
		$subject = str_replace('{classroomname}',$classroom->title, $subject);
		$subject = str_replace('{first_day}',date('d.m.Y', strtotime($day->first_day)), $subject);
		$mailer->setSubject($subject);
		$mailer->isHtml(true);
		$mailer->setBody($body);
		$send = $mailer->Send();
		if ( $send !== true ) {
		    return 'Error sending email: ';
		} else {
		    return 'Mail wurde erfolgreich an '.$first_name.' '.$last_name.' versendet';
		}
	}
	function stageFile() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('File','JclassroomTable',array());
		$table->load($id);
		$table->published = 1;
		$table->store();
		echo 'Datei wurde freigegeben.';
		exit();
	}
	function unstageFile() {
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('File','JclassroomTable',array());
		$table->load($id);
		$table->published = 0;
		$table->store();
		echo 'Datei wurde versteckt.';
		exit();
	}
	public function newStudent() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		//First check if student-email exisit
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('User','JclassroomTable',array());
		$load = array('email' => $input->get('studentEmail',0,'STR'));
		$check = $table->load($load);
		/*if($check):
			echo 'Es gibt bereits einen Teilnehmer mit dieser E-Mail-Adresse.';
			exit();
		endif;*/
		$id 		= $input->get('classroomID', 0, 'INT');
		$studentSalutation   = $input->get('studentSalutation',0,'STR');
		$studentFirstname    = $input->get('studentFirstname',0,'STR');
		$studentLastname     = $input->get('studentLastname',0,'STR');
		$studentCompany      = $input->get('studentCompany',0,'STR');
		$studentAdress       = $input->get('studentAdress',0,'STR');
		$studentPostcode     = $input->get('studentPostcode',0,'STR');
		$studentCity         = $input->get('studentCity',0,'STR');
		$studentPhone        = $input->get('studentPhone',0,'STR');
		$studentEmail        = $input->get('studentEmail',0,'STR');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Student','JclassroomTable',array());
		$data 		= array();
		$data['customerID'] = $session->get('customerID');
		$data['companyID'] 	= $input->get('studentCompanyID',0,'INT');
		$data['salutation'] = $studentSalutation;
		$data['first_name'] = $studentFirstname;
		$data['last_name'] 	= $studentLastname;
		$data['company'] 	= $studentCompany;
		$data['adress'] 	= $studentAdress;
		$data['postcode'] 	= $studentPostcode;
		$data['city'] 		= $studentCity;
		$data['phone'] 		= $studentPhone;
		$data['username'] 	= $studentEmail;
		$data['password'] 	= '';
		$data['email'] 		= $studentEmail;
		$data['created'] 	= strval($datum);
		$data['created_by'] = $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$userID = $table->tblUserID;
		$userID = $this->writeClassroom2User($data, $table->id, $password);
		$return 	= array(
			'userID' 	=> $userID,
			'badge' 	=> 'badge-success',
			'usergroup' => 'Teilnehmer',
			'userstate' => '<i class="fa fa-ban text-danger"></i>',
			'created' 	=> date('d.m.Y H:i')
		);
		//$password = JUserHelper::hashPassword('#mytraining1602');
		
		$this->writeClassroom2Student($id, $table->id, $userID);
		echo json_encode($return);
		exit();
	}
	public function chooseStudent() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
        $groups 	= JAccess::getGroupsByUser($user->id);
        // Students
        if(in_array(11,$groups)) {
        	$group 	= 'student';
        }
        // Customer
        if(in_array(12,$groups)) {
            $group 	= 'customer';
        }
        // Trainer
        if(in_array(10,$groups)) {
            $group 	= 'trainer';
        }
        // Trainer
        if(in_array(8,$groups)) {
            $group 	= 'superuser';
        }
		$input 		= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID', 0, 'INT');
		$students 		= $input->get('students', '', 'ARRAY');
		$return = array();
		if($students):
			foreach($students as $student):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$st 		= JTable::getInstance('Student','JclassroomTable',array());
				$load 		= array('tblUserID' => $student);
				$checkST 	= $st->load($load);
				if($checkST):
					$studentID 		= $st->id;
					$studentName 	= $st->first_name.' '.$st->last_name;
					$email 			= $st->email;
					$studentType	= 'Teilnehmer';
				endif;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tr 		= JTable::getInstance('Trainer','JclassroomTable',array());
				$load 		= array('tblUserID' => $student);
				$checkTR 	= $tr->load($load);
				if($checkTR):
					$studentID 		= $tr->id;
					$studentName 	= $tr->first_name.' '.$tr->last_name;
					$email 			= $tr->email;
					$studentType	= 'Trainer';
				endif;
				if(!$checkST && !$checkTR):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$us 		= JTable::getInstance('User','JclassroomTable',array());
					$load 		= array('id' => $student);
					$checkUS 	= $us->load($load);
					if($checkUS):
						$studentID 		= $us->id;
						$studentName 	= $us->name;
						$email 			= $us->email;
						$studentType	= 'Systemadministrator';
					endif;
				endif;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
				$load = array('classroomID' => $classroomID, 'userID' => $student);
				$check = $table->load($load);
				if(!$check):
					$data 		= array();
					$data['classroomID'] 	= $classroomID;
					//$data['studentID'] 		= $studentID;
					$data['userID'] 		= $student;
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
 					$table->bind($data);
					$table->store();
				else:
					$table->modified 		= date('Y-m-d H:i:s');
					$table->modified_by 	= $user->id;
					$table->store();;
				endif;
				$return[] = array(
					'id' 		=> $studentID,
					'email' 	=> $email,
					'tblUserID' => $student,
					'name' 		=> $studentName,
					'studentType' => $studentType,
					'vertify' 	=> '<i class="fa fa-check text-success"></i>',
					'dateOfVertify' 	=> '',
					'dateOfInvitation' 	=> '',
					'dateOfBooking' 	=> date('d.m.Y H:i')
				);
			endforeach;
		endif;
		echo json_encode($return);
		exit();
	}
	public function loadFileM() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$files  	= $input->files->get('inputMaterial');
		if($files): 
			foreach($files as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					//echo $filenameRaw.'<br/>';
					$filename	= '/images/learningrooms/LR'.$id.'/material/'.str_replace(' ','-',$filenameRaw);
					$savename	= str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
			        JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['unitID'] 		= '';
					$data['type'] 			= 'material';
					$data['folder'] 		= '';
					$data['filename'] 		= $savename;
					$data['path'] 			= $filename;
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$fileID = $table->id;
				endif;
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Die Datei(en) wurden importiert'.'<br/>'.$message, 'Message');
		$this->setRedirect(JURI::Root().'classroom-edit?layout=edit&id='.$id);
	}
	public function loadFileR() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$files  	= $input->files->get('inputReserve');
		echo '<pre>';
		print_r($files);
		if($files): 
			foreach($files as $file):
				if($file['name']):
					echo $file['name'];
					$filenameRaw 	= JFile::makeSafe($file['name']);
					echo $filenameRaw.'<br/>';
					$filename	= '/images/learningrooms/LR'.$id.'/reserve/'.str_replace(' ','-',$filenameRaw);
					$savename	= str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
			        JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['unitID'] 		= $unitID;
					$data['type'] 			= 'reserve';
					$data['folder'] 		= '';
					$data['filename'] 		= $savename;
					$data['path'] 			= $filename;
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$fileID = $table->id;
				endif;
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Die Datei(en) wurden importiert'.'<br/>'.$message, 'Message');
		$this->setRedirect(JURI::Root().'classroom-edit?layout=edit&id='.$id);
	}
	public function loadFile() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$unitID 	= $input->get('unitID','','STR');
		$file  		= $input->files->get('uploadUnit');
		echo '<pre>';
		print_r($input->files->get('uploadUnit'));
		print_r($file);
		if($file): 
			//foreach($files as $file):
				if($file['name']):
					echo $file->name;
					$filenameRaw 	= JFile::makeSafe($file['name']);
					//echo $filenameRaw.'<br/>';
					$filename	= '/images/learningrooms/LR'.$id.'/units/'.str_replace(' ','-',$filenameRaw);
					$savename	= str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
			        JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['unitID'] 		= $unitID;
					$data['type'] 			= 'unit';
					$data['folder'] 		= '';
					$data['filename'] 		= $savename;
					$data['path'] 			= $filename;
					$data['created'] 		= strval($datum);
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$fileID = $table->id;
				endif;
			//endforeach;
		endif;
        echo $fileID.'__'.$filename;
        exit();
		//JFactory::getApplication()->enqueueMessage('Die Datei(en) wurden importiert'.'<br/>'.$message, 'Message');
		//$this->setRedirect(JURI::Root().'classroom-edit?layout=edit&id='.$id);
	}
	public function loadImageFile() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$unitID 	= $input->get('unitID','','STR');
		$file  		= $input->files->get('uploadImageUnit');
		if($file): 
			//foreach($files as $file):
				if($file['name']):
					$filenameRaw 	= JFile::makeSafe($file['name']);
					//echo $filenameRaw.'<br/>';
					$filename	= '/images/learningrooms/LR'.$id.'/units/'.str_replace(' ','-',$filenameRaw);
					$savename	= str_replace(' ','-',$filenameRaw);
					$dest		= JPATH_SITE.$filename;
			        JFile::upload($file['tmp_name'], $dest);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('File','JclassroomTable',array());
					$data = array();
					$data['classroomID'] 	= $id;
					$data['unitID'] 		= $unitID;
					$data['type'] 			= 'imageUnit';
					$data['folder'] 		= '';
					$data['filename'] 		= $savename;
					$data['path'] 			= $filename;
					$data['created'] 		= date('Y-m-d H:i:s');
					$data['created_by'] 	= $user->id;
					$data['published'] 		= 1;
					$table->bind($data);
					$table->store();
					$fileID = $table->id;
				endif;
			//endforeach;
		endif;
        echo $fileID.'__'.$filename;
        exit();
		//JFactory::getApplication()->enqueueMessage('Die Datei(en) wurden importiert'.'<br/>'.$message, 'Message');
		//$this->setRedirect(JURI::Root().'classroom-edit?layout=edit&id='.$id);
	}
	public function deleteFile() {
		$input 		= JFactory::getApplication()->input;
		$fileID 	= $input->get('fileID','','INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('File','JclassroomTable',array());
		$table->load($fileID);
		$pathname = $table->path;
		$filename = $table->filename;
		$table->delete($fileID);
		unlink(JPATH_SITE.'/'.$pathname);
		echo str_replace('.', '', $filename);
		exit();
	}
	public function deleteFileFD() {
		$input 		= JFactory::getApplication()->input;
		$path 		= $input->get('path','','STR');
		/*JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('File','JclassroomTable',array());
		$table->load($fileID);
		$filename = $table->path;
		$table->delete($fileID);*/
		unlink($path);
		echo 'OK';
		exit();
	}
	public function loadCSV() {
		$session 	= JFactory::getSession();
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('classroomID', 0, 'INT');
		$file  		= $input->files->get('uploadCSV');
		echo '<pre>';
		$fileName   = $file['name'];
		$src        = $file['tmp_name'];
        $dest       = JPATH_BASE.'/images/jclassroom/import/'.$fileName;
        JFile::upload($src, $dest);
        if(($handle = fopen($dest, "r")) !== FALSE):
			$i = 1;
			while(($line = fgetcsv($handle, 0, ';', "\n")) !== false) {
				foreach($line as $key => $value) {
					echo $value.'<br/>';
                    if ($i === 1) {
                        $keys[$key] = $value;
                    } else {
                        $out[$i][$key] = $value;
                    }
                }
                $i++;
			}
		endif;
		if($out):
			foreach($out as $item):
				$return[] = array(
					'customer_number' 	=> $item[0], 
					'salutation' 	=> $item[1],
					'first_name' 	=> $item[2], 
					'last_name' 	=> $item[3],
					'company' 		=> $item[4], 
					'adress' 		=> $item[5], 
					'city' 			=> $item[7], 
					'phone' 		=> $item[8], 
					'mobile' 		=> $item[9],
					'email' 		=> $item[10]
				);
			endforeach;
		endif;
		echo '<pre>';
		print_r($return);
		if($return):
			$message = '';
			foreach($return as $item):
				$check = $this->checkPeople($item['email']);
				if($check == false && $item['email'] != '') {
					// Check if email exist
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 		= JTable::getInstance('User','JclassroomTable',array());
					$load = array('email' => $item['email']);
					$check = $table->load($load);
					if(!$check):
						echo $this->convertForExcel($item['last_name']);
						//Load the classroom
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$cl 		= JTable::getInstance('Classroom','JclassroomTable',array());
						$cl->load($id);
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$table 		= JTable::getInstance('Student','JclassroomTable',array());
						$data 		= array();
						$data['customerID'] = $session->get('customerID');
						$data['companyID'] 	= $cl->companyID;
						$data['customer_number'] = $item['customer_number'];
						$data['salutation'] = $item['salutation'];
						$data['first_name'] = $item['first_name'];
						$data['last_name'] 	= $item['last_name'];
						$data['company'] 	= $item['company'];
						$data['adress'] 	= $item['adress'];
						$data['postcode'] 	= $item['postcode'];
						$data['city'] 		= $item['city'];
						$data['phone'] 		= $item['phone'];
						$data['mobile'] 	= $item['mobile'];
						$data['email'] 		= $item['email'];
						$data['created'] 	= strval($datum);
						$data['created_by'] = $user->id;
						$data['published'] 	= 1;
						$table->bind($data);
						$table->store();
						$userID = $this->writeClassroom2User($data, $table->id,'');
						$this->writeClassroom2Student($id, $table->id, $userID);
					else:
						$back[] = 'Der Teilnehmer '.$item['first_name'].' '.$item['last_name'].' ist bereits registriert.';
					endif;
				} else {
					if($item['email'] != ''):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$table 	= JTable::getInstance('Student','JclassroomTable',array());
						$load 	= array('email' => $item['email']);
						$table->load($load);
						$table->adress = $item['adress'];
						$table->store();
						$studentID  = $table->id;
						$userID 	= $table->tblUserID;
						$this->writeClassroom2Student($id, $table->id, $userID);
					endif;
				}
			endforeach;
		endif;
		JFactory::getApplication()->enqueueMessage('Die Teilnehmer wurden importiert'.'<br/>'.$message, 'Message');
		$this->setRedirect(JURI::Root().'classroom-edit?layout=edit&id='.$id);
	}
	public function saveCSV() {
		$input 	= JFactory::getApplication()->input;
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		$query 		= $db->getQuery(true);
		$query->select(array('a.*,b.*,c,*'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_students', 'b') . ' ON (' . $db->quoteName('a.userID') . ' = ' . $db->quoteName('b.tblUserID') . ')');
       	$query->join('LEFT', $db->quoteName('#__jclassroom_customer', 'c') . ' ON (' . $db->quoteName('a.customerID') . ' = ' . $db->quoteName('c.id') . ')');
        $query->where('a.classroomID = ' . $db->quote($input->get('lr',0,'INT')));
		$query->order('a.id asc');
		$db->setQuery((string)$query);
		$result = $db->loadObjectList();
		$pfad = JPATH_SITE.'/images/jclassroom/export/students'.$user->id.'.csv';
		$file = fopen($pfad,'w');
		$header = array(
			'nr' 			=> 'Nr.',
			'customer_number' => 'Kunden-Nr.',
			'salutation' 	=> 'Anrede',
			'first_name' 	=> 'Vorname',
			'last_name' 	=> 'Nachname',
			'company'		=> 'Firma',
			'adress' 		=> 'Adresse',
			'postcode' 		=> 'PLZ',
			'city' 			=> 'Ort',
			'phone' 		=> 'Telefonnummer',
			'mobile' 		=> 'Mobilnummer',
			'email' 		=> 'E-Mail'
		);
		fputcsv($file, $header,';');
		if($result) {
			$ia = 1;
			foreach($result as $item) {
				$export = array(
					'nr' 				=> $ia,
					'customer_number' 	=> $this->convertForExcel($item->customer_number),
					'salutation' 		=> $this->convertForExcel($item->salutation),
					'first_name' 		=> $this->convertForExcel($item->first_name),
					'last_name' 		=> $this->convertForExcel($item->last_name),
					'company' 			=> $this->convertForExcel($item->company),
					'adress' 			=> $this->convertForExcel($item->adress),
					'postcode' 		=> $this->convertForExcel($item->postcode),
					'city' 			=> $this->convertForExcel($item->city),
					'phone' 			=> $this->convertForExcel($item->phone_number),
					'mobile' 		=> $this->convertForExcel($item->mobile_number),
					'email' 			=> $this->convertForExcel($item->email),
				);
				fputcsv($file, $export,';');
				$ia++;
			}
		}
		fclose($file);
		$app = JFactory::getApplication();
		$app->redirect(JURI::root().'images/jclassroom/export/students'.$user->id.'.csv');
	}
	function writeClassroom2User($studentData, $studentID, $password = false) {
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('User','JclassroomTable',array());
		$load = array('email' => $studentData['email']);
		$check = $table->load($load);
		if(!$check):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('User','JclassroomTable',array());
			$data = array();
			$data['name'] 		= $studentData['first_name'].' '.$studentData['last_name'];
			$data['username'] 	= $studentData['email'];
			$data['email'] 		= $studentData['email'];
			$data['password'] 	= '';
			$data['block'] 		= 0;
			$data['sendEmail'] 	= 0;
			$data['registerDate'] 	= date('Y-m-d H:i:s');
			$data['lastvisitDate'] 	= date('Y-m-d H:i:s');
			$data['activation'] 	= 0;
			$table->bind($data);
			$table->store();
			$tblUserID = $table->id;
			//Tabelle Usergroup-Map
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('Usergroup','JclassroomTable',array());
			$data = array();
			$data['user_id'] 	= $tblUserID;
			$data['group_id'] 	= 11;
			$table->bind($data);
			$table->store();
			// SAVE the user ID
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table = JTable::getInstance('Student','JclassroomTable',array());
			$table->load($studentID);
			$table->tblUserID = $tblUserID;
			$table->store();
		endif;
		return $tblUserID;
	}
	function writeClassroom2Student($classroomID, $studentID, $userID) {
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$load = array('classroomID' => $classroomID, 'studentID' => $studentID);
		$check = $table->load($load);
		if(!$check):
			$data 		= array();
			$data['classroomID'] 	= $classroomID;
			$data['studentID'] 		= $studentID;
			$data['userID'] 		= $userID;
			$data['created'] 		= date('Y-m-d H:i:s');
			$data['created_by'] 	= $user->id;
			$data['published'] 		= 1;
			$table->bind($data);
			$table->store();
		endif;
	}
	function convertForExcel($str) {
		echo 'ORIGINAL: '.$str.'<br/>';
		echo 'RESULT: '.mb_convert_encoding($str, "Windows-1252", mb_detect_encoding($str, "UTF-8, ISO-8859-1, ISO-8859-15",true)).'<br/>';
		return mb_convert_encoding($str, "Windows-1252", mb_detect_encoding($str, "UTF-8, ISO-8859-1, ISO-8859-15",true));
	}
	function checkPeople($email) {
		$return 	= false;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Student','JclassroomTable',array());
		$load 		= array('email' => $email);
		$check 		= $table->load($load);
		if($check):
			$return = true;
		endif;

		return $return;
	}
	function checkEmail() {
		$return = false;
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('User','JclassroomTable',array());
		$load 		= array('email' => $input->get('email' ,'', 'STR'));
		$check 		= $table->load($load);
		if($check):
			$return = true;
		endif;
		echo $return;
		exit();
	}
	function loadTab() {
		$input 		= JFactory::getApplication()->input;
		$tab 		= $input->get('tab','', 'STR');
		switch($tab) {
			case 'basic':
				$qresult = require_once(JPATH_COMPONENT.'/views/classroom/tmpl/basic.php');
				break;
		}
		echo $qresult;
		exit();
	}
	function getClassroomData($classroomID, $dayID) {
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.published = 1');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->cards = $result;
		// Load the modules
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.classroomID = '.$classroomID);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->modules = $result;
		// Load the units
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
		$query->where('a.classroomID = '.$classroomID);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->units = $result;
		// Load the reservefiles
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_files','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.type = "reserve"');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->reserve = $result;
		// Load the regular Files
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_files','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.type = "unit"');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$item->unitFiles = $result;

		return $item;
	}
	function loadTheThemesOfClassroom() {
		$input 	= JFactory::getApplication()->input;
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.id, a.title'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$input->get('classroomID', 0, 'INT'));
		$query->where('a.published = 1');
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$return[] = array('type' => 'optiongroupS', 'title' => $item->title, 'id' => 0);
				$query = $db->getQuery(true);
				$query->select(array('a.id, a.title'));
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
				$query->where('a.classroomID = '.$input->get('classroomID', 0, 'INT'));
				$query->where('a.dayID = '.$item->id);
				$query->where('a.published = 1');
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				foreach($modules as $module):
					$return[] = array('type' => 'option', 'title' => $module->title, 'id' => $module->id);
				endforeach;
				$return[] = array('type' => 'optiongroupE', 'title' => $item->title, 'id' => 0);
			endforeach;
		endif;
		echo json_encode($return);

		exit();
	}
	function exportLearningroom() {
		$input 		= JFactory::getApplication()->input;
		$classroomID= $input->get('classroomID', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$table->load($classroomID);
		$xml['learningroom'] = array(
			'title' => $table->title, 
			'description' => $table->description
		);
		$db = JFactory::getDbo();
		// Load the days
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->where('a.published = 1');
		$query->order('a.day asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
				$query->where('a.dayID = '.$item->id);
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				
				$days[] 	= array('day' => $item->day,'modules' => json_encode($modules));
			endforeach;
		endif;
		$xml['days']	= array($days);
		print_r($xml);
		//file_put_contents(JURI::Root().'images/learningrooms/LR'.$table->id.'.xml',$xml);
		echo $xml;
		exit();
	}
	function libraryLearningroom() {
		$session 	= JFactory::getSession();
		if($session->get('customerID')):
			$customerID = $session->get('customerID');
		else:
			$customerID = 0;
		endif;
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		// Load origin
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		// Create copy
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$lrCopy 	= JTable::getInstance('Classroom_library','JclassroomTable',array());
		$data 		= array();
		$data['customerID'] 	= $customerID;
		$data['companyID'] 		= 0;
		$data['title'] 			= $table->title;
		$data['description'] 	= $table->description;
		$data['main_trainer'] 	= 0;
		$data['co_trainer'] 	= 0;
		$data['fromDate'] 		= '0000-00-00';
		$data['toDate'] 		= '0000-00-00';
		$data['presentation'] 	= $table->presentation;
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$data['published'] 		= 1;
		$lrCopy->bind($data);
		$lrCopy->store();
		$newLearningRoomID = $lrCopy->id;
		// Load the origin days
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$id);
		$query->order('a.day asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				// Create copy
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$dayCopy 	= JTable::getInstance('Classroom_days_library','JclassroomTable',array());
				$data 		= array();
				$data['customerID'] 	= 0;
				$data['classroomID'] 	= $newLearningRoomID;
				$data['title'] 			= $item->title;
				$data['description'] 	= $item->description;
				$data['day'] 			= $item->day;
				$data['dayID'] 			= 0;
				$data['ordering'] 		= $item->ordering;
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $user->id;
				$data['published'] 		= 1;
				$dayCopy->bind($data);
				$dayCopy->store();
				$newDayID = $dayCopy->id;
				// Load origin modules
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
				$query->where('a.classroomID = '.$id);
				$query->where('a.dayID = '.$item->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				if($modules):
					foreach($modules as $module):
						// Create copy
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$moduleCopy 	= JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
						$data 		= array();
						$data['classroomID'] 	= $newLearningRoomID;
						$data['dayID'] 			= $newDayID;
						$data['title'] 			= $module->title;
						$data['description'] 	= $module->description;
						$data['ordering'] 		= $module->ordering;
						$data['created'] 		= date('Y-m-d H:i:s');
						$data['created_by'] 	= $user->id;
						$data['published'] 		= 1;
						$moduleCopy->bind($data);
						$moduleCopy->store();
						$newModuleID = $moduleCopy->id;
						// Load origin units
						$query = $db->getQuery(true);
						$query->select(array('a.*'));
				        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
						$query->where('a.moduleID = '.$item->id);
						$query->order('a.ordering asc');
						$db->setQuery($query);
						$units = $db->loadObjectList();
						if($units):
							foreach($units as $unit):
								// Create copy
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$unitCopy 	= JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
								$data 		= array();
								$data['title'] 			= $unit->title;
								$data['description'] 	= $unit->description;
								$data['moduleID'] 	= $newModuleID;
								$data['unitType'] 	= $unit->unitType;
								$data['ordering'] 	= $unit->ordering;
								$data['duration'] 	= $unit->duration;
								$data['content'] 	= $unit->content;
								$data['link'] 		= $unit->link;
								$data['quizzID'] 	= $unit->quizzID;
								$data['created'] 		= date('Y-m-d H:i:s');
								$data['created_by'] 	= $user->id;
								$data['published'] 		= 1;
								$unitCopy->bind($data);
								$unitCopy->store();
							endforeach;
						endif;
					endforeach;
				endif;
			endforeach;
		endif;

		// Load the units
        /*$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.classroomID = '.$id);
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$positionsU = array();
				$positionsF = array();
				// Load the units
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				$query->where('a.moduleID = '.$item->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$units = $db->loadObjectList();
				if($units):
					foreach($units as $unit):
						$positionsU[] 	= array(
							'id' 			=> $unit->id,
							'customerID' 	=> $unit->customerID,
							'classroomID' 	=> $unit->classroomID,
							'dayID' 		=> $unit->dayID,
							'moduleID' 		=> $unit->moduleID,
							'unitID' 		=> $unit->unitID,
							'unitType' 		=> $unit->unitType,
							'ordering' 		=> $unit->ordering,
							'duration' 		=> $unit->duration,
							'title' 		=> $unit->title,
							'content' 		=> $unit->content,
							'link' 			=> $unit->link,
							'quizzID' 		=> $unit->quizzID
						);
					endforeach;
				endif;
				// Load the files
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_files','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$positionsF[] 	= array(
							'type' 			=> $file->type,
							'classroomID' 	=> $file->classroomID,
							'unitID' 		=> $file->unitID,
							'studentID' 	=> $file->studentID,
							'folder' 		=> $file->folder,
							'filename' 		=> $file->filename,
							'path' 			=> $file->path
						);
					endforeach;
				endif;
				$modules[] 	= array(
					'id' 		=> $item->id,
					'customerID' 	=> $item->customerID,
					'classroomID' 	=> $item->classroomID,
					'dayID' 		=> $item->dayID,
					'title' 		=> $item->title,
					'description' 	=> $item->description,
					'ordering' 		=> $item->ordering,
					'units' 		=> $positionsU,
					'files' 		=> $positionsF
				);
				
			endforeach;
		endif;
		$xml['modules']	= $modules;*/

		echo 'OK';
		exit();
	}
	function addTimeblock() {
		$session 	= JFactory::getSession();
		$input 	= JFactory::getApplication()->input;
		//Get the last ordering of timeblock
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.ordering'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
		$query->where('a.classroomID = '.$input->get('classroomID',0,'INT'));
		$query->order('a.ordering DESC');
		$query->setLimit(1);
		$db->setQuery($query);
		$lastOrdering = $db->loadResult();
		//Save the new timeblock
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks','JclassroomTable',array());
		$data 		= array();
		$data['customerID'] 	= $session->get('customerID');
		$data['classroomID'] 	= $input->get('classroomID',0,'INT');
		$data['title'] 		=	date('Y-m-d');
		$data['ordering'] 	= $lastOrdering + 1;
		$data['background_color'] 	= '#eee';
		$timeblocks->bind($data);
		$timeblocks->store();
		$id = $timeblocks->id;
		echo $id;
		exit();
	}
	function addThemeToTimeblock() {
		$input 			= JFactory::getApplication()->input;
		$timeblockID 	= $input->get('droppedTo','','STR');
		$themeID 	 	= $input->get('droppedID','','STR');
		//Get the last ordering of timeblock
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.ordering'));
        $query->from($db->quoteName('#__jclassroom_timeblocks_themes','a'));
		$query->where('a.classroomID = '.$input->get('classroomID',0,'INT'));
		$query->order('a.ordering DESC');
		$query->setLimit(1);
		$db->setQuery($query);
		$lastOrdering = $db->loadResult();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks_themes','JclassroomTable',array());
		$data 		= array();
		$data['classroomID'] 	= $input->get('classroomID',0,'INT');
		$data['timeblockID'] 	= substr($timeblockID,9);
		$data['themeID'] 		= substr($themeID,5);
		$data['ordering'] 	= $lastOrdering + 1;
		$timeblocks->bind($data);
		$timeblocks->store();
		$id = $timeblocks->id;
		echo $id;
		exit();
	}
	function deleteThemeFromTimeblock() {
		$input 			= JFactory::getApplication()->input;
		$timeblockID 	= $input->get('droppedTo','','STR');
		$themeID 	 	= $input->get('droppedID','','STR');
		//Get the last ordering of timeblock
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_timeblocks_themes','a'));
		$query->where('a.classroomID = '.$input->get('classroomID',0,'INT'));
		//$query->where('a.timeblockID = '.substr($timeblockID,9));
		$query->where('a.themeID = '.substr($themeID,5));
		$db->setQuery($query);
		$deleteID = $db->loadResult();
		echo $deleteID;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks_themes','JclassroomTable',array());
		$timeblocks->delete($deleteID);
		echo 'OK';
		exit();
	}
	function deleteTimeblock() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks','JclassroomTable',array());
		$timeblocks->delete($input->get('timeblockID',0,'INT'));
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id'));
        $query->from($db->quoteName('#__jclassroom_timeblocks_themes','a'));
		$query->where('a.timeblockID = '.$input->get('timeblockID',0,'INT'));
		$db->setQuery($query);
		$toDeletes = $db->loadObjectList();
		if($toDeletes):
			foreach($toDeletes as $item):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$timeblocks 	= JTable::getInstance('Classroom_timeblocks_themes','JclassroomTable',array());
				$timeblocks->delete($item->id);
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function saveEditTimeblock() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$timeblocks 	= JTable::getInstance('Classroom_timeblocks','JclassroomTable',array());
		$timeblocks->load($input->get('timeblockID', 0, 'INT'));
		$timeblocks->title = date('Y-m-d', strtotime($input->get('dayDate', '', 'STR')));
		$timeblocks->background_color = $input->get('color', '', 'STR');
		$timeblocks->store();
		echo 'OK';
		exit();
	}
	function reorderRoom() {
		$input 			= JFactory::getApplication()->input;
		$classroomID 	= $input->get('classroomID',0,'INT');
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id,a.title'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$classroomID);
		$query->order('a.ordering ASC');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		echo '<pre>';
		if($days):
			$i = 1;
			foreach($days as $day):
				echo 'DAY: '.$day->title.'<br/>';
				$query = $db->getQuery(true);
				$query->select(array('a.id,a.title'));
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
				$query->where('a.classroomID = '.$classroomID);
				$query->where('a.dayID = '.$day->id);
				$query->order('a.ordering ASC');
				$db->setQuery($query);
				$modules = $db->loadObjectList();
				if($modules):
					foreach($modules as $module):
						echo 'MODULE: '.$module->title.'<br/>';
						$query = $db->getQuery(true);
						$query->select(array('a.id,a.title'));
				        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
						$query->where('a.classroomID = '.$classroomID);
						$query->where('a.moduleID = '.$module->id);
						$query->order('a.ordering ASC');
						$db->setQuery($query);
						$units = $db->loadObjectList();
						if($units):
							
							foreach($units as $unit):
								echo 'Unit: '.$unit->title.'<br/>';
								JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
								$table 		= JTable::getInstance('Classroom_days_units','JclassroomTable',array());
								$table->load($unit->id);
								$table->ordering 	= $i;
								$table->store();
								$i++;
							endforeach;
						endif;
					endforeach;
				endif;
			endforeach;
		endif;
		echo 'OK';
		exit();
	}
	function saveComment() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_student','JclassroomTable',array());
		$load 		= array('classroomID' => $input->get('classroomID',0,'INT'), 'userID' => $input->get('id',0,'INT'));
		$table->load($load);
		$table->comment = $input->get('comment', '', 'STR');
		$table->store();
		echo 'OK';
		exit();
	}
	function saveDayTitle() {
		$input 	= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->load($input->get('id',0,'INT'));
		$table->day = $input->get('date', '', 'STR');
		$table->title = $input->get('title', '', 'STR');
		$table->store();
		echo 'OK';
		exit();
	}
	function exportTemplate() {
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom','JclassroomTable',array());
		$table->load($id);
		$xml['classroom'] = array(
			'id' 			=> $table->id,
			'customerID'	=> $table->customerID,
			'companyID'		=> $table->companyID,
			'title' 		=> $table->title, 
			'description' 	=> $table->description,
			'main_trainer' 	=> $table->main_trainer,
			'co_trainer' 	=> $table->co_trainer,
			'showTo' 		=> $table->showTo,
			'showToUser' 	=> $table->showToUser,
			'email_template' 		=> $table->email_template,
			'verification_template' => $table->verification_template,
			'remember_template' 	=> $table->remember_template,
			'invitations'  	=> $table->invitations,
			'countRememberMails'  	=> $table->countRememberMails,
			'offsetRememberMails'  	=> $table->offsetRememberMails,
			'invitationsBy'  	=> $table->invitationsBy,
			'certificate'  		=> $table->certificate,
			'student_feedback'  	=> $table->student_feedback,
			'student_feedback_anonym'  	=> $table->student_feedback_anonym,
			'trainer_feedback'  	=> $table->trainer_feedback

		);
		$db = JFactory::getDbo();
		// Load the groups
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$id);
		$query->order('a.day asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$days[] 	= array(
					'id' 		=> $item->id,
					'classroomID' 	=> $item->classroomID,
					'day' 			=> $item->day,
					'title' 		=> $item->title,
					'dayID' 		=> $item->dayID
				);
			endforeach;
		endif;
		$xml['days']	= $days;
		// Load the units
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
		$query->where('a.classroomID = '.$id);
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result):
			foreach($result as $item):
				$positionsU = array();
				$positionsF = array();
				// Load the units
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				$query->where('a.moduleID = '.$item->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$units = $db->loadObjectList();
				if($units):
					foreach($units as $unit):
						$positionsU[] 	= array(
							'id' 			=> $unit->id,
							'customerID' 	=> $unit->customerID,
							'classroomID' 	=> $unit->classroomID,
							'dayID' 		=> $unit->dayID,
							'moduleID' 		=> $unit->moduleID,
							'unitID' 		=> $unit->unitID,
							'unitType' 		=> $unit->unitType,
							'ordering' 		=> $unit->ordering,
							'duration' 		=> $unit->duration,
							'title' 		=> $unit->title,
							'content' 		=> $unit->content,
							'link' 			=> $unit->link,
							'quizzID' 		=> $unit->quizzID
						);
					endforeach;
				endif;
				// Load the files
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_files','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$positionsF[] 	= array(
							'type' 			=> $file->type,
							'classroomID' 	=> $file->classroomID,
							'unitID' 		=> $file->unitID,
							'studentID' 	=> $file->studentID,
							'folder' 		=> $file->folder,
							'filename' 		=> $file->filename,
							'path' 			=> $file->path
						);
					endforeach;
				endif;
				$modules[] 	= array(
					'id' 		=> $item->id,
					'customerID' 	=> $item->customerID,
					'classroomID' 	=> $item->classroomID,
					'dayID' 		=> $item->dayID,
					'title' 		=> $item->title,
					'description' 	=> $item->description,
					'ordering' 		=> $item->ordering,
					'units' 		=> $positionsU,
					'files' 		=> $positionsF
				);
				
			endforeach;
		endif;
		$xml['modules']	= $modules;
		$save = json_encode($xml);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Classroom_library','JclassroomTable',array());
		$data 		= array();
		$data['structure'] 		= $save;
		$data['companyID'] 		= 0;
		$data['title'] 			= $xml['classroom']['title'];
		$data['description'] 	= $xml['classroom']['description'];
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$data['published'] 		= 1;
		$table->bind($data);
		$table->store();
		$newLearningRoomID = $table->id;
		/*$rand = rand(100,10000);
		$pathSave = JPATH_SITE."/images/jclassroom/temp/xml_".$rand.".json";
		$downloadPath = JURI::Root()."images/jclassroom/temp/xml_".$rand.".json";
		file_put_contents($pathSave,$save);
		echo $downloadPath;*/
		echo $newLearningRoomID;
		exit();
	}
	public function printStudentlist() {
      $input = JFactory::getApplication()->input;
      $id = $input->get('id','','INT');
      require_once JPATH_SITE. '/components/com_jclassroom/library/studentslist.php';
      $printAudit = new printList();
      $printAudit->studentlist($id);
   }
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'classroom';

	protected $view_list = 'classrooms';

}
?>