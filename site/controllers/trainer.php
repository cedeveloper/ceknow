<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Kunde item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerTrainer extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = 'manager';
				$retour = JURI::Root().'manager-administrator/trainer';
				break;
			case 'customer':
				$retour = JURI::Root().'/manager-customer/trainer-customer';
				break;
			case 'trainer':
				$retour = JURI::Root().'/manager-trainer/trainer';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = 'manager';
				$retour = JURI::Root().'trainer-bearbeiten?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'trainer-bearbeiten?layout=edit&id='.$id;
				break;
			case 'trainer':
				$retour = 'auditum/manager-reseller';
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$session 	= JFactory::getSession();
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$customerID = $session->get('customerID');
		if($formData->getInt('customerID')):
			$customerID = $formData->getInt('customerID');
		endif;
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		$password 	= JUserHelper::hashPassword($formData->getStr('password'));
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/trainers/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/trainers/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Trainer','JclassroomTable',array());
		$data = array();
		$data['customerID'] = $customerID;
		$data['salutation'] = $formData->getStr('salutation');
		$data['first_name'] = $formData->getStr('first_name');
		$data['last_name'] 	= $formData->getStr('last_name');
		$data['company'] 	= $formData->getStr('company');
		$data['adress'] 	= $formData->getStr('adress');
		$data['postcode'] 	= $formData->getStr('postcode');
		$data['city'] 		= $formData->getStr('city');
		$data['phone'] 		= $formData->getStr('phone');
		$data['email'] 		= $formData->getStr('email');
		$data['web'] 		= $formData->getStr('web');
		$data['logo'] 		= $savename;
		$data['asp'] 		= $formData->getStr('asp');
		$data['asp_phone']	= $formData->getStr('asp_phone');
		$data['asp_email'] 	= $formData->getStr('asp_email');
		$data['username'] 	= $formData->getStr('username');
		$data['password'] 	= $formData->getStr('password');
		$data['description']= $formData->getRaw('description');
		$data['skills'] 	= $formData->getRaw('skills');
		$data['published'] 	= $formData->getInt('published');
		$data['created'] 	= strval($date);
		$data['created_by']	= $user->id;
		$data['tblUserID'] 	= 0;
		$table->bind($data);
		$table->store();
		$tblTrainerID = $table->id;
		// ADD THE TRAINER AS USER
		$password = '';
		if($formData->getStr('password')):
			$password = JUserHelper::hashPassword($formData->getStr('password'));
		endif;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$data = array();
		$data['name'] 		= $formData->getStr('first_name').' '.$formData->getStr('last_name');
		$data['username'] 	= $formData->getStr('email');
		$data['email'] 		= $formData->getStr('email');
		$data['password'] 	= $password;
		$data['block'] 		= 0;
		$data['sendEmail'] 	= 0;
		$data['registerDate'] 	= strval($date);
		$data['lastvisitDate'] 	= strval($date);
		$data['activation'] 	= 0;
		$table->bind($data);
		$table->store();
		$tblUserID = $table->id;
		//Tabelle Usergroup-Map
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Usergroup','JclassroomTable',array());
		$data = array();
		$data['user_id'] 	= $tblUserID;
		$data['group_id'] 	= 10;
		$table->bind($data);
		$table->store();
		// SAVE the user ID
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Trainer','JclassroomTable',array());
		$table->load($tblTrainerID);
		$table->tblUserID = $tblUserID;
		$table->store();
		// Write Logfile
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($session->get('customerID'));
		$customerName = $table->company_name;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Logs','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $formData->getInt('customerID');
		$data['userID'] 		= $tblUserID;
		$data['parameter'] 		= 'Create Trainer';
		$data['wert'] 			= 'Der Benutzer '.$user->name.', Kunde:  '.$customerName.' hat einen neuen Trainer '.$formData->getStr('first_name').' '.$formData->getStr('last_name').' angelegt.';
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$table->bind($data);
		$table->store();
		return $tblTrainerID;
	}
	public function editToDatabase() {
		$session 	= JFactory::getSession();
		$input 		= JFactory::getApplication()->input;
		$id 		= $input->get('id',0,'INT');
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$datum 		= JFactory::getDate();
		$date 		= new JDate($datum);
		$datum 		= $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$user 		= JFactory::getUser();
		// Handling für das Upload
		
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/plans/logo/reseller/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/plans/logo/reseller/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Trainer','JclassroomTable',array());
		$table->load($id);
		$table->customerID 		= $formData->getInt('customerID');
		$table->salutation 		= $formData->getStr('salutation');
		$table->first_name 		= $formData->getStr('first_name');
		$table->last_name 		= $formData->getStr('last_name');
		$table->company 	= $formData->getStr('company');
		$table->adress 		= $formData->getStr('adress');
		$table->postcode 	= $formData->getStr('postcode');
		$table->city		= $formData->getStr('city');
		$table->phone 		= $formData->getStr('phone');
		$table->email		= $formData->getStr('email');
		$table->web 		= $formData->getStr('web');
		if($savename) {
			$table->logo 		= $savename;
		}
		$table->asp 		= $formData->getStr('asp');
		$table->asp_phone	= $formData->getStr('asp_phone');
		$table->asp_email	= $formData->getStr('asp_email');
		$table->username 	= $formData->getStr('username');
		$table->password 	= $formData->getStr('password');
		$table->description = $formData->getStr('description');
		$table->skills 		= $formData->getStr('skills');
		$table->published 	= $formData->getInt('published');
		$table->modified	= strval($date);
		$table->modified_by	= $user->id;
		$table->store();
		$userID = $table->tblUserID;
		$password = '';
		if($formData->getStr('password')):
			$password = JUserHelper::hashPassword($formData->getStr('password'));
		endif;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$table->load($userID);
		$table->name 		= $formData->getStr('first_name').' '.$formData->getStr('last_name');
		$table->username 	= $formData->getStr('email');
		$table->password 	= $password;
		$table->email 		= $formData->getStr('email');
		$table->store();
		$tblUserID = $table->id;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Customer','JclassroomTable',array());
		$table->load($formData->getInt('customerID'));
		$customerName = $table->company_name;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Logs','JclassroomTable',array());
		$data = array();
		$data['customerID'] 	= $formData->getInt('customerID');
		$data['userID'] 		= $tblUserID;
		$data['parameter'] 		= 'Edit Trainer';
		$data['wert'] 			= 'Der Benutzer '.$user->name.', Kunde:  '.$customerName.' hat den Trainer '.$formData->getStr('first_name').' '.$formData->getStr('last_name').' bearbeitet.';
		$data['created'] 		= date('Y-m-d H:i:s');
		$data['created_by'] 	= $user->id;
		$table->bind($data);
		$table->store();	
		return $tblResellerID;
	}
	public function checkEmail() {
		$ret = 'OK';
		$input 	= JFactory::getApplication()->input;
		$userID = $input->get('userID', 0, 'INT'); 
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$load = array('email' => $input->get('email','','STR'));
		$check = $table->load($load);
		if($check && $table->id != $userID) {
			$ret = 'NOK';
		} 
		if($table->id = $userID) {

		}
		echo $ret;
		exit();
	}
}
?>