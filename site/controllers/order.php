<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Frage item controller class.
 *
 * @package     Auditum
 * @subpackage  Controllers
 */
class JclassroomControllerOrder extends JControllerForm
{
	public function save($key = NULL, $urlVar = NULL) {
		$input = JFactory::getApplication()->input;
		$id   = $input->get('id','','INT');
		$uid   = $input->get('uid','','INT');
		$clr   = $input->get('clr','','INT');
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'manager-administrator/orders-admin';
				break;
			case 'trainer':
				$retour = JURI::Root().'celearning/students';
				break;
			case 'customer':
				$retour = JURI::Root().'manager-customer';
				break;
			case 'student':
				$retour = JURI::Root().'order-edit?&id='.$id.'&type=fromSt&uid='.$uid.'&clr='.$clr;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function simpleSave($key = NULL, $urlVar = NULL) {
		$input 	= JFactory::getApplication()->input;
		$post 	= $input->get('jform', array(), 'array');
		$id    	= $input->get('id','','INT');
		$uid   	= $post['uid'];
		$clr   	= $post['clr'];
		if($id) {
			$this->editToDatabase();
		} else {
			$id = $this->saveToDatabase();
		}
		JFactory::getApplication()->enqueueMessage('Der Datensatz wurde gespeichert', 'Message');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = JURI::Root().'order-edit?&id='.$id;
				break;
			case 'trainer':
				$retour = JURI::Root().'student-edit?layout=edit&id='.$id;
				break;
			case 'customer':
				$retour = JURI::Root().'order-edit?&id='.$id;
				break;
			case 'student':
				$retour = JURI::Root().'order-edit?&id='.$id.'&type=fromSt&uid='.$uid.'&clr='.$clr;
				break;
		}
		$this->setRedirect(JRoute::_($retour, false));
	}
	public function saveToDatabase() {
		$user 	= JFactory::getUser();
		$session= JFactory::getSession();
		$input 	= JFactory::getApplication()->input;
		$id 	= $input->get('id', 0, 'INT');
		$post 	= $input->get('jform', array(), 'array');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Order','JclassroomTable',array());
		$user 	= JFactory::getUser();
		$data = array();
		$data['customerID'] 		= $session->get('customerID');
		$data['userID'] 			= $post['uid'];
		$data['classroomID'] 		= $post['clr'];
		$data['deliveryDate'] 		= date('Y-m-d', strtotime($post['deliveryDate']));
		$data['salutation'] 		= $post['salutation'];
		$data['first_name'] 		= $post['first_name'];
		$data['last_name'] 			= $post['last_name'];
		$data['adress'] 			= $post['adress'];
		$data['postcode'] 			= $post['postcode'];
		$data['city'] 				= $post['city'];
		$data['phone'] 				= $post['phone'];
		$data['mobile'] 			= $post['mobile'];
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		$tblOrderID 		= $table->id;
		return $tblOrderID;
	}
	
	public function editToDatabase() {
		$input 		= JFactory::getApplication()->input;
		$formData 	= new JInput($input->get('jform', '', 'array'));
		$user 		= JFactory::getUser();
		// Handling für das Upload
		$filename	= '';
		$savename 	= '';
		$files  	= $input->files->get('jform');
		$file   	= $files['logo'];
		if($file && $file['name'] <> '') {
			$filenameRaw 	= JFile::makeSafe($file['name']);
			$filename	= '/images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$savename	= 'images/jclassroom/students/'.str_replace(' ','-',$filenameRaw);
			$dest		= JPATH_SITE.$filename;
			JFile::upload($file['tmp_name'], $dest);
		}
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Order','JclassroomTable',array());
		$table->load($input->get('id', 0, 'INT'));
		$table->deliveryDate= $formData->getStr('deliveryDate');
		$table->adress 		= $formData->getStr('adress');
		$table->postcode 	= $formData->getStr('postcode');
		$table->city		= $formData->getStr('city');
		$table->phone		= $formData->getStr('phone');
		$table->mobile		= $formData->getStr('mobile');
		$table->published 	= $formData->getInt('published');
		$table->modified	= date('Y-m-d H:i:s');
		$table->modified_by	= $user->id;
		$table->store();
		
		return $tblResellerID;
	}
	function addPosition() {
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Orderposition','JclassroomTable',array());
		$data = array();
		$data['orderID'] 		= $input->get('orderID', 0,'INT');
		$data['hardwareID'] 	= $input->get('hardwareID', 0,'INT');
		$data['amount'] 		= $input->get('hardwareAmount', 0,'INT');
		$data['created'] 	= date('Y-m-d H:i:s');
		$data['created_by']	= $user->id;
		$data['published'] 	= 1;
		$table->bind($data);
		$table->store();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$hardware = JTable::getInstance('Hardware','JclassroomTable',array());
		$hardware->load($input->get('hardwareID',0,'INT'));
		echo $table->id.'_'.$hardware->title;
		exit();
	}
	function editPosition() {
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Orderposition','JclassroomTable',array());
		$table->load($input->get('positionID', 0, 'INT'));
		$table->hardwareID = $input->get('hardwareID', 0, 'INT');
		$table->amount = $input->get('hardwareAmount', 0, 'INT');
		$table->modified 	= date('Y-m-d H:i:s');
		$table->modified_by = $user->id;
 		$table->store();
 		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$hardware = JTable::getInstance('Hardware','JclassroomTable',array());
		$hardware->load($input->get('hardwareID',0,'INT'));
		echo $hardware->title;
		exit();
	}
	function getOrder() {
		$user 		= JFactory::getUser();
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Orderposition','JclassroomTable',array());
		$table->load($input->get('positionID', 0, 'INT'));
		$return = array(
			'hardware_id' => $table->hardwareID,
			'hardware_amount' => $table->amount
		);
		echo json_encode($return);
		exit();
	}
	function loadLearningroom() {
		$input 	= JFactory::getApplication()->input;
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.day');
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($input->get('learningroomID', 0, 'INT')));
		$query->order('a.day ASC');
		$query->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadResult();
		//echo date('d.m.Y', strtotime($db->loadResult()));
		if($result):
			echo date('d.m.Y', strtotime($result));
		else: 
			echo 'NOK_Keine Datumswerte für diesen Learningroom gefunden.';
		endif;
		exit();
	}
	function deletePosition() {
		$input 		= JFactory::getApplication()->input;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Orderposition','JclassroomTable',array());
		$load 	= array();
		$table->delete($input->get('id'));
		echo 'OK';
		exit();

	}
	public function printOrder() {
      $input = JFactory::getApplication()->input;
      $id = $input->get('id','','INT');
      echo $id;
      require_once JPATH_SITE. '/components/com_jclassroom/library/order.php';
      $printAudit = new printOrder();
      $printAudit->order($id);
   }
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_item = 'order';

	protected $view_list = 'orders';

}
?>