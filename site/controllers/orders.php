<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

class JclassroomControllerOrders extends JControllerAdmin
{
	public function delete() {
		$input = JFactory::getApplication()->input;
		$id = $input->get('id', 0, 'INT');
		$delete = $input->get('cid',array(), 'array');
		if($delete) {
			foreach($delete as $del) {
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Order','JclassroomTable',array());
				$db 	= JFactory::getDbo(); 
				$query 	= $db->getQuery(true);
				$query->select(array('a.id'));
		        $query->from($db->quoteName('#__jclassroom_order_positions','a'));
		        $query->where($db->quotename('a.orderID').' = '.$db->quote($del));
				$db->setQuery($query);
				$positions = $db->loadObjectList();
				if($positions):
					foreach($positions as $position):
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableO = JTable::getInstance('Orderposition','JclassroomTable',array());
						$tableO->delete($position->id);
					endforeach;
				endif;
				$table->delete($del);	
			}
		}
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$retour = 'bestellungen';
				break;
			case 'customer':
				$retour = 'bestellungen';
				break;
			case 'trainer':
				$retour = 'bestellungen';
				break;
		}
		JFactory::getApplication()->enqueueMessage('Die Datensätze wurden gelöscht', 'Message');
		$this->setRedirect(JRoute::_(JURI::Root().$retour, false));
	}
	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'orders';
	
	/**
	 * Get the admin model and set it to default
	 *
	 * @param   string           $name    Name of the model.
	 * @param   string           $prefix  Prefix of the model.
	 * @param   array			 $config  The model configuration.
	 */
	public function getModel($name = 'Student', $prefix='JclassroomModel', $config = array())
	{
		$config['ignore_request'] = true;
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>