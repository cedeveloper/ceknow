<?php

/**
 *
 *
 */

defined('_JEXEC') or die();

class TemplatesHelper {
	
  	function templateForDay($typ,$day = false) {
  		$html = '';
  		$html .= '<div class="classroomDay">';
	  		$html .= '<div class="classroomDayHeader">';
	  			$html .= '<div class="row">';
	  				$html .= '<div class="col-12 col-sm-10">';
	  				$html .= '<h4>'.date('d.m.Y', strtotime($day)).'</h4>';
	  				$html .= '</div>';
	  				$html .= '<div class="col-12 col-sm-2">';
	  				$html .= '<i class="fa fa-plus bg-success" onclick="addClassroomModule(&quot;'.$day.'&quot;);"></i>';
	  				$html .= '<i class="fa fa-trash-o bg-danger" onclick="deleteClassroomDay(&quot;'.$day.'&quot;);"></i>';
	  				$html .= '</div>';
	  			$html .= '</div>';
	  		$html .= '</div>';
	  		$html .= '<div class="classroomDayBody">';

	  		$html .= '</div>';
  		$html .= '</div>';
  		return $html;
    }
    function templateForModule($title,$day = false,$moduleID) {
  		$html = '';
  		$html .= '<div id="classroomModule'.$moduleID.'" class="classroomModule">';
	  		$html .= '<div class="classroomModuleHeader">';
	  			$html .= '<div class="row">';
	  				$html .= '<div class="col-12 col-sm-10">';
	  				$html .= '<h4>'.$title.'</h4><i class="fa fa-pencil"></i>';
	  				$html .= '</div>';
	  				$html .= '<div class="col-12 col-sm-2">';
	  				$html .= '<i class="fa fa-plus bg-success" onclick="addClassroomUnit('.$moduleID.');"></i>';
	  				$html .= '<i class="fa fa-trash-o bg-danger" onclick="deleteClassroomDay(&quot;'.$day.'&quot;);"></i>';
	  				$html .= '</div>';
	  			$html .= '</div>';
	  		$html .= '</div>';
	  		$html .= '<div class="classroomModuleBody">';

	  		$html .= '</div>';
  		$html .= '</div>';
  		return $html;
    }
}