<?php

/**
 *	
 *
 */

defined('_JEXEC') or die();

class QuizzresultHelper {

	function getResult($result) {
		$html 		= false;
		if($result) {
			$json = json_decode($result);
			$maxPoints = 0;
			$sumPoints = 0;
			foreach($json as $item) {
				if($item->type == 3) {
					$correct 	= 0;
					$choices 	= 0;
					$points 	= 0;
					foreach($item->answers as $answer) {
						if($answer->correct == 'on') {
							$correct++;
						}
						if($answer->correct == 'on' && $answer->choice == 1) {
							$choices++;
						}
					}
					if($correct == $choices) {
						$points = $item->points;
					} 
					$sumPoints += $points;
					$maxPoints += $item->points;
				}
				if($item->type == 5) {
					$correct 	= 0;
					$choices 	= 0;
					$points 	= 0;
					foreach($item->answers as $answer) {
						if($answer->correct == 'on') {
							$correct++;
						}
						if($answer->correct == 'on' && $answer->choice == 1) {
							$choices++;
						}
					}
					if($correct == $choices) {
						$points = $item->points;
					} 
					$sumPoints += $points;
					$maxPoints += $item->points;
				}
			}
			$score = ceil(($sumPoints * 100) / $maxPoints);
			/*$html .= '<div class="d-inline-block w-100 mt-2 mb-2">Sie haben <b>'.$sumPoints.'</b> von maximal erreichbaren <b>'.$maxPoints.'</b> erzielt. Das entspricht einem Score von <b style="font-size: 48px;">'.number_format($score,0).'%.</b></div>';
			//$resultText = $this->getResultText($sumPoints);
			if($resultText) {
				$html .= '<div class="d-inline-block w-100 mt-3 mb-3">';
				$html .='<b>Unsere Empfehlung für Sie:</b><br/>';
				//$html .= $this->getResultText($score);
				$html .= '</div>';
			}*/
		}
		$html = '<p>'.$sumPoints.' Punkte von '.$maxPoints.' Punkten erreicht. Score: '.$score.'%</p>';
		return $html;
	}
	function getAuswertung($result) {
		$html 		= false;
		if($result) {
			$result = json_decode($result);
			$maxPoints = 0;
			$sumPoints = 0;
			foreach($result as $item):
				if($item->type == 3) {
					$html .= '<p><b>'.$item->question.'</b></p>';
					$html .= '<div class="row">';
					$html .= '<div class="col-8 col-sm-8 text-left" style="font-weight: bold;">Antwort</div>';
					$html .= '<div class="col-2 col-sm-2 text-center" style="font-weight: bold;">Richtig</div>';
					$html .= '<div class="col-2 col-sm-2 text-center" style="font-weight: bold;">Ihre Wahl</div>';
					$html .= '</div>';
					$correct 	= 0;
					$choices 	= 0;
					$points 	= 0;
					foreach($item->answers as $answer) {
						if($answer->correct == 'on') {
							$correct++;
						}
						if($answer->correct == 'on' && $answer->choice == 1) {
							$choices++;
						}
					}
					if($correct == $choices) {
						$points = $item->points;
					} 
					foreach($item->answers as $answer) {

						$icoCorrect = '<i class="fa fa-times text-danger"></i>';
						$icoChoice 	= '<i class="fa fa-times text-danger"></i>';
						if($answer->correct == 'on') {
							$icoCorrect = '<i class="fa fa-check text-success"></i>';
						}
						if($answer->choice == 1) {
							$icoChoice = '<i class="fa fa-check text-success"></i>';
						}

						$html .= '<div class="row mt-1 mb-1">';
						$html .= '<div class="col-8 col-sm-8" style="line-height: 20px;">'.html_entity_decode($answer->answer).'</div>';
						$html .= '<div class="col-2 col-sm-2 text-center">'.$icoCorrect.'</div>';
						$html .= '<div class="col-2 col-sm-2 text-center">'.$icoChoice.'</div>';

						$html .= '</div>';
						
					}
					$html .= '<div class="row">';
					$html .= '<div class="col-10 col-sm-10"><b>Gesamtpunkte:</b></div>';
					$html .= '<div class="col-2 col-sm-2 text-center"><b>'.$points.'</b></div>';
					$html .= '</div>';					
					$sumPoints += $points;
					$maxPoints += $item->points;
				}
				/*if($item['type'] == 5) {
					$html .= '<div class="card bg bg-light mt-2 mb-2 p-1">';
					$html .= '<p class="m-0"><b>'.$item['question'].'</b></p>';
					$html .= '<div class="row">';
					$html .= '<div class="col-8 col-sm-8 text-left" style="font-weight: bold;">Antwort</div>';
					$html .= '<div class="col-2 col-sm-2 text-center" style="font-weight: bold;">Richtig</div>';
					$html .= '<div class="col-2 col-sm-2 text-center" style="font-weight: bold;">Ihre Wahl</div>';
					$html .= '</div>';
					$correct 	= 0;
					$choices 	= 0;
					$points 	= 0;
					foreach($item['answers'] as $answer) {
						if($answer['correct'] == 'on') {
							$correct++;
						}
						if($answer['correct'] == 'on' && $answer['choice'] == 1) {
							$choices++;
						}
					}
					if($correct == $choices) {
						$points = $item['points'];
					} 
					foreach($item['answers'] as $answer) {

						$icoCorrect = '<i class="fa fa-times text-danger"></i>';
						$icoChoice 	= '<i class="fa fa-times text-danger"></i>';
						if($answer['correct'] == 'on') {
							$icoCorrect = '<i class="fa fa-check text-success"></i>';
						}
						if($answer['choice'] == 1) {
							$icoChoice = '<i class="fa fa-check text-success"></i>';
						}

						$html .= '<div class="row mt-1 mb-1">';
						$html .= '<div class="col-8 col-sm-8" style="line-height: 20px;">'.html_entity_decode($answer['answer']).'</div>';
						$html .= '<div class="col-2 col-sm-2 text-center">'.$icoCorrect.'</div>';
						$html .= '<div class="col-2 col-sm-2 text-center">'.$icoChoice.'</div>';

						$html .= '</div>';
						
					}	
					$html .= '<div class="row">';
					$html .= '<div class="col-10 col-sm-10"><b>Gesamtpunkte:</b></div>';
					$html .= '<div class="col-2 col-sm-2 text-center"><b>'.$points.'</b></div>';
					$html .= '</div>';					
					$html .= '</div>';
					$sumPoints += $points;
					$maxPoints += $item['points'];
				}*/
			endforeach;
		}

		return $html;
	}
	function loadQuizzResultComplete($classroomID, $unitPosID, $publishedQuizzID, $quizzID, $chartID, $openQuizz) {
		//echo $classroomID.' '.$unitPosID.' '.$quizzID.' '.$chartID;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			c.title,
			c.content,
			c.chart,
			c.type,
			c.id as questionID,
			c.correctAnswers,
			c.ordering
		'));
        $query->from($db->quoteName('#__jclassroom_theresults','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.quizzID') . ')');
        if($classroomID):
	        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
	        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
	        $query->where($db->quotename('a.quizzID').' = '.$db->quote($quizzID));
	    endif;
	    if($publishedQuizzID):
	    	 $query->where($db->quotename('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
	    endif;
        $query->where('('.$db->quotename('c.type').' <> '.$db->quote(1).' AND '.$db->quotename('c.type').' <> '.$db->quote(14).')');
        $query->order('c.ordering asc ');
        $query->group('c.id');
		$db->setQuery($query);
		$questions = $db->loadObjectList();
		if($questions):
			foreach($questions as $question):
				$type 		= $quesion->type;
				$labels 	= array();
				$datas 		= array();
				$indiAnswers= '';
				//If a chart is chossen and the choosen chart is not wordCloud
				if($question->chart && $question->chart != 'wordCloud'):
					// WW-Fragen
					if($question->type == 3):
						$cCount = 0;
						$query = $db->getQuery(true);
						$query->select(array('
							a.title,a.id
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersSOLL = $db->loadObjectList();
						foreach($answersSOLL as $answer):
							$titles     = strip_tags($answer->title);
				            $titles     = htmlspecialchars_decode($titles);
				            $labels[]   = $titles;
							$theReal = $this->getUsersResult($question->questionID, $answer->id, $classroomID, $unitPosID, $publishedQuizzID);
							$datas[] 	= $theReal['users'];
							$cCount 	+= $theReal['users'];
						endforeach;
						if($question->correctAnswers == 2):
							$theRealActives = $this->getTheActiveUsersOfQuestion($question->questionID, $answer->id, $classroomID, $unitPosID, 3, $publishedQuizzID);
							$cCount  	= $theRealActives;
						endif;
					endif;
					// Feedback
					if($question->type == 4):
						//echo $classroomID.' '.$unitPosID.'<br/>';
						$answersIS = 0;
						$labels 	= array('Sehr schlecht', 'Schlecht','Nicht schlecht','Gut','Sehr gut');
						$cCount = 0;
						for($i = 1;$i <= 5;$i++) {
							$query = $db->getQuery(true);
							$query->select(array('
								count(b.answerID) as data
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        if($classroomID):
					        	$query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        	$query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
					        endif;
					        if($publishedQuizzID):
					        	$query->where($db->quotename('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
					        endif;
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quotename('b.answerID').' = '.$db->quote($i));
					        
					        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
							$db->setQuery($query);
							$answersIS = $db->loadResult();
							if($answersIS):
								$datas[] 	= $answersIS;
								$cCount += $answersIS;
							else:
								$datas[] 	= 0;
							endif;
						}
					endif;
					// Ja/Nein-Fragen
					if($question->type == 5):
						$cCount = 0;
						$query = $db->getQuery(true);
						$query->select(array('
							a.id,
							a.content
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					       $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
					    if($classroomID):
					    	$query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					    	$query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
					    	$query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
					    endif;
					    if($publishedQuizzID):
				        	$query->where($db->quotename('b.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
				        endif;
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
				        //$query->group('a.content,a.created_by');
						$db->setQuery($query);
						$answersSOLL = $db->loadObjectList();
						$labels[] 	= 'Ja';
						$labels[] 	= 'Nein';
						$dJ = 0;
						$dN = 0;
						foreach($answersSOLL as $answer):
							if($answer->content == 'answerJ'):
								$dJ++;
							endif;
							if($answer->content == 'answerN'):
								$dN++;
							endif;
						endforeach;
						$datas[] 	= $dJ;
						$datas[]	= $dN;
						$theRealActives = $this->getTheActiveUsersOfQuestion($question->questionID, '', $classroomID, $unitPosID, 7, $publishedQuizzID);
						$cCount  	= $theRealActives;
					endif;
					// Eingabefeld mit 1 Auswertung
					if($question->type == 7):
						$cCount = 0;
						$answersIS = 0;
						$query = $db->getQuery(true);
						$query->select(array('
							a.id,
							a.title
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersSOLL = $db->loadObjectList();
						foreach($answersSOLL as $answer):
							$labels[] 	= strip_tags($answer->title);
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as data
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        if($classroomID):
					        	$query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        	$query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
						        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
						    endif;
						    if($publishedQuizzID):
						    	$query->where($db->quotename('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
						    endif;
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
					        $query->where($db->quotename('b.content').' = '.$db->quote($answer->title));
					        $query->order('a.created DESC');
					        $query->setLimit(1);
							$db->setQuery($query);
							$answersIS = $db->loadResult();
							$datas[] 	= $answersIS;
							$cCount 	+= $answersIS;
						endforeach;
						$labels[] 		= 'Andere Antwort';
						$theRealActives = $this->getTheActiveUsersOfQuestion($question->questionID, '', $classroomID, $unitPosID, 7, $publishedQuizzID);
						$datas[] 	= $theRealActives - $cCount;
						$cCount  	= $theRealActives;
					endif;
					// Auswahlliste
					if($question->type == 12):
						$cCount = 0;
						$answersIS = 0;
						$query = $db->getQuery(true);
						$query->select(array('
							a.id,
							a.title
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersSOLL = $db->loadObjectList();
						foreach($answersSOLL as $answer):
							$labels[] 	= strip_tags($answer->title);
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as data
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        if($classroomID):
					        	$query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        	$query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
					        endif;
					        if($publishedQuizzID):
					        	$query->where($db->quotename('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
					        endif;
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));    
					        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
					        $query->order('a.created DESC');
					        $query->setLimit(1);
							$db->setQuery($query);
							$answersIS = $db->loadResult();
							$datas[] 	= $answersIS;
							$cCount 	+= $answersIS;
						endforeach;
						if($question->correctAnswers == 2):
							$theRealActives = $this->getTheActiveUsersOfQuestion($question->questionID,'', $classroomID, $unitPosID,12, $publishedQuizzID);
							$cCount  	= $theRealActives;
						endif;
					endif;
					// Checkboxen
					if($question->type == 13):
						$cCount 	= 0;
						$answersIS 	= 0;
						$query = $db->getQuery(true);
						$query->select(array('
							a.id,
							a.title
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersSOLL = $db->loadObjectList();
						foreach($answersSOLL as $answer):
							$labels[] 	= $answer->title;
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as data
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        if($classroomID):
					        	$query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        	$query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
					        endif;
					        if($publishedQuizzID):
					        	$query->where($db->quotename('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
					        endif;
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
					        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
					        $query->order('a.created DESC');
							$db->setQuery($query);
							$answersIS = $db->loadResult();
							$datas[] 	= $answersIS;
							$cCount 	+= $answersIS;
						endforeach;
						if($question->correctAnswers == 2):
							$theRealActives = $this->getTheActiveUsersOfQuestion($question->questionID, $answer->id, $classroomID, $unitPosID, 13,$publishedQuizzID);
							$cCount  	= $theRealActives;
						endif;
					endif;
				endif;
				if($question->chart && $question->chart == 'wordCloud'):
					$query = $db->getQuery(true);
					$query->select(array('
						count(a.id) as data,a.content
					'));
			        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
			        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
			        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
			        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
			        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
			        if($question->type == 4):
			        	$query->group('a.answerID');
			        endif;
					$db->setQuery($query);
					$answersIS = $db->loadObjectList();
					if($answersIS):
						foreach($answersIS as $answer):
							$datas[] 	= array(
								'text' 		=> $answer->content, 
								'weight' 	=> $answer->data,
								'color'		=> '#ff3600'
							);
						endforeach;
					endif;
				endif;
				// If there is no charttype
				if($question->chart == '' || $question->chart == '0'):
					$cCount = 0;
					// WWM-Fragen
					if($question->type == 3):
						$cCount = 0;
						$query 	= $db->getQuery(true);
						$query->select(array('
							a.title,a.id
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersSOLL = $db->loadObjectList();
						foreach($answersSOLL as $answer):
							$labels[] 	= strip_tags($answer->title);
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as cc
							'));
							$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
					        $query->where($db->quotename('a.answerID').' = '.$db->quote($answer->id));
					        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
					        $query->order('a.created DESC');
							$db->setQuery($query);
							$answersIS 	= $db->loadObject();
							if($answersIS):
								
								$datas[] 		= $answersIS->cc;
								$cCount 		+= $answersIS->cc;
							else:
								$datas[] 	= 0;
							endif;
							$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.strip_tags($answer->title).'</span> Anzahl: '.$answersIS->cc.'</p>'; 
						endforeach;
					endif;
					if($question->type == 4):
						$answersIS = 0;
						$labels 	= array('','Sehr schlecht', 'Schlecht','Nicht schlecht','Gut','Sehr gut');
						$cCount = 0;
						for($i = 1;$i <= 5;$i++) {
							$query = $db->getQuery(true);
							$query->select(array('
								count(b.answerID) as data
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quotename('b.answerID').' = '.$db->quote($i));
					        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
							$db->setQuery($query);
							$answersIS = $db->loadResult();
							if($answersIS):
								$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.$labels[$i].'</span> Anzahl: '.$answersIS.'</p>'; 
								$datas[] 		= $answersIS;
								$cCount 		+= $answersIS;
							else:
								$datas[] 	= 0;
							endif;
						}
					endif;
					if($question->type == 5):
						$cCount = 0;
						$labels[] 	= array('Ja', 'Nein');
						$query = $db->getQuery(true);
						$query->select(array('
							count(a.id) as cJ
						'));
						$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
				        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
				        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
				        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
				        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
				        $query->where($db->quotename('a.content').' = '.$db->quote('answerJ'));
						$db->setQuery($query);
						$db->execute();
						$answersISJ 	= $db->loadObject();
						if($answersISJ):
							$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">Ja</span> Anzahl: '.$answersISJ->cJ.'</p>'; 
							$cCount 		+= $answersISJ->cJ;
						else:
							$datas[] 	= 0;
						endif;
						$query = $db->getQuery(true);
						$query->select(array('
							count(a.id) as cN
						'));
						$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
				        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
				        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
				        $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
				        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
				        $query->where($db->quotename('a.content').' = '.$db->quote('answerN'));
						$db->setQuery($query);
						$db->execute();
						$answersISN 	= $db->loadObject();
						if($answersISN):
							$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">Nein</span> Anzahl: '.$answersISN->cN.'</p>'; 
							$cCount 		+= $answersISN->cN;
						else:
							$datas[] 	= 0;
						endif;
					endif;
					// Eingabefeld mit 1 Auswertung
					if($question->type == 7):
						$cCount = 0;
						$answersIS = 0;
						$query = $db->getQuery(true);
						$query->select(array('
							a.id,
							a.title
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersSOLL = $db->loadObjectList();
						foreach($answersSOLL as $answer):
							$labels[] 	= strip_tags($answer->title);
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as data
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
					        $query->where($db->quotename('b.content').' = '.$db->quote($answer->title));
					        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
					        $query->order('a.created DESC');
					        $query->setLimit(1);
							$db->setQuery($query);
							$answersIS = $db->loadResult();
							$datas[] 	= $answersIS;
							$cCount 	+= $answersIS;
						endforeach;
						$labels[] 		= 'Andere Antwort';
						$indiAnswers 	= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.strip_tags($answer->title).'</span> Anzahl: '.$answersIS.'</p>';
						$delta = $cCount - $answersIS; 
						$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">Andere Antwort</span> Anzahl: '.$delta.'</p>';
					endif;
					// Texteingabefeld or Textarea
					if($question->type == 10 || $question->type == 11):
						$answersIS = 0;
						$query = $db->getQuery(true);
						$query->select(array('
							group_concat(b.content SEPARATOR "<br/>") as indiAnswers,
							count(a.id) as cCount
						'));
				        $query->from($db->quoteName('#__jclassroom_theresults','a'));
				        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
				        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
				        if($classroomID):
				        	$query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
				        endif;
				        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
				        $query->where('b.content <> ""');
				        $query->where('a.preview = 0');
						$db->setQuery($query);
						$answersIS = $db->loadObject();
						if($answersIS):
							$indiAnswers 	= $answersIS->indiAnswers;
						else:
							$indiAnswers 	= '';
						endif;
						$cCount = $answersIS->cCount;
					endif;
					// List
					if($question->type == 12):
						$answersIS = 0;
						$query = $db->getQuery(true);
						$query->select(array('
							a.id,
							a.title
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersSOLL = $db->loadObjectList();
						foreach($answersSOLL as $answer):
							$labels[] 	= html_entity_decode($answer->title);
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as data
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
					        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
					        $query->order('a.created DESC');
					        $query->setLimit(1);
							$db->setQuery($query);
							$answersIS = $db->loadResult();
							$datas[] 	= $answersIS;
							$cCount 	+= $answersIS;
							$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.$answer->title.'</span> Anzahl: '.$answersIS.'</p>'; 
						endforeach;
					endif;
					// Checkboxen
					if($question->type == 13):
						$cCount 	= 0;
						$answersIS 	= 0;
						$query = $db->getQuery(true);
						$query->select(array('
							a.id,
							a.title
						'));
				        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
				        $query->where($db->quotename('a.questionID').' = '.$db->quote($question->questionID));
						$db->setQuery($query);
						$answersSOLL = $db->loadObjectList();
						foreach($answersSOLL as $answer):
							$labels[] 	= $answer->title;
							$query = $db->getQuery(true);
							$query->select(array('
								count(a.id) as data
							'));
					        $query->from($db->quoteName('#__jclassroom_theresults','a'));
					        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
					        $query->where($db->quotename('b.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
					        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
					        $query->where($db->quotename('a.unitID').' = '.$db->quote($unitPosID));
					        $query->order('a.created DESC');
					        $query->setLimit(1);
							$db->setQuery($query);
							$answersIS = $db->loadResult();
							$datas[] 	= $answersIS;
							$cCount 	+= $answersIS;
							$indiAnswers 	.= '<p style="margin: 4px 0px;"><span style="font-weight: bold;font-style: italic;">'.$answer->title.'</span> Anzahl: '.$answersIS.'</p>'; 
						endforeach;
					endif;
				endif;
				$theQuestions[] = array(
					'questionType' => $question->type,
					'questionID'=> $question->questionID,
					'question' 	=> $question->title,
					'content'	=> $question->content,
					'chart' 	=> $question->chart,
					'labels' 	=> $labels,
					'datas' 	=> $datas,
					'indiAnswers'	=> $indiAnswers,
					'cCount' 	=> $cCount,
					'type' 		=> $type,
					'correctAnswers' => $question->correctAnswers,
					'usersFinishedTheQuizz' => $this->usersFinishedTheQuizz($classroomID, $quizzID)
				);
			endforeach;
		endif;

		return $theQuestions;
	}
	function getUsersResult($questionID, $answerID, $classroomID, $unitPosID, $publishedQuizzID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.id
		'));
		$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
        if($classroomID):
        	$query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
        endif;
        $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
        $query->where($db->quotename('a.answerID').' = '.$db->quote($answerID));
        if($classroomID):
        	$query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
        	$query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
        endif;
        if($publishedQuizzID):
        	$query->where($db->quotename('b.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
        endif;
        $query->order('a.created DESC');
		$db->setQuery($query);
		$db->execute();
		$numRows 	= $db->getNumRows();
		$return 	= array('users' => $numRows);

		return $return;
	}
	function getTheActiveUsersOfQuestion($questionID, $answerID, $classroomID, $unitPosID, $type, $publishedQuizzID) {
		//echo "QuestionID".$questionID.' AnswerID'.$answerID.' ClassroomID'.$classroomID.' UnitPosID'.$unitPosID.'<br>';
		// Calculate the active students
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.id
		'));
		$query->from($db->quoteName('#__jclassroom_quizzresults','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
        if($classroomID):
        	$query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
        	$query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
        endif;
        if($publishedQuizzID):
        	$query->where($db->quotename('b.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
        endif;
        $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
        if($answerID):
        	$query->where($db->quotename('a.answerID').' = '.$db->quote($answerID));
        endif;
       
        $query->where($db->quotename('b.unitID').' = '.$db->quote($unitPosID));
        if($type == 12 || $type == 13):
        	$query->group('a.created_by');
        endif;
        $query->order('a.created DESC');
		$db->setQuery($query);
		$db->execute();
		$numRows 	= $db->getNumRows();
		//echo '<pre>';
		//echo $db->loadObjectList();

		return $numRows;
	}
	function usersFinishedTheQuizz($classroomID, $quizzID) {
		// Calculate, how much users had finished the quizz
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.id
		'));
		$query->from($db->quoteName('#__jclassroom_theresults','a'));
       	$query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
        $query->where($db->quotename('a.quizzID').' = '.$db->quote($quizzID));
        $query->where($db->quotename('a.preview').' = 0');
        $query->order('a.created DESC');
        //$query->group('a.created_by');
		$db->setQuery($query);
		$db->execute();
		$numRows 	= $db->getNumRows();

		return $numRows;
	}
}