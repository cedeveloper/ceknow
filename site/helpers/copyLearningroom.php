<?php

/**
 *	
 *
 */

defined('_JEXEC') or die();

class CopyLearningroomHelper {
	
  	function copyLearningroom($id) {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$classroom = JTable::getInstance('Classroom','JclassroomTable',array());
		$classroom->load($id);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableN = JTable::getInstance('Classroom_library','JclassroomTable',array());
		$data = array();
		$data['customerID'] 		= $classroom->customerID;
		$data['companyID'] 			= $classroom->companyID;
		$data['title'] 				= $classroom->title.' (copy from LR '.$id.')';
		$data['description'] 		= $classroom->description;
		$data['main_trainer'] 		= $classroom->main_trainer;
		$data['co_trainer'] 		= $classroom->co_trainer;
		$data['fromDate'] 			= '';
		$data['toDate'] 			= '';
		$data['inventa_terminID'] 	= $classroom->inventa_terminID;
		$data['inventa_kursID'] 	= $classroom->inventa_kursID;
		$data['email_template'] 	= $classroom->email_template;
		$data['verification_template'] 	= $classroom->verification_template;
		$data['remember_template'] 	= $classroom->remember_template;
		$data['published'] 	= $classroom->published;
		$data['created'] 	= date('Y-m-d H.i:s');
		$data['created_by']	= $user->id;
		$data['logo'] 		= $classroom->logo;
		$data['files'] 			= $classroom->files;
		$data['files_students'] 	= $classroom->files_students;
		$data['published'] 	= $classroom->published;
		$data['showto'] 	= $classroom->showto;
		$data['editto'] 	= $classroom->editto;
		$data['choosableto'] 	= $classroom->choosableto;
		$tableN->bind($data);
		$tableN->store();
		$newLearningRoomID = $tableN->id;
		//Create imagefolders for new LR
		$pathFL 	= JPATH_SITE.'/images/learningrooms/LR'.$newLearningRoomID;
		JFolder::create($pathFL);
		JFolder::create($pathFL.'/material');
		JFolder::create($pathFL.'/reserve');
		JFolder::create($pathFL.'/material_students');
		JFolder::create($pathFL.'/units');

		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($id));
        $query->order('a.day ASC');
		$db->setQuery($query);
		$days = $db->loadObjectList();
		if($days):
			foreach($days as $day):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_days_library','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $day->customerID;
				$data['classroomID']= $newLearningRoomID;
				$data['day'] 		= $day->day;
				$data['title'] 		= $day->title;
				$data['ordering'] 	= $day->ordering;
				$data['dayID'] 		= $day->dayID;
				$data['published'] 	= $day->published;
				$tableD->bind($data);
				$tableD->store();
				$newDayID = $tableD->id;
				$this->copyModules($day->id,$newDayID, $newLearningRoomID);
			endforeach;
		endif;
		echo $newLearningRoomID;
		exit();
	}
	function copyModules($oldDayID, $newDayID, $newLearningRoomID) {
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
        $query->where($db->quotename('a.dayID').' = '.$db->quote($oldDayID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$modules = $db->loadObjectList();
		if($modules):
			foreach($modules as $module):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $module->customerID;
				$data['classroomID']= $newLearningRoomID;
				$data['dayID'] 		= $newDayID;
				$data['title'] 		= $module->title;
				$data['description'] = $module->description;
				$data['ordering'] 	= $module->ordering;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $module->published;
				$tableD->bind($data);
				$tableD->store();
				$this->copyUnits($module->id, $tableD->id, $newLearningRoomID);
			endforeach;
		endif;
	}
	function copyUnits($oldModuleID, $newModuleID, $newLearningRoomID) {
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
        $query->where($db->quotename('a.moduleID').' = '.$db->quote($oldModuleID));
        $query->order('a.ordering ASC');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableD = JTable::getInstance('Classroom_days_units_library','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $unit->customerID;
				$data['classroomID']= $newLearningRoomID;
				$data['dayID'] 		= 0;
				$data['moduleID'] 	= $newModuleID;
				$data['unitID'] 	= $unit->unitID;
				$data['unitType'] 	= $unit->unitType;
				$data['ordering'] 	= $unit->ordering;
				$data['duration'] 	= $unit->duration;
				$data['title'] 		= $unit->title;
				$data['content'] 	= $unit->content;
				$data['link'] 		= $unit->link;
				$data['quizzID'] 	= $unit->quizzID;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $unit->published;
				$tableD->bind($data);
				$tableD->store();
				$newUnitID = $tableD->id;
				// Copy files
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$pathLibraryToMove = '/images/learningrooms/LR'.$newLearningRoomID.'/units/';
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$tableF = JTable::getInstance('File_library','JclassroomTable',array());
						$data = array();
						$data['type'] 		= $file->type;
						$data['classroomID']= $newLearningRoomID;
						$data['unitID'] 	= $newUnitID;
						$data['studentID'] 	= 0;
						$data['folder'] 	= '';
						$data['filename'] 	= $file->filename;
						$data['path'] 		= $pathLibraryToMove.'/'.$file->filename;
						$data['created'] 	= date('Y-m-d H:i:s');
						$data['created_by'] = $user->id;
						$data['published'] 	= $file->published;
						$tableF->bind($data);
						$tableF->store();
						copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.'/'.$file->filename);
					endforeach;
				endif;
			endforeach;
		endif;
	}
}