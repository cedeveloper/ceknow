<?php

/** ceLearning 29.06.2020
 *	HelperClass TESTS
 *	Used to load all quizzes, which are available for
 *  the currently logged in user. SuperUser has full access on all quizzes.
 *  Trainers has only acces on their own quizzes or on approved quizzes.
 * 	Developer: Torsten Scheel
 */

defined('_JEXEC') or die();

class TestsHelper {
	
  	public function getTests() {
      $session    = JFactory::getSession();
      $user       = JFactory::getUser();
      $usergroup  = $session->get('group');
      $db 	      = JFactory::getDbo(); 
      $query 	   = $db->getQuery(true);
      $query->select(array('
         a.id,
         a.title,
         a.showto,
         a.created_by,
         a.choosableto
      '));
      $query->from($db->quoteName('#__jclassroom_units','a'));
      $query->where($db->quotename('a.published').' = '.$db->quote(1));
      if(
         $session->get('group') == 'customer' || 
         $session->get('group') == 'trainer') 
      {
         $query->where($db->quotename('a.customerID').' = '.$db->quote($session->get('customerID')));
      }
      $query->order('a.title asc');
      $db->setQuery($query);
      $result = $db->loadObjectList();
      if($result):
         foreach($result as $item):
            if($usergroup == 'superuser'):
               $return[] = array('id' => $item->id,'title' => $item->title);
            endif;
            if($usergroup == 'customer'):
               if($item->showto == 0):
                  if($item->created_by == $user->id):
                     $return[] = array('id' => $item->id,'title' => $item->title);
                  endif;
               endif;
               if($item->showto == 1):
                  //if($item->created_by == $user->id):
                     $return[] = array('id' => $item->id,'title' => $item->title);
                  //endif;
               endif;
               if($item->showto == 3):
                  $return[] = array('id' => $item->id,'title' => $item->title);
               endif;
            endif;
            if($usergroup == 'trainer'):
               if($item->choosableto == 2):
                  $return[] = array('id' => $item->id,'title' => $item->title);
               elseif($item->choosableto == 0):
                  
               elseif($item->choosableto == 1):
                  if($item->created_by == $user->id):
                     $return[] = array('id' => $item->id,'title' => $item->title);
                  endif;
               endif;
            endif;
         endforeach;
      endif;
		return $return;
	}
}