<?php

/**
 *	
 *
 */

defined('_JEXEC') or die();

class ModulesHelper {
	
  	public function getTemplate($classroomID,$dayID,$moduleID,$moduleTitle,$fromLibrary = false) {
  		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$table->load($moduleID);
		$moduleDescription = $table->description;
		if($table->published == 1):
			$hide 	= 'fa-eye';
		else:
			$hide 	= 'fa-eye-slash';
		endif;
		if(!$moduleTitle):
			$moduleTitle = $table->title;
			
		endif;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$tableU = JTable::getInstance('User','JclassroomTable',array());
		$tableU->load($table->created_by);
		$created 	= date('d.m.Y H:i', strtotime($table->created));
		$createdby 	= $tableU->name;
		$colorDescription = 'text-danger';
		$showDescription	= 'hideInfo';
		if($table->showDescription == 1):
			$colorDescription 	= 'text-success';
			$showDescription	= 'showInfo';
		endif;
  		$html = '';
	    $html .= '<div id="module'.$moduleID.'" class="module">';
       	$html .= '<div class="moduleHeader row" style="justify-content: space-between;">';
	    $html .= '<div class="col-12 col-sm-3 mt-1">';
		$html .= '<i title="Modul verschieben"class="move fa fa-ellipsis-v"></i>';
		$html .= '<i title="Modul löschen" onclick="deleteModule(' .$moduleID. ');" class="trash fa fa-trash-o"></i>';
		$html .= '<i title="Modul im Staging verstecken/anzeigen" onclick="hideModule(' .$moduleID. ');" class="hide ml-3 trash fa '.$hide.'"></i>';
		$html .= '<i title="Modul in ein anderes Thema verschieben" onclick="moveModule('.$moduleID.');" class="calendar ml-2 fa fa-arrows-alt"></i>';
		$html .= '<i title="Modul speichern" onclick="saveModule(' .$moduleID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
		$html .= '<i title="Modul in Bibliothek speichern" onclick="saveModuleLibrary(' .$moduleID. ');" class="ml-3 saveLibrary fa fa-university"></i>';
		$html .= '<i title="Modul auf- / zuklappen" onclick="openModule(' .$moduleID. ');" class="ml-3 fa fa-chevron-right"></i>';
		$html .= '</div>';
		$html .= '<div class="col-12 col-sm-7 text-center">';
		$html .= '<input data-id="'.$moduleID.'" type="text" class="moduleTitle" placeholder="Titel" value="'.$moduleTitle.'" />';
		$html .= '<i id="openInfo'.$moduleID.'" class="open-info fas fa-info-circle '.$colorDescription.'" onclick="openDescription('.$moduleID.');"title="Beschreibungsbox öffnen/schließen"></i>';
		$html .= '<textarea id="theInfo'.$moduleID.'" data-id="'.$moduleID.'" type="text" class="moduleDescription '.$showDescription.'" placeholder="Beschreibung">'.$moduleDescription.'</textarea>';
		$html .= '</div>';
		$html .= '<div class="moduleRight col-12 col-sm-2 text-right d-flex">';
		$html .= '<a title="Unit aus Bibliothek einfügen" id="addForLibrary' .$moduleID. '" onclick="addUnitFromLibrary('.$moduleID.');"><i style="font-size: 24px;border-radius: 50%;" class="addUnit fa fa-university bg-info text-white p-1"></i></a>';
		$html .= '<a title="Unit einfügen" id="addFor' .$moduleID. '" onclick="addUnit('.$moduleID.');"><i style="font-size: 24px;border-radius: 50%;" class="addUnit fa fa-plus bg-success text-white p-1"></i></a>';
		$html .= '<div class="moduleInformation" style="text-align: left;">';
		$html .= '<h4>MODUL</h4>';
		$html .= '<p style="margin: 2px 0;font-size: 9px;line-height: 12px;"><span style="display: inline-block; width: 40px;">Erstellt: </span>'.$created.'</p>';
		$html .= '<p style="margin: 2px 0;font-size: 9px;line-height: 12px;"><span style="display: inline-block; width: 40px;">Von: </span>'.$createdby.'</p>';
		$html .= '<p style="margin: 2px 0;font-size: 12px;">MID: '.$moduleID.'</p>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<input type="hidden" class="indicator" id="saveIndicatorModule'.$moduleID.'" value="0" />';
		$html .= '</div>';
		$html .= '<p class="mt-1 mb-1"><b>Units</b></p>';
		$html .= '<div id="moduleContent' .$moduleID. '" class="hide moduleContent mt-1">';
		$html .= '<ul style="padding: 0;">';
		$html .= '{units}';
		$html .= '</ul>';
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

}