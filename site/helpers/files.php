<?php

/**
 *	
 *
 */

defined('_JEXEC') or die();

class FilesHelper {
	
  	public function getDirContents($dir, &$results = array()) {
	    $files = scandir($dir);
	    foreach ($files as $key => $value) {
	        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
	        if (!is_dir($path)) {

	        } else if ($value != "." && $value != "..") {

	            $subfiles = $this->getFiles($path);
	            $results[] = array('type' => 'folder', 'name' => $value, 'path' => $path,'subfiles' => $subfiles);
	        }
	    }

	    return $results;
	}
	function getFiles($path) {
		$subfiles = array();
		$dir = new DirectoryIterator($path);
		foreach ($dir as $fileinfo) {
			$filename = $fileinfo->getFilename();
			$pathname = $fileinfo->getPathname();
			if($filename != "." && $filename != ".."):
				$size = filesize($pathname);
				$subfiles[] = array('type' => 'file','name' => $filename,'path' => $pathname, 'size' => $size);
			endif;
		}
		usort($subfiles, function($a, $b) {
		    return $a['name'] <=> $b['name'];
		});
		return $subfiles;
	}
	function copyFilesFromLibrary($originLibraryID, $newClassroomID) {
		$user 		= JFactory::getUser();
		$db = JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select('a.*');
		$query->from($db->quoteName('#__jclassroom_files_library','a'));
		$query->where($db->quotename('a.classroomID').' = '.$db->quote($originLibraryID));
		$db->setQuery($query);
		$files = $db->loadObjectList();
		if($files):
			foreach($files as $file):
				$pathLibraryToMove = '/images/learningrooms/LR'.$newClassroomID.'/units/';
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableF = JTable::getInstance('File','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $customerID;
				$data['type'] 		= $file->type;
				$data['classroomID']= $newClassroomID;
				$data['unitID'] 	= 0;
				$data['studentID'] 	= 0;
				$data['folder'] 	= '';
				$data['filename'] 	= $file->filename;
				$data['path'] 		= $pathLibraryToMove.$file->filename;
				$data['extention'] 	= $file->extention;
				$data['size'] 		= $file->size;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $file->published;
				$tableF->bind($data);
				$tableF->store();
				copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.$file->filename);
			endforeach;
		endif;
	}
	function copyFilesToLibrary($originClassroomID, $newClassroomID) {
		$user 		= JFactory::getUser();
		$session 	= JFactory::getSession();
		$customerID	= $session->get('customerID');
		$db = JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select('a.*');
		$query->from($db->quoteName('#__jclassroom_files','a'));
		$query->where($db->quotename('a.classroomID').' = '.$db->quote($originClassroomID));
		$db->setQuery($query);
		$files = $db->loadObjectList();
		if($files):
			foreach($files as $file):
				$pathLibraryToMove = '/images/library/learningrooms/LR'.$newClassroomID.'/units/';
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableF = JTable::getInstance('File_library','JclassroomTable',array());
				$data = array();
				$data['customerID'] = $customerID;
				$data['type'] 		= $file->type;
				$data['classroomID']= $newClassroomID;
				$data['unitID'] 	= 0;
				$data['studentID'] 	= 0;
				$data['folder'] 	= '';
				$data['filename'] 	= $file->filename;
				$data['path'] 		= $pathLibraryToMove.$file->filename;
				$data['extention'] 	= $file->extention;
				$data['size'] 		= $file->size;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published'] 	= $file->published;
				$tableF->bind($data);
				$tableF->store();
				copy(JPATH_SITE.$file->path,JPATH_SITE.$pathLibraryToMove.$file->filename);
			endforeach;
		endif;
	}
	function removeFilesFromLibrary($id) {
		$db = JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select('a.id');
		$query->from($db->quoteName('#__jclassroom_files_library','a'));
		$query->where($db->quotename('a.classroomID').' = '.$db->quote($id));
		$db->setQuery($query);
		$files = $db->loadObjectList();
		if($files):
			foreach($files as $file):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('File_library','JclassroomTable',array());
				$table->delete($file->id);
			endforeach;
			$pathFL 	= JPATH_SITE.'/images/library/learningrooms/LR'.$id;
			JFolder::delete($pathFL);
		endif;

	}
}