<?php

/**
 *	
 *
 */

defined('_JEXEC') or die();

class DayHelper {
	
  	public function getTemplate($day,$dayID) {
  		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_days','JclassroomTable',array());
		$table->load($dayID);
		if($table->published == 1):
			$hide 	= 'fa-eye';
		else:
			$hide 	= 'fa-eye-slash';
		endif;
		$title 	= $table->title;
		$date 	= date('l, d.m.Y', strtotime($day));
		
  		$html = '';
  		$html .= '<small id="idFor'.$dayID.'">ID: '.str_pad($dayID,6,'0', STR_PAD_LEFT).'</small><br/>';
  		$html .= '<div id="cardFor' .$dayID.'" class="card bg-light mb-2">';
  			$html .= '<div class="card-header d-flex" style="justify-content: space-between;">';
				$html .= '<div class="col-12 col-sm-2 card-header-action">';
					$html .= '<i title="Thema aufklappen" onclick="openDay('.$dayID.');" class="open fa fa-chevron-right"></i>';
					$html .= '<i title="Thema löschen" onclick="deleteDay('.$dayID.');" class="ml-2 fa fa-trash-o"></i>';
					$html .= '<i title="Thema verstecken/anzeigen" onclick="hideDay('.$dayID.');" class="hide ml-2 fa '.$hide.'"></i>';
					//$html .= '<i title="Tag verschieben" onclick="moveDay('.$dayID.');" class="ml-2 fa fa-calendar"></i>';
					$html .= '<i title="Thema in Bibliothek speichern" onclick="addDayToLibrary(' .$dayID. ',&quot;'.$table->title.'&quot;);" class="ml-2 saveLibrary fa fa-university"></i>';
				$html .= '</div>';
			$html .= '<div class="col-12 col-sm-8 text-center d-flex" style="justify-content: center;align-items: center;">';
			$html .= '<h2 class="dayTitle m-0 mr-2">'.$title.' </h2><i onclick="editDayTitle('.$dayID.');" class="fa fa-pencil" title="Titel bearbeiten"></i>';
			$html .= '</div>';
			$html .= '<div class="moduleRight col-12 col-sm-2 text-right d-flex" style="justify-content: end;">';
				$html .= '<a class="mr-2 text-right" title="Modul aus Bibliothek einfügen" id="addModuleFromLibrary' .$dayID. '" onclick="addModuleFromLibrary('.$dayID.');"><i style="font-size: 24px;border-radius: 50%;" class="addUnit fa fa-university bg-info text-white p-1"></i></a>';
				$html .= '<a class="text-right" title="Modul einfügen" id="addFor'.$dayID.'" onclick="addModule('.$dayID.', 0);">
			 <i style="font-size: 24px;border-radius: 50%;cursor: pointer;" class="addModule fa fa-plus bg-danger text-white p-1"></i>
			 </a>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="card-body">';
			$html .= '{modules}';
			$html .= '</div>';
		$html .= '</div>';

		return $html;
	}
}