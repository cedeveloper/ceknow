<?php

/**
 *	
 *
 */

defined('_JEXEC') or die();

class GlobalHelper {

    function getCustomer($setCustomerID = false) {
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$customerID = $setCustomerID ? $setCustomerID : $customerID;
		$db 	= JFactory::getDbo();
		// Load the customer data
        $query 	= $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__jclassroom_customer','a'));
		$query->where('a.id = '.$customerID);
		$db->setQuery($query);
		$customer = $db->loadObject();
		// Load Administrators
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.*,
			b.name,
			b.company_name,
			b.customer_number
		'));
        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
		$query->join('LEFT', $db->quoteName('#__jclassroom_customer', 'b') . ' ON (' . $db->quoteName('a.customerID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where('a.customerID = '.$customerID);
		$db->setQuery($query);
		$customerA = $db->loadObjectList();
		// Load Trainer
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.*,
			b.name,
			b.company_name,
			b.customer_number
		'));
        $query->from($db->quoteName('#__jclassroom_trainers','a'));
		$query->join('LEFT', $db->quoteName('#__jclassroom_customer', 'b') . ' ON (' . $db->quoteName('a.customerID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where('a.customerID = '.$customerID);
		$db->setQuery($query);
		$trainers = $db->loadObjectList();
		// Load Students
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.*,
			b.id as companyID,
			b.name as company_name
		'));
        $query->from($db->quoteName('#__jclassroom_students','a'));
		$query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'b') . ' ON (' . $db->quoteName('a.companyID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where('a.customerID = '.$customerID);
		$db->setQuery($query);
		$students = $db->loadObjectList();
		// Load Companies
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__jclassroom_companys','a'));
		$query->where('a.customerID = '.$customerID);
		$db->setQuery($query);
		$companies = $db->loadObjectList();
		// Load Learningrooms
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.id,
			a.title
		'));
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
		$query->where('a.customerID = '.$customerID);
		$query->order('a.title ASC');
		$db->setQuery($query);
		$learningrooms = $db->loadObjectList();
		// Load Quizze
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.id,
			a.title
		'));
        $query->from($db->quoteName('#__jclassroom_units','a'));
		$query->where('a.customerID = '.$customerID);
		$query->order('a.title ASC');
		$db->setQuery($query);
		$quizze = $db->loadObjectList();
		// Load Quizze
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.id,
			a.title
		'));
        $query->from($db->quoteName('#__jclassroom_templates','a'));
		$query->where('a.customerID = '.$customerID);
		$query->order('a.title ASC');
		$db->setQuery($query);
		$templates = $db->loadObjectList();
		// Set array
		$return = array(
			'customerID' 	=> $customerID,
			'customer' 		=> $customer,
			'administrators'=> $customerA,
			'trainers' 		=> $trainers,
			'students' 		=> $students,
			'companies' 	=> $companies,
			'learningrooms'	=> $learningrooms,
			'quizze' 		=> $quizze,
			'templates' 	=> $templates
		);
		return $return;
	}
	function getCompany($companyID) {
		$db 	= JFactory::getDbo();
		// Load the customer data
        $query 	= $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__jclassroom_companys','a'));
		$query->where('a.id = '.$companyID);
		$db->setQuery($query);
		$company = $db->loadObject();
		// Load Students
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.*,
			b.id as companyID,
			b.name as company_name
		'));
        $query->from($db->quoteName('#__jclassroom_students','a'));
		$query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'b') . ' ON (' . $db->quoteName('a.companyID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where('b.id = '.$companyID);
		$query->order('a.last_name ASC');
		$db->setQuery($query);
		$students = $db->loadObjectList();
		// Set array
		$return = array(
			'students' 		=> $students,
			'company' 		=> $company,
		);
		return $return;
	}
	function getClassroom($classroomID) {
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$db 	= JFactory::getDbo();
		// Load the customer data
        $query 	= $db->getQuery(true);
		$query->select(array('
			a.*,
			b.name as main_trainer_name,
			c.name as co_trainer_name,
			d.name as created_by_name,
			e.name as modified_by_name,
			f.text as verification_text,
			f.subject as verification_subject,
			g.text as invitation_text,
			g.subject as invitation_subject,
			h.text as remember_text,
			h.subject as remember_subject,
			i.name as company_name
		'));
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
		$query->join('LEFT', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('a.main_trainer') . ' = ' . $db->quoteName('b.id') . ')');
		$query->join('LEFT', $db->quoteName('#__users', 'c') . ' ON (' . $db->quoteName('a.co_trainer') . ' = ' . $db->quoteName('c.id') . ')');
		$query->join('LEFT', $db->quoteName('#__users', 'd') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('d.id') . ')');
		$query->join('LEFT', $db->quoteName('#__users', 'e') . ' ON (' . $db->quoteName('a.modified_by') . ' = ' . $db->quoteName('e.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_templates', 'f') . ' ON (' . $db->quoteName('a.verification_template') . ' = ' . $db->quoteName('f.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_templates', 'g') . ' ON (' . $db->quoteName('a.email_template') . ' = ' . $db->quoteName('g.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_templates', 'h') . ' ON (' . $db->quoteName('a.remember_template') . ' = ' . $db->quoteName('h.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'i') . ' ON (' . $db->quoteName('a.companyID') . ' = ' . $db->quoteName('i.id') . ')');
		$query->where('a.id = '.$classroomID);
		$db->setQuery($query);
		$classroom = $db->loadObject();
		// Load the days
		$query 	= $db->getQuery(true);
		$query->select(array('
			Min(a.title) as first_day,
			MAX(a.title) as last_day'
		));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        //$query->where($db->quotename('a.type').' = '.$db->quote($classroom->email_template));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$days 	= $db->loadObject();
		//Load the days 
		$query 	= $db->getQuery(true);
		$query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
        $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
		$db->setQuery($query);
		$allDays 	= $db->loadObjectList();
		$all_days = '';
		if($allDays):
			foreach($allDays as $dayS1):
				$all_days .= date('d.m.Y', strtotime($dayS1->title)).'<br/>';
			endforeach;
		endif;
		// Load Students
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.*,
			b.*
		'));
        $query->from($db->quoteName('#__jclassroom_students','a'));
		$query->join('LEFT', $db->quoteName('#__jclassroom_classroom_students', 'b') . ' ON (' . $db->quoteName('a.tblUserID') . ' = ' . $db->quoteName('b.userID') . ')');
		$query->where('b.classroomID = '.$classroomID);
		$db->setQuery($query);
		$students = $db->loadObjectList();
		// Set array
		$return = array(
			'classroomID' 	=> $classroomID,
			'classroom' 	=> $classroom,
			'students' 		=> $students,
			'days' 			=> $days,
			'all_days' 		=> $all_days
		);

		return $return;
	}
	function getSystemdata() {
		$login 		= $this->getLogindata();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 		= array('customerID' => 0, 'parameter' => 'systemname');
		$table->load($load);
		$systemName = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemLink');
		$table->load($load);
		$systemLink = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemversion');
		$table->load($load);
		$systemVersion= $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'systemdate');
		$table->load($load);
		$systemDate = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSender');
		$table->load($load);
		$systemSenderMail = $table->wert;
		$load 		= array('customerID' => 0, 'parameter' => 'emailSenderName');
		$table->load($load);
		$systemSenderName = $table->wert;
		if($login['customerID']):
			$load 		= array('customerID' => $login['customerID'], 'parameter' => 'emailSender');
			$check = $table->load($load);
			if($check && $table->wert != ''):
				$systemSenderMail = $table->wert;
			endif;
			$load 		= array('customerID' => $login['customerID'], 'parameter' => 'emailSenderName');
			$check = $table->load($load);
			if($check && $table->wert != ''):
				$systemSenderName = $table->wert;
			endif;
		endif;
		$return = array(
			'systemName' 	=> $systemName,
			'systemLink' 	=> $systemLink,
			'systemVersion' => $systemVersion,
			'systemDate' 	=> $systemDate,
			'systemSenderMail' 		=> $systemSenderMail,
			'systemSenderName'		=> $systemSenderName
		);
		return $return;
	}
	function getLogindata() {
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$group 		= $session->get('group');
		$return = array(
			'customerID' 	=> $customerID,
			'group' 		=> $group 
		);
		return $return;
	}
	function getClassroomFromTo($classroomID) {
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo();
        $query 	= $db->getQuery(true);
		$query->select(array('
			Min(a.title) as start,
			Max(a.title) as end
		'));
        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
		$query->where('a.classroomID = '.$classroomID);
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}
	function getClassroomHeader($learningroom, $title = false) {
		$cl = $learningroom['classroom']->id;
		if($learningroom['classroom']->visibleTo == 1):
			$visible = '<i style="color: forestgreen;" class="fa fa-users"></i>';
		endif;
		if($learningroom['classroom']->visibleTo == 2):
			$visible = '<i style="color: firebrick;" class="fa fa-key"></i>';
		endif;
		if($learningroom['classroom']->testmode == 1):
			$testmode_message = '<div class="alert alert-danger p-1 mb-1">!TESTMODUS! E-Mail-Versand an: '.$learningroom['classroom']->email_testmode.'</div>';
		endif;
		$html = '';
		$html .= '<div class="card bg-light p-1 mb-2">';
			$html .= '<div id="learningroom-info">';
				$html .= $testmode_message;
				$html .= '<div id="subnavcl" class="row">';
					$html .= '<div class="col-12 col-sm-10 col-lg-11">';
						$html .= '<div>';
							$html .= '<span style="font-size: 32px;">'.$visible.'</span> ';
							$html .= '<span style="font-size: 20px;"><b id="learningroomID">LR'.str_pad($learningroom['classroom']->id,8,'0',STR_PAD_LEFT).'</b></span> ';
							$html .= '<span style="font-size: 32px; font-weight: bold;">'.$learningroom['classroom']->title.'</span>';
						$html .= '</div>';
						$html .= '<p class="m-0"><b>Firma:</b> '.$learningroom['classroom']->company_name.'</p>';
						$html .= '<p class="m-0"><b>Trainer:</b> '.$learningroom['classroom']->main_trainer_name.'</p>';
						$html .= '<h3 style="font-size: 24px;" class="m-0"><i>'.$title.'</i></h3>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 col-lg-1">';
					$html .= '<a href="classroom-edit?layout=global&id='.$cl.'" class="btn btn-warning text-white btn-sm w-100 mb-1">Übersicht</a>';
						$html .= '<a href="classroom-edit?layout=basicdata&id='.$cl.'" class="btn btn-danger text-white btn-sm w-100 mb-1">Basisdaten</a>';
						$html .= '<a href="classroom-edit?layout=students&id='.$cl.'" class="btn btn-info text-white btn-sm w-100 mb-1">Teilnehmer</a>';
						$html .= '<a href="classroom-edit?layout=content&id='.$cl.'" class="btn btn-light border-dark text-dark btn-sm w-100 mb-1">Inhalte</a>';
						$html .= '<a href="classroom-edit?layout=structures&id='.$cl.'" class="btn btn-dark text-white btn-sm w-100">Struktur</a>';
					$html .= '</div>';
				$html .= '</div>';	
			$html .= '</div>';
		$html .= '</div>';

		return $html;
	}
	function getFiles($setCustomerID = false, $part = false, $id = false) {
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$customerID = $setCustomerID ? $setCustomerID : $customerID;
		$db 	= JFactory::getDbo();
		// Load the customer data
        $query 	= $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__jclassroom_files','a'));
		//$query->where('a.customerID = '.$customerID);
		if($part == 'classroom'):
			$query->where('(a.type = "unit" OR a.type = "imageUnit")');
			$query->where('a.classroomID = '. $id);
		endif;
		if($part == 'quizz'):
			$query->where('a.type = "quizz"');
			$query->where('a.unitID = '. $id);
		endif;
		if($part == 'template'):
			$query->where('a.type = "template"');
			$query->where('a.unitID = '. $id);
		endif;
		$db->setQuery($query);
		$files = $db->loadObjectList();
		return $files;
	}
}