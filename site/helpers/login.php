<?php

/**
 *
 *
 */

defined('_JEXEC') or die();

class LoginHelper {
	
  	function checkLogin() {
      $session = JFactory::getSession();
      if(!$session->get('group')):
         JFactory::getApplication()->enqueueMessage('Ihre Sitzung ist abgelaufen. Bitte melden Sie sich an.', 'Message');
         JFactory::getApplication()->redirect(JURI::Root().'abmelden');
      endif;
    }
    function checkTestmode() {
      $session = JFactory::getSession();
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $table   = JTable::getInstance('Configuration','JclassroomTable',array());
      // SAVE TESTMODE
      $load    = array('parameter' => 'testmode','customerID' => 0);
      $check   = $table->load($load);
      $return  = '';
      if($check && $table->wert == 1):
         $session->set('globalTestmode', 1);

         JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
         $TE   = JTable::getInstance('Configuration','JclassroomTable',array());
         // SAVE TESTMODE
         $load    = array('parameter' => 'testmode_email','customerID' => 0);
         $check   = $TE->load($load);
         $session->set('globalTestmodeEmail', $TE->wert);
         $return =  '<div class="alert alert-danger">SYSTEMWEITER TESTMODUS AKTIVIERT. E-Mail: '.$TE->wert.'</div>';
      endif;
      return $return;
    }
    function checkDemo() {
      $user   = JFactory::getUser();
      $db     = JFactory::getDbo();
      $query  = $db->getQuery(true);
      $query->select(array('a.*,b.*,c.title as plan_name'));
      $query->from($db->quoteName('#__jclassroom_customer','a'));
      $query->join('LEFT', $db->quoteName('#__jclassroom_customer_administratoren', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.customerID') . ')');
      $query->join('LEFT', $db->quoteName('#__jclassroom_plans', 'c') . ' ON (' . $db->quoteName('a.planID') . ' = ' . $db->quoteName('c.id') . ')');
      $query->where('b.userID = '.$user->id);;
      $db->setQuery($query);
      $result = $db->loadObject();
		if($result->planID == 4 && $result->plan_to < date('Y-m-d')):
			$app = JFactory::getApplication();
			$app->redirect(JURI::Root().'prices-novalid?deno=0&pid=4');
		endif;
    }
}