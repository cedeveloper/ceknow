<?php

/** ceLearning 29.06.2020
 *	HelperClass TESTS
 *	Used to load all quizzes, which are available for
 *  the currently logged in user. SuperUser has full access on all quizzes.
 *  Trainers has only acces on their own quizzes or on approved quizzes.
 * 	Developer: Torsten Scheel
 */

defined('_JEXEC') or die();

class ResultHelper {
	
  	public function getResult($theResultID) {
      $html = '';
      // Load the Resultdata
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $table   = JTable::getInstance('Theresults','JclassroomTable',array());
      $table->load($theResultID);
      // Load the Quizzdata
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $unit   = JTable::getInstance('Unit','JclassroomTable',array());
      $unit->load($table->quizzID);
      // The Result in details
      $db = JFactory::getDbo();
      $query = $db->getQuery(true);
      $query->select(array('
         a.classroomID,
         a.quizzID,
         a.created as resultCreated,
         a.created_by as resultBy,
         b.id as resultID,
         b.content,
         b.answerID,
         c.title as quizzTitle,
         d.id as questionID,
         d.title as questionTitle,
         d.content as questionContent,
         d.type as questionType,
         d.points,
         d.correctAnswers
      '));
      $query->from($db->quoteName('#__jclassroom_theresults','a'));
      $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
      $query->join('LEFT', $db->quoteName('#__jclassroom_units', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.id') . ')');
      $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'd') . ' ON (' . $db->quoteName('b.questionID') . ' = ' . $db->quoteName('d.id') . ')');
      $query->where($db->quotename('a.id').' = '.$db->quote($theResultID));
      $query->group('d.id');
      $query->order('d.ordering ASC');
      $db->setQuery($query);
      $result = $db->loadObjectList();
      /*echo '<pre>';
      print_r($result);
      echo '</pre>';*/
      $html .= '<table class="table table-striped table-sm" style="font-size: 14px;line-height: 16px;">';
      foreach($result as $item):
         $html .= '<tr>';
         $html .= '<th>Frage</th>';
         $html .= '<th class="text-center">Antworten</th>';
         if($unit->calculate == 2):
            $html .= '<th width="10%" class="text-center">Max. Punkte</th>';
            $html .= '<th width="10%" class="text-center">Err. Punkte</th>';
         endif;
         $html .= '</tr>';
         $html .= '<tr>';
         $content = html_entity_decode($item->questionContent);
         $html .= '<td>'.$item->questionTitle.'<br/><i>'.strip_tags($content).'</i></td>';
         $html .= '<td>';
         $query   = $db->getQuery(true);
         if($item->questionType == 3 ||  $item->questionType == 12 || $item->questionType == 13):
            //echo 'QUESTIONTYPE: '.$item->questionType.'<br/>';
            switch($item->questionType):
               case 13:
                  $query->select(array('a.id,a.title,a.correct'));
                  $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
                  $query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
                  break;
               case 3:
                  $query->select(array('a.id,a.title,a.correct'));
                  $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
                  $query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
                  break;
               case 5:
                  $query->select(array('a.id,a.title,a.correctAnswers as correct'));
                  $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
                  $query->where($db->quotename('a.id').' = '.$db->quote($item->questionID));
                  break;
               case 12:
                  $query->select(array('a.id,a.title,a.correct'));
                  $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
                  $query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
                  break;
            endswitch;
            $db->setQuery($query);
            $answersSoll = $db->loadObjectList();
            //get answers is
            $query = $db->getQuery(true);
            $query->select(array('
               a.*
            '));
            $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
            $query->where($db->quotename('a.theResultID').' = '.$db->quote($theResultID));
            $query->where($db->quotename('a.questionID').' = '.$db->quote($item->questionID));
            $db->setQuery($query);
            $answersIS = $db->loadObjectList();
            if($answersSoll):
               $html .= '<table class="table table-bordered table-sm mb-0">';
               $html .= '<tr>';
               $html .= '<th width="50%">Mögliche Antworten</th>';
               $html .= '<th width="50%">Ihre Antwort</th>';
               $html .= '</tr>';
               $countCAS = 0;
               $countCAI = 0;
               foreach($answersSoll as $answer):
                  $bg = '';
                  $tc = 'black';
                  $correct = 0;
                  if($answer->correct == 1):
                     $correct = 1;
                     $bg = 'forestgreen';
                     $tc = 'white';
                     $countCAS++;
                  endif;
                  $html .= '<tr>';
                  $html .= '<td style="background-color:'.$bg.';color:'.$tc.';">'.$answer->title.'</td>';
                  foreach($answersIS as $answerIS):
                     if($unit->calculate == 2):
                        $bg1 = 'red';
                     else:
                        $bg1 = 'forestgreen';
                     endif;
                     $tc1 = 'white';
                     
                     if(($item->questionType == $answerIS->type) && $answer->id == $answerIS->answerID):
                        if($correct == 1):
                           $bg1 = 'forestgreen';
                           $tc1 = 'white';
                           $countCAI++;
                        endif;
                        $html .= '<td style="background-color:'.$bg1.';color:'.$tc1.';">'.$answer->title.'</td>';
                     endif;
                  endforeach;
                  $html .= '</tr>';
               endforeach;
               $html .= '</table>';
            endif;
         endif;
         if($item->questionType == 5):
            $countCAS = 1;
            $countCAI = 0;
            $bgN  = '';
            $bgJ  = '';
            $bgN1  = '';
            $bgJ1  = '';
            $tJ   = 'black';
            $tN   = 'black';
            if($item->correctAnswers == 1):
               $bgN = 'forestgreen';
               $tN   = 'white';
            else:
               $bgJ = 'forestgreen';
               $tJ   = 'white';
            endif;
            $html .= '<table class="table table-bordered table-sm mb-0">';
            $html .= '<tr>';
            $html .= '<th width="50%">Mögliche Antworten</th>';
            $html .= '<th width="50%">Ihre Antwort</th>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td style="color: '.$tJ.';background-color:'.$bgJ.';">Ja</td>';
            if($item->content == 'answerJ' && $item->correctAnswers == 0):
               $bgJ1 = 'forestgreen';
               $tJ1   = 'white';
               $html .= '<td style="color: '.$tJ1.';background-color: '.$bgJ1.';">';
               $html .= 'Ja';
               $html .= '</td>';
               $countCAI++;
            endif;
            if($item->content == 'answerJ' && $item->correctAnswers == 1):
               $bgJ1 = 'red';
               $tJ1   = 'white';
               $html .= '<td style="color: '.$tJ1.';background-color: '.$bgJ1.';">';
               $html .= 'Ja';
               $html .= '</td>';
            endif;
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td style="color: '.$tN.';background-color:'.$bgN.';">Nein</td>';
            $bgn1 = '';
            $bgJ1 = '';
            if($item->content == 'answerN' && $item->correctAnswers == 0):
               $bgN1 = 'red';
               $tN1   = 'white';
               $html .= '<td style="color: '.$tN1.';background-color: '.$bgN1.';">';
               $html .= 'Nein';
               $html .= '</td>';
            endif;
            if($item->content == 'answerN' && $item->correctAnswers == 1):
               $bgN1 = 'forestgreen';
               $tN1   = 'white';
               $html .= '<td style="color: '.$tN1.';background-color: '.$bgN1.';">';
               $html .= 'Nein';
               $html .= '</td>';
               $countCAI++;
            endif;
            $html .= '</tr>';
            /*$html .= '<tr>';
            $html .= '<td></td>';
            /*if($item->calculate == 2):
               $html .= '<td>'.$item->points.'</td>';
               $html .= '<td>'.$item->questionID.'</td>';
            endif;
            $html .= '</tr>';*/
            $html .= '</table>';
         endif;
         if($item->questionType == 10 || $item->questionType == 11 || $item->questionType == 4):
            $html .= '<table class="table table-bordered table-sm mb-0">';
            $html .= '<tr>';
            $html .= '<th width="50%"></th>';
            $html .= '<th width="50%">Ihre Antwort</th>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td>';
            if($item->questionType == 4):
               $html .= $item->content;
               $html .= '<img width="50" src="images/jclassroom/smiley'.$item->answerID.'.png" />';
            else:
               $html .= $item->content;
            endif;
            $html .= '</td>';
            if($item->calculate == 2):
               $html .= '<td>'.$item->points.'</td>';
               $html .= '<td>'.$item->questionID.'</td>';
            endif;
            $html .= '</tr>';
            $html .= '</table>';
         endif;
         $html .= '</td>';
         if($unit->calculate == 2):
            $html .= '<td class="text-center">'.$item->points.'</td>';
            $sumMax  += $item->points;
            $points  = ($countCAI * $item->points) / $countCAS;
            $sumReal += $points;
            $html .= '<td class="text-center">'.round($points).'</td>';
         endif;
         $html .= '</tr>';
      endforeach;
      if($unit->calculate == 2):
         $html .= '<tr style="font-weight: bold;">';
         $html .= '<td colspan="2">Gesamtauswertung:</td>';
         $html .= '<td class="text-center">'.$sumMax.'</td>';
         $html .= '<td class="text-center">'.round($sumReal).'</td>';
         $html .= '</tr>';
      endif;
      $html .= '</table>';

		return $html;
	}
   function getQuizzOfUser($quizzID, $unitID, $userID) {
      if($quizzID && $userID):
         $db = JFactory::getDbo();
         $query = $db->getQuery(true);
         $query->select(array('
            a.id as theResultID,
            a.created_by,
            a.created as created,
            a.quizzID
         '));
         $query->from($db->quoteName('#__jclassroom_theresults','a'));
         $query->where('a.quizzID   = '.$quizzID);
         $query->where('a.unitID    = '.$unitID);
         $query->where('a.created_by = '.$userID);
         $query->order('a.created DESC');
         $query->setLimit(1);
         $db->setQuery($query);
         $quizz = $db->loadObject();
         $return = '';
         if($quizz):
            $return .= '<td class="text-center">';
            $return .= '<div id="result'.$quizz->theResultID.'">';
            /*$return .= '<i class="fa fa-check text-success"></i> <span style="font-size: 12px;">Abgeschlossen: '.date('d.m.Y H:i:s',strtotime($quizz->created)).'</span><br/>
            <a title="Resultat löschen" onclick="deleteResult('.$result->theResultID.');" class="btn btn-danger btn-sm mr-1 text-white"><i class="fa fa-trash-o text-white"></i></a>
            <a onclick="resultQuizz('.$quizz->theResultID.');" class="btn btn-success btn-sm text-white">Auswertung</a>';*/
            $return .= '<i class="fa fa-check text-success"></i> <span style="font-size: 12px;">Abgeschlossen: '.date('d.m.Y H:i:s',strtotime($quizz->created)).'</span><br/>
            <a onclick="resultQuizz('.$quizz->theResultID.','.$quizzID.');" class="btn btn-success btn-sm text-white" title="Ergebnis anzeigen"><i class="fa fa-pie-chart"></i></a>';
            $return .= '
            <a onclick="deleteResult('.$quizz->theResultID.');" class="btn btn-danger btn-sm text-white" title="Ergebnis löschen"><i class="fa fa-trash-o text-white"></i></a>';
            $return .= '</div>';
            $return .= '</td>';
         else:
            $return = '<td></td>';
         endif;
      endif;
      return $return;
   }
}