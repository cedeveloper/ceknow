<?php

/**
 *	
 *
 */

defined('_JEXEC') or die();

class LoadpersonsHelper {

	function loadSystemadmins() {
		$systemadmins 	= false;
		$session 		= JFactory::getSession();
		if($session->get('group') == 'superuser'):
			// Load Systemadmins
			$db 		= JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('
				a.id,
				a.name'
			)->from("#__users as a");
			$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
			$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
			$query->where('d.title = "Super Users"');
			$query->order('a.name ASC');
			$db->setQuery($query);
			$systemadmins = $db->loadObjectList();
		endif;
		return $systemadmins;
	}
	function loadCustomeradmins() {
		$customeradmins = false;
		$session 	= JFactory::getSession();
		$db 		= JFactory::getDbo();
		// Load Customeradmins
		$query = $db->getQuery(true);
		$query->select('
			a.id, 
			a.name,
			d.title,
			f.email,
			f.company_name,
			e.first_name,
			e.last_name
		')->from("#__users as a");
		$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
		$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
		$query->join('INNER', $db->quoteName('#__jclassroom_customer_administratoren', 'e') . ' ON (' . $db->quoteName('e.userID') . ' = ' . $db->quoteName('a.id') . ')');
		$query->join('INNER', $db->quoteName('#__jclassroom_customer', 'f') . ' ON (' . $db->quoteName('e.customerID') . ' = ' . $db->quoteName('f.id') . ')');
		if($session->get('group') != 'superuser'):
			$query->where('e.customerID = '.$session->get('customerID'));
		endif;
		$query->order('f.company_name ASC,a.name ASC');
		$db->setQuery($query);
		$customeradmins = $db->loadObjectList();
		return $customeradmins;
	}
	function loadTrainer() {
		$trainer 	= false;
		$session 	= JFactory::getSession();
		$db 		= JFactory::getDbo();
		// Load Trainer 
		$query = $db->getQuery(true);
		$query->select("
			a.id, 
			a.name,
			d.title,
			e.first_name,
			e.last_name,
			f.company_name
		")->from("#__users as a");
		$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
		$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
		
		$query->join('INNER', $db->quoteName('#__jclassroom_trainers', 'e') . ' ON (' . $db->quoteName('e.tblUserID') . ' = ' . $db->quoteName('a.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_customer', 'f') . ' ON (' . $db->quoteName('e.customerID') . ' = ' . $db->quoteName('f.id') . ')');
		if($session->get('group') != 'superuser'):
			$query->where('e.customerID = '.$session->get('customerID'));
		endif;
		$query->order('f.company_name ASC,e.last_name ASC');
		$db->setQuery($query);
		$trainer = $db->loadObjectList();
		return $trainer;
	}
	function loadCustomeradmin($userID) {
		$db 		= JFactory::getDbo();
		// Load Data 
		$query = $db->getQuery(true);
		$query->select("
			a.*,
			a.email as customer_email, 
			b.*
		")->from("#__jclassroom_customer as a");
		$query->join('LEFT', $db->quoteName('#__jclassroom_customer_administratoren', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.customerID') . ')');
		$query->where('b.userID = '.$userID);
		$db->setQuery($query);
		$customer = $db->loadObject();

		return $customer;
	}
}