<?php

/**
 *	
 *
 */

defined('_JEXEC') or die();

class SectionsHelper {
	
	public function loadAnswer($questionID, $answerID) {
		$html = '';
		$html .= '<div id="answerTyp3row'.$answerID.'" class="answerTyp3_row row mt-1 mb-1">'; 
			$html .= '<div class="answer3 col-12 col-sm-10">';
				$html .= '<i style="padding-top: 8px;" class="fa fa-trash-o mr-2" onclick="deleteAnswer(' .$answerID. ');"></i>';
				$html .= '<div data-sectionID="'.$questionID.'" onclick="editContent('.$answerID.',&quot;wwmanswer&quot;);" class="wwmanswerHTML">'.$table->answer->title.'</div>';
				$html .= '<textarea style="display: none;" name="wwmanswer['.$answerID.'][]" class="wwmanswer">'.$answer->title.'</textarea>';
				//$html .= '<input data-id="'.$questionID.'" type="text" class="answerTyp3_answer d-inline" style="width: 90%;" value="" placeholder="Bitte geben Sie den Antworttext ein" />';
			$html .= '</div>';
			$html .= '<div class="col-12 col-sm-2 text-center">';
				$html .= '<input type="hidden" class="answerTyp3_id" value="0" />';
				$html .= '<input data-id="'.$questionID.'" id="toggle_' .$answerID. '" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Richtig" data-off="Falsch" class="toggle answerTyp3_correct" />';
			$html .= '</div>';
		$html .= '</div>';
		return $html;
	}
	public function loadOption($optionID,$answerID) {
		$html = '';
		$html .= '<div id="optionrow'.$optionID.'" class="optionrow row mt-1 mb-1">'; 
			$html .= '<div class="option col-12 col-sm-10">';
				$html .= '<i class="fa fa-trash-o mr-2" onclick="deleteOption(' .$optionID. ');"></i>';
				$html .= '<input data-id="'.$answerID.'" type="text" class="option_text d-inline" style="width: 90%;" value="" placeholder="Bitte geben Sie den Optionstext ein" />';
			$html .= '</div>';
			$html .= '<div class="col-12 col-sm-2 text-center">';
				$html .= '<input id="toggle_'.$optionID.'" type="checkbox"  
				class="optionCorrect text-center" data-id="'.$answerID.'" value="0" />';
			$html .= '</div>';
		$html .= '</div>';
		return $html;
	}
	public function loadCheckbox($checkboxID,$answerID) {
		$html = '';
		$html .= '<div id="checkboxrow'.$checkboxID.'" class="checkboxrow row mt-1 mb-1">'; 
			$html .= '<div class="checkbox col-12 col-sm-10">';
				$html .= '<i class="fa fa-trash-o mr-2" onclick="deleteCheckbox(' .$checkboxID. ');"></i>';
				$html .= '<input id="checkbox_text'.$checkboxID.'" type="text" class="checkbox_text d-inline" style="width: 90%;" value="" placeholder="Bitte geben Sie den Text ein" />';
			$html .= '</div>';
			$html .= '<div class="col-12 col-sm-2 text-center">';
				$html .= '<input id="toggle_'.$checkboxID.'" type="checkbox"  
			class="checkboxCorrect text-center" data-id="'.$answerID.'" value="0" />';
			$html .= '</div>';
		$html .= '</div>';
		return $html;
	}
  	public function getTemplate($key, $typ, $structure, $questionID,$inGroup, $inGroupID) {
  		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$table->load($questionID);
		//echo '<pre>';
		//print_r($table->chart);
		//Load Answers
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
	    $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$answers = $db->loadObjectList();
		//Load Options
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
	    $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$options = $db->loadObjectList();
		//Load Checkboxes
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
	    $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$checkboxes = $db->loadObjectList();
		//Load Groupquestions
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
	    $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.groupID').' = '.$db->quote($questionID));
		$query->order('a.ordering ASC');
		$db->setQuery($query);
		$groupQuestions = $db->loadObjectList();
  		$html = '';
  		switch($typ) {
  			case 1:
  				// Freitext
				$html .= '<li id="section'.$questionID. '" class="section card bg bg-light p-1 mt-1 mb-1 w-100 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
				$html .= '<div class="sectionHeader row">';
					$html .= '<div class="col-12 col-sm-3">';
						$html .= '<i class="move fa fa-ellipsis-v"></i>';
						$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
						$html .= '<i onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
						$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
						$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
						$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ');" class="open ml-3 trash fa fa-chevron-right"></i>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-7 text-center">';
						$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 text-right">';
						$html .= '<h5 class="mb-0">Typ: Freitext</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
					$html .= '<div class="col-12">';
						$html .= '<h5 class="mt-3 text-dark">Zusatztext</h5>';
						$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
						$html .= '<textarea style="display: none;" name="content['.$questionID.'][]" class="summernote">'.$table->content.'</textarea>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '</li>';
  				break;
  			case 3:
  				// WWM-Frage 
  				$html .= '<li id="section'.$questionID. '" class="section card bg bg-secondary p-1 mt-1 mb-1 w-100 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
				$html .= '<div class="sectionHeader row">';
					$html .= '<div class="col-12 col-sm-3">';
						$html .= '<i title="Frage verschieben" class="move fa fa-ellipsis-v"></i>';
						$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
						$html .= '<i title="Frage löschen" onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
						$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
						$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
						$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ');" class="open ml-3 trash fa fa-chevron-right"></i>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-7 text-center">';
						$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 text-right">';
						$html .= '<h5 class="mb-0">Typ: WWM</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="text-white">Max. erreichbare Punkte</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<input type="text" id="sectionPoints'.$questionID.'" class="w-300 sectionPoints" value="'.$table->points.'" />';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="row">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="text-white">Auswertung</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<select id="sectionCalculate'.$questionID.'" class="select2-max sectionCalculate">';
								$selected0 		= '';
								$selectedBar 	= '';
								$selectedHBar 	= '';
								$selectedPie 	= '';
								$selectedWord   = '';
								if($table->chart == 0):
									$selected0 = 'selected';
								endif;
								if($table->chart == 'bar'):
									$selectedBar = 'selected';
								endif;
								if($table->chart == 'horizontalBar'):
									$selectedHBar = 'selected';
								endif;
								if($table->chart == 'pie'):
									$selectedPie = 'selected';
								endif;
								if($table->chart == 'wordCloud'):
									$selectedWord = 'selected';
								endif;
								$html .= '<option value="" disabled>Bitte auswählen</option>';
								$html .= '<option '.$selected0.' value="0">Keine Auswertung</option>';
								$html .= '<option '.$selectedBar.' value="bar">Vertikales Balkendiagramm</option>';
								$html .= '<option '.$selectedHBar.' value="horizontalBar">Horizontales Balkendiagramm</option>';
								$html .= '<option '.$selectedPie.' value="pie">Tortendiagramm</option>';
								$html .= '<option '.$selectedWord.' value="wordCloud">Wordcloud</option>';
								$html .= '</select>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="text-white">Richtige Antworten</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<select id="correctAnswers_'.$questionID.'" class="correctAnswers sectionMultiple select2-max">';
									$select1 = '';
									$select2 = '';
									if($table->correctAnswers == 1):
										$select1 = 'selected';
									endif;
									if($table->correctAnswers == 2):
										$select2 = 'selected';
									endif;
									$html .= '<option value="1" '.$select1.'>Eine Antwort</option>';
									$html .= '<option value="2" '.$select2.'>Mehrere Antworten</option>';
								$html .= '</select>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12">';
						$html .= '<h5 class="mt-3 text-white">Zusatztext</h5>';
						$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
						$html .= '<textarea style="display: none;" name="content['.$questionID.'][]" class="summernote">'.$table->content.'</textarea>';
					$html .= '</div>';
					/*$html .= '<div class="row w-100" style="margin-left: 0px;margin-right: 0px;">';
						$html .= '<div class="col-12 col-sm-6">';
							$html .= '<h5 class="mt-3 text-white">Infotext positiv (Wird nach Beantwortung angezeigt)</h5>';
							$html .= '<div onclick="editContent('.$questionID.',&quot;infotextPositiv&quot;);" class="infotextPositivHTML">'.$table->infotextPositiv.'</div>';
							$html .= '<textarea style="display: none;" name="infotextPositiv['.$questionID.'][]" class="infotextPositiv">'.$table->infotextPositiv.'</textarea>';
						$html .= '</div>';
						$html .= '<div class="col-12 col-sm-6">';
							$html .= '<h5 class="mt-3 text-white">Infotext negativ (Wird nach Beantwortung angezeigt)</h5>';
							$html .= '<div onclick="editContent('.$questionID.',&quot;infotextNegativ&quot;);" class="infotextNegativHTML">'.$table->infotextNegativ.'</div>';
							$html .= '<textarea style="display: none;" name="infotextNegativ['.$questionID.'][]" class="infotextNegativ">'.$table->infotextNegativ.'</textarea>';
						$html .= '</div>';
					$html .= '</div>';*/
					$html .= '<div class="col-12">';
						$html .= '<h5 class="mt-3 text-white">Antworten</h5>';
						$html .= '<a onClick="newAnswerTyp3('.$questionID.');" style="width: 200px;" class="d-inline-block btn btn-warning btn-sm">Neue Antwort</a>';
						$html .= '<div class="row">';
							$html .= '<div class="col-12 col-sm-10 text-white">Antwort</div>';
							$html .= '<div class="col-12 col-sm-2 text-center text-white">Richtig</div>';
						$html .= '</div>';
						$html .= '<div id="answersTyp3_body_'.$questionID.'" class="answersTyp3">';
						if($answers):
							foreach($answers as $answer):
								$on = $answer->correct == '1' ? 'checked':'';
								$html .= '<div id="answerTyp3row'.$answer->id.'" class="answerTyp3_row row mt-1 mb-1">'; 
									$html .= '<div class="answer3 col-12 col-sm-10">';
										$html .= '<i title="Antowrt löschen" class="fa fa-trash-o mr-2" style="padding-top: 8px;" onclick="deleteAnswer('.$answer->id.');" style=""></i>';
										$html .= '<div data-sectionID="'.$questionID.'" onclick="editContent('.$answer->id.',&quot;wwmanswer&quot;);" class="wwmanswerHTML">'.$answer->title.'</div>';
										$html .= '<textarea style="display: none;" name="wwmanswer['.$answer->id.'][]" class="wwmanswer">'.$answer->title.'</textarea>';
									$html .= '</div>';
									$html .= '<div class="col-12 col-sm-2 text-center">';
										$html .= '<input data-id="'.$questionID.'" id="toggle_'.$answer->id.'" type="checkbox"  
										class="answerTyp3_correct text-center" '.$on.' value="'.$answer->correct.'" />';
									$html .= '</div>';
								$html .= '</div>';
							endforeach;
						else:
							$html .= '<div id="wwm_noanswers" class="row">';
								$html .= '<div class="col-12 text-white">Keine Antworten gefunden</div>';
							$html .= '</div>';
						endif;
					$html .= '</div>';	
				$html .= '</div>';
				$html .= '</div>';
				$html .= '<input type="hidden" class="answerTyp_type" value="3" />';
				$html .= '<input type="hidden" id="" name="sectionID[]" value="0" />';
				$html .= '<input type="hidden" id="" name="type['.$questionID.'][]" value="3" />';
				$html .= '</li>';

  				break;
  			case 4:
  				// Smileys
  				$html .= '<li id="section'.$questionID. '" style="background-color: indianred;" class="section card p-1 mt-1 mb-1 w-100 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
				$html .= '<div class="sectionHeader row">';
					$html .= '<div class="col-12 col-sm-3">';
						$html .= '<i class="move fa fa-ellipsis-v"></i>';
						$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
						$html .= '<i onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
						$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
						$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
						$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ');" class="open ml-3 trash fa fa-chevron-right"></i>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-7 text-center">';
						$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 text-right">';
						$html .= '<h5 class="mb-0">Typ: Smileys</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="">Max. erreichbare Punkte</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<input type="text" id="sectionPoints'.$questionID.'" class="w-300 sectionPoints" value="'.$table->points.'" />';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="row">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="text-dark">Auswertung</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<select id="sectionCalculate'.$questionID.'" class="select2-max sectionCalculate">';
								$selected0 		= '';
								$selectedBar 	= '';
								$selectedHBar 	= '';
								$selectedPie 	= '';
								$selectedWord   = '';
								if($table->chart == 0):
									$selected0 = 'selected';
								endif;
								if($table->chart == 'bar'):
									$selectedBar = 'selected';
								endif;
								if($table->chart == 'horizontalBar'):
									$selectedHBar = 'selected';
								endif;
								if($table->chart == 'pie'):
									$selectedPie = 'selected';
								endif;
								if($table->chart == 'wordCloud'):
									$selectedWord = 'selected';
								endif;
								$html .= '<option value="" disabled>Bitte auswählen</option>';
								$html .= '<option '.$selected0.' value="0">Keine Auswertung</option>';
								$html .= '<option '.$selectedBar.' value="bar">Vertikales Balkendiagramm</option>';
								$html .= '<option '.$selectedHBar.' value="horizontalBar">Horizontales Balkendiagramm</option>';
								$html .= '<option '.$selectedPie.' value="pie">Tortendiagramm</option>';
								$html .= '<option '.$selectedWord.' value="wordCloud">Wordcloud</option>';
								$html .= '</select>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12">';
						$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
						$html .= '<textarea style="display: none;" class="summernote">'.$table->content.'</textarea>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '</li>';
				break;
  			case 5:
  				$html .= '<li id="section'.$questionID. '" style="background-color: lightsteelblue;" class="section card p-1 mt-1 mb-1 w-100 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
				$html .= '<div class="sectionHeader row">';
					$html .= '<div class="col-12 col-sm-3">';
						$html .= '<i class="move fa fa-ellipsis-v"></i>';
						$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
						$html .= '<i onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
						$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
						$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
						$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ',&quot;content&quot;);" class="open ml-3 trash fa fa-chevron-right"></i>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-7 text-center">';
						$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 text-right">';
						$html .= '<h5 class="mb-0">Typ: Ja/Nein-Frage</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="">Max. erreichbare Punkte</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<input type="text" id="sectionPoints'.$questionID.'" class="w-300 sectionPoints" value="'.$table->points.'" />';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="text-black">Richtige Antwort</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<select id="sectionCorrect'.$questionID.'" class="select2-max sectionCorrect">';
								$selectedYes 	= '';
								$selectedNo 	= '';
								if($table->correctAnswers == 0):
									$selectedYes = 'selected';
								endif;
								if($table->correctAnswers == 1):
									$selectedNo = 'selected';
								endif;
								$html .= '<option value="" disabled>Bitte auswählen</option>';
								$html .= '<option '.$selectedYes.' value="0">Ja</option>';
								$html .= '<option '.$selectedNo.' value="1">Nein</option>';
								$html .= '</select>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="text-black">Auswertung</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<select id="sectionCalculate'.$questionID.'" class="select2-max sectionCalculate">';
								$selected0 		= '';
								$selectedBar 	= '';
								$selectedHBar 	= '';
								$selectedPie 	= '';
								$selectedWord   = '';
								if($table->chart == 0):
									$selected0 = 'selected';
								endif;
								if($table->chart == 'bar'):
									$selectedBar = 'selected';
								endif;
								if($table->chart == 'horizontalBar'):
									$selectedHBar = 'selected';
								endif;
								if($table->chart == 'pie'):
									$selectedPie = 'selected';
								endif;
								if($table->chart == 'wordCloud'):
									$selectedWord = 'selected';
								endif;
								$html .= '<option value="" disabled>Bitte auswählen</option>';
								$html .= '<option '.$selected0.' value="0">Keine Auswertung</option>';
								$html .= '<option '.$selectedBar.' value="bar">Vertikales Balkendiagramm</option>';
								$html .= '<option '.$selectedHBar.' value="horizontalBar">Horizontales Balkendiagramm</option>';
								$html .= '<option '.$selectedPie.' value="pie">Tortendiagramm</option>';
								$html .= '<option '.$selectedWord.' value="wordCloud">Wordcloud</option>';
								$html .= '</select>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12">';
						$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
						$html .= '<textarea id="sectionContent'.$questionID.'" style="display: none;" class="summernote">'.$table->content.'</textarea>';
					$html .= '</div>';
				$html .= '</li>';
				break;
			case 6:
  				$html .= '<li id="section'.$questionID. '" style="background-color: skyblue;" class="section card p-1 mt-1 mb-1 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
				$html .= '<div class="sectionHeader row">';
					$html .= '<div class="col-12 col-sm-3">';
						$html .= '<i class="move fa fa-ellipsis-v"></i>';
						$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
						$html .= '<i onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
						$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
						$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
						$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ');" class="open ml-3 trash fa fa-chevron-right"></i>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-7 text-center">';
						$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 text-right">';
						$html .= '<h5 class="mb-0">Typ: Audit-Frage</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
					$html .= '<div class="col-12 mb-2">';
						$html .= '<b>Detailtext</b>';
						$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
						$html .= '<textarea id="sectionContent'.$questionID.'" style="display: none;" class="summernote">'.$table->content.'</textarea>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="">Max. erreichbare Punkte</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<input type="text" id="sectionPoints'.$questionID.'" class="w-300 sectionPoints" value="'.$table->points.'" />';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-3 col-form-label">';
								$html .= '<label class="">Prüffeld</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-9">';
								$html .= '<input type="text" id="testfield'.$questionID.'" class="w-300 testfield" value="'.$table->testfield.'" />';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-3 col-form-label">';
								$html .= '<label class="">Thema</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-9">';
								$html .= '<input type="text" id="theme'.$questionID.'" class="w-300 theme" value="'.$table->theme.'" />';
							$html .= '</div>';
							
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-3 col-form-label">';
								$html .= '<label class="text-black">Auswertung</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-9">';
								$html .= '<select id="sectionCalculate'.$questionID.'" class="select2-max sectionCalculate">';
								$selected0 		= '';
								$selectedBar 	= '';
								$selectedHBar 	= '';
								$selectedPie 	= '';
								$selectedWord   = '';
								if($table->chart == 0):
									$selected0 = 'selected';
								endif;
								if($table->chart == 'bar'):
									$selectedBar = 'selected';
								endif;
								if($table->chart == 'horizontalBar'):
									$selectedHBar = 'selected';
								endif;
								if($table->chart == 'pie'):
									$selectedPie = 'selected';
								endif;
								if($table->chart == 'wordCloud'):
									$selectedWord = 'selected';
								endif;
								$html .= '<option value="" disabled>Bitte auswählen</option>';
								$html .= '<option '.$selected0.' value="0">Keine Auswertung</option>';
								$html .= '<option '.$selectedBar.' value="bar">Vertikales Balkendiagramm</option>';
								$html .= '<option '.$selectedHBar.' value="horizontalBar">Horizontales Balkendiagramm</option>';
								$html .= '<option '.$selectedPie.' value="pie">Tortendiagramm</option>';
								$html .= '<option '.$selectedWord.' value="wordCloud">Wordcloud</option>';
								$html .= '</select>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-3 col-form-label mb-2">';
								$html .= '<label class="">Positiv-Text</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-9 mb-2">';
								$html .= '<textarea id="infotextPositiv'.$questionID.'" class="w-300 infotextPositiv">'.$table->infotextPositiv.'</textarea>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-3 col-form-label mb-2">';
								$html .= '<label class="">Negativ-Text</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-9 mb-2">';
								$html .= '<textarea id="infotextNegativ'.$questionID.'" class="w-300 infotextNegativ">'.$table->infotextNegativ.'</textarea>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-3 col-form-label mb-2">';
								$html .= '<label class="">KO-Text</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-9 mb-2">';
								$html .= '<textarea id="kotext'.$questionID.'" class="w-300 kotext">'.$table->kotext.'</textarea>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-6 mb-3">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-3 col-form-label mb-2">';
								$html .= '<label class="">ToDo-Text</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-9 mb-2">';
								$html .= '<textarea id="todo'.$questionID.'" class="w-300 todo">'.$table->todo.'</textarea>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-3 col-form-label">';
								$html .= '<label class="">Info-Text</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-9">';
								$html .= '<textarea id="infotext'.$questionID.'" class="w-300 infotext">'.$table->infotext.'</textarea>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '</li>';
				break;
  			case 7:
  				$html .= '<li id="section'.$questionID. '" style="background-color: mediumvioletred;" class="section card p-1 mt-1 mb-1 w-100 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
				$html .= '<div class="sectionHeader row">';
					$html .= '<div class="col-12 col-sm-3">';
						$html .= '<i class="move fa fa-ellipsis-v"></i>';
						$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
						$html .= '<i onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
						$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
						$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
						$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ',&quot;content&quot;);" class="open ml-3 trash fa fa-chevron-right"></i>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-7 text-center">';
						$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 text-right">';
						$html .= '<h5 class="mb-0">Typ: Text mit 1 Eingabe</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="">Max. erreichbare Punkte</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<input type="text" id="sectionPoints'.$questionID.'" class="w-300 sectionPoints" value="'.$table->points.'" />';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="text-black">Richtige Antwort</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<input type="text" id="sectionCorrectText'.$questionID.'" class="w-300 sectionCorrectText" value="'.$answers[0]->title.'" />';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="text-black">Auswertung</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<select id="sectionCalculate'.$questionID.'" class="select2-max sectionCalculate">';
								$selected0 		= '';
								$selectedBar 	= '';
								$selectedHBar 	= '';
								$selectedPie 	= '';
								$selectedWord   = '';
								if($table->chart == 0):
									$selected0 = 'selected';
								endif;
								if($table->chart == 'bar'):
									$selectedBar = 'selected';
								endif;
								if($table->chart == 'horizontalBar'):
									$selectedHBar = 'selected';
								endif;
								if($table->chart == 'pie'):
									$selectedPie = 'selected';
								endif;
								if($table->chart == 'wordCloud'):
									$selectedWord = 'selected';
								endif;
								$html .= '<option value="" disabled>Bitte auswählen</option>';
								$html .= '<option '.$selected0.' value="0">Keine Auswertung</option>';
								$html .= '<option '.$selectedBar.' value="bar">Vertikales Balkendiagramm</option>';
								$html .= '<option '.$selectedHBar.' value="horizontalBar">Horizontales Balkendiagramm</option>';
								$html .= '<option '.$selectedPie.' value="pie">Tortendiagramm</option>';
								$html .= '<option '.$selectedWord.' value="wordCloud">Wordcloud</option>';
								$html .= '</select>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12">';
						$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
						$html .= '<textarea id="sectionContent'.$questionID.'" style="display: none;" class="summernote">'.$table->content.'</textarea>';
					$html .= '</div>';
				$html .= '</li>';
				break;
  			case 10:
  				$html .= '<li id="section'.$questionID. '" style="background-color: magenta;" class="section card p-1 mt-1 mb-1 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
				$html .= '<div class="sectionHeader row">';
					$html .= '<div class="col-12 col-sm-3">';
						$html .= '<i class="move fa fa-ellipsis-v"></i>';
						$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
						$html .= '<i onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
						$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
						$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
						$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ',&quot;content&quot;);" class="open ml-3 trash fa fa-chevron-right"></i>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-7 text-center">';
						$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 text-right">';
						$html .= '<h5 class="mb-0">Typ: Textfeld</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="">Max. erreichbare Punkte</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<input type="text" id="sectionPoints'.$questionID.'" class="w-300 sectionPoints" value="'.$table->points.'" />';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12">';
						$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
						$html .= '<textarea style="display: none;" class="summernote">'.$table->content.'</textarea>';
					$html .= '</div>';
					$html .= '</div>';
				$html .= '</li>';
				break;
			case 11:
  				$html .= '<li id="section'.$questionID. '" style="background-color: papayawhip;" class="section card p-1 mt-1 mb-1 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
				$html .= '<div class="sectionHeader row">';
					$html .= '<div class="col-12 col-sm-3">';
						$html .= '<i class="move fa fa-ellipsis-v"></i>';
						$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
						$html .= '<i onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
						$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
						$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
						$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ',&quot;content&quot;);" class="open ml-3 trash fa fa-chevron-right"></i>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-7 text-center">';
						$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 text-right">';
						$html .= '<h5 class="mb-0">Typ: Textarea</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="">Max. erreichbare Punkte</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<input type="text" id="sectionPoints'.$questionID.'" class="w-300 sectionPoints" value="'.$table->points.'" />';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12">';
						$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
						$html .= '<textarea style="display: none;" class="summernote">'.$table->content.'</textarea>';
					$html .= '</div>';
					$html .= '</div>';
				$html .= '</li>';
				break;
			case 12:
  				$html .= '<li id="section'.$questionID. '" style="background-color: darkseagreen;" class="section card p-1 mt-1 mb-1 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
				$html .= '<div class="sectionHeader row">';
					$html .= '<div class="col-12 col-sm-3">';
						$html .= '<i class="move fa fa-ellipsis-v"></i>';
						$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
						$html .= '<i onclick="deleteQuestion('.$questionID.',&quot;content&quot;);" class="fa fa-trash-o"></i>';
						$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
						$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
						$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ');" class="open ml-3 trash fa fa-chevron-right"></i>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-7 text-center">';
						$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-2 text-right">';
						$html .= '<h5 class="mb-0">Typ: Auswahlliste</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="">Max. erreichbare Punkte</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<input type="text" id="sectionPoints'.$questionID.'" class="w-300 sectionPoints" value="'.$table->points.'" />';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label>Richtige Antworten</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<select id="correctAnswers_'.$questionID.'" class="correctAnswers sectionMultiple select2-max">';
									$select1 = '';
									$select2 = '';
									if($table->correctAnswers == 1):
										$select1 = 'selected';
									endif;
									if($table->correctAnswers == 2):
										$select2 = 'selected';
									endif;
									$html .= '<option value="1" '.$select1.'>Eine Antwort</option>';
									$html .= '<option value="2" '.$select2.'>Mehrere Antworten</option>';
								$html .= '</select>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12 col-sm-6">';
						$html .= '<div class="form-group row mb-0">';
							$html .= '<div class="col-12 col-sm-4 col-form-label">';
								$html .= '<label class="">Auswertung</label>';
							$html .= '</div>';
							$html .= '<div class="col-12 col-sm-8">';
								$html .= '<select id="sectionCalculate'.$questionID.'" class="select2-max sectionCalculate">';
									$selected0 		= '';
									$selectedBar 	= '';
									$selectedHBar 	= '';
									$selectedPie 	= '';
									$selectedWord   = '';
									if($table->chart == 0):
										$selected0 = 'selected';
									endif;
									if($table->chart == 'bar'):
										$selectedBar = 'selected';
									endif;
									if($table->chart == 'horizontalBar'):
										$selectedHBar = 'selected';
									endif;
									if($table->chart == 'pie'):
										$selectedPie = 'selected';
									endif;
									if($table->chart == 'wordCloud'):
										$selectedWord = 'selected';
									endif;
									$html .= '<option value="" disabled>Bitte auswählen</option>';
									$html .= '<option '.$selected0.' value="0">Keine Auswertung</option>';
									$html .= '<option '.$selectedBar.' value="bar">Vertikales Balkendiagramm</option>';
									$html .= '<option '.$selectedHBar.' value="horizontalBar">Horizontales Balkendiagramm</option>';
									$html .= '<option '.$selectedPie.' value="pie">Tortendiagramm</option>';
									$html .= '<option '.$selectedWord.' value="wordCloud">Wordcloud</option>';
									$html .= '</select>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div class="col-12">';
						$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
						$html .= '<textarea style="display: none;" class="summernote">'.$table->content.'</textarea>';
					$html .= '</div>';
					$html .= '<div class="col-12">';
						$html .= '<h5 class="mt-3 text-white">Optionen</h5>';
						$html .= '<a onClick="newOption('.$questionID.');" style="width: 200px;" class="d-inline-block btn btn-warning btn-sm">Neue Option</a>';
						$html .= '<div class="row">';
							$html .= '<div class="col-12 col-sm-10 text-white">Option</div>';
							$html .= '<div class="col-12 col-sm-2 text-center text-white">Richtig</div>';
						$html .= '</div>';
						$html .= '<div id="option_body_'.$questionID.'" class="options">';
						if($options):
							foreach($options as $option):
								$on = $option->correct == '1' ? 'checked':'';
								$html .= '<div id="optionrow'.$option->id.'" class="optionrow row mt-1 mb-1">'; 
									$html .= '<div class="option col-12 col-sm-10">';
										$html .= '<i class="fa fa-trash-o mr-2" onclick="deleteOption('.$option->id.');"></i>';
										$html .= '<input id="option_text'.$questionID.'" type="text" style="width: 90%;" class="d-inline option_text" 
										value="'.$option->title.'" placeholder="Bitte geben Sie den Optionstext ein" />';
									$html .= '</div>';
									$html .= '<div class="col-12 col-sm-2 text-center">';
										$html .= '<input id="toggle_'.$option->id.'" type="checkbox"  
										class="optionCorrect text-center" '.$on.' data-id="'.$questionID.'"  value="'.$option->correct.'" />';
									$html .= '</div>';
								$html .= '</div>';
							endforeach;
						endif;
					$html .= '</div>';	
				$html .= '</div>';
				$html .= '</li>';
				break;
			case 13:
				$html .= '<li id="section'.$questionID. '" style="background-color: burlywood;" class="section card p-1 mt-1 mb-1 '.$inGroup.'" data-groupid="'.$inGroupID.'">';
					$html .= '<div class="sectionHeader row">';
						$html .= '<div class="col-12 col-sm-3">';
							$html .= '<i class="move fa fa-ellipsis-v"></i>';
							$html .= '<i title="Frage speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
							$html .= '<i title="Frage löschen" onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
							$html .= '<i title="Frage kopieren" onclick="copyQuestion('.$questionID.');" class="fa fa-files-o"></i>';
							$html .= '<i title="Frage in eine Gruppe verschieben" onclick="moveQuestion('.$questionID.');" class="ml-3 fa fa-arrows-alt"></i>';
							$html .= '<i title="Frage auf- / zuklappen" onclick="openQuestion(' .$questionID. ',&quot;content&quot;);" class="open ml-3 trash fa fa-chevron-right"></i>';
						$html .= '</div>';
						$html .= '<div class="col-12 col-sm-7 text-center">';
							$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
						$html .= '</div>';
						$html .= '<div class="col-12 col-sm-2 text-right">';
							$html .= '<h5 class="mb-0">Typ: Checkboxen</h5><small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody row mt-2" style="display: none;">';
						$html .= '<div class="col-12 col-sm-6">';
							$html .= '<div class="form-group row mb-0">';
								$html .= '<div class="col-12 col-sm-4 col-form-label">';
									$html .= '<label class="">Max. erreichbare Punkte</label>';
								$html .= '</div>';
								$html .= '<div class="col-12 col-sm-8">';
									$html .= '<input type="text" id="sectionPoints'.$questionID.'" class="w-300 sectionPoints" value="'.$table->points.'" />';
								$html .= '</div>';
								$html .= '<div class="col-12 col-sm-4 col-form-label">';
									$html .= '<label>Richtige Antworten</label>';
								$html .= '</div>';
								$html .= '<div class="col-12 col-sm-8">';
									$html .= '<select id="correctAnswers_'.$questionID.'" class="correctAnswers sectionMultiple select2-max">';
										$select1 = '';
										$select2 = '';
										if($table->correctAnswers == 1):
											$select1 = 'selected';
										endif;
										if($table->correctAnswers == 2):
											$select2 = 'selected';
										endif;
										$html .= '<option value="1" '.$select1.'>Eine Antwort</option>';
										$html .= '<option value="2" '.$select2.'>Mehrere Antworten</option>';
									$html .= '</select>';
								$html .= '</div>';
							$html .= '</div>';
						$html .= '</div>';
						$html .= '<div class="col-12 col-sm-6">';
							$html .= '<div class="form-group row mb-0">';
								$html .= '<div class="col-12 col-sm-4 col-form-label">';
									$html .= '<label class="">Auswertung</label>';
								$html .= '</div>';
								$html .= '<div class="col-12 col-sm-8">';
									$html .= '<select id="sectionCalculate'.$questionID.'" class="select2-max sectionCalculate">';
									$selected0 		= '';
									$selectedBar 	= '';
									$selectedHBar 	= '';
									$selectedPie 	= '';
									$selectedWord   = '';
									if($table->chart == 0):
										$selected0 = 'selected';
									endif;
									if($table->chart == 'bar'):
										$selectedBar = 'selected';
									endif;
									if($table->chart == 'horizontalBar'):
										$selectedHBar = 'selected';
									endif;
									if($table->chart == 'pie'):
										$selectedPie = 'selected';
									endif;
									if($table->chart == 'wordCloud'):
										$selectedWord = 'selected';
									endif;
									$html .= '<option value="" disabled>Bitte auswählen</option>';
									$html .= '<option '.$selected0.' value="0">Keine Auswertung</option>';
									$html .= '<option '.$selectedBar.' value="bar">Vertikales Balkendiagramm</option>';
									$html .= '<option '.$selectedHBar.' value="horizontalBar">Horizontales Balkendiagramm</option>';
									$html .= '<option '.$selectedPie.' value="pie">Tortendiagramm</option>';
									$html .= '<option '.$selectedWord.' value="wordCloud">Wordcloud</option>';
									$html .= '</select>';
								$html .= '</div>';
							$html .= '</div>';
						$html .= '</div>';
						$html .= '<div class="col-12">';
							$html .= '<div onclick="editContent('.$questionID.',&quot;content&quot;);" class="contentHTML">'.html_entity_decode($table->content).'</div>';
							$html .= '<textarea style="display: none;" class="summernote">'.$table->content.'</textarea>';
						$html .= '</div>';
						$html .= '<div class="col-12">';
							$html .= '<h5 class="mt-3">Checkboxen</h5>';
							$html .= '<a onClick="newCheckbox('.$questionID.');" style="width: 200px;" class="d-inline-block btn btn-warning btn-sm">Neue Checkbox</a>';
							$html .= '<div class="row">';
								$html .= '<div class="col-12 col-sm-10">Checkbox</div>';
								$html .= '<div class="col-12 col-sm-2 text-center text-white">Richtig</div>';
							$html .= '</div>';
							$html .= '<div id="checkbox_body_'.$questionID.'" class="checkboxes">';
							if($checkboxes):
								foreach($checkboxes as $checkbox):
									$on = $checkbox->correct == '1' ? 'checked':'';
									$html .= '<div id="checkboxrow'.$checkbox->id.'" class="checkboxrow row mt-1 mb-1">'; 
										$html .= '<div class="checkbox col-12 col-sm-10">';
											$html .= '<i class="fa fa-trash-o mr-2" onclick="deleteCheckbox('.$checkbox->id.');"></i>';
											$html .= '<input id="checkbox_text'.$questionID.'" type="text" style="width: 90%;" class="d-inline checkbox_text" 
											value="'.$checkbox->title.'" placeholder="Bitte geben Sie den Text ein" />';
										$html .= '</div>';
										$html .= '<div class="col-12 col-sm-2 text-center">';
											$html .= '<input id="toggle_'.$checkbox->id.'" type="checkbox"  
											class="checkboxCorrect text-center" '.$on.' data-id="'.$questionID.'" value="'.$checkbox->correct.'" />';
										$html .= '</div>';
									$html .= '</div>';
								endforeach;
							endif;
						$html .= '</div>';	
					$html .= '</div>';
				$html .= '</li>';
				break;
			case 14:
				$html .= '<li id="section'.$questionID. '" class="section group card p-1 mt-1 mb-1" style="background-color: #c1c1c1;">';
					$html .= '<div class="groupHeader row">';
						$html .= '<div class="col-12 col-sm-3">';
							$html .= '<i title="Gruppe verschieben" class="move fa fa-ellipsis-v"></i>';
							$html .= '<i title="Gruppe speichern" onclick="saveQuestion(' .$questionID. ');" class="text-success ml-3 save fa fa-floppy-o"></i>';
							$html .= '<i title="Gruppe löschen" onclick="deleteQuestion('.$questionID.');" class="fa fa-trash-o"></i>';
							$html .= '<i title="Gruppe auf- / zuklappen" onclick="openQuestion(' .$questionID. ');" class="open ml-3 trash fa fa-chevron-right"></i>';
						$html .= '</div>';
						$html .= '<div class="col-12 col-sm-7 text-center">';
							$html .= '<input type="text" id="sectionTitle'.$questionID.'" class="sectionTitle" placeholder="Individueller Titel" value="'.$table->title.'" />';
						$html .= '</div>';
						$html .= '<div class="col-12 col-sm-2 text-right">';
							$html .= '<a onclick="newQuestionToPart('.$questionID.');" id="newQuestionToPart" data-id="'.$questionID.'" class="newQuestionToPart btn btn-success btn-sm text-white mb-1" style="padding: 0px 10px;">Frage einfügen</a>';
							$html .= '<small style="margin-top: -5px;" class="d-block">'.$questionID.'</small>';
						$html .= '</div>';
					$html .= '</div>';
					
					$html .= '<div id="sectionBody'.$questionID.'" class="sectionBody groupBody w-100" style="display: none;">';
					$html .= '<div class="w-100">';
						$html .= '<div class="col-12 mb-2 w-100">';
							$html .= '<h5 class="mt-3 text-dark">Zusatztext</h5>';
							$html .= '<div onclick="editContent('.$questionID.',&quot;zusatz&quot;);" class="zusatz contentHTML">'.html_entity_decode($table->content).'</div>';
							$html .= '<textarea style="display: none;" name="zusatz['.$questionID.'][]" class="zusatz summernote">'.$table->content.'</textarea>';
						$html .= '</div>';
						$html .= '<div class="col-12 mb-2 w-100">';
							$html .= '<ul class="w-100 p-0">';
							if($groupQuestions):
								foreach($groupQuestions as $groupQuestion):
									JLoader::register('SectionsHelper',JPATH_COMPONENT_SITE.'/helpers/sections.php');
									$template = new SectionsHelper();
									$Shtml = $template->getTemplate($groupQuestion->id, $groupQuestion->type, $groupQuestion,$groupQuestion->id,'inGroup',$questionID);
									$html .= $Shtml;
								endforeach;
							endif;
							$html .= '</ul>';
						$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</li>';
				break;
  		}
		return $html;
	}
}