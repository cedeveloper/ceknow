<?php

/**
 *	
 *
 */

defined('_JEXEC') or die();

class AddSomethingHelper {
	
  	public function addModule($saveNew, $classroomID, $dayID) {
  		$user = JFactory::getUser();
  		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$data = array();
		$data['classroomID'] 	= $classroomID;
		$data['dayID'] 			= $dayID;
		$data['title'] 			= '';
		$data['description'] 	= '';
		$data['created']		= date('Y-m-d H:i:s');
		$data['created_by']		= $user->id;
		$data['published']		= 1;
		$table->bind($data);
		$table->store();
		$moduleID = $table->id;
		return $moduleID;
	}
	public function addModuleFromLibrary($saveNew, $classroomID, $dayID, $libraryModuleID) {
		$user = JFactory::getUser();
		// Load the module from library
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$library = JTable::getInstance('Classroom_modules_library','JclassroomTable',array());
		$library->load($libraryModuleID);
		// Write copy to database
  		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Classroom_modules','JclassroomTable',array());
		$data = array();
		$data['classroomID'] 	= $classroomID;
		$data['dayID'] 			= $dayID;
		$data['title'] 			= $library->title;
		$data['description'] 	= $library->description;
		$data['created']		= date('Y-m-d H:i:s');
		$data['created_by']		= $user->id;
		$data['published']		= 1;
		$table->bind($data);
		$table->store();
		$moduleID = $table->id;
		return $moduleID;
	}
	public function addUnitsForModuleFromLibrary($classroomID, $dayID, $libraryModuleID, $newModuleID) {
		$session 	= JFactory::getSession(); 
		$user 	= JFactory::getUser();
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select('a.*');
        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
        $query->where($db->quotename('a.moduleID').' = '.$db->quote($libraryModuleID));
        $query->order('a.ordering asc');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		$unitsTemplate = '';
		if($units):
			foreach($units as $unit):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table = JTable::getInstance('Classroom_days_units','JclassroomTable',array());
				$data = array();
				$data['classroomID']= $classroomID;
				$data['dayID'] 		= $dayID;
				$data['moduleID'] 	= $newModuleID;
				$data['unitID'] 	= $unit->unitType;
				$data['unitType'] 	= $unit->unitType;
				$data['title'] 		= $unit->title;
				$data['description']= $unit->description;
				$data['quizzID']	= $unit->quizzID;
				$data['content'] 	= $unit->content;
				$data['ordering'] 	= $unit->ordering;
				$data['duration'] 	= $unit->duration;
				$data['link'] 		= $unit->link;
				$data['created'] 	= date('Y-m-d H:i:s');
				$data['created_by'] = $user->id;
				$data['published']	= 1;
				$table->bind($data);
				$table->store();
				$newUnitID = $table->id;
				// Save the images from library to files
				$pathFiles = '/images/learningrooms/LR'.$classroomID.'/units';
				JFolder::create($pathLibrary);
				// Copy the files
				$db 	= JFactory::getDbo(); 
				$query 	= $db->getQuery(true);
				$query->select('a.*');
		        $query->from($db->quoteName('#__jclassroom_files','a'));
		        $query->where($db->quotename('a.unitID').' = '.$db->quote($unit->id));
		        $query->order('a.id ASC');
				$db->setQuery($query);
				$files = $db->loadObjectList();
				if($files):
					foreach($files as $file):
						$file_library = JTable::getInstance('File','JclassroomTable',array());
						$data 			= array();
						$data['customerID'] = $session->get('customerID');
						$data['type'] 		= $file->type;
						$data['classroomID']= $classroomID;
						$data['unitID'] 	= $newUnitID;
						$data['studentID'] 		= 0;
						$data['folder'] 		= '';
						$data['filename'] 		= $file->filename;
						$data['path'] 			= $pathFiles.'/'.$file->filename;
						$data['ordering'] 		= 0;
						$data['created'] 		= date('Y-m-d H:i:s');
						$data['created_by'] 	= $user->id;
						$data['published'] 		= 1;
						$file_library->bind($data);
						$file_library->store();
						copy(JPATH_SITE.$file->path,JPATH_SITE.$pathFiles.'/'.$file->filename);
					endforeach;
				endif;
				// Now get the template for the new unit
				JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
		    	$template = new UnitsHelper();
		    	$unitsTemplate .= $template->getTemplate('structure', $classroomID, $dayID, $unit->unitType,$table->id);
			endforeach;
		endif;
		// At least return the complete template for insertig into the module
		return $unitsTemplate;
	}
}