<?php

/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class JclassroomModelLibrary_days extends JModelList {
	
	public function __construct($config = array()) {
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'a.id','id',
				'a.title', 'title',
				'a.published', 'published'
			);
		}
		parent::__construct($config);
	}
	
	protected function populateState($ordering = 'id', $direction = 'ASC') {
		// Get the Application
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		
		// Set filter state for search
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
	
		// Set filter state for bereich
		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		// Set filter state for bereich
		$ort = $this->getUserStateFromRequest($this->context.'.filter.ort', 'filter_ort', '');
		$this->setState('filter.ort', $ort);
		
		// Load the parameters.
		$params = JComponentHelper::getParams('com_inclure');
		$active = $menu->getActive();
		empty($active) ? null : $params->merge($active->params);
		$this->setState('params', $params);

		// List state information.
		parent::populateState($ordering, $direction);
	}

	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.category_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery() {
		$session 	= JFactory::getSession();
		// Get database object
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('a.*')->from('#__jclassroom_classroom_days_library AS a');
		$query->select(array('b.name AS creator'))
			->join('LEFT', '#__users AS b ON b.id = a.created_by');
		//$query->where('a.created_by = ' . $db->quote($db->escape($user->id)));
		if($session->get('customerID')):
			$query->where('a.customerID = ' . $db->quote($session->get('customerID')));
		endif;
		// Filter by search
		$search = $this->getState('filter.search');
		
		if (!empty($search))
		{	
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where('a.title LIKE ' . $search.' OR a.description LIKE '.$search);
		}	
		// SET FILTER
		$published = $this->getState('filter.published');
		if ($published != "")
		{
			$query->where('a.published = ' . $db->quote($db->escape($published)));
		}
		
		// Add list oredring and list direction to SQL query
		$sort = $this->getState('list.ordering', 'id');
		$order = $this->getState('list.direction', 'ASC');
		$query->order($db->escape($sort).' '.$db->escape($order));
		return $query;
	}
	public function getAuthors()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text')
			->from('#__users AS u')
			->join('INNER', '#__inclure_druckgruppen AS a ON a.created_by = u.id')
			->group('u.id, u.name')
			->order('u.name');

		// Setup the query
		$db->setQuery($query);

		// Return the result
		return $db->loadObjectList();
	}
	function getModules($dayID) {
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
        $query->where($db->quotename('a.dayID').' = '.$db->quote($dayID));
		$db->setQuery($query);
		$db->execute();
		$numRows 	= $db->getNumRows();
		return $numRows;
	}
	function getItemsOfModules($dayID) {
		$db 	= JFactory::getDbo(); 
		$query 	= $db->getQuery(true);
		$query->select(array('Count(b.id) as cUnits'));
        $query->from($db->quoteName('#__jclassroom_classroom_modules_library','a'));
        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_days_units_library', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.moduleID') . ')');
        $query->where($db->quotename('a.dayID').' = '.$db->quote($dayID));
		$db->setQuery($query);
		$units = $db->loadResult();

		return $units;
	}
	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   12.2
	 */
	public function getItems()
	{
		if ($items = parent::getItems()) {
			//Do any procesing on fields here if needed
			foreach($items as $item):
				$modules 	= $this->getModules($item->id);
				$item->modules 	= $modules;
				$itemsOM 		= $this->getItemsOfModules($item->id);
				$item->units 	= $itemsOM;
			endforeach;
		}

		return $items;
	}

}