<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * List Model for fragenkataloge.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelUnits extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'a.id', 'id',
				'a.title','title',
				'a.published', 'published',
				'a.created', 'created',
				'a.created_by', 'created_by', 'author_id',
				'a.hits', 'hits','state'
			);
		}
		parent::__construct($config);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 */
	protected function populateState($ordering = 'title', $direction = 'ASC')
	{
		// Get the Application
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		// Set filter state for search
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
		// Set filter state for fromDate
		$fromDate = $app->getUserStateFromRequest($this->context . '.filter.fromdate', 'filter_fromdate','','string');
		$this->setState('filter.fromdate', $fromDate);
		$toDate = $app->getUserStateFromRequest($this->context . '.filter.todate', 'filter_todate','','string');
		$this->setState('filter.todate', $toDate);
		// Set filter state for publish state
        $published = $app->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '', 'string');
        $this->setState('filter.published', $published);
        $customerID = $app->getUserStateFromRequest($this->context . '.filter.customerID', 'filter_customerID', '', 'integer');
        $this->setState('filter.published', $published);		
		// Load the parameters.
		$params = JComponentHelper::getParams('com_auditum_ad');
		$active = $menu->getActive();
		empty($active) ? null : $params->merge($active->params);
		$this->setState('params', $params);
		// List state information.
		parent::populateState($ordering, $direction);
	}
	
	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.published');
		$id .= ':' . $this->getState('filter.author_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery()
	{
		$user 	= JFactory::getUser();
		$session 	= JFactory::getSession();
		// Get database object
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('a.*')->from('#__jclassroom_units AS a');
		$query->select('ua.name AS author_name')
			->join('LEFT', '#__users AS ua ON ua.id = a.created_by');
		$query->select('cu.company_name AS company_name')
			->join('LEFT', '#__jclassroom_customer AS cu ON cu.id = a.customerID');

        if($session->get('group') == 'superuser') {
        	$customerID = $this->getState('filter.customerID');
        	if($customerID):
        		$query->where('a.customerID = ' . $db->quote($customerID));
        	endif;
        }
        if($session->get('group') == 'customer' || $session->get('group') == 'trainer') {
            $query->where('a.customerID = ' . $db->quote($session->get('customerID')));
		}
		//if($session->get('group') == 'trainer') {
            //$query->where('a.showto <> 1');
		//}
		// Filter by search
		$search = $this->getState('filter.search');
		$s = $db->quote('%'.$db->escape($search, true).'%');
		if (!empty($search)) {
			$query->where('(a.title LIKE ' . $s . ' OR a.description LIKE ' . $s . ' OR a.id LIKE '. $s.')');
		}
		// Filter by published state.
		$published = $this->getState('filter.published');
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			// Only show items with state 'published' / 'unpublished'
			$query->where('(a.published IN (0, 1))');
		}

		$fromDate = $this->getState('filter.fromdate');
		if($fromDate):
			$query->where('a.created >= "' .date('Y-m-d', strtotime($fromDate)).'"');
		endif;
		$toDate = $this->getState('filter.todate');
		if($toDate):
			$query->where('a.created <= "' .date('Y-m-d', strtotime($toDate)).'"');
		endif;


		
		// Add list oredring and list direction to SQL query
		$sort = $this->getState('list.ordering', 'title');
		$order = $this->getState('list.direction', 'ASC');
		$query->order($db->escape($sort).' '.$db->escape($order));
		
		return $query;
	}
	
	/**
	 * Build a list of authors
	 *
	 * @return  array
	 *
	 * @since   1.6
	 */
	public function getAuthors()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text')
			->from('#__users AS u')
			->join('INNER', '#__audit_fragenkataloge AS a ON a.created_by = u.id')
			->group('u.id, u.name')
			->order('u.name');

		// Setup the query
		$db->setQuery($query);

		// Return the result
		return $db->loadObjectList();
	}
	
	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   12.2
	 */
	public function getItems()
	{
		if ($items = parent::getItems()) {
			$newItems = array();
			foreach($items as $item) {
				if($item->showto == 1):
					$newItems[] = $item;
				endif;
			}
		}
		return $items;
	}
	/*function getResult($resultID) {
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.*, b.*'));
        $query->from($db->quoteName('#__audit_results','a'));
        $query->join('LEFT', $db->quoteName('#__audit_resultelements', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.resultID') . ')');
		$query->where('a.id = '.$item->id);
		//$query->order('b.position asc');
		$db->setQuery($query);
		$result = $db->loadObjectList();
	}*/
}