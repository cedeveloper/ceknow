<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for fragenkatalog.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelStage_quizz extends JModelList
{
	function getUnit() {
		$session  	= JFactory::getSession();

		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('unitID', 0, 'INT');
		$lid 		= $input->get('lid', 0, 'INT');
		$q 			= $input->get('q', 0, 'INT');
		if(!$q):
			$q = 0;
		endif;
		if($q == 0):
			$session->clear('quizzes');
		endif;
		$return 	= false;
		if($lid != 9) {
			$option = array(); 
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_units','a'));
			$query->where($db->quoteName('a.id').' = '.$db->quote($unitID));
			$db->setQuery($query);
			$result 	= $db->loadObject();
			//Count Quizzpositions
			$query = $db->getQuery(true);
			$query->select(array('count(a.id)'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->id));
			$db->setQuery($query);
			$countQuestions 	= $db->loadResult();
			//Get Quizzpositions
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->id));
			$query->where($db->quoteName('a.ordering').' = '.$db->quote($q));
			$db->setQuery($query);
			$theQuestions 	= $db->loadObject();
			//Get Answers
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theAnswers 	= $db->loadObjectList();
			//Get Options
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theOptions 	= $db->loadObjectList();
			//Get Checkboxes
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theCheckboxes 	= $db->loadObjectList();
			//Get Answers correct
			$query = $db->getQuery(true);
			$query->select(array('count(a.id)'));
	        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->where($db->quoteName('a.correct').' = 1');
			$db->setQuery($query);
			$theAnswersCorrect 	= $db->loadResult();
			$return = array('quizzID' => $result->id, 
				'title' 		=> $result->title, 
				'description' 	=> $result->description,
				'question' 		=> $theQuestions->title,
				'questionID' 	=> $theQuestions->id,
				'type' 			=> $theQuestions->type,
				'content' 		=> $theQuestions->content,
				'answers' 		=> $theAnswers,
				'options' 		=> $theOptions,
				'checkboxes' 	=> $theCheckboxes,
				'correctAnswers'=> $theQuestions->correctAnswers,
				'calculate'		=> $result->calculate,
				'points'		=> $theQuestions->points,
				'countSections' => $countQuestions
			);	
		}
		return $return;
	}
	function getUnit_sections() {
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('id', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db = JDatabaseDriver::getInstance( $option );
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__audit_unit_sections','a'));
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
		$db->setQuery($query);
		$result = $db->loadObjectList();

		foreach($result as $item) {
			$answers 	= $this->getUnit_section_answers($item->id);
			$return[] 	= array('type' => $item->type, 'content' => $item->content, 
				'answers' => $answers,'sectionTitle' => $item->title,
				'points' => $item->points);
		}

		return $return;
	}
	function getUnit_section() {
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('id', 0, 'INT');
		$sectionPosition 	= $input->get('q', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db = JDatabaseDriver::getInstance( $option );
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__audit_unit_sections','a'));
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
		$query->where($db->quoteName('a.ordering').' = '.$db->quote($sectionPosition));
		$db->setQuery($query);
		$result = $db->loadObject();
		$answers 	= $this->getUnit_section_answers($result->id);
		$return 	= array(
			'sectionID' => $result->id,
			'type' => $result->type, 
			'content' => $result->content, 
			'answers' => $answers,
			'sectionTitle' => $result->title,
			'points' => $result->points,
			'ordering' => $result->ordering);
	
		return $return;
	}
	function getUnit_section_answers($sectionID) {
		$return  = false;
		$session = JFactory::getSession();
		$answers = $session->get('answers');
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('unitID', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db = JDatabaseDriver::getInstance( $option );
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__audit_unit_section_answers','a'));
		$query->where($db->quoteName('a.sectionID').' = '.$db->quote($sectionID));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result) {
			foreach($result as $item) {
				$btn 		= 'btn-light';
				$aResult 	= 0;
				$aChoice 	= 0;
				if($answers) {
					foreach($answers as $answer) {
						if($answer['answers']) {
							foreach($answer['answers'] as $a) {
								if($a['answerID'] == $item->id) {
									if($a['choice'] == 1) {
										$btn 	= 'btn-success';
										$aChoice = $a['choice'];
														echo $aChoice;
									}
								}
							}
						}
					}
				}

				$return[] = array('id' => $item->id, 'sectionID' => $item->sectionID, 'answer' => $item->answer, 'correct' => $item->correct, 'result' => $aResult, 'choice' => $aChoice, 'btn' => $btn);
			}
		}
		return $return;
	}
	function getCountSections() {
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('id', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db 		= JDatabaseDriver::getInstance( $option );
		$db = $this->getDbo();
		$query 		= $db->getQuery(true);
		$query->select(array('COUNT(a.id) as countSections'));
        $query->from($db->quoteName('#__audit_unit_sections','a'));
		//$query->join('LEFT', $db->quoteName('#__inclure_projekte', 'b') . ' ON (' . $db->quoteName('a.projekt') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
		$db->setQuery($query);
		$result = $db->loadResult();

		return $result;
	}
	function getResult() {
		$input 		= JFactory::getApplication()->input;
		$theResultID= $input->get('rID', 0, 'INT');
		JLoader::register('ResultHelper',JPATH_COMPONENT_SITE.'/helpers/result.php');
		$result = new ResultHelper();
		$html = $result->getResult($theResultID);
		
		return $html;
	}
	function getResultText($score) {
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('id', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db 		= JDatabaseDriver::getInstance( $option );
		$query 		= $db->getQuery(true);
		$query->select(array('a.result'));
        $query->from($db->quoteName('#__audit_unit_results','a'));
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
		$query->where($db->quoteName('a.fromPercent').' <= '.$db->quote($score));
		$query->where($db->quoteName('a.tilPercent').' >= '.$db->quote($score));
		$db->setQuery($query);
		$result = $db->loadResult();

		return $result;
	}
	function getPoints($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('SUM(a.points) as points');
        $query->from($db->quoteName('#__audit_resultelements','a'));
		$query->join('LEFT', $db->quoteName('#__audit_results', 'b') . ' ON (' . $db->quoteName('a.resultID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('b.id').' = '.$db->quote($resultID));
		//$query->group('a.resultID');
		$db->setQuery($query);
		$points = $db->loadResult();
		
		return $points;
	}
	function getTimes($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.start, a.stop'));
        $query->from($db->quoteName('#__audit_times','a'));
		$query->join('LEFT', $db->quoteName('#__audit_results', 'b') . ' ON (' . $db->quoteName('a.resultID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('b.id').' = '.$db->quote($resultID));
		//$query->group('a.resultID');
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}
	function getResultS($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__audit_results','a'));
		$query->where($db->quoteName('a.id').' = '.$db->quote($resultID));
		$db->setQuery($query);
		$result = $db->loadObject();
		$elements = $this->getResultelements($resultID);
		$html = '';
		if($result) {
			$i = 1;
			$html .= '<table class="table table-striped table-sm">';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<th>Frage Nr.</th>';
			$html .= '<th>Frage</th>';
			$html .= '<th>Antwort</th>';
			$html .= '<th>Max. Punkte</th>';
			$html .= '<th class="text-right">Erreichte Punkte</th>';
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			$sum_points = 0;
			$max_points = 0;
			foreach($elements as $item) {
				if($item->soll != $item->ist) {
					$color = 'text-danger';
				} else {
					$color = 'text-success';
				}
				$html .= '<tr>';
				$html .= '<td>'.$i.'</td>';
				$html .= '<td class="'.$color.'"><b>'.$item->name.'</b></td>';
				$html .= '<td>'.$item->answers.'</td>';
				$html .= '<td>'.$item->max_points.'</td>';
				$html .= '<td class="text-right">'.$item->sum_points.'</td>';
				$html .= '</tr>';
				$i++;
				$max_points += $item->max_points;
				$sum_points += $item->sum_points;
			}
			$html .= '</tbody>';
			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td colspan="3">In dieser Unit erreichte Punkte:</td>';
			$html .= '<td>'.$max_points.'</td>';
			$html .= '<td class="text-right">'.$sum_points.'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="4">Entspricht einem Highscore von:</td>';
			$html .= '<td class="text-right">'.number_format(($sum_points * 100) / $max_points,2,',','.').'%</td>';
			$html .= '</tr>';
			$times = $this->getAllTimes($resultID);
			$html .= '<tr>';
			$html .= '<td colspan="4">Benötigte Zeit:</td>';
			$html .= '<td class="text-right">'.$times.'</td>';
			$html .= '</tr>';
			$html .= '</tfoot>';
			$html .= '</table>';
		}

		return $html;
	}
	function getResultelements($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*,
			a.points as sum_points,
			a.answers as ist,
			b.name,
			b.answers as soll,
			c.points as max_points
		'));
        $query->from($db->quoteName('#__audit_resultelements','a'));
        $query->join('LEFT', $db->quoteName('#__audit_quizzes', 'b') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('b.id') . ')');
        $query->join('LEFT', $db->quoteName('#__audit_unitelements', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.quizzID') . ')');
		$query->where($db->quoteName('a.resultID').' = '.$db->quote($resultID));
		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;
	}
	function getAllTimes($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__audit_times','a'));
		$query->where($db->quoteName('a.resultID').' = '.$db->quote($resultID));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$dauer = 0;
		if($result) {
			foreach($result as $item) {
				$dauer += $item->stop - $item->start;
			}
		}
		$dauer = (($dauer / 60) % 60).' Minuten';

		return $dauer;
	}
	/**
	 * Increment the hit counter for the item.
	 *
	 * @param   integer  $pk  Optional primary key of the item to increment.
	 *
	 * @return  boolean  True if successful; false otherwise and internal error set.
	 */
	public function hit($pk = 0)
	{
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);

		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('fragenkatalog.id');

			$table = $this->getTable();
			$table->load($pk);
			$table->hit($pk);
		}

		return true;
	}
}
?>