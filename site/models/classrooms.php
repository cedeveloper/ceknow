<?php

/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class JclassroomModelClassrooms extends JModelList {
	
	public function __construct($config = array()) {
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'a.id', 'id',
				'a.ort', 'ort',
				'a.title', 'title',
				'a.published', 'published'
			);
		}
		parent::__construct($config);
	}
	
	protected function populateState($ordering = 'id', $direction = 'ASC') {
		// Get the Application
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		
		// Set filter state for search
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
	
		// Set filter state for bereich
		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		// Set filter state for bereich
		$created_by = $this->getUserStateFromRequest($this->context.'.filter.created_by', 'filter_created_by', '');
		$this->setState('filter.created_by', $created_by);
		
		// Load the parameters.
		$params = JComponentHelper::getParams('com_inclure');
		$active = $menu->getActive();
		empty($active) ? null : $params->merge($active->params);
		$this->setState('params', $params);

		// List state information.
		parent::populateState($ordering, $direction);
	}

	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.category_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery() {
		$user 		= JFactory::getUser();
        $groups 	= JAccess::getGroupsByUser($user->id);
        $session 	= JFactory::getSession();
        $group 		= $session->get('group');
        $customerID = $session->get('customerID');
		// Get database object
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('
			a.*,
			b.name as creator,
			c.name as companyName,
			d.name as modifier,
			e.name as mainTrainer,
			f.name as coTrainer
		')->from('#__jclassroom_classrooms AS a');
		$query->join('LEFT', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('b.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'c') . ' ON (' . $db->quoteName('a.companyID') . ' = ' . $db->quoteName('c.id') . ')');
		$query->join('LEFT', $db->quoteName('#__users', 'd') . ' ON (' . $db->quoteName('a.modified_by') . ' = ' . $db->quoteName('d.id') . ')');
		$query->join('LEFT', $db->quoteName('#__users', 'e') . ' ON (' . $db->quoteName('a.main_trainer') . ' = ' . $db->quoteName('e.id') . ')');
		$query->join('LEFT', $db->quoteName('#__users', 'f') . ' ON (' . $db->quoteName('a.co_trainer') . ' = ' . $db->quoteName('f.id') . ')');
   		// Filter by search
		$search = $this->getState('filter.search');
		
		if (!empty($search))
		{	
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where('a.id LIKE '.$search. ' OR a.title LIKE ' . $search.' OR a.description LIKE '.$search);
		}	
		// SET FILTER
		//Superuser
		if($group == 'superuser') {}
		//Customer
        if($group == 'customer' || $group == 'customeradmin') {
        	$query->where('(a.customerID = ' . $db->quote($customerID).')');
        }
        //Trainer
        if($group == 'trainer') {
        	$query->where('a.customerID = ' . $db->quote($customerID));
        }
        
		/*$published = $this->getState('filter.published');
		if ($published != "")
		{
			$query->where('a.published = ' . $db->quote($db->escape($published)));
		}
		// SET FILTER
		$created_by = $this->getState('filter.created_by');

		if ($created_by != "")
		{
			$query->where('a.created_by = ' . $db->quote($db->escape($created_by)));
		}*/
		
		// Add list oredring and list direction to SQL query
		$sort = $this->getState('list.ordering', 'id');
		$order = $this->getState('list.direction', 'ASC');
		$query->order($db->escape($sort).' '.$db->escape($order));
		return $query;
	}
	public function getAuthors()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text')
			->from('#__users AS u')
			->join('INNER', '#__inclure_druckgruppen AS a ON a.created_by = u.id')
			->group('u.id, u.name')
			->order('u.name');

		// Setup the query
		$db->setQuery($query);

		// Return the result
		return $db->loadObjectList();
	}
	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   12.2
	 */
	public function getItems()
	{
		if ($items = parent::getItems()) {
			$session 	= JFactory::getSession();
			$usergroup 	= $session->get('group');
			foreach($items as $item):
				$maintrainer 	= $item->main_trainer;
				$cotrainer 		= $item->co_trainer;
				$db = JFactory::getDbo();
				$groups = JAccess::getGroupsByUser($maintrainer);
				if(in_array(12,$groups) || in_array(13, $groups)) {
            		$query = $db->getQuery(true);
					$query->select(array('CONCAT(a.salutation," ",a.first_name," ",a.last_name)'));
			        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
					$query->where('a.userID = '.$maintrainer);;
					$db->setQuery($query);
					$result = $db->loadResult();
					if($usergroup == 'trainer' && $item->created_by == 0):
						$item->main_trainer = '';
					else:
						$item->main_trainer = '<i title="Haupttrainer" class="fa fa-graduation-cap"></i> '.$result;
					endif;
		        }
		        if(in_array(10,$groups)) {
		            $query = $db->getQuery(true);
					$query->select(array('CONCAT(a.salutation," ",a.first_name," ",a.last_name)'));
			        $query->from($db->quoteName('#__jclassroom_trainers','a'));
					$query->where('a.tblUserID = '.$maintrainer);;
					$db->setQuery($query);
					$result = $db->loadResult();
					if($usergroup == 'trainer' && $item->created_by == 0):
						$item->main_trainer = '';
					else:
						$item->main_trainer = '<i title="Haupttrainer" class="fa fa-graduation-cap"></i> '.$result;
					endif;
		        }
		        if(in_array(8,$groups)) {
		            $query = $db->getQuery(true);
					$query->select(array('a.name'));
			        $query->from($db->quoteName('#__users','a'));
					$query->where('a.id = '.$maintrainer);;
					$db->setQuery($query);
					$result = $db->loadResult();
					if($usergroup == 'superuser' && $item->created_by == 0):
						$item->main_trainer = '';
					else:
						$item->main_trainer = '<i title="Haupttrainer" class="fa fa-graduation-cap"></i> '.$result;
					endif;
		        }
		        if($maintrainer == 0 || $maintrainer == -1):
		        	$item->main_trainer = '';
		        endif;

				$groups = JAccess::getGroupsByUser($item->co_trainer);
				if(in_array(12,$groups) || in_array(13, $groups)) {
            		$query = $db->getQuery(true);
					$query->select(array('CONCAT(a.salutation," ",a.first_name," ",a.last_name)'));
			        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
					$query->where('a.userID = '.$item->co_trainer);;
					$db->setQuery($query);
					$result = $db->loadResult();
					if($usergroup == 'trainer' && $item->created_by == 0):
						$item->co_trainer = '';
					else:
						$item->co_trainer = '<i title="Co-Trainer" class="fa fa-user"></i> '.$result;
					endif;
		        }
		        if(in_array(10,$groups)) {
		            $query = $db->getQuery(true);
					$query->select(array('CONCAT(a.salutation," ",a.first_name," ",a.last_name)'));
			        $query->from($db->quoteName('#__jclassroom_trainers','a'));
					$query->where('a.tblUserID = '.$item->co_trainer);;
					$db->setQuery($query);
					$result = $db->loadResult();
					if($usergroup == 'trainer' && $item->created_by == 0):
						$item->co_trainer = '';
					else:
						$item->co_trainer = '<i title="Co-Trainer" class="fa fa-user"></i> '.$result;
					endif;
		        }
		        if($cotrainer == 0 || $cotrainer == -1):
		        	$item->co_trainer = '';
		        endif;
				$item->created 	= date('d.m.Y H:i', strtotime($item->created));
				$item->modified = date('d.m.Y H:i', strtotime($item->modified));
				$item->fromDate = date('d.m.Y', strtotime($item->fromDate));
				$item->toDate = date('d.m.Y', strtotime($item->toDate));
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_rights','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$rights = $db->loadObjectList();
				$item->rights = $rights;
			endforeach;
		}

		return $items;
	}

}