<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldCompanies extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$session 	= JFactory::getSession();
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$options 	= array();
		
		$query = $db->getQuery(true);
		$query->select("a.id, a.name")->from("#__jclassroom_companys as a");
		if($session->get('customerID')):
			$query->where('a.customerID = ' . $session->get('customerID'));
		endif;
	
		$query->order('a.name asc');
		$db->setQuery($query);
		
		foreach ($db->loadObjectList() as $item)
		{
			// Create a new option object based on the <option /> element.
			$kdnr 	= '( CU'.str_pad($item->id, 8, '0', STR_PAD_LEFT).' ) ';
			$tmp 	= JHtml::_('select.option', $item->id, $kdnr.$item->name);

			// Add the option object to the result set.
			$options[] = $tmp;
		}

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>