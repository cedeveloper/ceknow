<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldTextmoduletitles extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$options = array();
		$options[] = JHtml::_('select.optgroup', 'Plan bestellen');
		$tmp = JHtml::_('select.option', 'Einleitungstext Planbestellung', 'Einleitungstext Planbestellung');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Registrierung Planbestellung', 'Registrierung Planbestellung');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Waitformail Planbestellung', 'Waitformail Planbestellung');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Bestätigungsmail Planbestellung -> Register', 'Bestätigungsmail Planbestellung -> Register');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Accountready Planbestellung', 'Accountready Planbestellung');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Bestätigungsmail Planbestellung -> Final', 'Bestätigungsmail Planbestellung -> Final');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Danke Planbestellung', 'Danke Planbestellung');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Danke Planbestellung mit Konto', 'Danke Planbestellung mit Konto');
		$options[] = JHtml::_('select.optgroup', 'Plan bestellen');
		$options[] = JHtml::_('select.optgroup', 'Plan ändern');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Accountready Planänderung', 'Accountready Planänderung');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Neuer Plan mit Konto', 'Neuer Plan mit Konto');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Bestätigungsmail Planwechsel Upgrade', 'Bestätigungsmail Planwechsel Upgrade');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 'Bestätigungsmail Planwechsel Downgrade', 'Bestätigungsmail Planwechsel Downgrade');
		$options[] = $tmp;
		$options[] = JHtml::_('select.optgroup', 'Plan ändern');
		$options[] = JHtml::_('select.optgroup', 'Plan kündigen');
		$tmp = JHtml::_('select.option', 'Kündigungsmail Plan', 'Kündigungsmail Plan');
		$options[] = $tmp;
		$options[] = JHtml::_('select.optgroup', 'Plan kündigen');
		$options[] = JHtml::_('select.optgroup', 'Demoaccount');
		$tmp = JHtml::_('select.option', 'Demoaccount erstellen', 'Demoaccount erstellen');
		$options[] = $tmp;
		$options[] = JHtml::_('select.optgroup', 'Demoaccount');

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>