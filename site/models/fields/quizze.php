<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldQuizze extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$group 		= $session->get('group');
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$options 	= array();
		$query 		= $db->getQuery(true);
		$query->select('
			a.id,
			a.title,
			a.description,
			a.choosableto,
			a.created_by,
			a.showto,
			b.name as customer_name,
			b.customer_number
		')->from("#__jclassroom_units as a");
		 $query->join('LEFT', $db->quoteName('#__jclassroom_customer', 'b') . ' ON (' . $db->quoteName('a.customerID') . ' = ' . $db->quoteName('b.id') . ')');
		if($session->get('customerID')):
			$query->where('a.customerID = '.$session->get('customerID'));
		endif;
		$query->order('a.title asc');
		$db->setQuery($query);
		
		foreach ($db->loadObjectList() as $item) {
			// Superuser can see all quizze
			if($group == 'superuser'):
				$tmp = JHtml::_('select.option', $item->id, '( '.str_pad($item->id, 6, '0', STR_PAD_LEFT).' )[ '.$item->customer_number.' '.$item->customer_name.' ] '.$item->title);
				$options[] = $tmp;
			endif;
			if($group == 'customer' || $group == 'customeradmin'):
				if($item->showto == 1):
					if($user->id == $item->created_by):
						$tmp = JHtml::_('select.option', $item->id, '( '.str_pad($item->id, 6, '0', STR_PAD_LEFT).' ) '.$item->title);
						$options[] = $tmp;
					endif;
				endif;
				if($item->showto == 3):
					$tmp = JHtml::_('select.option', $item->id, '( '.str_pad($item->id, 6, '0', STR_PAD_LEFT).' ) '.$item->title);
					$options[] = $tmp;
				endif;
			endif;
			if($group == 'trainer'):
				if($item->showto == 3):
					$tmp = JHtml::_('select.option', $item->id, '( '.str_pad($item->id, 6, '0', STR_PAD_LEFT).' ) '.$item->title);
					$options[] = $tmp;
				endif;
			endif;
		}

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>