<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldCustomers extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$db 	= JFactory::getDbo();
		$user 	= JFactory::getUser();
		$session = JFactory::getSession();
		$options = array();
		
		$query = $db->getQuery(true);

		$query->select("a.id, a.customer_number, a.company_name")->from("#__jclassroom_customer as a");
		if($session->get('customerID')):
			$query->where('a.id = '. $session->get('customerID'));
		endif;

		$query->order('a.name asc');
		$db->setQuery($query);
		
		foreach ($db->loadObjectList() as $item)
		{
			// Create a new option object based on the <option /> element.
			$tmp = JHtml::_('select.option', $item->id, '( '.$item->customer_number.' ) '.$item->company_name);

			// Add the option object to the result set.
			$options[] = $tmp;
		}

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>