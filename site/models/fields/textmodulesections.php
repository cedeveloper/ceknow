<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldTextmodulesections extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$options = array();
		$tmp = JHtml::_('select.option', 3, 'Testkonto erstellen');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 1, 'Planbestellung');
		$options[] = $tmp;
		$tmp = JHtml::_('select.option', 2, 'Planänderung');
		$options[] = $tmp;


		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>