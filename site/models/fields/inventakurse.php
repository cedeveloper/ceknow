<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldInventakurse extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		$options = array();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      	$table      = JTable::getInstance('Configuration','JclassroomTable',array());
      	$load 	= array('parameter' => 'connected_to_inventa');
      	$table->load($load);
      	$connected_to_inventa = $table->wert;
		// Load Data from INVENTA
		if($connected_to_inventa == 1):
			$option = array(); 
			$option['driver']   = 'mysql';  	// Database driver name
			$option['host']     = 'localhost'; 	// Database host name
			$option['user']     = 'cede01';    	// User for database authentication
			$option['password'] = 'sbZ78p5etz';   // Password for database authentication
			$option['database'] = 'cede01';    	// Database name
			$option['prefix']   = 'web3_';     // Database prefix (may be empty)
		
			$db = JDatabaseDriver::getInstance( $option );
			$query = $db->getQuery(true);
			$query->select(array('a.id,a.kurs_nummer, a.kurs_name'));
	        $query->from($db->quoteName('#__inventa_kurse','a'));
			$query->where('a.aktiv = 1');
			$db->setQuery($query);
		
			foreach ($db->loadObjectList() as $item)
			{
				// Create a new option object based on the <option /> element.
				$tmp = JHtml::_('select.option', $item->id, '( '.$item->kurs_nummer.' ) '.$item->kurs_name);
				// Add the option object to the result set.
				$options[] = $tmp;
			}
		endif;

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>