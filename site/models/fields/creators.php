<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldCreators extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$session 	= JFactory::getSession();
		$options 	= array();
		// Append, if user is Super User, else do not render
		if($session->get('group') == 'superuser'):
			// Load the Systemadmins to list
			$query = $db->getQuery(true);
			$query->select("a.id, a.name")->from("#__users as a");
			$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
			$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
			$query->where('d.title = "Super Users"');
			$query->order('a.name ASC');
			$db->setQuery($query);
			$options[] = JHtml::_('select.optgroup', 'Systemadministratoren');
			foreach ($db->loadObjectList() as $item) {
				$tmp = JHtml::_('select.option', $item->id, $item->name);
				$options[] = $tmp;
			}
			$options[] = JHtml::_('select.optgroup', 'Systemadministratoren');
		endif;
		// Set the Customeradmins to list
		$query = $db->getQuery(true);
		$query->select("a.id, a.name,d.title,f.company_name")->from("#__users as a");
		$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
		$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
		$query->join('INNER', $db->quoteName('#__jclassroom_customer_administratoren', 'e') . ' ON (' . $db->quoteName('e.userID') . ' = ' . $db->quoteName('a.id') . ')');
		$query->join('INNER', $db->quoteName('#__jclassroom_customer', 'f') . ' ON (' . $db->quoteName('e.customerID') . ' = ' . $db->quoteName('f.id') . ')');
		if($session->get('group') == 'customer' || $session->get('group') == 'trainer'):
			$query->where('e.customerID = ' .$session->get('customerID'));
		endif;
		$query->order('f.company_name ASC,a.name ASC');
		$db->setQuery($query);
		$options[] = JHtml::_('select.optgroup', 'Kundenadministratoren');
		foreach ($db->loadObjectList() as $item) {
			$tmp = JHtml::_('select.option', $item->id, $item->name.' ( ' .$item->company_name.' )');
			$options[] = $tmp;
		}
		$options[] = JHtml::_('select.optgroup', 'Kundenadministratoren');
		$options[] = JHtml::_('select.optgroup', 'Trainer');
		// Load the trainers to list
		if($session->get('group') == 'superuser' || $session->get('group') == 'customer' || $session->get('group') == 'trainer'):
			$query = $db->getQuery(true);
			$query->select("
				a.id, 
				a.name,
				d.title,
				e.first_name,
				e.last_name,
				f.company_name
			")->from("#__users as a");
			$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
			$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
			
			$query->join('INNER', $db->quoteName('#__jclassroom_trainers', 'e') . ' ON (' . $db->quoteName('e.tblUserID') . ' = ' . $db->quoteName('a.id') . ')');
			$query->join('LEFT', $db->quoteName('#__jclassroom_customer', 'f') . ' ON (' . $db->quoteName('e.customerID') . ' = ' . $db->quoteName('f.id') . ')');

			$query->where('e.customerID = '.$session->get('customerID'));

			$query->order('f.company_name ASC,e.last_name ASC');
			$db->setQuery($query);
			
			foreach ($db->loadObjectList() as $item) {
				$tmp = JHtml::_('select.option', $item->id, $item->first_name.' '.$item->last_name.' ( '.$item->company_name.' )');
				$options[] = $tmp;
			}
		endif;
		$options[] = JHtml::_('select.optgroup', 'Trainer');
		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>