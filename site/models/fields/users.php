<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldUsers extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$session 	= JFactory::getSession();
		$options 	= array();
		// Set the Customeradmins to list
		$query = $db->getQuery(true);
		$query->select("a.id, a.name,d.title")->from("#__users as a");
		$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
		$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
		if($session->get('customerID')):
			$query->join('LEFT', $db->quoteName('#__jclassroom_customer_administratoren', 'e') . ' ON (' . $db->quoteName('e.userID') . ' = ' . $db->quoteName('a.id') . ')');
			$query->where('e.customerID = '.$session->get('customerID'));
		endif;
		//$query->where('d.title = "Student"','OR');
		//$query->where('d.title = "Super Users"');
		$query->order('d.title ASC, a.name ASC');
		$db->setQuery($query);
		$tmp = JHtml::_('select.optgroup', 'Kundenadministratoren');
		$options[] = $tmp;
		foreach ($db->loadObjectList() as $item) {
			$tmp = JHtml::_('select.option', $item->id, $item->name.' ( '.$item->title.' )');
			$options[] = $tmp;
		}

		$tmp = JHtml::_('select.optgroup', 'Trainer');
		$options[] = $tmp;
		if($session->get('group') == 'superuser' || $session->get('group') == 'customer' || $session->get('group') == 'trainer'):
			$query = $db->getQuery(true);
			$query->select("
				a.id, 
				a.name,
				d.title,
				e.first_name,
				e.last_name
			")->from("#__users as a");
			$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
			$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
			
				$query->join('LEFT', $db->quoteName('#__jclassroom_trainers', 'e') . ' ON (' . $db->quoteName('e.tblUserID') . ' = ' . $db->quoteName('a.id') . ')');
				$query->where('e.customerID = '.$session->get('customerID'));

			$query->order('d.title ASC, e.last_name ASC');
			$db->setQuery($query);
			
			foreach ($db->loadObjectList() as $item)
			{
				// Create a new option object based on the <option /> element.
				$tmp = JHtml::_('select.option', $item->id, $item->first_name.' '.$item->last_name.' ( '.$item->title.' )');

				// Add the option object to the result set.
				$options[] = $tmp;
			}
		endif;
		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>