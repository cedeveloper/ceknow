<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldVerificationtemplates extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$session 	= JFactory::getSession();
		$options 	= array();
		
		$query = $db->getQuery(true);
		$query->select(array('a.id, a.title'))->from("#__jclassroom_templates as a");
		$query->where('a.type = 2');
		if($session->get('group') == 'superuser'):

		endif;
		if($session->get('group') == 'customer'):
			$query->where('a.customerID = '.$session->get('customerID'));
		endif;
		if($session->get('group') == 'trainer'):
			$query->where('a.created_by = '.$user->id);
		endif;
		$db->setQuery($query);
		
		foreach ($db->loadObjectList() as $item)
		{
			// Create a new option object based on the <option /> element.
			$tmp = JHtml::_('select.option', $item->id, $item->title);

			// Add the option object to the result set.
			$options[] = $tmp;
		}

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>