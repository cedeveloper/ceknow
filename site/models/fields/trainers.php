<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldTrainers extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$session 	= JFactory::getSession();
		$usergroup 	= $session->get('group');
		$options 	= array();

		// 1. Add the Administratoren of customer
		$options[] = JHtml::_('select.optgroup', 'Kundenadministratoren');
		$query = $db->getQuery(true);
		$query->select("a.id, CONCAT(a.first_name,' ',a.last_name) as name, userID")->from("#__jclassroom_customer_administratoren as a");
		if($usergroup == 'customer' || $usergroup == 'trainer'):
			$query->where('a.customerID = ' . $session->get('customerID'));
		endif;
		$db->setQuery($query);
		
		foreach ($db->loadObjectList() as $item)
		{
			// Create a new option object based on the <option /> element.
			$tmp = JHtml::_('select.option', $item->userID, $item->name);

			// Add the option object to the result set.
			$options[] = $tmp;
		}
		$options[] = JHtml::_('select.optgroup', 'Kundenadministratoren');
		// 2. Add the Trainer of customer
		$options[] = JHtml::_('select.optgroup', 'Trainer');

		$query = $db->getQuery(true);
		$query->select("a.id, CONCAT(a.first_name,' ',a.last_name) as name, tblUserID")->from("#__jclassroom_trainers as a");
		if($usergroup == 'customer' || $usergroup == 'trainer'):
			$query->where('a.customerID = ' . $session->get('customerID'));
		endif;
		$query->order('a.last_name ASC');
		$db->setQuery($query);
		
		foreach ($db->loadObjectList() as $item)
		{
			// Create a new option object based on the <option /> element.
			$tmp = JHtml::_('select.option', $item->tblUserID, $item->name);

			// Add the option object to the result set.
			$options[] = $tmp;
		}
		$options[] = JHtml::_('select.optgroup', 'Trainer');

		if($usergroup == 'superuser'):
			// 3. Add the Systemadministratoren
			$options[] = JHtml::_('select.optgroup', 'Systemadministratoren');
			$query = $db->getQuery(true);
			$query->select("a.id, a.name")->from("#__users as a");
			$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.user_id') . ')');
			$query->join('LEFT', $db->quoteName('#__usergroups', 'c') . ' ON (' . $db->quoteName('b.group_id') . ' = ' . $db->quoteName('c.id') . ')');
			$query->where('c.title = "Super Users"');
			$db->setQuery($query);
			
			foreach ($db->loadObjectList() as $item)
			{
				$tmp = JHtml::_('select.option', $item->id, $item->name);
				$options[] = $tmp;
			}
			$options[] = JHtml::_('select.optgroup', 'Systemadministratoren');
		endif;
		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);
		return $options;
	}
}
?>