<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JFormHelper::loadFieldClass('list');

/**
 * Form field for Mandant items.
 *
 * @package		Auditum
 * @subpackage	Fields
 */
class JFormFieldClassrooms extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'list';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$session 	= JFactory::getSession();
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$options 	= array();
		
		$query = $db->getQuery(true);
		$query->select(array('
			a.id,
			a.title,
			a.showto,
			a.created_by
		'))->from("#__jclassroom_classrooms as a");
        if($session->get('customerID')):
			$query->where('a.customerID = ' . $session->get('customerID'));
		endif;
		$query->order('a.title asc');
		$db->setQuery($query);
		
		foreach ($db->loadObjectList() as $item)
		{
			$tmp = array();
			if($session->get('group') != 'superuser' && $item->showto == 0):
				$query = $db->getQuery(true);
				$query->select(array('
					a.rightType
				'))->from("#__jclassroom_classroom_rights as a");
				$query->where('a.classroomID = '.$item->id);
				$query->where('a.userID = '.$user->id);
				$db->setQuery($query);
				$rigths = $db->loadResult();
				if($rights):
					$tmp = JHtml::_('select.option', $item->id, '( LR'.str_pad($item->id, 8, '0', STR_PAD_LEFT).' ) '.$item->title);
					$options[] = $tmp;
				endif;
			endif;
			if($session->get('group') != 'superuser' && $item->showto == 1):
				if($item->created_by == $user->id):
					$tmp = JHtml::_('select.option', $item->id, '( LR'.str_pad($item->id, 8, '0', STR_PAD_LEFT).' ) '.$item->title);
					$options[] = $tmp;
				endif;
			endif;
			if($session->get('group') != 'superuser' && $item->showto == 3):
				$tmp = JHtml::_('select.option', $item->id, '( LR'.str_pad($item->id, 8, '0', STR_PAD_LEFT).' ) '.$item->title);
				$options[] = $tmp;
			endif;
			if($session->get('group') == 'superuser'):
				$tmp = JHtml::_('select.option', $item->id, '( LR'.str_pad($item->id, 8, '0', STR_PAD_LEFT).' ) '.$item->title);
				$options[] = $tmp;
			endif;
		}

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>