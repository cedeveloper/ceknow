<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for auditor.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelOrder extends JModelAdmin
{
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_JCLASSROOM';

	/**
	 * The type alias for this content type.
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_jclassroom.order';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object    $record    A record object.
	 *
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id))
		{
			if ($record->published != -2)
			{
				return false;
			}
			

			$user = JFactory::getUser();
			return $user->authorise('core.delete', $this->typeAlias . '.' . (int) $record->id);
		}
	}		

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable    A JTable object.
	 *
	 * @return  void
	 * @since   1.6
	 */
	protected function prepareTable($table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
	}

	/**
	 * Auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('administrator');

		// Load the User state.
		$pk = $app->input->getInt('id');
		$this->setState($this->getName() . '.id', $pk);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jclassroom');
		$this->setState('params', $params);
	}
	
	/**
	 * Alias for JTable::getInstance()
	 *
	 * @param   string  $type    The type (name) of the JTable class to get an instance of.
	 * @param   string  $prefix  An optional prefix for the table class name.
	 * @param   array   $config  An optional array of configuration values for the JTable object.
	 *
	 * @return  mixed    A JTable object if found or boolean false if one could not be found.
	 */
	public function getTable($type = 'Order', $prefix = 'JclassroomTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	/**
	 * Method for getting the form from the model.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/fields');

		JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR.'/models/rules');		
		
		$options = array('control' => 'jform', 'load_data' => $loadData);
		$form = $this->loadForm($this->typeAlias, $this->name, $options);
		
		if(empty($form))
		{
			return false;
		}

		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  array    The default data is an empty array.
	 */
	protected function loadFormData()
	{
		$app = JFactory::getApplication();
		$data = $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());
		
		if(empty($data))
		{
			$data = $this->getItem();
		}
		
		return $data;
	}
	
	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if (!$item = parent::getItem($pk))
		{			
			throw new Exception('Failed to load item');
		}
		$session 	= JFactory::getSession();
		$input 		= JFactory::getApplication()->input;
		$db = JFactory::getDbo();
		// Load Contacts
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_hardware','a'));
		$query->where('a.customerID = '.$session->get('customerID'));
		$query->where('a.published = 1');
		$query->order('a.title asc');
		$db->setQuery($query);
		$item->hardware = $db->loadObjectList();
		
		if($item->id):
			$session 	= JFactory::getSession();
			// Load Contacts
			$query = $db->getQuery(true);
			$query->select(array('
				a.id,
				a.amount,
				b.title
			'));
	        $query->from($db->quoteName('#__jclassroom_order_positions','a'));
	        $query->join('LEFT', $db->quoteName('#__jclassroom_hardware', 'b') . ' ON (' . $db->quoteName('a.hardwareID') . ' = ' . $db->quoteName('b.id') . ')');
			$query->where('a.orderID = '.$item->id);
			$query->where('a.published = 1');
			$query->order('b.title asc');
			$db->setQuery($query);
			$item->positions 	= $db->loadObjectList();
			$item->salutation 	= $item->salutation;
			$item->first_name 	= $item->first_name;
			$query = $db->getQuery(true);
			$query->select(array('a.title'));
	        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
			$query->where('a.classroomID = '.$item->classroomID);
			$query->order('a.title asc');
			$query->setLimit(1);
			$db->setQuery($query);
			$item->deliveryDate = date('d.m.Y', strtotime($db->loadResult()));
			if($input->get('type')):
				$item->closed = 1;
			endif;
			if($input->get('type','','STR') == 'fromSt'):
				// Load the classroom
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$classroom 	= JTable::getInstance('Classroom','JclassroomTable',array());
				$classroom->load($input->get('clr', 0, 'INT'));
				$item->userID = $classroom->tblUserID;
			endif;
			// Load Learningroom
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
			$query->where('a.id = '.$item->classroomID);
			$db->setQuery($query);
			$classroom = $db->loadObject();
			$item->classroomName = $classroom->title;
			// Load the student
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Student','JclassroomTable',array());
			$load 	= array('tblUserID'=> $item->userID);
			$table->load($load);
			// Load the company
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$tableC 	= JTable::getInstance('Company','JclassroomTable',array());
			$load 	= array('id'=> $table->companyID);
			$tableC->load($load);
			$item->companyName 	= $tableC->name;
			$item->companyID 	= '<b>(CO'.str_pad($tableC->id,8,'0', STR_PAD_LEFT).')</b>';
			// Load Timeblockdata
			require_once(JPATH_COMPONENT_SITE.'/helpers/global.php');
        	$item->timeblocks = GlobalHelper::getClassroomFromTo($item->classroomID);
			// Load the company
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$tableC 	= JTable::getInstance('Customer','JclassroomTable',array());
			$load 	= array('id'=> $item->customerID);
			$check = $tableC->load($load);
			if(!$check):
				$item->customerName 	= 'Der Kunde, der das Training für diese Bestellung geplant hat, ist nicht mehr verfügbar';
				$item->customerID 		= '';
			else:
				if($tableC->company_name):
					$item->customerName 	= $tableC->company_name;
				else:
					$item->customerName 	= $tableC->name;
				endif;
				$item->customerID 		= '<b>(CU'.str_pad($item->customerID,8,'0', STR_PAD_LEFT).')</b>';
			endif;
		else:
			if($input->get('type','','STR') == 'fromSt'):
				// Load the classroom
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$classroom 	= JTable::getInstance('Classroom','JclassroomTable',array());
				$classroom->load($input->get('clr', 0, 'INT'));
				// Load the student
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('Student','JclassroomTable',array());
				$load 	= array('tblUserID'=> $input->get('uid', 0, 'INT'));
				$table->load($load);
				
				// Load the company
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$tableC 	= JTable::getInstance('Company','JclassroomTable',array());
				$load 	= array('id'=> $table->companyID);
				$tableC->load($load);
				$item->companyName 	= $tableC->name;
				$item->companyID 	= '<b>(CO'.str_pad($tableC->id,8,'0', STR_PAD_LEFT).')</b>';
				// Load Contacts
				$query = $db->getQuery(true);
				$query->select(array('a.title'));
		        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
				$query->where('a.classroomID = '.$input->get('clr', 0, 'INT'));
				$query->order('a.title asc');
				$query->setLimit(1);
				$db->setQuery($query);
				$item->deliveryDate = date('d.m.Y', strtotime($db->loadResult()));
				$item->classroomID 	= $input->get('clr', 0, 'INT');
				$item->customerID 	= $classroom->customerID;
				$item->classroomName = $classroom->title;
				$item->userID 		= $input->get('uid', 0, 'INT');
				$item->adress 		= $table->adress;
				if($table->postcode != 0):
					$item->postcode 	= $table->postcode;
				else:
					$item->postcode 	= '';
				endif;
				$item->city 		= $table->city;
				$item->phone 		= $table->phone;
				$item->mobile 		= $table->mobile;
				$item->salutation 	= $table->salutation;
				$item->first_name 	= $table->first_name;
				$item->last_name 	= $table->last_name;
				$item->closed = 1;
			endif;
		endif;

		return $item;
	}
	
	/**
	 * Increment the hit counter for the item.
	 *
	 * @param   integer  $pk  Optional primary key of the item to increment.
	 *
	 * @return  boolean  True if successful; false otherwise and internal error set.
	 */
	public function hit($pk = 0)
	{
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);

		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('kunde.id');

			$table = $this->getTable();
			$table->load($pk);
			$table->hit($pk);
		}

		return true;
	}
}
?>