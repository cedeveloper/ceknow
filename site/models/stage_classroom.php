<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for auditor.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelStage_classroom extends JModelAdmin
{
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_JCLASSROOM';

	/**
	 * The type alias for this content type.
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_jclassroom.classroom';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object    $record    A record object.
	 *
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id))
		{
			if ($record->published != -2)
			{
				return false;
			}
			

			$user = JFactory::getUser();
			return $user->authorise('core.delete', $this->typeAlias . '.' . (int) $record->id);
		}
	}		

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable    A JTable object.
	 *
	 * @return  void
	 * @since   1.6
	 */
	protected function prepareTable($table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
	}

	/**
	 * Auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('administrator');

		// Load the User state.
		$pk = $app->input->getInt('id');
		$this->setState($this->getName() . '.id', $pk);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jclassroom');
		$this->setState('params', $params);
	}
	
	/**
	 * Alias for JTable::getInstance()
	 *
	 * @param   string  $type    The type (name) of the JTable class to get an instance of.
	 * @param   string  $prefix  An optional prefix for the table class name.
	 * @param   array   $config  An optional array of configuration values for the JTable object.
	 *
	 * @return  mixed    A JTable object if found or boolean false if one could not be found.
	 */
	public function getTable($type = 'Classroom', $prefix = 'JclassroomTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	/**
	 * Method for getting the form from the model.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/fields');

		JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR.'/models/rules');		
		
		$options = array('control' => 'jform', 'load_data' => $loadData);
		$form = $this->loadForm($this->typeAlias, $this->name, $options);
		
		if(empty($form))
		{
			return false;
		}

		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  array    The default data is an empty array.
	 */
	protected function loadFormData()
	{
		$app = JFactory::getApplication();
		$data = $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());
		
		if(empty($data))
		{
			$data = $this->getItem();
		}
		
		return $data;
	}
	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if (!$item = parent::getItem($pk))
		{			
			throw new Exception('Failed to load item');
		} else {
			$user 		= JFactory::getUser();
			$input 		= JFactory::getApplication()->input;
			$preview 	= $input->get('preview', '', 'INT');
			if($preview == 1):
				$preview = '&preview=1';
			endif;
			$db 	= JFactory::getDbo();
			$query 	= $db->getQuery(true);
			$query->select(array('a.*,b.name as author'));
	        $query->from($db->quoteName('#__jclassroom_chats','a'));
	        $query->join('INNER', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('b.id') . ' = ' . $db->quoteName('a.userID') . ')');
			$query->where('a.classroomID = '.$item->id);
			$query->order('a.created ASC');
			$db->setQuery($query);
			$result = $db->loadObjectList();
			$item->chats = $result;
			if($item->id):
				if($item->presentation == 0 || $item->presentation == 2):
					// Presentation Roommodus or Timetable
			        $query 	= $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_files','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.published = 1');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->reserve = $result;
					//Load the student-Data
					$user 	= JFactory::getUser();
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 	= JTable::getInstance('Student','JclassroomTable',array());
					$load 	= array('tblUserID' => $user->id);
					$table->load($load);
					$studentID = $table->id;
					$user 	= JFactory::getUser();
					$item->user = $user->id;

					$db 	= JFactory::getDbo();
			        $query 	= $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.published = 1');
					$query->order('a.ordering ASC');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->days = $result;

					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table = JTable::getInstance('Student','JclassroomTable',array());
					$load = array('tblUserID' => $user->id);
					$table->load($load);
					$item->studentID = $table->id;
					// Load other students
					$query 	= $db->getQuery(true);
					$query->select(array('b.first_name, b.last_name'));
			        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
			        $query->join('INNER', $db->quoteName('#__jclassroom_students', 'b') . ' ON (' . $db->quoteName('b.tblUserID') . ' = ' . $db->quoteName('a.userID') . ')');
					$query->where('a.classroomID = '.$item->id);
					if($studentID):
						$query->where('a.studentID <> '.$studentID);
					endif;
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->students = $result;
					// Load other Trainer
					$query 	= $db->getQuery(true);
					$query->select(array('b.first_name,b.last_name'));
			        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
			        $query->join('INNER', $db->quoteName('#__jclassroom_trainers', 'b') . ' ON (' . $db->quoteName('b.tblUserID') . ' = ' . $db->quoteName('a.userID') . ')');
					$query->where('a.classroomID = '.$item->id);
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->trainers = $result;
					// Load MainTrainer
					$query 	= $db->getQuery(true);
					$query->select(array('b.id,b.name'));
			        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
			        $query->join('INNER', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('b.id') . ' = ' . $db->quoteName('a.main_trainer') . ')');
					$query->where('a.id = '.$item->id);
					$db->setQuery($query);
					$result = $db->loadObject();
					if($result):
						$mainTrainer[] = $result;
					endif;
					// Load CoTrainer
					$query 	= $db->getQuery(true);
					$query->select(array('b.id,b.name'));
			        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
			        $query->join('INNER', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('b.id') . ' = ' . $db->quoteName('a.co_trainer') . ')');
					$query->where('a.id = '.$item->id);
					$db->setQuery($query);
					$result = $db->loadObject();
					if($result):
						$mainTrainer[] = $result;
					endif;
					$item->mainTrainer 	= $mainTrainer;
					$item->attendees 	= array_merge($item->students,$item->trainers);
					// Load the modules
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.published = 1');
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->modules = $result;
					// Load the units
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.published = 1');
					$query->order('a.ordering asc');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->units = $result;
					// Load the regular Files
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_files','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.type = "unit"');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->unitFiles = $result;
					// Load the Files
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_files','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.type <> "unit"');
					$query->where('a.published = 1');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->files = $result;
					// Load the QuizzResults
			        $query = $db->getQuery(true);
					$query->select(array('
						a.id as theResultID,
						a.*,
						b.title,
						b.id as unitID
					'));
			        $query->from($db->quoteName('#__jclassroom_theresults','a'));
			        $query->join('INNER', $db->quoteName('#__jclassroom_units', 'b') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('b.id') . ')');
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.created_by = '.$user->id);
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->results = $result;
					//The structure of the Learningroom
					$html = '';
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query->select(array('a.id,a.title,a.background_color'));
			        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->order('a.ordering ASC');
					$db->setQuery($query);
					$blocks = $db->loadObjectList();
					if($blocks):
						foreach($blocks as $block):
							$html .= '<div class="mb-1" style="padding-bottom: 5px;background-color: '.$block->background_color.';">';
							$html .= '<h3 class="p-2" style="font-size: 24px;background-color: '.$block->background_color.';">'.date('l, d.m.Y',strtotime($block->title)).'</h3>';
							$query = $db->getQuery(true);
							$query->select(array('a.themeID,b.title'));
					        $query->from($db->quoteName('#__jclassroom_timeblocks_themes','a'));
					        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_days', 'b') . ' ON (' . $db->quoteName('a.themeID') . ' = ' . $db->quoteName('b.id') . ')');
							$query->where('a.classroomID = '.$item->id);
							$query->where('a.timeblockID = '.$block->id);
							$query->where('b.published = 1');
							$query->order('a.ordering ASC');
							$db->setQuery($query);
							$themes = $db->loadObjectList();
							if($themes):
								foreach($themes as $theme):
									$html .= '<h3 class="tttheme p-2 mb-1" style="font-size: 18px;background-color: lightsteelblue;">'.$theme->title.'</h3>';
									$query = $db->getQuery(true);
									$query->select(array('a.id,a.title'));
							        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
									$query->where('a.classroomID = '.$item->id);
									$query->where('a.dayID = '.$theme->themeID);
									$query->where('a.published = 1');
									$query->order('a.ordering ASC');
									$db->setQuery($query);
									$modules = $db->loadObjectList();
									if($modules):
										foreach($modules as $module):
											$html .= '<h3 class="ttmodule p-2 mb-1" style="font-size: 16px;color: #fff;background-color: steelblue;">'.$module->title.'</h3>';
											$query = $db->getQuery(true);
											$query->select(array('a.id,a.title,a.content,a.link,a.quizzID,a.unitType'));
									        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
											$query->where('a.classroomID = '.$item->id);
											$query->where('a.moduleID = '.$module->id);
											$query->where('a.published = 1');
											$query->order('a.ordering ASC');
											$db->setQuery($query);
											$units = $db->loadObjectList();
											if($units):
												$html .= '<div style="flex-wrap: wrap;" class="ttunit d-flex">';
												foreach($units as $unit):
													//Search for Files
													$query = $db->getQuery(true);
													$query->select(array('a.id,a.filename,a.path'));
											        $query->from($db->quoteName('#__jclassroom_files','a'));
													$query->where('a.classroomID = '.$item->id);
													$query->where('a.unitID = '.$unit->id);
													$query->where('a.published = 1');
													$query->where('a.type = "unit"');
													$db->setQuery($query);
													$files = $db->loadObjectList();
													// Search for Introimage
													$query = $db->getQuery(true);
													$query->select(array('a.id,a.filename,a.path'));
											        $query->from($db->quoteName('#__jclassroom_files','a'));
													$query->where('a.classroomID = '.$item->id);
													$query->where('a.unitID = '.$unit->id);
													$query->where('a.published = 1');
													$query->where('a.type = "imageUnit"');
													$db->setQuery($query);
													$introimage = $db->loadObject();
													switch($unit->unitType):
														case 1:
															$bg = 'palegreen';
															break;
														case 2:
															$bg = '#00c9ff';
															break;
														case 3:
															$bg = 'darkgray';
															break;
														case 4:
															$bg = 'darkorange';
															break;
														case 5:
															$bg = '#ff3600';
															break;
														case 6:
															$bg = 'lightseagreen';
															break;
														case 7:
															$bg = 'lightsalmon';
															break;
														case 8:
															$bg = 'crimson';
															break;
														case 9:
															$bg = 'PaleTurquoise';
															break;
													endswitch;
													$html .= '<div class="contentOuter p-1 mb-1 mr-1" style="position: relative;width: 250px;background-color:'.$bg.';">';
													$html .= '<h3 style="width: 200px;font-size: 14px;">'.$unit->title.'</h3>';
													if($introimage):
														$html .= '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
													endif;
													$html .= '<div id="content'.$unit->id.'" class="theContent" style="display: none;background-color: '.$bg.';">';
													$html .= '<i style="cursor: pointer; font-size: 24px;" class="float-right fa fa-close" onclick="closeContent('.$unit->id.');"></i>';
													$html .= '<b font-size: 20px;" class="d-inline">'.$unit->title.'</b>';
													$html .= '<div class="contentInner mt-2" style="padding: 15px;background-color: #fff;">';
													$html .= $unit->content;
													if($files):
														$html .= '<div class="filesContainer mb-3">';
														$html .= '<b class="d-block mb-2">Dateien zu dieser Unit</b>';
														foreach($files as $file):
															$html .= '<a href="'.$file->path.'" target="_blank" class="btn btn-primary btn-sm mr-1">'.$file->filename.'</a>';
														endforeach;
														$html .= '</div>';
													endif;
													if($unit->link):
														$html .= '<b class="d-block mb-2">Link für diese Unit</b>';
														$html .= '<a href="'.$unit->link.'" target="_blank" class="btn btn-primary btn-sm">Link öffnen</a>';
													endif;
													if($unit->quizzID):
														$html .= '<b class="d-block mb-2">Link zum Quizz</b>';
														$html .= '<a href="stage?layout=edit&sT=lr&unitID='.$unit->quizzID.'&uID='.$unit->id.'&clr='.$item->id.'&q=-1'.$preview.'" target="_blank" class="btn btn-primary btn-sm">Quizz öffnen</a>';
													endif;
													$html .= '</div>';
													$html .= '</div>';
													$html .= '</div>';
												endforeach;
												$html .= '</div>';
											endif;
										endforeach;
									endif;
								endforeach;
							else:
								$html .= '<b>Keine Themen für diesen Tag gefunden.</b>';
							endif;
							$html .= '</div>';
						endforeach;
					endif;
					$item->structure = $html;
				endif;
				// Catalog-Mode
				if($item->presentation == 1):
					$input 		= JFactory::getApplication()->input;
					$direction 	= $input->get('dir', '', 'STR');
					$session 	= JFactory::getSession();
					$db 		= JFactory::getDbo();
					$query 		= $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.published = 1');
					$db->setQuery($query);
					$db->execute();
					$numRows = $db->getNumRows();
					if($numRows == 0):
						echo 'Dieser Learningroom enthält keine Units.';
					endif;
					$item->stepsCount 	= $numRows;
					$step 		= $session->get('stepKatalog');
					if($step < 0):
						$session->set('stepKatalog', 1);
						$step = 1;
					endif;
					if(!$step):
						$session->set('stepKatalog', 1);
						$step = 1;
					endif;
					$item->step = $step;
					// Look for the next available step (ordering).
					$query 	= $db->getQuery(true);
					$query->select(array('
						a.ordering
					'));
			        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
			        $query->join('LEFT', $db->quoteName('#__jclassroom_classroom_days', 'b') . ' ON (' . $db->quoteName('a.dayID') . ' = ' . $db->quoteName('b.id') . ')');
			        $query->join('LEFT', $db->quoteName('#__jclassroom_classroom_modules', 'c') . ' ON (' . $db->quoteName('a.moduleID') . ' = ' . $db->quoteName('c.id') . ')');
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.published = 1');
					$query->where('b.published = 1');
					$query->where('c.published = 1');
					if($direction == 'forward'):
						$query->where('a.ordering >= '.$step);
						$query->order('a.ordering ASC');
					else:
						$query->where('a.ordering <= '.$step);
						$query->order('a.ordering DESC');
					endif;
					
					$query->setLimit(1);
					$db->setQuery($query);
					$next = $db->loadResult();
					if(!$next):
						$next = 1;
					endif;
					// Load the real step
			        $query 	= $db->getQuery(true);
					$query->select(array('
						a.id as unitID,
						a.title as unit_title,
						a.unitType,
						a.content,
						a.resolution,
						a.duration,
						a.link,
						a.quizzID,
						b.title as day_title,
						b.day as day_day,
						c.title as module_title,
						c.description as module_description
					'));
			        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
			        $query->join('LEFT', $db->quoteName('#__jclassroom_classroom_modules', 'c') . ' ON (' . $db->quoteName('a.moduleID') . ' = ' . $db->quoteName('c.id') . ')');
			        $query->join('LEFT', $db->quoteName('#__jclassroom_classroom_days', 'b') . ' ON (' . $db->quoteName('c.dayID') . ' = ' . $db->quoteName('b.id') . ')');
			        
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.published = 1');
					$query->where('a.ordering = '.$next);
					$query->order('a.ordering ASC');
					$db->setQuery($query);
					$result = $db->loadObject();
					$unitID = $result->unitID;
					$item->structure 	= $result;
					$item->step 		= $next;
					// Load the files
					$query 	= $db->getQuery(true);
					$query->select(array('
						a.unitID,
						a.filename,
						a.path
					'));
			        $query->from($db->quoteName('#__jclassroom_files','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.unitID = '.$result->unitID);
					$query->where('a.type <> "resolution"');
					$query->where('a.published = 1');
					$query->order('a.id ASC');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->files = $result;
					// Load the resolution-files
					$query 	= $db->getQuery(true);
					$query->select(array('
						a.unitID,
						a.filename,
						a.path
					'));
			        $query->from($db->quoteName('#__jclassroom_files','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.unitID = '.$unitID);
					$query->where('a.type = "resolution"');
					$query->where('a.created_by = '.$user->id);
					$query->where('a.published = 1');
					$query->order('a.id ASC');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->resolutionfiles = $result;
					// Load the resolutions
					if($user):
					$query 	= $db->getQuery(true);
					$query->select(array('
						a.*
					'));
			        $query->from($db->quoteName('#__jclassroom_learningroom_results','a'));
					$query->where('a.classroomID = '.$item->id);
					$query->where('a.unitID = '.$unitID);
					$query->where('a.userID = '. $user->id);
					$db->setQuery($query);
					$result = $db->loadObject();
					$item->resolution = $result;
					endif;
				endif;
			else:
				$item->days = false;
			endif;
		}

		return $item;
	}
	
	/**
	 * Increment the hit counter for the item.
	 *
	 * @param   integer  $pk  Optional primary key of the item to increment.
	 *
	 * @return  boolean  True if successful; false otherwise and internal error set.
	 */
	public function hit($pk = 0)
	{
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);

		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('kunde.id');

			$table = $this->getTable();
			$table->load($pk);
			$table->hit($pk);
		}

		return true;
	}
}
?>