<?php

/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class JclassroomModelOrders extends JModelList {
	
	public function __construct($config = array()) {
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'a.ort', 'ort',
				'a.name', 'name',
				'a.published', 'published'
			);
		}
		parent::__construct($config);
	}
	
	protected function populateState($ordering = 'id', $direction = 'ASC') {
		// Get the Application
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		
		// Set filter state for search
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		// Set filter for classroom
		// Set filter state for bereich
		$classroomID = $this->getUserStateFromRequest($this->context.'.filter.classroomID', 'filter_classroomID', '');
		$this->setState('filter.classroomID', $classroomID);
		// Set filter state for bereich
		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		// Set filter state for bereich
		$ort = $this->getUserStateFromRequest($this->context.'.filter.ort', 'filter_ort', '');
		$this->setState('filter.ort', $ort);
		
		// Load the parameters.
		$params = JComponentHelper::getParams('com_inclure');
		$active = $menu->getActive();
		empty($active) ? null : $params->merge($active->params);
		$this->setState('params', $params);

		// List state information.
		parent::populateState($ordering, $direction);
	}

	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.category_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery() {
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		// Get database object
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('
			a.*,
			c.title as classroom_name
		')->from('#__jclassroom_orders AS a');
		$query->join('INNER', $db->quoteName('#__jclassroom_order_positions', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.orderID') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_classrooms', 'c') . ' ON (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.id') . ')');
		if($customerID && $customerID != 0):
			$query->where('a.customerID = ' . $db->quote($customerID));
		endif;
		//$query->select(array('b.standort AS ' . $db->quote('standort')))
			//->join('LEFT', '#__inclure_standorte AS b ON b.id = a.standort');
		//$query->where('a.created_by = ' . $db->quote($db->escape($user->id)));
		// Filter by search
		/*$search = $this->getState('filter.search');
		
		if (!empty($search))
		{	
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where('a.name LIKE ' . $search.' OR a.kundennr LIKE '.$search);
		}*/
		// Set Filter classroom
		$classroomID = $this->getState('filter.classroomID');
		if ($classroomID != "")
		{
			$query->where('a.classroomID = ' . $db->quote($db->escape($classroomID)));
		}
		// SET FILTER
		/*$published = $this->getState('filter.published');
		if ($published != "")
		{
			$query->where('a.published = ' . $db->quote($db->escape($published)));
		}
		// SET FILTER
		$ort = $this->getState('filter.ort');
		if ($ort != "")
		{
			$query->where('a.ort = ' . $db->quote($db->escape($ort)));
		}*/
		$query->group('a.id');
		// Add list oredring and list direction to SQL query
		$sort = $this->getState('list.ordering', 'id');
		$order = $this->getState('list.direction', 'ASC');
		$query->order($db->escape($sort).' '.$db->escape($order));
		return $query;
	}
	public function getAuthors()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text')
			->from('#__users AS u')
			->join('INNER', '#__inclure_druckgruppen AS a ON a.created_by = u.id')
			->group('u.id, u.name')
			->order('u.name');

		// Setup the query
		$db->setQuery($query);

		// Return the result
		return $db->loadObjectList();
	}
	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   12.2
	 */
	public function getItems()
	{
		if ($items = parent::getItems()) {
			//Do any procesing on fields here if needed
			foreach($items as $item):
				$db = $this->getDbo();
				$query = $db->getQuery(true);
				$query->select('a.*,b.title')->from('#__jclassroom_order_positions AS a');
				$query->join('INNER','#__jclassroom_hardware AS b ON a.hardwareID = b.id');
				$query->where('a.orderID = ' . $db->quote($item->id));
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->positions = $result;
				// Load Timeblocks
				require_once(JPATH_COMPONENT_SITE.'/helpers/global.php');
        		$item->theData = GlobalHelper::getClassroomFromTo($item->classroomID);
			endforeach;
		}
		return $items;
	}

}