<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * List Model for unternehmen.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelDashboard_students extends JModelList {

	function getStudentsData() {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('
			a.*,
			b.name as companyName,
			b.showVertifyAGB,
			b.showVertifyDS,
			b.showVertifyFilm'));
        $query->from($db->quoteName('#__jclassroom_students','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'b') . ' ON (' . $db->quoteName('a.companyID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where('a.tblUserID = '.$user->id);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}
	function getClassroomDates() {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('
			Min(a.day) as fromDate,
			Max(a.day) as toDate
		'));
        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
		$query->where('a.classroomID = '.$user->id);;
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}
	function getStudentsClassrooms() {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		/*$query->select(array('
			a.title,
			a.id as classroomID,
			c.tblUserID as studentID,
			CONCAT(d.first_name," ",d.last_name) as trainer_name,
			CONCAT(i.first_name," ",i.last_name) as trainer_name2,
			f.id as orderID,
			f.created as orderdate,
			GROUP_CONCAT(DISTINCT CONCAT_WS(";", h.title, g.amount)) as orderpositions,
			Min(e.title) as fromDate,
			Max(e.title) as toDate
		'));*/
		$query->select(array('
			a.title,
			a.id as classroomID,
			a.hardware_order,
			c.tblUserID as studentID,
			CONCAT(d.first_name," ",d.last_name) as trainer_name,
			CONCAT(f.first_name," ",f.last_name) as trainer_name2,
			g.id as orderID,
			g.created as orderdate,
			GROUP_CONCAT(DISTINCT CONCAT_WS(";", i.title, h.amount)) as orderpositions,
			Min(e.title) as fromDate,
			Max(e.title) as toDate
		'));
		/*
		$query->select(array('
			a.*,
			a.id as classroomID,
			c.tblUserID as studentID,
			CONCAT(d.first_name," ",d.last_name) as trainer_name,
			f.id as orderID,
			f.created as orderdate,
			GROUP_CONCAT(DISTINCT CONCAT_WS(";", h.title, g.amount)) as orderpositions
		'));
		*/
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.classroomID') . ')');
        $query->join('INNER', $db->quoteName('#__jclassroom_students', 'c') . ' ON (' . $db->quoteName('b.userID') . ' = ' . $db->quoteName('c.tblUserID') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_trainers', 'd') . ' ON (' . $db->quoteName('a.main_trainer') . ' = ' . $db->quoteName('d.tblUserID') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_timeblocks', 'e') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('e.classroomID') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_customer_administratoren', 'f') . ' ON (' . $db->quoteName('a.main_trainer') . ' = ' . $db->quoteName('f.userID') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_orders', 'g') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('g.classroomID') . ') AND ('.$db->quoteName('c.tblUserID').' = '.$db->quoteName('g.userID').')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_order_positions', 'h') . ' ON (' . $db->quoteName('g.id') . ' = ' . $db->quoteName('h.orderID') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_hardware', 'i') . ' ON (' . $db->quoteName('h.hardwareID') . ' = ' . $db->quoteName('i.id') . ')');
  
		$query->where('b.userID = '.$user->id);
		$query->group('a.id');
		//query->order('e.day DESC');
		//Remove after Umfrage
		//$query->where('a.id = 28');
		$db->setQuery($query);
		$result = $db->loadObjectList();

		/*echo '<pre>';
		echo $user->id;
		print_r($result);*/

		return $result;
	}
}
?>