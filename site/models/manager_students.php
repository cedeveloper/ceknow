<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * List Model for unternehmen.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelManager_students extends JModelList {

	function getStudentsData() {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_students','a'));
		$query->where('a.tblUserID = '.$user->id);;
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}
	function getStudentsClassrooms() {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_classroom_students', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.classroomID') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_students', 'c') . ' ON (' . $db->quoteName('b.studentID') . ' = ' . $db->quoteName('c.id') . ')');
		$query->where('c.tblUserID = '.$user->id);;
		$db->setQuery($query);
		$result = $db->loadObjectList();
		print_r($result);
		return $result;
	}
}
?>