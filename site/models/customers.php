<?php

/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class JclassroomModelCustomers extends JModelList {
	
	public function __construct($config = array()) {
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'a.id', 'id',
				'a.customer_number', 'customer_number',
				'a.company_name', 'company_name',
				'a.published', 'published'
			);
		}
		parent::__construct($config);
	}
	
	protected function populateState($ordering = 'id', $direction = 'ASC') {
		// Get the Application
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		
		// Set filter state for search
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		// Set filter state for bereich
		$ort = $this->getUserStateFromRequest($this->context.'.filter.ort', 'filter_ort', '');
		$this->setState('filter.ort', $ort);
		
		// Load the parameters.
		$params = JComponentHelper::getParams('com_inclure');
		$active = $menu->getActive();
		empty($active) ? null : $params->merge($active->params);
		$this->setState('params', $params);

		// List state information.*/
		parent::populateState($ordering, $direction);
	}

	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.category_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery() {
		// Get database object
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('a.*')->from('#__jclassroom_customer AS a');
		$query->select(array('b.name AS creator, b.password'))
			->join('LEFT', '#__users AS b ON b.id = a.created_by');
		$query->select(array('c.name AS modifier'))
			->join('LEFT', '#__users AS c ON c.id = a.modified_by');
		// Filter by search
		$search = $this->getState('filter.search');
		
		if (!empty($search))
		{	
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where('a.company_name LIKE ' . $search.' OR a.name LIKE '.$search.' OR a.id LIKE '.$search);
		}	
		// SET FILTER
		$published = $this->getState('filter.published');
		if ($published != "")
		{
			$query->where('a.published = ' . $db->quote($db->escape($published)));
		}
		// SET FILTER
		/*$ort = $this->getState('filter.ort');
		if ($ort != "")
		{
			$query->where('a.ort = ' . $db->quote($db->escape($ort)));
		}*/
		
		// Add list oredring and list direction to SQL query
		$sort = $this->getState('list.ordering', 'id');
		$order = $this->getState('list.direction', 'ASC');
		$query->order($db->escape($sort).' '.$db->escape($order));
		return $query;
	}
	public function getAuthors()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text')
			->from('#__users AS u')
			->join('INNER', '#__inclure_druckgruppen AS a ON a.created_by = u.id')
			->group('u.id, u.name')
			->order('u.name');

		// Setup the query
		$db->setQuery($query);

		// Return the result
		return $db->loadObjectList();
	}
	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   12.2
	 */
	public function getItems()
	{
		if ($items = parent::getItems()) {
			$db = JFactory::getDbo();
			foreach($items as $item):
				// Load Admins
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
				$query->where('a.customerID = '.$item->id);
				$query->order('a.last_name asc');
				$db->setQuery($query);
				$item->customer_administratoren = $db->loadObjectList();
				// Load Contacts
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_contacts','a'));
				$query->where('a.customerID = '.$item->id);
				$query->where('a.type = 1');
				$query->order('a.last_name asc');
				$db->setQuery($query);
				$item->customer_contacts = $db->loadObjectList();
			endforeach;
		}

		return $items;
	}

}