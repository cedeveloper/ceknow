<?php

/** ceLearning 29.06.2020
 *	HelperClass TESTS
 *	Used to load all quizzes, which are available for
 *  the currently logged in user. SuperUser has full access on all quizzes.
 *  Trainers has only acces on their own quizzes or on approved quizzes.
 * 	Developer: Torsten Scheel
 */

defined('_JEXEC') or die();

class StageresultHelper {

   function getResult($resultID) {
      $input      = JFactory::getApplication()->input;
      if($input->get('rID', '', 'INT')):
         $resultID   = $input->get('rID', 0, 'INT');
      endif;
      $user       = JFactory::getUser();
      $db      = JFactory::getDbo();
      if($resultID):
         // Load the result of the quizz, if exist
         $query      = $db->getQuery(true);
         $query->select(array('
            a.*,
            a.id as theResultID,
            b.id as resultPositionID,
            b.questionID
            '));
           $query->from($db->quoteName('#__jclassroom_theresults','a'));
           $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
           $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('b.questionID') . ' = ' . $db->quoteName('c.id') . ')');
         $query->where($db->quoteName('a.id').' = '.$db->quote($resultID));
         $query->group('b.questionID');
         $query->order('c.ordering');
         $db->setQuery($query);
         $questions  = $db->loadObjectList();
         $quizzID    = $questions[0]->quizzID;
         $theResultID= $questions[0]->theResultID;
      else:
         $quizzID = $input->get('qid', 0, 'INT');
      endif;

      //Load the quizz
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quizz      = JTable::getInstance('Unit','JclassroomTable',array());
      $quizz->load($quizzID);
      $html       = false;
      if($quizz->type == 0):
         $html .= '<p>Auswertung für Quizz</p>';
         $html .= $this->getQuizzResult($quizz->id);
      endif;
      if($quizz->type == 1):
         $html .= '<p>Auswertung für Audit</p>';
         if($questions):
            $html .= $this->getAuditResult($quizz, $theResultID);
         endif;
      endif;

      
      return $html;
   }
   function getQuizzResult($quizzID) {
      $session    = JFactory::getSession();
      $cart       = $session->get('quizzes');
      /*echo '<pre>';
      print_r($cart);*/
      // Load the quizz
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quizz      = JTable::getInstance('Unit','JclassroomTable',array());
      $quizz->load($quizzID);
      $counter    = 1;
      $points  = 0;
      $maxPoints  = 0;
      $html = '';
      $html .= '<table class="table table-striped">';
      $html .= '<thead>';
      $html .= '<tr>';
      $html .= '<th class="p-0" width="50%">Frage</th>';
      $html .= '<th class="p-0" width="50%">';
      //if($quizz->type != 1 && $quizz->calculate == 2):
      $html .= '<table width="100%">';
      $html .= '<tr class="bg-white">';
      $html .= '<th class="p-0" style="border: none;" width="50%" class="text-center">Antwortmöglichkeiten</th>';
      $html .= '<th class="p-0" style="border: none;" width="50%" class="text-center">Ihre Antworten</th>';
      $html .= '</tr>';
      $html .= '</table>';
      $html .= '</th>';
      //endif;
      //$html .= '<th width="30%" class="text-center">Ihre Antwort</th>';
      if($quizz->calculate == 2):
         $html .= '<th class="p-0" width="10%" class="text-center">Punkte</th>';
      endif;
      $html .= '</tr>';
      $html .= '</thead>';
      //Load the quizzPositions 
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
      $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
      $query->where($db->quoteName('a.type').' <> '.$db->quote(1));
      $query->order('a.ordering asc');
      $db->setQuery($query);
      $quizzPositions   = $db->loadObjectList();
      if($quizzPositions):
         $html .= '<tbody>';
         foreach($quizzPositions as $quizzPosition):
            $colspan    = 1;
            $theBG      = '';
            if($quizzPosition->type == 14):
               $theBG = 'style="background-color: lightskyblue;"';
               if($quizz->calculate == 2):
                  $colspan = 4;
               else:
                  $colspan = 3;
               endif;
            endif;
            $maxPoints += $quizzPosition->points;
            // Questiontitle for all questiontypes
            $html .= '<tr>';
            $html .= '<td '.$theBG.' colspan="'.$colspan.'"><h5><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
            
            switch($quizzPosition->type):
               case '3':
                  // WWM-Fragen
                  $return = $this->loadType3($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
                  $html .= $return['html'];
                  $points += $return['points'];
                  break;
               case '4':
                  // Smiley-Fragen
                  $return = $this->loadType4($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
                  $html .= $return['html'];
                  $points += $return['points'];
                  break;
               case '5':
                  // Ja/Nein-Fragen
                  $return = $this->loadType5($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
                  $html .= $return['html'];
                  $points += $return['points'];
                  break;
               case '10':
                  // Textfeld-Fragen
                  $return = $this->loadType10($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
                  $html .= $return['html'];
                  $points += $return['points'];
                  break;
               case '11':
                  // Textarea-Fragen
                  $return = $this->loadType11($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
                  $html .= $return['html'];
                  $points += $return['points'];
                  break;
               case '12':
                  // Auswahlliste-Fragen
                  $return = $this->loadType12($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
                  $html .= $return['html'];
                  $points += $return['points'];
                  break;
               case '13':
                  // Checkbox-Fragen
                  $return = $this->loadType13($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
                  $html .= $return['html'];
                  $points += $return['points'];
                  break;
            endswitch;
            $html .= '</tr>';
            $counter++;
         endforeach;
         $html .= '</tbody>';
      endif;
      if($quizz->calculate == 2):
         $html .= '<tfoot>';
         $html .= '<tr>';
         $html .= '<td><b>Erreichte Gesamtpunktzahl</b></td>';
         $html .= '<td colspan="1"></td>';
         $html .= '<td style="text-align: center;"><b><span style="color: forestgreen;">'.$points.'</span>/<span style="color: steelblue;">'.$maxPoints.'</span></b></td>';
         $html .= '</tr>';
         $html .= '<tr>';
         $html .= '<td><b>Dies entspricht einem Score von</b></td>';
         $html .= '<td colspan="1"></td>';
         $score = 0;
         if($points && $maxPoints):
            $score = round($points * 100 / $maxPoints);
         endif;
         $html .= '<td style="text-align: center;"><b>'.$score.'%</b></td>';
         $html .= '</tr>';
         $html .= '</tfoot>';
      endif;
      $html .= '</table>';

      $session->clear('quizzes');
      $session->clear('answers');
      return $html;
   }
   // for WWM-Fragen
   function loadType3($questionID, $cart, $points, $calculate) {
      $html = '';
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $answers = $db->loadObjectList();
      //Count correct answers
      $query   = $db->getQuery(true);
      $query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
      $db->setQuery($query);
      $correctSoll   = $db->loadResult();
      $correctIst    = 0;
      if($answers):
         $html .= '<td style="padding:5px;">';
         $html .= '<table style="width: 100%;">';
         foreach($answers as $answer):
            if($answer->correct == 1):
               $bgS = 'bg-success';
            else:
               $bgS = 'bg-danger';
            endif;
            $html .= '<tr>';
            $html .= '<td width="50%"style="padding: 3px;"><span class="d-flex p-1 d-block text-white  badge '.$bgS.'" style="justify-content: left;">'.$answer->title.'</span></td>';
            $setCorrect = $answer->correct;
            $theTD      = '<td width="50%"></td>';
            $theAnswer  = '';
            foreach($cart as $part):
               $eparts  = $part['content'];
               if($eparts):
                  foreach($eparts as $epart):
                     $EPS = explode('_', $epart);
                     if($EPS[0] == $answer->id):
                        if($answer->correct == 1):
                           $bgS = 'bg-success';
                           $correctIst++;
                           $thePoints += $points;
                        else: 
                           $bgS = 'bg-danger';
                        endif;
                        $theTD = '<td width="50%" style="padding: 3px;"><span class="d-flex p-1 d-block text-white badge '.$bgS.'" style="justify-content: left;">'.$answer->title.' '.$theID.'</span></td>';
                     endif;
                  endforeach;
               endif;
            endforeach;
            $html .= $theTD;
         endforeach;
         $html .= '</tr>';
         $html .= '</table>';
         $html .= '</td>';
         if($correctSoll > $correctIst):
            $thePoints = ($correctIst * $points / $correctSoll);
         endif;
         if($correctSoll == $correctIst):
            $thePoints = $points;
         endif;
         if($calculate == 2):
            $html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
         endif;
      endif;
      
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   // for Smiley-Fragen
   function loadType4($questionID, $cart, $points, $calculate) {
      $thePoints = 0;
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);
      $html = '';
      $db   = JFactory::getDbo();
      $html .= '<td style="padding:5px;">';
      $html .= '<table style="width: 100%;">';
      $html .= '<tr>';
      $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: forestgreen;justify-content: center;">Sehr gut</span></td>';
      $html .= '</tr>';
      $html .= '<tr>';
      $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: yellowgreen;justify-content: center;">Gut</span></td>';
      $html .= '</tr>';
      $html .= '<tr>';
      $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: gold;justify-content: center;">Nicht schlecht</span></td>';
      $html .= '</tr>';
      $html .= '<tr>';
      $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: red; justify-content: center;">Schlecht</span></td>';
      $html .= '</tr>';
      $html .= '<tr>';
      $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: firebrick;justify-content: center;">Sehr schlecht</span></td>';
      $html .= '</tr>';
      $html .= '</table>';
      $html .= '</td>';
      //if cart is not empty
      if($cart):
         $html .= '<td style="padding:5px;">';
         $html .= '<table style="width: 100%;">';
         $html .= '<tr>';
         foreach($cart as $part):
            if($part['questionID'] == $questionID):
               $content = explode('_', $part['content'][0]);
               if($content[0] == '5'):
                  $AT = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: forestgreen;justify-content: center;">Sehr gut</span></td>';
               else:
                  $AT = '<td style="height: 27px;"></td>';
               endif;
            endif;
         endforeach;
         $html .= $AT;
         $html .= '</tr>';
         $html .= '<tr>';
         foreach($cart as $part):
            if($part['questionID'] == $questionID):
               $content = explode('_', $part['content'][0]);
               if($content[0] == '4'):
                  $AT = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: yellowgreen;justify-content: center;">Gut</span></td>';
               else:
                  $AT = '<td style="height: 27px;"></td>';
               endif;
            endif;
         endforeach;
         $html .= $AT;
         $html .= '</tr>';
         $html .= '<tr>';
         foreach($cart as $part):
            if($part['questionID'] == $questionID):
               $content = explode('_', $part['content'][0]);
               if($content[0] == '3'):
                  $AT = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: gold;justify-content: center;">Nicht schlecht</span></td>';
               else:
                  $AT = '<td style="height: 27px;"></td>';
               endif;
            endif;
         endforeach;
         $html .= $AT;
         $html .= '</tr>';
         $html .= '<tr>';
         foreach($cart as $part):
            if($part['questionID'] == $questionID):
               $content = explode('_', $part['content'][0]);
               if($content[0] == '2'):
                  $AT = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: red;justify-content: center;">Schlecht</span></td>';
               else:
                  $AT = '<td style="height: 27px;"></td>';
               endif;
            endif;
         endforeach;
         $html .= $AT;
         $html .= '</tr>';
         $html .= '<tr>';
         foreach($cart as $part):
            if($part['questionID'] == $questionID):
               $content = explode('_', $part['content'][0]);
               if($content[0] == '1'):
                  $AT = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: firebrick;justify-content: center;">Sehr schlecht</span></td>';
               else:
                  $AT = '<td style="height: 27px;"></td>';
               endif;
            endif;
         endforeach;
         $html .= $AT;
         $html .= '</tr>';
         $html .= '</table>';
         $html .= '</td>';
         if($calculate == 2):
            $html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
         endif;
      endif;
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   // for Ja/Nein-Fragen
   function loadType5($questionID, $cart, $points, $calculate) {
      $thePoints = 0;
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);
      if($quest->correctAnswers == 0):
         $bgsJ = 'bg-success';
         $bgsN = 'bg-danger';
      else:
         $bgsJ = 'bg-danger';
         $bgsN = 'bg-success';
      endif;
      $html = '';
      $db   = JFactory::getDbo();
      $html .= '<td style="padding:5px;">';
      $html .= '<table style="width: 100%;">';
      $html .= '<tr>';
      $html .= '<td width="50%" style="padding: 3px;"><span class="d-flex p-1 d-block text-white badge '.$bgsJ.'" style="justify-content: left;">Ja</span></td>';
      foreach($cart as $part):
         if($part['questionID'] == $questionID):
            $content = explode('_', $part['content'][0]);
            if($content[1] == 'answerJ'):
               $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: left;">Ja</span></td>';
               if($quest->correctAnswers == 0):
                  $thePoints = $points;
               endif;
            else:
               $html .= '<td></td>';
            endif;
         endif;
      endforeach;
      $html .= '</tr>';
      $html .= '<tr>';
      $html .= '<td width="50%" style="padding: 3px;"><span class="d-flex p-1 d-block text-white badge '.$bgsN.'" style="justify-content: left;">Nein</span></td>';
      foreach($cart as $part):
         if($part['questionID'] == $questionID):
            $content = explode('_', $part['content'][0]);
            if($content[1] == 'answerN'):
               $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: left;">Nein</span></td>';
               if($quest->correctAnswers == 1):
                  $thePoints = $points;
               endif;
            else:
               $html .= '<td></td>';
            endif;
         endif;
      endforeach;
      $html .= '</tr>';
      $html .= '</table>';
      $html .= '</td>';
      if($correctSoll > $correctIst):
         $thePoints = ($correctIst * $points / $correctSoll);
      endif;
      if($calculate == 2):
         $html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
      endif;

      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   // for Textfeld-Fragen
   function loadType10($questionID, $cart, $points, $calculate) {
      $thePoints = 0;
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);
      if($quest->correctAnswers == 0):
         $bgsJ = 'bg-success';
         $bgsN = 'bg-danger';
      else:
         $bgsJ = 'bg-danger';
         $bgsN = 'bg-success';
      endif;
      $html = '';
      $db   = JFactory::getDbo();
      $html .= '<td style="padding:5px;">';
      $html .= '<table style="width: 100%;">';
      $html .= '<tr>';
      $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
      $html .= '</tr>';
      $html .= '<tr>';
      $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
      $html .= '</tr>';
      $html .= '</table>';
      $html .= '</td>';
      //if cart is not empty
      if($cart):
         $html .= '<td style="padding:5px;">';
         $html .= '<table style="width: 100%;">';
         foreach($cart as $part):
            if($part['questionID'] == $questionID):
               $content = explode('_', $part['content'][0]);
               if($content[1] == 'answerJ'):
                  $ATJ = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
                  $ATN = '<td style="height: 27px;"></td>';
                  if($quest->correctAnswers == 0):
                     $thePoints = $points;
                  endif;
               endif;
               if($content[1] == 'answerN'):
                  $ATJ = '<td style="height: 27px;"></td>';
                  $ATN = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
                  if($quest->correctAnswers == 1):
                     $thePoints = $points;
                  endif;
               endif;
            endif;
         endforeach;
         $html .= '<tr>';
         $html .= $ATJ;
         $html .= '</tr>';
         $html .= '<tr>';
         $html .= $ATN;
         $html .= '</tr>';
         $html .= '</table>';
         $html .= '</td>';
         if($calculate == 2):
            $html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
         endif;
      endif;
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   // for Textarea-Fragen
   function loadType11($questionID, $cart, $points, $calculate) {
      $thePoints = 0;
      $html = '';
      $db   = JFactory::getDbo();
      $html .= '<td style="padding:5px;">';
      $html .= '</td>';
      //if cart is not empty
      if($cart):
         $html .= '<td style="padding:5px;">';
         foreach($cart as $part):
            if($part['questionID'] == $questionID):
               $content = explode('_', $part['content'][0]);
               if($content[1] != ''):
                  $ATJ = $content[1];
               else:
                  $ATJ = '';
               endif;
            endif;
         endforeach;
         $html .= $ATJ;
         $html .= '</td>';
         if($calculate == 2):
            $html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
         endif;
      endif;
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   // for Auswahlliste-Fragen
   function loadType12($questionID, $cart, $points, $calculate) {
      $html = '';
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $answers = $db->loadObjectList();
      //Count correct answers
      $query   = $db->getQuery(true);
      $query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
      $db->setQuery($query);
      $correctSoll   = $db->loadResult();
      $correctIst    = 0;
      if($answers):
         $html .= '<td style="padding:5px;">';
         $html .= '<table style="width: 100%;">';
         foreach($answers as $answer):
            if($answer->correct == 1):
               $bgS = 'bg-success';
            else:
               $bgS = 'bg-danger';
            endif;
            $html .= '<tr>';
            $html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$answer->title.'</span></td>';
            $html .= '</tr>';
         endforeach;
         $html .= '</table>';
         $html .= '</td>';
      endif;
      //if cart is not empty
      if($cart):
         $html .= '<td style="padding:5px;">';
         $html .= '<table style="width: 100%;">';
         // 
         foreach($answers as $answer):
            $setCorrect = $answer->correct;
            $theTD      = '<td style="height: 27px;"></td>';
            $theAnswer  = '';
            foreach($cart as $part):
               $eparts  = $part['content'];
               if($eparts):
                  foreach($eparts as $epart):
                     $EPS = explode('_', $epart);
                     if($EPS[0] == $answer->id):
                        if($answer->correct == 1):
                           $bgS = 'bg-success';
                           $correctIst++;
                           $thePoints += $points;
                        else: 
                           $bgS = 'bg-danger';
                        endif;
                        $theTD = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$EPS[1].' '.$theID.'</span></td>';
                     endif;
                  endforeach;
               endif;
            endforeach;
            $html .= '<tr>';
            $html .= $theTD;
            $html .= '</tr>';
         endforeach;
         $html .= '</table>';
         $html .= '</td>';
         if($correctSoll > $correctIst):
            $thePoints = ($correctIst * $points / $correctSoll);
         endif;
         if($calculate == 2):
            $html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
         endif;
      endif;
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   // for Checkbox-Fragen
   function loadType13($questionID, $cart, $points, $calculate) {
      $html = '';
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $answers = $db->loadObjectList();
      //Count correct answers
      $query   = $db->getQuery(true);
      $query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
      $db->setQuery($query);
      $correctSoll   = $db->loadResult();
      $correctIst    = 0;
      if($answers):
         $html .= '<td style="padding:5px;">';
         $html .= '<table style="width: 100%;">';
         foreach($answers as $answer):
            if($answer->correct == 1):
               $bgS = 'bg-success';
            else:
               $bgS = 'bg-danger';
            endif;
            $html .= '<tr>';
            $html .= '<td width="50%" style="padding: 3px;"><span class="d-flex p-1 d-block text-white badge '.$bgS.'" style="justify-content: left;">'.$answer->title.'</span></td>';
            $setCorrect = $answer->correct;
            $theTD      = '<td style="height: 27px;"></td>';
            $theAnswer  = '';
            foreach($cart as $part):
               $eparts  = $part['content'];
               if($eparts):
                  foreach($eparts as $epart):
                     $EPS = explode('_', $epart);
                     if($EPS[0] == $answer->id):
                        if($answer->correct == 1):
                           $bgS = 'bg-success';
                           $correctIst++;
                           //$thePoints += $points;
                        else: 
                           $bgS = 'bg-danger';
                        endif;
                        $theTD = '<td width="50%" style="padding: 3px;"><span class="d-flex p-1 d-block text-white badge '.$bgS.'" style="justify-content: left;">'.$answer->title.' '.$theID.'</span></td>';
                     endif;
                  endforeach;
               endif;
            endforeach;
            $html .= $theTD;

            $html .= '</tr>';
         endforeach;
         $html .= '</table>';
         $html .= '</td>';
      endif;
      if($correctSoll > $correctIst):
         $thePoints = ($correctIst * $points / $correctSoll);
      endif;
      if($correctSoll == $correctIst):
         $thePoints = $points;
      endif;
      if($calculate == 2):
         $html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
      endif;
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }


   function getAuditResult($quizz, $theResultID) {
      echo $quizz->id;
      $html = '';
      $theScoreMax   = 0;
      $theScorePoints = 0;
      // LOAD the textfields
      $db      = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
      $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizz->id));
      $query->where($db->quoteName('a.type').' = 14');
      $query->group('a.testfield');
      $query->order('a.groupOrdering ASC');
      $db->setQuery($query);
      $testfields    = $db->loadObjectList();
      echo '<pre>';
      print_r($testfields);
      echo '</pre>';
      if($testfields):
         foreach($testfields as $testfield):
            $html .= '<h1 style="font-size: 24px;"><span class="badge bg-success text-white mr-1">'.$testfield->title.'</span></h1>';
            $html .= $testfield->content;
            $query   = $db->getQuery(true);
            $query->select(array('a.*'));
              $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
            $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizz->id));
            $query->where($db->quoteName('a.testfield').' = '.$db->quote($testfield->title));
            $query->where($db->quoteName('a.type').' <> '.$db->quote(14));
            //$query->group('a.theme');
            $db->setQuery($query);
            $questions  = $db->loadObjectList();
            if($questions):
               $html .= '<table class="table table-striped">';
               $html .= '<thead>';
               $html .= '<tr>';
               $html .= '<th>Frage</th>';
               if($quizz->type != 1 && $quizz->calculate == 2):
                  $html .= '<th>Richtige Antwort</th>';
               endif;
               $html .= '<th>Ihre Antwort</th>';
               if($quizz->calculate == 2):
                  $html .= '<th>Punkte</th>';
               endif;
               $html .= '</tr>';
               $html .= '</thead>';
               $html .= '<tbody>';
               $maxPoints  = 0;
               $points  = 0;
               foreach($questions as $question):
                  $maxPoints += $question->points;
                  $theScoreMax += $question->points;
                  $html .= '<tr>';
                  //Get the Question
                  JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
                  $result  = JTable::getInstance('Quizz_results','JclassroomTable',array());
                  $load       = array('theResultID' => $theResultID,'questionID' => $question->id);
                  $result->load($load);
                  $html .= '<td><h5 style="font-size: 16px;"><b>'.$question->title.'</b></h5>'.$question->content.'<h2 style="font-size: 18px;"><span class="badge bg-primary text-white">'.$question->theme.'</span></h2></td>';
                  if($result->content == "A"):
                     $html .= '<td><span class="card bg-success p-1 text-white">'.$question->infotextPositiv.'</span></td>';
                     $html .= '<td class="text-center">'.$question->points.'</td>';
                     $points += $question->points;
                  endif;
                  if($result->content == "AA"):
                     $html .= '<td>';
                     $html .= '<span class="card bg-warning p-1 text-dark">'.$question->infotextPositiv.'</span>';
                     $html .= '<small style="font-weight: bold;font-style: italic;">'.$result->exeptionText.'</small>';
                     $html .= '</td>';
                     if($result->exeptionPoints != 0):
                        $thePoints = $result->exeptionPoints;
                     else:
                        $thePoints = $question->points;
                     endif; 
                     $html .= '<td class="text-center">'.$thePoints.'</td>';
                     $points += $thePoints;
                  endif;
                  if($result->content == "N"):
                     $html .= '<td><span class="card bg-danger p-1 text-white">'.$question->infotextNegativ.'</span></td>';
                     $html .= '<td class="text-center">0</td>';
                     $points += 0;
                  endif;
                  $html .'</tr>';
               endforeach;
               $html .= '</tbody>';
               if($quizz->calculate == 2):
                  $theScorePoints += $points;
                  $html .= '<tfoot>';
                  $html .= '<tr>';
                  $html .= '<td><b>Ihre erreicht Punktzahl im Thema '.$question->theme.' (von maximal '.$maxPoints.')</b></td>';
                  if($quizz->type != 1 && $quizz->calculate == 2):
                     $html .= '<td></td>';
                     $html .= '<td></td>';
                  else:
                     $html .= '<td></td>';
                  endif;
                  $html .= '<td class="text-center"><b>'.$points.'</b></td>';
                  $html .= '</tr>';
                  $html .= '<tr>';
                  $html .= '<td><b>Dies entspricht einem Score von</b></td>';
                  if($quizz->type != 1 && $quizz->calculate == 2):
                     $html .= '<td></td>';
                     $html .= '<td></td>';
                  else:
                     $html .= '<td></td>';
                  endif;
                  $score = $points * 100/ $maxPoints;
                  $html .= '<td class="text-center"><b>'.round($score,2).'%</b></td>';
                  $html .= '</tr>';
                  $html .= '</tfoot>';
               endif;
               $html .= '</table>';
            endif;
         endforeach;
      endif;
      if($quizz->calculate == 2):
         $html .= '<table class="table table-striped">';
            $html .= '<tfoot>';
            $html .= '<tr>';
            $html .= '<td><b>Gesamtscore</b></td>';f;
            $score = $theScorePoints * 100/ $theScoreMax;
            $html .= '<td class="text-right"><b style="font-size: 32px;" class="badge bg-success text-white">'.round($score,2).'%</b></td>';
            $html .= '</tr>';
            $html .= '</tfoot>';
         $html .= '</table>';
      endif;

      return $html;
   }
}