<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * List Model for unternehmen.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelManager_customer extends JModelList {
	
	function getCustomer() {
		$session 	= JFactory::getSession();
		$user 		= JFactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*,
			b.*,
			c.title as plan_name
		'));
        $query->from($db->quoteName('#__jclassroom_customer','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_customer_administratoren', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.customerID') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_plans', 'c') . ' ON (' . $db->quoteName('a.planID') . ' = ' . $db->quoteName('c.id') . ')');
		$query->where('a.id = '.$session->get('customerID'));
		$query->where('b.userID = '.$user->id);
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}
}
?>