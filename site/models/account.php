<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for auditor.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelAccount extends JModelAdmin
{
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_JCLASSROOM';

	/**
	 * The type alias for this content type.
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_jclassroom.account';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object    $record    A record object.
	 *
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id))
		{
			if ($record->published != -2)
			{
				return false;
			}
			

			$user = JFactory::getUser();
			return $user->authorise('core.delete', $this->typeAlias . '.' . (int) $record->id);
		}
	}		

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable    A JTable object.
	 *
	 * @return  void
	 * @since   1.6
	 */
	protected function prepareTable($table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
	}

	/**
	 * Auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('administrator');

		// Load the User state.
		$pk 	= $app->input->getInt('id');
		$user 	= JFactory::getUser();
		$pk 	= $user->id;
		$this->setState($this->getName() . '.id', $pk);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jclassroom');
		$this->setState('params', $params);
	}
	
	/**
	 * Alias for JTable::getInstance()
	 *
	 * @param   string  $type    The type (name) of the JTable class to get an instance of.
	 * @param   string  $prefix  An optional prefix for the table class name.
	 * @param   array   $config  An optional array of configuration values for the JTable object.
	 *
	 * @return  mixed    A JTable object if found or boolean false if one could not be found.
	 */
	public function getTable($type = 'User', $prefix = 'JclassroomTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	/**
	 * Method for getting the form from the model.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/fields');

		JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR.'/models/rules');		
		
		$options = array('control' => 'jform', 'load_data' => $loadData);
		$form = $this->loadForm($this->typeAlias, $this->name, $options);
		
		if(empty($form))
		{
			return false;
		}

		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  array    The default data is an empty array.
	 */
	protected function loadFormData()
	{
		$app 	= JFactory::getApplication();
		$data 	= $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());
		$user 	= JFactory::getUser();
		if(empty($data))
		{
			$data = $this->getItem($user->id);
		}
		
		return $data;
	}
	
	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		$user = JFactory::getUser();
		if (!$item = parent::getItem($user->id))
		{			
			throw new Exception('Failed to load item');
		}
		if($item->id):
			$db = JFactory::getDbo();
			$item->password 	= '';
			$session = JFactory::getSession();
			// Load Plans
	        $query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_plans','a'));
			$query->where('a.for_sale = 1');
			$query->order('id asc');
			$db->setQuery($query);
			$item->plans = $db->loadObjectList();
			switch($session->get('group')) {
				case 'superuser':
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$user 	= JTable::getInstance('User','JclassroomTable',array());
					$load 	= array('id' => $user->id);
					$user->load($load);
					/*echo $user->id;
					$item->id 			= $user->id;
					$item->tblUserID 	= $user->id;*/
					$item->first_name 	= $item->name;
					/*$item->last_name 	= '';
					$item->company 		= '';
					$item->adress 		= '';
					$item->postcode 	= $student->postcode;
					$item->city 		= $student->city;
					$item->phone 		= $student->phone;
					$item->mobile 		= $student->mobile;
					$item->fax 			= $student->fax;*/
					break;
				case 'customer':
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$customerA 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
					$load 	= array('userID' => $user->id);
					$customerA->load($load);
					$customer 	= JTable::getInstance('Customer','JclassroomTable',array());
					$load 	= array('id' => $customerA->customerID);
					$customer->load($load);
					$item->salutation 	= $customerA->salutation;
					$item->id 			= $customer->id;
					$item->customerID 	= $customer->id;
					$item->customerAdministratorID 	= $customerA->id;
					$item->tblUserID 	= $customer->userID;
					$item->customer_number 	= $customer->customer_number;
					$item->first_name 	= $customerA->first_name;
					$item->last_name 	= $customerA->last_name;
					$item->company_name = $customer->company_name;
					$item->address 		= $customer->address;
					$item->postcode 	= $customer->postcode;
					$item->city 		= $customer->city;
					// Repeats for Paymentdetails
					$item->company_repeat = $customer->company_name;
					$item->address_repeat 		= $customer->address;
					$item->postcode_repeat 	= $customer->postcode;
					$item->city_repeat 		= $customer->city;
					$item->phone 		= $customerA->phone;
					$item->mobile 		= $customerA->mobile;
					$item->fax 			= $customerA->fax;
					$item->plan_from 		= $customer->plan_from;
					$item->plan_to 			= $customer->plan_to;
					// Count Administrators
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
					$query->where('a.customerID = '.$customer->id);
					$db->setQuery($query);
					$db->execute();
					$item->count_customer_administratoren = $db->getNumRows();
					// Count Trainers
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_trainers','a'));
					$query->where('a.customerID = '.$customer->id);
					$db->setQuery($query);
					$db->execute();
					$item->count_customer_trainers = $db->getNumRows();
					// Count Learningrooms
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
					$query->where('a.customerID = '.$customer->id);
					$db->setQuery($query);
					$db->execute();
					$item->count_customer_learningrooms = $db->getNumRows();
					// Count Modules
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
					$query->where('a.customerID = '.$customer->id);
					$db->setQuery($query);
					$db->execute();
					$item->count_customer_modules = $db->getNumRows();
					// Count Units
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
					$query->where('a.customerID = '.$customer->id);
					$db->setQuery($query);
					$db->execute();
					$item->count_customer_units = $db->getNumRows();
					// Load Plan
					$query = $db->getQuery(true);
					$query->select(array('a.*,b.*'));
			        $query->from($db->quoteName('#__jclassroom_customer','a'));
			        $query->join('LEFT', $db->quoteName('#__jclassroom_plans', 'b') . ' ON (' . $db->quoteName('a.planID') . ' = ' . $db->quoteName('b.id') . ')');
					$query->where('a.id = '.$customer->id);
					$db->setQuery($query);
					$result 				= $db->loadObject();
					$item->plan 			= $result->planID;
					$item->planName 		= $result->title;
					$item->customer_plans 	= $result;
					// Load Administrators
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
					$query->where('a.customerID = '.$customer->id);
					$query->where('a.userID <> '.$user->id);
					$db->setQuery($query);
					$item->customer_administratoren = $db->loadObjectList();
					// Load Payment
					$query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_customer_payments','a'));
					$query->where('a.customerID = '.$customer->id);
					$db->setQuery($query);
					$result 				= $db->loadObject();
					$item->alt_invoice 		= $result->alt_invoice;
					$item->paymentAbo 		= $result->paymentAbo;
					$item->paymentID 		= $result->paymentID;
					$item->invoice_first_name 		= $result->invoice_first_name;
					$item->invoice_last_name		= $result->invoice_last_name;
					$item->invoice_function 		= $result->invoice_function;
					$item->invoice_adress 		= $result->invoice_adress;
					$item->invoice_postcode 	= $result->invoice_postcode;
					$item->invoice_city 		= $result->invoice_city;
					$item->account_owner 	= $result->account_owner;
					$item->iban 			= $result->iban;
					$item->bic 		 		= $result->bic;
					if(!$result):
						$item->valid_from 	= '';
						$item->valid_to 	= '';
					else:
						if($result->valid_from == '1970-01-01'):
							$item->valid_from 	= '';
						else:
							$item->valid_from 	= date('d.m.Y', strtotime($result->valid_from));
						endif;
						if($result->valid_to == '1970-01-01'):
							$item->valid_to 	= '';
						else:
							$item->valid_to 	= date('d.m.Y', strtotime($result->valid_to));
						endif;
					endif;
					$item->creditcard_bank 	= $result->creditcard_bank;
					$item->creditcard_number 		= $result->creditcard_number;
					$item->creditcard_check_number	= $result->creditcard_check_number;
					$item->creditcard_valid_to 		= date('d.m.Y', strtotime($result->creditcard_valid_to));
					$item->customer_payments 		= $result;
					$item->customer_number			= 'CU'.str_pad($item->id, 8, '0', STR_PAD_LEFT);
					/*if($result->invoice_title):
						$item->invoice_title 			= $result->invoice_title;
					else:
						$item->invoice_title 			= $item->company;
					endif;
					if($result->invoice_adress):
						$item->invoice_adress 			= $result->invoice_adress;
					else:
						$item->invoice_adress 			= $item->adress;
					endif;
					if($result->invoice_postcode):
						$item->invoice_postcode 			= $result->invoice_postcode;
					else:
						$item->invoice_postcode 			= $item->postcode;
					endif;
					if($result->invoice_city):
						$item->invoice_city 			= $result->invoice_city;
					else:
						$item->invoice_city 			= $item->city;
					endif;*/
					break;
				case 'trainer':
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$userD 	= JTable::getInstance('User','JclassroomTable',array());
					$load 	= array('id' => $user->id);
					$userD->load($load);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$trainer 	= JTable::getInstance('Trainer','JclassroomTable',array());
					$load 	= array('tblUserID' => $user->id);
					$trainer->load($load);
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$customer 	= JTable::getInstance('Customer','JclassroomTable',array());
					$load 	= array('id' => $trainer->customerID);
					$customer->load($load);
					$item->id 			= $userD->id;
					$item->tblUserID 	= $userD->id;
					$item->salutation 	= $trainer->salutation;
					$item->first_name 	= $trainer->first_name;
					$item->last_name 	= $trainer->last_name;
					$item->company 		= $customer->company_name;
					$item->customer_adress	= $customer->address;
					$item->customer_postcode= $customer->postcode;
					$item->customer_city 	= $customer->city;
					$item->customer_phone 	= $customer->phone;
					$item->customer_email 	= $customer->email;
					$item->adress 		= $trainer->adress;
					$item->postcode 	= $trainer->postcode;
					$item->city 		= $trainer->city;
					$item->phone 		= $trainer->phone;
					$item->mobile 		= $trainer->mobile;
					$item->fax 			= $trainer->fax;
					$item->customer_number = 'CU'.str_pad($customer->id,8,'0', STR_PAD_LEFT);
					$item->password 	= '';
					// Load Administrators
			        $query = $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
					$query->where('a.customerID = '.$customer->id);
					$db->setQuery($query);
					$item->customer_administratoren = $db->loadObjectList();
					break;
				case 'student':
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$student 	= JTable::getInstance('Student','JclassroomTable',array());
					$load 		= array('tblUserID' => $user->id);
					$student->load($load);
					$item->id 			= $student->id;
					$item->salutation 	= $student->salutation;
					$item->tblUserID 	= $student->tblUserID;
					$item->first_name 	= $student->first_name;
					$item->last_name 	= $student->last_name;
					$item->company 		= $student->company;
					$item->adress 		= $student->adress;
					$item->postcode 	= $student->postcode;
					$item->city 		= $student->city;
					$item->phone 		= $student->phone;
					$item->mobile 		= $student->mobile;
					$item->fax 			= $student->fax;
					break;
			}
		endif;
		/*$user = JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_plans/tables');
		$table = JTable::getInstance('Customer','PlansTable',array());
		$load = array('tblUserID' => $user->id);
		$table->load($load);
		$tblResellerID 	= $table->id;
		$item->postcode = ($item->postcode == 0) ? '' : $item->postcode;
		if (!$item->id)
		{
			$item->created_by = JFactory::getUser()->get('id');
			$item->modified_by = JFactory::getUser()->get('id');
			$item->newDS = 1;
			$item->currentUserID = 0;
		}
		if($item->id) {
			$item->newDS = 0;
			$item->currentUserID = $item->tblUserID;
		}
		if($tblResellerID) {
			$db = JFactory::getDbo();
	        $query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__plans_customers','a'));
			$query->where('a.id = '.$tblResellerID);;
			$db->setQuery($query);
			$result = $db->loadObject();
			$item = $result;
		}*/
		return $item;
	}
}
?>