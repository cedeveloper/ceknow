<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * List Model for unternehmen.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelManager_trainer extends JModelList {
	
	function getTrainerData() {

		$user = JFactory::getUser();
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('
			a.*,
			a.email as trainerEmail,
			b.*
		'));
        $query->from($db->quoteName('#__jclassroom_trainers','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_customer', 'b') . ' ON (' . $db->quoteName('a.customerID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where('a.tblUserID = '.$user->id);;
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}
	function getTrainerAsStudent() {

		$user = JFactory::getUser();
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('
			a.id as classroomID,
			a.title,
			c.name as trainer_name
		'));
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_classroom_students', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.classroomID') . ')');
        $query->join('LEFT', $db->quoteName('#__users', 'c') . ' ON (' . $db->quoteName('a.main_trainer') . ' = ' . $db->quoteName('c.id') . ')');
		$query->where('b.userID = '.$user->id);
		$query->group('a.id,a.title');
		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;
	}

}
?>