<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for kunde.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelEvaluate_quizz extends JModelAdmin
{
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_JCLASSROOM';

	/**
	 * The type alias for this content type.
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_jclassroom.evaluate_quizz';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object    $record    A record object.
	 *
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id))
		{
			if ($record->published != -2)
			{
				return false;
			}
			

			$user = JFactory::getUser();
			return $user->authorise('core.delete', $this->typeAlias . '.' . (int) $record->id);
		}
	}		

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable    A JTable object.
	 *
	 * @return  void
	 * @since   1.6
	 */
	protected function prepareTable($table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
		
		if ($table->published == 1 && (int) $table->publish_up == 0)
		{
			$table->publish_up = JFactory::getDate()->toSql();
		}

		if ($table->published == 1 && intval($table->publish_down) == 0)
		{
			$table->publish_down = $db->getNullDate();
		}
	}

	/**
	 * Auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('administrator');

		// Load the User state.
		$pk = $app->input->getInt('id');
		$this->setState($this->getName() . '.id', $pk);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jclassroom');
		$this->setState('params', $params);
	}
	
	/**
	 * Alias for JTable::getInstance()
	 *
	 * @param   string  $type    The type (name) of the JTable class to get an instance of.
	 * @param   string  $prefix  An optional prefix for the table class name.
	 * @param   array   $config  An optional array of configuration values for the JTable object.
	 *
	 * @return  mixed    A JTable object if found or boolean false if one could not be found.
	 */
	public function getTable($type = 'Quizz', $prefix = 'JclassroomTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	/**
	 * Method for getting the form from the model.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/fields');

		JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR.'/models/rules');		
		
		$options = array('control' => 'jform', 'load_data' => $loadData);
		$form = $this->loadForm($this->typeAlias, $this->name, $options);
		
		if(empty($form))
		{
			return false;
		}

		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  array    The default data is an empty array.
	 */
	protected function loadFormData()
	{
		$app = JFactory::getApplication();
		$data = $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());
		
		if(empty($data))
		{
			$data = $this->getItem();
		}
		
		return $data;
	}
	function getResult() {
		$input 		= JFactory::getApplication()->input;
		$publishedQuizzID = $input->get('id', 0, 'INT');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quizz 		= JTable::getInstance('Theresults','JclassroomTable',array());
		$load 		= array('publishedQuizzID' => $publishedQuizzID); 
		$quizz->load($load);
		$rID 	= $quizz->id;
		JLoader::register('StageResultHelper',JPATH_COMPONENT_SITE.'/helpers/stageResult.php');
		$result     = new StageResultHelper();
		$html       = $result->getResult($rID, 0, 0, 1, 0, $publishedQuizzID, $quizz->quizzID, 0);
		
		return $html;
	}

	function getAuditResult($quizz, $theResultID) {
		$html = '';
		$theScoreMax 	= 0;
		$theScorePoints = 0;
		// LOAD the textfields
		$db 		= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizz->id));
		$query->where($db->quoteName('a.type').' = 14');
		$query->group('a.testfield');
		$query->order('a.groupOrdering ASC');
		$db->setQuery($query);
		$testfields 	= $db->loadObjectList();
		if($testfields):
			foreach($testfields as $testfield):
				$html .= '<h1 style="font-size: 24px;"><span class="badge bg-success text-white mr-1">'.$testfield->title.'</span></h1>';
				$html .= $testfield->content;
				$query 	= $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizz->id));
				$query->where($db->quoteName('a.testfield').' = '.$db->quote($testfield->title));
				$query->where($db->quoteName('a.type').' <> '.$db->quote(14));
				//$query->group('a.theme');
				$db->setQuery($query);
				$questions 	= $db->loadObjectList();
				if($questions):
					$html .= '<table class="table table-striped">';
					$html .= '<thead>';
					$html .= '<tr>';
					$html .= '<th>Frage</th>';
					if($quizz->type != 1 && $quizz->calculate == 2):
						$html .= '<th>Richtige Antwort</th>';
					endif;
					$html .= '<th>Ihre Antwort</th>';
					if($quizz->calculate == 2):
						$html .= '<th>Punkte</th>';
					endif;
					$html .= '</tr>';
					$html .= '</thead>';
					$html .= '<tbody>';
					$maxPoints 	= 0;
					$points 	= 0;
					foreach($questions as $question):
						$maxPoints += $question->points;
						$theScoreMax += $question->points;
						$html .= '<tr>';
						//Get the Question
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$result 	= JTable::getInstance('Quizz_results','JclassroomTable',array());
						$load 		= array('theResultID' => $theResultID,'questionID' => $question->id);
						$result->load($load);
						$html .= '<td><h5 style="font-size: 16px;"><b>'.$question->title.'</b></h5>'.$question->content.'<h2 style="font-size: 18px;"><span class="badge bg-primary text-white">'.$question->theme.'</span></h2></td>';
						if($result->content == "A"):
							$html .= '<td><span class="card bg-success p-1 text-white">'.$question->infotextPositiv.'</span></td>';
							$html .= '<td class="text-center">'.$question->points.'</td>';
							$points += $question->points;
						endif;
						if($result->content == "AA"):
							$html .= '<td>';
							$html .= '<span class="card bg-warning p-1 text-dark">'.$question->infotextPositiv.'</span>';
							$html .= '<small style="font-weight: bold;font-style: italic;">'.$result->exeptionText.'</small>';
							$html .= '</td>';
							if($result->exeptionPoints != 0):
								$thePoints = $result->exeptionPoints;
							else:
								$thePoints = $question->points;
							endif; 
							$html .= '<td class="text-center">'.$thePoints.'</td>';
							$points += $thePoints;
						endif;
						if($result->content == "N"):
							$html .= '<td><span class="card bg-danger p-1 text-white">'.$question->infotextNegativ.'</span></td>';
							$html .= '<td class="text-center">0</td>';
							$points += 0;
						endif;
						$html .'</tr>';
					endforeach;
					$html .= '</tbody>';
					if($quizz->calculate == 2):
						$theScorePoints += $points;
						$html .= '<tfoot>';
						$html .= '<tr>';
						$html .= '<td><b>Ihre erreicht Punktzahl im Thema '.$question->theme.' (von maximal '.$maxPoints.')</b></td>';
						if($quizz->type != 1 && $quizz->calculate == 2):
							$html .= '<td></td>';
							$html .= '<td></td>';
						else:
							$html .= '<td></td>';
						endif;
						$html .= '<td class="text-center"><b>'.$points.'</b></td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td><b>Dies entspricht einem Score von</b></td>';
						if($quizz->type != 1 && $quizz->calculate == 2):
							$html .= '<td></td>';
							$html .= '<td></td>';
						else:
							$html .= '<td></td>';
						endif;
						$score = $points * 100/ $maxPoints;
						$html .= '<td class="text-center"><b>'.round($score,2).'%</b></td>';
						$html .= '</tr>';
						$html .= '</tfoot>';
					endif;
					$html .= '</table>';
				endif;
			endforeach;
		endif;
		if($quizz->calculate == 2):
			$html .= '<table class="table table-striped">';
				$html .= '<tfoot>';
				$html .= '<tr>';
				$html .= '<td><b>Gesamtscore</b></td>';f;
				$score = $theScorePoints * 100/ $theScoreMax;
				$html .= '<td class="text-right"><b style="font-size: 32px;" class="badge bg-success text-white">'.round($score,2).'%</b></td>';
				$html .= '</tr>';
				$html .= '</tfoot>';
			$html .= '</table>';
		endif;

		return $html;
	}
	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if (!$item = parent::getItem($pk))
		{			
			throw new Exception('Failed to load item');
		}
		if (!$item->id)
		{
		}
		if($item->id):
			$db 		= JFactory::getDbo();
			// Load the result of the quizz
			$query 		= $db->getQuery(true);
			$query->select(array('
				COUNT(a.id) as participations
			'));
	        $query->from($db->quoteName('#__jclassroom_theresults','a'));
			$query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($item->id));
			$db->setQuery($query);
			$item->participations 	= $db->loadResult();
			$item->result = $this->getResult();
			//Load the quizz
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$quizz 		= JTable::getInstance('Quizz','JclassroomTable',array());
			$quizz->load($item->id);
			$companyID = $quizz->companyID;
			$item->vorwort = $quizz->preface;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$company 		= JTable::getInstance('Company','JclassroomTable',array());
			$company->load($companyID);
			$item->companyName = $company->name;
		endif;
		
		return $item;
	}
	
	/**
	 * Increment the hit counter for the item.
	 *
	 * @param   integer  $pk  Optional primary key of the item to increment.
	 *
	 * @return  boolean  True if successful; false otherwise and internal error set.
	 */
	public function hit($pk = 0)
	{
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);

		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('kunde.id');

			$table = $this->getTable();
			$table->load($pk);
			$table->hit($pk);
		}

		return true;
	}
}
?>