<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for auditor.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelConfiguration extends JModelAdmin
{
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_JCLASSROOM';

	/**
	 * The type alias for this content type.
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_jclassroom.configuration';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object    $record    A record object.
	 *
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id))
		{
			if ($record->published != -2)
			{
				return false;
			}
			

			$user = JFactory::getUser();
			return $user->authorise('core.delete', $this->typeAlias . '.' . (int) $record->id);
		}
	}		

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable    A JTable object.
	 *
	 * @return  void
	 * @since   1.6
	 */
	protected function prepareTable($table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
	}

	/**
	 * Auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('administrator');

		// Load the User state.
		$pk 	= $app->input->getInt('id');
		$user 	= JFactory::getUser();
		$pk 	= $user->id;
		$this->setState($this->getName() . '.id', $pk);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jclassroom');
		$this->setState('params', $params);
	}
	
	/**
	 * Alias for JTable::getInstance()
	 *
	 * @param   string  $type    The type (name) of the JTable class to get an instance of.
	 * @param   string  $prefix  An optional prefix for the table class name.
	 * @param   array   $config  An optional array of configuration values for the JTable object.
	 *
	 * @return  mixed    A JTable object if found or boolean false if one could not be found.
	 */
	public function getTable($type = 'Configuration', $prefix = 'JclassroomTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	/**
	 * Method for getting the form from the model.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/fields');

		JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR.'/models/rules');		
		
		$options = array('control' => 'jform', 'load_data' => $loadData);
		$form = $this->loadForm($this->typeAlias, $this->name, $options);
		
		if(empty($form))
		{
			return false;
		}

		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  array    The default data is an empty array.
	 */
	protected function loadFormData()
	{
		$app 	= JFactory::getApplication();
		$data 	= $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());
		$user 	= JFactory::getUser();
		if(empty($data))
		{
			$data = $this->getItem($user->id);
		}
		
		return $data;
	}
	
	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		$user 		= JFactory::getUser();
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		// LOAD TESTMODE
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'testmode');
		$table->load($load);
		$item->testmode = $table->wert;
		// LOAD TESTMODE EMAIL
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'testmode_email');
		$table->load($load);
		$item->testmode_email = $table->wert;
		// LOAD SYSTEMVERSION
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'systemversion');
		$table->load($load);
		$item->systemversion = $table->wert;
		// LOAD SYSTEMDATE
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'systemdate');
		$table->load($load);
		$item->systemdate = $table->wert;
		// LOAD SYSTEMNAME
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'systemname');
		$table->load($load);
		$item->systemname = $table->wert;
		// LOAD SYSTEMLINK
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'systemLink');
		$table->load($load);
		$item->systemLink = $table->wert;
		// LOAD EMAILSENDER
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailSender', 'customerID' => $customerID);
		$table->load($load);
		$item->emailSender = $table->wert;
		// LOAD EMAILSENDERNAME
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailSenderName','customerID' => $customerID);
		$table->load($load);
		$item->emailSenderName = $table->wert;
		// LOAD EMAILBODY
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailBody','customerID' => $customerID);
		$table->load($load);
		$item->emailBody = $table->wert;
		// LOAD EMAILSTYLES
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailStyles','customerID' => $customerID);
		$table->load($load);
		$item->emailStyles = $table->wert;
		// LOAD EMAILSCRIPTS
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'emailScripts','customerID' => $customerID);
		$table->load($load);
		$item->emailScripts = $table->wert;
		// LOAD EMAILSCRIPTS
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'description_confirm','customerID' => $customerID);
		$table->load($load);
		$item->description_confirm_mail = $table->wert;
		// LOAD EMAILSCRIPTS
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'subject_confirm','customerID' => $customerID);
		$table->load($load);
		$item->subject_confirm_mail = $table->wert;
		// LOAD EMAILSCRIPTS
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'text_confirm','customerID' => $customerID);
		$table->load($load);
		$item->text_confirm_mail = $table->wert;
		// LOAD EMAILSCRIPTS
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'description_plan','customerID' => $customerID);
		$table->load($load);
		$item->description_plan_mail = $table->wert;
		// LOAD EMAILSCRIPTS
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'subject_plan','customerID' => $customerID);
		$table->load($load);
		$item->subject_plan_mail = $table->wert;
		// LOAD EMAILSCRIPTS
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'text_plan','customerID' => $customerID);
		$table->load($load);
		$item->text_plan_mail = $table->wert;
		// LOAD ASP
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'asp_text','customerID' => $customerID);
		$table->load($load);
		$item->asp = $table->wert;


		return $item;
	}
}
?>