<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * List Model for unternehmen.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelManager extends JModelList {

	function getLogfiles() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.id,
			a.wert,
			a.created
		'));
        $query->from($db->quoteName('#__jclassroom_logfiles','a'));
        $query->join('LEFT', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('b.id') . ' = ' . $db->quoteName('a.userID') . ')');
        $query->where('a.quitt_by = 0');
        $query->order('a.created DESC');
		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;
	}
}
?>