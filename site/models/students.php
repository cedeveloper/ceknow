<?php

/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class JclassroomModelStudents extends JModelList {
	
	public function __construct($config = array()) {
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'a.ort', 'ort',
				'a.name', 'name',
				'a.published', 'published'
			);
		}
		parent::__construct($config);
	}
	
	protected function populateState($ordering = 'id', $direction = 'ASC') {
		// Get the Application
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		
		// Set filter state for search
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
	
		// Set filter state for bereich
		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		// Set filter state for bereich
		$company = $this->getUserStateFromRequest($this->context.'.filter.companyID', 'filter_companyID', '', 'integer');
		$this->setState('filter.companyID', $company);
		$classroomID = $this->getUserStateFromRequest($this->context.'.filter.classroomID', 'filter_classroomID', '', 'integer');
		$this->setState('filter.classroomID', $classroomID);
		// Load the parameters.
		$params = JComponentHelper::getParams('com_inclure');
		$active = $menu->getActive();
		empty($active) ? null : $params->merge($active->params);
		$this->setState('params', $params);

		// List state information.
		parent::populateState($ordering, $direction);
	}

	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.category_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery() {
		$session 	= JFactory::getSession();
		$customerID = $session->get('customerID');
		$userID 	= JFactory::getUser()->id;
		// Get database object
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('
			a.id,
			a.customerID,
			a.first_name,
			a.last_name,
			a.adress,
			a.postcode,
			a.city,
			a.phone,
			a.mobile,
			a.email,
			a.web,
			a.customer_number,
			a.logo,
			a.tblUserID,
			a.published,
			b.id as companyID,
			b.name as company_name,
			GROUP_CONCAT(CONCAT_WS(",", d.id, d.title,d.showto,e.rightType,d.created_by,e.userID)SEPARATOR "//") as classroomID
		')->from('#__jclassroom_students AS a');
		$query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'b') . ' ON (' . $db->quoteName('a.companyID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.tblUserID') . ' = ' . $db->quoteName('c.userID') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_classrooms', 'd') . ' ON (' . $db->quoteName('c.classroomID') . ' = ' . $db->quoteName('d.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_classroom_rights', 'e') . ' ON (' . $db->quoteName('d.id') . ' = ' . $db->quoteName('e.classroomID') . ')');
		if($customerID && $customerID != 0):
			$query->where('a.customerID = ' . $db->quote($customerID));
		endif;
		// Filter by search
		$search = $this->getState('filter.search');
		if (!empty($search))
		{	
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where('a.last_name LIKE ' . $search.' OR a.customer_number LIKE '.$search .' OR b.name LIKE '.$search);
		}	
		// SET FILTER
		$published = $this->getState('filter.published');
		if ($published != "")
		{
			$query->where('a.published = ' . $db->quote($db->escape($published)));
		}
		// SET FILTER
		$company = $this->getState('filter.companyID');
		if ($company != "")
		{
			$query->where('a.companyID = ' . $db->quote($db->escape($company)));
		}
		$classroomID = $this->getState('filter.classroomID');
		if ($classroomID != "")
		{
			$query->where('d.id = ' . $db->quote($db->escape($classroomID)));
		}
		//$query->where('a.tblUserID = 1248');
		$query->group('a.id');
		// Add list oredring and list direction to SQL query
		$sort = $this->getState('list.ordering', 'id');
		$order = $this->getState('list.direction', 'ASC');
		$query->order($db->escape($sort).' '.$db->escape($order));
		return $query;
	}
	public function getAuthors()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text')
			->from('#__users AS u')
			->join('INNER', '#__inclure_druckgruppen AS a ON a.created_by = u.id')
			->group('u.id, u.name')
			->order('u.name');

		// Setup the query
		$db->setQuery($query);

		// Return the result
		return $db->loadObjectList();
	}
	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   12.2
	 */
	public function getItems()
	{
		if ($items = parent::getItems()) {
			$session 	= JFactory::getSession();
			$user 		= JFactory::getUser();
			//Do any procesing on fields here if needed
			foreach($items as $item):
				if($item->postcode == 0):
					$item->postcode = '';
				endif;
			endforeach;
			foreach($items as $item):
				$groups = JAccess::getGroupsByUser($item->tblUserID);
		        // Students
		        if(in_array(11,$groups)) {
		        	$item->usergroup = '<span class="badge badge-success text-white">Teilnehmer</span>';
		        }
		        // Reseller
		        if(in_array(12,$groups)) {
		            $item->usergroup = '<span class="badge badge-secondary text-white">Kunde</span>';
		        }
		        // Trainer
		        if(in_array(10,$groups)) {
		            $item->usergroup = '<span class="badge badge-danger text-white">Trainer</span>';
		        }
		        // Superuser
		        if(in_array(8,$groups)) {
		            $item->usergroup = '<span class="badge badge-primary text-white">Systemadministrator</span>';
		        }
			endforeach;
		}
		return $items;
	}

}