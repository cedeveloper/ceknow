<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for auditor.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelClassroom extends JModelAdmin
{
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_JCLASSROOM';

	/**
	 * The type alias for this content type.
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_jclassroom.classroom';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object    $record    A record object.
	 *
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id))
		{
			if ($record->published != -2)
			{
				return false;
			}
			

			$user = JFactory::getUser();
			return $user->authorise('core.delete', $this->typeAlias . '.' . (int) $record->id);
		}
	}		

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable    A JTable object.
	 *
	 * @return  void
	 * @since   1.6
	 */
	protected function prepareTable($table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
	}

	/**
	 * Auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('administrator');

		// Load the User state.
		$pk = $app->input->getInt('id');
		$this->setState($this->getName() . '.id', $pk);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jclassroom');
		$this->setState('params', $params);
	}
	
	/**
	 * Alias for JTable::getInstance()
	 *
	 * @param   string  $type    The type (name) of the JTable class to get an instance of.
	 * @param   string  $prefix  An optional prefix for the table class name.
	 * @param   array   $config  An optional array of configuration values for the JTable object.
	 *
	 * @return  mixed    A JTable object if found or boolean false if one could not be found.
	 */
	public function getTable($type = 'Classroom', $prefix = 'JclassroomTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	/**
	 * Method for getting the form from the model.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/fields');

		JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR.'/models/rules');		
		
		$options = array('control' => 'jform', 'load_data' => $loadData);
		$form = $this->loadForm($this->typeAlias, $this->name, $options);
		
		if(empty($form))
		{
			return false;
		}

		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  array    The default data is an empty array.
	 */
	protected function loadFormData()
	{
		$app = JFactory::getApplication();
		$data = $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());
		
		if(empty($data))
		{
			$data = $this->getItem();
		}
		
		return $data;
	}
	function getResult($studentID, $quizzID, $unitID) {
		$input 	= JFactory::getApplication()->input;
		//$classroomID 	= $input->get('id', 0, 'INT');
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('
			a.id
			'));
        $query->from($db->quoteName('#__jclassroom_theresults','a'));
        //$query->where('a.classroomID = '.$classroomID);
        $query->where('a.quizzID = '.$quizzID);
        $query->where('a.unitID = '.$unitID);
        $query->where('a.created_by = '.$studentID);
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;

	}
	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if (!$item = parent::getItem($pk))
		{			
			throw new Exception('Failed to load item');
		} else {
			$library = JFactory::getApplication()->input;
			if($library->get('lib', '', 'INT') == 1):
				$libraryID = $library->get('id', 0, 'INT');
				// If Learningroom is from library
				$db = JFactory::getDbo();
				// Load the days
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classrooms_library','a'));
				$query->where('a.id = '.$libraryID);
				$db->setQuery($query);
				$result = $db->loadObject();
			endif;
			$user 	= JFactory::getUser();
			$item->testmode_message = '';
			if($item->showto == 2):
				$item->showto = 1;
			endif;
			if($item->id):
				$session = JFactory::getSession();
				$db = JFactory::getDbo();
				// Load the rights
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_rights','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$rights = $db->loadObjectList();
				$item->rights = $rights;
				// Load the days
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days','a'));
				$query->where('a.classroomID = '.$item->id);
				//$query->where('a.published = 1');
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->cards = $result;
				// Load the modules
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->modules = $result;
				// Load the units
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->units = $result;
				// Get the size of used files
		        $query = $db->getQuery(true);
				$query->select(array('SUM(a.size)'));
		        $query->from($db->quoteName('#__jclassroom_files','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$result = $db->loadResult();
				
				$item->currentSize = $result;
				// Load the reservefiles
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_files','a'));
				$query->where('a.classroomID = '.$item->id);
				$query->where('a.type = "reserve"');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->reserve = $result;
				// Load the regular Files
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_files','a'));
				$query->where('a.classroomID = '.$item->id);
				$query->where('a.type = "unit"');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->unitFiles = $result;
				// Load all Files
		        $query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_files','a'));
				$query->where('a.classroomID = '.$item->id);
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->files = $result;
				$template = '';
				// Load the company
		        $query = $db->getQuery(true);
				$query->select(array('a.name'));
		        $query->from($db->quoteName('#__jclassroom_companys','a'));
				$query->where('a.id = '.$item->companyID);
				$db->setQuery($query);
				$result = $db->loadResult();
				$item->company = $result;
				// Load the invitation template
		        $query = $db->getQuery(true);
				$query->select(array('a.text,a.styles'));
		        $query->from($db->quoteName('#__jclassroom_templates','a'));
				$query->where('a.id = '.$item->email_template);
				$db->setQuery($query);
				$result = $db->loadObject();
				$template = $result->text;
				$item->invitationTemplate = $template;
				// Load the students
				$students = false;
				$query = $db->getQuery(true);
				$query->select(array('
					a.created,
					a.comment,
					a.certificate,
					a.certificate_on,
					b.*,
					b.id as studentID,
					d.title,
					e.id as studentIntID,
					f.id as companyID,
					f.name as company,
					g.created as verified_on
				'));
		        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
		        $query->join('LEFT', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('a.userID') . ' = ' . $db->quoteName('b.id') . ')');
		        $query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('b.id') . ' = ' . $db->quoteName('c.user_id') . ')');
		        $query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
		        $query->join('LEFT', $db->quoteName('#__jclassroom_students', 'e') . ' ON (' . $db->quoteName('e.tblUserID') . ' = ' . $db->quoteName('a.userID') . ')');
		        $query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'f') . ' ON (' . $db->quoteName('e.companyID') . ' = ' . $db->quoteName('f.id') . ')');
		        $query->join('LEFT', $db->quoteName('#__jclassroom_verifications', 'g') . ' ON (' . $db->quoteName('a.userID') . ' = ' . $db->quoteName('g.studentID') . ')');
				$query->where('a.classroomID = '.$item->id);;
				$db->setQuery($query);
				$students = $db->loadObjectList();
				$setStudents = array();
				if($students):
					foreach($students as $student):
						if($student->studentID):
							//Check for sending Invitation
							$query = $db->getQuery(true);
							$query->select(array('a.send'));
					        $query->from($db->quoteName('#__jclassroom_emails','a'));
					        $query->where('a.typ = 2');
							$query->where('a.classroomID = '.$item->id);
							$query->where('a.send_to = '.$student->studentID);
							$query->order('a.send DESC');
							$query->setLimit(1);
							$db->setQuery($query);
							$invitation = $db->loadResult();
							//Check for sending Verification
							$query = $db->getQuery(true);
							$query->select(array('a.send'));
					        $query->from($db->quoteName('#__jclassroom_emails','a'));
					        $query->where('a.typ = 1');
							$query->where('a.classroomID = '.$item->id);
							$query->where('a.send_to = '.$student->id);
							$query->order('a.send DESC');
							$query->setLimit(1);
							$db->setQuery($query);
							$vertify = $db->loadResult();
							//Check for sending Remember
							$query = $db->getQuery(true);
							$query->select(array('a.send'));
					        $query->from($db->quoteName('#__jclassroom_emails','a'));
					        $query->where('a.typ = 3');
							$query->where('a.classroomID = '.$item->id);
							$query->where('a.send_to = '.$student->id);
							$query->order('a.send DESC');
							$query->setLimit(1);
							$db->setQuery($query);
							$remember = $db->loadResult();
							//Check for Verification
							$query = $db->getQuery(true);
							$query->select(array('a.created'));
					        $query->from($db->quoteName('#__jclassroom_verifications','a'));
					        $query->where('a.studentID = '.$student->studentID);
							$db->setQuery($query);
							$vertified = $db->loadResult();
							if($vertified):
								$vertified = date('d.m.Y H:i:s',strtotime($vertified));
							else:
								$vertified = '';
							endif;
							if($vertify):
								$vertify = date('d.m.Y H:i:s',strtotime($vertify));
							else:
								$vertify = '';
							endif;
							if($invitation):
								$invitation = date('d.m.Y H:i:s',strtotime($invitation));
							else:
								$invitation = '';
							endif;
							if($remember):
								$remember = date('d.m.Y H:i:s',strtotime($remember));
							else:
								$remember = '';
							endif;
							if($student->certificate == 1):
								$certificate_on = date('d.m.Y H:i', strtotime($student->certificate_on));
							else:
								$certificate_on = '';
							endif;
						endif;
						$setStudents[] = array(
							'id' 		=> $student->id,
							'comment' 	=> $student->comment,
							'studentID' => $student->studentID,
							'company' 	=> $student->company,
							'companyID' => 'CO'.str_pad($student->companyID,8,'0',STR_PAD_LEFT),
							'name' 		=> $student->name,
							'lastInvitation' => $invitation,
							'lastVertify' => $vertify, 
							'lastRemember' => $remember, 
							'verified_on'	=> $vertified,
							'email' 	=> $student->email,
							'password' 	=> $student->password,
							'userID' 	=> $student->id,
							'role' 		=> $student->title,
							'created' 	=> $student->created,
							'certificate' 	=> $student->certificate,
							'certificate_on'=> $certificate_on
						);
					endforeach;
				endif;
				$item->students = $setStudents;
				if($item->fromDate):
					$fromDate 		= new Datetime($item->fromDate);
					$item->fromDate = $fromDate->format('d.m.Y');
				endif;
				if($item->toDate):
					$toDate 		= new Datetime($item->toDate);
					$item->toDate 	= $toDate->format('d.m.Y');
				endif;
				// Load the quizzes in learningroom
				$query = $db->getQuery(true);
				$query->select(array('
					b.id as unitID,
					b.quizzID,
					b.title as unitName,
					c.title as quizzName,
					c.bgColor
					'));
		        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
		        $query->join('LEFT', $db->quoteName('#__jclassroom_classroom_days_units', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.classroomID') . ')');
		        $query->join('LEFT', $db->quoteName('#__jclassroom_units', 'c') . ' ON (' . $db->quoteName('b.quizzID') . ' = ' . $db->quoteName('c.id') . ')');
		        $query->where('a.id = '.$item->id);
		        $query->where('b.quizzID <> 0');
				$query->where('b.unitType = 4');
				$query->order('b.title ASC');
				$db->setQuery($query);
				$quizze = $db->loadObjectList();

				$item->quizze = $quizze;
				// Load the own customeraccount
				if($session->get('customerID')):
					$query = $db->getQuery(true);
					$query->select(array('
						a.*
						'));
			        $query->from($db->quoteName('#__jclassroom_customer','a'));
			        $query->where('a.id = '.$session->get('customerID'));
					$query->order('a.name asc,a.company_name asc');
					$db->setQuery($query);
					$customer = $db->loadObject();
					$item->customerAccount = $customer;
				else:
					$query = $db->getQuery(true);
					$query->select(array('
						a.*
						'));
			        $query->from($db->quoteName('#__jclassroom_customer','a'));
			        $query->order('a.name asc,a.company_name asc');
					$db->setQuery($query);
					$customers = $db->loadObjectList();
					$item->customersA = $customers;
				endif;
				if($session->get('customerID')):
					// Load the companies of the customer
					$query = $db->getQuery(true);
					$query->select(array('
						a.*
						'));
			        $query->from($db->quoteName('#__jclassroom_companys','a'));
			        $query->where('a.customerID = '.$session->get('customerID'));
					$query->order('a.name ASC');
					$db->setQuery($query);
					$companies = $db->loadObjectList();
					$item->companies = $companies;
				else:
					// Load all companies
					$query = $db->getQuery(true);
					$query->select(array('
						a.*
						'));
			        $query->from($db->quoteName('#__jclassroom_companys','a'));
			        //$query->where('a.customerID = '.$session->get('customerID'));
					$query->order('a.name ASC');
					$db->setQuery($query);
					$companies = $db->loadObjectList();
					$item->companies = $companies;
				endif;
				// Load the timeblocks
				$query = $db->getQuery(true);
				$query->select(array('
					a.*
				'));
		        $query->from($db->quoteName('#__jclassroom_timeblocks','a'));
				$query->where('a.classroomID = '.$item->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$timeblocks = $db->loadObjectList();
				$item->timeblocks = $timeblocks;
				// Load the themes in timeblocks
				$query = $db->getQuery(true);
				$query->select(array('
					a.*,
					b.title
				'));
		        $query->from($db->quoteName('#__jclassroom_timeblocks_themes','a'));
		        $query->join('LEFT', $db->quoteName('#__jclassroom_classroom_days', 'b') . ' ON (' . $db->quoteName('a.themeID') . ' = ' . $db->quoteName('b.id') . ')');
				$query->where('a.classroomID = '.$item->id);
				$query->order('a.ordering asc');
				$db->setQuery($query);
				$timeblocks_themes = $db->loadObjectList();
				$item->timeblocks_themes = $timeblocks_themes;
				$timeblockstemp = array();
				foreach($item->cards as $card):
					$setTheme = 0;
					foreach($timeblocks_themes as $timeblock_theme):
						if($card->id == $timeblock_theme->themeID):
							$setTheme = 1;
						endif;
					endforeach;
					if($setTheme == 0):
						$timeblockstemp[] = $card;
					endif;
				endforeach;
				$item->extractTimeblocks = $timeblockstemp;
				// Load the existing Students
		        $query = $db->getQuery(true);
				$query->select(array('
					a.id as tblUserID,
					c.title as role,
					d.first_name,
					d.last_name,
					d.id as userID,
					e.company_name as customer_name,
					f.name as company_name
					'));
		        $query->from($db->quoteName('#__users','a'));
		        $query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.user_id') . ')');
		        $query->join('LEFT', $db->quoteName('#__usergroups', 'c') . ' ON (' . $db->quoteName('b.group_id') . ' = ' . $db->quoteName('c.id') . ')');
		        $query->join('INNER', $db->quoteName('#__jclassroom_students', 'd') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('d.tblUserID') . ')');
		        $query->join('INNER', $db->quoteName('#__jclassroom_customer', 'e') . ' ON (' . $db->quoteName('d.customerID') . ' = ' . $db->quoteName('e.id') . ')');
		        $query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'f') . ' ON (' . $db->quoteName('d.companyID') . ' = ' . $db->quoteName('f.id') . ')');
		        if($session->get('customerID')):
					$query->where('d.customerID = '.$session->get('customerID'));
				endif;
				$query->where('c.title = "Students"');
				$query->order('d.last_name asc');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->chooseStundents = $result;
				// Load the existing Trainer
		        $query = $db->getQuery(true);
				$query->select(array('
					a.id as tblUserID,
					c.title as role,
					d.first_name,
					d.last_name,
					a.id as userID,
					e.company_name
					'));
		        $query->from($db->quoteName('#__users','a'));
		        $query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.user_id') . ')');
		        $query->join('LEFT', $db->quoteName('#__usergroups', 'c') . ' ON (' . $db->quoteName('b.group_id') . ' = ' . $db->quoteName('c.id') . ')');
		        $query->join('INNER', $db->quoteName('#__jclassroom_trainers', 'd') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('d.tblUserID') . ')');
		        $query->join('INNER', $db->quoteName('#__jclassroom_customer', 'e') . ' ON (' . $db->quoteName('d.customerID') . ' = ' . $db->quoteName('e.id') . ')');
				if($session->get('customerID')):
					$query->where('d.customerID = '.$session->get('customerID'));
				endif;
				$query->where('a.id <> '.$user->id);
				$query->where('c.title = "Trainer"');
				$query->order('e.company_name ASC, d.last_name asc');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->chooseTrainer = $result;
				// Load the existing Admins
				// If User is Customer or Trainer
				//if($session->get('group') == 'customer' || $session->get('group') == 'trainer'):
			        $query = $db->getQuery(true);
					$query->select(array('
						a.id as tblUserID,
						c.title as role,
						a.id as userID,
						d.first_name,
						d.last_name,
						e.company_name
						'));
			        $query->from($db->quoteName('#__users','a'));
			        $query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.user_id') . ')');
			        $query->join('LEFT', $db->quoteName('#__usergroups', 'c') . ' ON (' . $db->quoteName('b.group_id') . ' = ' . $db->quoteName('c.id') . ')');
			        $query->join('INNER', $db->quoteName('#__jclassroom_customer_administratoren', 'd') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('d.userID') . ')');
			        $query->join('INNER', $db->quoteName('#__jclassroom_customer', 'e') . ' ON (' . $db->quoteName('d.customerID') . ' = ' . $db->quoteName('e.id') . ')');
			        //$query->where('(a.id <> '.$user->id.')');
			        if($session->get('group') == 'customer' || $session->get('group') == 'trainer'):
						$query->where('d.customerID = '.$session->get('customerID'));
					endif;
					$query->where('c.title <> "Super Users"');

					$query->order('e.company_name ASC, d.last_name asc');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->chooseCustomerAdmins = $result;
				//endif;
				// If User is systemadministrator
				if($session->get('group') == 'superuser'):
			        $query = $db->getQuery(true);
					$query->select(array('
						a.*,
						a.id as tblUserID,
						c.title as role,
						a.id as userID
						'));
			        $query->from($db->quoteName('#__users','a'));
			        $query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.user_id') . ')');
			        $query->join('LEFT', $db->quoteName('#__usergroups', 'c') . ' ON (' . $db->quoteName('b.group_id') . ' = ' . $db->quoteName('c.id') . ')');
					$query->where('c.title = "Super Users"');
					$query->order('a.name asc');
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->chooseSystemAdmins = $result;
				endif;
				
			else:
				$session = JFactory::getSession();
				$item->created_by 	= $user->id;
				if($session->get('group') == 'trainer'):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 	= JTable::getInstance('Trainer','JclassroomTable',array());
					$load 	= array('tblUserID' => $user->id);
					$table->load($load);
					$item->main_trainer	= $table->id;
				else:

				endif;
				$item->students 	= false;
				$item->cards 		= false;
			endif;
		}

		return $item;
	}
	
	/**
	 * Increment the hit counter for the item.
	 *
	 * @param   integer  $pk  Optional primary key of the item to increment.
	 *
	 * @return  boolean  True if successful; false otherwise and internal error set.
	 */
	public function hit($pk = 0)
	{
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);

		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('kunde.id');

			$table = $this->getTable();
			$table->load($pk);
			$table->hit($pk);
		}

		return true;
	}
}
?>