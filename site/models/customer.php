<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for auditor.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelCustomer extends JModelAdmin
{
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_JCLASSROOM';

	/**
	 * The type alias for this content type.
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_jclassroom.customer';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object    $record    A record object.
	 *
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id))
		{
			if ($record->published != -2)
			{
				return false;
			}
			

			$user = JFactory::getUser();
			return $user->authorise('core.delete', $this->typeAlias . '.' . (int) $record->id);
		}
	}		

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable    A JTable object.
	 *
	 * @return  void
	 * @since   1.6
	 */
	protected function prepareTable($table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
	}

	/**
	 * Auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('administrator');

		// Load the User state.
		$pk = $app->input->getInt('id');
		$this->setState($this->getName() . '.id', $pk);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jclassroom');
		$this->setState('params', $params);
	}
	
	/**
	 * Alias for JTable::getInstance()
	 *
	 * @param   string  $type    The type (name) of the JTable class to get an instance of.
	 * @param   string  $prefix  An optional prefix for the table class name.
	 * @param   array   $config  An optional array of configuration values for the JTable object.
	 *
	 * @return  mixed    A JTable object if found or boolean false if one could not be found.
	 */
	public function getTable($type = 'Customer', $prefix = 'JclassroomTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	/**
	 * Method for getting the form from the model.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/fields');

		JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR.'/models/rules');		
		
		$options = array('control' => 'jform', 'load_data' => $loadData);
		$form = $this->loadForm($this->typeAlias, $this->name, $options);
		
		if(empty($form))
		{
			return false;
		}

		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  array    The default data is an empty array.
	 */
	protected function loadFormData()
	{
		$app = JFactory::getApplication();
		$data = $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());
		
		if(empty($data))
		{
			$data = $this->getItem();
		}
		
		return $data;
	}
	
	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if (!$item = parent::getItem($pk))
		{			
			throw new Exception('Failed to load item');
		}
		/*$user = JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_plans/tables');
		$table = JTable::getInstance('Customer','PlansTable',array());
		$load = array('tblUserID' => $user->id);
		$table->load($load);
		$tblResellerID 	= $table->id;
		$item->postcode = ($item->postcode == 0) ? '' : $item->postcode;
		if (!$item->id)
		{
			$item->created_by = JFactory::getUser()->get('id');
			$item->modified_by = JFactory::getUser()->get('id');
			$item->newDS = 1;
			$item->currentUserID = 0;
		}*/
		if($item->id) {
			$item->planID 	= $item->planID;
			$item->plan 	= $item->planID;
			$db = JFactory::getDbo();
			// Load Contacts
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_contacts','a'));
			$query->where('a.customerID = '.$item->id);
			$query->where('a.type = 1');
			$query->order('a.last_name asc');
			$db->setQuery($query);
			$item->customer_contacts = $db->loadObjectList();
			// Load Administrators
	        $query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
			$query->where('a.customerID = '.$item->id);
			$db->setQuery($query);
			$item->customer_administratoren = $db->loadObjectList();
			// Count Administrators
	        $query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_customer_administratoren','a'));
			$query->where('a.customerID = '.$item->id);
			$db->setQuery($query);
			$db->execute();
			$item->count_customer_administratoren = $db->getNumRows();
			// Count Trainers
	        $query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_trainers','a'));
			$query->where('a.customerID = '.$item->id);
			$db->setQuery($query);
			$db->execute();
			$item->count_customer_trainers = $db->getNumRows();
			// Count Learningrooms
	        $query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
			$query->where('a.customerID = '.$item->id);
			$db->setQuery($query);
			$db->execute();
			$item->count_customer_learningrooms = $db->getNumRows();
			// Count Modules
	        $query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_classroom_modules','a'));
			$query->where('a.customerID = '.$item->id);
			$db->setQuery($query);
			$db->execute();
			$item->count_customer_modules = $db->getNumRows();
			// Count Units
	        $query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
			$query->where('a.customerID = '.$item->id);
			$db->setQuery($query);
			$db->execute();
			$item->count_customer_units = $db->getNumRows();
			// Load Plan
			$query = $db->getQuery(true);
			$query->select(array('a.*,b.*'));
	        $query->from($db->quoteName('#__jclassroom_customer','a'));
	        $query->join('LEFT', $db->quoteName('#__jclassroom_plans', 'b') . ' ON (' . $db->quoteName('a.planID') . ' = ' . $db->quoteName('b.id') . ')');
			$query->where('a.id = '.$item->id);
			$db->setQuery($query);
			$result 				= $db->loadObject();
			$item->customer_plans 	= $result;
			// Load Payment
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_customer_payments','a'));
			$query->where('a.customerID = '.$item->id);
			$db->setQuery($query);
			$result 				= $db->loadObject();
			$item->paymentID 		= $result->paymentID;
			$item->account_owner 	= $result->account_owner;
			$item->iban 			= $result->iban;
			$item->bic 		 		= $result->bic;
			$invoice_title 			= $result->invoice_title;
			if($invoice_title):
				$item->invoice_title = $invoice_title; 		
			else:
				$item->invoice_title = $item->company_name;
			endif;
			if(!$result):
				$item->valid_from 	= '';
				$item->valid_to 	= '';
			else:
				if($result->valid_from == '1970-01-01'):
					$item->valid_from 	= '';
				else:
					$item->valid_from 	= date('d.m.Y', strtotime($result->valid_from));
				endif;
				if($result->valid_to == '1970-01-01'):
					$item->valid_to 	= '';
				else:
					$item->valid_to 	= date('d.m.Y', strtotime($result->valid_to));
				endif;
			endif;
			$item->creditcard_bank 	= $result->creditcard_bank;
			$item->creditcard_number 		= $result->creditcard_number;
			$item->creditcard_check_number	= $result->creditcard_check_number;
			$item->creditcard_valid_to 		= date('d.m.Y', strtotime($result->creditcard_valid_to));
			$item->customer_payments 		= $result;
			$item->customer_number			= 'CU'.str_pad($item->id, 8, '0', STR_PAD_LEFT);
			$item->invoice_adress 			= $result->invoice_adress;
			
			if($result->invoice_postcode == 0):
				$item->invoice_postcode 		= '';
			else:
				$item->invoice_postcode 		= $result->invoice_postcode;
			endif;
			$item->invoice_city 			= $result->invoice_city;
			$item->paymentData = $payment;
			if($item->plan_from == '0000-00-00'):
				$item->plan_from = '';
			else:
				$item->plan_from = date('d.m.Y',strtotime($item->plan_from));
			endif;
			if($item->plan_to == '0000-00-00'):
				$item->plan_to = '';
			else:
				$item->plan_to = date('d.m.Y',strtotime($item->plan_to));
			endif;
		} else {
			$session = JFactory::getSession();
			$customerID = $session->get('customerID');
			if($customerID):
				// Load Payment
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_customer_payments','a'));
				$query->where('a.customerID = '.$customerID);
				$db->setQuery($query);
				$result 					= $db->loadObject();
				$item->customer_number		= 'CU'.str_pad($customerID, 8, '0', STR_PAD_LEFT);
				$item->valid_from 			= '';
				$item->valid_to				= '';
				/* Payments
				1 = Rechnung
				2 = Lastschrift
				3 = Kreditkarte
				4 = PayPal
				5 = Keine Zahlungsdaten
				*/
				$item->paymentIDPlan = $result->paymentID; 
				$payment = array(
					'paymentID' 	=> $result->paymentID,
					'account_owner' => $result->account_owner,
					'iban'   		=> $result->iban,
					'bic' 			=> $result->bic,
					'valid_from' 	=> $item->valid_from,
					'valid_to' 		=> $item->valid_to,
					'creditcard_bank'	=> $result->creditcard_bank,
					'creditcard_number' => $result->creditcard_number,
					'creditcard_check_number' 	=> $result->creditcard_check_number,
					'creditcard_valid_to' 		=> $result->creditcard_valid_to,
					'customer_number' 	=> 'CU'.str_pad($customerID, 8, '0', STR_PAD_LEFT),
					'invoice_title' 	=> $result->invoice_title,
					'invoice_adress' 	=> $result->invoice_adress,
					'invoice_postcode' 	=> $result->invoice_postcode,
					'invoice_city' 		=> $result->invoice_city
					
				);
				$item->invoice_title = $result->invoice_title;
				$item->invoice_adress = $result->invoice_adress;
				$item->invoice_postcode = $result->invoice_postcode;
				$item->invoice_city = $result->invoice_city;
				$item->account_owner = $result->account_owner;
				$item->iban = $result->iban;
				$item->bic = $result->bic;
				$item->paymentData = $payment;
			else:
				$item->customer_number		= 'CU'.str_pad($customerID, 8, '0', STR_PAD_LEFT);
				$item->valid_from 			= '';
				$item->valid_to				= '';
			endif;
		}

		return $item;
	}
	
	/**
	 * Increment the hit counter for the item.
	 *
	 * @param   integer  $pk  Optional primary key of the item to increment.
	 *
	 * @return  boolean  True if successful; false otherwise and internal error set.
	 */
	public function hit($pk = 0)
	{
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);

		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('kunde.id');

			$table = $this->getTable();
			$table->load($pk);
			$table->hit($pk);
		}

		return true;
	}
}
?>