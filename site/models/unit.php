<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for fragenkatalog.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelUnit extends JModelAdmin
{
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_JCLASSROOM';

	/**
	 * The type alias for this content type.
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_jclassroom.unit';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object    $record    A record object.
	 *
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id))
		{
			if ($record->published != -2)
			{
				return false;
			}
			

			$user = JFactory::getUser();
			return $user->authorise('core.delete', $this->typeAlias . '.' . (int) $record->id);
		}
	}		

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable    A JTable object.
	 *
	 * @return  void
	 * @since   1.6
	 */
	protected function prepareTable($table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
	}

	/**
	 * Auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('administrator');

		// Load the User state.
		$pk = $app->input->getInt('id');
		$this->setState($this->getName() . '.id', $pk);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jclassroom');
		$this->setState('params', $params);
	}
	
	/**
	 * Alias for JTable::getInstance()
	 *
	 * @param   string  $type    The type (name) of the JTable class to get an instance of.
	 * @param   string  $prefix  An optional prefix for the table class name.
	 * @param   array   $config  An optional array of configuration values for the JTable object.
	 *
	 * @return  mixed    A JTable object if found or boolean false if one could not be found.
	 */
	public function getTable($type = 'Unit', $prefix = 'JclassroomTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	/**
	 * Method for getting the form from the model.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR.'/models/fields');

		JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR.'/models/rules');		
		
		$options = array('control' => 'jform', 'load_data' => $loadData);
		$form = $this->loadForm($this->typeAlias, $this->name, $options);
		
		if(empty($form))
		{
			return false;
		}

		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  array    The default data is an empty array.
	 */
	protected function loadFormData()
	{
		$app = JFactory::getApplication();
		$data = $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());
		
		if(empty($data))
		{
			$data = $this->getItem();
		}
		
		return $data;
	}
	
	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		$session 	= JFactory::getSession();
		if (!$item = parent::getItem($pk))
		{			
			throw new Exception('Failed to load item');
		}
		if (!$item->id)
		{
			$item->created_by = JFactory::getUser()->get('id');
			$item->modified_by = JFactory::getUser()->get('id');
		}
		if($item->id) {
			
			$input 		= JFactory::getApplication()->input;
			$q 			= $input->get('q', 0, 'INT');
			
		}
		$db 		= JFactory::getDbo();
		if($session->get('group') == 'superuser'):
			// Load Systemadmins
			$query = $db->getQuery(true);
			$query->select('
				a.id,
				a.name'
			)->from("#__users as a");
			$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
			$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
			$query->where('d.title = "Super Users"');
			$query->order('a.name ASC');
			$db->setQuery($query);
			$item->systemadmins = $db->loadObjectList();
		endif;
		// Load Customeradmins
		$query = $db->getQuery(true);
		$query->select("a.id, a.name,d.title,f.company_name")->from("#__users as a");
		$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
		$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
		$query->join('INNER', $db->quoteName('#__jclassroom_customer_administratoren', 'e') . ' ON (' . $db->quoteName('e.userID') . ' = ' . $db->quoteName('a.id') . ')');
		$query->join('INNER', $db->quoteName('#__jclassroom_customer', 'f') . ' ON (' . $db->quoteName('e.customerID') . ' = ' . $db->quoteName('f.id') . ')');
		if($session->get('group') != 'superuser'):
			$query->where('e.customerID = '.$session->get('customerID'));
		endif;
		$query->order('f.company_name ASC,a.name ASC');
		$db->setQuery($query);
		$item->customeradmins = $db->loadObjectList();
		// Load Trainer 
		$query = $db->getQuery(true);
		$query->select("
			a.id, 
			a.name,
			d.title,
			e.first_name,
			e.last_name,
			f.company_name
		")->from("#__users as a");
		$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
		$query->join('LEFT', $db->quoteName('#__usergroups', 'd') . ' ON (' . $db->quoteName('c.group_id') . ' = ' . $db->quoteName('d.id') . ')');
		
		$query->join('INNER', $db->quoteName('#__jclassroom_trainers', 'e') . ' ON (' . $db->quoteName('e.tblUserID') . ' = ' . $db->quoteName('a.id') . ')');
		$query->join('LEFT', $db->quoteName('#__jclassroom_customer', 'f') . ' ON (' . $db->quoteName('e.customerID') . ' = ' . $db->quoteName('f.id') . ')');
		if($session->get('group') != 'superuser'):
			$query->where('e.customerID = '.$session->get('customerID'));
		endif;
		$query->order('f.company_name ASC,e.last_name ASC');
		$db->setQuery($query);
		$item->trainer = $db->loadObjectList();
		return $item;
	}
	function getCountFragen($item) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_unitelements','a'));
		//$query->join('LEFT', $db->quoteName('#__inclure_projekte', 'b') . ' ON (' . $db->quoteName('a.projekt') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($item->id));
		$db->setQuery($query);
		$db->execute();
		$num_rows = $db->getNumRows();
		
		return $num_rows;
	}
	function getPoints($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('SUM(a.points) as points');
        $query->from($db->quoteName('#__jclassroom_resultelements','a'));
		$query->join('LEFT', $db->quoteName('#__jclassroom_results', 'b') . ' ON (' . $db->quoteName('a.resultID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('b.id').' = '.$db->quote($resultID));
		//$query->group('a.resultID');
		$db->setQuery($query);
		$points = $db->loadResult();
		
		return $points;
	}
	function getTimes($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.start, a.stop'));
        $query->from($db->quoteName('#__audit_times','a'));
		$query->join('LEFT', $db->quoteName('#__jclassroom_results', 'b') . ' ON (' . $db->quoteName('a.resultID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('b.id').' = '.$db->quote($resultID));
		//$query->group('a.resultID');
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}
	function getResult($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__jclassroomresults','a'));
		$query->where($db->quoteName('a.id').' = '.$db->quote($resultID));
		$db->setQuery($query);
		$result = $db->loadObject();
		$elements = $this->getResultelements($resultID);
		$html = '';
		if($result) {
			$i = 1;
			$html .= '<table class="table table-striped table-sm">';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<th>Frage Nr.</th>';
			$html .= '<th>Frage</th>';
			$html .= '<th>Antwort</th>';
			$html .= '<th>Max. Punkte</th>';
			$html .= '<th class="text-right">Erreichte Punkte</th>';
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			$sum_points = 0;
			$max_points = 0;
			foreach($elements as $item) {
				if($item->soll != $item->ist) {
					$color = 'text-danger';
				} else {
					$color = 'text-success';
				}
				$html .= '<tr>';
				$html .= '<td>'.$i.'</td>';
				$html .= '<td class="'.$color.'"><b>'.$item->name.'</b></td>';
				$html .= '<td>'.$item->answers.'</td>';
				$html .= '<td>'.$item->max_points.'</td>';
				$html .= '<td class="text-right">'.$item->sum_points.'</td>';
				$html .= '</tr>';
				$i++;
				$max_points += $item->max_points;
				$sum_points += $item->sum_points;
			}
			$html .= '</tbody>';
			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td colspan="3">In dieser Unit erreichte Punkte:</td>';
			$html .= '<td>'.$max_points.'</td>';
			$html .= '<td class="text-right">'.$sum_points.'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="4">Entspricht einem Highscore von:</td>';
			$html .= '<td class="text-right">'.number_format(($sum_points * 100) / $max_points,2,',','.').'%</td>';
			$html .= '</tr>';
			$times = $this->getAllTimes($resultID);
			$html .= '<tr>';
			$html .= '<td colspan="4">Benötigte Zeit:</td>';
			$html .= '<td class="text-right">'.$times.'</td>';
			$html .= '</tr>';
			$html .= '</tfoot>';
			$html .= '</table>';
		}

		return $html;
	}
	function getResultelements($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*,
			a.points as sum_points,
			a.answers as ist,
			b.name,
			b.answers as soll,
			c.points as max_points
		'));
        $query->from($db->quoteName('#__jclassroom_resultelements','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzes', 'b') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('b.id') . ')');
        $query->join('LEFT', $db->quoteName('#__jclassroom_unitelements', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.quizzID') . ')');
		$query->where($db->quoteName('a.resultID').' = '.$db->quote($resultID));
		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;
	}
	function getAllTimes($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__jclassroom_times','a'));
		$query->where($db->quoteName('a.resultID').' = '.$db->quote($resultID));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$dauer = 0;
		if($result) {
			foreach($result as $item) {
				$dauer += $item->stop - $item->start;
			}
		}
		$dauer = (($dauer / 60) % 60).' Minuten';

		return $dauer;
	}
	/**
	 * Increment the hit counter for the item.
	 *
	 * @param   integer  $pk  Optional primary key of the item to increment.
	 *
	 * @return  boolean  True if successful; false otherwise and internal error set.
	 */
	public function hit($pk = 0)
	{
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);

		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('fragenkatalog.id');

			$table = $this->getTable();
			$table->load($pk);
			$table->hit($pk);
		}

		return true;
	}
}
?>