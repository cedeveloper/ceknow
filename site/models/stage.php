<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for fragenkatalog.
 *
 * @package     Auditum
 * @subpackage  Models
 */
class JclassroomModelStage extends JModelList
{
	//If quizz will be called by staging
	function getQuizzFromStageing() {
		$app 		= JFactory::getApplication();
		$session  	= JFactory::getSession();
		$input 		= JFactory::getApplication()->input;
		$quizzID 	= $input->get('qID', 0, 'INT');
		$q 			= $input->get('q', 0, 'INT');
		$preview 	= $input->get('preview', '', 'INT');
		$user 		= JFactory::getUser();
		$db = $this->getDbo();
		
		//The quizz is preview
		if($preview == 1):
			$query = $db->getQuery(true);
			$query->select(array('
				a.*
			'));
	        $query->from($db->quoteName('#__jclassroom_units','a'));
			$query->where($db->quoteName('a.id').' = '.$db->quote($quizzID));
			$db->setQuery($query);
			$result 	= $db->loadObject();
			//Count Quizzpositions
			$query = $db->getQuery(true);
			$query->select(array('count(a.id)'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
			$query->where($db->quoteName('a.type').' <> '.$db->quote(1).' AND '.$db->quoteName('a.type').' <> '.$db->quote(14));
			$db->setQuery($query);
			$countQuestions 	= $db->loadResult();
			//Count No Quizzpositions
			$query = $db->getQuery(true);
			$query->select(array('count(a.id)'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
	        $query->where('(a.type = 1 OR a.type = 14)');
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
			$db->setQuery($query);
			$countNoQuestions 	= $db->loadResult();
			//Check if there are groups
			$query = $db->getQuery(true);
			$query->select(array('a.id'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
			$query->where($db->quoteName('a.type').' = 14');
			$db->setQuery($query);
			$groups 	= $db->loadObjectList();
			//Get Quizzpositions
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
			$query->where($db->quoteName('a.ordering').' = '.$db->quote($q));
			$db->setQuery($query);
			$theQuestions 	= $db->loadObject();
			//Get invalid positions before current position
			$query = $db->getQuery(true);
			$query->select(array('COUNT(a.id) as noBefore'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
			$query->where('(a.type = 1 OR a.type = 14)');
			$query->where($db->quoteName('a.ordering').' < '.$db->quote($q));
			$db->setQuery($query);
			$noBefore 	= $db->loadObject();
			//Get Answers
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theAnswers 	= $db->loadObjectList();
			//Get Options
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theOptions 	= $db->loadObjectList();
			//Get Checkboxes
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theCheckboxes 	= $db->loadObjectList();
			//Get Answers correct
			$query = $db->getQuery(true);
			$query->select(array('count(a.id)'));
	        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->where($db->quoteName('a.correct').' = 1');
			$db->setQuery($query);
			$theAnswersCorrect 	= $db->loadResult();
			$existingAnswer 	= $this->getExistingAnswer($theQuestions->id);
			$return = array(
				'quizzID' 		=> $quizzID, 
				'quizzType' 		=> $result->quizzType,
				'publishedQuizzID' 	=> 0,
				'title' 		=> $result->title, 
				'description' 	=> $result->description,
				'ordering' 		=> $theQuestions->ordering,
				'noBefore'		=> $noBefore->noBefore,
				'question' 		=> $theQuestions->title,
				'questionID' 	=> $theQuestions->id,
				'type' 			=> $theQuestions->type,
				'content' 		=> $theQuestions->content,
				'infotext' 		=> $theQuestions->infotext,
				'testfield' 	=> $theQuestions->testfield,
				'theme' 		=> $theQuestions->theme,
				'answers' 		=> $theAnswers,
				'options' 		=> $theOptions,
				'checkboxes' 	=> $theCheckboxes,
				'countCorrect'	=> $theAnswersCorrect,
				'correctAnswers'=> $theQuestions->correctAnswers,
				'countSections' => $countQuestions,
				'countNoSections' => $countNoQuestions,
				'stageType'		=> 'quizz',
				'showResultAfter'=> $result->showResultAfter,
				'allowRestart' 	=> $result->allowRestart,
				'groups' 		=> $groups,
				'groupTitle' 	=> $theQuestions->title,
				'existingAnswer'=> $existingAnswer,
				'mustBeAnswered'=> $result->mustBeAnswered
			);
		else:
			if($quizzID):
				if(!$q):
					$q = 0;
				endif;
				$db = $this->getDbo();
				$query = $db->getQuery(true);
				$query->select(array('
					a.*,
					b.type as quizzType
				'));
		        $query->from($db->quoteName('#__jclassroom_quizzes','a'));
		        $query->join('INNER', $db->quoteName('#__jclassroom_units', 'b') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('b.id') . ')');
				$query->where($db->quoteName('a.id').' = '.$db->quote($quizzID));
				$db->setQuery($query);
				$result 	= $db->loadObject();
				// Stop, if quizz is private and no user is logged in
				if($result->openQuizz == 0 && !$user->id):
					JFactory::getApplication()->enqueueMessage('Dies ist ein geschlossenes Quizz. Bitte melden sie sich an.', 'Message');
					$app->redirect(JRoute::_(JURI::Root().'demo', false));
				endif;
				// Stop, if the quizz is not published
				if($result->publish_up != '0000-00-00 00:00:00' && $result->publish_down != '0000-00-00 00:00:00'):
					if($result->publish_up > date('Y-m-d H:i:s')):
						JFactory::getApplication()->enqueueMessage('Dieses Quizz ist erst ab dem '.date('d.m.Y H:i', strtotime($result->publish_up)).' freigegeben.', 'Message');
						$app->redirect(JRoute::_(JURI::Root(), false));
					endif;
					if($result->publish_down < date('Y-m-d H:i:s')):
						JFactory::getApplication()->enqueueMessage('Dieses Quizz ist nur bis zum '.date('d.m.Y H:i', strtotime($result->publish_down)).' freigegeben.', 'Message');
						$app->redirect(JRoute::_(JURI::Root(), false));
					endif;
				endif;
				//Get Quizzpositions
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->quizzID));
				$query->where($db->quoteName('a.ordering').' = '.$db->quote($q));
				$db->setQuery($query);
				$theQuestions 	= $db->loadObject();
				//Get invalid positions before current position
				$query = $db->getQuery(true);
				$query->select(array('COUNT(a.id) as noBefore'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->quizzID));
				$query->where('(a.type = 1 OR a.type = 14)');
				$query->where($db->quoteName('a.ordering').' < '.$db->quote($q));
				$db->setQuery($query);
				$noBefore 	= $db->loadObject();
				//Count Quizzpositions
				$query = $db->getQuery(true);
				$query->select(array('count(a.id)'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->quizzID));
				$query->where($db->quoteName('a.type').' <> '.$db->quote(1).' AND '.$db->quoteName('a.type').' <> '.$db->quote(14));
				$db->setQuery($query);
				$countQuestions 	= $db->loadResult();
				//Check if there are groups
				$query = $db->getQuery(true);
				$query->select(array('a.id'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->quizzID));
				$query->where($db->quoteName('a.type').' = 14');
				$db->setQuery($query);
				$groups 	= $db->loadObjectList();
				//Get Answers
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
				$query->order('a.ordering ASC');
				$db->setQuery($query);
				$theAnswers 	= $db->loadObjectList();
				//Get Options
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
				$query->order('a.ordering ASC');
				$db->setQuery($query);
				$theOptions 	= $db->loadObjectList();
				//Get Checkboxes
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
				$query->order('a.ordering ASC');
				$db->setQuery($query);
				$theCheckboxes 	= $db->loadObjectList();
				//Get Answers correct
				$query = $db->getQuery(true);
				$query->select(array('count(a.id)'));
		        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
				$query->where($db->quoteName('a.correct').' = 1');
				$db->setQuery($query);
				$theAnswersCorrect 	= $db->loadResult();
				$existingAnswer 	= $this->getExistingAnswer($theQuestions->id);
				if($result):
					// the quizzID is the ID of the published quizz.
					// the unitID is the ID of the quizz, which is connected with the published quizz
					$return = array('quizzID' => $result->quizzID, 
						'quizzType' 	=> $result->quizzType,
						'publishedQuizzID' 	=> $quizzID,
						'title' 		=> $result->title, 
						'description' 	=> $result->description,
						'ordering' 		=> $theQuestions->ordering,
						'noBefore'		=> $noBefore->noBefore,
						'question' 		=> $theQuestions->title,
						'questionID' 	=> $theQuestions->id,
						'type' 			=> $theQuestions->type,
						'content' 		=> $theQuestions->content,
						'infotext' 		=> $theQuestions->infotext,
						'testfield' 	=> $theQuestions->testfield,
						'theme' 		=> $theQuestions->theme,
						'answers' 		=> $theAnswers,
						'options' 		=> $theOptions,
						'checkboxes' 	=> $theCheckboxes,
						'countCorrect'	=> $theAnswersCorrect,
						'correctAnswers'=> $theQuestions->correctAnswers,
						'countSections' => $countQuestions,
						'stageType'		=> 'quizz',
						'showResultAfter'=> $result->showResultAfter,
						'allowRestart' 	=> $result->allowRestart,
						'groups' 		=> $groups,
						'existingAnswer'=> $existingAnswer,
						'groupTitle' 	=> $theQuestions->title
					);
				endif;	
			endif;
		endif;
		return $return;
	}
	//If quizz will be called by learningroom
	function getUnit() {
		$session  	= JFactory::getSession();
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('unitID', 0, 'INT');
		$uID 		= $input->get('uID', 0, 'INT');
		$lid 		= $input->get('lid', 0, 'INT');
		$q 			= $input->get('q', 0, 'INT');
		$classroomID= $input->get('clr', 0, 'INT');
		if(!$q):
			$q = 0;
		endif;
		if($q == 0):
			$session->clear('quizzes');
		endif;
		$return 	= false;
		if($lid != 9) {
			$option = array(); 
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_units','a'));
			$query->where($db->quoteName('a.id').' = '.$db->quote($unitID));
			$db->setQuery($query);
			$result 	= $db->loadObject();
			// Get the unit in classroom
			$query = $db->getQuery(true);
			$query->select(array('a.id'));
	        $query->from($db->quoteName('#__jclassroom_classroom_days_units','a'));
			$query->where($db->quoteName('a.classroomID').' = '.$db->quote($classroomID));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($unitID));
			$db->setQuery($query);
			//$uID 	= $db->loadResult();
			//Count Quizzpositions
			$query = $db->getQuery(true);
			$query->select(array('count(a.id)'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->id));
			$query->where($db->quoteName('a.type').' <> '.$db->quote(1).' AND '.$db->quoteName('a.type').' <> '.$db->quote(14));
			$db->setQuery($query);
			$countQuestions 	= $db->loadResult();
			//Get invalid positions before current position
			$query = $db->getQuery(true);
			$query->select(array('COUNT(a.id) as noBefore'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->id));
			$query->where('(a.type = 1 OR a.type = 14)');
			$query->where($db->quoteName('a.ordering').' < '.$db->quote($q));
			$db->setQuery($query);
			$noBefore 	= $db->loadObject();
			//Check if there are groups
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->id));
			$query->where($db->quoteName('a.type').' = 14');
			$db->setQuery($query);
			$groups 	= $db->loadObjectList();
			//Get Quizzpositions
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->id));
			//$query->where($db->quoteName('a.type').' <> 14');
			$query->where($db->quoteName('a.ordering').' = '.$db->quote($q));
			$db->setQuery($query);
			$theQuestions 	= $db->loadObject();
			//Get Answers
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theAnswers 	= $db->loadObjectList();
			//Get Options
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theOptions 	= $db->loadObjectList();
			//Get Checkboxes
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theCheckboxes 	= $db->loadObjectList();
			//Get Answers correct
			$query = $db->getQuery(true);
			$query->select(array('count(a.id)'));
	        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->where($db->quoteName('a.correct').' = 1');
			$db->setQuery($query);
			$theAnswersCorrect 	= $db->loadResult();
			$existingAnswer 	= $this->getExistingAnswer($theQuestions->id);
			$return = array('quizzID' => $result->id, 
				'uID' 			=> $uID,
				'publishedQuizzID' => 0,
				'title' 		=> $result->title, 
				'description' 	=> $result->description,
				'ordering' 		=> $theQuestions->ordering,
				'noBefore' 		=> $noBefore->noBefore,
				'mustBeAnswered'=> $result->mustBeAnswered,
				'question' 		=> $theQuestions->title,
				'questionID' 	=> $theQuestions->id,
				'type' 			=> $theQuestions->type,
				'content' 		=> $theQuestions->content,
				'infotext' 		=> $theQuestions->infotext,
				'answers' 		=> $theAnswers,
				'options' 		=> $theOptions,
				'checkboxes' 	=> $theCheckboxes,
				'countCorrect'	=> $theAnswersCorrect,
				'countSections' => $countQuestions,
				'correctAnswers'=> $theQuestions->correctAnswers,
				'stageType'		=> 'lr',
				'existingAnswer'=> $existingAnswer,
				'message' 		=> $groups
			);	
		}
		return $return;
	}
	function getExistingAnswer($questionID) {
		$session 	= JFactory::getSession();
		$cart 		= $session->get('quizzes');
		$input 		= JFactory::getApplication()->input;
		$stageType 	= $input->get('sT','','STR');
		$unitID 	= $input->get('unitID',0,'INT');
		$answer 	= '';
		if($cart):
			foreach($cart as $item):
				//$answer .= "A".$item['questionID'].' '.$questionID.'<br/>';
				if($item['questionID'] == $questionID):
					if($item['type'] != 6):
						$answer 	= $item['content'][0];
					else:
						$answer 	= $item['content'][0].$item['exeptionPoints'].'_'.$item['exeptionText'];
					endif;
				endif;
			endforeach;
		endif;
		$return 	= array(
			'classroomID' 	=> $input->get('clr', 0, 'INT'),
			'quizzID' 		=> $input->get('unitID',0,'INT'),
			'questionID' 	=> $questionID,
			'q' 			=> $input->get('q',0, 'INT'),
			'answer' 		=> $answer
		);

		return $return;
	}
	function getUnit_sections() {
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('id', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db = JDatabaseDriver::getInstance( $option );
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__audit_unit_sections','a'));
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
		$db->setQuery($query);
		$result = $db->loadObjectList();

		foreach($result as $item) {
			$answers 	= $this->getUnit_section_answers($item->id);
			$return[] 	= array('type' => $item->type, 'content' => $item->content, 
				'answers' => $answers,'sectionTitle' => $item->title,
				'points' => $item->points);
		}

		return $return;
	}
	function getUnit_section() {
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('id', 0, 'INT');
		$sectionPosition 	= $input->get('q', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db = JDatabaseDriver::getInstance( $option );
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__audit_unit_sections','a'));
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
		$query->where($db->quoteName('a.ordering').' = '.$db->quote($sectionPosition));
		$db->setQuery($query);
		$result = $db->loadObject();
		$answers 	= $this->getUnit_section_answers($result->id);
		$return 	= array(
			'sectionID' => $result->id,
			'type' => $result->type, 
			'content' => $result->content, 
			'answers' => $answers,
			'sectionTitle' => $result->title,
			'points' => $result->points,
			'ordering' => $result->ordering);
	
		return $return;
	}
	function getUnit_section_answers($sectionID) {
		$return  = false;
		$session = JFactory::getSession();
		$answers = $session->get('answers');
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('unitID', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db = JDatabaseDriver::getInstance( $option );
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__audit_unit_section_answers','a'));
		$query->where($db->quoteName('a.sectionID').' = '.$db->quote($sectionID));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result) {
			foreach($result as $item) {
				$btn 		= 'btn-light';
				$aResult 	= 0;
				$aChoice 	= 0;
				if($answers) {
					foreach($answers as $answer) {
						if($answer['answers']) {
							foreach($answer['answers'] as $a) {
								if($a['answerID'] == $item->id) {
									if($a['choice'] == 1) {
										$btn 	= 'btn-success';
										$aChoice = $a['choice'];
														echo $aChoice;
									}
								}
							}
						}
					}
				}

				$return[] = array('id' => $item->id, 'sectionID' => $item->sectionID, 'answer' => $item->answer, 'correct' => $item->correct, 'result' => $aResult, 'choice' => $aChoice, 'btn' => $btn);
			}
		}
		return $return;
	}
	function getCountSections() {
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('id', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db 		= JDatabaseDriver::getInstance( $option );
		$db = $this->getDbo();
		$query 		= $db->getQuery(true);
		$query->select(array('COUNT(a.id) as countSections'));
        $query->from($db->quoteName('#__audit_unit_sections','a'));
		//$query->join('LEFT', $db->quoteName('#__inclure_projekte', 'b') . ' ON (' . $db->quoteName('a.projekt') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
		$db->setQuery($query);
		$result = $db->loadResult();

		return $result;
	}
	function getResult($resultID = 0) {
		$input 		= JFactory::getApplication()->input;
		if($input->get('rID', ' ', 'INT')):
			$resultID 	= $input->get('rID', 0, 'INT');
		endif;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$res 		= JTable::getInstance('Theresults','JclassroomTable',array());
		$res->load($resultID);
		$unitID = $res->quizzID;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$unit 		= JTable::getInstance('Unit','JclassroomTable',array());
		$unit->load($unitID);
		$unitName = $unit->title;
		$html = '';
		$html .= '<div class="print" id="printHeader" style="width: 100%; margin-bottom: 20px;">';
        $html .= '<img style="display: inline;" src="images/logo-black.png" />';
        $html .= '<h1 class="float-right" style="font-size: 56px;margin-top: 13px;text-align: right;">Zertifikat<br/><span class="float-right" style="font-size: 24px;font-weight: bold;">Quizz: <i>'.$unitName.'</i></span></h1>';
        $html .= '</div>';
		JLoader::register('StageresultHelper',JPATH_COMPONENT_SITE.'/helpers/stageResult.php');
        $template = new StageresultHelper();
        $html .= $template->getResult($resultID, 0, 0, 0, 0, 0, $unitID,0);
        return  $html;
	}
	
	function getQuizzResultOLD($quizz) {
		$session 	= JFactory::getSession();
		$questions 	= $session->get('quizzes');
		$counter = 1;
		$html = '';
		$html .= '<table class="table table-striped">';
		$html .= '<thead>';
		$html .= '<tr>';
		$html .= '<th width="50%">Frage</th>';
		//if($quizz->type != 1 && $quizz->calculate == 2):
			$html .= '<th width="30%" class="text-center">Antwortmöglichkeiten</th>';
		//endif;
		$html .= '<th width="30%" class="text-center">Ihre Antwort</th>';
		if($quizz->calculate == 2):
			$html .= '<th width="10%" class="text-center">Punkte</th>';
		endif;
		$html .= '</tr>';
		$html .= '</thead>';
		$html .= '<tbody>';
		$db 		= JFactory::getDbo();
		foreach($questions as $question):
			$html .= '<tr>';
			//Get the Question
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$quest 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
			$quest->load($question['questionID']);
			$points = $quest->points;
			$html .= '<td><h5><b>'.$counter.'. '.$quest->title.'</b></h5></td>';
			// WWM
			if($quest->type == 3):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$ans 		= JTable::getInstance('Quizz_answers','JclassroomTable',array());
				$ans->load($question->answerID);
				//if($quizz->calculate == 2):
					$query 	= $db->getQuery(true);
					$query->select(array('a.*'));
			        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
					$query->where($db->quoteName('a.questionID').' = '.$db->quote($question['questionID']));
					$db->setQuery($query);
					$corrects 	= $db->loadObjectList();
					if($corrects):
						$correctSoll 	= 0;
						$correctIst 	= 0;
						$html .= '<td style="padding:0;">';
						$html .= '<table style="width: 100%;">';
						foreach($corrects as $correct):
							if($correct->correct == 1):
								$bgS = 'bg-success';
								$correctSoll++;
							else:
								$bgS = 'bg-danger';
							endif;
							$html .= '<tr>';
							$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$correct->title.'</span></td>';
							$html .= '</tr>';
						endforeach;
						$html .= '</table>';
						$html .= '</td>';
						$html .= '<td style="padding:0;">';
						$html .= '<table style="width: 100%;">';
						foreach($corrects as $correct):
							echo $correct->correct.'<br/>';
							if($cart):
								foreach($cart as $part):
									print_r($part['content']);
								endforeach;
							endif;
							$html .= '<tr>';
							if($correct->correct == 1 && $result->id):
								$html .= '<td style="padding: 3px;"><span class="p-1 d-flex text-white w-100 badge bg-success" style="justify-content: center;"><i class="fa fa-check"></i>'.$correct->title.'</span></td>';
								$correctIst++;
							endif;
							if($correct->correct == 1 && !$result->id):
								$html .= '<td style="padding: 3px;"><span style="color: rgba(0,0,0,0);" class="p-1 d-block w-100 badge">NIX</span></td>';
							endif;
							if($correct->correct == 0 && $result->id):
								$html .= '<td style="padding: 3px;"><span class="p-1 d-flex text-white w-100 badge bg-danger" style="justify-content: center;"><i class="fa fa-ban"></i>'.$correct->title.'</span></td>';
							endif;
							if($correct->correct == 0 && !$result->id):
								$html .= '<td style="padding: 3px;"><span style="color: rgba(0,0,0,0);" class="p-1 d-block w-100 badge">NIX</span></td>';
							endif;
							$html .= '</tr>';
						endforeach;

						$html .= '</table>';
						$html .= '</td>';
					endif;
					if($correctSoll == $correctIst):
						$html .= '<td class="text-center">'.$points.'</td>';
						$gesPoints += $points;
					else:
						$html .= '<td class="text-center">0</td>';
					endif;
					$maxPoints += $quest->points;
			endif;
			// Smileys
			if($quest->type == 4):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
				$result->load($question->resultPositionID);
				if($quizz->calculate == 2):
					$points = 0;
					$html .= '<td style="padding:0px;">';
					$html .= '<table style="width: 100%;font-size: 11px;">';
					$html .= '<tr>';
					$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #BC1A3D; color:#fff;font-size: 14px;text-align: center;">Sehr schlecht</span></td>';
					$html .= '</tr>';
					$html .= '<tr>';
					$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #DD4610;color:#fff;font-size: 14px;text-align: center;">Schlecht</span></td>';
					$html .= '</tr>';
					$html .= '<tr>';
					$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #F4D532;color:#fff;font-size: 14px;text-align: center;">Nicht schlecht</span></td>';
					$html .= '</tr>';
					$html .= '<tr>';
					$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #A9CB44;color:#fff;font-size: 14px;text-align: center;">Gut</span></td>';
					$html .= '</tr>';
					$html .= '<tr>';
					$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #538930;color:#fff;font-size: 14px;text-align: center;">Sehr gut</span></td>';
					$html .= '</tr>';
					$html .= '</table>';
					$html .= '</td>';
					$html .= '<td style="padding:0px;">';
					$html .= '<table style="width: 100%;font-size: 11px;">';
					$html .= '<tr>';
					if($result->answerID == '1'):
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #BC1A3D; color:#fff;font-size: 14px;text-align: center;"><i class="fa fa-check"></i>Sehr schlecht</span></td>';
						$points 	= ($quest->points / 5) * 1;
					else:	
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="color:rgba(0,0,0,0);font-size: 14px;text-align: center;">Ja</span></td>';
					endif;
					$html .= '</tr>';
					$html .= '<tr>';
					if($result->answerID == '2'):
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #DD4610;color:#fff;font-size: 14px;text-align: center;"><i class="fa fa-check"></i>Schlecht</span></td>';
						$points 	= ($quest->points / 5) * 2;
					else:		
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="color:rgba(0,0,0,0);font-size: 14px;text-align: center;">Ja</span></td>';
					endif;
					$html .= '</tr>';
					$html .= '<tr>';
					if($result->answerID == '3'):
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #F4D532;color:#fff;font-size: 14px;text-align: center;"><i class="fa fa-check"></i> Nicht schlecht</span></td>';
						$points 	= ($quest->points / 5) * 3;
					else:		
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="color:rgba(0,0,0,0);font-size: 14px;text-align: center;">Ja</span></td>';
					endif;
					$html .= '</tr>';
					$html .= '<tr>';
					if($result->answerID == '4'):
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #A9CB44;color:#fff;font-size: 14px;text-align: center;"><i class="fa fa-check"></i> Gut</span></td>';
						$points 	= ($quest->points / 5) * 4;
					else:		
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="color:rgba(0,0,0,0);font-size: 14px;text-align: center;">Ja</span></td>';
					endif;
					$html .= '</tr>';
					$html .= '<tr>';
					if($result->answerID == '5'):
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #538930;color:#fff;font-size: 14px;text-align: center;"><i class="fa fa-check"></i> Sehr gut</span></td>';
						$points 	= $quest->points;
					else:		
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="color:rgba(0,0,0,0);font-size: 14px;text-align: center;">Ja</span></td>';
					endif;
					$html .= '</tr>';
					$html .= '</table>';
					$html .= '</td>';	
					$html .= '<td class="text-center">'.$points.'</td>';
					$gesPoints += $points;
				else:
					$html .= '<td class="p-1"><span class="d-block w-100 badge" style="background-color: #B0C4DE">'.$result->content.'</span></td>';
				endif;
				$maxPoints += $quest->points;
			endif;
			// Ja/Nein
			if($quest->type == 5):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
				$result->load($question->resultPositionID);
				// 0 = Ja, 1 = Nein
				$correctAnswer 	= $quest->correctAnswer;
				switch($correctAnswer):
					case 0:
						$answS5 	= 'Ja';
						break;
					case 1:
						$answS5 	= 'Nein';
						break;
				endswitch;
				switch($result->content):
					case 'answerJ':
						$answI5 	= 'Ja';
						break;
					case 'answerN':
						$answI5 	= 'Nein';
						break;
				endswitch;
				if($quizz->calculate == 2):
					$points = 0;
					if($answS5 == $answI5):
						$points = $quest->points;
						$gesPoints += $points;
						$bgR 	= 'bg-success';
						$icon 	= '<i class="fa fa-check"></i> ';
					else:
						$bgR 	= 'bg-danger';
						$icon 	= '<i class="fa fa-ban"></i> ';
					endif;
					$html .= '<td style="padding: 3px;"><span style="font-size: 14px;" class="text-white d-block w-100 p-1 badge bg-success"> '.$answS5.'</span></td>';
					$html .= '<td style="padding: 3px;"><span style="font-size: 14px;" class="d-block text-white w-100 p-1 badge '.$bgR.'">'.$icon.$answI5.'</span></td>';
					$html .= '<td class="text-center">'.$points.'</td>';
				else:
					$html .= '<td class="p-1 bg bg-success text-center">'.$answI5.'</td>';
				endif;
				$maxPoints += $quest->points;
			endif;
			// Audit Frage
			if($quest->type == 6):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
				$result->load($question->resultPositionID);
				switch($result->content):
					case 'A':
						$answI5 	= 'Ja';
						$bgI 		= 'forestgreen';
						break;
					case 'AA':
						$answI5 	= 'Ja, mit Einschränkung';
						$bgI 		= 'orange';
						break;
					case 'N':
						$answI5 	= 'Nein';
						$bgI 		= 'firebrick';
						break;
				endswitch;
				if($quizz->calculate == 2):
					$points = 0;
					$html .= '<td style="padding:0px;">';
					$html .= '<table style="width: 100%;font-size: 11px;">';
					$html .= '<tr>';
					$html .= '<td class="p-1"><span class="bg-success d-block w-100 badge" style="color:#fff;font-size: 14px;text-align: center;">Ja</span></td>';
					$html .= '</tr>';
					$html .= '<tr>';
					$html .= '<td class="p-1"><span class="bg-warning d-block w-100 badge" style="color:#fff;font-size: 14px;text-align: center;">Ja, mit Einschränkung</span></td>';
					$html .= '</tr>';
					$html .= '<tr>';
					$html .= '<td class="p-1"><span class="bg-danger d-block w-100 badge" style="color:#fff;font-size: 14px;text-align: center;">Nein</span></td>';
					$html .= '</tr>';
					$html .= '</table>';
					$html .= '</td>';
					$html .= '<td style="padding:0px;">';
					$html .= '<table style="width: 100%;font-size: 11px;">';
					$html .= '<tr>';
					if($answI5 == 'Ja'):
						$html .= '<td class="p-1"><span class="bg-success d-block w-100 badge" style="color:#fff;font-size: 14px;text-align: center;"><i class="fa fa-check"></i>Ja</span></td>';
						$points 	= $quest->points;
					else:	
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="color:rgba(0,0,0,0);font-size: 14px;text-align: center;">Ja</span></td>';
					endif;
					$html .= '</tr>';
					$html .= '<tr>';
					if($answI5 == 'Ja, mit Einschränkung'):
						$html .= '<td class="p-1"><span class="bg-warning d-block w-100 badge" style="color:#fff;font-size: 14px;text-align: center;"><i class="fa fa-check"></i>Ja, mit Einschränkung</span></td>';
						if($result->exeptionPoints):
								$points 	= $result->exeptionPoints;
							else:
								$points 	= $quest->points;
							endif;
					else:		
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="color:rgba(0,0,0,0);font-size: 14px;text-align: center;">Ja</span></td>';
					endif;
					$html .= '</tr>';
					$html .= '<tr>';
					if($answI5 == 'Nein'):
						$html .= '<td class="p-1"><span class="bg-danger d-block w-100 badge" style="color:#fff;font-size: 14px;text-align: center;"><i class="fa fa-ban"></i> Nein</span></td>';
					else:		
						$html .= '<td class="p-1"><span class="d-block w-100 badge" style="color:rgba(0,0,0,0);font-size: 14px;text-align: center;">Ja</span></td>';
					endif;
					$html .= '</tr>';
					$html .= '</table>';
					$html .= '</td>';	
					$html .= '<td class="text-center">'.$points.'</td>';
					$gesPoints += $points;
				else:
					$html .= '<td class="p-1 bg bg-success text-center">'.$answI5.'</td>';
				endif;
				$maxPoints += $quest->points;
			endif;
			// Textfeld Frage
			if($quest->type == 10):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
				$result->load($question->resultPositionID);
				if($quizz->calculate == 2):
					if($result->content):
						$points 	= $quest->points;
						$gesPoints += $points;
						$maxPoints += $quest->points;
					else:
						$points = 0;
					endif;
					$html .= '<td></td>';
					$html .= '<td style="padding:3px;"><span style=font-size:12px;" class="p-1 badge bg-primary text-white text-center">'.$result->content.'</span></td>';
					$html .= '<td class="text-center">'.$points.'</td>';
					
				else:
					$html .= '<td class="p-1 bg bg-success text-center">'.$result->content.'</td>';
				endif;
			endif;
			// Textarea Frage
			if($quest->type == 11):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$result 		= JTable::getInstance('Quizz_results','JclassroomTable',array());
				$result->load($question->resultPositionID);
				if($quizz->calculate == 2):
					if($result->content):
						$points 	= $quest->points;
						$gesPoints += $points;
						$maxPoints += $quest->points;
					else:
						$points = 0;
					endif;
					$html .= '<td></td>';
					$html .= '<td style="padding:3px;"><span style=font-size:12px;" class="p-1 badge bg-primary text-white text-center">'.$result->content.'</span></td>';
					$html .= '<td class="text-center">'.$points.'</td>';
					
				else:
					$html .= '<td class="p-1 bg bg-success text-center">'.$result->content.'</td>';
				endif;
			endif;
			// Auswahlliste
			if($quest->type == 12):
				$query 	= $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
				$db->setQuery($query);
				$corrects 	= $db->loadObjectList();
				if($quizz->calculate == 2):
					if($corrects):
						$correctSoll 	= 0;
						$correctIst 	= 0;
						$html .= '<td style="padding:0;">';
						$html .= '<table style="width: 100%;">';
						foreach($corrects as $correct):
							if($correct->correct == 1):
								$bgS = 'bg-success';
								$correctSoll++;
							else:
								$bgS = 'bg-danger';
							endif;
							$html .= '<tr>';
							$html .= '<td style="padding:3px;"><span class="p-1 d-block text-white w-100 badge '.$bgS.'">'.$correct->title.'</span></td>';
							$html .= '</tr>';
						endforeach;
						$html .= '</table>';
						$html .= '</td>';
						$html .= '<td style="padding:0;">';
						$html .= '<table style="width: 100%;">';
						foreach($corrects as $correct):
							$query 	= $db->getQuery(true);
							$query->select(array('a.*'));
					        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					        $query->where($db->quoteName('a.theResultID').' = '.$db->quote($question->theResultID));
					        $query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quoteName('a.answerID').' = '.$db->quote($correct->id));
							$db->setQuery($query);
							$result 	= $db->loadObject();
							$html .= '<tr>';
							if($correct->correct == 1 && $result):
								$bgI = 'bg-success';
								$html .= '<td style="padding: 3px;"><span class="p-1 d-block w-100 text-white badge '.$bgI.'"><i class="fa fa-check"></i> '.$correct->title.'</span></td>';
								$correctIst++;
							endif;
							if($correct->correct == 1 && !$result):
								$html .= '<td style="padding: 3px;"><span style="color: rgba(0,0,0,0);" class="p-1 d-block w-100 badge">NIX</span></td>';
							endif;
							if($correct->correct == 0 && !$result):
								$html .= '<td style="padding: 3px;"><span style="color: rgba(0,0,0,0);" class="p-1 d-block w-100 badge">NIX</span></td>';
							endif;
							if($correct->correct == 0 && $result):
								$html .= '<td style="padding: 3px;"><span class="p-1 d-block w-100 text-white badge bg-danger"><i class="fa fa-ban"></i> '.$correct->title.'</span></td>';
							endif;
							$html .= '</tr>';
						endforeach;
						$html .= '</table>';
						$html .= '</td>';
						
					endif;
					if($correctSoll == $correctIst):
						$html .= '<td class="text-center">'.$points.'</td>';
						$gesPoints += $points;
					else:
						$html .= '<td class="text-center">0</td>';
					endif;
					$maxPoints += $quest->points;
				else:
					$html .= '<td style="padding: 0px;">';
					$html .= '<table style="width:100%;">';
					foreach($corrects as $correct):
						$query 	= $db->getQuery(true);
						$query->select(array('a.*'));
				        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
				        $query->where($db->quoteName('a.theResultID').' = '.$db->quote($question->theResultID));
				        $query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
						$query->where($db->quoteName('a.answerID').' = '.$db->quote($correct->id));
						$db->setQuery($query);
						$result 	= $db->loadObject();
						if($result):	
							$html .= '<tr>';
							$html .= '<td style="padding: 3px;"><span class="p-1 d-block w-100 badge" style="background-color: lightsteelblue;">'.$correct->title.'</span></td>';
							$html .= '</tr>';
						endif;
					endforeach;
					$html .= '</table>';
					$html .='</td>';
				endif;
			endif;
			// Checkboxen
			if($quest->type == 13):
				$query 	= $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
				$db->setQuery($query);
				$corrects 	= $db->loadObjectList();
				if($quizz->calculate == 2):
					if($corrects):
						$correctSoll 	= 0;
						$correctIst 	= 0;
						$html .= '<td style="padding:0;">';
						$html .= '<table style="width: 100%;">';
						foreach($corrects as $correct):
							if($correct->correct == 1):
								$bgS = 'bg-success';
								$correctSoll++;
							else:
								$bgS = 'bg-danger';
							endif;
							$html .= '<tr>';
							$html .= '<td style="padding:3px;"><span class="p-1 d-block text-white w-100 badge '.$bgS.'">'.$correct->title.'</span></td>';
							$html .= '</tr>';
						endforeach;
						$html .= '</table>';
						$html .= '</td>';
						$html .= '<td style="padding:0;">';
						$html .= '<table style="width: 100%;">';
						foreach($corrects as $correct):
							$query 	= $db->getQuery(true);
							$query->select(array('a.*'));
					        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
					        $query->where($db->quoteName('a.theResultID').' = '.$db->quote($question->theResultID));
					        $query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
							$query->where($db->quoteName('a.answerID').' = '.$db->quote($correct->id));
							$db->setQuery($query);
							$result 	= $db->loadObject();
							$html .= '<tr>';
							if($correct->correct == 1 && $result):
								$bgI = 'bg-success';
								$html .= '<td style="padding: 3px;"><span class="p-1 d-block w-100 text-white badge '.$bgI.'"><i class="fa fa-check"></i> '.$correct->title.'</span></td>';
								$correctIst++;
							endif;
							if($correct->correct == 1 && !$result):
								$html .= '<td style="padding: 3px;"><span style="color: rgba(0,0,0,0);" class="p-1 d-block w-100 badge">NIX</span></td>';
							endif;
							if($correct->correct == 0 && !$result):
								$html .= '<td style="padding: 3px;"><span style="color: rgba(0,0,0,0);" class="p-1 d-block w-100 badge">NIX</span></td>';
							endif;
							if($correct->correct == 0 && $result):
								$html .= '<td style="padding: 3px;"><span class="p-1 d-block w-100 text-white badge bg-danger"><i class="fa fa-ban"></i> '.$correct->title.'</span></td>';
							endif;
							$html .= '</tr>';
						endforeach;
						$html .= '</table>';
						$html .= '</td>';
						if($correctSoll == $correctIst):
							$html .= '<td class="text-center">'.$points.'</td>';
							$gesPoints += $points;
						else:
							$html .= '<td class="text-center">0</td>';
						endif;
						$maxPoints += $quest->points;
					endif;
				else:
					$html .= '<td style="padding: 0px;">';
					$html .= '<table style="width:100%;">';
					foreach($corrects as $correct):
						$query 	= $db->getQuery(true);
						$query->select(array('a.*'));
				        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
				        $query->where($db->quoteName('a.theResultID').' = '.$db->quote($question->theResultID));
				        $query->where($db->quoteName('a.questionID').' = '.$db->quote($question->questionID));
						$query->where($db->quoteName('a.answerID').' = '.$db->quote($correct->id));
						$db->setQuery($query);
						$result 	= $db->loadObject();
						if($result):	
							$html .= '<tr>';
							$html .= '<td style="padding: 3px;"><span class="p-1 d-block w-100 badge" style="background-color: lightsteelblue;">'.$correct->title.'</span></td>';
							$html .= '</tr>';
						endif;
					endforeach;
					$html .= '</table>';
					$html .='</td>';
				endif;
			endif;
			$html .'</tr>';
			$counter++;
		endforeach;
		$html .= '</tbody>';
		if($quizz->calculate == 2):
			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td><b>Ihre erreicht Punktzahl (von maximal '.$maxPoints.')</b></td>';
			if($quizz->type != 1 && $quizz->calculate == 2):
				$html .= '<td></td>';
				$html .= '<td></td>';
			else:
				$html .= '<td></td>';
			endif;
			$html .= '<td class="text-center"><b>'.$gesPoints.'</b></td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td><b>Dies entspricht einem Score von</b></td>';
			if($quizz->type != 1 && $quizz->calculate == 2):
				$html .= '<td></td>';
				$html .= '<td></td>';
			else:
				$html .= '<td></td>';
			endif;
			$score = $gesPoints * 100/ $maxPoints;
			$html .= '<td class="text-center"><b>'.round($score,2).'%</b></td>';
			$html .= '</tr>';
			$html .= '</tfoot>';
		endif;
		$html .= '</table>';

		return $html;
	}
	function getAuditResult($quizz, $theResultID) {
		echo $quizz->id;
		$html = '';
		$theScoreMax 	= 0;
		$theScorePoints = 0;
		// LOAD the textfields
		$db 		= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizz->id));
		$query->where($db->quoteName('a.type').' = 14');
		$query->group('a.testfield');
		$query->order('a.groupOrdering ASC');
		$db->setQuery($query);
		$testfields 	= $db->loadObjectList();
		if($testfields):
			foreach($testfields as $testfield):
				$html .= '<h1 style="font-size: 24px;"><span class="badge bg-success text-white mr-1">'.$testfield->title.'</span></h1>';
				$html .= $testfield->content;
				$query 	= $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizz->id));
				$query->where($db->quoteName('a.testfield').' = '.$db->quote($testfield->title));
				$query->where($db->quoteName('a.type').' <> '.$db->quote(14));
				//$query->group('a.theme');
				$db->setQuery($query);
				$questions 	= $db->loadObjectList();
				if($questions):
					$html .= '<table class="table table-striped">';
					$html .= '<thead>';
					$html .= '<tr>';
					$html .= '<th>Frage</th>';
					if($quizz->type != 1 && $quizz->calculate == 2):
						$html .= '<th>Richtige Antwort</th>';
					endif;
					$html .= '<th>Ihre Antwort</th>';
					if($quizz->calculate == 2):
						$html .= '<th>Punkte</th>';
					endif;
					$html .= '</tr>';
					$html .= '</thead>';
					$html .= '<tbody>';
					$maxPoints 	= 0;
					$points 	= 0;
					foreach($questions as $question):
						$maxPoints += $question->points;
						$theScoreMax += $question->points;
						$html .= '<tr>';
						//Get the Question
						JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
						$result 	= JTable::getInstance('Quizz_results','JclassroomTable',array());
						$load 		= array('theResultID' => $theResultID,'questionID' => $question->id);
						$result->load($load);
						$html .= '<td><h5 style="font-size: 16px;"><b>'.$question->title.'</b></h5>'.$question->content.'<h2 style="font-size: 18px;"><span class="badge bg-primary text-white">'.$question->theme.'</span></h2></td>';
						if($result->content == "A"):
							$html .= '<td><span class="card bg-success p-1 text-white">'.$question->infotextPositiv.'</span></td>';
							$html .= '<td class="text-center">'.$question->points.'</td>';
							$points += $question->points;
						endif;
						if($result->content == "AA"):
							$html .= '<td>';
							$html .= '<span class="card bg-warning p-1 text-dark">'.$question->infotextPositiv.'</span>';
							$html .= '<small style="font-weight: bold;font-style: italic;">'.$result->exeptionText.'</small>';
							$html .= '</td>';
							if($result->exeptionPoints != 0):
								$thePoints = $result->exeptionPoints;
							else:
								$thePoints = $question->points;
							endif; 
							$html .= '<td class="text-center">'.$thePoints.'</td>';
							$points += $thePoints;
						endif;
						if($result->content == "N"):
							$html .= '<td><span class="card bg-danger p-1 text-white">'.$question->infotextNegativ.'</span></td>';
							$html .= '<td class="text-center">0</td>';
							$points += 0;
						endif;
						$html .'</tr>';
					endforeach;
					$html .= '</tbody>';
					if($quizz->calculate == 2):
						$theScorePoints += $points;
						$html .= '<tfoot>';
						$html .= '<tr>';
						$html .= '<td><b>Ihre erreicht Punktzahl im Thema '.$question->theme.' (von maximal '.$maxPoints.')</b></td>';
						if($quizz->type != 1 && $quizz->calculate == 2):
							$html .= '<td></td>';
							$html .= '<td></td>';
						else:
							$html .= '<td></td>';
						endif;
						$html .= '<td class="text-center"><b>'.$points.'</b></td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td><b>Dies entspricht einem Score von</b></td>';
						if($quizz->type != 1 && $quizz->calculate == 2):
							$html .= '<td></td>';
							$html .= '<td></td>';
						else:
							$html .= '<td></td>';
						endif;
						$score = $points * 100/ $maxPoints;
						$html .= '<td class="text-center"><b>'.round($score,2).'%</b></td>';
						$html .= '</tr>';
						$html .= '</tfoot>';
					endif;
					$html .= '</table>';
				endif;
			endforeach;
		endif;
		if($quizz->calculate == 2):
			$html .= '<table class="table table-striped">';
				$html .= '<tfoot>';
				$html .= '<tr>';
				$html .= '<td><b>Gesamtscore</b></td>';f;
				$score = $theScorePoints * 100/ $theScoreMax;
				$html .= '<td class="text-right"><b style="font-size: 32px;" class="badge bg-success text-white">'.round($score,2).'%</b></td>';
				$html .= '</tr>';
				$html .= '</tfoot>';
			$html .= '</table>';
		endif;

		return $html;
	}
	function getResultText($score) {
		$input 		= JFactory::getApplication()->input;
		$unitID 	= $input->get('id', 0, 'INT');
		$option = array(); 
 
		$option['driver']   = 'mysql';  	// Database driver name
		$option['host']     = 'localhost'; 	// Database host name
		$option['user']     = 'cede09';    	// User for database authentication
		$option['password'] = 'Eriwan991';   // Password for database authentication
		$option['database'] = 'cede09';    	// Database name
		$option['prefix']   = 'invce_';     // Database prefix (may be empty)
		
		$db 		= JDatabaseDriver::getInstance( $option );
		$query 		= $db->getQuery(true);
		$query->select(array('a.result'));
        $query->from($db->quoteName('#__audit_unit_results','a'));
		$query->where($db->quoteName('a.unitID').' = '.$db->quote($unitID));
		$query->where($db->quoteName('a.fromPercent').' <= '.$db->quote($score));
		$query->where($db->quoteName('a.tilPercent').' >= '.$db->quote($score));
		$db->setQuery($query);
		$result = $db->loadResult();

		return $result;
	}
	function getPoints($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('SUM(a.points) as points');
        $query->from($db->quoteName('#__audit_resultelements','a'));
		$query->join('LEFT', $db->quoteName('#__audit_results', 'b') . ' ON (' . $db->quoteName('a.resultID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('b.id').' = '.$db->quote($resultID));
		//$query->group('a.resultID');
		$db->setQuery($query);
		$points = $db->loadResult();
		
		return $points;
	}
	function getTimes($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.start, a.stop'));
        $query->from($db->quoteName('#__audit_times','a'));
		$query->join('LEFT', $db->quoteName('#__audit_results', 'b') . ' ON (' . $db->quoteName('a.resultID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('b.id').' = '.$db->quote($resultID));
		//$query->group('a.resultID');
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}
	function getResultS($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__audit_results','a'));
		$query->where($db->quoteName('a.id').' = '.$db->quote($resultID));
		$db->setQuery($query);
		$result = $db->loadObject();
		$elements = $this->getResultelements($resultID);
		$html = '';
		if($result) {
			$i = 1;
			$html .= '<table class="table table-striped table-sm">';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<th>Frage Nr.</th>';
			$html .= '<th>Frage</th>';
			$html .= '<th>Antwort</th>';
			$html .= '<th>Max. Punkte</th>';
			$html .= '<th class="text-right">Erreichte Punkte</th>';
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			$sum_points = 0;
			$max_points = 0;
			foreach($elements as $item) {
				if($item->soll != $item->ist) {
					$color = 'text-danger';
				} else {
					$color = 'text-success';
				}
				$html .= '<tr>';
				$html .= '<td>'.$i.'</td>';
				$html .= '<td class="'.$color.'"><b>'.$item->name.'</b></td>';
				$html .= '<td>'.$item->answers.'</td>';
				$html .= '<td>'.$item->max_points.'</td>';
				$html .= '<td class="text-right">'.$item->sum_points.'</td>';
				$html .= '</tr>';
				$i++;
				$max_points += $item->max_points;
				$sum_points += $item->sum_points;
			}
			$html .= '</tbody>';
			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td colspan="3">In dieser Unit erreichte Punkte:</td>';
			$html .= '<td>'.$max_points.'</td>';
			$html .= '<td class="text-right">'.$sum_points.'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="4">Entspricht einem Highscore von:</td>';
			$html .= '<td class="text-right">'.number_format(($sum_points * 100) / $max_points,2,',','.').'%</td>';
			$html .= '</tr>';
			$times = $this->getAllTimes($resultID);
			$html .= '<tr>';
			$html .= '<td colspan="4">Benötigte Zeit:</td>';
			$html .= '<td class="text-right">'.$times.'</td>';
			$html .= '</tr>';
			$html .= '</tfoot>';
			$html .= '</table>';
		}

		return $html;
	}
	function getResultelements($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*,
			a.points as sum_points,
			a.answers as ist,
			b.name,
			b.answers as soll,
			c.points as max_points
		'));
        $query->from($db->quoteName('#__audit_resultelements','a'));
        $query->join('LEFT', $db->quoteName('#__audit_quizzes', 'b') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('b.id') . ')');
        $query->join('LEFT', $db->quoteName('#__audit_unitelements', 'c') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('c.quizzID') . ')');
		$query->where($db->quoteName('a.resultID').' = '.$db->quote($resultID));
		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;
	}
	function getAllTimes($resultID) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*
		'));
        $query->from($db->quoteName('#__audit_times','a'));
		$query->where($db->quoteName('a.resultID').' = '.$db->quote($resultID));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$dauer = 0;
		if($result) {
			foreach($result as $item) {
				$dauer += $item->stop - $item->start;
			}
		}
		$dauer = (($dauer / 60) % 60).' Minuten';

		return $dauer;
	}
	/**
	 * Increment the hit counter for the item.
	 *
	 * @param   integer  $pk  Optional primary key of the item to increment.
	 *
	 * @return  boolean  True if successful; false otherwise and internal error set.
	 */
	public function hit($pk = 0)
	{
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);

		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('fragenkatalog.id');

			$table = $this->getTable();
			$table->load($pk);
			$table->hit($pk);
		}

		return true;
	}
}
?>