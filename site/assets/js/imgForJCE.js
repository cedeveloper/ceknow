// For use: the name (id) of the editor is required. It will be given by a div-container, in which the button #imageChoose is inside
// This div-container must have the class "use_editor" and a attribut data-editor with the name of the editor, e.g. jform_description
jQuery(function ($) {
	$(document).ready(function() {
		let editor = false;
		$('.imageChoose').each(function() {
			editor = $(this).parent().parent('.use_editor').data('editor');
			$(this).attr('onclick', 'insertContentImage("' + editor + '");');
		});
	});
	$('#uploadJCE').on('change', function(e) {
		let theFiles      = document.getElementById('uploadJCE').files;
		var form_data     = new FormData();
		for(i = 0; i < theFiles.length; i++) {
			form_data.append('uploadUnit[]', theFiles[i]);
		}
		form_data.append('classroomID', classroomID);
		form_data.append('unitID', unitID);
		form_data.append('part', part);
		$('#waitForWork').css('display', 'flex');
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=global.loadFileContent",
			data: form_data,
			processData: false,
			contentType: false,
			//dataType: 'json',
			success: function( data ) {
				hideSaveJCE();
				$('#readyForWork').css('display', 'flex');
				hideSaveJCE();
			}
		});
	});
});
function chooseThisFile(id) {
	if($('#image' + id).hasClass('active')) {
		$('#image' + id).removeClass('active');
		$('#theChoosenImageID').val('');
	} else {
		$('.image').removeClass('active');
		$('#image' + id).addClass('active');
		$('#theChoosenImageID').val(id);
	}
}
function uploadContentImage() {
	let unitID = $('#editContentUnitID').val();
	jQuery('#contentImageUnitID').val(unitID);
	jQuery('#uploadJCE').trigger('click');
}
function insertImage() {
	let imageID = $('#theChoosenImageID').val();
	let editor 	= $('#insertImage #editor').val();
	let width 	= $('#insertImage #width').val();
	let height 	= $('#insertImage #height').val();
	let border 	= $('#insertImage #border').val();
	let setInAsFile 	= $('#insertImage #setInAsFile').val();
	let setInAsFileID 	= $('#insertImage #setInAsFileID').val();
	if(!editor && setInAsFile == false) {
		editor = 'jform_content';
	}
	$('#insertImage').modal('hide');
	if(!setInAsFile) {
		$('#editContent').css('display', 'block');
	}
	jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=global.loadImageTag",
		data: {
			imageID:imageID,
			width:width,
			height:height,
			border:border,
			setInAsFile: setInAsFile,
			setInAsFileID: setInAsFileID
		},
		//dataType: 'json',
		success: function( data ) {
			if(!setInAsFile) {
				tinymce.get(editor).execCommand('mceInsertContent', false, data);
			}
			if(setInAsFile == 'file') {
				$('#uploads' + setInAsFileID).append(data);
			}
		}
	});
}
function insertContentImage(editor, setInAsFile = false, setInAsFileID = false) {
	jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=global.loadContentImages",
		data: {
			part:part,
			classroomID:classroomID,
			unitID:unitID
		},
		//dataType: 'json',
		success: function( data ) {
			$('#insertImage #contentImages').empty();
			$('#insertImage #contentImages').html(data);
		}
	});

	if(setInAsFile == false) {
		$('#editContent').css('display', 'none');
		$('#insertImage #editor').val(editor);
		$('#insertImage #setInAsFile').val('');
		$('#insertImage #setInAsFileID').val('');
	} else {
		$('#insertImage #setInAsFile').val(setInAsFile);
		$('#insertImage #setInAsFileID').val(setInAsFileID);
	}
	
	$('#insertImage').modal('show');
}
function showContent() {
	$('#editContent').css('display', 'block');
	$('#insertImage').modal('hide');
}
function chooseImage(imageID) {
	
}
function deleteImage(unitID, imageID) {
	if(confirm("Soll das Bild gelöscht werden?")== true) {
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=global.deleteImage",
			data: {
				unitID:unitID,
				imageID:imageID
			},
			//dataType: 'json',
			success: function( data ) {
				$('#image' + imageID).remove();
			}
		});
		
	}
}
function hideSaveJCE() {
	setTimeout(hideSaveExecuteJCE,800);
 }
 function hideSaveExecuteJCE() {
	jQuery('.wait').slideUp(200);
 }
 function chooseUnit(unitID) {
	insertContentImage(false, 'file', unitID);
 }