jQuery(function ($) {
	

	$('#saveSections').on('click', function() {
		$('.section').each(function() {
			console.log($(this));
		})
	});

	// Fire on click on class t5answer
	$('.t5answer').on('click', function() {
		var answer  = $(this).attr('id');
		answer      = answer.replace('answer', '');
		$('#t5answer').val(answer);
		$('.t5answer').each(function() {
			$(this).addClass('disabled');
		});
		$(this).removeClass('btn-success').addClass('btn-danger');
	});

	$('#check').on('click', function() {
		var quizzID 	= $('#quizzID').val();
		var unitID 		= $('#unitID').val();
		var position 	= $('#position').val();
		if($('#quizzTyp').val() == 1) {
		var answers = new Array();
		$('.custom-select').each(function() {
		if($(this).val()) {
		answers.push($(this).val());
		}
		})
		if(answers.length != $('#countQue').val()) {
		alert("Bitte beantworten Sie zunächst alle Fragen");
		return false;
		}
		jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=quizz.checkQuizz",
		data: {quizzID:quizzID, answers:answers},
		dataType: 'json',
		success: function( data ) {
		$('#result .result-inner').empty();
		var hs      = 0;
		$(data).each(function(e, value) {
		var antwort = '';
		var color   = '';

		if(value.result == 0) {
		antwort  = 'Leider Falsch';
		color    = 'text-danger';
		}
		if(value.result == 1) {
		antwort  = 'Richtig';
		color    = 'text-success';
		hs++;
		}

		$('#result .result-inner').append(
		'<p class="mt-1 mb-1 ' + color + '"><b>Frage ' + value.antwort + ': ' + antwort + '</b></p>'
		);

		});
		hs = (hs * 100) / $('#countQue').val();
		hs = Math.ceil(hs);
		$('#result .result-inner').append(
		'<h2>Highscore: ' + hs + '%</h2>'
		);
		$('#check').addClass('disabled');
		}
		});
		}
		if($('#quizzTyp').val() == 2) {
		if(t2Choices.length != $('#countQue').val()) {
		alert("Bitte beantworten Sie zunächst alle Fragen");
		return false;
		}
		jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=quizz.checkQuizz2",
		data: {quizzID:quizzID, t2Choices:t2Choices},
		dataType: 'json',
		success: function( data ) {
		$('#result .result-inner').empty();
		var hs      = 0;
		$(data).each(function(e, value) {
		var antwort = '';
		var color   = '';

		if(value.result == 0) {
		antwort  = 'Leider Falsch';
		color    = 'text-danger';
		}
		if(value.result == 1) {
		antwort  = 'Richtig';
		color    = 'text-success';
		hs++;
		}

		$('#result .result-inner').append(
		'<p class="mt-1 mb-1 ' + color + '"><b>Frage ' + value.antwort + ': ' + antwort + '</b></p>'
		);

		});
		hs = (hs * 100) / $('#countQue').val();
		hs = Math.ceil(hs);
		$('#result .result-inner').append(
		'<h2>Highscore: ' + hs + '%</h2>'
		);
		t2Choices = new Array();
		$('#check').addClass('disabled');
		}
		});
		}
		if($('#quizzTyp').val() == 5) {
			var answer = $('#t5answer').val();
			if(!answer) {
				alert("Bitte beantworten Sie zunächst die Frage");
				return false;
			}
			jQuery.ajax({
				type: "POST",
				url: "index.php?option=com_jclassroom&task=quizz.checkQuizz5",
				data: {quizzID:quizzID, answer:answer, unitID:unitID, position:position, unit:1},
				dataType: 'json',
				success: function( data ) {
					location.href = data.path;
					/*$('#result .result-inner').empty();
					if(data[0].result == 0) {
						antwort  = 'Leider Falsch';
						color    = 'text-danger';
					}
					if(data[0].result == 1) {
						antwort  = 'Richtig';
						color    = 'text-success';
					}
					/*$('#result .result-inner').append(
					'<p class="mt-1 mb-1 ' + color + '"><b>' + antwort + '</b></p>'
					);
					$('#check').addClass('disabled');*/
				}
			});
		}
	});
});