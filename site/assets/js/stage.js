jQuery(function ($) {

	// Fire on click on class t5answer
	$('.theAnswer').on('click', function() {
		var answer  	= $(this).attr('id');
		answerID      	= answer.replace('answer', '');
		if($(this).hasClass("btn-success")) {
			$(this).removeClass('btn-success').addClass('btn-light');
			$('#hiddenAnswer' + answerID).val(0);
		} else {
			$(this).removeClass('btn-light').addClass('btn-success');
			$('#hiddenAnswer' + answerID).val(1);
		}
	});
	$('#sayNo').on('click', function() {
		$('#hiddenAnswer').val(1);
		$('#sayYes').removeClass('btn-success').addClass('btn-light');
		$('#sayNo').removeClass('btn-light').addClass('btn-danger');
	});
	$('#sayYes').on('click', function() {
		$('#hiddenAnswer').val(2);
		$('#sayNo').removeClass('btn-danger').addClass('btn-light');
		$('#sayYes').removeClass('btn-light').addClass('btn-success');
	});
	$('#back').on('click', function() {
		var quizzID 		= $('#quizzID').val();
		var questionID 		= $('#questionID').val();
		var questionType 	= $('#questionType').val();
		var position 		= $('#position').val();
		var clr 			= $('#clr').val();
		var answers 		= [];
		$('.hiddenAnswer').each(function() {
			answers.push($(this).attr('id') + '_' + $(this).val());
		})
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=stage.checkSection",
			data: {questionType:questionType, quizzID:quizzID, questionID:questionID, position:position,direction:0,answers:answers},
			//dataType: 'json',
				success: function( data ) {
					location.href = 'stage?lid=2&unitID=' + quizzID +'&clr=' + clr + '&q=' + data;
				}
		});
	});
	$('#break').on('click', function() {
		if(confirm('Möchten Sie das Quizz unterbrechen?') == true) {
			var unitID 		= $('#unitID').val();
			var sectionID 	= $('#sectionID').val();
			var sectionType = $('#sectionType').val();
			var position 	= $('#position').val();
			var clr 		= $('#clr').val();
			jQuery.ajax({
				type: "POST",
				url: "index.php?option=com_jclassroom&task=stage.takeABreak",
				data: {type:sectionType, unitID:unitID, sectionID:sectionID, position:position,direction:0},
				//dataType: 'json',
					success: function( data ) {
						location.href = 'stage-classroom?id=' + clr;
					}
			});
		}
	});
	$('#stop').on('click', function() {
		var quizzID 		= $('#quizzID').val();
		var questionID 		= $('#questionID').val();
		var questionType 	= $('#questionType').val();
		var position 		= $('#position').val();
		var clr 			= $('#clr').val();
		if(questionType == 3) {
			var checkAnswer3 = $('.hiddenAnswer[value="1"]').length;
			if(checkAnswer3 == 0) {
				alert('Bitte beantworten Sie die Frage');
				return false;
			}
		}
		if(questionType == 5) {
			var checkAnswer5 = $('#hiddenAnswer').val();
			if(checkAnswer5 == 0) {
				alert('Bitte beantworten Sie die Frage');
				return false;
			}
		}
		var answers 		= [];
		$('.hiddenAnswer').each(function() {
			answers.push($(this).attr('id') + '_' + $(this).val());
		})
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=stage.stop",
			data: {questionType:questionType, quizzID:quizzID, questionID:questionID, position:position,direction:1,answers:answers,clr:clr},
			//dataType: 'json',
				success: function( data ) {
					location.href = 'stage?lid=9&unitID=' + quizzID + '&clr=' + clr + '&q=166671198';
				}
		});
	});
	$('#check').on('click', function() {
		var quizzID 		= $('#quizzID').val();
		var questionID 		= $('#questionID').val();
		var questionType 	= $('#questionType').val();
		var position 		= $('#position').val();
		var clr 			= $('#clr').val();
		if(questionType == 3) {
			var checkAnswer3 = $('.hiddenAnswer[value="1"]').length;
			if(checkAnswer3 == 0) {
				alert('Bitte beantworten Sie die Frage');
				return false;
			}
		}
		if(questionType == 5) {
			var checkAnswer5 = $('#hiddenAnswer').val();
			if(checkAnswer5 == 0) {
				alert('Bitte beantworten Sie die Frage');
				return false;
			}
		}
		var answers 		= [];
		$('.hiddenAnswer').each(function() {
			answers.push($(this).attr('id') + '_' + $(this).val());
		})
		
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=stage.checkSection",
			data: {questionType:questionType, quizzID:quizzID, questionID:questionID, position:position,direction:1,answers:answers},
			dataType: 'json',
				success: function( data ) {
					if(data != 0) {
						location.href = 'stage?lid=2&unitID=' + quizzID +'&clr=' + clr + '&q=' + data;
					}
					if(data == 0) {
						//location.href = 'stage?lid=9&unitID=' + quizzID;
					}
				}
		});
	});
});
function openResolution() {
	if(confirm('Möchtest Du die Lösung einsehen?') == true) {
		$('#resolution').fadeIn(200);
	}
}
function saveUserResolution() {
	let learningroomID = $('#theLearningroomID').val();
	let unitID 		= $('#theUnitID').val();
	let resolution 	= $('#userresolution').val();
	jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=stage_classroom.saveUserResolution",
		data: {
			learningroomID:learningroomID, 
			unitID:unitID, 
			resolution:resolution
		},
		//dataType: 'json',
		success: function( data ) {
			console.log(data);
		}
	});
}