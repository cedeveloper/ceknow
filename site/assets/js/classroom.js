function sendVerification(all, studentID) {
	checkTemplate(all, 1, studentID);
}
function sendInvitation(all, studentID) {
	checkTemplate(all, 2, studentID);
}
function sendRemember(all, studentID) {
	checkTemplate(all, 3, studentID);
}
function checkTemplate(all, type, studentID) {
	$.ajax({
		url: "index.php?option=com_jclassroom&task=classroom2.checkTemplateExist",
		data: {
			classroomID:classroomID,
			template:type
		},
		method: 'POST',
		//dataType: 'json',
		success: function( data ) {
			if(data == 'OK') {
				emailExecute(all, type, studentID);
			}
			if(data == 'NOK') {
				$('#students_dialog .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Warnung');
				$('#students_dialog .modal-body').html('Der Versand von E-Mails ist nicht möglich, da kein Template gewählt wurde -> Bereich Kommunikation');
				$('#students_dialog').modal('show');
				return false;
			}
		}
	});
}
function emailExecute(all, type, studentID) {
	let typeText = '';
	if(type == 1) {
		if(all == 0) {
			typeText = 'diesen Teilnehmer eine Verifikations-E-Mail';
		}
		if(all == 1) {
			typeText = 'alle Teilnehmer eine Verifikations-E-Mail';
		}
	}
	if(type == 2) {
		if(all == 0) {
			typeText = 'diesen Teilnehmer eine Einladungs-E-Mail';
		}
		if(all == 1) {
			typeText = 'alle Teilnehmer eine Einladungs-E-Mail';
		}
	}
	if(type == 3) {
		if(all == 0) {
			typeText = 'diesen Teilnehmer eine Erinnerungs-E-Mail';
		}
		if(all == 1) {
			typeText = 'alle Teilnehmer eine Erinnerungs-E-Mail';
		}
	}
	if(confirm('Möchten Sie an ' + typeText + ' versenden?') == true) {
		$('#wait').css('display', 'block');
		$('#message').remove();
		$.ajax({
			url: "index.php?option=com_jclassroom&task=classroom2.sendEmail",
			data: {
				all:all,
				type:type,
				classroomID:classroomID,
				studentID:studentID
			},
			method: 'POST',
			dataType: 'json',
			success: function( data ) {
				$('#students-actions').append(data['html']);
				if(all == 0) {
					if(type == 1) {
						$('#vertify_on' + studentID).html(data['created']);
					}
					if(type == 2) {
						$('#invitation_on' + studentID).html(data['created']);
					}
					if(type == 3) {
						$('#remember_on' + studentID).html(data['created']);
					}
				}
				if(all == 1) {
					if(type == 1) {
						$('.vertify_on').each(function() {
							$(this).html(data['created']);
						});
					}
					if(type == 2) {
						$('.invitation_on').each(function() {
							$(this).html(data['created']);
						});
					}
					if(type == 3) {
						$('.remember_on').each(function() {
							$(this).html(data['created']);
						});
					}
				}
				
				$('#wait').css('display', 'none');
			}
		});
	}
}
function deleteFolder() {
	jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=classroom.deleteFolder",
		data: {},
		//dataType: 'json',
		success: function( data ) {

		}
	});
}
function createFolderTree() {
	$('#folderTree').empty();
	jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=classroom.createFolderTree",
		data: {},
		dataType: 'json',
		success: function( data ) {
			console.log(data);
			$.each(data, function(e,value) {
				$('#folderTree').append('<p class="mt-1 mb-1"><i class="fa fa-folder-o"></i> ' + value['name'] + '</p>');
			});
		}
	});
}
function createStudent() {
	let companyID  = $('#companyID').val();
	if(companyID && companyID != 0){
		$('#studentCompanyID').val(companyID);
		jQuery('#studentCompany').val(companyID).attr('disabled', true).select2({width: '100%'});
	}
	jQuery('#newStudent_dialog').modal('show');
}
function openDescription(moduleID) {
	let openInfo 	= $('#openInfo' + moduleID);
	let theInfo		= $('#theInfo' + moduleID);
	let show 		= 0;
	if(theInfo.hasClass('showInfo')) {
		theInfo.removeClass('showInfo').addClass('hideInfo');
		openInfo.addClass('text-danger').removeClass('text-success');

	} else {
		theInfo.removeClass('hideInfo').addClass('showInfo');
		openInfo.addClass('text-success').removeClass('text-danger');
		show = 1;
	}
	$.ajax({
		url: "index.php?option=com_jclassroom&task=classroom2.setModuleShowInfo",
		data: {
			moduleID:moduleID,
			show:show
		},
		method: 'POST',
		//dataType: 'json',
		success: function( data ) {
			
		}
	});
}
$('#jform_invitations').on('change', function() {
	var invitations = $('#jform_invitations option:selected').val();
	if(invitations == 2) {
		$('#jform_invitationsBy').attr('disabled', true);
	}
});



