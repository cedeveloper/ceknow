jQuery(function ($) {

});
function openLightbox(path, id, size, extension) {
    let lightbox = $('#lightbox');
    let imageSize = parseInt(size)/1000;
    lightbox.find('#lightbox_theImage').attr('src', path);
    lightbox.find('#lightbox_deleteImage').attr('onclick', 'lightbox_deleteImage(' + id + ');');
    lightbox.find('#lightbox_size').html(imageSize.toFixed(2));
    lightbox.find('#lightbox_extension').html(extension);
    lightbox.modal('show');
}
function lightbox_deleteImage(id) {
    let lightbox = $('#lightbox');
    if(confirm("Soll das aktuelle Bild entgültig gelöscht werden?") == true) {
        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=global.lightbox_deleteImage",
            data: {
                id:id
            },
            //dataType: 'json',
            success: function( data ) {
                $('#folderTreeLearningrooms #file' + id).remove();
                lightbox.modal('hide');
                lightbox.find('#lightbox_theImage').attr('src', '');
                lightbox.find('#lightbox_deleteImage').attr('onclick', '');
            }
        });
        
    }
}