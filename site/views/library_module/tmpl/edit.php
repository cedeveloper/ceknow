<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/imgForJCE.js');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newPart.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newPartLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/editContent.php');
require_once(JPATH_COMPONENT.'/views/dialogs/imageContent.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

   <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a href="library-modules" class="btn btn-success text-white">Speichern</a>
      <?php endif;?>
        <a href="library-modules" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('description'); ?>
         </div>
         <div class="use_editor col-12 col-sm-10" data-editor="description">
            <?php 
            echo $this->form->getInput('description');
            ?>
         </div>
      </div>
   </div>
   <div id="structure">
      <?php echo $this->module;?>
   </div>
<input type="hidden" id="id" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<div id="wait">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<style>
   #editContent .modal-dialog {
      max-width: 80%!important;
   }
   #editContent #editor {
      height: 450px!important;
      overflow-y: scroll;
   }
</style>
<script type="text/javascript">
let unitID = $('#id').val();
let classroomID = false;
let part = 'library-module';
   $(document).ready(function() {
      $('.module .moduleHeader .move').remove();
      $('.module .moduleHeader .trash').remove();
      $('.module .moduleHeader .save').remove();
      $('.module .moduleHeader .calendar').remove();
      $('.module .moduleHeader .saveLibrary').remove();
      $('.module .moduleHeader .fa-arrows-alt').remove();
      $('.unit .sectionHeader .hide').remove();
      $('.unit .sectionHeader .fa-arrows-alt').remove();
      $('.unit .sectionHeader .saveLibrary').remove();
   });
   function openModule(id) {
      if(jQuery('#moduleContent' + id).hasClass('hide')) {
         jQuery('#moduleContent' + id).removeClass('hide');
         jQuery('#moduleContent' + id).addClass('show');
         jQuery('#module' + id + ' .moduleHeader .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      } else {
         jQuery('#moduleContent' + id).removeClass('show');
         jQuery('#moduleContent' + id).addClass('hide');
         jQuery('#module' + id + ' .moduleHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      }
   }
   function openUnit(id) {
      if(jQuery('#unitContent' + id).hasClass('openUnit')) {
         jQuery('#unitContent' + id).removeClass('openUnit');
         jQuery('#unit' + id + ' .sectionHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      } else {
         jQuery('#unitContent' + id).addClass('openUnit');
         jQuery('#unit' + id + ' .sectionHeader .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      }
   }
   function addUnit(moduleID) {
      var dayID   = 0;
      jQuery('.moduleContent').each(function() {
        jQuery(this).removeClass('show').addClass('hide');
        jQuery(this).parent('.module').find('.moduleHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      });
      openModule(moduleID);
      jQuery('#newUnitType').val('').select2({width: '300px'});
      jQuery('#newPartToModuleID').val(moduleID);
      jQuery('#newPartToDayID').val(dayID);
      jQuery('#newPart_dialog').modal();
   }
   jQuery(document).on('change','.moduleTitle', function() {
      var moduleID = jQuery(this).attr('data-id');
      saveModule(moduleID);
   });
   jQuery(document).on('change','#jform_description', function() {
      var moduleID = $('#id').val();
      saveModule(moduleID);
   });
   jQuery(document).on('change','.title', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   jQuery(document).on('change','.duration', function() {
      var dataValue = $(this).val();
      if(isNaN(dataValue)) {
         alert('Bitte geben Sie eine Zahl ein.');
         $(this).val(0);
         return false;
      }
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   jQuery(document).on('change','.link', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   jQuery(document).on('change','.quizz', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   jQuery(document).on('summernote.change','.summernote', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   function saveUnit(unitID) {
      jQuery('#save').fadeIn(200);
      var title      = jQuery('#unit' + unitID).find('.title').val();
      var description= jQuery('#jform_description').val();
      var duration   = jQuery('#unit' + unitID).find('.duration').val();
      var content    = jQuery('#unit' + unitID).find('.contentHTML').html();
      var link       = jQuery('#unit' + unitID).find('.link').val();
      var quizz      = jQuery('#unit' + unitID).find('.quizz option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=library_module.saveUnit",
         data: {unitID:unitID,title:title,duration:duration,content:content,link:link,quizz:quizz,description:description},
         //dataType: 'json',
         success: function( data ) {
            hideSave();
         }
      });
   }
   jQuery('#newUnitSave').on('click', function(e) {
      var typ = jQuery('#newUnitType option:selected').val();
      getType(typ,0,0);
      jQuery('#newPart_dialog').modal('toggle');
   });
   function getType(typ, library, unitID) {
      jQuery('#wait').css('display', 'block');
      var classroomID   = 0;
      var dayID         = 0;
      var moduleID      = jQuery('#newPartToModuleID').val();
      var unitCounter   = jQuery('#unitCounter').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=library_module.getTemplate",
         data: {library:library,typ:typ, classroomID:classroomID,dayID:dayID, moduleID:moduleID, unitCounter:unitCounter,unitID:unitID},
         //dataType: 'json',
         success: function( data ) {
            //jQuery('#structure #cardFor' + dayID + ' .card-body .cardMessage').remove();
            jQuery('#module' + moduleID + ' .moduleContent > ul').append(data);
            $('.unit .sectionHeader .hide').remove();
            $('.unit .sectionHeader .fa-arrows-alt').remove();
            $('.unit .sectionHeader .saveLibrary').remove();
            jQuery('.select2').select2({width: '100%'});
            jQuery('#wait').css('opacity', 0);
         }
      });
   }
   function hideSave() {
      setTimeout(hideSaveExecute,800);
   }
   function hideSaveExecute() {
      jQuery('#save').slideUp(200);
   }
   // SAVE SOMETHING
   function saveModule(moduleID) {
      jQuery('#save').fadeIn(200);
      var title      = jQuery('#module' + moduleID).find('.moduleTitle').val();
      var description= jQuery('#jform_description').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=library_module.saveModule",
         data: {moduleID:moduleID,title:title,description:description},
         //dataType: 'json',
         success: function( data ) {
            hideSave();
         }
      });
   }
   function editContent(unitID) {
      var content = jQuery('#unit' + unitID).find(' .contentHTML').html();
      tinyMCE.get('jform_content').setContent(content);
      jQuery('#editContentUnitID').val(unitID);
      jQuery('#editContent').modal();
   }
   function editContentModule() {
      //jQuery('#save').fadeIn(200);
      var content = tinyMCE.get('jform_content').getContent();
      var unitID  = jQuery('#editContentUnitID').val();
      jQuery('#unit' + unitID).find(' .contentHTML').html(content);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      saveUnit(unitID);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
      jQuery('#editContent').modal('hide');
      hideSave();
       //jQuery('#save').fadeOut(200);
   }
   function closeEditContentModule() {
      jQuery('#editContent').modal('hide');
   }
   function deleteUnit(unitID) {
      if(confirm('Soll diese Unit gelöscht werden?') == true) {
         jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=library_module.deleteUnit",
           data: {unitID:unitID},
           //dataType: 'json',
           success: function( data ) {
             jQuery('#unit' + unitID).remove();
           }
         });
      }
   }
   jQuery( ".moduleContent ul" ).sortable({
      connectWith: '.li',
      handle: '.move',
      axis: 'y',
      update: function (event, ui) {
         var nodes = jQuery(this).context.childNodes;
         var order = [];
         jQuery(nodes).each(function(e) {
            var id   = jQuery(this).context.id;
            id       = id.substring(4);
            order.push(e + '_' + id);
         });
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=library_module.writeSortableUnits",
            data: {order:order},
            //dataType: 'json',
            success: function( data ){
               console.log(data);
            }
         });
      }
   });
</script>