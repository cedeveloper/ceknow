<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
 
/**
 * Kunde item view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewLibrary_module extends JViewLegacy
{
	protected $item;
	protected $form;
	protected $state;
	
	public function display($tpl = null)
	{
		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		JLoader::register('ModulesHelper',JPATH_COMPONENT_SITE.'/helpers/modules.php');
        $template 		= new ModulesHelper();
        $module 		= $template->getTemplate(0, 0, $this->item->id, $this->item->title,1); 
		// LOAD THE UNITS
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
		$query->select(array('a.id,a.unitType,a.quizzID'));
        $query->from($db->quoteName('#__jclassroom_classroom_days_units_library','a'));
		$query->where('a.moduleID = '.$this->item->id);
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$units = $db->loadObjectList();
		if($units):
			foreach($units as $unit):
    			JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
				$template = new UnitsHelper();
				$unitTemplate .= $template->getTemplate('lib', 0, 0, $unit->unitType, $unit->id);
			endforeach;
		endif;
		$moduleTemplate = str_replace('{units}', $unitTemplate, $module);

        $this->module 	= $moduleTemplate;
        $session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour = 'manager-administrator/learningrooms';
				$this->usergroup = 'superuser';
				break;
			case 'trainer':
				$this->retour = 'library-modules';
				$this->usergroup = 'trainer';
				break;
			case 'student':
				$this->retour = 'manager-student';
				$this->usergroup = 'student';
				break;
		}
		parent::display($tpl);
	}
}
?>