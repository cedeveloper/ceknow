<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<style>
    .card-header {
        max-height: 240px;
        overflow-y: scroll;
    }
    .teaser {
        background-color: #ff3600;
        color: #fff;
        position: absolute;
        transform: rotate(45deg);
        text-align: center;
        padding: 0px 15px;
        font-size: 11px;
        top: 12px;
        right: -29px;
    }
    .card-link {
        color: #007bff!important;
        cursor: pointer;
    }
    .price {
        position: absolute;
        top: 20px;
        background-color: #ff3600;
        padding: 4px 10px;
        font-weight: bold;
        border-radius: 4px;
        color: #ffffff;
        font-size: 24px;
        border: 0.5px solid #ffffff;
    }
    .imageContainer {
        height: 200px;
        background-position: center center;
        background-size: cover;
    }
</style>
<form action="index.php?option=com_jclassroom&task=students.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept=".csv" name="uploadCSV" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right mb-3">Zurück</a>
    </div>
    <?php if($this->items) { ?>
        <div id="cards" style="margin-left: -15px;margin-right: -15px;">
            <div class="row">
            <?php foreach ($this->items as $i => $item) : ?>
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="card mb-2" style="overflow: hidden;">
                        <?php 
                        if($item->title_image):
                            $image =  JURI::Root().$item->title_image;
                        else:
                            $image = JURI::Root().'images/jclassroom/ceknow-learningroom.jpg';
                        endif;
                        ?>
                        <div class="imageContainer" style="background-image: URL('<?php echo $image;?>');"></div>
                        
                        <div class="card-header" style="min-height: 240px;">
                            <?php
                            if($item->customerID == 0):
                                echo $item->teaser;
                            endif;
                            if($item->freeQuizz == 0):
                                echo '<span class="price">'.number_format($item->price,2,',','.') .'€</span>';
                            endif;
                            ?>
                            <h5 class="card-title" style="padding-top: 20px;padding-right: 25px;">
                                <a href="<?php echo JURI::Root().'library-modules-edit?layout=edit&id='. $item->id.'&noS'; ?>">
                                <?php echo $item->title;?>
                            </a>
                            </h5>
                            <p class="card-text">
                                <?php 
                                if($item->title_description):
                                    echo $item->title_description;
                                else:
                                    echo $item->description;
                                endif;
                                ?>
 
                            </p>
                        </div>
                        <div class="card-body">
                            <?php if($this->usergroup != 'superuser'): ?>
                                <?php if($item->freeQuizz == 0): ?>
                                    <p class="mb-1"><a title="Erstellen Sie aus dieser Vorlage einen neuen Learningroom und passen Sie diesen an Ihre Wünsche an." onclick="createQuizz(<?php echo $item->id;?>);" class="w-100 btn btn-success text-white btn-sm">Quizz in den Warenkorb legen</a></p>
                                <?php else: ?>
                                    <p class="mb-1"><a title="Erstellen Sie aus dieser Vorlage einen neuen Learningroom und passen Sie diesen an Ihre Wünsche an." onclick="createQuizz(<?php echo $item->id;?>);" class="w-100 btn btn-primary text-white btn-sm">Quizz aus Vorlage erstellen</a></p>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php if($this->usergroup == 'superuser'):?>
                                <p class="mb-1"><a href="library-quizz-edit?id=<?php echo $item->id;?>" class="w-100 btn btn-success text-whiet btn-sm">Vorlage Bearbeiten</a></p>
                                <p class="m-0"><a class="w-100 btn btn-danger text-white btn-sm" onclick="deleteTemplate(<?php echo $item->id;?>);">Vorlage löschen</a></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    <?php } else { ?>
        <p>Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</p>
   	<?php } ?>			
    <?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
function createQuizz(id) {
    if(confirm('Möchten Sie aus der Vorlage ' + id + ' ein neues Quizz erstellen?') == true) {
        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=library_quizze.createQuizz",
            data: {id:id},
            //dataType: 'json',
            success: function( data ) {
               alert('Die Vorlage ' + id + ' wurde erfolgreich als Quizz ' + data + ' gespeichert.');
            }
         });
    }
}
</script>