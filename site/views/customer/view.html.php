<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewCustomer extends JViewLegacy {

   	protected $item;
	protected $form;
	protected $state;

    public function display($tpl = null) {

		$user = JFactory::getUser();
		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour 	= 'manager-administrator/customers';
				break;
			case 'trainer':

				break;
			case 'student':

				break;
		}
		// Textmodule for Demoaccount
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$demoaccount = JTable::getInstance('Textmodule','JclassroomTable',array());
		$load = array('section' => 3,'title' => 'Demoaccount erstellen');
		$check = $demoaccount->load($load);
		if($check):
			$this->demoaccount = $demoaccount->theText;
		else:
			$this->demoaccount = 'Kein Eingabeformular gefunden.';
		endif;
		$input 	= JFactory::getApplication()->input;
		$p 		= $input->get('p', '', 'INT');
		$n 		= $input->get('n', '', 'INT');
		if($n):
			$tpl = 'newPlan';
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Plan','JclassroomTable',array());
			$table->load($n);
			$this->pid = $n;
			$this->p = '<b>'.$table->title.'-Plan</b>';
			$this->price = number_format($table->price,2,',','.').' €';
			$this->plandescription = $table->description;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
			$load 	= array('parameter' => 'asp_text','customerID' => 0);
			$check 	= $table->load($load);
			if($check):
				$this->asp = $table->wert;
			else:
				$this->asp = 'Keine Kontaktdaten gefunden.';
			endif;
			//Introtext
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$intro1 = JTable::getInstance('Textmodule','JclassroomTable',array());
			$load = array('section' => 1,'title' => 'Einleitungstext Planbestellung');
			$intro1->load($load);
			//$intro1 = file_get_contents(JURI::Root().'images/jclassroom/configuration/newplan.txt');
			$intro1 = str_replace('{planname}', $this->p, $intro1->theText);
			$intro1 = str_replace('{plandescription}', $this->plandescription, $intro1);
			$this->intro1 = $intro1;
			//Register
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$register = JTable::getInstance('Textmodule','JclassroomTable',array());
			$load = array('section' => 1,'title' => 'Registrierung Planbestellung');
			$register->load($load);
			//$intro1 = file_get_contents(JURI::Root().'images/jclassroom/configuration/newplan.txt');
			$register = str_replace('{planname}', $this->p, $register->theText);
			$register = str_replace('{planID}', $n, $register);
			$register = str_replace('{plandescription}', $this->plandescription, $register);
			$this->register = $register;
		endif;
		if($p):
			$tpl = 'editPlan';
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Plan','JclassroomTable',array());
			$table->load($p);
			$this->pid = $p;
			$this->p = '<b>'.$table->title.'-Plan</b>';
			$this->price = number_format($table->price,2,',','.').' €';
			$this->plandescription = $table->description;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Configuration','JclassroomTable',array());
			$load 	= array('parameter' => 'asp_text','customerID' => 0);
			$check 	= $table->load($load);
			if($check):
				$this->asp = $table->wert;
			else:
				$this->asp = 'Keine Kontaktdaten gefunden.';
			endif;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$text = JTable::getInstance('Textmodule','JclassroomTable',array());
			$load = array('section' => 2,'title' => 'Accountready Planänderung');
			$text->load($load);
			$theText = $text->theText;
			// 1. Load the customerpayments
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$customerP 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
			$load 	= array('customerID' => $session->get('customerID'));
			$customerP->load($load);
			// 2. Activate the CustomerAdminAccount
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$customerA 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 	= array('userID' => $user->id);
			$customerA->load($load);
			// 3. Activate the Customeraccount
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$customerC 	= JTable::getInstance('Customer','JclassroomTable',array());
			$customerC->load($session->get('customerID'));
			// 5. Load the Plan
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Plan','JclassroomTable',array());
			$table->load($p);
			$planname = $table->title.'-Plan';
			$this->price 	= $table->price;
			$price = number_format($table->price,2,',','.').'€*';
			$plandescription = $table->description;
			$theText = str_replace('{planname}',$planname, $theText);
			$theText = str_replace('{plandescription}',$plandescription, $theText);
			$theText = str_replace('{price}',$price, $theText);
			$theText = str_replace('{rabatt}','0%', $theText);
			$theText = str_replace('{priceWithRabatt}',$price, $theText);
			$theText = str_replace('{first_name}',$customerA->first_name, $theText);
			$theText = str_replace('{last_name}',$customerA->last_name, $theText);
			$theText = str_replace('{email}',$customerA->email, $theText);
			$theText = str_replace('{company}',$customerC->company_name, $theText);
			$theText = str_replace('{invoice_adress}',$customerC->address, $theText);
			$theText = str_replace('{invoice_postcode}',$customerC->postcode, $theText);
			$theText = str_replace('{invoice_city}',$customerC->city, $theText);
			$theText = str_replace('{customernumber}',$customerC->customer_number, $theText);
			$theText = str_replace('{invoice_first_name}',$customerP->invoice_first_name, $theText);
			$theText = str_replace('{invoice_last_name}',$customerP->invoice_last_name, $theText);
			$theText = str_replace('{invoice_function}',$customerP->invoice_function, $theText);
			$theText = str_replace('{invoice_alt_adress}',$customerP->invoice_adress, $theText);
			$theText = str_replace('{invoice_alt_plz}',$customerP->invoice_postcode, $theText);
			$theText = str_replace('{invoice_alt_city}',$customerP->invoice_city, $theText);
			$this->text = $theText;
			$this->userID 	= $customerA->userID;
			$this->customerID 	= $customerC->id;
			$this->alt_invoice 	= $customerP->alt_invoice;
		endif;
		$r 		= $input->get('r', '', 'INT');
		if($r == 1):
			$text = file_get_contents(JURI::Root().'images/jclassroom/configuration/planchange.txt');
			$this->text = $text;
			$tpl = 'resultchangeplan';
		endif;
		if($r == 2):
			$text = file_get_contents(JURI::Root().'images/jclassroom/configuration/resultNewPlan.txt');
			$this->text = $text;
			$tpl = 'resultnewplan';
		endif;
		if($r == 3):
			$token      = $input->get('SMA', '', 'STR');
			$id     	= base64_decode($token);
			$uid 		= $input->get('uid', 0, 'INT');
			$deno 		= $input->get('deno', '', 'INT');
			if($uid):
				$id = $uid;
			endif;
			$this->deno = $deno;
			$isRegistered = 0;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$checkReg 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 	= array('userID' => $id, 'published' => 1);
			$check 	= $checkReg->load($load);
			if($check):
				$isRegistered = 0;
			endif;
			if($isRegistered == 0):
				// 1. Activate the Useraccount
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$us 	= JTable::getInstance('User','JclassroomTable',array());
				$us->load($id);
				// 2. Activate the CustomerAdminAccount
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$customerA 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
				$load 	= array('userID' => $id);
				$customerA->load($load);
				$customerA->published = 1;
				$customerA->store();
				// 3. Activate the Customeraccount
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$customerC 	= JTable::getInstance('Customer','JclassroomTable',array());
				$customerC->load($customerA->customerID);
				$customerC->published = 1;
				$customerC->store();
				// 4. Create the Logfile
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$table 	= JTable::getInstance('Logs','JclassroomTable',array());
				$data = array();
				$data['customerID'] 	= $customerA->customerID;
				$data['userID'] 		= $id;
				$data['parameter'] 		= 'Activate Account';
				$data['wert'] 			= 'Der Benutzer '.$customerA->first_name.' '.$customerA->last_name.', Kundennummer: '.$customerC->customer_number.'  hat sein Konto aktiviert.';
				$data['created'] 		= date('Y-m-d H:i:s');
				$data['created_by'] 	= $id;
				$table->bind($data);
				$table->store();
			
				if($uid):
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$text = JTable::getInstance('Textmodule','JclassroomTable',array());
					$load = array('section' => 2,'title' => 'Accountready Planänderung');
					$text->load($load);
					$theText = $text->theText;
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$customerP 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
					$load 	= array('customerID' => $customerC->id);
					$customerP->load($load);
					// 5. Load the Plan
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 	= JTable::getInstance('Plan','JclassroomTable',array());
					$table->load($n);
					$planname = $table->title.'-Plan';
					$this->price 	= $table->price;
					$price = number_format($table->price,2,',','.').'€*';
					$plandescription = $table->description;
					$this->alt_invoice = $customerP->alt_invoice;
					$this->planID 			= $n;
					$this->planIDCurrent 	= $customerC->planID;
					$this->userID 	= $id;
					$this->customerID 	= $customerC->id;
					$tpl = 'accountchange';
				else:
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$text = JTable::getInstance('Textmodule','JclassroomTable',array());
					$load = array('section' => 1,'title' => 'Accountready Planbestellung');
					$text->load($load);
					$theText = $text->theText;
					// 5. Load the Plan
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$table 	= JTable::getInstance('Plan','JclassroomTable',array());
					$table->load($customerC->planID);
					$planname = $table->title.'-Plan';
					$this->price 	= $table->price;
					$price = number_format($table->price,2,',','.').'€*';
					$plandescription = $table->description;
					$this->alt_invoice = $customerP->alt_invoice;
					if(!$n):
						$this->planID 	= $customerC->planID;
					else:
						$this->planID 	= $n;
					endif;
					$this->planIDCurrent 	= $customerC->planID;
					$this->userID 	= $id;
					$this->customerID 	= $customerC->id;
					$tpl = 'accountready';
				endif;
				$theText = str_replace('{first_name}',$customerA->first_name, $theText);
				$theText = str_replace('{last_name}',$customerA->last_name, $theText);
				$theText = str_replace('{email}',$us->email, $theText);
				$theText = str_replace('{planname}',$planname, $theText);
				$theText = str_replace('{plandescription}',$plandescription, $theText);
				$theText = str_replace('{price}',$price, $theText);
				$theText = str_replace('{rabatt}','0%', $theText);
				$theText = str_replace('{priceWithRabatt}',$price, $theText);
				$theText = str_replace('{customernumber}',$customerC->customer_number, $theText);
				$theText = str_replace('{company}',$customerC->company_name, $theText);
				$theText = str_replace('{invoice_adress}',$customerC->address, $theText);
				$theText = str_replace('{invoice_postcode}',$customerC->postcode, $theText);
				$theText = str_replace('{invoice_city}',$customerC->city, $theText);
				$theText = str_replace('{invoice_first_name}',$customerP->invoice_first_name, $theText);
				$theText = str_replace('{invoice_last_name}',$customerP->invoice_last_name, $theText);
				$theText = str_replace('{invoice_function}',$customerP->invoice_function, $theText);
				$theText = str_replace('{invoice_alt_adress}',$customerP->invoice_adress, $theText);
				$theText = str_replace('{invoice_alt_plz}',$customerP->invoice_postcode, $theText);
				$theText = str_replace('{invoice_alt_city}',$customerP->invoice_city, $theText);
				$this->text = $theText;
			else:
				$tpl = 'isregistered';
			endif;
		endif;
		if($r == 4):
			// User will change plan without Login und with existing Account
			$session 	= JFactory::getSession();
			$set 		= $session->get('setAccount');
			$planID 	= $session->get('setAccountPID');
			// 1. Load the Useraccount
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$us 	= JTable::getInstance('User','JclassroomTable',array());
			$us->load($user->id);
			// 2. ActivLoadate the CustomerAdminAccount
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$customerA 	= JTable::getInstance('Customer_administratoren','JclassroomTable',array());
			$load 	= array('userID' => $user->id);
			$customerA->load($load);
			// 3. Load the Customeraccount
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$customerC 	= JTable::getInstance('Customer','JclassroomTable',array());
			$customerC->load($customerA->customerID);
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$text = JTable::getInstance('Textmodule','JclassroomTable',array());
			$load = array('section' => 2,'title' => 'Neuer Plan mit Konto');
			$text->load($load);
			$theText = $text->theText;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$customerP 	= JTable::getInstance('Customer_payment','JclassroomTable',array());
			$load 	= array('customerID' => $customerC->id);
			$customerP->load($load);
			// 5. Load the Plan
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Plan','JclassroomTable',array());
			$table->load($planID);
			$planname = $table->title.'-Plan';
			$this->price 		= $table->price;
			$price = number_format($table->price,2,',','.').'€*';
			$plandescription 	= $table->description;
			$this->alt_invoice 	= $customerP->alt_invoice;
			$this->planID 		= $planID;
			$this->userID 		= $user->id;
			$this->customerID 	= $customerC->id;
			$theText = str_replace('{first_name}',$customerA->first_name, $theText);
			$theText = str_replace('{last_name}',$customerA->last_name, $theText);
			$theText = str_replace('{email}',$us->email, $theText);
			$theText = str_replace('{planname}',$planname, $theText);
			$theText = str_replace('{plandescription}',$plandescription, $theText);
			$theText = str_replace('{price}',$price, $theText);
			$theText = str_replace('{rabatt}','0%', $theText);
			$theText = str_replace('{priceWithRabatt}',$price, $theText);
			$theText = str_replace('{customernumber}',$customerC->customer_number, $theText);
			$theText = str_replace('{company}',$customerC->company_name, $theText);
			$theText = str_replace('{company_name}',$customerC->company_name, $theText);
			$theText = str_replace('{invoice_adress}',$customerC->address, $theText);
			$theText = str_replace('{invoice_postcode}',$customerC->postcode, $theText);
			$theText = str_replace('{invoice_city}',$customerC->city, $theText);
			$theText = str_replace('{invoice_first_name}',$customerP->invoice_first_name, $theText);
			$theText = str_replace('{invoice_last_name}',$customerP->invoice_last_name, $theText);
			$theText = str_replace('{invoice_function}',$customerP->invoice_function, $theText);
			$theText = str_replace('{invoice_alt_adress}',$customerP->invoice_adress, $theText);
			$theText = str_replace('{invoice_alt_plz}',$customerP->invoice_postcode, $theText);
			$theText = str_replace('{invoice_alt_city}',$customerP->invoice_city, $theText);
			$this->text = $theText;
			$tpl = 'editplan';
		endif;
		if($r == 5):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$text = JTable::getInstance('Textmodule','JclassroomTable',array());
			$load = array('section' => 1,'title' => 'Waitformail Planbestellung');
			$text->load($load);
			$this->text = $text->theText;
			$tpl = 'waitformail';
		endif;
		if($r == 6):
			$session 	= JFactory::getSession();
			$set 		= $session->get('setAccount');
			$planID 	= $session->get('setAccountPID');
			$token      = $input->get('SMA', '', 'STR');
			$id     	= base64_decode($token);
			$pid 		= $input->get('pid', 0, 'INT');
			if($set == 1):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$text = JTable::getInstance('Textmodule','JclassroomTable',array());
				$load = array('section' => 1,'title' => 'Danke Planbestellung mit Konto');
				$text->load($load);
			else:
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$text = JTable::getInstance('Textmodule','JclassroomTable',array());
				$load = array('section' => 1,'title' => 'Danke Planbestellung');
				$text->load($load);
			endif;
			// Load the Planorder
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$po 	= JTable::getInstance('Planorders','JclassroomTable',array());
			$po->load($pid);
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$pop 	= JTable::getInstance('Planorders_positions','JclassroomTable',array());
			$load 	= array('planorderID' => $pid);
			$pop->load($load);
			if(!$planID):
				$planID = $pop->planID;
			else:

			endif;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$table 	= JTable::getInstance('Plan','JclassroomTable',array());
			$table->load($planID);
			$planname = $table->title.'-Plan';
			$text = str_replace('{ordernumber}','B01-'.str_pad($po->id,8,'0', STR_PAD_LEFT), $text->theText);
			$text = str_replace('{planname}',$planname, $text);
			$this->text = $text;
			$session->clear('setAccount');
			$session->clear('setAccountPID');
			$tpl = 'resultnewplan';
		endif;
		
        parent::display($tpl);
    }
}
