<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
?>
<style>
.stepwizard-step p {
    margin-top: 10px;    
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;     
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
    
}

.stepwizard-step {    
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
.btn-default {
    border: 1px solid #a1a1a1;
    background-color: #ffffff;
}
</style>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&task=customer.createDemo'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="row" style="margin-top: 100px;">
        <div class="col-12 col-sm-3"></div>
        <div class="col-12 col-sm-6">
            <?php echo $this->text;?>
        </div>

        <div class="col-12 col-sm-3"></div>
    </div>
    <input type="hidden" id="userID" value="<?php echo $this->userID;?>" />
    <input type="hidden" id="planID" value="<?php echo $this->planID;?>" />
    <input type="hidden" id="planIDCurrent" value="<?php echo $this->planIDCurrent;?>" />
    <input type="hidden" id="price" value="<?php echo $this->price;?>" />
    <input type="hidden" id="deno" value="<?php echo $this->deno;?>" />
    <input type="hidden" id="customerID" value="<?php echo $this->customerID;?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="wait" style="text-align: center;">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<div id="theMessageA" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 700px!important;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Nachricht</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var planIDCurrent  = $('#planIDCurrent').val();
    var deno    = $('#deno').val();
    $('#returnToPlans').attr('href', 'prices-novalid?deno=' + deno + '&pid=' + planIDCurrent);
});
function changeAbo() {
	var abo 	= $('#paymentAbo option:selected').val();
	var price 	= $('#price').val();
	$('#calculationPrice').empty();
	$('#calculationRabatt').empty();
	$('#calculationPriceWithRabatt').empty();
	if(abo == 'monthly') {

		$('#calculationPrice').html(price.replace('.',',') + '€*'); 
		$('#calculationRabatt').html('0%'); 
		$('#calculationPriceWithRabatt').html(price.replace('.',',') + '€*'); 
	}
	if(abo == 'yearly') {
		$('#calculationPrice').html(price.replace('.',',') + '€*'); 
		$('#calculationRabatt').html('25%'); 
		var p1 = (price * 75/100) * 12;
		p1 = p1.toFixed(2);
		p1 = p1.toString();
		p1 = p1.replace('.',',');
		$('#calculationPriceWithRabatt').html(p1.replace('.',',') + '€*'); 
	}
}
function openInvoiceAdress() {
	if($('#check_invoice_adress').is(':checked')) {
		$('#invoice').fadeIn(200);
	} else {
		$('#invoice').fadeOut(200);
	}
}
function changePaymentType() {
	var paymentID = $('#paymentType option:selected').val();
	if(paymentID == 'sepa') {
		$('#paymentSepa').fadeIn(200);
	} else {
		$('#paymentSepa').fadeOut(200);
	}
}
function conclusion() {
    var userID      = $('#userID').val();
    var planID      = $('#planID').val();
    var customerID  = $('#customerID').val();
    var paymentAbo  = $('#paymentAbo option:selected').val();
    var paymentType = $('#paymentType option:selected').val();
    var invoice_company     = $('#invoice_company').val();
    var invoice_adress      = $('#invoice_adress').val();
    var invoice_postcode    = $('#invoice_plz').val();
    var invoice_city        = $('#invoice_city').val();
    var calculationRabatt   = $('#calculationRabatt').html();
    var calculationPriceWithRabatt = $('#calculationPriceWithRabatt').html();
    if(!invoice_adress || !invoice_postcode || !invoice_city) {
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte füllen Sie alle Pflichtfelder im Bereich <b>Adresse</b> aus.');
        $('#theMessageA').modal('show');
        return false;
    }
    if($('#check_invoice_adress').is(':checked')) {
        var invoice_first_name      = $('#invoice_first_name').val();
        var invoice_last_name       = $('#invoice_last_name').val();
        var invoice_function        = $('#invoice_function').val();
        var invoice_alt_adress      = $('#invoice_alt_adress').val();
        var invoice_alt_postcode    = $('#invoice_alt_plz').val();
        var invoice_alt_city        = $('#invoice_alt_city').val();
        if(!invoice_alt_adress || !invoice_alt_postcode || !invoice_alt_city) {
            $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
            $('#theMessageA .modal-body').html('Bitte füllen Sie alle Pflichtfelder im Bereich <b>Abweichende Rechnungsadresse</b> aus.');
            $('#theMessageA').modal('show');
            return false;
        }
    }
    if(paymentAbo == 'monthly') {
        $('#conc_paymentAbo').html('Monatlich');
    } else {
        $('#conc_paymentAbo').html('Jährlich');
    }
    if(paymentType == 'invoice') {
        $('#conc_paymentType').html('Rechnung');
    } else {
         $('#conc_paymentType').html('Bankeinzug');
    }
    $('#conc_invoice_company').html(invoice_company);
    $('#conc_invoice_adress').html(invoice_adress);
    $('#conc_invoice_plz').html(invoice_postcode);
    $('#conc_invoice_city').html(invoice_city);
    $('#conc_calculationRabatt').html(calculationRabatt);
    $('#conc_calculationPriceWithRabatt').html(calculationPriceWithRabatt);

    if($('#check_invoice_adress').is(':checked')) {
        $('#conc_alt_invoice').css('display', 'block');
        $('#conc_alt_invoice_first_name').html(invoice_first_name);
        $('#conc_alt_invoice_last_name').html(invoice_last_name);
        $('#conc_alt_invoice_function').html(invoice_function);
        $('#conc_alt_invoice_adress').html(invoice_alt_adress);
        $('#conc_alt_invoice_plz').html(invoice_alt_postcode);
        $('#conc_alt_invoice_city').html(invoice_alt_city);
    }

    $('#theStep2').fadeOut(200);
    $('#theStep3').fadeIn(200);
    $("html, body").animate(
      {
        scrollTop: 0
      },
      600
    );
}
function toStep2() {
    $('#theStep2').fadeIn(200);
    $('#theStep3').fadeOut(200);
}
function buy() {
    $('#wait').css('display', 'block');
	var userID 		= $('#userID').val();
	var planID 		= $('#planID').val();
	var customerID 	= $('#customerID').val();
	var paymentAbo 	= $('#paymentAbo option:selected').val();
	var paymentType	= $('#paymentType option:selected').val();
	var invoice_company 	= $('#invoice_company').val();
	var invoice_adress 		= $('#invoice_adress').val();
	var invoice_postcode 	= $('#invoice_plz').val();
	var invoice_city 		= $('#invoice_city').val();
    var invoice_first_name      = $('#invoice_first_name').val();
    var invoice_last_name       = $('#invoice_last_name').val();
    var invoice_function        = $('#invoice_function').val();
    var invoice_alt_adress      = $('#invoice_alt_adress').val();
    var invoice_alt_plz         = $('#invoice_alt_plz').val();
    var invoice_alt_city        = $('#invoice_alt_city').val();
    var alt_invoice             = 0;
    if($('#check_invoice_adress').is(':checked')) {
        alt_invoice = 1;
    }
    if(!invoice_adress || !invoice_postcode || !invoice_city) {
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte füllen Sie alle Pflichtfelder im Bereich <b>Adresse</b> aus.');
        $('#theMessageA').modal('show');
        return false;
    }
	$.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=customer.completeAccount",
        data: {
        	customerID:customerID,
            userID:userID,
            planID:planID,
            paymentAbo:paymentAbo,
            paymentType:paymentType,
            invoice_company:invoice_company,
            invoice_adress:invoice_adress,
            invoice_postcode:invoice_postcode,
            invoice_city:invoice_city,
            invoice_first_name:invoice_first_name,
            invoice_last_name:invoice_last_name,
            invoice_function:invoice_function,
            invoice_alt_adress:invoice_alt_adress,
            invoice_alt_plz:invoice_alt_plz,
            invoice_alt_city:invoice_alt_city,
            alt_invoice:alt_invoice
        },
        //dataType: 'json',
        success: function( data ) {
            window.location.href = data; 
        }
     });
}
</script>
