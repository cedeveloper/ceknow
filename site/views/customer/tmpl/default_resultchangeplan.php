<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&task=customer.createDemo'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="row" style="margin-top: 100px;">
        <div class="col-12 col-sm-3"></div>
        <div class="col-12 col-sm-6">
            <?php echo $this->text;?>
        </div>
        <div class="col-12 col-sm-3"></div>
    </div>
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">

</script>
