<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
require_once(JPATH_COMPONENT.'/views/customer/tmpl/newAdmin.php');
require_once(JPATH_COMPONENT.'/views/customer/tmpl/editAdmin.php');
require_once(JPATH_COMPONENT.'/views/customer/tmpl/newContact.php');
require_once(JPATH_COMPONENT.'/views/customer/tmpl/editContact.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

    <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a class="btn btn-success text-white" onclick="checkPlanValids(1);">Speichern</a>
         <a class="btn btn-secondary text-white" onclick="checkPlanValids(2);">Speichern & Schließen</a>
      <?php endif;?>
        <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
    </div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
         <li class="nav-item">
            <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Kundendaten</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="plan-tab" data-toggle="tab" href="#plan" role="tab" aria-controls="plan" aria-selected="false">Plan</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="pay-tab" data-toggle="tab" href="#pay" role="tab" aria-controls="pay" aria-selected="false">Zahlungsdaten</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="false">Administratoren</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="asp-tab" data-toggle="tab" href="#asp" role="tab" aria-controls="asp" aria-selected="false">Ansprechpartner</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="freigabe-tab" data-toggle="tab" href="#freigabe" role="tab" aria-controls="freigabe" aria-selected="false">Freigabe</a>
         </li>
    </ul>
    <div class="tab-content" id="tabContent">
         <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
            <div class="form-horizontal mt-3 w-50">
               <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('customer_number'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('customer_number'); ?>
                    </div>
                </div>
                <!--<div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php //echo $this->form->getLabel('name'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php //echo $this->form->getInput('name'); ?>
                    </div>
                </div>-->
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('company_name'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('company_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('address'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('address'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('postcode'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('postcode'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('city'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('city'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('email'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('web'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('web'); ?>
                    </div>
                </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('ustid'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('ustid'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="tab-pane fade" id="plan" role="tabpanel" aria-labelledby="plan-tab">
            <div class="form-horizontal mt-3 w-50">
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('plan'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('plan'); ?>
                  </div>
               </div>
               <?php 
               if($this->item->planID == 4): 
                  $showPlanvalids = 'block';
               else:
                  $showPlanvalids = 'none';
               endif;
               ?>
               <div id="planValids" style="display: <?php echo $showPlanvalids;?>;">
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('plan_from'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('plan_from'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('plan_to'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('plan_to'); ?>
                     </div>
                  </div>
               </div>
               <h5>Dieser Plan beinhaltet:</h5>
               <div style="background-color: lightgray;padding:10px;">
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        
                     </div>
                     <div class="col-12 col-sm-2 text-center">
                        <b>Plan</b>
                     </div>
                     <div class="col-12 col-sm-2 text-center">
                        <b>In Verwendung</b>
                     </div>
                     <div class="col-12 col-sm-2 text-center">
                        <b>Frei</b>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        Anzahl Administratoren
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_admins" value="<?php echo $this->item->customer_plans->count_admin;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: darksalmon;"readonly="true" id="plan_admins_is" value="<?php echo $this->item->count_customer_administratoren;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <?php 
                        $rest    = $this->item->customer_plans->count_admin - $this->item->count_customer_administratoren;
                        ?>
                        <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        Anzahl Trainer
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_trainer" value="<?php echo $this->item->customer_plans->count_trainer;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: darksalmon;" readonly="true" id="plan_trainer_is" value="<?php echo $this->item->count_customer_trainers;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <?php 
                        $rest    = $this->item->customer_plans->count_trainer - $this->item->count_customer_trainers;
                        ?>
                        <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        Anzahl Learningrooms
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_learningrooms" value="<?php echo $this->item->customer_plans->count_learningrooms;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: darksalmon;" readonly="true" id="plan_trainer_is" value="<?php echo $this->item->count_customer_learningrooms;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <?php 
                        $rest    = $this->item->customer_plans->count_learningrooms - $this->item->count_customer_learningrooms;
                        ?>
                        <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        Anzahl Module
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_modules" value="<?php echo $this->item->customer_plans->count_modules;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: darksalmon;" readonly="true" id="plan_trainer_is" value="<?php echo $this->item->count_customer_modules;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <?php 
                        $rest    = $this->item->customer_plans->count_modules - $this->item->count_customer_modules;
                        ?>
                        <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        Anzahl Units
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_units" value="<?php echo $this->item->customer_plans->count_units;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <input type="text" class="text-center" style="background-color: darksalmon;" readonly="true" id="plan_trainer_is" value="<?php echo $this->item->count_customer_units;?>" />
                     </div>
                     <div class="col-12 col-sm-2">
                        <?php 
                        $rest    = $this->item->customer_plans->count_units - $this->item->count_customer_units;
                        ?>
                        <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="tab-pane fade" id="pay" role="tabpanel" aria-labelledby="pay-tab">
            <div class="form-horizontal mt-3 w-50">
               <div class="form-group row">
                  <div class="col-12 col-sm-4 col-form-label">
                     <?php echo $this->form->getLabel('paymentID'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <?php echo $this->form->getInput('paymentID'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-4 col-form-label">
                     <?php echo $this->form->getLabel('valid_from'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <?php echo $this->form->getInput('valid_from'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-4 col-form-label">
                     <?php echo $this->form->getLabel('valid_to'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <?php echo $this->form->getInput('valid_to'); ?>
                  </div>
               </div>
               <?php
               switch($this->item->customer_payments->paymentID):
                  case 1:
                     $showInvoice      = 'block';
                     $showCreditCard   = 'none';
                     $showDirectDebit  = 'none';
                     break;
                  case 2:
                     $showInvoice      = 'none';
                     $showCreditCard   = 'none';
                     $showDirectDebit  = 'block';
                     break;
                  case 3:
                     $showInvoice      = 'none';
                     $showCreditCard   = 'block';
                     $showDirectDebit  = 'none';
                     break;
                  default:
                     $showInvoice      = 'none';
                     $showCreditCard   = 'none';
                     $showDirectDebit  = 'none';
               endswitch;
               ?>
               <div id="creditCard" style="background-color: lightsalmon;padding: 10px;display: <?php echo $showCreditCard;?>;">
                  <h5>Kreditkartendaten</h5>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('creditcard_bank'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('creditcard_bank'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('creditcard_number'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('creditcard_number'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('creditcard_check_number'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('creditcard_check_number'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('creditcard_valid_to'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('creditcard_valid_to'); ?>
                     </div>
                  </div>
               </div>
               <div id="directDebit" style="background-color: lightcoral;padding: 10px;display: <?php echo $showDirectDebit;?>;">
                  <h5>Lastschrift</h5>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        <?php echo $this->form->getLabel('account_owner'); ?>
                     </div>
                     <div class="col-12 col-sm-8">
                        <?php echo $this->form->getInput('account_owner'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        <?php echo $this->form->getLabel('iban'); ?>
                     </div>
                     <div class="col-12 col-sm-8">
                        <?php echo $this->form->getInput('iban'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        <?php echo $this->form->getLabel('bic'); ?>
                     </div>
                     <div class="col-12 col-sm-8">
                        <?php echo $this->form->getInput('bic'); ?>
                     </div>
                  </div>
               </div>
               <div id="invoice" style="background-color: indianred;padding: 10px;display: <?php echo $showInvoice;?>;">
                  <h5>Rechnungsdaten</h5>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('invoice_title'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('invoice_title'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('invoice_adress'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('invoice_adress'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        <?php echo $this->form->getLabel('invoice_postcode'); ?>
                     </div>
                     <div class="col-12 col-sm-8">
                        <?php echo $this->form->getInput('invoice_postcode'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 col-form-label">
                        <?php echo $this->form->getLabel('invoice_city'); ?>
                     </div>
                     <div class="col-12 col-sm-8">
                        <?php echo $this->form->getInput('invoice_city'); ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="tab-pane fade" id="users" role="tabpanel" aria-labelledby="users-tab">
            <div class="form-horizontal mt-3">
               <a class="btn btn-success text-white mb-3" onclick="newAdmin();">Neuer Administrator</a>
               <h5 class="m-0">Registrierte Administratoren</h5>
               <small class="d-block mb-3">Administratoren erhalten einen Zugang zum Verwaltungsbereich des Systems</small>
               <div class="row">
                  <div class="col-12 col-sm-2">Aktionen</div>
                  <div class="col-12 col-sm-5">Name</div>
                  <div class="col-12 col-sm-5">Kontaktdaten</div>
               </div>
               <div id="admins" class="bg-light p-1">
                  <?php
                  if($this->item->customer_administratoren):
                     foreach($this->item->customer_administratoren as $admin):
                        echo '<div id="admin'.$admin->id.'" class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                        echo '<div class="row">';
                        echo '<div class=" col-12 col-sm-2">';
                        echo '<span class="badge bg-dark text-white mr-2">ID: '.$admin->id.'</span>';
                        echo '<i title="Administrator bearbeiten" onclick="editAdmin('.$admin->id.');" class="fa fa-pencil mr-2"></i>';
                        echo '<i title="Administrator löschen" onclick="deleteAdmin('.$admin->id.');" class="fa fa-trash-o"></i>';
                        echo '</div>';
                        echo '<div class="administratorName col-12 col-sm-5">';
                        echo $admin->salutation.' '.$admin->first_name.' '.$admin->last_name;
                        echo '</div>';
                        echo '<div class=" col-12 col-sm-5">';
                        echo '<span class="badge bg-success text-white"><i class="fa fa-phone"></i> <span class="administratorPhone">'.$admin->phone.'</span></span> | <span class="badge bg-danger text-white"><i class="fa fa-envelope"></i> <span class="administratorEmail">'.$admin->email.'</span></span>';
                        echo '</div>';
                        echo '</div>';
                        echo '</div>';
                     endforeach;
                  else:
                     echo '<div id="adminPlaceholder" class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                     echo 'Keine Administratoren gefunden';
                     echo '</div>';
                  endif;
                  ?>
               </div>
            </div>
         </div>
         <div class="tab-pane fade" id="asp" role="tabpanel" aria-labelledby="asp-tab">
            <div class="form-horizontal mt-3">
               <a class="btn btn-success text-white mb-3" onclick="newContact();">Neuer Ansprechpartner</a>
               <h5 class="m-0">Ansprechpartner</h5>
               <div class="row">
                  <div class="col-12 col-sm-2">Aktionen</div>
                  <div class="col-12 col-sm-4">Name</div>
                  <div class="col-12 col-sm-2">Funktion</div>
                  <div class="col-12 col-sm-4">Kontaktdaten</div>
               </div>
               <div id="contacts" class="bg-light p-1">
                  <?php
                  if($this->item->customer_contacts):
                     foreach($this->item->customer_contacts as $contact):
                        echo '<div id="contact'.$contact->id.'" class="contact" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                        echo '<div class="row">';
                        echo '<div class=" col-12 col-sm-2">';
                        echo '<span class="badge bg-dark text-white mr-2">ID: '.$contact->id.'</span>';
                        echo '<i title="Ansprechpartner bearbeiten" onclick="editContact('.$contact->id.');" class="fa fa-pencil mr-2"></i>';
                        echo '<i title="Ansprechpartner löschen" onclick="deleteContact('.$contact->id.');" class="fa fa-trash-o"></i>';
                        echo '</div>';
                        echo '<div id="edit_contact_feld1" class=" col-12 col-sm-4">';
                        echo $contact->salutation.' '.$contact->first_name.' '.$contact->last_name;
                        echo '</div>';
                        echo '<div id="edit_contact_feld2" class=" col-12 col-sm-2">';
                        echo $contact->function;
                        echo '</div>';
                        echo '<div id="contact_contactdata" class=" col-12 col-sm-4">';
                        if($contact->phone):
                           echo '<span id="edit_contact_feld3" class="badge bg-success text-white"><i class="fa fa-phone"></i> '.$contact->phone.'</span>';
                        endif;
                        if($contact->phone && $contact->email):
                           echo ' | ';
                        endif;
                        if($contact->email):
                           echo '<span id="edit_contact_feld4" class="badge bg-danger text-white"><i class="fa fa-envelope"></i> '.$contact->email.'</span>';
                        endif;
                        echo '</div>';
                        echo '</div>';
                        echo '</div>';
                     endforeach;
                  else:
                     echo '<div id="contactPlaceholder" class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                     echo 'Keine Ansprechpartner gefunden';
                     echo '</div>';
                  endif;
                  ?>
               </div>
            </div>
         </div>
         <div class="tab-pane fade" id="freigabe" role="tabpanel" aria-labelledby="freigabe-tab">
            <div class="form-horizontal mt-3">
               <div class="form-horizontal mt-3 w-50">
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('published'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('published'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('id'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('id'); ?>
                     </div>
                  </div>
                </div>
            </div>
         </div>
    </div>
<input type="hidden" id="id" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      invalidHandler: function(form, validator) {
         //var map = $('name="' + validator.errorMap + '"');
         var iEL  = validator.invalidElements();
         var name = iEL[0].id;
         var tab  = jQuery('#' + name).closest('div.tab-pane');
         $('#myTab li a').each(function() {
            $(this).removeClass('active');
         });
         $('.tab-pane').each(function() {
            $(this).removeClass('show').removeClass('active');
         });
         $('#' + tab.attr('id') + '-tab').addClass('active');
         $('#' + tab.attr('id')).addClass('show').addClass('active');
      },
      rules: {
         'jform[email]': {
            required: false,
            email: true
         },
         'jform[company_name]': {
            required: true
         },
         'jform[plan]': {
            required: true
         }
      },
      messages: {
         'jform[plan]': 'Bitte wählen Sie einen Plan aus.',
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[company_name]': 'Bitte geben Sie einen Firmennamen ein.'
      },
      submitHandler: function(form) {
         form.submit();
      }
   });
   function checkPlanValids(type) {
      var planID = $('#jform_plan option:selected').val();
      if(planID == 4) {
         var plan_from  = $('#jform_plan_from').val();
         var plan_to    = $('#jform_plan_to').val();
         if(!plan_from || !plan_to) {
            alert('Bitte geben Sie den Gültigkeitszeitraum für den Demozugang ein.');
            return false;
         }
      }
      if(planID != 4) {
         $('#jform_paymentID-error').remove();
         var paymentID = $('#jform_paymentID option:selected').val();
         if(!paymentID) {
            alert('Bitte wählen Sie eine Zahlungsart aus.');
            $('.nav-tabs a[href="#pay"]').tab('show');
            $('#jform_paymentID').next('span.select2').append('<label id="jform_paymentID-error" class="error" for="jform_paymentID">Bitte wählen Sie eine Zahlungsart aus.</label>');
            return false;
         } 
         /*if(paymentID != 5) {
            var valid_from = $('#jform_valid_from').val();
            if(!valid_from) {
               $('.nav-tabs a[href="#pay"]').tab('show');
               $('#jform_valid_from').parent('div').append('<label id="jform_valid_from-error" class="error" for="jform_valid_from">Bitte wählen Sie einen Gültigkeitszeitraum aus.</label>');
               return false;
            } 
            var valid_to = $('#jform_valid_to').val();
            if(!valid_to) {
               $('.nav-tabs a[href="#pay"]').tab('show');
               $('#jform_valid_to').parent('div').append('<label id="jform_valid_to-error" class="error" for="jform_valid_to">Bitte wählen Sie einen Gültigkeitszeitraum aus.</label>');
               return false;
            } 
         }*/
      }
      if(type == 1) {
         if(checkInvoice(planID) == true) {
            Joomla.submitform('customer.simpleSave');
         };
      }
      if(type == 2) {
         if(checkInvoice(planID) == true) {
            Joomla.submitform('customer.save');
         };
      }
   }
   function checkInvoice(planID) {
      var paymentID = $('#jform_paymentID option:selected').val();
      if(paymentID == 1 && planID != 4) {
         var invoice_adress   = $('#jform_invoice_adress').val();
         var invoice_postcode = $('#jform_invoice_postcode').val();
         var invoice_city     = $('#jform_invoice_city').val();
         if(!invoice_adress || !invoice_postcode || !invoice_city) {
            alert('Bitte füllen Sie die Felder in den Rechnungsdaten aus.');
            if(!invoice_adress) {
               $('#jform_invoice_adress').parent('div').append('<label id="jform_invoice_adress-error" class="error" for="jform_invoice_adress">Bitte geben Sie eine Rechnungsadresse ein</label>');
            }
            if(!invoice_postcode) {
               $('#jform_invoice_postcode').parent('div').append('<label id="jform_invoice_postcode-error" class="error" for="jform_invoice_postcode">Bitte geben Sie eine Postleitzahl ein</label>');
            }
            if(!invoice_adress) {
               $('#jform_invoice_city').parent('div').append('<label id="jform_invoice_city-error" class="error" for="jform_invoice_city">Bitte geben Sie einen Ort ein</label>');
            }
            return false;
         }
      }
      return true;
   }
   function newContact() {
      $('#newContact').modal('show');
   }
   $('#saveNewContact').on('click', function() {
      saveNewContact();
   });
   function saveNewContact() {
      var customerID    = $('#id').val();
      var salutation    = $('#contact_salutation option:selected').val();
      var first_name    = $('#contact_first_name').val();
      var last_name     = $('#contact_last_name').val();
      var functionS     = $('#contact_function').val();
      var phone         = $('#contact_phone').val();
      var email         = $('#contact_email').val();
      if(!salutation || !first_name || !last_name) {
         alert('Bitte füllen Sie alle Pflichtfelder aus.');
         return false;
      }
      $('#newContact').modal('hide');
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=customer.addContact",
         data: {
            customerID:customerID,
            salutation:salutation,
            first_name:first_name,
            last_name:last_name,
            functionS:functionS,
            phone:phone,
            email:email
         },
         //dataType: 'json',
         success: function( data ) {
            $('#contactPlaceholder').remove();
            $('#contacts').append(
               '<div id="contact' + data + ' class="contact" style="border-bottom: 1px solid #aaa;padding: 4px 0 4px 0;">' + 
               '<div class="row">' + 
               '<div class="col-12 col-sm-2">' + 
               '<span class="badge bg-dark text-white mr-2">ID: ' + data + '</span>' +
               '<i title="Ansprechpartner bearbeiten" onclick="editContact(' + data + ');" class="fa fa-pencil mr-2"></i>'+
               '<i title="Ansprechpartner löschen" onclick="deleteContact(' + data + ');" class="fa fa-trash-o"></i>' +
               '</div>' +
               '<div class="col-12 col-sm-4">' + 
               salutation + ' ' + first_name + ' ' + last_name + 
               '</div>' +
               '<div class="col-12 col-sm-2">' + 
               functionS +
               '</div>' +
               '<div class="col-12 col-sm-4">' + 
               '<span class="badge bg-success text-white"><i class="fa fa-phone"></i> ' + phone + '</span> | <span class="badge bg-danger text-white"><i class="fa fa-envelope"></i> ' + email + '</span>' + 
               '</div>' +
               '</div>' +
               '</div>'
            );
            $('#contact_salutation option:selected').val('').select2();
            $('#contact_first_name').val('');
            $('#contact_last_name').val('');
            $('#contact_function').val('');
            $('#contact_phone').val('');
            $('#contact_email').val('');
         }
      });
   }
   function editContact(id) {
      $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=customer.getContact",
            data: {
               id:id
            },
            dataType: 'json',
            success: function( data ) {
               $('#edit_contact_salutation').val(data['salutation']);
               jQuery('#edit_contact_salutation').select2({
                  placeholder: 'Bitte auswählen'
               });
               $('#edit_contact_first_name').val(data['first_name']);
               $('#edit_contact_last_name').val(data['last_name']);
               $('#edit_contact_phone').val(data['phone']);
               $('#edit_contact_email').val(data['email']);
               $('#edit_contact_function').val(data['function']);
               $('#editContactID').val(id);
            }
         });
      $('#editContact').modal('show');
   }
   $('#saveEditContact').on('click', function() {
      var contactID     = $('#editContactID').val();
      var salutation    = $('#edit_contact_salutation option:selected').val();
      var first_name    = $('#edit_contact_first_name').val();
      var last_name     = $('#edit_contact_last_name').val();
      var functionS     = $('#edit_contact_function').val();
      var phone         = $('#edit_contact_phone').val();
      var email         = $('#edit_contact_email').val(); 
      $('#editContact').modal('hide'); 
      $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=customer.editContact",
            data: {
               contactID:contactID,
               salutation:salutation,
               first_name:first_name,
               last_name:last_name,
               functionS:functionS,
               phone:phone,
               email:email
            },
            dataType: 'json',
            success: function( data ) {
               $('#contact' + contactID + ' #edit_contact_feld1').html(salutation + ' ' + first_name + ' ' + last_name);
               $('#contact' + contactID + ' #edit_contact_feld2').html(functionS);
               if(phone) {
                  if($('#contact' + contactID + ' #edit_contact_feld3')) {
                     $('#contact' + contactID + ' #edit_contact_feld3').html('<i class="fa fa-phone"></i>' + phone );
                  } else {
                     $('#contact' + contactID + ' #contact_contactdata').append(
                        '<span id="contact_feld3" class="badge bg-success text-white">' + 
                        '<i class="fa fa-phone></i>' +
                        phone + 
                        '</span>'
                     );
                  }
               }
               if(email) {
                  if($('#contact' + contactID + ' #edit_contact_feld4')) {
                     $('#contact' + contactID + ' #edit_contact_feld4').html('<i class="fa fa-envelope"></i>' + email);
                  } else {
                     $('#contact' + contactID + ' #contact_contactdata').append(
                        '<span id="contact_feld4" class="badge bg-success text-white">' + 
                        '<i class="fa fa-envelope></i>' +
                        email + 
                        '</span>'
                     );
                  }
               }
            }
         });
   });
   function deleteContact(id) {
      if(confirm('Soll der Ansprechpartner mit der ID ' + id + ' gelöscht werden?') == true) {
         $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=customer.deleteContact",
            data: {
               id:id
            },
            //dataType: 'json',
            success: function( data ) {
               $('#contact' + id).remove();
            }
         });
      }
   }
   function newAdmin() {
      var admins_is  = $('#plan_admins_rest').val();
      if(admins_is == 0) {
         alert('Sie haben die maximale Anzahl an Administratoren für Ihren Plan erreicht.');
         return false;
      }
      $('#newAdmin').modal('show');
   }
   $('#saveNewAdmin').on('click', function() {
      var salutation    = $('#salutation option:selected').val();
      var first_name    = $('#first_name').val();
      var last_name     = $('#last_name').val();
      var email         = $('#email').val();
      var password      = $('#password').val();
      var cpassword     = $('#password_confirm').val();
      if(!salutation) {
         alert('Bitte wählen Sie eine Anrede');
         return false;
      }
      if(!first_name) {
         alert('Bitte geben Sie einen Vornamen ein');
         return false;
      }
      if(!last_name) {
         alert('Bitte geben Sie einen Nachnamen ein');
         return false;
      }
      if(!email) {
         alert('Bitte geben Sie eine E-Mail-Adresse ein');
         return false;
      }
      if(!password) {
         alert('Bitte geben Sie ein Passwort ein');
         return false;
      }
      if(!cpassword) {
         alert('Bitte wiederholen Sie das Passwort');
         return false;
      }
      if(password != cpassword) {
         alert('Beide Passwörter müssen identisch sein');
         return false;
      }
      saveNewAdmin();
   });
   function saveNewAdmin() {
      var customerID    = $('#id').val();
      var salutation    = $('#salutation option:selected').val();
      var first_name    = $('#first_name').val();
      var last_name     = $('#last_name').val();
      var phone         = $('#phone').val();
      var email         = $('#email').val();
      var password      = $('#password').val();
      $('#newAdmin').modal('hide');
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=customer.addAdmin",
         data: {
            customerID:customerID,
            salutation:salutation,
            first_name:first_name,
            last_name:last_name,
            phone:phone,
            email:email,
            password:password
         },
         //dataType: 'json',
         success: function( data ) {
            $('#adminPlaceholder').remove();
            $('#admins').append(
               '<div id="admin' + data + ' class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0 4px 0;">' + 
               '<div class="row">' + 
               '<div class="col-12 col-sm-2">' + 
               '<span class="badge bg-dark text-white mr-2">ID: ' + data + '</span>' +
               '<i title="Administrator bearbeiten" onclick="editAdmin(' + data + ');" class="fa fa-pencil mr-2"></i>'+
               '<i title="Administrator löschen" onclick="deleteAdmin(' + data + ');" class="fa fa-trash-o"></i>' +
               '</div>' +
               '<div class="col-12 col-sm-5">' + 
               salutation + ' ' + first_name + ' ' + last_name + 
               '</div>' +
               '<div class="col-12 col-sm-5">' + 
               '<span class="badge bg-success text-white"><i class="fa fa-phone"></i> ' + phone + '</span> | <span class="badge bg-danger text-white"><i class="fa fa-envelope"></i> ' + email + '</span>' + 
               '</div>' +
               '</div>' +
               '</div>'
            );
            var admin_is   = $('#plan_admins_is').val();
            var admin_rest = $('#plan_admins_rest').val();
            admin_is++;
            admin_rest--;
            $('#plan_admins_is').val(admin_is);
            $('#plan_admins_rest').val(admin_rest);
         }
      });
   }
   function editAdmin(id) {
      $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=customer.getAdmin",
            data: {
               id:id
            },
            dataType: 'json',
            success: function( data ) {
               $('#edit_salutation').val(data['salutation']);
               jQuery('#edit_salutation').select2({
                  placeholder: 'Bitte auswählen'
               });
               $('#edit_first_name').val(data['first_name']);
               $('#edit_last_name').val(data['last_name']);
               $('#edit_phone').val(data['phone']);
               $('#edit_email').val(data['email']);
               $('#administratorID').val(id);
            }
         });
      $('#editAdmin').modal('show');
   }
   function editNewAdmin() {
      var administratorID    = $('#administratorID').val();
      var salutation    = $('#edit_salutation option:selected').val();
      var first_name    = $('#edit_first_name').val();
      var last_name     = $('#edit_last_name').val();
      var phone         = $('#edit_phone').val();
      var email         = $('#edit_email').val();
      var password      = $('#edit_password').val();
      var funktion      = $('#edit_function').val();
      $('#editAdmin').modal('hide');
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=customer.editAdmin",
         data: {
            administratorID:administratorID,
            salutation:salutation,
            first_name:first_name,
            last_name:last_name,
            phone:phone,
            email:email,
            password:password,
            funktion:funktion
         },
         //dataType: 'json',
         success: function( data ) {
            $('#admin' + administratorID + ' .administratorName').html(salutation + ' ' + first_name + ' ' + last_name);
            $('#admin' + administratorID + ' .administratorPhone').html(phone);
            $('#admin' + administratorID + ' .administratorEmail').html(email);
            /*$('#adminPlaceholder').remove();
            $('#admins').append(
               '<div id="admin' + data + ' class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0 4px 0;">' + 
               '<div class="row">' + 
               '<div class="col-12 col-sm-2">' + 
               '<span class="badge bg-dark text-white mr-2">ID: ' + data + '</span>' +
               '<i title="Administrator bearbeiten" onclick="editAdmin(' + data + ');" class="fa fa-pencil mr-2"></i>'+
               '<i title="Administrator löschen" onclick="deleteAdmin(' + data + ');" class="fa fa-trash-o"></i>' +
               '</div>' +
               '<div class="col-12 col-sm-5">' + 
               salutation + ' ' + first_name + ' ' + last_name + 
               '</div>' +
               '<div class="col-12 col-sm-5">' + 
               '<span class="badge bg-success text-white"><i class="fa fa-phone"></i> ' + phone + '</span> | <span class="badge bg-danger text-white"><i class="fa fa-envelope"></i> ' + email + '</span>' + 
               '</div>' +
               '</div>' +
               '</div>'
            );
            var admin_is   = $('#plan_admins_is').val();
            var admin_rest = $('#plan_admins_rest').val();
            admin_is++;
            admin_rest--;
            $('#plan_admins_is').val(admin_is);
            $('#plan_admins_rest').val(admin_rest);*/
         }
      });
   }
   function deleteAdmin(id) {
      if(confirm('Soll der Administrator mit der ID ' + id + ' gelöscht werden?') == true) {
         $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=customer.deleteAdmin",
            data: {
               id:id
            },
            //dataType: 'json',
            success: function( data ) {
               $('#admin' + id).remove();
               var admin_is   = $('#plan_admins_is').val();
               var admin_rest = $('#plan_admins_rest').val();
               admin_is--;
               admin_rest++
               $('#plan_admins_is').val(admin_is);
               $('#plan_admins_rest').val(admin_rest);
            }
         });
      }
   }
   function loadPlanData() {
      var planID  = $('#jform_plan option:selected').val();
      if(planID == 4) {
         $('#planValids').css('display', 'block');
      } else {
         $('#planValids').css('display', 'none');
      }
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=customer.loadPlanData",
         data: {
            planID:planID
         },
         dataType: 'json',
         success: function( data ) {
            $('#plan_admins').val(data['count_admin']);
            $('#plan_trainer').val(data['count_trainer']);
            $('#plan_learningrooms').val(data['count_learningrooms']);
            $('#plan_modules').val(data['count_modules']);
            $('#plan_units').val(data['count_units']);
         }
      });
   }
   function loadPaymentData() {
      var paymentID  = $('#jform_paymentID option:selected').val();
      if(paymentID) {
         $('#jform_paymentID-error').remove();
      }
      if(paymentID == 3) {
         $('#creditCard').fadeIn(200);
         $('#directDebit').fadeOut(100);
         $('#invoice').fadeOut(100);
      }
      if(paymentID == 1) {
         $('#creditCard').fadeOut(100);
         $('#directDebit').fadeOut(100);
         $('#invoice').fadeIn(200);
      }
      if(paymentID == 2) {
         $('#creditCard').fadeOut(100);
         $('#directDebit').fadeIn(200);
         $('#invoice').fadeOut(100);
      }
      if(paymentID == 4) {
         $('#creditCard').fadeOut(100);
         $('#directDebit').fadeOut(200);
         $('#invoice').fadeOut(100);
      }
   }
   jQuery('#jform_email').on('change', function() {
      var email = jQuery('#jform_email').val();
      jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
            data: {email:email},
            //dataType: 'json',
            success: function( data ) {
               if(data == 'OK') {
                  jQuery('#jform_username').val(email);
               } else {
                  alert('Die E-Mail-Adresse wird bereits verwendet.');
                  jQuery('#jform_email').val('');
                  return false;
               }
            }
        });
   });
</script>