<?php
/**
 * ceKNOW - LMS
 * @version     2.1.12c
 * @package     com_jclassroom
 * @copyright   Copyright (C) 2021. Alle Rechte vorbehalten.
 * @author      ce - corporate eductaion GmbH
 * @email       tscheel@ce.de
 */
// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
?>
<style>
   #adminForm ul li {
      list-style: none;
   }
   #adminForm ul li:before {
      content: '\f05d';
      font-family: 'Fontawesome';
      display: inline-block;
      width: 20px;
   }
   .submessage {
      margin: 0px;
   }
   .showP {
      background-color: #ffffff;
      position: absolute;
      left: 18px;
      top: 3px;
      width: 84%;
      padding: 5px;
      display: none;
   }
</style>
<!--<form action="<?php //echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">-->
    <div id="introtext">
        <div class="row" style="margin-top: 50px;">
            <div class="col-12 col-sm-3"></div>
            <div class="col-12 col-sm-6">
                <?php echo $this->intro1;?>
            </div>
            <div class="col-12 col-sm-3"></div>
        </div>
    </div>
    <div id="register">
        <div class="row" style="margin-top: 50px;">
            <div class="col-12 col-sm-3"></div>
            <div class="col-12 col-sm-6">
                <?php echo $this->register;?>
            </div>
            <div class="col-12 col-sm-3"></div>
        </div>
    </div>
</div>
<?php echo JHtml::_('form.token'); ?>
<input type="hidden" id="jform_planID" name="jform[planID]" value="<?php echo $this->pid;?>" />
<input type="hidden" id="paymentIDPlan" value="<?php echo $this->item->paymentIDPlan;?>"/>
<input type="hidden" id="secureID" value=""/>
<input type="hidden" id="ssecure" value="" />
<!--</form>-->
<div id="getInfo" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 700px!important;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Kontaktieren Sie uns</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <?php echo $this->asp;?>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
         </div>
      </div>
   </div>
</div>
<div id="theMessageA" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 700px!important;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Nachricht</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
   $.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=customer.getSecure",
      data: {},
      //dataType: 'json',
      async: false,
      success: function( data ) {
         var r = data.split('_');
         $('#n1').html(r[0]);
         $('#n2').html(r[1]);
         $('#secureID').val(r[3]);
      }
   });
});
var form = $("#adminForm");
$.validator.setDefaults({
  ignore: []
});
form.validate({
  invalidHandler: function(form, validator) {
     /*var map = $('name="' + validator.errorMap + '"');
     var iEL  = validator.invalidElements();
     var name = iEL[0].id;
     var tab  = jQuery('#' + name).closest('div.tab-pane');
     $('#myTab li a').each(function() {
        $(this).removeClass('active');
     });
     $('.tab-pane').each(function() {
        $(this).removeClass('show').removeClass('active');
     });
     $('#' + tab.attr('id') + '-tab').addClass('active');
     $('#' + tab.attr('id')).addClass('show').addClass('active');*/
  },
  rules: {
    'jform[salutation]': {
        required: true
    },
    'jform[email]': {
        required: true,
        email: true
    },
    'jform[first_name]': {
        required: true
    },
    'jform[paymentIDPlan]': {
        required: true
     },
    'p1': {
        required: true
     }
  },
  messages: {
     'jform[salutation]': 'Bitte wählen Sie eine Anrede aus.',
     'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
     'jform[first_name]': 'Bitte geben Sie Ihren Vornamen ein.',
     'jform[paymentIDPlan]': 'Bitte wählen Sie eine Zahlungsart aus.',
     'p1': 'Bitte geben Sie ein Passwort ein.'
  },
  submitHandler: function(form) {
     form.submit();
  }
});
$(document).ready(function() {
    $('#secure').val('');
    $('#secure').removeClass('is-invalid');
    $('#first_name').val('');
    $('#last_name').val('');
    $('#function').val('');
    $('#email').val('');
    $('#p1').val('');
    $('#p2').val('');
    var paymentID = $('#paymentIDPlan').val();
    if(paymentID == 1) {
        $('#payment1').css('display','block');
        $('#payment2').css('display','none');
    }
    if(paymentID == 2) {
        $('#payment2').css('display','block');
        $('#payment1').css('display','none');
    }
});
$('#secure').on('click', function() {
   $('#secure').removeClass('is-invalid');
})
$('#secure').on('change', function() {
   var secureID = $('#secureID').val();
   $('#theSpinner').fadeIn(200);
   $.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=customer.compareSecure",
      data: {secureID:secureID},
      //dataType: 'json',
      success: function( data ) {
         var r       = data.split('_');
         var secure  = $('#secure').val();
         if(secure == r[2]) {
            $('#secure').removeClass('is-invalid').addClass('is-valid');
            $('#ssecure').val(r[2]);
            $('#theSpinner').fadeOut(200);
            $('#theSubmit').removeAttr('disabled');
         } else {
            $('#secure').removeClass('is-valid').addClass('is-invalid');
            $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
           $('#theMessageA .modal-body').html('Bitte beantworten Sie die Sicherheitsabfrage.');
           $('#theMessageA').modal('show');
           $('#secure').addClass('is-invalid');
           $('#theSubmit').prop('disabled',true);
           return false;
         }
      }
   });
})
function createAccount() {
    var first_name  = $('#first_name').val();
    var last_name   = $('#last_name').val();
    var email       = $('#email').val();
    var p1      = $('#p1').val();
    var p2      = $('#p2').val();
    $('#secure').focusout();
    if(!first_name) {
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte geben Sie Ihren Vornamen ein.');
        $('#first_name').addClass('is-invalid');
        $('#theMessageA').modal('show');
        return false;
    }
    if(!last_name) {
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte geben Sie Ihren Nachnamen ein.');
        $('#last_name').addClass('is-invalid');
        $('#theMessageA').modal('show');
        return false;
    }
    if(!email) {
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte geben Sie Ihre E-Mail-Adresse ein.');
        $('#email').addClass('is-invalid');
        $('#theMessageA').modal('show');
        return false;
    }
    if(!p1) {
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte geben Sie ein sicheres Passwort ein.');
        $('#p1').addClass('is-invalid');
        $('#theMessageA').modal('show');
        return false;
    }
    if($('#secure').val() != $('#ssecure').val()) {
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte beantworten Sie die Sicherheitsabfrage.');
        $('#theMessageA').modal('show');
        $('#secure').addClass('is-invalid');
        return false;
    }
    var check   = 0;
    if($('#theCheck').is(':checked')) {
        check = 1;
    }
    if(check == 0) {
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte bestätigen Sie, dass Sie unsere <b>Datenschutzerklärung</b> und die <b>Nutzungsbedingungen</b> zur Kenntnis genommen haben und mit diesen einverstanden sind.');
        $('#theMessageA').modal('show');
        $('#theCheck').addClass('is-invalid');
        return false;
    }
    if(check == 1) {
        $('#p1').removeClass('is-invalid');
        $('#theCheck').removeClass('is-invalid');
        createAccountExecute();
    }
}
function createAccountExecute() {
    var first_name  = $('#first_name').val();
    var last_name   = $('#last_name').val();
    var email   = $('#email').val();
    var p       = $('#p1').val();
    var functionA   = $('#function').val();
    var planID  = $('#jform_planID').val();
    $.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=customer.createAccount",
        data: {
            email:email,
            p:p,
            first_name:first_name,
            last_name:last_name,
            planID:planID,
            functionA:functionA
        },
        //dataType: 'json',
        success: function( data ) {
            window.location.href = data; 
        }
     });
}


jQuery('#email').on('change', function() {
    var email = $('#email').val();
    $.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=customer.checkEmail",
        data: {
           email:email
        },
        //dataType: 'json',
        success: function( data ) {
            if(data == 'OK_') {
            } else {
                $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Warnung');
                $('#theMessageA .modal-body').html('Die E-Mail-Adresse <b>' + email + '</b> ist bereits registriert.');
                $('#theMessageA').modal('show');
                jQuery('#email').val('');
                $('#p1').val('');
                $('#p2').val('');
                return false;
            }
        }
     });
});
function showInfo() {
    $('#getInfo').modal('show');
}
function setPaymentInviroment() {
    var paymentID = $('#jform_paymentIDPlan option:selected').val();
    if(paymentID == 1) {
        $('#payment1').css('display','block');
        $('#payment2').css('display','none');
    }
    if(paymentID == 2) {
        $('#payment2').css('display','block');
        $('#payment1').css('display','none');
    }
}
$('#theSubmit').on('click', function(e) {
    var paymentID = $('#jform_paymentIDPlan option:selected').val();
    if(paymentID ==1) {
        var invoice_title   = $('#jform_invoice_title_new').val();
        var invoice_adress  = $('#jform_invoice_adress_new').val();
        var invoice_postcode = $('#jform_invoice_postcode_new').val();
        var invoice_city    = $('#jform_invoice_city_new').val();
        if(!invoice_title || !invoice_adress || !invoice_postcode || !invoice_city) {
            $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
            $('#theMessageA .modal-body').html('Bitte füllen Sie die Pflichtfelder aus.');
            $('#theMessageA').modal('show');
            return false;
        }
    }
    if(paymentID ==2) {
        var account_owner   = $('#jform_account_owner').val();
        var iban  = $('#jform_iban').val();
        var bic = $('#jform_bic').val();
        if(!account_owner || !iban || !bic) {
            $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
            $('#theMessageA .modal-body').html('Bitte füllen Sie die Pflichtfelder aus.');
            $('#theMessageA').modal('show');
            return false;
        }
    }
    var p1 = $('#p1').val();
    if(!p1) {
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte geben Sie ein sicheres Passwort ein.');
        $('#theMessageA').modal('show');
        return false;
    }
    var check = 0;
    if($('#theCheck').is(':checked')) {
        check = 1;
    }
    if(check == 0) {
        e.preventDefault();
        $('#theMessageA .modal-title').html('<i class="fa fa-exclamation-circle text-danger"></i> Nachricht');
        $('#theMessageA .modal-body').html('Bitte bestätigen Sie, dass Sie unsere Datenschutzerklärung und die Nutzungsbedingungen zur Kenntnis genommen haben und mit diesen einverstanden sind.');
        $('#theMessageA').modal('show');
        return false;
    }
    if(check == 1) {
        $('#adminForm').submit();
    }
});

function checkP1() {
   var p1 = $('#p1').val();
   var p2 = $('#p2').val();
   var html = '';
   var checkLength  = 0;
   var checkNumber  = 0;
   var checkUpper   = 0;
   var checkSpecial = 1;
   if(p1.length < 8) {
      html += '<p class="submessage">Die Passwortlänge ist zu kurz. Vergeben Sie mindestens ein 8 Zeichen langes Passwort.</p>';
      checkLength = 1;
   }
   var rgularExp = {
     containsNumber : /\d+/
   }
   if(rgularExp.containsNumber.test(p1) == false) {
      html += '<p class="submessage">Das Passwort muss mindestens 1 Zahl enthalten.</p>';
      $('#checkLength').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
      checkNumber = 1;
   }
   if(!p1.match(/[A-Z]{1}/)) {
      html += '<p class="submessage">Das Passwort muss einen GROSSBUCHSTABEN enthalten.</p>';
      checkUpper = 1;
   } 
   var specialChars = "<>@§!#$%^&*()_+[]{}?:;|'\"\\,./~`-=";
   for(i = 0; i < specialChars.length;i++) {
      if(p1.indexOf(specialChars[i]) > -1) {
         checkSpecial = 0;
      }
   }
   if(checkSpecial == 1) {
      html += '<p class="submessage">Das Passwort muss SONDERZEICHEN enthalten.</p>';
   }
   if(html) {
      $('#theMessage').html(html);
      $('#theMessage').css('display','inline-block');
      $('#p2').attr('readonly',true).attr('disabled', true);
   } else {
      $('#theMessage').css('display','none');
      $('#p2').attr('readonly',false).attr('disabled', false).focus();
   }
   if(checkLength == 1) {
      $('#checkLength').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      $('#checkLength').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
   if(checkNumber == 1) {
      $('#checkNumber').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      $('#checkNumber').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
   if(checkUpper == 1) {
      $('#checkUpper').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      $('#checkUpper').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
   if(checkSpecial == 1) {
      $('#checkSpecial').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      $('#checkSpecial').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
   $('#p1').removeClass('is-invalid');
}
function checkP2() {
   var p1 = $('#p1').val();
   var p2 = $('#p2').val();
   var html = '';
   var checkUnique  = 0;
   if(p1 != p2) {
      html += '<p class="submessage">Beide Passwörter müssen identisch sein.</p>';
      checkUnique = 1;
   }
   if(html) {
      $('#theMessage').html(html);
      $('#theMessage').css('display','inline-block');
   } else {
      $('#theMessage').css('display','none');
   }

   if(checkUnique == 1) {
      $('#checkUnique').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      $('#checkUnique').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
}
$('#iShowP1').on('mousedown', function() {
   var sp1 = $('#p1').val();
   if(sp1) {
      $('#showP1').html(sp1).css('display', 'inline');
   }
});
$('#iShowP1').on('mouseup', function() {
   $('#showP1').css('display','none');
});

$('#iShowP2').on('mousedown', function() {
   var sp2 = $('#p2').val();
   if(sp2) {
      $('#showP2').html(sp2).css('display', 'inline');
   }
});
$('#iShowP2').on('mouseup', function() {
   $('#showP2').css('display','none');
});
</script>
