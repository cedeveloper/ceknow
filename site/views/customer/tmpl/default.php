<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">
  var onloadCallback = function() {
    grecaptcha.render('html_element', {
      'sitekey' : '6LdCzNIZAAAAADqFiQe6u3P5rIcnqqKWUAfLxO_h'
    });
  };
</script>
<style>
    #radiogroup-size input {
        margin-right: 10px;
    }
    #radiogroup-size label {
        color: #fff;
        margin-right: 15px;
    }
</style>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&task=customer.createDemo'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="row" style="margin-top: 50px;">
        <div class="col-12 col-sm-3"></div>
        <div class="col-12 col-sm-6">
            <?php echo $this->demoaccount;?>
        </div>
        <div class="col-12 col-sm-3"></div>
    </div>
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="theDialog" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title"><i class="fa fa-exclamation-circle text-danger"></i> Nachricht</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p id="dialogMessage"></p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>
    </div>
</div>
<script type="text/javascript">
 var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      invalidHandler: function(form, validator) {

      },
      rules: {
         'jform[email]': {
            required: false,
            email: true
         }
      },
      messages: {
         'jform[register_email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.'
      },
      submitHandler: function(form) {
         form.submit();
      }
   });
jQuery('#jform_register_email').on('change', function() {
    var email = jQuery('#jform_register_email').val();
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=customer.checkEmail",
        data: {email:email},
        //dataType: 'json',
        success: function( data ) {
            var d1 = data.split('_');
            if(d1[0] == 'OK') {
              jQuery('#jform_register_email').val(email);
            } else {
              $('#dialogMessage').html('Die E-Mail-Adresse ' + email + ' ist bereits registriert.');
                $('#theDialog').modal('show');
              jQuery('#jform_register_email').val('');
              return false;
           }
        }
    });
});

$('#createDemo').on('click', function() {
    let first_name  = $('#jform_register_first_name').val();
    let last_name   = $('#jform_register_last_name').val();
    let email       = $('#jform_register_email').val();
    let password    = $('#jform_register_password').val();
    let dsgvo       = $('#dsgvo');
    
    if(!first_name || !last_name || !email || !password) {
        $('#dialogMessage').html("Bitte füllen Sie die Pflichtfelder aus.");
        $('#theDialog').modal('show');
        return false;
    }
 
    if(!dsgvo.is(':checked')) {
        $('#dialogMessage').html("Bitte bestätigen Sie, dass Sie die Datenschutzererklärung gelesen und zur Kenntnis genommen haben.");
        $('#theDialog').modal('show');
        return false;
    }
    $('#adminForm').submit();
});
</script>
