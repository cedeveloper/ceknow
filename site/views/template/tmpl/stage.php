<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');

?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&task=template.sendTemplate&id='.$this->item->id);?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

   <div class="buttonleiste d-inline-block w-100 mb-4">       
      <a href="template-edit?id=<?php echo $this->item->id;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
      <div class="form-group row">
         <input style="width: 400px;" type="text" id="reciever" name="reciever" placeholder="Empfänger eingeben" value="" />
         <button type="submit" class="btn btn-danger text-white m-1">Versenden</button>
      </div>
      <div class="form-group row">
         <?php echo $this->item->text;?>
      </div>
   </div>
        
<input type="hidden" name="jform[id]" value="<?php echo $this->item->id;?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
jQuery('#adminForm').on('submit', function(e) {
   
   if(jQuery('#reciever').val() == '') {
      e.preventDefault();
      alert('Geben Sie eine gültige E-Mail-Adresse ein.');
      return false;
   } 
})
</script>