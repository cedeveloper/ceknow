<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/imgForJCE.js');
$doc->addScript('https://cdn.tiny.cloud/1/nup2cipzhyxgl8grs73mx7786ygtlyloqg61ez5xyx42k2t8/tinymce/5/tinymce.min.js" referrerpolicy="origin">');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

   <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
      <a class="btn btn-success text-white" onclick="Joomla.submitform('template.apply')">Speichern</a>
      <a class="btn btn-secondary text-white" onclick="Joomla.submitform('template.save')">Speichern & Schließen</a>
      <a class="btn btn-primary text-white" onclick="Joomla.submitform('template.save2copy')">Kopieren</a>
      <a href="template-edit?layout=stage&id=<?php echo $this->item->id;?>" class="btn btn-info text-white">Stage Template</a>
      <?php endif;?>
      <a href="email-templates" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('title'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('title'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('type'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('type'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('description'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('description'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('subject'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('subject'); ?>
         </div>
      </div>
      <div class="card bg-light">
         <h4>Shortcuts, die Sie in Ihrem Template verwenden können</h4>
         <ul>
            <li><b>{salutation}</b> -> Fügt die Anrede des Teilnehmers ein</li>
            <li><b>{first_name}</b> -> Fügt den Vornamen des Teilnehmers ein</li>
            <li><b>{last_name}</b> -> Fügt den Nachnamen des Teilnehmers ein</li>
            <li><b>{email}</b> -> Fügt die E-Mail-Adresse des Teilnehmers ein</li>
            <li><b>{lr_name}</b> -> Fügt den Namen des Learningroom ein</li>
            <li><b>{activationlink}</b> -> Fügt einen codierten Aktivierungslink ein</li>
            <li><b>{systemname}</b> -> Fügt den Namen des Systems ein</li>
            <li><b>{main_trainer}</b> -> Fügt den vollständigen Namen des Hauptrainers ein</li>
            <li><b>{co_trainer}</b> -> Fügt den vollständigen Namen des Co-Trainer ein</li>
            <li><b>{first_day}</b> -> Fügt das Datum des ersten Veranstaltungstages ein</li>
            <li><b>{last_day}</b> -> Fügt das Datum des letzten Veranstaltungstages ein</li>
            <li><b>{all_days}</b> -> Fügt eine Auflistung aller Veranstaltungstage ein</li>
         </ul>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('text'); ?>
         </div>
         <div class="use_editor col-12 col-sm-10" data-editor="jform_text">
            <?php echo $this->form->getInput('text'); ?>
            <?php require(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');?>
            <a class="btn btn-primary text-white mt-1" onclick="createTemplate();">Basistemplate laden</a>
         </div>
      </div>
      
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('head'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('head'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('body'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('body'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('styles'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('styles'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('scripts'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('scripts'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('published'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('published'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('id'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('id'); ?>
         </div>
      </div>
   </div>
<input type="hidden" id="id" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<?php
require_once(JPATH_COMPONENT.'/views/dialogs/imageContent.php');
require_once(JPATH_COMPONENT.'/views/dialogs/formForJCE.php');
?>
<script type="text/javascript">
   let classroomID = false;
   let unitID = $('#id').val();
   let part = 'template';
   setTinyMCE();
   function setTinyMCE() {
      tinymce.init({
         selector: '.tinymce',
         plugins: 'image table code link',
         menubar: 'file insert format table tools',
         toolbar: 'undo redo format fontsizeselect alignleft alignright aligncenter forecolor backcolor code table image link responsivefilemanager',
         image_uploadtab: true,
         contextmenu: false,
         content_style: '#tinymce {font-family: Arial;font-size: 14px;line-height: 18px;}',
         min_height: 600,
         branding: false,
         custom_undo_redo_levels: 10,
         external_filemanager_path:"/celearning/components/com_jclassroom/assets/filemanager/",
         filemanager_title:"ceLearning Filemanager" ,
         external_plugins: {"filemanager": "/celearning/components/com_jclassroom/assets/filemanager/plugin.min.js"}
      });
   }
   var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[email]': {
            required: true,
            email: true
         },
         'jform[first_name]': {
            required: true
         },
         'jform[last_name]': {
            required: true
         }
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[first_name]': 'Bitte geben Sie Ihren Vornamen ein.',
         'jform[last_name]': 'Bitte geben Sie Ihren Nachnamen ein.'
      },
      submitHandler: function(form) {
         form.submit();
      }
   });
   function createTemplate() {
      if(confirm("Möchten Sie ein leeres Basistemplate in den Editor laden?") == true) {
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=configuration.loadBasicTemplate",
            data: {},
            //dataType: 'json',
            success: function( data ) {
               if(data) {
                  tinyMCE.activeEditor.execCommand('mceInsertContent',false,data);
               } 
            }
        });
      }
   }
   jQuery('#jform_email').on('change', function() {
      var email = jQuery('#jform_email').val();
      jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
            data: {email:email},
            //dataType: 'json',
            success: function( data ) {
               if(data == 'OK') {
                  jQuery('#jform_username').val(email);
               } else {
                  alert('Die E-Mail-Adresse wird bereits verwendet.');
                  jQuery('#jform_email').val('');
                  return false;
               }
            }
        });
   });
</script>