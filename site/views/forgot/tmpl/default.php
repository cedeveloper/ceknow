<?php
/**
 *  	ce corporate education GmbH
 * 	  	JClassroom 2.1
 * 	 	(C) 2021. All Rights reserved.
 * 		25. Januar 2021
 *		Developer: Torsten Scheel
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');

?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&task=forgot.reset'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<p>Hier können Sie Ihr Passwort zurücksetzen.</p>
	<p>1. Geben Sie in das Eingabefeld Ihre E-Mail-Adresse ein und bestätigen Sie die Sicherheitsabfrage. Sie erhalten im Anschluss eine E-Mail an Ihre E-Mail-Adresse.<br/>Klicken Sie hier auf den Link <b><i>Passwort zurücksetzen</i></b>.</p>
	<p>2. Sie werden auf eine Seite geleitet, auf der Sie die weiteren Schritte vornehmen.</p>
	<p>3. Vergeben Sie nun ein neues Passwort und bestätigen Sie es im zweiten Eingabefeld.</p>
	<p>4. Danach können Sie sich mit Ihrem Benutzernamen und dem neuen Passwort anmelden.</p>
	<div class="form-horizontal mt-5">
        <div class="form-group row w-50">
            <div class="col-3">
              E-Mail-Adresse
            </div>
            <div class="col-9">
            	<input onchange="checkMail();" type="email" name="email" id="email" placeholder="Geben Sie Ihre E-Mail-Adresse ein" value="" />
            </div>
        </div>
        <div class="form-group row w-50">
        	<div class="col-3"></div>
            <div class="col-9">
            	<button id="submit" type="submit" class="btn btn-primary">Passwort zurücksetzen</button>
            </div>
        </div>
   	</div>
<?php echo JHtml::_('form.token'); ?>
</form>
<script>
var form = $("#adminForm");
$.validator.setDefaults({
  ignore: []
});
form.validate({
  rules: {
     'email': {
        required: true,
        email: true
     }
  },
  messages: {
     'email': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.'
  },
  submitHandler: function(form) {
     form.submit();
  }
});
function checkMail() {
    var email = $('#email').val();
    $.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=forgot.checkMail",
        data: {
           email:email
        },
        //dataType: 'json',
        success: function( data ) {
           console.log(data);
        }
     });
}
</script>