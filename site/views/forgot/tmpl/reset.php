<?php
/**
 *  	ce corporate education GmbH
 * 	  	JClassroom 2.1
 * 	 	(C) 2021. All Rights reserved.
 * 		25. Januar 2021
 *		Developer: Torsten Scheel
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$input   = JFactory::getApplication()->input;
$mark    = $input->get('SMA', '', 'STR');
?>
<style>
   #adminForm ul li {
      list-style: none;
   }
   #adminForm ul li:before {
      content: '\f05d';
      font-family: 'Fontawesome';
      display: inline-block;
      width: 20px;
   }
   .submessage {
      margin: 0px;
   }
   .showP {
      background-color: #ffffff;
      position: absolute;
      left: 2px;
      top: -5px;
      width: 95%;
      padding: 5px;
      display: none;
   }
</style>
<form style="margin-top: 35px;" action="<?php echo JRoute::_('index.php?option=com_jclassroom&task=forgot.resetgo'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
   <div class="row">
      <div id="theLeft" class="col-12 col-sm-3" style="background-color: #ff3600;padding: 25px 40px;">
         <h1 style="font-size: 48px;"><span style="color: #ffffff;">Passwort</span><br/>zurücksetzen</h1>
         <p><b>Um ein sicheres Passwort zu erstellen, verwenden Sie:</b></p>
         <ul class="p-0">
            <li>8 Zeichen insgesamt</li>
            <li>1 Zahl</li>
            <li>1 GROSSBUCHSTABE</li>
            <li>1 Sonderzeichen</li>
         </ul>
      </div>
      <div id="theRight" class="col-12 col-sm-9" style="padding: 80px 150px;">
        <div class="form-horizontal" style="margin-top: 150px;">
           <div class="form-group row">
               <div class="col-12">
                  <div style="position: relative; display: inline;">
                  	<input type="password" onchange="checkP1();" style="display: inline;width: 50%;" name="p1" id="p1" placeholder="Neues Passwort" value="" /> 
                     <span id="showP1" class="showP"></span>
                  </div>
                  <i id="iShowP1" class="fa fa-eye"></i>
                  <br/>
                  <i id="checkLength" class="pt-3 fa fa-ban text-danger"></i> 8 Zeichen Länge<br/>
                  <i id="checkNumber" class="fa fa-ban text-danger"></i> 1 Zahl<br/>
                  <i id="checkUpper" class="fa fa-ban text-danger"></i> 1 GROSSBUCHSTABE<br/>
                  <i id="checkSpecial" class="fa fa-ban text-danger"></i> 1 Sonderzeichen<br/>
                  <p style="background-color: red; color: #ffffff;padding: 5px;margin-top: 5px;font-size: 14px;font-style: italic;display: none;font-weight: bold;" id="theMessage"></p>
               </div>
           </div>
           <div class="form-group row">
               <div class="col-12">
                  <div style="position: relative; display: inline;">
                     <input type="password" onchange="checkP2();" readonly="true" disabled="disabled" style="display: inline;width: 50%;" name="p2" id="p2" placeholder="Neues Passwort wiederholen" value="" />
                     <span id="showP2" class="showP"></span>
                  </div>
                  <i id="iShowP2" class="fa fa-eye"></i>
                  <br/>
                  <i id="checkUnique" class="pt-3 fa fa-ban text-danger"></i> Passwörter identisch<br/>
               </div>
           </div>
           <div class="form-group row">
               <div class="col-12">
               	<button id="submit" type="submit" class="btn btn-primary">Passwort zurücksetzen</button>
               </div>
           </div>
         </div>
   	</div>
   </div>
   <input type="hidden" id="mark" name="mark" value="<?php echo $mark;?>" />
   <?php echo JHtml::_('form.token'); ?>
</form>
<script>
$(document).ready(function() {
   var w1 = $(window).width();
   var w2 = $('.container').width();
   var h1 = $(window).height();
   w3 = (w2 - w1) / 2;
   $('#adminForm > .row').css('margin-left', w3 + 'px').css('margin-right', w3 + 'px');
   $('#theLeft').css('height',h1 + 5);
   $('#sp-main-body').css('padding-bottom', '0px');
});
var form = $("#adminForm");
$.validator.setDefaults({
  ignore: []
});
form.validate({
  rules: {
     'p1': {
        required: true
     },
     'p2': {
        required: true
     }
  },
   messages: {
      'p1': 'Bitte geben Sie ein neues Passwort ein.',
      'p2': 'Wiederholen Sie das neue Passwort.'
   },
   submitHandler: function(form) {
      
      form.submit();
   }
});
function checkP1() {
   var p1 = $('#p1').val();
   var p2 = $('#p2').val();
   var html = '';
   var checkLength  = 0;
   var checkNumber  = 0;
   var checkUpper   = 0;
   var checkSpecial = 1;
   if(p1.length < 8) {
      html += '<p class="submessage">Die Passwortlänge ist zu kurz. Vergeben Sie mindestens ein 8 Zeichen langes Passwort.</p>';
      checkLength = 1;
   }
   var rgularExp = {
     containsNumber : /\d+/
   }
   if(rgularExp.containsNumber.test(p1) == false) {
      html += '<p class="submessage">Das Passwort muss mindestens 1 Zahl enthalten.</p>';
      $('#checkLength').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
      checkNumber = 1;
   }
   if(!p1.match(/[A-Z]{1}/)) {
      html += '<p class="submessage">Das Passwort muss einen GROSSBUCHSTABEN enthalten.</p>';
      checkUpper = 1;
   } 
   var specialChars = "<>@§!#$%^&*()_+[]{}?:;|'\"\\,./~`-=";
   for(i = 0; i < specialChars.length;i++) {
      if(p1.indexOf(specialChars[i]) > -1) {
         checkSpecial = 0;
      }
   }
   if(checkSpecial == 1) {
      html += '<p class="submessage">Das Passwort muss SONDERZEICHEN enthalten.</p>';
   }
   if(html) {
      $('#theMessage').html(html);
      $('#theMessage').css('display','inline-block');
      $('#p2').attr('readonly',true).attr('disabled', true);
   } else {
      $('#theMessage').css('display','none');
      $('#p2').attr('readonly',false).attr('disabled', false).focus();
   }
   if(checkLength == 1) {
      $('#checkLength').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      $('#checkLength').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
   if(checkNumber == 1) {
      $('#checkNumber').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      $('#checkNumber').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
   if(checkUpper == 1) {
      $('#checkUpper').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      $('#checkUpper').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
   if(checkSpecial == 1) {
      $('#checkSpecial').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      $('#checkSpecial').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
}
function checkP2() {
   var p1 = $('#p1').val();
   var p2 = $('#p2').val();
   console.log(p1 + ' ' + p2);
   var html = '';
   var checkUnique  = 0;
   if(p1 != p2) {
      html += '<p class="submessage">Beide Passwörter müssen identisch sein.</p>';
      checkUnique = 1;
   }
   if(html) {
      $('#theMessage').html(html);
      $('#theMessage').css('display','inline-block');
   } else {
      $('#theMessage').css('display','none');
   }

   if(checkUnique == 1) {
      console.log(checkUnique);
      $('#checkUnique').removeClass('fa-check').addClass('fa-ban').removeClass('text-success').addClass('text-danger');
   } else {
      console.log("HALLO");
      $('#checkUnique').removeClass('fa-ban').addClass('fa-check').removeClass('text-danger').addClass('text-success');
   }
}
$('#iShowP1').on('mousedown', function() {
   var sp1 = $('#p1').val();
   if(sp1) {
      $('#showP1').html(sp1).css('display', 'inline');
   }
});
$('#iShowP1').on('mouseup', function() {
   $('#showP1').css('display','none');
});

$('#iShowP2').on('mousedown', function() {
   var sp2 = $('#p2').val();
   if(sp2) {
      $('#showP2').html(sp2).css('display', 'inline');
   }
});
$('#iShowP2').on('mouseup', function() {
   $('#showP2').css('display','none');
});
function checkMail() {
    var email = $('#email').val();
    $.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=forgot.checkMail",
        data: {
           email:email
        },
        //dataType: 'json',
        success: function( data ) {
           console.log(data);
        }
     });
}
</script>