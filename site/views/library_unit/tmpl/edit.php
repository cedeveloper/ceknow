<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/imgForJCE.js');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/editContent.php');
require_once(JPATH_COMPONENT.'/views/dialogs/imageContent.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

   <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a href="library-units" class="btn btn-success text-white">Speichern</a>
      <?php endif;?>
        <a href="library-units" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('description'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('description'); ?>
         </div>
      </div>
   </div>
   <div id="structure">
      <?php echo $this->unit;?>
   </div>
<input type="hidden" id="id" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<form action="index.php?option=com_jclassroom&task=classroom.loadFile" method="post" name="uploaderFormUnit" id="uploaderFormUnit" enctype="multipart/form-data">
   <input id="uploaderUnit" title="file input" type="file" accept="" name="uploadUnit" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="formUnitID" name="unitID" value="" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom.loadIntroImage" method="post" id="uploaderImageUnit" enctype="multipart/form-data">
   <input id="uploadImageUnit" title="file input" type="file" accept="" name="uploadImageUnit" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="imageUnitID" name="imageUnitID" value="" />
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<div id="wait">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<style>
   #editContent .modal-dialog {
      max-width: 80%!important;
   }
   #editContent #editor {
      height: 450px!important;
      overflow-y: scroll;
   }
</style>
<script type="text/javascript">
let unitID = $('#id').val();
let classroomID = false;
let part = 'library-module';
   $(document).ready(function() {
      $('.move').remove();
      $('.trash').remove();
      $('.save').remove();
      $('.saveLibrary').remove();
      $('.fa-arrows-alt').remove();
   });
   function openUnit(id) {
      if(jQuery('#unitContent' + id).hasClass('openUnit')) {
         jQuery('#unitContent' + id).removeClass('openUnit');
         jQuery('#unit' + id + ' .sectionHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      } else {
         jQuery('#unitContent' + id).addClass('openUnit');
         jQuery('#unit' + id + ' .sectionHeader .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      }
   }
   jQuery(document).on('change','#jform_description', function() {
      var unitID = $('#id').val();
      saveUnit(unitID);
   });
   jQuery(document).on('change','.title', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   jQuery(document).on('change','.duration', function() {
      var dataValue = $(this).val();
      if(isNaN(dataValue)) {
         alert('Bitte geben Sie eine Zahl ein.');
         $(this).val(0);
         return false;
      }
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   jQuery(document).on('change','.link', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   jQuery(document).on('change','.quizz', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   jQuery(document).on('summernote.change','.summernote', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
   });
   function saveUnit(unitID) {
      jQuery('#save').fadeIn(200);
      var title      = jQuery('#unit' + unitID).find('.title').val();
      var description= jQuery('#jform_description').val();
      var duration   = jQuery('#unit' + unitID).find('.duration').val();
      var content    = jQuery('#unit' + unitID).find('.contentHTML').html();
      var link       = jQuery('#unit' + unitID).find('.link').val();
      var quizz      = jQuery('#unit' + unitID).find('.quizz option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=library_unit.saveUnit",
         data: {unitID:unitID,title:title,duration:duration,content:content,link:link,quizz:quizz,description:description},
         //dataType: 'json',
         success: function( data ) {
            hideSave();
         }
      });
      function hideSave() {
         setTimeout(hideSaveExecute,800);
      }
      function hideSaveExecute() {
         jQuery('#save').slideUp(200);
      }
   }
   function editContent(unitID) {
      var content = jQuery('#unit' + unitID).find(' .contentHTML').html();
      tinyMCE.get('jform_content').setContent(content);
      jQuery('#editContentUnitID').val(unitID);
      jQuery('#editContent').modal();
   }
   function editContentModule() {
      //jQuery('#save').fadeIn(200);
      var content = tinyMCE.get('jform_content').getContent();
      var unitID  = jQuery('#editContentUnitID').val();
      jQuery('#unit' + unitID).find(' .contentHTML').html(content);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      saveUnit(unitID);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
      jQuery('#editContent').modal('hide');
      hideSave();
       //jQuery('#save').fadeOut(200);
   }
   function closeEditContentModule() {
      jQuery('#editContent').modal('hide');
   }
   function uploadUnit(unitID) {
      jQuery('#formUnitID').val(unitID);
      jQuery('#uploaderUnit').trigger('click');
   }
   function introImageUnit(unitID) {
      jQuery('#imageUnitID').val(unitID);
      jQuery('#uploadImageUnit').trigger('click');
   }
   jQuery('#uploaderUnit').on('change', function(e) {
      var classroomID   = jQuery('#classroomID').val();
      var unitID        = jQuery('#formUnitID').val();
      var theFiles      = jQuery(this)[0].files;
      var theFileName   = jQuery('#uploaderUnit')[0].files[0].name;
      var form_data     = new FormData();
      for(i = 0; i <= theFiles.length; i++) {
         
      }
      form_data.append('uploadUnit', jQuery('#uploaderUnit')[0].files[0]);
      form_data.append('classroomID', classroomID);
      form_data.append('unitID', unitID);
      $('#save').modal('show');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.loadFile",
         data: form_data,
         processData: false,
         contentType: false,
         //dataType: 'json',
         success: function( data ) {
            var dataW = data.split('__');
            jQuery('#uploads' + unitID).append('<div id="file' + dataW[0] + '" class="row mb-1">' + 
            '<div class="col-12">' + 
               '<div class=" fileRow">' + 
                  '<div class="row">' +
                     '<div class="col-12 col-sm-10">' + 
                        '<a target="_blank" href="' + dataW[1] + '">' + theFileName + '</a>' + 
                     '</div>' +
                     '<div class="text-right col-12 col-sm-2">' + 
                        '<i class="fa fa-trash-o bg-danger text-white p-1" style="cursor: pointer;border-radius: 3px;" onclick="deleteFile(' + dataW[0] + ');"></i>' +
                     '</div>' +
                  '</div>' +
               '</div>' +
            '</div>'
            );
             $('#save').modal('hide');
         }
      });
   });
   jQuery('#uploadImageUnit').on('change', function(e) {
      var classroomID   = jQuery('#classroomID').val();
      var unitID        = jQuery('#imageUnitID').val();
      var theFiles      = jQuery(this)[0].files;
      var theFileName   = jQuery('#uploadImageUnit')[0].files[0].name;
      var form_data     = new FormData();
      for(i = 0; i <= theFiles.length; i++) {
         
      }
      form_data.append('uploadImageUnit', jQuery('#uploadImageUnit')[0].files[0]);
      form_data.append('classroomID', classroomID);
      form_data.append('unitID', unitID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadImageFile",
         data: form_data,
         processData: false,
         contentType: false,
         //dataType: 'json',
         success: function( data ) {
            var dataW = data.split('__');
            jQuery('#introImage' + unitID).append('<div id="file' + dataW[0] + '" class="row mb-1">' + 
            '<div class="col-12">' + 
               '<div class=" fileRow">' + 
                  '<div class="row">' +
                     '<div class="col-12 col-sm-10">' + 
                        '<a target="_blank" href="' + dataW[1] + '">' + 
                        '<div class="introImageThumb" style="background-image: URL(' + dataW[1] + ');"></div>' +
                        theFileName + '</a>' + 
                     '</div>' +
                     '<div class="text-right col-12 col-sm-2">' + 
                        '<i class="fa fa-trash-o bg-danger text-white p-1" style="cursor: pointer;border-radius: 3px;" onclick="deleteImageFile(' + dataW[0] + ');"></i>' +
                     '</div>' +
                  '</div>' +
               '</div>' +
            '</div>'
            );
            console.log(data);
         }
      });
   });
   function deleteFile(fileID) {
      if(confirm("Soll diese Datei gelöscht werden?") == true) {
         $('#save').modal('show');
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteFile",
            data: {fileID:fileID},
            //dataType: 'json',
            success: function( data ) {
               jQuery('#file' + fileID).remove();
                $('#save').modal('hide');
            }
         });
      }
   }
</script>