<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
 
/**
 * Kunde item view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewLibrary_unit extends JViewLegacy
{
	protected $item;
	protected $form;
	protected $state;
	
	public function display($tpl = null)
	{
		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
        $template 	= new UnitsHelper();
        $this->unit = $template->getTemplate('lib', 0, 0, $this->item->unitType, $this->item->id); 

		parent::display($tpl);
	}
}
?>