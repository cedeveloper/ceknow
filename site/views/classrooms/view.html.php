<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewClassrooms extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;
	protected $toolbar;

    public function display($tpl = null) {
        $session = JFactory::getSession();

        require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        // Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
            JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
            $demo = new LoginHelper();
            $demo->checkDemo();
        endif;
        $document   = JFactory::getDocument();
        $document->setTitle('Learningrooms ['.$session->get('systemname').']');
		$this->items 		 = $this->get('Items');
		$this->state 		 = $this->get('State');
		$this->pagination 	 = $this->get('Pagination');
		$this->user		 	 = JFactory::getUser();
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$user = JFactory::getUser();
        $groups = JAccess::getGroupsByUser($user->id);
        if(in_array(8,$groups)) {
            $this->return = 'dashboard';
        }
        if(in_array(11,$groups)) {
            $this->return = 'celearning/classrooms';
        }
        if(in_array(12,$groups)) {
            $this->return = 'manager-customer';
        }
        if(in_array(13,$groups)) {
            $this->return = 'manager-customer';
        }
        if(in_array(10,$groups)) {
            $this->return = 'manager-trainer';
        }
        $this->usergroup = $session->get('group');

        parent::display($tpl);
    }
}
