<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
?>
<style>
 i.fa {
   cursor: pointer;
 }
</style>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <div id="learningroom-info" class="col-12">
      <?php echo $this->item->testmode_message;?>
      <h2><b><?php echo 'LR'.str_pad($this->item->id,8,'0',STR_PAD_LEFT);?></b><h2>
      <h4><?php echo $this->visible.' '.$this->item->title;?></h4>
      <h3>Materialien</h3>
   </div>
   <div class="buttonleiste mb-4">         
      <a id="retour" href="classroom-edit?layout=global&id=<?php echo $this->item->id;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
      <div class="row mt-2">
         <div class="col-12 col-sm-4">
            <h5>Verzeichnisstruktur für LR<?php echo str_pad($this->item->id,8,'0', STR_PAD_LEFT);?></h5>
            <div class="alert alert-danger p-1">Achtung! Sie haben hier uneingeschränkten Zugriff auf die für diesen Learningroom auf dem Server gespeicherten Dateien. Gelöschte Dateien sind unwiderruflich verloren.</div>
            <i class="fa fa-question-circle"></i> <br/><small style="display: inline-block; width: 100%;line-height: 14px;">Sie können nur Dateien aus den Ordnern löschen. Die Ordner selbst sind technisch notwendig und können nicht gelöscht werden.</small>
            <div id="folderTree" class="card bg-light p-1" style="font-size: 12px;">
               <?php
               if($this->filesOP):
                  foreach($this->filesOP as $file):
                     echo '<p class="mt-1 mb-1 p-1" style="background-color: #a1a1a1;"><i class="fa fa-folder-o"></i> '.$file['name'].'</p>';
                     if($file['subfiles']):
                        foreach($file['subfiles'] as $subfile):
                           $theName = str_replace('.', '', $subfile['name']);
                           echo '<p id="file'.$theName.'" class="mt-1 mb-1 ml-5 p-1" style="background-color: #d1d1d1;"><i class="fa fa-file-o"></i> '.$subfile['name'].'<i class="pt-1 fa fa-trash-o float-right" title="Diese Datei löschen" onclick="deleteFileFD(&quot;'.$subfile['path'].'&quot;,&quot;'.$theName.'&quot;,&quot;&quot;);"></i></p>';
                        endforeach;
                     endif;
                  endforeach;
               endif;
               ?>
            </div>
            <div class="card bg-light p-1 mt-3">
               <div class="card-header">
                  <h5 class="m-0">Speichergröße</h5>
               </div>
               <div class="card-body>">
                  <div class="row">
                     <div class="col-12 col-md-8">Maximal</div>
                     <div class="col-12 col-md-4 text-right">40,00 MB</div>
                  </div>
                  <div class="row">
                     <?php
                     if($this->item->currentSize):
                        $result = $this->item->currentSize / 1000000;
                     endif;
                     ?>
                     <div class="col-12 col-md-8">Aktuell</div>
                     <div class="col-12 col-md-4 text-right"><?php echo number_format($result,2,',','.');?> MB</div>
                  </div>
                  <div class="row">
                     <div class="col-12">
                        <div class="progress">
                           <?php
                           $max  = 40000000;
                           $cu   = $this->item->currentSize;
                           $percent = (100 * $cu) / $max;
                           ?>
                           <div class="progress-bar" role="progressbar" style="width: <?php echo $percent;?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?php echo $percent;?>%</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12 col-sm-8">
            <h5>Uploadfunktionen für LR<?php echo str_pad($this->item->id,8,'0', STR_PAD_LEFT);?></h5>
            <div class="alert alert-success p-1">Laden Sie über die nachfolgenden Möglichkeiten Daten für diesen Learningroom auf den Server.</div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('material'); ?>
               </div>
               <div class="col-12 col-sm-10">
                   <button type="button" class="btn btn-info text-white d-inline mb-1"id="openUploader">Material erfassen</button>
                  <?php
                  if($this->item->files):
                     foreach($this->item->files as $file):
                        if($file->type == 'material'):
                           echo '<div id="file'.$file->id.'" class="row">';
                              echo '<div class="col-12">';
                                 echo '<div class="bg-light p-1 mb-1 border-dark">';
                                    echo '<a href="'.$file->path.'" target="_blank">'.$file->filename.'</a>';
                                    echo '<a class="bg-danger text-white pl-1 pr-1 float-right" onClick="deleteFile('.$file->id.');"><i class="fa fa-trash-o"></i></a>';
                                 echo '</div>';
                              echo '</div>';
                           echo '</div>';
                        endif;
                     endforeach;
                  endif;
                  ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <label>Reserve</label>
               </div>
               <div class="col-12 col-sm-10">
                 <button type="button" class="btn btn-info text-white d-inline mb-1"id="openUploaderR">Reservematerial erfassen</button>
                  <?php
                  if($this->item->files):
                    $iC = 1;
                     foreach($this->item->files as $file):
                        if($file->type == 'reserve'):
                           echo '<div class="row">';
                              echo '<div id="file'.$file->id.'" class="col-12">';
                                 echo '<div class="bg-light p-1 mb-1 border-dark">';
                                    echo '<div class="row">';
                                       echo '<div class="col-8">';
                                          echo $iC.'. ';
                                          echo '<a href="'.$file->path.'" target="_blank">'.$file->filename.'</a>';
                                          echo '</div>';
                                          echo '<div class="col-2">';
                                             echo $file->folder;
                                          echo '</div>';
                                          echo '<div class="col-2">';
                                          if($file->published == 0):
                                             $hideStage     = 'd-inline-block';
                                             $hideUnstage   = 'd-none';
                                          else:
                                             $hideStage     = 'd-none';
                                             $hideUnstage   = 'd-inline-block';
                                          endif;
                                          echo '<a id="stageFile_'.$file->id.'" onclick="stageFile('.$file->id.');" class="btn btn-success '.$hideStage.' text-white btn-sm">Freigeben</a>';
                                          echo '<a id="unstageFile_'.$file->id.'" onclick="unstageFile('.$file->id.');" class="btn btn-danger '.$hideUnstage.' text-white btn-sm">Verstecken</a>';
                                          echo '<a class="deleteButton bg-danger text-white pl-1 pr-1 float-right" onClick="deleteFile('.$file->id.');"><i class="fa fa-trash-o"></i></a>';
                                       echo '</div>';
                                    echo '</div>';
                                 echo '</div>';
                              echo '</div>';
                           echo '</div>';
                           $iC++;
                        endif;
                     endforeach;
                  endif;
                  ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Dateien für Teilnehmer
               </div>
               <div class="col-12 col-sm-10">
                  <table id="students" class="table table-striped table-sm">
                     <thead>
                        <tr>
                           <th>Name</th>
                           <th>Firma</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($this->item->students):
                           $fileID = 0;
                           foreach($this->item->students as $student):
                              echo '<tr id="student'.$student['id'].'">';
                              echo '<td><a href="student-edit?layout=edit&id='.$student['id'].'" target="_blank">'.$student['name']. '</a></td>';
                              echo '<td>'.$student['company'].'</td>';
                              echo '<td>';
                              echo '<button type="button" data-id="'.$student['id'].'" class="openStudentUploader btn btn-info text-white d-inline mb-1"id="openUploader">Material erfassen</button>';
                              if($this->item->files):
                                 foreach($this->item->files as $file):
                                    if($file->type == 'student' && ($file->studentID == $student['id'])):
                                       echo '<br/><div id="file'.$file->id.'"><a href="'.$file->path.'">'.$file->filename.'</a>';
                                       echo '<a class="bg-danger text-white pl-1 pr-1 float-right" onClick="deleteFileFD(&quot;'.$file->path.'&quot;,&quot;'.$file->filename.'&quot;,'.$file->id.');"><i class="fa fa-trash-o"></i></a></div>';
                                    endif;   
                                 endforeach;
                              endif;
                              echo '</td>';
                              echo '</tr>';
                              $fileID++;
                           endforeach;
                        else:
                              echo '<tr>';
                              echo '<td colspan="3">Keine Teilnehmer gefunden.</td>';
                              echo '</tr>';
                        endif;
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
<input type="hidden" id="classroomID" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<form action="index.php?option=com_jclassroom&task=classroom2.loadFileM" method="post" name="uploaderFormMaterial" id="uploaderFormMaterial" enctype="multipart/form-data">
   <input id="inputMaterial" title="file input" type="file" accept="" multiple name="inputMaterial[]" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom2.loadFileR" method="post" name="uploaderFormReserve" id="uploaderFormReserve" enctype="multipart/form-data">
   <input id="inputReserve" title="file input" type="file" accept="" multiple="" name="inputReserve[]" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom2.loadFileS" method="post" name="uploaderFormStudent" id="uploaderFormStudent" enctype="multipart/form-data">
   <input id="inputStudent" title="file input" type="file" accept="" multiple="" name="inputStudent[]" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="uploadStudentUserID" name="uploadStudentUserID" value="<?php echo $this->item->id;?>" />
</form>
<script>
$('#openUploader').click(function() {
   $('#inputMaterial').trigger('click');
   
});
$('#inputMaterial').on('change', function() {
   $('#save').css('display','block');
   $('#uploaderFormMaterial').submit();
});
$('#openUploaderR').click(function() {
   $('#inputReserve').trigger('click');
});
$('#inputReserve').on('change', function() {
   $('#save').css('display','block');
   $('#uploaderFormReserve').submit();
});
$('.openStudentUploader').click(function() {
   var studentID = $(this).attr('data-id');
   $('#uploadStudentUserID').val(studentID);
   $('#inputStudent').trigger('click');
});
$('#inputStudent').on('change', function() {
   $('#save').css('display','block');
   $('#uploaderFormStudent').submit();
});
function deleteFolderFD(id) {

}
function deleteFile(fileID) {
   if(confirm("Soll diese Datei gelöscht werden?") == true) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.deleteFile",
         data: {fileID:fileID},
         //dataType: 'json',
         success: function( data ) {
            $('#file' + data).remove();
            $('#file' + fileID).remove();
         }
      });
   }
}
function deleteFileFD(path, name, id) {
   if(confirm('Möchten Sie die Datei ' + name + ' endgültig löschen?') == true) {
      jQuery.ajax({
          url: "index.php?option=com_jclassroom&task=classroom2.deleteFileFD",
          data: {path:path,id:id},
          method: 'POST',
          //dataType: 'json',
          success: function( data ) {
               $('#file' + name).remove();
               if(id) {
                  $('#file' + id).remove();
               }
          }
      });
   }
}
function stageFile(id) {
   if(confirm('Soll diese Datei für die Teilnehmer freigegeben werden?') == true) {
      var classroomID = jQuery('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.stageFile",
         data: {id:id, classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#stageFile_' + id).removeClass('d-inline-block').addClass('d-none');
            $('#unstageFile_' + id).removeClass('d-none').addClass('d-inline-block');
         }
      });
   }
}
function unstageFile(id) {
   if(confirm('Soll diese Datei für die Teilnehmer versteckt werden?') == true) {
      var classroomID = jQuery('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.unstageFile",
         data: {id:id, classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#stageFile_' + id).removeClass('d-none').addClass('d-inline-block');
            $('#unstageFile_' + id).removeClass('d-inline-block').addClass('d-none');
         }
      });
   }
}
</script>