<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
$user = JFactory::getUser();
?>
<style>
    table tr {
        position: relative;
    }
    .hideme {
        position: absolute;
        background-color: rgba(255,255,255,0.95);
        height: calc(100% - 10px);
        top: 5px;
        left: 5px;
        display: none;
        color: red;
        font-size: 16px;
        font-weight: bold;
        font-style: italic;
        padding: 10px;
    }

</style>
<form action="index.php?option=com_jclassroom&task=classrooms.loadJSON" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept="application/json" name="uploadJSON" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <a class="btn btn-success text-white" onclick="Joomla.submitform('classroom2.add')">Neuer Learningroom</a>
        <a class="btn btn-danger text-white m-1" id="deleteLearningrooms" id="deleteClassroom">Learningroom löschen</a>
        <button type="button" class="btn btn-warning text-dark"id="openUploader">Learningroom Upload</button>
        <a href="<?php echo $this->return;?>" class="btn btn-danger text-white m-1 float-right">Zurück</a>
    </div>
    <?php echo $this->pagination->getPagesLinks(); ?>
    <table class="table table-striped">	
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                 <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Name', 'a.title', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Haupttrainer<br/>Co-Trainer', 'a.created_by', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Erstellt von/am<br/>Bearbeitet von/am', 'a.created', $listDirn, $listOrder) ?>
                </th>
                <th width="300"></th> 
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', 'Status', 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>		
        <tbody>
        <?php if($this->items) { ?>
        <?php foreach ($this->items as $i => $item) :?>
            <?php 
                $hideTheContent = '';
                $hideEdit = '';
                if($this->usergroup == 'customer' || $this->usergroup == 'customeradmin'):
                    // show individual
                    if($item->showto == 0):
                        $hideTheContent = 'hide';
                        if($item->created_by == $user->id):
                            $hideTheContent = '';
                        endif;
                        foreach($item->rights as $right):
                            if(($right->classroomID == $item->id) && ($right->userID == $user->id)):
                                if($right->rightType == 1):
                                    $hideTheContent = '';
                                endif;
                                if($right->rightType == 2):
                                    $hideTheContent = '';
                                endif;
                            endif;
                        endforeach;
                    endif;
                    // show just to creator
                    if($item->showto == 1):
                        if($item->created_by == $user->id):
                            // if current user is creator, show item
                            $hideTheContent = '';
                        endif;
                    endif;
                    if($item->showto == 1):
                        if($item->created_by != $user->id):
                            // if current user is not creator, show item if creator is a trainer of customer
                            $groups = JAccess::getGroupsByUser($item->created_by);
                            if(in_array(12,$groups) || in_array(13,$groups)):
                                // if creator is another admin of customer and 
                                $hideTheContent = 'hide';
                            else:
                                $hideTheContent = '';
                            endif;
                        endif;
                    endif;
                    if($item->showto == 3):
                        //if($item->created_by == $user->id):
                            // if current user is creator, show item
                            $hideTheContent = '';
                        //endif;
                    endif;
                endif;
                if($this->usergroup == 'trainer'):
                    if($item->showto == 0):
                        $hideTheContent = 'hide';
                        if($item->created_by == $user->id):
                            $hideTheContent = '';
                        else:
                            if($item->rights):
                                foreach($item->rights as $right):
                                    if($right->rightType == 1 && $right->userID == $user->id):
                                        $hideTheContent = '';
                                    endif;
                                endforeach;
                            endif; 
                        endif;
                    endif;
                    if($item->showto == 1):
                        if($item->created_by == $user->id):
                            // if current user is creator, show item
                            $hideTheContent = '';
                        else:
                            $hideTheContent = 'hide';
                        endif;
                    endif;
                    if($item->editto == 0):
                        $hideEdit = 'disabled';
                        if($item->created_by == $user->id):
                            $hideEdit = '';
                        else:
                            if($item->rights):
                                foreach($item->rights as $right):
                                    if($right->rightType == 2 && $right->userID == $user->id):
                                        $hideEdit = '';
                                    endif;
                                endforeach;
                            endif; 
                        endif;
                    endif;
                    if($item->editto == 1):
                        if($item->created_by == $user->id):
                            $hideEdit = '';
                        else:
                            $hideEdit = 'disabled';
                        endif;
                    endif;
                    if($item->editto == 3):
                        if($item->created_by == $user->id):
                            $hideEdit = '';
                        endif;
                    endif;
                endif;
            ?>
            <tr class="row<?php echo $i % 2; ?> <?php echo $hideTheContent;?>">
            	<td class="center">
                    <?php
                        echo JHtml::_('grid.id', $i, $item->id);
                    ?>
                </td>
				<td class="right"><?php echo $this->escape($item->id); ?></td>
                <td>
                    <a class="link <?php echo $hideEdit;?>" href="<?php echo JURI::Root().'classroom-edit?layout=global&id='. $item->id.$noS; ?>">
                        <?php echo '<b>LR'.str_pad($item->id, 8,'0', STR_PAD_LEFT);?></b><br/>
                        <?php echo $item->title;?>
                        <?php
                        if($item->companyName):
                            echo '<br/><span class="badge bg-dark text-white" style="font-size:12px;">Für Firma: '.$item->companyName.'</span>';
                        endif;
                        ?>
                    </a>
                    <?php
                    ?>
                </td>
                <td class="left">
                    <?php echo $item->mainTrainer; ?><br/>
                    <?php echo $item->coTrainer; ?>  
                </td>
                <td class="left">
                    <?php 
                        $groups = JAccess::getGroupsByUser($item->created_by);
                        if(in_array(8,$groups)) {
                            $groupIcon = '<i class="fa fa-building-o"></i>';
                        }
                        if(in_array(11,$groups)) {
                            $this->return = 'celearning/classrooms';
                        }
                        if(in_array(12,$groups)) {
                            $groupIcon = '<i class="fa fa-building"></i>';
                        }
                        if(in_array(13,$groups)) {
                            $groupIcon = '<i class="fa fa-building"></i>';
                        }
                        if(in_array(10,$groups)) {
                            $groupIcon = '<i class="fa fa-child"></i>';
                        }
                        $groupsM = JAccess::getGroupsByUser($item->modified_by);
                        if(in_array(8,$groupsM)) {
                            $groupIconM = '<i class="fa fa-building-o"></i>';
                        }
                        if(in_array(11,$groupsM)) {
                            $this->return = 'celearning/classrooms';
                        }
                        if(in_array(12,$groupsM)) {
                            $groupIconM = '<i class="fa fa-building"></i>';
                        }
                        if(in_array(13,$groupsM)) {
                            $groupIconM = '<i class="fa fa-building"></i>';
                        }
                        if(in_array(10,$groupsM)) {
                            $groupIconM = '<i class="fa fa-child"></i>';
                        }
                        echo '<b>('.$groupIcon.' '.$item->creator.')</b> ' .$this->escape($item->created);
                        if($item->modifier):
                            echo '<br>';
                            echo '<b>('.$groupIconM.' '.$item->modifier.')</b> ' .$this->escape($item->modified);
                        endif;
                    ?><br/>      
                </td>
                <td class="text-center">
                    <a href="<?php echo JURI::Root().'classroom-edit?layout=global&id='. $item->id; ?>" class="btn btn-success btn-sm w-100 mb-1 <?php echo $hideEdit;?>"><i class="fa fa-gears"></i> Learningroom Konfigurieren</a>
                    <a onclick="copyLearningroom(<?php echo $item->id;?>);" class="btn btn-danger btn-sm w-100 mb-1 text-white <?php echo $hideEdit;?>"><i class="fa fa-copy"></i> Learningroom kopieren</a>
                    <a onclick="exportLearningroom(<?php echo $item->id;?>);" class="btn btn-warning btn-sm w-100 mb-1 <?php echo $hideEdit;?>"><i class="fa fa-download"></i> Learningroom exportieren</a>
                    <a href="stage-classroom?id=<?php echo $item->id;?>" target="_blank" class="btn btn-primary btn-sm w-100"><i class="fa fa-eye"></i> Vorschau</a>
                </td>
                <?php
                if($item->published == 0) {
					$class="badge-danger";
					$wert = "<i class='fa fa-ban'></i>";
				} elseif($item->published == 1) {
					$class="badge-success";
					$wert = "<i class='fa fa-check'></i>";
				}
				?>
                <td class="text-center">
                	<span class="btn <?php echo $class;?> btn-sm"><?php echo $wert;?></span>
               	</td>
            </tr>
        <?php endforeach ?>
        <?php } else { ?>
        	<tr>
            	<td colspan="9">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
       	<?php } ?>
        </tbody>			
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var trW = $('table tr').width();
        trW = trW - 10;
        $('.hideme').each(function() {
            $(this).css('width', trW + 'px');
        });
        $('tr.hide').each(function() {
            var id = $(this).attr('data-id');
            $(this).empty();
            $(this).append('<td colspan="7" class="text-danger" style="font-weight: bold;">Nur für Urheber sichtbar (Kein Leserecht)</td>');
        });
        $('a.link.disabled').each(function() {
            $(this).removeAttr('href', '');
        });
    });
    jQuery('#openUploader').click(function() {
        jQuery('#uploader').trigger('click');
        $('#save').fadeIn(200);
        
    });
    jQuery('#uploader').on('change', function() {
        jQuery('#uploaderForm').submit();
    });
    jQuery('#deleteLearningrooms').on('click', function() {
        if(confirm('Sollen die gewählten Learningrooms gelöscht werden?') == true) {
            Joomla.submitform('classrooms.delete');
        }
    });
    function copyLearningroom(id) {
      if(confirm("Soll dieser Learningroom kopiert werden?") == true) {
        $('#save').fadeIn(200);
        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classrooms.copyLearningroom",
            data: {id:id},
            //dataType: 'json',
            success: function( data ) {
               $('#save').fadeOut(200);
               location.reload();
            }
         });
      }
    }
    function exportLearningroom(id) {
        if(confirm("Soll der Learningroom mit der ID " + id + " als JSON-Datei exportiert werden?") == true) {
            console.log(id);
            jQuery.ajax({
                url: "index.php?option=com_jclassroom&task=classrooms.export",
                data: {id:id},
                method: 'GET',
                xhrFields: {
                    responseType: 'text'
                },
                //dataType: 'json',
                success: function( data ) {
                    var element = document.createElement('a');
                    //element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                    element.setAttribute('href', data);
                    element.setAttribute('download', 'learningroom' + id + '.zip');
                    element.style.display = 'none';
                    document.body.appendChild(element);

                    element.click();

                    document.body.removeChild(element);
                }
            });
        }
    }
</script>