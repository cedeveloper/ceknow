<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$dokument = JFactory::getDocument();
$dokument->addScript('components/com_jclassroom/assets/js/jquery.validate.js' );
$dokument->addScript('components/com_jclassroom/assets/js/stage.js');

$now = new DateTime();
$session = JFactory::getSession();
?>
<style>
    input[type="checkbox"] {
        height: 30px;
        width: 30px;
    }
    .smileys {
        display: flex;
        justify-content: space-between;
    }
    .smiley {
        width: 120px;
        height: 120px;
        margin: 0 25px;
        cursor: pointer;
        transition: all 0.4s ease-in-out;
    }
    .smiley:hover {
        transform: rotate(45deg);
    }

</style>
<form action="<?php JRoute::_('index.php?option=com_jclassroom&task=unit.checkQuestion'); ?>" method="post" name="adminForm" id="adminForm">
	<div class="stage row">
        <div class="col-12">
            <p class=" text-info mb-2"><?php echo $now->format('d.m.Y H:i');?></p>
        	<h1><i><?php echo $this->unit['title']?></i></h1>
            <?php echo $this->unit['description'];?>
      	</div>
       	<div class="col-12">
       		<div class="row">
                <div class="col-12 "> 
                    <div class="card">
                        <div class="card-body">
                            <h2>Frage <?php echo $this->q + 1;?> von <?php echo $this->unit['countSections'];?></h2>
                            <?php
                            echo '<h1>'.$this->unit['question'].'</h1>';
                            if($this->unit['correctAnswers'] > 1):
                                echo '<p>Mehrere Antworten sind möglich.</p>';
                            endif;
                            echo $this->unit['content'];
                            if($this->unit['answers']):
                                foreach($this->unit['answers'] as $answer):
                                    if($this->unit['correctAnswers'] == 2):
                                        echo '<h2><a id="answer'.$answer->id.'" onclick="chooseAnswer('.$answer->id.');" class="btn w-100" style="text-align: left;background-color: lightsteelblue;"><b>'.$answer->title.'</b></a></h2>';
                                    else:  
                                        echo '<h2><a id="answer'.$answer->id.'" onclick="nextQuestion('.$answer->id.');" class="btn w-100" style="text-align: left;background-color: lightsteelblue;"><b>'.$answer->title.'</b></a></h2>';
                                    endif;
                                endforeach;
                            endif;
                            if($this->unit['type'] == 4):
                                echo '<div class="smileys">';
                                echo '<div id="smiley1" class="smiley" onclick="nextQuestion(1);">
                                    <img src="images/jclassroom/smiley1.png" />
                                    </div>';
                                echo '<div id="smiley2" class="smiley" onclick="nextQuestion(2);">
                                    <img src="images/jclassroom/smiley2.png" />
                                    </div>';
                                echo '<div id="smiley3" class="smiley" onclick="nextQuestion(3);">
                                    <img src="images/jclassroom/smiley3.png" />
                                    </div>';
                                echo '<div id="smiley4" class="smiley"onclick="nextQuestion(4);">
                                    <img src="images/jclassroom/smiley4.png" />
                                    </div>';
                                echo '<div id="smiley5" class="smiley" onclick="nextQuestion(5);">
                                    <img src="images/jclassroom/smiley5.png" />
                                    </div>'; 
                                echo '</div>';
                            endif;
                            if($this->unit['type'] == 10):
                                echo '<input type="text" id="textFeld" value="" placeholder="Bitte geben Sie hier Ihre Antwort ein."/>';
                            endif;
                            if($this->unit['type'] == 11):
                                echo '<textarea style="width: 100%; min-height: 100px;" id="textArea"></textarea>';
                            endif;
                            if($this->unit['type'] == 12):
                                echo '<select class="select2" id="auswahlliste">';
                                echo '<option value="" selected disabled>Bitte auswählen</option>';
                                if($this->unit['options']):
                                    foreach($this->unit['options'] as $option):
                                        echo '<option value="'.$option->id.'">'.$option->title.'</option>';
                                    endforeach;
                                endif;
                                echo '</select>';
                            endif;
                            if($this->unit['type'] == 13):
                                if($this->unit['checkboxes']):
                                    foreach($this->unit['checkboxes'] as $checkbox):
                                        echo '<div class="checkbox_container d-flex" style="width: 400px;">';
                                        echo '<span class="d-inline-block" style="width: 300px;">'.$checkbox->title.'</span>';
                                        echo '<input class="aCheckbox ml-2" name="theCheckboxes[]" type="checkbox" data-answerID="'.$checkbox->id.'" value="'.$checkbox->title.'" />';
                                        echo '</div>';
                                    endforeach;
                                endif;
                            endif;
                            ?>   
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer mt-3">
                <?php
                if($this->q == 0):
                    echo '<a onclick="quitQuizz();" class="float-left btn btn-success btn-lg text-white">Quizz beenden</a>';
                endif;
                if($this->q == 1):
                    echo '<a onclick="breakQuizz();" class="float-left btn btn-success btn-lg text-white">Quizz unterbrechen</a>';
                endif;
                //if(($this->q + 1) != $this->unit['countSections']):
                    if(($this->unit['type'] == 3 && $this->unit['correctAnswers'] == 2) || $this->unit['type'] == 12 || $this->unit['type'] == 10 || $this->unit['type'] == 11 || $this->unit['type'] == 1 || $this->unit['type'] == 13): 
                        echo '<a onclick="nextQuestion();" class="float-right btn btn-success btn-lg text-white">Weiter</a>';
                    endif;
                //endif;
                ?>
            </div>
        </div>
    </div>
<input type="hidden" id="clr" value="<?php echo $this->clr;?>" />
<input type="hidden" id="q" value="<?php echo $this->q;?>" />
<input type="hidden" id="type" value="<?php echo $this->unit['type'];?>" />
<input type="hidden" id="quizzID" value="<?php echo $this->unit['quizzID'];?>" />
<input type="hidden" id="questionID" value="<?php echo $this->unit['questionID'];?>" />
<input type="hidden" id="classroomID" value="<?php echo $this->item->id;?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
$('input[type="checkbox"]').on('click', function() {
    $('input[type="checkbox"]').each(function() {
        $(this).prop('checked', false);
    });
    $(this).prop('checked', true);
})
function nextQuestion(choosenAnswer) {
    var areThereChoosen = $('.choosen').length;
    if(areThereChoosen > 0) {
        //jQuery('.card-body').each('a', function() {
            //jQuery(this).attr('disabled','disabled');
        //});
    }
    var quizzID     = $('#quizzID').val();
    var questionID  = $('#questionID').val();
    var q           = $('#q').val();
    var clr         = $('#clr').val();
    var answers     = [];
    var indAnswers  = [];
    var options     = [];
    var checkboxes  = [];
    var smiley      = [];
    var type        = $('#type').val();
    if(type == 4) {
        var text = '';
        if(choosenAnswer == 1) {
            text = 'Sehr schlecht';
        }
        if(choosenAnswer == 2) {
            text = 'Schlecht';
        }
        if(choosenAnswer == 3) {
            text = 'Nicht schlecht';
        }
        if(choosenAnswer == 4) {
            text = 'Gut';
        }
        if(choosenAnswer == 5) {
            text = 'Sehr gut';
        }
        smiley.push(choosenAnswer + '_' + text);
    }
    if(type == 10) {
        indAnswers.push('0_' + $('#textFeld').val());
    }
    if(type == 11) {
        indAnswers.push('0_' + $('#textArea').val());
    }
    if(type == 12) {
        options.push($('#auswahlliste option:selected').val() + '_' + $('#auswahlliste option:selected').text());
    }
    if(type == 13) {
        $('.aCheckbox').each(function() {
            if($(this).prop('checked') == true) {
                checkboxes.push($(this).attr('data-answerID') + '_' + $(this).val());
            }
        });
    }
    if(choosenAnswer) {
        var answer  = getAnswer(choosenAnswer);
        $('.choosen').each(function() {
             $(this).css('background-color', 'lightsteelblue').removeClass('choosen');
        });
        if($('#answer' + choosenAnswer).hasClass('choosen')) {
            $('#answer' + choosenAnswer).css('background-color', 'lightsteelblue').removeClass('choosen');
        } else {
            $('#answer' + choosenAnswer).css('background-color', 'forestgreen').addClass('choosen');
        }
        $('.choosen').each(function() {
            answers.push(choosenAnswer + '_' + answer);
        });
    } else {
        if(areThereChoosen > 0) {
            $('.choosen').each(function() {
                var id      = $(this).attr('id');
                id          = id.replace('answer', '');
                var answer  = getAnswer(id);
                answers.push(id + '_' + answer);
            });
        }
    }
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=stage.nextQuestion",
        data: {
            type:type,
            quizzID:quizzID, 
            questionID:questionID, 
            answers:answers,
            q:q,
            clr:clr,
            indAnswers:indAnswers,
            options:options,
            checkboxes:checkboxes,
            smiley:smiley
        },
        //dataType: 'json',
        success: function( data ) {
            window.location.href = data;
        }
    });
}
function getAnswer(id) {
    var answer = '';
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=stage.getAnswerText",
        data: {id:id},
        async: false,
        success: function( data ) {
            answer = data;
        }
    });
    return answer;
}
function quitQuizz() {
    if(confirm('Sind Sie sicher, dass Sie das Quizz beenden möchten?') == true) {
        window.close();
    }
}
function chooseAnswer(choosenAnswer) {
    if($('#answer' + choosenAnswer).hasClass('choosen')) {
        $('#answer' + choosenAnswer).css('background-color', 'lightsteelblue').removeClass('choosen');
    } else {
        $('#answer' + choosenAnswer).css('background-color', 'forestgreen').addClass('choosen');
    }
}
</script>