<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$doc = JFactory::getDocument();
$session = JFactory::getSession();
print_r($session->get('quizzes'));
?>
<form action="" method="post" name="adminForm" id="adminForm">
   	<div class="col-12">
    	<h3>Vielen Dank für Ihre Teilnahme</h3>
    	<p>
        	<a onclick="quitQuizz();" class="btn btn-primary">Quizz verlassen</a>
        </p>
    </div>
   	<input type="hidden" id="auditID" name="auditID" value="" />
    <input type="hidden" name="option" value="com_auditum" />
    <input type="hidden" name="task" value="audit.stop" />
    <?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
	function quitQuizz() {
		window.close();
	}
 </script>