<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$doc = JFactory::getDocument();
$session = JFactory::getSession();
$doc->addScript('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js');
?>
<style>
    .quizzResult_theQuestion {
        border-bottom: 2px solid #aaa;
    }
    .quizzResult_yourAnswer {
        font-size: 18px;
        font-weight: bold;
        color: cornflowerblue;
    }
   canvas {
      max-width: 400px!important;
      max-height: 400px!important;
   }

</style>
<form action="" method="post" name="adminForm" id="adminForm">
   	<div class="col-12">
    	<h3>Vielen Dank für Ihre Teilnahme!</h3>
    	<h4>Hier ist Ihr Ergebnis im Detail</h4>
    	<?php echo $this->result;?>
      <canvas id="theModalChart" height="200" width="400"></canvas>
      <p class="mt-4">
      <a onclick="quitQuizz();" class="btn btn-primary text-white">Quizz verlassen</a>
      <a onclick="printQuizz();" class="btn btn-secondary text-white"><i class="fa fa-printer"></i> Ergebnis drucken</a>
      </p>
    </div>
   <input type="hidden" id="theResultID" name="auditID" value="<?php echo $this->theResultID;?>" />
    <input type="hidden" name="option" value="com_auditum" />
    <input type="hidden" name="task" value="audit.stop" />
    <?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   $(document).ready(function() {
      var theResultID = $('#theResultID').val();
      loadSingleChart(theResultID);
   });
    function loadSingleChart(theResultID) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadSingleChart",
         data: {theResultID:theResultID},
         dataType: 'json',
         success: function( data ) {
            if(data['calculate'] == 2) {
               writeChart(data,'');
            }
         }
      });
    }
    function writeChart(data, i) {
      var labels = new Array();
      jQuery.each(data['labels'], function(e, value) {
         labels.push(value);
      });
      var datas = new Array();
      jQuery.each(data['datas'], function(e, value) {
         datas.push(value);
      });
      var backgroundColor = [
         'rgba(255,99,132,1)',
         'rgba(54,162,235,1)',
         'rgba(75,192,192,1)',
         'rgba(22,144,166,1)'
      ]
      var ctx     = document.getElementById('theModalChart' + i).getContext("2d");
      ctx.canvas.height  = 500;
      ctx.canvas.width   = 500;
      if(data['chart'] == 'bar' || data['chart'] == 'horizontalBar') {
         var myChart = new Chart(ctx, {
            type: data['chart'],
            data: {
               labels: labels,
               datasets: [{
                  data: datas,
                  backgroundColor: backgroundColor
               }]
            },
            options: {
               legend: {
                  display: false
               },
               responsive:true,
               maintainAspectRatio: false,
               scales: {
                  yAxes: [{
                      ticks: {
                           stepSize: 1,
                           beginAtZero: true
                      }
                  }],
                  xAxes: [{
                      ticks: {
                           stepSize: 1,
                           beginAtZero: true
                      }
                  }]
               }
            }
         });
      }
      if(data['chart'] == 'pie') {
         var myChart = new Chart(ctx, {
            type: data['chart'],
            data: {
               labels: labels,
               datasets: [{
                  data: datas,
                  backgroundColor: backgroundColor
               }]
            },
            options: {
               scales: {
                  yAxes: [{
                     gridLines: {
                        min: 0,
                        max: 20,
                        stepSize: 1,
                        display: false
                     }
                  }],
                  xAxes: [{
                     gridLines: {
                        display: false
                     }
                  }]
               }
            }
         });
      }
   }
	function quitQuizz() {
		window.close();
	}
</script>