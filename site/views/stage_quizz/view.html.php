<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
 
/**
 * Kunde item view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewStage_quizz extends JViewLegacy
{
	protected $unit;
	protected $result;
	
	public function display($tpl = null)
	{
		$input 		= JFactory::getApplication()->input;
		$session 	= JFactory::getSession();
		$q			= $input->get('q',0,'INT');
		if($q == 0):
			$session->clear('answers');
		endif;
		/*echo '<pre>';
		print_r($session->get('answers'));
		echo '</pre>';*/
		$this->unit 				= $this->get('Unit');
		$this->result 				= $this->get('Result');
		$this->clr 					= $input->get('clr',0,'INT');
		$this->q 					= $input->get('q',0,'INT');
		$app 	= JFactory::getApplication();
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$layout = $input->get('lid','','INT');
		switch($layout) {
			case 1:
				$layout = 'audit-erstellen';
				break;
			case 2:
				$layout = 'katalog';
				break;
			case 3:
				$layout = 'auswertung';
				break;
			case 4:
				$layout = 'vorwort';
				break;
			case 6:
				$layout = 'gueltigkeit';
				break;
			case 7:
				$layout = 'scripts';
				break;
			case 9:
				$layout = 'ende';
				break;
			case 10:
				$theResultID = $input->get('rID', 0, 'INT');
				$this->theResultID = $theResultID;
				$layout = 'result';
				break;
		}
		$this->setLayout($layout);
		
		parent::display($tpl);
	}
}
?>