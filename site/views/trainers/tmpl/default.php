<?php
/**
 * @author      
 * @copyright   
 * @license     
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
$user = JFactory::getUser();
?>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm">
<div class="auditum form-content">
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionboard mt-3 mb-3">
        <button type="button" class="btn btn-success" onclick="Joomla.submitform('trainer.add')">Neuer Trainer</button>
        <button id="deleteTrainer" type="button" class="btn btn-danger" onclick="">Trainer löschen</button>
        <a class="btn btn-danger float-right" href="<?php echo $this->retour;?>">Zurück</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th class="nowrap left" style="width: 50px;">
                    <?php echo JHtml::_('grid.sort', JText::_('ID'), 'a.id', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', JText::_('Name / Firma'), 'a.name', $listDirn, $listOrder) ?>
                </th>
                <th>Anschrift</th>
                <th class="nowrap center">Status</th>
            </tr>
        </thead>
        <tbody>
        <?php if($this->items) { ?>
            <?php foreach($this->items as $item) { ?>
                <?php $i = 0;?>
                <tr>
                    <td class="cid" data-id="<?php echo $item->tblUserID;?>"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
                    <td><?php echo $item->id;?></td>
                    <td>
                        <a href="<?php echo JURI::Root().'trainer-bearbeiten?&layout=edit&id=' . $item->id;?>">
                            <div class="row">
                                <?php
                                if($item->logo) {
                                    echo '<div class="col-12 col-sm-1">';
                                    echo '<img style="display: inline;" src="'.$item->logo.'" />';
                                    echo '</div>';
                                }
                                ?>
                                <div class="col-12 col-sm-11" style="">
                                    <?php echo $item->first_name.' '.$item->last_name.'<br/> '.$item->company;?>
                                </div>
                            </div>
                        </a>
                    </td>
                    <td>
                        <?php
                            if($item->adress): 
                                echo $item->adress;
                            endif;
                            if($item->postcode && $item->city):
                                 echo '<br/>'.$item->postcode.' '.$item->city;
                            endif;
                        ?>
                    </td>
                    <td class="center">
                    <?php 
                        switch($item->published) { 
                            case 1:
                                echo '<h1 class="badge badge-success m-0"><i class="fa fa-check"></i></h1>';
                                break;
                            case 0:
                                echo '<h1 class="badge badge-danger m-0"><i class="fa fa-ban"></i></h1>';
                                break;
                            case 2:
                                echo '<h1 class="badge badge-primary m-0"><i class="fa fa-archive"></i></h1>';
                                break;
                            case -2:
                                echo '<h1 class="badge badge-warning m-0"><i class="fa fa-trash"></i></h1>';
                                break;
                        } 
                    ?>
                    </td>
                </tr>
                <?php $i++;?>
            <?php } ?>
        <?php }  else { ?>
            <tr>
                <td colspan="5">
                    <p>Es wurden keine Trainer gefunden. Bitte überprüfen Sie die Filtereinstellungen</p>
                </td>
            </tr>
        </div>
        <?php } ?>
        </tbody>
    </table>
    <?php echo $this->pagination->getPagesLinks(); ?>
</div>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="theDialog" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title"><i class="fa fa-exclamation-circle text-danger"></i> Löschkonflikte</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p id="dialogMessage"></p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#deleteTrainer').on('click', function() {
        if(confirm("Sollen die ausgewählten Trainer gelöscht werden?") == true) {
            var cid     = $('.cid input');
            var html    = '';
            if(cid) {
                cid.each(function() {
                    if($(this).is(':checked')) {
                        var trainerID = $(this).parent('td').attr('data-id');
                        jQuery.ajax({
                            url: "index.php?option=com_jclassroom&task=trainers.loadContent",
                            data: {trainerID:trainerID},
                            method: 'POST',
                            async: false,
                            //dataType: 'json',
                            success: function( data ) {
                                html += data;
                            }
                        });
                    }
                });
                if(html) {
                    html += '<p><i class="fa fa-exclamation-circle text-danger"></i> Bitte weisen Sie den genannten Elementen zunächst einen neuen Verfasser zu und löschen Sie erst dann den Trainer.</p>';
                    showDialog(html);
                } else {
                    Joomla.submitform('trainers.delete');
                }
            }
        }
    });
    function showDialog(html) {
        $('#dialogMessage').html(html);
        $('#theDialog').modal('show');
    }
</script>