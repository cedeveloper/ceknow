<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Kunden list view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewTrainers extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $toolbar;

	public function display($tpl = null) {
		$session = JFactory::getSession();

    	require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        // Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
	        JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
	        $demo = new LoginHelper();
	        $demo->checkDemo();
	    endif;
	    
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$this->items 		 = $this->get('Items');
		$this->state 		 = $this->get('State');
		$this->pagination 	 = $this->get('Pagination');
		$this->user		 	 = JFactory::getUser();
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour = 'manager-administrator';
				break;
			case 'customer':
				$this->retour = 'manager-customer';
				break;
			case 'trainer':
				$this->retour = 'manager-trainer';
				break;
			case 'student':
				$this->retour = 'manager-student';
				break;
		}
		
		parent::display($tpl);
	}
}
?>