<?php
/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

$doc = JFactory::getDocument();
?>
<div class="inflow admin row">
	<div class="col-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: skyblue;">
					<div class="card-header">Kunden</div>
					<div class="card-body">
						<a href="manager-administrator/customers" class="btn btn-secondary">Zu den Kunden</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: lightseagreen;">
					<div class="card-header">Firmen</div>
					<div class="card-body">
						<a href="manager-administrator/companies-admin" class="btn btn-secondary">Firmen</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-primary mb-3">
					<div class="card-header">Trainer</div>
					<div class="card-body">
						<a href="manager-administrator/trainer" class="btn btn-secondary">Zu den Trainern</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-success mb-3">
					<div class="card-header">Teilnehmer</div>
					<div class="card-body">
						<a href="manager-administrator/students" class="btn btn-secondary">Zur Teilnehmerverwaltung</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-warning mb-3">
					<div class="card-header">Learningrooms</div>
					<div class="card-body">
						<a href="classrooms" class="btn btn-secondary">Zur Learningroom-Verwaltung</a>
					</div>
				</div>
			</div>
			<!--<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: sandybrown;">
					<div class="card-header">Audits</div>
					<div class="card-body">
						<a href="library-days" class="btn btn-secondary">Audits</a>
					</div>
				</div>
			</div>-->
			<!--<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: teal;">
					<div class="card-header">Tutorials</div>
					<div class="card-body">
						<a href="library-days" class="btn btn-secondary">Tutorials</a>
					</div>
				</div>
			</div>-->
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-danger mb-3">
					<div class="card-header">Quizze</div>
					<div class="card-body">
						<a href="quizze" class="btn btn-secondary">Zu den Quizzen</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-info mb-3">
					<div class="card-header">Bibliothek</div>
					<div class="card-body">
						<a href="library-days" class="btn btn-secondary">Tage</a>
						<a href="library-modules" class="btn btn-secondary">Module</a>
						<a href="library-units" class="btn btn-secondary">Units</a>
					</div>
				</div>
			</div>
			<!--<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: indianred;">
					<div class="card-header">Veröffentlichen</div>
					<div class="card-body">
						<a href="library-days" class="btn btn-secondary">Quizze</a>
						<a href="library-modules" class="btn btn-secondary">Audits</a>
						<a href="library-units" class="btn btn-secondary">Learningrooms</a>
					</div>
				</div>
			</div>-->
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: orchid;">
					<div class="card-header">Auswertungen</div>
					<div class="card-body">
						<a href="evaluate-quizze" class="btn btn-secondary">Quizze</a>
						<!--<a href="evaluate-learningrooms" class="btn btn-secondary">Learningrooms</a>-->
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: darksalmon;">
					<div class="card-header">Templates</div>
					<div class="card-body">
						<a href="email-templates" class="btn btn-secondary">Zu den Templates</a>
					</div>
				</div>
			</div>
			<!--<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: palevioletred;">
					<div class="card-header"><i class="fa fa-euro"></i> Rechnungen</div>
					<div class="card-body">
						<a href="invoices" class="btn btn-light">Zu den Rechnungen</a>
					</div>
				</div>
			</div>-->
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-secondary mb-3">
					<div class="card-header">Einstellungen</div>
					<div class="card-body">
						<a href="configuration" class="btn btn-light">Zu den Einstellungen</a>
					</div>
				</div>
			</div>
		</div>
		<div class="card text-white bg-dark mb-3">
			<div class="card-header text-white">Staging</div>
			<div class="card-body">
				<div class="col-12">
					<div class="row">
						<!--<div class="col-12 col-sm-6 col-md-4 col-lg-4">
							<div class="card text-white bg-primary mb-3">
								<div class="card-header">Learningroom erstellen und veröffentlichen</div>
								<div class="card-body">
									<p>Erstellen Sie einen Learningroom oder wählen Sie einen bestehenden Learningroom aus und veröffentlichen Sie ihn</p>
									<a href="manager-administrator/trainer" class="btn btn-secondary">Zum Learningroom</a>
								</div>
							</div>
						</div>-->
						<div class="col-12 col-sm-6 col-md-4 col-lg-4">
							<div class="card text-white bg-primary mb-3">
								<div class="card-header">Quizz erstellen und veröffentlichen</div>
								<div class="card-body">
									<p>Erstellen Sie ein Quizz oder wählen Sie ein bestehendes Quizz aus und veröffentlichen Sie es</p>
									<a href="veroeffentlichen/quizz-veroeffentlichen" class="btn btn-secondary"> Quizz veröffentlichen</a>
									<a href="veroeffentlichen/veroeffentlichte-quizze" class="btn btn-secondary">Veröffentlichte Quizze</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card text-white bg-light mb-3">
			<div class="card-header text-dark">Shortcuts</div>
			<div class="card-body">
				<a href="manager-administrator/reseller" class="float-right btn btn-danger">Abmelden</a>
			</div>
		</div>
		<div class="card text-white mb-3" style="background-color: lightsteelblue;">
			<div class="card-header text-dark">Logfiles</div>
			<div class="card-body">
				<?php
					if($this->logfiles):
						$i = 1;
						foreach($this->logfiles as $logfile):
							echo '<p id="logfile_'.$logfile->id.'" class="text-dark">
								<span class="badge badge-dark text-white">'.$i.'</span> 
								<span class="badge badge-primary text-white">'.date('d.m.Y H:i', strtotime($logfile->created)).'</span> 
								'.$logfile->wert.'<a onclick="quit('.$logfile->id.')" class="float-right btn btn-danger btn-sm text-white">Quittieren</a></p>';
							$i++;
						endforeach;
					endif;
				?>
			</div>
		</div>
	</div>
</div>
<script>
function quit(id) {
	if(confirm('Soll dieses Logfile quittiert werden?') == true) {
		$.ajax({
		    type: "POST",
		    url: "index.php?option=com_jclassroom&task=manager.quitt",
		    data: {
		       id:id
		    },
		    //dataType: 'json',
		    success: function( data ) {
		      $('#logfile_' + id).remove();
		    }
		});
	}
}

</script>