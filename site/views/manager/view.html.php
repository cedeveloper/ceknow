<?php

/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewManager extends JViewLegacy {

    public function display($tpl = null) {
        require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        $testmode = LoginHelper::checkTestmode();
        if($testmode):
            echo $testmode;
        endif;
        $session    = JFactory::getSession();
        $document   = JFactory::getDocument();
        $document->setTitle('Manager [Kunde]['.$session->get('systemname').']');
        // Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
            JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
            $demo = new LoginHelper();
            $demo->checkDemo();
        endif;
        $session->set('group','superuser');
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $groups = JAccess::getGroupsByUser($user->id);
        // Students
        if(in_array(11,$groups)) {
            $session->set('group','student');
            $app->redirect('plans/manager-reseller');
            exit();
        }
        // Reseller
        if(in_array(12,$groups)) {
            $session->set('group','reseller');
            $app->redirect('plans/manager-reseller');
            exit();
        }
        // Trainer
        if(in_array(10,$groups)) {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('Trainer','JclassroomTable',array());
            $load   = array('tblUserID' => $user->id);
            $table->load($load);
            $customerID = $table->customerID;
            echo 'HHH'.$customerID;
            $session->set('customerID', $customerID);
            $session->set('group','trainer');
            //$app->redirect(JURI::Root().'manager-trainer');
            exit();
        }
        $this->logfiles = $this->get('Logfiles');

        parent::display($tpl);
    }
}
