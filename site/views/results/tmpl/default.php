<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="index.php?option=com_jclassroom&task=students.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept=".csv" name="uploadCSV" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <!--<a class="btn btn-success text-white" onclick="Joomla.submitform('student.add')">Neuer Teilnehmer</a>
        <a class="btn btn-danger text-white m-1" id="deleteCustomer" onclick="Joomla.submitform('students.delete')">Teilnehmer löschen</a>-->
        <button type="button" class="btn btn-info" onclick="Joomla.submitform('students.saveCSV')">Liste exportieren</button>
        <a href="dashboard-trainer" class="btn btn-danger text-white float-right">Zurück</a>
    </div>
    <table class="table table-striped">	
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                 <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Kunden-Nr.', 'a.customer_number', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Kunde', 'a.name', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Adresse', 'a.city', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Kontakt', 'a.email', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Ansprechpartner', 'a.asp_name', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Status'), 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>		
        <tbody>
        <?php if($this->items) { ?>
        <?php foreach ($this->items as $i => $item) :
        ?>
            <tr class="row<?php echo $i % 2; ?>">
            	<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
				<td class="right"><?php echo $this->escape($item->id); ?></td>
                <td class="left"><?php echo $this->escape($item->customer_number); ?></td>
                <td>
                    <a href="<?php echo JURI::Root().'student-edit?layout=edit&id='. $item->id; ?>">
                        <?php
                        if($item->logo) {
                            echo '<img class="mr-2" style="display: inline; height: 40px;" src="'.$item->logo.'" />';
                        }
                        if($item->company) {
                            echo $item->company.'<br/>';
                        }
                        ?>
                        <?php echo $item->first_name.' '.$item->last_name;?>
                    </a>
                </td>
                <td class="left">
                    <?php echo $this->escape($item->adress); ?><br/>
                    <?php echo $item->postcode.' '.$item->city; ?> 
                </td>
                <td class="left">
                    Telefon: <?php echo $this->escape($item->phone); ?><br/>
                    Mobil: <?php echo $this->escape($item->mobile); ?><br/>
                    E-Mail: <?php echo $this->escape($item->email); ?>        
                </td>
                
                <td class="left">
                    <?php echo $this->escape($item->asp_name); ?><br/>
                    <?php echo $this->escape($item->asp_phone); ?><br/>
                    <?php echo $this->escape($item->asp_email); ?>        
                </td>
                <?php
                if($item->published == 0) {
					$class="badge-danger";
					$wert = "<i class='fa fa-ban'></i>";
				} elseif($item->published == 1) {
					$class="badge-success";
					$wert = "<i class='fa fa-check'></i>";
				}
				?>
                <td class="text-center">
                	<span class="btn <?php echo $class;?>"><?php echo $wert;?></span>
               	</td>
            </tr>
        <?php endforeach ?>
        <?php } else { ?>
        	<tr>
            	<td colspan="8">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
       	<?php } ?>
        </tbody>			
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
jQuery('#openUploader').click(function() {
    jQuery('#uploader').trigger('click');
    
});
jQuery('#uploader').on('change', function() {
    jQuery('#uploaderForm').submit();
});
</script>