<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
require_once(JPATH_COMPONENT.'/views/account/tmpl/newAdmin.php');
require_once(JPATH_COMPONENT.'/views/account/tmpl/editAdmin.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

    <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a class="btn btn-success text-white" onclick="checkPlanValids(1);">Speichern</a>
         <a class="btn btn-secondary text-white" onclick="checkPlanValids(2);">Speichern & Schließen</a>
      <?php endif;?>
        <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div id="theAlerts" class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> Bitte prüfen Sie die Pflichtfelder</div>
   <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
         <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Basisdaten</a>
      </li>
      <?php if($this->usergroup == 'superuser' || $this->usergroup == 'customer'):?>
      <li class="nav-item">
         <a class="nav-link" id="plan-tab" data-toggle="tab" href="#plan" role="tab" aria-controls="plan" aria-selected="false">Plan</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="pay-tab" data-toggle="tab" href="#pay" role="tab" aria-controls="pay" aria-selected="false">Zahlungsdaten</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="invoices-tab" data-toggle="tab" href="#invoices" role="tab" aria-controls="invoices" aria-selected="false">Rechnungen</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="false">Administratoren</a>
      </li>
      <?php endif; ?>
      <?php if($this->usergroup == 'trainer'):?>
      <li class="nav-item">
         <a class="nav-link" id="theCustomer-tab" data-toggle="tab" href="#theCustomer" role="tab" aria-controls="theCustomer" aria-selected="false">Kunde</a>
      </li>
      <?php endif; ?>
   </ul>
   <div class="tab-content" id="tabContent">
      <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
         <div class="form-horizontal mt-3 w-50">
            <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('salutation'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('salutation'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('first_name'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('first_name'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('last_name'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('last_name'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('customer_number'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('customer_number'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('company_name'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('company_name'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('address'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('address'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('postcode'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('postcode'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('city'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('city'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('phone'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('phone'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('fax'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('fax'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-2 col-form-label">
                   <?php echo $this->form->getLabel('email'); ?>
                 </div>
                 <div class="col-12 col-sm-10">
                   <?php echo $this->form->getInput('email'); ?>
                   <input type="hidden" id="originEmail" value="<?php echo $this->item->email;?>" />
                 </div>
             </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('username'); ?><br/>
                  
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('username'); ?>
                  <small><i class="fa fa-exclamation"></i> Der Benutzername ist die E-Mail-Adresse. Dieser kann nur geändert werden, wenn sich auch Ihre E-Mail-Adresse ändert.</small>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('password'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('password'); ?>
                  <?php echo $this->form->getInput('newDS'); ?>
                  <?php echo $this->form->getInput('currentUserID'); ?>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="plan" role="tabpanel" aria-labelledby="plan-tab">
         <div class="form-horizontal mt-3">
            <div class="row">
               <div class="col-12 col-sm-6">
                  <h4>Ihr aktueller Plan: <i><?php echo $this->item->planName;?></i></h4>
                  <?php //if($this->item->plan == 4): ?>
                     <?php
                     $now  = new Datetime();
                     $to   = new Datetime($this->item->plan_to);
                     $to->sub(new DateInterval('P1D'));
                     $delta = $now->diff($to);
                     $deltaTo = $delta->format('%r%a') + 1;
                     if($this->item->paymentAbo == 1):
                       $cancel = new Datetime($to->format('Y-m-d'));
                       $cancel = $cancel->sub(new DateInterval('P7D'));
                     else:
                       $cancel = new Datetime($to->format('Y-m-d'));
                       $cancel = $cancel->sub(new DateInterval('P2M'));
                     endif;
                     ?>
                     <p><b>Ihr Zugang ist gültig:</b><br/>
                     Vom: <?php echo date('d.m.Y', strtotime($this->item->plan_from));?><br/>
                     Bis: <?php echo $to->format('d.m.Y');?><br>
                     Verbleibende Tage: <b><?php echo $deltaTo.' Tage';?></b>
                     </p>
                  <?php //endif; ?>
                  <?php if($this->item->plan != 3): ?>
                  <div class="card bg-light p-3">
                     <h2><b>Upgrade</b> Noch mehr Möglichkeiten!<br>Jetzt auf einen anderen Plan wechseln</h2>
                     <a href="prices-novalid?deno=1&pid=<?php echo $this->item->plan;?>" class="btn btn-primary">Jetzt wechseln</a>
                  </div>
                  <?php endif; ?>
                  <?php if($this->item->plan != 4):?>
                     <div class="card bg-light mt-2 p-3">
                        <h2><b>Downgrade</b></h2>
                        <div id="plansToChange">
                           <?php
                           if($this->item->plans):
                              if($this->item->plan > 1):
                                 foreach($this->item->plans as $plan):
                                    if($plan->id < $this->item->plan):
                                       $disabled   = '';
                                       $visible    = '';
                                       if($this->item->plan == $plan->id):
                                          $disabled   = 'disabled';
                                          $visible    = 'd-none';
                                       endif;
                                       /*if($this->item->plan == 2):
                                          $basic = '';
                                          $premium = 'disabled';
                                       endif;
                                       if($this->item->plan == 3):
                                          $basic = '';
                                          $premium = '';
                                       endif;*/
                                       echo '<a style="cursor: pointer;" onclick="downgradePlan('.$plan->id.');" id="basis" class="'.$visible.' text-center text-white btn btn-'.$plan->color_btn.' p-2 '.$disabled.' mr-2">'.$plan->title.'plan</a>';
                                    endif;
                                 endforeach;
                              else:
                                 echo '<p>Keine Downgrade-Möglichkeiten vorhanden</p>';
                              endif;
                           endif;
                           ?>
                        </div>
                     </div>
                     <div class="card bg-light mt-2 p-3">
                        <h2>Sie möchten Ihren Plan kündigen?</h2>
                        <h4 style="font-size: 20px;">Ihr frühestes Kündigungsdatum: <?php echo $cancel->format('d.m.Y');?></h4>
                        <a onclick="cancelPlan();" class="btn btn-danger text-white">Kündigung</a>
                     </div>
                  <?php endif;?>
               </div>
               <div class="col-12 col-sm-6">
                  <h5>Ihr aktueller Plan beinhaltet:</h5>
                  <div class="card bg-light p-3">
                     <div class="form-group row">
                        <div class="col-12 col-sm-4 col-form-label">
                           
                        </div>
                        <div class="col-12 col-sm-2 text-center">
                           <b>Plan</b>
                        </div>
                        <div class="col-12 col-sm-2 text-center">
                           <b>In Verwendung</b>
                        </div>
                        <div class="col-12 col-sm-2 text-center">
                           <b>Frei</b>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12 col-sm-4 col-form-label">
                           Anzahl Administratoren
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_admins" value="<?php echo $this->item->customer_plans->count_admin;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: darksalmon;"readonly="true" id="plan_admins_is" value="<?php echo $this->item->count_customer_administratoren;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <?php 
                           $rest    = $this->item->customer_plans->count_admin - $this->item->count_customer_administratoren;
                           ?>
                           <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12 col-sm-4 col-form-label">
                           Anzahl Trainer
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_trainer" value="<?php echo $this->item->customer_plans->count_trainer;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: darksalmon;" readonly="true" id="plan_trainer_is" value="<?php echo $this->item->count_customer_trainers;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <?php 
                           $rest    = $this->item->customer_plans->count_trainer - $this->item->count_customer_trainers;
                           ?>
                           <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12 col-sm-4 col-form-label">
                           Anzahl Learningrooms
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_learningrooms" value="<?php echo $this->item->customer_plans->count_learningrooms;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: darksalmon;" readonly="true" id="plan_trainer_is" value="<?php echo $this->item->count_customer_learningrooms;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <?php 
                           $rest    = $this->item->customer_plans->count_learningrooms - $this->item->count_customer_learningrooms;
                           ?>
                           <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12 col-sm-4 col-form-label">
                           Anzahl Module
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_modules" value="<?php echo $this->item->customer_plans->count_modules;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: darksalmon;" readonly="true" id="plan_trainer_is" value="<?php echo $this->item->count_customer_modules;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <?php 
                           $rest    = $this->item->customer_plans->count_modules - $this->item->count_customer_modules;
                           ?>
                           <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12 col-sm-4 col-form-label">
                           Anzahl Units
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: lightblue;" readonly="true" id="plan_units" value="<?php echo $this->item->customer_plans->count_units;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <input type="text" class="text-center" style="background-color: darksalmon;" readonly="true" id="plan_trainer_is" value="<?php echo $this->item->count_customer_units;?>" />
                        </div>
                        <div class="col-12 col-sm-2">
                           <?php 
                           $rest    = $this->item->customer_plans->count_units - $this->item->count_customer_units;
                           ?>
                           <input type="text" class="text-center" style="background-color: lightgreen;"readonly="true" id="plan_admins_rest" value="<?php echo $rest;?>" />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="pay" role="tabpanel" aria-labelledby="pay-tab">
         <div class="form-horizontal mt-3 w-50">
             <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('paymentAbo'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('paymentAbo'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('paymentID'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('paymentID'); ?>
               </div>
            </div>
            <!--<div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php //echo $this->form->getLabel('valid_from'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php //echo $this->form->getInput('valid_from'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php //echo $this->form->getLabel('valid_to'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php //echo $this->form->getInput('valid_to'); ?>
               </div>
            </div>-->
            <?php
            switch($this->item->customer_payments->paymentID):
               case 1:
                  $showInvoice      = 'block';
                  $showCreditCard   = 'none';
                  $showDirectDebit  = 'none';
                  break;
               case 2:
                  $showInvoice      = 'none';
                  $showCreditCard   = 'none';
                  $showDirectDebit  = 'block';
                  break;
               case 3:
                  $showInvoice      = 'none';
                  $showCreditCard   = 'block';
                  $showDirectDebit  = 'none';
                  break;
               default:
                  $showInvoice      = 'none';
                  $showCreditCard   = 'none';
                  $showDirectDebit  = 'none';
            endswitch;
            ?>
            <div id="creditCard" style="background-color: lightsalmon;padding: 10px;display: <?php echo $showCreditCard;?>;">
               <h5>Kreditkartendaten</h5>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('creditcard_bank'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('creditcard_bank'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('creditcard_number'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('creditcard_number'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('creditcard_check_number'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('creditcard_check_number'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('creditcard_valid_to'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('creditcard_valid_to'); ?>
                  </div>
               </div>
            </div>
            <div id="directDebit" style="background-color: lightcoral;padding: 10px;display: <?php echo $showDirectDebit;?>;">
               <h5>Lastschrift</h5>
               <div class="form-group row">
                  <div class="col-12 col-sm-4 col-form-label">
                     <?php echo $this->form->getLabel('account_owner'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <?php echo $this->form->getInput('account_owner'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-4 col-form-label">
                     <?php echo $this->form->getLabel('iban'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <?php echo $this->form->getInput('iban'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-4 col-form-label">
                     <?php echo $this->form->getLabel('bic'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <?php echo $this->form->getInput('bic'); ?>
                  </div>
               </div>
            </div>
            <div id="tab_invoice" class="card bg-light p-3" style="display: <?php echo $showInvoice;?>;">
               <h5><b>Adresse</b></h5>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('company_repeat'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('company_repeat'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('address_repeat'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('address_repeat'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('postcode_repeat'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('postcode_repeat'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('city_repeat'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('city_repeat'); ?>
                  </div>
               </div>
               <hr>
               <div class="form-group row">
                  <div class="col-12 col-sm-4 form-label">
                     <label>Abweichende Rechnungsadresse</label>
                  </div>
                  <div class="col-12 col-sm-8 text-left">
                     <?php
                     $alt_invoice = '';
                     if($this->item->alt_invoice == 1):
                        $alt_invoice = 'checked';
                     endif;
                     ?>
                     <input style="width: 30px;height: 30px;" type="checkbox" id="check_invoice_adress" <?php echo $alt_invoice;?> name="jform[alt_invoice]" class="form-control" onchange="openInvoiceAdress();" value="1" />
                  </div>
               </div>
               <div id="invoice" style="display: none;">
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 form-label">
                        <label>Vorname</label>
                     </div>
                     <div class="col-12 col-sm-8">
                        <input type="text" id="invoice_first_name" name="jform[invoice_first_name]" class="form-control" placeholder="Vorname" value="<?php echo $this->item->invoice_first_name;?>" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 form-label">
                        <label>Nachname</label>
                     </div>
                     <div class="col-12 col-sm-8">
                        <input type="text" id="invoice_last_name" name="jform[invoice_last_name]" class="form-control" placeholder="Nachname" value="<?php echo $this->item->invoice_last_name;?>" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 form-label">
                        <label>Abteilung</label>
                     </div>
                     <div class="col-12 col-sm-8">
                        <input type="text" id="invoice_function" name="jform[invoice_function]" class="form-control" placeholder="Funktion" value="<?php echo $this->item->invoice_function;?>" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 form-label">
                        <label>Straße u. Hausnr.*</label>
                     </div>
                     <div class="col-12 col-sm-8">
                        <input type="text" id="invoice_alt_adress" name="jform[invoice_adress]" class="form-control" placeholder="Straße u. Hausnr." value="<?php echo $this->item->invoice_adress;?>" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 form-label">
                        <label>PLZ*</label>
                     </div>
                     <div class="col-12 col-sm-8">
                        <input type="text" id="invoice_alt_plz" name="jform[invoice_postcode]" class="form-control" placeholder="Postleitzahl" value="<?php echo $this->item->invoice_postcode;?>" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-4 form-label">
                        <label>Ort*</label>
                     </div>
                     <div class="col-12 col-sm-8">
                        <input type="text" id="invoice_alt_city" name="jform[invoice_city]" class="form-control" placeholder="Ort" value="<?php echo $this->item->invoice_city;?>" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="invoices" role="tabpanel" aria-labelledby="invoices-tab">
         <div class="form-horizontal mt-3 w-50">
            <h4>Ihre Rechnungen</h4>
         </div>
      </div>
      <div class="tab-pane fade" id="users" role="tabpanel" aria-labelledby="users-tab">
         <div class="form-horizontal mt-3">
            <a class="btn btn-success text-white mb-3" onclick="newAdmin();">Neuer Administrator</a>
            <h5 class="m-0">Weitere Administratoren</h5>
            <small class="d-block mb-3">Administratoren erhalten einen Zugang zum Verwaltungsbereich des Systems.<br/>Sie werden als Administrator nicht mit angezeigt, da Sie bereits registriert sind. Sie können daher nur weitere Administratoren einlegen (Verfügbarkeit ist abhängig von Ihrem Plan).</small>
            <div class="row">
               <div class="col-12 col-sm-2">Aktionen</div>
               <div class="col-12 col-sm-5">Name</div>
               <div class="col-12 col-sm-5">Kontaktdaten</div>
            </div>
            <div id="admins" class="bg-light p-1">
               <?php
               if($this->item->customer_administratoren):
                  foreach($this->item->customer_administratoren as $admin):
                     echo '<div id="admin'.$admin->id.'" class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                     echo '<div class="row">';
                     echo '<div class=" col-12 col-sm-2">';
                     echo '<span class="badge bg-dark text-white mr-2">ID: '.$admin->id.'</span>';
                     echo '<i title="Administrator bearbeiten" onclick="editAdmin('.$admin->id.');" class="fa fa-pencil mr-2"></i>';
                     echo '<i title="Administrator löschen" onclick="deleteAdmin('.$admin->id.');" class="fa fa-trash-o"></i>';
                     echo '</div>';
                     echo '<div class="administratorName col-12 col-sm-5">';
                     echo $admin->salutation.' '.$admin->first_name.' '.$admin->last_name;
                     echo '</div>';
                     echo '<div class=" col-12 col-sm-5">';
                     echo '<span class="badge bg-success text-white"><i class="fa fa-phone"></i> <span class="administratorPhone">'.$admin->phone.'</span></span> | <span class="badge bg-danger text-white"><i class="fa fa-envelope"></i> <span class="administratorEmail">'.$admin->email.'</span></span>';
                     echo '</div>';
                     echo '</div>';
                     echo '</div>';
                  endforeach;
               else:
                  echo '<div id="adminPlaceholder" class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                  echo 'Keine Administratoren gefunden';
                  echo '</div>';
               endif;
               ?>
            </div>
         </div>
      </div>
      <?php if($this->usergroup == 'trainer'):?>
         <div class="tab-pane fade" id="theCustomer" role="tabpanel" aria-labelledby="theCustomer-tab">
            <div class="form-horizontal mt-3 w-50">
               <h4>Ihr Trainerkonto wird verwaltet von:</h4>
               <div class="form-group row m-0">
                  <div class="col-12 col-sm-4 col-form-label">
                     <b>Kunde</b>
                  </div>
                  <div class="col-12 col-sm-8 p-2">
                     <?php echo $this->item->company; ?>
                  </div>
               </div>
               <div class="form-group row m-0">
                  <div class="col-12 col-sm-4 col-form-label">
                     <b>Anschrift</b>
                  </div>
                  <div class="col-12 col-sm-8 p-2">
                     <?php echo $this->item->customer_adress; ?>
                  </div>
               </div>
               <div class="form-group row m-0">
                  <div class="col-12 col-sm-4 col-form-label">
                     <b>Postleitzahl</b>
                  </div>
                  <div class="col-12 col-sm-8 p-2">
                     <?php echo $this->item->customer_postcode; ?>
                  </div>
               </div>
               <div class="form-group row m-0">
                  <div class="col-12 col-sm-4 col-form-label">
                     <b>Ort</b>
                  </div>
                  <div class="col-12 col-sm-8 p-2">
                     <?php echo $this->item->customer_city; ?>
                  </div>
               </div>
               <div class="form-group row m-0">
                  <div class="col-12 col-sm-4 col-form-label">
                     <b>Telefon</b>
                  </div>
                  <div class="col-12 col-sm-8 p-2">
                     <?php echo $this->item->customer_phone; ?>
                  </div>
               </div>
               <div class="form-group row m-0">
                  <div class="col-12 col-sm-4 col-form-label">
                     <b>E-Mail</b>
                  </div>
                  <div class="col-12 col-sm-8 p-2">
                     <?php echo $this->item->customer_email; ?>
                  </div>
               </div>
               <div class="form-group row m-0">
                  <div class="col-12 col-sm-4 col-form-label">
                     <b>Administratoren</b>
                  </div>
                  <div class="col-12 col-sm-8 p-2">
                     <?php
                     if($this->item->customer_administratoren):
                        foreach($this->item->customer_administratoren as $admin):
                           echo '<p>'.$admin->first_name.' '.$admin->last_name.' 
                           <span class="badge badge-success text-white">Telefon: '.$admin->phone.'</span>
                           <span class="badge badge-danger text-white">E-Mail: '.$admin->email.'</span></p>';
                        endforeach;
                     endif;
                     ?>
                  </div>
               </div>
            </div>
         </div>
      <?php endif; ?>
   </div>
<input type="hidden" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="jform_customerID" name="jform[customerID]" value="<?php echo $this->item->customerID;?>" />
<input type="hidden" name="jform[customerAdministratorID]" value="<?php echo $this->item->customerAdministratorID;?>" />
<input type="hidden" name="userID" value="<?php echo $this->item->tblUserID;?>" />
<input type="hidden" id="usergroup" name="usertype" value="<?php echo $this->usergroup;?>" />
<input type="hidden" id="alt_invoice" value="<?php echo $this->item->alt_invoice;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="account_dialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Nachricht</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            
         </div>
         <div class="modal-footer">
            <button type="button" id="saveAccountDialog" class="btn btn-success" data-dismiss="modal">OK</button>
            <button type="button" id="cancelAccountDialog" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<script type="text/javascript">
   $(document).ready(function() {
      $('#jform_password').val('');
      var usergroup = $('#usergroup').val();
      if(usergroup == 'trainer') {
         $('#jform_company').attr('readonly', true).attr('disabled', true);
      }
      if(usergroup == 'student') {
         $('#jform_company').attr('readonly', true).attr('disabled', true);
      }
      if($('#alt_invoice').val() == 1) {
         $('#check_invoice_adress').prop('checked', true);
         $('#invoice').fadeIn(200);
      }
   });   
   var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      invalidHandler: function(form, validator) {
         /*var map = validator.errorMap;
         var theAlerts = '';
         for (var i in validator.errorMap) {
            var inval = validator.errorMap[i];
            theAlerts += '<p class="m-0">' + inval + '</p>';
          }*/
          $('#theAlerts').css('display', 'block');
      },
      rules: {
         'jform[email]': {
            required: true,
            email: true
         },
         'jform[first_name]': {
            required: true
         },
         'jform[last_name]': {
            required: true
         },
         'jform[salutation]': {
            required: true
         }
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[first_name]': 'Bitte geben Sie Ihren Vornamen ein.',
         'jform[last_name]': 'Bitte geben Sie Ihren Nachnamen ein.',
          'jform[salutation]': 'Bitte wählen Sie eine Anrede aus.'
      },
      submitHandler: function(form) {
         form.submit();
      }
   });
   jQuery('#jform_email').on('change', function() {
      var email = jQuery('#jform_email').val();
      var originEmail = $('#originEmail').val();
      if(email) {
         jQuery.ajax({
               type: "POST",
               url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
               data: {email:email},
               //dataType: 'json',
               success: function( data ) {
                  if(data == 'OK') {
                     jQuery('#jform_username').val(email);
                  } else {
                     if(email != originEmail) {
                        $('#account_dialog .modal-body').html('Fehler: 0001(Check EMail). Die E-Mail-Adresse wird bereits verwendet.');
                        $('#account_dialog').modal('show');
                        jQuery('#jform_email').val(originEmail);
                        return false;
                     }
                  }
               }
           });
      }
   });
   jQuery('#newAdmin #email').on('change', function() {
      var email = jQuery('#newAdmin #email').val();
      if(email) {
         alert(email);
         jQuery.ajax({
               type: "POST",
               url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
               data: {email:email},
               //dataType: 'json',
               success: function( data ) {
                  if(data == 'OK') {
                  } else {
                     $('#account_dialog .modal-body').html('Fehler: 0001(Check Admin-EMail). Die E-Mail-Adresse wird bereits verwendet.');
                     $('#account_dialog').modal('show');
                     $('#newAdmin #email').val('');
                     return false;
                  }
               }
           });
      }
   });
   function openInvoiceAdress() {
      if($('#check_invoice_adress').is(':checked')) {
         $('#invoice').fadeIn(200);
      } else {
         $('#invoice').fadeOut(200);
      }
   }
   function checkPlanValids(type) {
      var usertype   = $('#usertype').val();
      if(usertype    == 'superuser' || usertype == 'customer') {
         var planID    = $('#jform_plan option:selected').val();
         if(planID == 4) {
            var plan_from  = $('#jform_plan_from').val();
            var plan_to    = $('#jform_plan_to').val();
            if(!plan_from || !plan_to) {
               $('#account_dialog .modal-body').html('Bitte geben Sie den Gültigkeitszeitraum für den Demozugang ein.');
                $('#account_dialog').modal('show');
               return false;
            }
         }
         if(type == 1) {
            if(checkInvoice(planID) == true) {
               Joomla.submitform('account.simpleSave');
            };
         }
         if(type == 2) {
            if(checkInvoice(planID) == true) {
               Joomla.submitform('account.save');
            };
         }
      } else {
         if(type == 1) {
            Joomla.submitform('account.simpleSave');
         }
         if(type == 2) {
            Joomla.submitform('account.save');
         }
      }
   }
   function downgradePlan(planID) {
      if(confirm('Möchten Sie Ihren Plan downgraden?') == true) {
         $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=customer.downgradePlan",
            data: {
               planID:planID
            },
            //dataType: 'json',
            success: function( data ) {
               $('#plansToChange').empty();
               $('#account_dialog .modal-body').html('Ihr Downgrade wurde übermittelt. Sie erhalten ein einer separaten Mail weitere Informationen.');
               $('#account_dialog').modal('show');
               return false;
            }
         });
      }
   }
   function cancelPlan() {
      $('#account_dialog .modal-body').html('Möchten Sie Ihren Plan zum nächstmöglichen Zeitpunkt kündigen?');
      $('#account_dialog .modal-footer #cancelAccountDialog').css('display', 'inline-block')
      $('#account_dialog .modal-footer #saveAccountDialog').attr('onclick','cancelPlanExecute();');
      $('#account_dialog').modal('show');
   }
   function cancelPlanExecute() {
      $('#account_dialog').modal('hide');
      $('#save').css('display', 'block');
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=customer.cancelPlan",
         data: {
         },
         //dataType: 'json',
         success: function( data ) {
            $('#account_dialog .modal-body').html('Ihre Kündigung wurde ausgeführt. Sie erhalten weitere Informationen per E-Mail.');
            $('#account_dialog .modal-footer #saveAccountDialog').removeAttr('onclick');
            $('#account_dialog .modal-footer #cancelAccountDialog').css('display', 'none');
            hideSave();
            $('#account_dialog').modal('show');
         }
      });
   }
   function hideSave() {
      setTimeout(hideSaveExecute,800);
   }
   function hideSaveExecute() {
      jQuery('#save').slideUp(200);
   }
   function checkInvoice(planID) {
      var paymentID = $('#jform_paymentID option:selected').val();
      if(paymentID == 1 && planID != 4) {
         var invoice_adress   = $('#jform_invoice_adress').val();
         var invoice_postcode = $('#jform_invoice_postcode').val();
         var invoice_city     = $('#jform_invoice_city').val();
         if(!invoice_adress || !invoice_postcode || !invoice_city) {
            alert('Bitte füllen Sie die Felder in den Rechnungsdaten aus.');
            if(!invoice_adress) {
               $('#jform_invoice_adress').parent('div').append('<label id="jform_invoice_adress-error" class="error" for="jform_invoice_adress">Bitte geben Sie eine Rechnungsadresse ein</label>');
            }
            if(!invoice_postcode) {
               $('#jform_invoice_postcode').parent('div').append('<label id="jform_invoice_postcode-error" class="error" for="jform_invoice_postcode">Bitte geben Sie eine Postleitzahl ein</label>');
            }
            if(!invoice_adress) {
               $('#jform_invoice_city').parent('div').append('<label id="jform_invoice_city-error" class="error" for="jform_invoice_city">Bitte geben Sie einen Ort ein</label>');
            }
            return false;
         }
      }
      return true;
   }
   function newAdmin() {
      var admins_is  = $('#plan_admins_rest').val();
      if(admins_is == 0) {
         alert('Sie haben die maximale Anzahl an Administratoren für Ihren Plan erreicht.');
         return false;
      }
      $('#newAdmin').modal('show');
   }
   $('#saveNewAdmin').on('click', function() {
      var salutation    = $('#salutation option:selected').val();
      var first_name    = $('#first_name').val();
      var last_name     = $('#last_name').val();
      var email         = $('#email').val();
      var password      = $('#password').val();
      var cpassword     = $('#password_confirm').val();
      if(!salutation) {
         alert('Bitte wählen Sie eine Anrede');
         return false;
      }
      if(!first_name) {
         alert('Bitte geben Sie einen Vornamen ein');
         return false;
      }
      if(!last_name) {
         alert('Bitte geben Sie einen Nachnamen ein');
         return false;
      }
      if(!email) {
         alert('Bitte geben Sie eine E-Mail-Adresse ein');
         return false;
      }
      if(!password) {
         alert('Bitte geben Sie ein Passwort ein');
         return false;
      }
      if(!cpassword) {
         alert('Bitte wiederholen Sie das Passwort');
         return false;
      }
      if(password != cpassword) {
         alert('Beide Passwörter müssen identisch sein');
         return false;
      }
      saveNewAdmin();
   });
   function saveNewAdmin() {
      jQuery('#save').css('display', 'block');
      var customerID    = $('#jform_customerID').val();
      var salutation    = $('#salutation option:selected').val();
      var first_name    = $('#first_name').val();
      var last_name     = $('#last_name').val();
      var phone         = $('#phone').val();
      var email         = $('#email').val();
      var password      = $('#password').val();
      $('#newAdmin').modal('hide');
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=customer.addAdmin",
         data: {
            customerID:customerID,
            salutation:salutation,
            first_name:first_name,
            last_name:last_name,
            phone:phone,
            email:email,
            password:password
         },
         //dataType: 'json',
         success: function( data ) {
            $('#adminPlaceholder').remove();
            $('#admins').append(
               '<div id="admin' + data + ' class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0 4px 0;">' + 
               '<div class="row">' + 
               '<div class="col-12 col-sm-2">' + 
               '<span class="badge bg-dark text-white mr-2">ID: ' + data + '</span>' +
               '<i title="Administrator bearbeiten" onclick="editAdmin(' + data + ');" class="fa fa-pencil mr-2"></i>'+
               '<i title="Administrator löschen" onclick="deleteAdmin(' + data + ');" class="fa fa-trash-o"></i>' +
               '</div>' +
               '<div class="col-12 col-sm-5">' + 
               salutation + ' ' + first_name + ' ' + last_name + 
               '</div>' +
               '<div class="col-12 col-sm-5">' + 
               '<span class="badge bg-success text-white"><i class="fa fa-phone"></i> ' + phone + '</span> | <span class="badge bg-danger text-white"><i class="fa fa-envelope"></i> ' + email + '</span>' + 
               '</div>' +
               '</div>' +
               '</div>'
            );
            var admin_is   = $('#plan_admins_is').val();
            var admin_rest = $('#plan_admins_rest').val();
            admin_is++;
            admin_rest--;
            $('#plan_admins_is').val(admin_is);
            $('#plan_admins_rest').val(admin_rest);
            $('#salutation option:selected').val('');
            $('#first_name').val('');
            $('#last_name').val('');
            $('#phone').val('');
            $('#email').val('');
            $('#password').val('');
            $('#password_confirm').val('');
            jQuery('#save').css('display', 'none');
         }
      });
   }
   function editAdmin(id) {
      $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=customer.getAdmin",
            data: {
               id:id
            },
            dataType: 'json',
            success: function( data ) {
               $('#edit_salutation').val(data['salutation']);
               jQuery('#edit_salutation').select2({
                  placeholder: 'Bitte auswählen'
               });
               $('#edit_first_name').val(data['first_name']);
               $('#edit_last_name').val(data['last_name']);
               $('#edit_phone').val(data['phone']);
               $('#edit_email').val(data['email']);
               $('#administratorID').val(id);
            }
         });
      $('#editAdmin').modal('show');
   }
   function editNewAdmin() {
      var administratorID    = $('#administratorID').val();
      var salutation    = $('#edit_salutation option:selected').val();
      var first_name    = $('#edit_first_name').val();
      var last_name     = $('#edit_last_name').val();
      var phone         = $('#edit_phone').val();
      var email         = $('#edit_email').val();
      var password      = $('#edit_password').val();
      var funktion      = $('#edit_function').val();
      $('#editAdmin').modal('hide');
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=customer.editAdmin",
         data: {
            administratorID:administratorID,
            salutation:salutation,
            first_name:first_name,
            last_name:last_name,
            phone:phone,
            email:email,
            password:password,
            funktion:funktion
         },
         //dataType: 'json',
         success: function( data ) {
            $('#admin' + administratorID + ' .administratorName').html(salutation + ' ' + first_name + ' ' + last_name);
            $('#admin' + administratorID + ' .administratorPhone').html(phone);
            $('#admin' + administratorID + ' .administratorEmail').html(email);
            $('#editPassword').val('');
            
         }
      });
   }
   function deleteAdmin(id) {
      if(confirm('Soll der Administrator mit der ID ' + id + ' gelöscht werden?') == true) {
         jQuery('#save').css('display', 'block');
         $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=customer.deleteAdmin",
            data: {
               id:id
            },
            //dataType: 'json',
            success: function( data ) {
               $('#admin' + id).remove();
               var admin_is   = $('#plan_admins_is').val();
               var admin_rest = $('#plan_admins_rest').val();
               admin_is--;
               admin_rest++
               $('#plan_admins_is').val(admin_is);
               $('#plan_admins_rest').val(admin_rest);
            }
         });
         jQuery('#save').css('display', 'none');
      }
   }
   function loadPaymentData() {
      var paymentID  = $('#jform_paymentID option:selected').val();
      if(paymentID) {
         $('#jform_paymentID-error').remove();
      }
      if(paymentID == 3) {
         $('#creditCard').fadeIn(200);
         $('#directDebit').fadeOut(100);
         $('#invoice').fadeOut(100);
      }
      if(paymentID == 1) {
         $('#creditCard').fadeOut(100);
         $('#directDebit').fadeOut(100);
         $('#invoice').fadeIn(200);
      }
      if(paymentID == 2) {
         $('#creditCard').fadeOut(100);
         $('#directDebit').fadeIn(200);
         $('#invoice').fadeOut(100);
      }
      if(paymentID == 4) {
         $('#creditCard').fadeOut(100);
         $('#directDebit').fadeOut(200);
         $('#invoice').fadeOut(100);
      }
   }
   $('#jform_company_name').on('keyup', function() {
      $('#jform_company_repeat').val($(this).val());
   });
   $('#jform_address').on('keyup', function() {
      $('#jform_address_repeat').val($(this).val());
   });
   $('#jform_postcode').on('keyup', function() {
      $('#jform_postcode_repeat').val($(this).val());
   });
   $('#jform_city').on('keyup', function() {
      $('#jform_city_repeat').val($(this).val());
   });
</script>