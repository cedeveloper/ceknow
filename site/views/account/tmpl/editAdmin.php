<div id="editAdmin" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 800px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Administrator bearbeiten</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Anrede
               </div>
               <div class="col-12 col-sm-10">
                  <select id="edit_salutation" class="select2-max">
                     <option value="" selected="selected" disabled>Bitte auswählen</option>
                     <option value="Herr">Herr</option>
                     <option value="Frau">Frau</option>
                  </select>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Vorname
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="edit_first_name" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Nachname
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="edit_last_name" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Telefon
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="edit_phone" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  E-Mail
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="edit_email" value="" autocomplete="off" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Funktion
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="edit_function" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Passwort
               </div>
               <div class="col-12 col-sm-10">
                  <input type="password" id="edit_password" value="" autocomplete="off" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label" style="margin-top: -10px;">
                  Passwort Wiederholung
               </div>
               <div class="col-12 col-sm-10">
                  <input type="password" id="edit_password_confirm" value="" autocomplete="off" />
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button id="saveEditAdmin" onclick="editNewAdmin();" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="administratorID" value="" />
         </div>
      </div>
   </div>
</div>