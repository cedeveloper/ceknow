<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/imgForJCE.js');
require_once(JPATH_COMPONENT.'/views/account/tmpl/newAdmin.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a class="btn btn-success text-white" onclick="Joomla.submitform('configuration.simpleSave')">Speichern</a>
         <a class="btn btn-secondary text-white" onclick="Joomla.submitform('configuration.simpleSave')">Speichern & Schließen</a>
      <?php endif;?>
        <a href="<?php echo $this->return;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
    </div>
   <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
         <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Basisdaten</a>
      </li>
      <?php if($this->usergroup == 'superuser' || $this->usergroup == 'customer' || $this->usergroup == 'trainer'):?>
      <li class="nav-item">
         <a class="nav-link" id="plan-tab" data-toggle="tab" href="#plan" role="tab" aria-controls="plan" aria-selected="false">E-Mail</a>
      </li>
      <?php endif; ?>
      <?php if($this->usergroup == 'superuser'):?>
      <li class="nav-item">
         <a class="nav-link" id="asp-tab" data-toggle="tab" href="#asp" role="tab" aria-controls="plan" aria-selected="false">Text Ansprechpartner</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="template-tab" data-toggle="tab" href="#template" role="tab" aria-controls="plan" aria-selected="false">Bestätigungsmail Demozugang</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="template1-tab" data-toggle="tab" href="#template1" role="tab" aria-controls="plan" aria-selected="false">Bestätigungsmail Planänderung</a>
      </li>
      <?php endif; ?>
      <!--<li class="nav-item">
         <a class="nav-link" id="pay-tab" data-toggle="tab" href="#pay" role="tab" aria-controls="pay" aria-selected="false">Zahlungsdaten</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="invoices-tab" data-toggle="tab" href="#invoices" role="tab" aria-controls="invoices" aria-selected="false">Rechnungen</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="false">Administratoren</a>
      </li>-->
      
   </ul>
    <div class="tab-content" id="tabContent">
      <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
         <div class="form-horizontal mt-3 w-50">
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('systemversion'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('systemversion'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('systemdate'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('systemdate'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('systemname'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('systemname'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('systemLink'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('systemLink'); ?>
               </div>
            </div>
            <?php if($this->usergroup == 'superuser'):?>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <label >Testmodus</label>
                  </div>
                  <div class="col-12 col-sm-9">
                     <div class="custom-control custom-switch">
                        <?php
                        $testmode = '';
                        if($this->item->testmode == 1):
                           $testmode = 'checked';
                        endif;
                        ?>
                        <input type="checkbox" class="custom-control-input" <?php echo $testmode;?> id="jform_testmode" name="jform[testmode]">
                        <label class="custom-control-label" for="jform_testmode">Aktivieren oder deaktivieren Sie den globalen Testmodus</label>
                     </div>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('testmode_email'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('testmode_email'); ?>
                  </div>
               </div>
            <?php endif;?>
         </div>
      </div>
      <div class="tab-pane fade" id="plan" role="tabpanel" aria-labelledby="plan-tab">
         <div class="form-horizontal mt-3 col-12 col-sm-6">
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('emailSender'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('emailSender'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('emailSenderName'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('emailSenderName'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('emailBody'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('emailBody'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('emailStyles'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('emailStyles'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('emailScripts'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('emailScripts'); ?>
               </div>
            </div>
         </div>
         <div class="col-12 col-sm-6" id="emailPreview"></div>
      </div>
      <div class="tab-pane fade" id="asp" role="tabpanel" aria-labelledby="asp-tab">
         <div class="form-horizontal mt-3 col-12 col-sm-6">
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('asp'); ?>
               </div>
               <div class="use_editor col-12 col-sm-9" data-editor="jform_asp">
                  <?php 
                  echo $this->form->getInput('asp'); 
                  require(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');
                  ?>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="template" role="tabpanel" aria-labelledby="template-tab">
         <div class="form-horizontal mt-3">
            <div class="row">
               <div class="col-12 col-sm-6">
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('description_confirm_mail'); ?>
                     </div>
                     <div class="col-12 col-sm-10">
                        <?php 
                        echo $this->form->getInput('description_confirm_mail'); 
                        ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('subject_confirm_mail'); ?>
                     </div>
                     <div class="col-12 col-sm-10">
                        <?php echo $this->form->getInput('subject_confirm_mail'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('text_confirm_mail'); ?>
                     </div>
                     <div class="use_editor col-12 col-sm-10" data-editor="jform_text_confirm_mail">
                        <?php 
                        echo $this->form->getInput('text_confirm_mail'); 
                        require(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');
                        ?>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-sm-6">
                   <div class="card bg-light p-1 mb-3">
                     <h4 style="font-size: 24px;">Shortcuts, die Sie in Ihrem Template verwenden können</h4>
                     <ul>
                        <li><b>{salutation}</b> -> Fügt die Anrede des Teilnehmers ein</li>
                        <li><b>{first_name}</b> -> Fügt den Vornamen des Teilnehmers ein</li>
                        <li><b>{last_name}</b> -> Fügt den Nachnamen des Teilnehmers ein</li>
                        <li><b>{email}</b> -> Fügt die E-Mail-Adresse des Teilnehmers ein</li>
                        <li><b>{activationlink}</b> -> Fügt einen codierten Aktivierungslink ein</li>
                        <li><b>{systemname}</b> -> Fügt den Namen des Systems ein</li>
                     </ul>
                  </div>
                  <a class="btn btn-primary text-white" onclick="createTemplate();">Basistemplate laden</a>
               </div>
            </div> 
         </div>
      </div>
      <div class="tab-pane fade" id="template1" role="tabpanel" aria-labelledby="template1-tab">
         <div class="form-horizontal mt-3">
            <div class="row">
               <div class="col-12 col-sm-6">
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('description_plan_mail'); ?>
                     </div>
                     <div class="col-12 col-sm-10">
                        <?php echo $this->form->getInput('description_plan_mail'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('subject_plan_mail'); ?>
                     </div>
                     <div class="col-12 col-sm-10">
                        <?php echo $this->form->getInput('subject_plan_mail'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('text_plan_mail'); ?>
                     </div>
                     <div class="use_editor col-12 col-sm-10" data-editor="jform_text_plan_mail">
                        <?php 
                        echo $this->form->getInput('text_plan_mail');
                        require(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');
                        ?>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-sm-6">
                   <div class="card bg-light p-1 mb-3">
                     <h4 style="font-size: 24px;">Shortcuts, die Sie in Ihrem Template verwenden können</h4>
                     <ul>
                        <li><b>{salutation}</b> -> Fügt die Anrede des Teilnehmers ein</li>
                        <li><b>{first_name}</b> -> Fügt den Vornamen des Teilnehmers ein</li>
                        <li><b>{last_name}</b> -> Fügt den Nachnamen des Teilnehmers ein</li>
                        <li><b>{email}</b> -> Fügt die E-Mail-Adresse des Teilnehmers ein</li>
                        <li><b>{planname}</b> -> Fügt Namen des gewählten Plans ein</li>
                        <li><b>{plandescription}</b> -> Fügt eine Beschreibung zum Plan ein</li>
                     </ul>
                  </div>
                  <a class="btn btn-primary text-white" onclick="createTemplatePlan();">Basistemplate laden</a>
               </div>
            </div> 
         </div>
      </div>
      <?php if($this->usergroup == 'trainer'):?>
         <div class="tab-pane fade" id="theCustomer" role="tabpanel" aria-labelledby="theCustomer-tab">
            <div class="form-horizontal mt-3 w-50">
              
            </div>
         </div>
      <?php endif; ?>
    </div>
<input type="hidden" name="task" value="" />
<input type="hidden" id="usergroup" value="<?php echo $this->usergroup;?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<?php
require_once(JPATH_COMPONENT.'/views/dialogs/imageContent.php');
require_once(JPATH_COMPONENT.'/views/dialogs/formForJCE.php');
?>
<script type="text/javascript">
   let unitID = $('#id').val();
   let classroomID = false;
   let part = 'config';
   $(document).ready(function() {
      var usergroup = $('#usergroup').val();
      if(usergroup == 'customer' || usergroup == 'trainer') {
         $('#jform_systemversion').attr('readonly', true).attr('disabled', 'true');
         $('#jform_systemdate').attr('readonly', true).attr('disabled', 'true');
         $('#jform_systemname').attr('readonly', true).attr('disabled', 'true');
         $('#jform_systemLink').attr('readonly', true).attr('disabled', 'true');
      }
   }); 
   
   function createTemplate() {
      if(confirm("Möchten Sie ein leeres Basistemplate in den Editor laden?") == true) {
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=configuration.loadBasicTemplate",
            data: {},
            //dataType: 'json',
            success: function( data ) {
               if(data) {
                  tinyMCE.activeEditor.execCommand('mceInsertContent',false,data);
               } 
            }
        });
      }
   }
   function createTemplatePlan() {
      if(confirm("Möchten Sie ein leeres Basistemplate in den Editor laden?") == true) {
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=configuration.loadBasicTemplate",
            data: {},
            //dataType: 'json',
            success: function( data ) {
               if(data) {
                  tinyMCE.activeEditor.execCommand('mceInsertContent',false,data);
               } 
            }
        });
      }
   }
</script>