<?php
/**
 *  	DATS Torsten Scheel
 * 	  	JClassroom 1.0
 * 		LMS-System
 * 	 	(C) 2020. All Rights reserved.
 * 		05. April 2020
 *		Development: Torsten Scheel
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class JclassroomViewConfiguration extends JViewLegacy
{
    protected $item;
    protected $form;
    protected $state;
	function display($tpl = null)
	{
        require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        $testmode = LoginHelper::checkTestmode();
        if($testmode):
            echo $testmode;
        endif;
        $session    = JFactory::getSession();
        $document   = JFactory::getDocument();
        $document->setTitle('Einstellungen ['.$session->get('systemname').']');
        // Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
            JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
            $demo = new LoginHelper();
            $demo->checkDemo();
        endif;

        $user = JFactory::getUser();
        $this->state    = $this->get('State');
        $this->item     = $this->get('Item');
        $this->form     = $this->get('Form');
		$session = JFactory::getSession();
        if($session->get('group') == 'superuser'):
            $this->usergroup    = 'superuser';
            $this->return       = 'manager-administrator';
        endif;
        if($session->get('group') == 'customer'):
            $this->usergroup    = 'customer';
            $this->return       = 'manager-customer';
        endif;
        if($session->get('group') == 'trainer'):
            $this->usergroup    = 'trainer';
            $this->return       = 'manager-trainer';
        endif;
        
		// Display the view
		parent::display($tpl);
	}
}
