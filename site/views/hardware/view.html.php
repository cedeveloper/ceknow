<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewHardware extends JViewLegacy {

   	protected $item;
	protected $form;
	protected $state;

    public function display($tpl = null) {

		$user = JFactory::getUser();
		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour 	= 'manager-administrator/hardwares-admin';
				$this->usergroup= 'superuser';
				break;
			case 'customer':
				$this->retour 	= 'manager-customer/hardwares-customer';
				$this->usergroup= 'customer';
				break;
			case 'trainer':
				$this->usergroup= 'trainer';
				break;
			case 'student':

				break;
		}
        parent::display($tpl);
    }
}
