<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
$user       = JFactory::getUser();
?>
<form action="index.php?option=com_jclassroom&task=students.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept=".csv" name="uploadCSV" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <!--<a class="btn btn-success text-white" onclick="Joomla.submitform('student.add')">Neuer Teilnehmer</a>-->
        <a class="btn btn-danger text-white m-1" id="deleteCustomer" onclick="deleteStudents();">Teilnehmer löschen</a>
        <button type="button" class="btn btn-info" onclick="Joomla.submitform('students.saveCSV')">Liste exportieren</button>
        <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right">Zurück</a>
    </div>
    <table class="table table-striped">	
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                <?php if($this->usergroup == 'superuser'):?>
                 <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Customer-ID<br>Kunden-Nr.', 'a.customer_number', $listDirn, $listOrder) ?>
                </th> 
                <?php endif;?>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Name<br/>Firma', 'a.name', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Adresse', 'a.city', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Kontakt', 'a.email', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Status'), 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>		
        <tbody>
        <?php if($this->items) { ?>
            <?php foreach ($this->items as $i => $item) :?>
            <?php //if($item->tblUserID == 1436):?>
                <?php
                $classroomTitle = '';
                $hideTheRow     = 0;
                $hideArray      = array();
                if($item->classroomID):
                    $theRightString = explode('//', $item->classroomID);
                    $classrooms     = array();
                    foreach($theRightString as $string):
                        $theDetails = explode(',', $string);
                        if($theDetails[2] == 0):
                            $hideTheRow = 1;
                            if($theDetails[3] == 1 && ($theDetails[4] == $user->id || $theDetails[5] == $user->id)):
                                $hideTheRow = 0;
                                $classrooms[]   = '(LR'.$theDetails[0].') '.$theDetails[1];
                                $hideArray[]    = $theDetails[0];
                            endif;
                        endif;
                        if($theDetails[2] == 1):
                            $hideTheRow = 1;
                            if($theDetails[4] == $user->id || $this->usergroup == 'customer'):
                                $hideTheRow = 0;
                                $classrooms[] = '(LR'.$theDetails[0].') '.$theDetails[1];
                                $hideArray[]    = $theDetails[0];
                            endif;
                        endif;
                        if($theDetails[2] == 3):
                            $hideTheRow     = 0;
                            $classrooms[]   = '(LR'.$theDetails[0].') '.$theDetails[1];
                            $hideArray[]    = $theDetails[0];
                        endif;
                    endforeach;
                    if($this->usergroup == 'superuser' || $this->usergroup == 'customer'):
                        $hideTheRow = '';
                    endif;
                    $classrooms = array_unique($classrooms);
                    $classroomTitle = implode('<br>', $classrooms);
                else:
                    if($this->usergroup != 'trainer'):
                        $classroomTitle = 'Keine Zuordnung zu Learningrooms gefunden.';
                    else:
                        $hideTheRow = 1;
                    endif;
                endif;
                ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <?php if($hideTheRow == 0 || count($hideArray) > 0): ?>
                	<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
    				<td class="right">
                        <span class="badge badge-primary text-white">SID: <?php echo $this->escape($item->id); ?></span><br>
                        <span class="badge badge-dark text-white">UID: <?php echo $item->tblUserID;?></span>
                    </td>
                    <?php if($this->usergroup == 'superuser'):?>
                    <td class="left">
                        <?php echo 'CU'.str_pad($this->escape($item->customerID),8,'0',STR_PAD_LEFT); ?><br>
                        <?php echo $this->escape($item->customer_number); ?>
                    </td>
                    <?php endif;?>
                    <td>
                        <a href="<?php echo JURI::Root().'student-edit?layout=edit&id='. $item->tblUserID.'&noS'; ?>">
                            <?php
                            if($item->logo) {
                                echo '<img class="mr-2" style="display: inline; height: 40px;" src="'.$item->logo.'" />';
                            }
                            echo $item->usergroup .' '.$item->first_name.' '.$item->last_name.'<br/>';?>
                        </a>
                        <?php     
                            if($item->company_name):
                                echo '<p style="font-size: 14px;margin: 0px;"><b>CO'.str_pad($item->companyID, 8, '0', STR_PAD_LEFT).'</b> '.$item->company_name.'</p>';
                            else:
                                echo '<p style="font-size: 14px;margin: 0px;"><i>Keine Firmenzuordnung</i></p>';
                            endif;
                            echo '<hr style="margin: 2px 0px!important;">';
                            echo '<span style="font-size: 14px;">'.$classroomTitle.'</span>';
                            echo '<br>'.$item->created_by.' '.$item->showto.' '.$item->readRights;
                        ?>
                    </td>
                    <td class="left">
                        <?php echo $this->escape($item->adress); ?><br/>
                        <?php echo $item->postcode.' '.$item->city; ?> 
                    </td>
                    <td class="left">
                        <span class="badge bg-success text-white"><i class="fa fa-phone"></i> <?php echo $this->escape($item->phone); ?></span><br/>
                        <span class="badge bg-primary text-white"><i class="fa fa-mobile"></i> <?php echo $this->escape($item->mobile); ?></span><br/>
                        <span class="badge bg-danger text-white"><i class="fa fa-envelope"></i> <?php echo $this->escape($item->email); ?></span>       
                        <?php if($item->web):?>
                            <br><span class="badge bg-warning text-white"><i class="fa fa-globe"></i> <?php echo $this->escape($item->web); ?></span>
                        <?php endif;?>
                    </td>
                <?php
                if($item->published == 0) {
					$class="badge-danger";
					$wert = "<i class='fa fa-ban'></i>";
				} elseif($item->published == 1) {
					$class="badge-success";
					$wert = "<i class='fa fa-check'></i>";
				}
				?>
                    <td class="text-center">
                    	<span class="btn <?php echo $class;?>"><?php echo $wert;?></span>
                   	</td>
                <?php else: ?>
                    <td colspan="8">Keine Leserechte</td>
                <?php endif; ?>
                </tr>
        <?php //endif;?>
        <?php endforeach ?>
        <?php } else { ?>
        	<tr>
            	<td colspan="8">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
       	<?php } ?>
        </tbody>			
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
jQuery('#openUploader').click(function() {
    jQuery('#uploader').trigger('click');
    
});
jQuery('#uploader').on('change', function() {
    jQuery('#uploaderForm').submit();
});
function deleteStudents() {
    if(confirm('Sollen die ausgewählten Teilnehmer gelöscht werden?') == true) {
        Joomla.submitform('students.delete');
    }
}
</script>