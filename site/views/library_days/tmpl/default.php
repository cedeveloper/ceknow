<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="index.php?option=com_jclassroom&task=students.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept=".csv" name="uploadCSV" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <!--<a class="btn btn-success text-white" onclick="Joomla.submitform('student.add')">Neuer Teilnehmer</a>-->
        <a class="btn btn-danger text-white m-1" id="deleteCustomer" onclick="deleteLibrary();">Thema/Themen löschen</a>
        <!--<button type="button" class="btn btn-info" onclick="Joomla.submitform('students.saveCSV')">Liste exportieren</button>-->
        <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right mb-3">Zurück</a>
    </div>
    <table class="table table-striped">	
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                 <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Name', 'a.title', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    Beschreibung
                </th> 
                <th class="nowrap text-center">
                    Module
                </th> 
                <th class="nowrap text-center">
                    Units
                </th>
                <th class="nowrap left">
                    Erstellt am<br/>Erstellt von
                </th>
                <th class="nowrap left">
                    Geändert am<br/>
                    Geändert von
                </th> 
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Status'), 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>		
        <tbody>
        <?php if($this->items) { ?>
        <?php foreach ($this->items as $i => $item) :
        ?>
            <tr class="row<?php echo $i % 2; ?>">
            	<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
				<td class="right"><?php echo $this->escape($item->id); ?></td>
                <td>
                    <!--<a href="<?php //echo JURI::Root().'library-days-edit?layout=edit&id='. $item->id; ?>">
                        
                        <?php //echo $item->title;?>
                    </a>-->
                    <?php echo $item->title;?>
                </td>
                <td class="left">
                    <small><i><?php echo $this->escape($item->description); ?></i></small> 
                </td>
                <td class="text-center"> 
                    <span class="badge badge-success"><?php echo $item->modules;?></span>                
                </td>
                <td class="text-center">  
                    <span class="badge badge-danger"><?php echo $item->units;?></span>               
                </td>
                <td class="left">
                    <?php echo date('d.m.Y H:i', strtotime($item->created)); ?><br/>
                    <?php echo $this->escape($item->creator); ?>       
                </td>
                <td class="left">
                    <?php 
                    if($item->modified != 0):
                        echo date('d.m.Y H:i', strtotime($item->modified));
                        echo '<br/>'.$this->escape($item->modified_by);
                    endif;
                    ?>
                </td>
                <?php
                if($item->published == 0) {
					$class="badge-danger";
					$wert = "<i class='fa fa-ban text-danger'></i>";
				} elseif($item->published == 1) {
					$class="badge-success";
					$wert = "<i class='fa fa-check text-success'></i>";
				}
				?>
                <td class="text-center"><?php echo $wert;?></td>
            </tr>
        <?php endforeach ?>
        <?php } else { ?>
        	<tr>
            	<td colspan="9">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
       	<?php } ?>
        </tbody>			
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
jQuery('#openUploader').click(function() {
    jQuery('#uploader').trigger('click');
    
});
jQuery('#uploader').on('change', function() {
    jQuery('#uploaderForm').submit();
});
function deleteLibrary() {
    var cID = 0;
    $('[name="cid[]"]').each(function() {
        if($(this).is(':checked')) {
            cID = 1;
        }
    });
    if(cID == 0) {
        alert('Bitte wählen Sie die zu löschenden Einträge aus.');
        return false;
    }
    if(confirm('Sollen die ausgewählten Einträge gelöscht werden?') == true) {
        Joomla.submitform('library_days.delete');
    }
}
</script>