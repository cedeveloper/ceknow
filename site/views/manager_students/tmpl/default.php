<?php
/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

$doc = JFactory::getDocument();
?>
<div class="inflow admin row">
	<div class="col-12">
		<div class="row">
			<div class="col-12">
				<div class="card text-white bg-light mb-3" style="font-size: 12px;">
					<div class="card-body p-1">
						<p class="card-title text-dark"><b>Ihre Kundendaten</b></p>
						<div class="row">
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-4">
											Name:
										</div>
										<div class="col-12 col-sm-8">
											<i><?php echo $this->studentsData->first_name.' '.$this->studentsData->last_name;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-4">
											Kunden-Nr.:
										</div>
										<div class="col-12 col-sm-8">
											<i><?php echo $this->studentsData->customer_number;?></i>
										</div>
									</div>
								</div>
							</div>
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-3">
											Anschrift:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->studentsData->adress;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->studentsData->postcode.' '.$this->studentsData->city;?></i>
										</div>
									</div>
								</div>
							</div>
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-3">
											Telefon:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->studentsData->phone;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
											Fax:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->studentsData->fax;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
											E-Mail:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->studentsData->email;?></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-primary mb-3">
					<div class="card-header">Mein Konto</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier Ihr Konto</h5>
						<p class="card-text">Beschreibung zu Reseller.</p>
						<a href="customer/customer-konto" class="btn btn-secondary">Zu meinem Konto</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-secondary mb-3">
					<div class="card-header">Unternehmen</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier Ihre Unternehmen</h5>
						<p class="card-text">Beschreibung zu Unternehmen.</p>
						<a href="manager-auditoren/auditoren-unternehmen?bn=3" class="btn btn-primary">Zu den Unternehmen</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>