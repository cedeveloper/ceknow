<?php

/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewManager_students extends JViewLegacy {

    public function display($tpl = null) {
        $this->studentsData = $this->get('StudentsData');

        parent::display($tpl);
    }
}
