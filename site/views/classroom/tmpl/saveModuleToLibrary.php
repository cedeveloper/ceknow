<div id="saveModuleToLibrary" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Modul in der Bibliothek speichern</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-horizontal mt-3">
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <label>Name</label>
                  </div>
                  <div class="col-12 col-sm-9">
                     <input id="saveModuleToLibrary_title" type="text" value="" />
                     <small>Passen Sie hier bei Bedarf den Namen des Moduls an</small>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <label>Beschreibung</label>
                  </div>
                  <div class="col-12 col-sm-9">
                     <textarea style="width: 100%;border: 1px solid #e1e1e1;border-radius: 4px;" id="saveModuleToLibrary_description"></textarea>
                     <small>Beschreiben Sie hier bei Bedarf das Modul. Die Beschreibung wird nur intern verwendet.</small>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button onclick="saveModuleToLibraryExecute();" id="newModuleFromLibrary" type="button" class="btn btn-primary">Speichern</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="saveModuleIDToLibrary" value="" />
         </div>
      </div>
   </div>
</div>