<div id="newStudent_dialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 700px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Neuen Teilnehmer einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Anrede
               </div>
               <div class="col-12 col-sm-10">
                  <select id="studentSalutation" class="select2">
                     <option value="" selected="selected" disabled>Bitte auswählen</option>
                     <option value="Herr">Herr</option>
                     <option value="Frau">Frau</option>
                  </select>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Vorname*
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="studentFirstname" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Nachname*
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="studentLastname" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Firma
               </div>
               <div class="col-12 col-sm-10">
                  <?php
                  JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
                  $load 		= new GlobalHelper();
                  $customer 	= $load->getCustomer();
                  ?>
                  <select id="studentCompany" class="select2-max">
                     <option value="" selected="selected" disabled>Bitte auswählen</option>
                     <?php 
                        if($customer['companies']):
                           foreach($customer['companies'] as $companie):
                              echo '<option value="'.$companie->id.'">'.$companie->name.'</option>';
                           endforeach;
                        endif;
                     ?>
                  </select>
                  <!--<input type="text" class="bg-light" readonly="true" disabled="true" id="studentCompany" value="" />-->
                  <input type="hidden" id="studentCompanyID" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Adresse
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="studentAdress" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Postleitzahl
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="studentPostcode" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Ort
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="studentCity" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Telefon
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="studentPhone" value="" />
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  E-Mail*
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="studentEmail" value="" />
                  <small class="d-inline-block" style="line-height: 16px;">Bitte beachten Sie: der Benutzername zur Anmeldung an den Learningroom ist immer identisch zur E-Mail-Adresse des Teilnehmers</small>
               </div>
            </div>
            <!--<div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Passwort
               </div>
               <div class="col-12 col-sm-10">
                  <input type="text" id="studentPassword" value="" />
               </div>
            </div>-->
         </div>
         <div class="modal-footer">
            <button id="newStudentSave" onclick="newStudentSave();" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>