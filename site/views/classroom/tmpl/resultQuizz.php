<div id="resultQuizz" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 90%;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Resultat für Quizz <b><span id="quizzTitle"></span></b> / Teilnehmer <b><span id="quizzStudent"></span></b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">

         </div>
         <div class="modal-footer">
            <a onclick="printResult();" class="btn btn-primary text-white">Auswertung als PDF exportieren</a>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
            <input type="hidden" id="theResultID" value="" />
            <input type="hidden" id="completeEvaluation" value="" />
            <input type="hidden" id="unitID" value="" />
         </div>
      </div>
   </div>
</div>