<div id="editTimeblock" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Timeblock bearbeiten</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <label>Datum</label>
            <input type="text" id="timeblockDayDate" class="datepicker" placeholder="Datum auswählen" />
         </div>
         <div class="modal-body">
            <label>Farbe</label>
            <input type="text" id="timeblockColor" class="colorpicker" placeholder="Titel eingeben" />
         </div>
         <div class="modal-footer">
            <button onclick="saveEditTimeblock();" type="button" class="btn btn-primary">Speichern</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="timeblockID" value="" />
         </div>
      </div>
   </div>
</div>