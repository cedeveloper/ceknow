<div id="moveDay" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Tag auf ein neues Datum verschieben</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <input type="text" class="datepicker" id="moveDayToDay" value="" />
         </div>
         <div class="modal-footer">
            <button onclick="saveMoveDayToDay();" type="button" class="btn btn-primary">Verschieben</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="dayToMove" value="" />
         </div>
      </div>
   </div>
</div>