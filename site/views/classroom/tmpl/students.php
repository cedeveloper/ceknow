<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addStyleSheet('https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.css');
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
$doc->addScript('https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.js');

require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newStudent.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/chooseStudent2.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/comment.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <?php 
	JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
	$load = new GlobalHelper();
	echo $load->getClassroomHeader($this->learningroom, 'Teilnehmerverwaltung');
	?>
   <div class="buttonleiste mb-4">         
      <!--<a class="btn btn-success text-white" onclick="Joomla.submitform('classroom2.simpleSaveStudents')">Speichern</a>
      <a class="btn btn-secondary text-white" onclick="Joomla.submitform('classroom2.saveStudents')">Speichern & Schließen</a>-->
      <a id="retour" href="classroom-edit?layout=global&id=<?php echo $this->item->id;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
      <small>Sie können folgenden Dateiformate hochladen: CSV<br/>
      Die maximale Uploadgröße beträgt 8MB je Upload.<br/>
      Bitte beachten Sie die korrekte Anordnung der Dateifelder in der CSV-Datei.</small>
      <div class="d-block mt-3">
         <a class="btn btn-primary" style="margin-top: -5px;" target="_blank" href="index.php?option=com_jclassroom&task=classroom2.saveCSV&lr=<?php echo $this->item->id;?>">CSV-Liste als Master exportieren</a>
         <button type="button" style="background-color: #ff3600;" class="btn text-white d-inline mb-1"id="openUploaderT">CSV-Upload prüfen</button>
         <button type="button" class="btn btn-warning text-white d-inline mb-1"id="openUploader">Teilnehmer über CSV-Upload erfassen</button>
         <button type="button" onclick="createStudent();" class="btn btn-info text-white d-inline mb-1">Teilnehmer manuell erfassen</button>
         <button type="button" onclick="chooseStudent();" class="btn btn-success text-white d-inline mb-1">Bestehende Personen einfügen </button>
         <a style="padding: 6px 10px;display: inline-block!important;margin-top: -1px;" href="index.php?option=com_jclassroom&task=classroom.printStudentlist&id=<?php echo $this->item->id;?>" target="_blank" class="btn btn-danger text-white d-inline mb-1">Teilnehmerliste erzeugen</a>
      </div>
      <div id="students-actions" class="d-block">
         <button type="button" class="btn btn-secondary text-white d-inline mt-1 mb-3" onclick="sendVerification(1, false);">Verifikation an alle Teilnehmer versenden</button>
         <button type="button" class="btn btn-primary text-white d-inline mt-1 mb-3" onclick="sendInvitation(1, false);">Einladung an alle Teilnehmer versenden</button>
         <button type="button" class="border-dark btn btn-light text-dark d-inline mt-1 mb-3" onclick="sendRemember(1,false);">Erinnerung an alle Teilnehmer versenden</button>
      </div>
      <table id="students" class="table table-striped table-sm">
         <thead>
            <tr>
               <th>Name</th>
               <th style="width: 300px;" class="text-center">Userstate<br/>Bemerkungen</th>
               <th>Gebucht am</th>
               <th>Events</th>
               <th>Aktionen</th>
            </tr>
         </thead>
         <tbody>
            <?php
            if($this->item->students):
               $fileID = 0;
               foreach($this->item->students as $student):
                  echo '<tr id="student'.$student['id'].'" class="student">';
                  switch($student['role']):
                     case 'Customeradmin':
                        $role = '<span class="badge badge-primary text-white">Administrator</span>';
                        break;
                     case 'Customer':
                        $role = '<span class="badge badge-primary text-white">Administrator</span>';
                        break;
                     case 'Students':
                        $role = '<span class="badge badge-success text-white">Teilnehmer</span>';
                        break;
                     case 'Trainer':
                        $role = '<span class="badge badge-danger text-white">Trainer</span>';
                        break;
                     case 'Super Users':
                        $role = '<span class="badge badge-primary text-white">Systemadministrator</span>';
                        break;
                  endswitch;
                  echo '<td>
                  <a href="student-edit?layout=edit&id='.$student['studentID'].'" target="_blank">'.$student['name'].'</a> '.$role.' 
                  <br><i class="fa fa-envelope"></i> '.$student['email'];
                  if($student['company']):
                     echo '<br/>
                     <span style="font-size: 12px;font-weight: bold;text-style:italic;">'.$student['companyID'].' '.$student['company'].'</span>
                     </td>';
                  endif;
                  if($student['password']):
                     $state = '<i class="fa fa-check text-success"></i>';
                  else:
                     $state = '<i class="fa fa-ban text-danger"></i>';
                  endif;
                  echo '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Password besitzt.">';
                  echo$state;
                  echo '<br/><small style="display: inline-block;line-height: 14px;" id="comment'.$student['id'].'">'.$student['comment'].'</small>';
                  echo '</td>';
                  echo '<td>'.date('d.m.Y H:i', strtotime($student['created'])).'</td>';
                  echo '<td>
                  <b style="display: inline-block;width: 230px;">Verifizierung versendet am:</b><span id="vertify_on'.$student['id'].'" class="vertify_on"> '.$student['lastVertify'].'</span><br/>
                  <b style="display: inline-block;width: 230px;">Verifiziert am: </b><span id="verified_on">'.$student['verified_on'].'</span><br/>
                  <b style="display: inline-block;width: 230px;">Einladung versendet am:</b><span id="invitation_on'.$student['id'].'" class="invitation_on"> '.$student['lastInvitation'].'</span><br/>
                  <b style="display: inline-block;width: 230px;">Erinnerung versendet am:</b><span id="remember_on'.$student['id'].'" class="remember_on"> '.$student['lastRemember'].'</span><br/>
                  <b style="display: inline-block;width: 230px;">Zertifikat freigegeben am:</b><span id="certificateGo'.$student['id'].'">'.$student['certificate_on'].'</span><br/>
                  <b style="display: inline-block;width: 230px;">Zertifikat Download am:</b> '.$student['certificate_download_on'].'
                  </td>';
                  echo '<td>';
                  //if(!$student['lastVertify']):
                   echo '<i onclick="sendVerification(0, '. $student['id'].');" title="E-Mail zur Passwortvergabe" class="fa fa-map-marker p-2"></i>';
                  //endif;
                  //if(!$student['lastInvitation']):
                  echo '<i onclick="sendInvitation(0,'. $student['id'].');" title="E-Mail-Einladung" class="fa fa-envelope p-2"></i>';
                  echo '<i onclick="sendRemember(0,'. $student['id'].');" title="E-Mail-Erinnerung" class="fa fa-bell p-2"></i>';
                  echo '<i onclick="comment('. $student['id'].',&quot;'.$student['name'].'&quot;);" title="Bemerkungen" class="fa fa-comment p-2"></i>';
                  if($student['certificate'] == 1):
                     echo '<i id="certificate'.$student['id'].'" onclick="certificateDe('. $student['id'].',&quot;'.$student['name'].'&quot;);" title="Zertifikat blockieren" class="fa fa-certificate p-2 text-success"></i>';
                  else:
                     echo '<i id="certificate'.$student['id'].'" onclick="certificate('. $student['id'].',&quot;'.$student['name'].'&quot;);" title="Zertifikat freigeben" class="fa fa-certificate p-2 text-danger"></i>';
                  endif;
                  
                  //endif;
                  echo '<i onclick="deleteStudent('. $student['id'].',&quot;'.$student['name'].'&quot;);" title="Teilnehmer löschen" class="fa fa-trash-o p-2"></i>
                  </td>';
                  echo '</tr>';
                  $fileID++;
               endforeach;
            else:
                  echo '<tr id="noStudents">';
                  echo '<td colspan="5">Keine Teilnehmer gefunden.</td>';
                  echo '</tr>';
            endif;
            ?>
         </tbody>
      </table>
   </div>
<input type="hidden" id="classroomID" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="company" name="jform[company]" value="<?php echo $this->item->company;?>" />
<input type="hidden" id="companyID" name="jform[companyID]" value="<?php echo $this->item->companyID;?>" />
<input type="hidden" id="jform_verification_template" value="<?php echo $this->item->verification_template;?>" />
<input type="hidden" id="jform_remember_template" value="<?php echo $this->item->remember_template;?>" />
<input type="hidden" id="jform_email_template" value="<?php echo $this->item->email_template;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="students_dialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
         </div>
      </div>
   </div>
</div>
<div id="wait">
   Daten werden geladen
   <img width="80px" src="images/logo_solo.png" />
</div>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<form action="index.php?option=com_jclassroom&task=classroom2.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
   <input id="uploader" title="file input" multiple="true" type="file" accept="" name="uploadCSV" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom2.testCSV" method="post" name="uploaderFormT" id="uploaderFormT" enctype="multipart/form-data">
   <input id="uploaderT" title="file input" multiple="true" type="file" accept="" name="uploadCSVT" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>

<script type="text/javascript">
   $(document).ready(function() {

   });
   $('.colorpicker').spectrum({

   });
   var check_email = false;
   var form = $("#adminForm");
      $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[email]': {
            required: true,
            email: true
         },
         'jform[presentation]': {
            required: true
         }
         
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[presentation]': 'Bitte wählen Sie eine Ausgabeart aus.'
      },
      submitHandler: function(form) {
         var checkIndicator   = 0;
         jQuery('.indicator').each(function() {
            if(jQuery(this).val() == 1) {
               checkIndicator = 1;
            }
         });
         if(checkIndicator == 1) {
            alert('In der Tageskarte gibt es nicht gesicherte Änderungen');
            return false;
         }
         form.submit();
      }
   });
   let classroomID = $('#classroomID').val();
   function deleteStudent(id, name) {
      if(confirm('Soll der Teilnehmer ' + name + ' aus diesem Learningroom entfernt werden?') == true) {
         var classroomID = jQuery('#classroomID').val();
         var cStudents = 0;
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteStudent",
            data: {studentID:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               jQuery('#students tr#student' + id).remove();
               cStudents = $('#students .student').length;
               if(cStudents == 0) {
                  $('#students').append(
                     '<tr id="noStudents">' + 
                     '<td colspan="5">Keine Teilnehmer gefunden.</td>' +
                     '</tr>'
                  );
               }
            }
         });
      }
   }
   function chooseStudent() {
      jQuery('#chooseStudent2').modal('show');
   }
   function chooseStudentSave() {
      jQuery('#chooseStudent2').modal('hide');
      let students   = [];
      $('#chooseStudentList > option:selected').each(function() {
         let set = [$(this).val(), $(this).attr('data-company'), $(this).attr('data-number')];
         students.push(set);
      });
      jQuery('#chooseStudentList').empty();
      let cST = 0;
      $.each(students, function(e, value) {
         $('table#students .student').each(function() {
            var check = $(this).attr('id');
            if('student' + value[0] == check) {
               cST = 1;
            }
         });
      });
      if(cST == 1) {
         alert('Mindestens ein gewählter Teilnehmer ist dem Learningroom bereits zugeordnet.');
         return false;
      }
      $('#noStudents').remove();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.chooseStudent",
         data: {
            classroomID:classroomID,
            students:students
         },
         dataType: 'json',
         success: function( data ) {
            var type = '';
            if(data[0]['id'] == null) {
               alert('Keine Daten zu diesem Teilnehmer gefunden.');
               jQuery('#chooseStudentList').val(null).trigger('change');
               return false;
            }
            jQuery(data).each(function(e, value) { 
               if(value['studentType'] == 'Teilnehmer') {
                  type = '<span class="badge badge-success text-white">Teilnehmer</span>';
               }
               if(value['studentType'] == 'Trainer') {
                 type = '<span class="badge badge-danger text-white">Trainer</span>';
               }
               if(value['studentType'] == 'Systemadministrator') {
                  type = '<span class="badge badge-primary text-white">Systemadministrator</span>';
               }
               if(value['studentType'] == 'Administrator') {
                  type = '<span class="badge badge-primary text-white">Administrator</span>';
               }
               if(value['company']) {
                  company = '<br><span style="font-size: 12px; font-weight: bold";font-style: italic;">' + value['company_number'] + ' ' + value['company'] + '</span>';
               }
               jQuery('#students').append(
                  '<tr id="student' + value['tblUserID'] + '" class="student">' + 
                  '<td><a href="student-edit?layout=edit&id=' + value['tblUserID'] + '" target="_blank">' + value['name'] + '</a> ' + type + 
                  '<br/><i class="fa fa-envelope"></i> ' + value['email'] + ' ' + company + '</td>' + 
                  '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Password besitzt.">' + 
                  value['vertify'] +
                  '<br/><small id="comment' + value['tblUserID'] + '" style="display:inline-block;line-height: 14px;"></span>' +
                  '</td>' + 
                  '<td>' + value['dateOfBooking'] + '</td>' + 
                  '<td><b style="display: inline-block;width: 230px;">Verifizierung versendet am: </b><span id="vertify_on' + value['tblUserID'] + '" class="vertify_on">' + value['dateOfVertify'] + '</span><br/>' +
                     '<b style="display: inline-block; width: 230px;">Verifiziert am: </b><br/>' +
                     '<b style="display: inline-block; width: 230px;">Einladung versendet am:</b><span id="invitation_on' + value['tblUserID'] + '" class="invitation_on">' + value['dateOfInvitation'] + '</span><br/>' +
                     '<b style="display: inline-block; width: 230px;">Erinnerung versendet am:</b><span id="remember_on' + value['tblUserID'] + '" class="remember_on">' + value['dateOfRemember'] + '<br/>' +
                     '<b style="display: inline-block; width: 230px;">Zertifikat freigegeben am:</b><span id="certificateGo' + value['tblUserID'] + '"></span><br/>' + 
                     '<b style="display: inline-block; width: 230px;">Zertifikat Download am:</b>' + 
                  '</td>' + 
                  '<td>' + 
                  '<i class="fa fa-map-marker p-2" onclick="emailVertify(' + value['tblUserID'] + ');" title="E-Mail zur Passwortvergabe"></i>' + 
                  '<i class="fa fa-envelope p-2" onclick="emailInvitation(' + value['tblUserID'] + ');" title="E-Mail Einladung"></i>' + 
                  '<i class="fa fa-bell p-2" onclick="emailRemember(' + value['tblUserID'] + ');" title="E-Mail Erinnerung"></i>' + 
                  '<i onclick="comment(' + value['tblUserID'] + ');" title="Bemerkungen" class="fa fa-comment p-2"></i>' +
                  '<i id="certificate' + value['tblUserID'] + '" onclick="certificate('+ value['tblUserID'] + ',&quot;' + value['name'] + '&quot;);" title="Zertifikat freigeben" class="fa fa-certificate p-2 text-danger"></i>' +
                  '<i class="fa fa-trash-o p-2" onclick="deleteStudent(' + value['tblUserID'] + ',&quot;' + value['name'] + '&quot;);" title="Teilnehmer löschen"></i>' + 
                  '</td>' + 
                  '</tr>'
               );
               jQuery('#chooseCompany').val(null).trigger('change');
            });
            //location.reload();
         }
      });
   }
   function hideSave() {
      setTimeout(hideSaveExecute,800);
   }
   function hideSaveExecute() {
      jQuery('#save').slideUp(200);
   }
 
   function newStudentSave() {
      var classroomID         = jQuery('#classroomID').val();
      var studentSalutation   = jQuery('#studentSalutation option:selected').val();
      var studentFirstname    = jQuery('#studentFirstname').val();
      var studentLastname     = jQuery('#studentLastname').val();
      var studentCompanyID    = jQuery('#studentCompany option:selected').val();
      var studentCompany      = jQuery('#studentCompany option:selected').text();
      var studentAdress       = jQuery('#studentAdress').val();
      var studentPostcode     = jQuery('#studentPostcode').val();
      var studentCity         = jQuery('#studentCity').val();
      var studentPhone        = jQuery('#studentPhone').val();
      var studentEmail        = jQuery('#studentEmail').val();
      var studentPassword     = jQuery('#studentPassword').val();
      if(!studentFirstname) {
         alert('Bitte füllen Sie das Pflichtfeld Vorname (*) aus');
         $('#studentFirstname').focus();
         return false;
      }
      if(!studentLastname) {
         alert('Bitte füllen Sie das Pflichtfeld Nachname (*) aus');
         $('#studentLastname').focus();
         return false;
      }
      if(!studentEmail) {
         alert('Bitte füllen Sie das Pflichtfeld E-Mail (*) aus');
         $('#studentEmail').focus();
         return false;
      }
      var check_email = checkEmail(studentEmail);
      if(check_email == 1) {
         alert('Die E-Mail-Adresse ' + studentEmail + ' ist bereits registriert. Bitte wählen Sie eine andere E-Mail-Adresse oder wählen Sie den bestehenden Teilnehmer mit dieser E-Mail-Adresse aus.');
         return false;
      }
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.newStudent",
         data: {
            classroomID:classroomID,
            studentSalutation:studentSalutation,
            studentFirstname:studentFirstname,
            studentLastname:studentLastname,
            studentCompany:studentCompany,
            studentCompanyID:studentCompanyID,
            studentAdress:studentAdress,
            studentPostcode:studentPostcode,
            studentCity:studentCity,
            studentPhone:studentPhone,
            studentEmail:studentEmail,
            studentPassword:studentPassword
         },
         dataType: 'json',
         success: function( data ) {
            jQuery('#newStudent_dialog').modal('toggle');
            jQuery('#noStudents').remove();
            jQuery('#students').append(
            '<tr id="student' + data['userID'] + '" class="student">' + 
            '<td>' + 
            '<a href="/student-edit?layout=edit&id=' + data['userID'] + '" target="_blank">' + studentFirstname + ' ' + studentLastname + '</a> ' + 
            '<span class="badge ' + data['badge'] + ' text-white">' + data['usergroup'] + '</span><br/><i class="fa fa-envelope"></i> ' +
            studentEmail + '<br/>' +  
            '<span style="font-size: 12px;font-weight: bold;text-style:italic;">' + studentCompany + '</span>' +
            '</td>' +
            '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Passwort besitzt.">' + data['userstate'] + 
            '<small id="comment' + data['userID'] + '" style="display: inline-block;line-height: 14px;"></small>' +
            '</td>' +
            '<td>' + data['created'] + '</td>' +
            '<td>' + '<b style="display: inline-block; width: 230px;">Verifizierung versendet am:</b><span id="vertify_on' + data['userID'] + '" class="vertify_on"></span><br/>' + 
            '<b style="display: inline-block; width: 230px;">Verifiziert am:</b><br/>' +
            '<b style="display: inline-block; width: 230px;">Einladung versendet am:</b><span id="invitation_on' + data['userID'] + '" class="inivitation_on"></span><br/>' + 
            '<b style="display: inline-block; width: 230px;">Erinnerung versendet am:</b><span id="remember_on' + data['userID'] + '" class="remember_on"></span><br/>' + 
            '<b style="display: inline-block; width: 230px;">Zertifikat freigegeben am:</b><span id="certificateGo' + data['userID'] + '"></span><br/>' + 
            '<b style="display: inline-block; width: 230px;">Zertifikat Download am:</b>' + 
            '</td>' +
            '<td>' + 
            '<i onclick="emailVertify(' + data['userID'] + ');" title="E-Mail zur Passwortvergabe" class="fa fa-map-marker p-2"></i>' + 
            '<i onclick="emailInvitation(' + data['userID'] + ');" title="E-Mail-Einladung" class="fa fa-envelope p-2"></i>' + 
            '<i onclick="emailRemember(' + data['userID'] + ');" title="E-Mail-Erinnerung" class="fa fa-bell p-2"></i>' + 
            '<i onclick="comment(' + data['userID'] + ',&quot;' + studentFirstname + ' ' + studentLastname + '&quot;);" title="Bemerkungen" class="fa fa-comment p-2"></i>' + 
            '<i id="certificate' + data['userID'] + '" onclick="certificate('+ data['userID'] + ',&quot;' +studentFirstname + ' ' + studentLastname + '&quot;);" title="Zertifikat freigeben" class="fa fa-certificate p-2 text-danger"></i>' +
            '<i onclick="deleteStudent(' + data['userID'] + ');" title="Teilnehmer löschen" class="fa fa-trash-o p-2"></i>' +
            '</td>' +
            '</tr>'
            );
            jQuery('#studentSalutation').val('').select2({width: '300px'});
            jQuery('#studentFirstname').val('');
            jQuery('#studentLastname').val('');
            jQuery('#studentCompany').val('').select2({width: '100%'});
            jQuery('#studentAdress').val('');
            jQuery('#studentPostcode').val('');
            jQuery('#studentCity').val('');
            jQuery('#studentPhone').val('');
            jQuery('#studentEmail').val('');
            jQuery('#studentPassword').val('');
         }
      });
   }
   function checkEmail(email) {
      var result = '';
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.checkEmail",
         data: {email:email},
         async: false,
         success: function(data) {
            result = data;
         }
      });
      return result;
   }
   function comment(id, name) {
      var comment = $('#comment' + id).text();
      $('#commentStudentID').val(id);
      $('#comment').val(comment);
      $('#studentName').html(name);
      jQuery('#comment_dialog').modal('show');
   }
   function saveComment() {
      var id            = $('#commentStudentID').val();
      var classroomID   = $('#classroomID').val();
      var comment       = $('#comment').val();
      jQuery('#comment_dialog').modal('hide');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.saveComment",
         data: {id:id,comment:comment,classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#comment' + id).html(comment);
         }
      });
   }
   function certificate(id, name) {
      if(confirm('Soll für Teilnehmer ' + name + ' der Zertifikatdownload freigegeben werden?') == true) {
         var classroomID   = $('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.saveCertificate",
            data: {id:id,classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               $('#certificateGo' + id).html(data);
               $('#certificate' + id).toggleClass('text-danger text-success');
               $('#certificate' + id).attr('onclick', 'certificateDe(' + id + ',"' + name + '")');
            }
         });
      }
   }
   function certificateDe(id, name) {
      if(confirm('Soll für Teilnehmer ' + name + ' der Zertifikatdownload blockiert werden?') == true) {
         var classroomID   = $('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.saveCertificateDe",
            data: {id:id,classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               $('#certificateGo' + id).html('');
               $('#certificate' + id).toggleClass('text-success text-danger');
            }
         });
      }
   }
   //Load all persons of the choosen company
   function loadPersons() {
      let companyID  = $('#chooseCompany option:selected').val();
      if(companyID) {
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.loadPersonsOfCompany",
            data: {
               companyID:companyID,
            },
            dataType: 'json',
            success: function( data ) {
               let entry = '';
               $('#chooseStudentList').empty();
               $('#chooseStudentList').append('<option value="" disabled>Bitte auswählen</option>');
               $('#chooseStudentList').append('<optgroup label="Administratoren">');
               jQuery(data).each(function(e, value) { 
                  if(value['function'] == "Administrator") {
                     entry = value['name'] + ' ' + value['companyF'];
                     $('#chooseStudentList').append('<option value="' + value['id'] + '" data-number="' + value['company_number'] + '" data-company="' + value['company'] + '">' + entry + '</option>');
                  }
               });
               $('#chooseStudentList').append('</optgroup">');
               $('#chooseStudentList').append('<optgroup label="Trainer">');
               jQuery(data).each(function(e, value) { 
                  if(value['function'] == "Trainer") {
                     entry = value['name'] + ' ' + value['companyF'];
                     $('#chooseStudentList').append('<option value="' + value['id'] + '" data-number="' + value['company_number'] + '" data-company="' + value['company'] + '">' + entry + '</option>');
                  }
               });
               $('#chooseStudentList').append('</optgroup">');
               $('#chooseStudentList').append('<optgroup label="Teilnehmer">');
               jQuery(data).each(function(e, value) { 
                  if(value['function'] == "Student") {
                     entry = value['name'] + '  ' + value['companyF'];
                     $('#chooseStudentList').append('<option value="' + value['id'] + '" data-number="' + value['company_number'] + '" data-company="' + value['company'] + '">' + entry + '</option>');
                  }
               });
               $('#chooseStudentList').append('</optgroup">');
            }
         });
      }
   }
   function saveCSVMaster() {
      var classroomID = $('#classroomID').val();
      if(classroomID) {
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.saveCSVMaster",
            data: {classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               
            }
         });
      }
   }
   $('#openUploader').click(function() {
      $('#uploader').trigger('click');
      
   });
   $('#uploader').on('change', function() {
      $('#wait').css('display','block');
      $('#uploaderForm').submit();
   });
   $('#openUploaderT').click(function() {
      $('#uploaderT').trigger('click');
      
   });
   $('#uploaderT').on('change', function() {
      $('#wait').css('display','block');
      $('#uploaderFormT').submit();
   });
</script>
