<div id="moveModule" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Modul in ein anderes Thema verschieben</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <select id="moveModuleToDay" class="">
               <option value="" disabled="disabled">Bitte ein Thema auswählen</option>
            </select>
         </div>
         <div class="modal-footer">
            <button onclick="saveMoveModuleToDay();" type="button" class="btn btn-primary">Verschieben</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="moduleToMove" value="" />
         </div>
      </div>
   </div>
</div>