<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
$doc->addScript('components/com_jclassroom/assets/js/imgForJCE.js');

require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newPart.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newPartLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newDayLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newModuleLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newDay.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/moveDay.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/editDay.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/moveModule.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/moveUnit.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/saveDayToLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/saveModuleToLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/saveUnitToLibrary.php');
require_once(JPATH_COMPONENT.'/views/dialogs/imageContent.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <?php 
	JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
	$load = new GlobalHelper();
	echo $load->getClassroomHeader($this->learningroom, 'Inhalte');
	?>
   <div class="buttonleiste mb-4">         
      <input type="text" id="focus" style="opacity: 0;" value="" />
   </div>    
   <div class="form-horizontal mt-3">
      <div class="classroomActions mb-3">
         <a class="btn btn-success text-white" onclick="addNewDay();"><i class="fa fa-plus"></i> Neues Thema einfügen</a>
         <a class="btn btn-info text-white" onclick="addDayFromLibrary();"><i class="fa fa-plus"></i> Thema aus Bibliothek einfügen</a>
         <!--<a class="btn btn-warning text-dark" onclick="addDayToLibrary('<?php //echo $this->item->title;?>');"><i class="fa fa-university"></i> Thema in der Bibliothek speichern</a>-->
         <a id="retour" href="classroom-edit?layout=global&id=<?php echo $this->item->id;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
      </div>
      <div class="classroomDays mb-3">
         <h4 style="font-size: 14px;font-weight: bold;">Themen</h4>
         <?php if($this->item->cards): ?>
            <?php foreach($this->item->cards as $card): ?>
               <?php 
                  $date    = new Datetime($card->day);
                  $title   = '';
                  if($card->title):
                     $title   .= $card->title;
                  else:
                     $title   .=  '<br/>'.$date->format('d.m.Y');
                  endif;
               ?>
               <a 
               id="loadDay<?php echo $card->id;?>" 
               data-date="<?php echo date('d.m.Y', strtotime($card->day));?>" 
               data-title="<?php echo $card->title;?>" 
               class="btn mb-2" 
               onclick="loadDay(<?php echo $this->item->id;?>,<?php echo $card->id;?>);"><?php echo $title;?>
               </a>
            <?php endforeach;?>
         <?php endif; ?>
      </div>
      <?php $unitCounter = 0;?>
      <div id="structure">
         
      </div>
   </div>
<input type="hidden" id="unitCounter" value="<?php echo $unitCounter;?>" />
<input type="hidden" id="dayID" value="" />
<input type="hidden" id="classroomID" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="showTo" value="<?php echo $this->item->showTo;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<?php
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/editContent.php');
?>
<div id="wait">
   Daten werden geladen
   <img width="80px" src="images/logo_solo.png" />
</div>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<form action="index.php?option=com_jclassroom&task=classroom.loadFile" method="post" name="uploaderFormUnit" id="uploaderFormUnit" enctype="multipart/form-data">
   <input id="uploaderUnit" title="file input" type="file" accept="" name="uploadUnit" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="formUnitID" name="unitID" value="" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom.loadIntroImage" method="post" id="uploaderImageUnit" enctype="multipart/form-data">
   <input id="uploadImageUnit" title="file input" type="file" accept="" name="uploadImageUnit" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="imageUnitID" name="imageUnitID" value="" />
</form>

<script type="text/javascript">
let unitID = false;
let classroomID = $('#classroomID').val();
let part       = 'classroom';
$(document).ready(function() {

   //jQuery('.select2').select2({width: '100%'});
   var h1 = jQuery(window).height();
   var w1 = jQuery(window).width();
   jQuery('#editContent .modal-dialog').css('width', w1 -300).css('max-width', '1280px');
   setRights();
});
var check_email = false;
   var form = $("#adminForm");
      $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[email]': {
            required: true,
            email: true
         },
         'jform[presentation]': {
            required: true
         }
         
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[presentation]': 'Bitte wählen Sie eine Ausgabeart aus.'
      },
      submitHandler: function(form) {
         var checkIndicator   = 0;
         jQuery('.indicator').each(function() {
            if(jQuery(this).val() == 1) {
               checkIndicator = 1;
            }
         });
         if(checkIndicator == 1) {
            alert('In der Tageskarte gibt es nicht gesicherte Änderungen');
            return false;
         }
         form.submit();
      }
   });
   function exportLearningroom(id) {
      if(confirm("Soll der Learningroom mit der ID " + id + " als JSON-Datei exportiert werden?") == true) {
         jQuery.ajax({
             url: "index.php?option=com_jclassroom&task=classrooms.export",
             data: {id:id},
             method: 'GET',
             xhrFields: {
                 responseType: 'text'
             },
             //dataType: 'json',
             success: function( data ) {
                 var element = document.createElement('a');
                 //element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                 element.setAttribute('href', data);
                 element.setAttribute('download', 'learningroom' + id + '.json');
                 element.style.display = 'none';
                 document.body.appendChild(element);

                 element.click();

                 document.body.removeChild(element);
             }
         });
      }
   }
   function libraryLearningroom(id) {
      if(confirm("Soll der Learningroom mit der ID " + id + " in der Bibliothek gespeichert werden?") == true) {
         jQuery.ajax({
             url: "index.php?option=com_jclassroom&task=classroom.libraryLearningroom",
             data: {id:id},
             method: 'POST',
             //dataType: 'json',
             success: function( data ) {
                 console.log(data);
             }
         });
      }
   }
   function exportTemplate(id) {
      if(confirm("Soll der Learningroom mit der ID " + id + " als Vorlage gespeichert werden?") == true) {
         jQuery('#wait').css('display', 'block');
         jQuery.ajax({
             url: "index.php?option=com_jclassroom&task=classroom.exportTemplate",
             data: {id:id},
             method: 'POST',
             //dataType: 'json',
             success: function( data ) {
                 alert('Der Learningroom ' + id + ' wurde erfolgreich mit der ID ' + data + ' als Vorlage gespeichert');
             }
         });
         jQuery('#wait').css('display', 'none');
      }
   }
   function loadTemplate() {
      var invitationTemplate = $('#jform_email_template option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadInvitationTemplate",
         data: {invitationTemplate:invitationTemplate},
         //dataType: 'json',
         success: function( data ) {
            $('#templatePreview').html(data);
         }
      });
   }
   function loadTermine() {
      var kursID = jQuery('#jform_inventa_kursID option:selected').val();
      loadKursData(kursID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadTermine",
         data: {kursID:kursID},
         dataType: 'json',
         success: function( data ) {
            if(data[0]['title']) {
               alert(data[0]['title']);
            } else {
               jQuery(data).each(function(e, value) {
                  console.log(value['beginn']);
                  var o = new Option(value['beginn'] + ' - ' + value['ende'], value['id']);
                  /// jquerify the DOM object 'o' so we can use the html method
                  //$(o).html("option text");
                  jQuery("#jform_inventa_terminID").append(o);
               });
            }
         }
      });
   }
   function loadKursData(kursID) {
      tinyMCE.activeEditor.execCommand('mceInsertContent',false,'');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadKursData",
         data: {kursID:kursID},
         //dataType: 'json',
         success: function( data ) {
            tinyMCE.activeEditor.execCommand('mceInsertContent',false,'');
            tinyMCE.activeEditor.execCommand('mceInsertContent',false,data);
         }
      });
   }
   function stageFile(id) {
      if(confirm('Soll diese Datei für die Teilnehmer freigegeben werden?') == true) {
         var classroomID = jQuery('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.stageFile",
            data: {id:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               $('#stageFile_' + id).removeClass('d-inline-block').addClass('d-none');
               $('#unstageFile_' + id).removeClass('d-none').addClass('d-inline-block');
            }
         });
      }
   }
   function unstageFile(id) {
      if(confirm('Soll diese Datei für die Teilnehmer versteckt werden?') == true) {
         var classroomID = jQuery('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.unstageFile",
            data: {id:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               $('#stageFile_' + id).removeClass('d-none').addClass('d-inline-block');
               $('#unstageFile_' + id).removeClass('d-inline-block').addClass('d-none');
            }
         });
      }
   }
   function setRights() {
      var set = $('#jform_showTo option:selected').val();
      if(set != 2) {
         $('#jform_showToUser-lbl').hide();
         $('#jform_showToUser').hide();
         $('#jform_showToUser').next(".select2-container").hide();
      } 
      if(set == 2) {
         $('#jform_showToUser-lbl').show();
         $('#jform_showToUser').next(".select2-container").show();
      }
   }
   jQuery('#retour').on('click', function(e) {
      e.preventDefault();
      var href = jQuery(this).attr('href');
      var checkIndicator   = 0;
      jQuery('.indicator').each(function() {
         if(jQuery(this).val() == 1) {
            checkIndicator = 1;
         }
      });
      if(checkIndicator == 1) {
         alert('In der Tageskarte gibt es nicht gesicherte Änderungen');
         return false;
      }
      location.href = href;
   });
   //ADD SOMETHING
   function addDays() {
      var $this   = jQuery(this);
      var start   = jQuery('#jform_fromDate').val();
      var end   = jQuery('#jform_toDate').val();
      if(!start || !end) {
      alert('Bitte wählen Sie ein Anfangs- und Enddatum für diesen Klassenraum aus.');
      return false;
      }
      jQuery('#structure').empty();
      jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=classroom.addDays",
        data: {start:start, end:end},
        //dataType: 'json',
        success: function( data ) {
          jQuery('#structure').append(data);
        }
      });
   }
   function addNewDay() {
      jQuery('#newDay').modal('show');
   }
   function addDayToLibrary(dayID, name) {
      if(!dayID) {
         alert("Bitte wählen Sie ein Thema aus, das in der Bibliotek gesichert werden soll.");
         return false;
      }
      if(confirm('Soll das Thema ' + name + ' in der Bibliothek gespeichert werden?') == true) {
         jQuery('#wait').css('display', 'block');
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=library.addThemeToLibrary",
            data: {dayID:dayID},
            //dataType: 'json',
            success: function( data ) {
               jQuery('#wait').css('display', 'none');
            }
         });
      }
   }
   function addDayFromLibrary() {
      var dayID  = jQuery('#dayID').val();
      /*if(!dayID) {
         alert("Bitte wählen Sie ein Thema aus, in den die Elemente aus der Bibliotek eingefügt werden sollen.");
         return false;
      }*/
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.addDayFromLibrary",
         data: {dayID:dayID},
         dataType: 'json',
         success: function( data ) {
            $('#addDayFromLibraray').empty();
            if(data) {
               jQuery(data).each(function(e, value) {
                  $("#addDayFromLibraray").append(new Option(value['TID'] + ' ' + value['title'] + ' ( ' + value['description'] + ' )', value['id']));
               });
            }   
            jQuery('#addDayFromLibraray').select2({
              width: '100%'
            });
         }
      });
      jQuery('#newDayLibrary').modal('show');
   }
   function addModuleFromLibrary(dayID) {
      var day = jQuery('#structure .card').length;
      if(day == 0) {
         alert('Bitte laden Sie zunächst ein Thema');
         return false;
      }
      $('#newModuleToDayID').val(dayID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.addModuleFromLibrary",
         data: {},
         dataType: 'json',
         success: function( data ) {
            $('#addModuleFromLibraray').empty();
            if(data) {
               jQuery(data).each(function(e, value) {
                  $("#addModuleFromLibraray").append(new Option(value['title'] + ' [ MID: ' + value['id'] + ' ]', value['id']));
               });
            }   
            jQuery('#addModuleFromLibraray').select2({
              width: '100%',
              tags: true,
              dropdownParent: $("#newModuleLibrary")
            });
         }
      });
      jQuery('#newModuleLibrary').modal('show');
   }
   // Add the choosen day to dayID
   function newDayFromLibraray() {
      jQuery('#wait').css('display', 'flex');
      var classroomID      = $('#classroomID').val();
      var dayID            = $('#dayID').val();
      var insertDayID      = $('#addDayFromLibraray option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.insertDayFromLibrary",
         data: {classroomID:classroomID,dayID:dayID,insertDayID:insertDayID},
         //dataType: 'json',
         success: function( data ) {
            var ret = data.split('_');
            jQuery('.classroomDays').append('<a id="loadDay' + ret[0] + '" class="btn mb-2" onclick="loadDay(' + classroomID + ',' + ret[0] + ');">' + ret[1] + '</a>');
            loadDay(classroomID, ret[0]);
         }
      });
      
      jQuery('#newDayLibrary').modal('hide');
   }
   // Add the choosen module to new dayID
   function newModuleFromLibraray() {
      var classroomID      = $('#classroomID').val();
      var dayID            = $('#dayID').val();
      var insertModuleID   = $('#addModuleFromLibraray option:selected').val();
      var newModuleID      = 0;
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.insertModuleFromLibrary",
         data: {classroomID:classroomID,dayID:dayID,insertModuleID:insertModuleID},
         //dataType: 'json',
         success: function( data ) {
            newModuleID = data;
            renderNewModule(newModuleID);
         }
      });
      
      jQuery('#newModuleLibrary').modal('hide');
   }
   var newModuleID = 0;
   // Add the choosen module to new dayID
   function newUnitFromLibraray() {
      var classroomID      = $('#classroomID').val();
      var insertUnitID     = $('#addUnitFromLibrary option:selected').val();
      newModuleID          = $('#newUnitLibrarayToModuleID').val();
      var dayID            = $('#newUnitLibrarayToDayID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.insertUnitFromLibrary",
         data: {classroomID:classroomID,newModuleID:newModuleID,dayID:dayID,insertUnitID:insertUnitID},
         dataType: 'json',
         success: function( data ) {
            newModuleID = data['moduleID'];
            getType(data['typeID'], 1, data['unitID']);
         }
      });
      
      jQuery('#newPartLibrary').modal('hide');
   }
   function renderNewModule(newModuleID) {
      var classroomID      = $('#classroomID').val();
      var dayID            = $('#dayID').val();
      jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=classroom2.addModule",
        data: {newModuleID:newModuleID,library:1,classroomID:classroomID,dayID:dayID},
        //dataType: 'json',
        success: function( data ) {
            jQuery('#cardFor' + dayID + ' .card-body').append(data);
            jQuery('.select2').select2({width: '300px'});
        }
      });
   }
   function saveNewClassRoomDay() {
      var day     = jQuery('#newClassroomDay').val();
      var title   = jQuery('#newClassroomTitle').val();
      if(!title) {
         alert('Bitte geben Sie einen Titel für das neue Thema ein.');
         return false;  
      }
      var classroomID = jQuery('#classroomID').val();
      jQuery('#newDay').modal('hide');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.addNewDay",
         data: {classroomID:classroomID,day:day,title:title},
         //dataType: 'json',
         success: function( data ) {
            var textR = data.substring(0,7);
            if(textR == 'Fehler:') {
               alert(data);
            } else {
               if(title) {
                  showT = title;
               } else {
                  showT = day;
               }
               jQuery('.classroomDays').append('<a id="loadDay' + data + '" class="btn mb-2" onclick="loadDay(' + classroomID + ',' + data + ');">' + showT + '</a>');
               $('#themes').append(
                  '<a id="theme' + data + '" class="theme mb-2 btn ui-draggable ui-draggable-handle" data-title="' + showT + '" data-ordering="" style="position: relative;" data-id="' + data + '">' + 
                  showT + 
                  '</a>'
               );
               jQuery(".theme").draggable({ 
                  cursor: "crosshair", 
                  revert: "invalid"
               });
            }
            jQuery('#newClassroomDay').val('');
            $('#newClassroomTitle').val('');
         }
      });
   }
   function addModule(id,saveFromLibrary) {
      var dayID            = jQuery('#dayID').val();
      var classroomID      = jQuery('#classroomID').val();
      var copyFromModuleID = jQuery('#addModuleFromLibraray option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.addModule",
         data: {dayID:dayID,classroomID:classroomID,copyFromModuleID:copyFromModuleID,saveFromLibrary: saveFromLibrary},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#cardFor' + dayID + ' .card-body').append(data);
            if(saveFromLibrary == 1) {
                jQuery('#newModuleLibrary').modal('hide');
            }
         }
      });
   }
   function addUnit(moduleID) {
      var dayID   = $('#dayID').val();
      jQuery('.moduleContent').each(function() {
        jQuery(this).removeClass('show').addClass('hide');
        jQuery(this).parent('.module').find('.moduleHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      });
      openModule(moduleID);
      jQuery('#newPartToModuleID').val(moduleID);
      jQuery('#newPartToDayID').val(dayID);
      jQuery('#newPart_dialog').modal();
   }
   function addUnitFromLibrary(moduleID) {
      var dayID   = $('#dayID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.addUnitFromLibrary",
         data: {},
         dataType: 'json',
         success: function( data ) {
            $('#addUnitFromLibrary').empty();
            $('#newPartLibrary .select2').remove();
            if(data) {
               jQuery(data).each(function(e, value) {
                  $("#addUnitFromLibrary").append(new Option(value['BUID'] + ' ' + value['title'] + ' (' + value['unitType'] + ')', value['id']));
               });
            }
            //jQuery("#addUnitFromLibrary").select2("destroy");
            jQuery("#addUnitFromLibrary").select2({width: '100%'});
         }
      });
      jQuery('#newUnitLibrarayToModuleID').val(moduleID);
      jQuery('#newUnitLibrarayToDayID').val(dayID);
      jQuery('#newPartLibrary').modal();
   }   
   function editContent(unitID) {
      var content = jQuery('#unit' + unitID).find(' .contentHTML').html();
      tinyMCE.get('jform_content').setContent(content);
      jQuery('#editContentUnitID').val(unitID);
      jQuery('#editContent #editContentModule').attr('onclick', 'editContentModule(0);');
      jQuery('#editContent').modal();
   }
   function editResolution(unitID) {
      var content = jQuery('#unit' + unitID).find(' .resolutionHTML').html();
      tinyMCE.get('jform_content').setContent(content);
      jQuery('#editContentUnitID').val(unitID);
      jQuery('#editContent #editContentModule').attr('onclick', 'editContentModule(1);');
      jQuery('#editContent').modal();
   }
   function editContentModule(type) {
      //jQuery('#save').fadeIn(200);
      var content = tinyMCE.get('jform_content').getContent();
      var unitID  = $('#editContentUnitID').val();
      if(type == 0) {
         $('#unit' + unitID).find(' .contentHTML').html(content);     
      }
      if(type == 1) {
         $('#unit' + unitID).find(' .resolutionHTML').html(content);
      }
      $('#saveIndicatorUnit' + unitID).val(1);
      saveUnit(unitID);
      $('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
      jQuery('#editContent').modal('hide');
      hideSave();
       //jQuery('#save').fadeOut(200);
   }
   function addTable() {
      var content = '<table style="width: 100%;height:50px;" class="table table-striped table-sm"><head><tr><td>Spalte 1</td><td>Spalte 2</td></tr></thead><tbody><tr><td></td><td></td></tr></tbody></table>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function addTestlevel() {
      var content = '<table style="width: 100%; height: 155px;" class="table table-bordered">' +
      '<tbody>' +
      '<tr>' +
         '<td style="width: 20%;"><strong>Schwierigkeitsgrad</strong></td>' +
         '<td><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></td>' +
      '</tr>' +
      '<tr>' +
         '<td style="width: 20%;"><strong>Übungsinhalte</strong></td>' +
         '<td>' +
            '<ul>' +
               '<li>Ordner erstellen</li>' +
               '<li>Dateien verschieben</li>' +
               '<li>Kommentare eingeben</li>' +
            '</ul>' +
         '</td>' +
      '</tr>' +
      '<tr>' +
         '<td style="width: 20%;"><strong>Übungsdatei</strong></td>' +
         '<td>&nbsp;hotel05.html</td>' +
      '</tr>' +
      '<tr>' +
         '<td style="width: 20%;"><strong>Ergebnisdatei</strong></td>' +
         '<td>hotel05-e.html</td>' +
      '</tr>' +
   '</tbody>' +
   '</table>';
   tinyMCE.get('jform_content').setContent(content);
   }
   function addCode() {
      var content = '<!-- HTML generated using hilite.me --><div style="background: #f8f8f8; overflow:auto;width:auto;font-size:18px;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008000; font-weight: bold">&lt;div</span> <span style="color: #7D9029">id=</span><span style="color: #BA2121">&quot;&quot;</span> <span style="color: #7D9029">class=</span><span style="color: #BA2121">&quot;&quot;</span><span style="color: #008000; font-weight: bold">&gt;&lt;/div&gt;</span></pre></td></tr></table></div>'
      tinyMCE.get('jform_content').setContent(content);
   }
   function addMessage() {
      var content = 
      '<div class="alert alert-success p-1"><p>Message</p></div>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function addWarnung() {
      var content = 
      '<div class="alert alert-danger p-1"><p>Warnung</p></div>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function addInfo() {
      var content = 
      '<div class="alert alert-info p-1"><p>Info</p></div>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function addBox() {
      var content = 
      '<div class="alert p-1" style="background-color: #e1e1e1;border: 1px solid #c1c1c1;"><p>Box</p></div>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function add2Col() {
      var content = '<div class="row">' +
      '<div class="col-12 col-sm-6">' + 
      '<div class="card bg-light p-1">Spalte 1</div>' +
      '</div>' +
      '<div class="col-12 col-sm-6">' +
      '<div class="card bg-light p-1">Spalte 2</div>' +
      '</div>' +
      '</div>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function add3Col() {
      var content = '<div class="row">' +
      '<div class="col-12 col-sm-4">' +
      '<div class="card bg-light p-1">Spalte 1</div>' +
      '</div>' +
      '<div class="col-12 col-sm-4">' +
      '<div class="card bg-light p-1">Spalte 2</div>' +
      '</div>' +
      '<div class="col-12 col-sm-4">' +
      '<div class="card bg-light p-1">Spalte 2</div>' +
      '</div>' +
      '</div>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function add4Col() {
      var content = '<div class="row">' +
      '<div class="col-12 col-sm-3">' + 
      '<div class="card bg-light p-1">Spalte 1</div>' +
      '</div>' +
      '<div class="col-12 col-sm-3">' + 
      '<div class="card bg-light p-1">Spalte 2</div>' +
      '</div>' +
      '<div class="col-12 col-sm-3">' +
      '<div class="card bg-light p-1">Spalte 3</div>' +
      '</div>' +
      '<div class="col-12 col-sm-3">' +
      '<div class="card bg-light p-1">Spalte 4</div>' +
      '</div>' +
      '</div>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function addRow() {
      var content = '<p></p>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function closeEditContentModule() {
      jQuery('#editContent').modal('hide');
   }
   function getType(typ, library, unitID) {
      jQuery('#wait').css('display', 'block');
      var dayID         = jQuery('#dayID').val();
      var moduleID      = jQuery('#newPartToModuleID').val();
      if(moduleID == '') {
         moduleID = newModuleID;
      }
      var unitCounter   = jQuery('#unitCounter').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.getTemplate",
         data: {
            library:library,
            typ:typ, 
            classroomID:classroomID,
            dayID:dayID, 
            moduleID:moduleID, 
            unitCounter:unitCounter,
            unitID:unitID
         },
         //dataType: 'json',
         success: function( data ) {
            jQuery('#module' + moduleID + ' .moduleContent > ul').append(data);
            jQuery('.select2').select2({width: '100%'});
            jQuery('.select2-max').select2({width: '100%'});
            jQuery('#wait').css('display', 'none');
         }
      });
   }
   function chooseStudent() {
      jQuery('#chooseStudent').modal('show');
   }
   //DELETE
   function deleteDay(dayID) {
      if(confirm('Soll dieses Thema gelöscht werden?') == true) {
         jQuery('#wait').css('display', 'flex');
         var classroomID = $('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.deleteDay",
            data: {classroomID:classroomID,dayID:dayID},
            //dataType: 'json',
            success: function( data ) {
               $('#theme' + dayID).remove();
               jQuery('#cardFor' + dayID).remove();
               jQuery('#idFor' + dayID).remove();
               jQuery('#loadDay' + dayID).remove();
               jQuery('#wait').css('display', 'none');
            }
         });
      }
   }
   function deleteModule(moduleID) {
      if(confirm('Soll dieses Modul gelöscht werden?') == true) {
         jQuery('#wait').css('display', 'block');
         jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=classroom2.deleteModule",
           data: {moduleID:moduleID},
           //dataType: 'json',
           success: function( data ) {
             jQuery('#module' + moduleID).remove();
             hideSave();
           }
         });
      }
   }
   function deleteUnit(unitID) {
      if(confirm('Soll diese Unit gelöscht werden?') == true) {
         jQuery('#wait').css('display', 'block');
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.deleteUnit",
            data: {unitID:unitID},
            //dataType: 'json',
            success: function( data ) {
               jQuery('#unit' + unitID).remove();
               jQuery('#wait').css('display', 'none');
            }
         });
      }
   }
   // OPEN AUTOMATIC
   function openDay(id) {
      if(jQuery('#cardFor' + id).hasClass('openDay')) {
         jQuery('#cardFor' + id).removeClass('openDay');
         jQuery('#cardFor' + id + ' .open').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      } else {
         jQuery('#cardFor' + id).addClass('openDay');
         jQuery('#cardFor' + id + ' .open').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      }
   }
   function openModule(id) {
      if(jQuery('#moduleContent' + id).hasClass('hide')) {
         jQuery('#moduleContent' + id).removeClass('hide');
         jQuery('#moduleContent' + id).addClass('show');
         jQuery('#module' + id + ' .moduleHeader .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      } else {
         jQuery('#moduleContent' + id).removeClass('show');
         jQuery('#moduleContent' + id).addClass('hide');
         jQuery('#module' + id + ' .moduleHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      }
   }
   function openUnit(id) {
      if(jQuery('#unitContent' + id).hasClass('openUnit')) {
         jQuery('#unitContent' + id).removeClass('openUnit');
         jQuery('#unit' + id + ' .sectionHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      } else {
         jQuery('#unitContent' + id).addClass('openUnit');
         jQuery('#unit' + id + ' .sectionHeader .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      }
   }
   // HIDE SOMETHING
   function hideDay(dayID) {
      var published  = 0;
      var classHide  = 'fa-eye';
      var text       = '';
      if(jQuery('#cardFor' + dayID + ' .card-header .card-header-action').find('.hide').hasClass('fa-eye-slash')) {
         published   = 1;
         classHide   = 'fa-eye';
         removeClass = 'fa-eye-slash';
         text        = 'Solle dieser Tag aktiviert werden?';
      } else {
         published   = 0;
         classHide   = 'fa-eye-slash';
         removeClass = 'fa-eye';
         text        = 'Solle dieser Tag deaktiviert werden?';
      }
      if(confirm(text) == true) {
         
         jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=classroom.hideDay",
           data: {dayID:dayID,published:published},
           //dataType: 'json',
           success: function( data ) {
             jQuery('#cardFor' + dayID + ' .card-header .card-header-action').find('.hide').removeClass(removeClass).addClass(classHide);
           }
         });
      }
   }
   function hideModule(moduleID) {
      var published  = 0;
      var classHide  = 'fa-eye';
      var text       = '';
      if(jQuery('#module' + moduleID).find('.hide').hasClass('fa-eye-slash')) {
         published   = 1;
         classHide   = 'fa-eye';
         removeClass = 'fa-eye-slash';
         text        = 'Solle dieses Modul aktiviert werden?';
      } else {
         published   = 0;
         classHide   = 'fa-eye-slash';
         removeClass = 'fa-eye';
         text        = 'Solle dieses Modul deaktiviert werden?';
      }
      if(confirm(text) == true) {
         
         jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=classroom.hideModule",
           data: {moduleID:moduleID,published:published},
           //dataType: 'json',
           success: function( data ) {
             jQuery('#module' + moduleID).find('.hide').removeClass(removeClass).addClass(classHide);
           }
         });
      }
   }
   function hideUnit(unitID) {
      var published  = 0;
      var classHide  = 'fa-eye';
      var text       = '';
      if(jQuery('#unit' + unitID).find('.hide').hasClass('fa-eye-slash')) {
         published   = 1;
         classHide   = 'fa-eye';
         removeClass = 'fa-eye-slash';
         text        = 'Solle diese Unit aktiviert werden?';
      } else {
         published   = 0;
         classHide   = 'fa-eye-slash';
         removeClass = 'fa-eye';
         text        = 'Solle diese Unit deaktiviert werden?';
      }
      if(confirm(text) == true) {
         
         jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=classroom.hideUnit",
           data: {unitID:unitID,published:published},
           //dataType: 'json',
           success: function( data ) {
             jQuery('#unit' + unitID).find('.hide').removeClass(removeClass).addClass(classHide);
           }
         });
      }
   }
   // SAVE SOMETHING
   function saveModule(moduleID) {
      jQuery('#save').fadeIn(200);
      var title         = jQuery('#module' + moduleID).find('.moduleTitle').val();
      let description   = $('#module' + moduleID).find('.moduleDescription').val(); 
      if(title.length > 255) {
         hideSave();
         alert("Bitte begrenzen Sie die Anzahl der Zeichen im Modul-Titel auf 255.");
         return false;
      }
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveModule",
         data: {
            moduleID:moduleID,
            title:title,
            description:description
         },
         //dataType: 'json',
         success: function( data ) {
            jQuery('#saveIndicatorModule' + moduleID).val(0);
            jQuery('#module' + moduleID + ' .moduleHeader').find('.save').removeClass('text-danger').addClass('text-success');
            hideSave();
         }
      });
      return true;
   }
   function saveUnit(unitID) {
      jQuery('#save').fadeIn(200);
      var title      = jQuery('#unit' + unitID).find('.title').val();
      var duration   = jQuery('#unit' + unitID).find('.duration').val();
      var content    = jQuery('#unit' + unitID).find('.contentHTML').html();
      var resolution = jQuery('#unit' + unitID).find('.resolutionHTML').html();
      var link       = jQuery('#unit' + unitID).find('.link').val();
      var quizz      = jQuery('#unit' + unitID).find('.quizz option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveUnit",
         data: {
            unitID:unitID,
            title:title,
            duration:duration,
            content:content,
            resolution:resolution,
            link:link,
            quizz:quizz
         },
         //dataType: 'json',
         success: function( data ) {
            jQuery('#saveIndicatorUnit' + unitID).val(0);
            jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-danger').addClass('text-success');
            hideSave();
         }
      });
   }
   function saveDayLibrary(dayID) {
      if(confirm('Soll dieses Thema in Ihrer Bibliotek gespeichert werden?') == true) {
         var title = $('.card').find('.card-header h2').html();
         $('#saveDayToLibrary_title').val(title);
         jQuery('#saveDayToLibrary').modal('show');
         
      }
   }
   function saveDayToLibraryExecute() {
      var dayID = $('#dayID').val();
      var title = $('#saveDayToLibrary_title').val();
      var desc  = $('#saveDayToLibrary_description').val();
      jQuery('#saveDayToLibrary').modal('hide');
      jQuery('#save').fadeIn(200);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.saveDayLibrary",
         data: {dayID:dayID,title:title,desc:desc},
         //dataType: 'json',
         success: function( data ) {
            $('#saveDayToLibrary_title').val('');
            $('#saveDayToLibrary_description').val('');
            hideSave(); 
         }
      });
   }
   function saveModuleLibrary(moduleID) {
      if(confirm('Soll dieses Modul in Ihrer Bibliothek gespeichert werden?') == true) {
         var moduleTitle = $('#module' + moduleID).find('.moduleTitle').val();
         $('#saveModuleIDToLibrary').val(moduleID);
         $('#saveModuleToLibrary_title').val(moduleTitle);
         jQuery('#saveModuleToLibrary').modal('show');
      }
   }
   function saveModuleToLibraryExecute() {
      var moduleID   = $('#saveModuleIDToLibrary').val();
      var title      = $('#saveModuleToLibrary_title').val();
      var desc       = $('#saveModuleToLibrary_description').val();
      jQuery('#saveModuleToLibrary').modal('hide');
      jQuery('#save').fadeIn(200);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.saveModuleLibrary",
         data: {moduleID:moduleID,title:title,description:desc},
         //dataType: 'json',
         success: function( data ) {
            $('#saveModuleToLibrary_title').val('');
            $('#saveModuleToLibrary_description').val('');
            hideSave(); 
         }
      });
   }
   function saveUnitLibrary(unitID) {
      if(confirm('Soll diese Unit in Ihrer Bibliothek gespeichert werden?') == true) {
         $('#saveUnitIDToLibrary').val(unitID);
         var unitTitle = $('#unit' + unitID).find('.title').val();
         $('#saveUnitToLibrary_title').val(unitTitle);
         jQuery('#saveUnitToLibrary').modal('show');
      }
   }
   function saveUnitToLibraryExecute() {
      var unitID     = $('#saveUnitIDToLibrary').val();
      var title      = $('#saveUnitToLibrary_title').val();
      if(title) {
        var desc       = $('#saveUnitToLibrary_description').val();
        jQuery('#saveUnitToLibrary').modal('hide');
        jQuery('#save').fadeIn(200);
        jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=classroom2.saveUnitLibrary",
           data: {unitID:unitID,title:title,description:desc},
           //dataType: 'json',
           success: function( data ) {
              hideSave();
           }
        });
      } else {
        alert('Bitte geben Sie einen Namen für diese Unit ein.');
        return false;
      }
   }
   function chooseStudentSave() {
      jQuery('#chooseStudent').modal('hide');
      var students      = jQuery('#chooseStudentList').val();
      if(students) {
         var set = [];
         jQuery(students).each(function(e, id) {
            set.push(id);
         })
      }
      $('#noStudents').remove();
      var classroomID   = jQuery('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.chooseStudent",
         data: {classroomID:classroomID,students:set},
         dataType: 'json',
         success: function( data ) {
            var type = '';
            if(data[0]['id'] == null) {
               alert('Keine Daten zu diesem Teilnehmer gefunden.');
               jQuery('#chooseStudentList').val(null).trigger('change');
               return false;
            }
            jQuery(data).each(function(e, value) { 
               if(value['studentType'] == 'Teilnehmer') {
                  type = '<span class="badge badge-success text-white">Teilnehmer</span>';
               }
               if(value['studentType'] == 'Trainer') {
                 type = '<span class="badge badge-danger text-white">Trainer</span>';
               }
               if(value['studentType'] == 'Systemadministrator') {
                  type = '<span class="badge badge-primary text-white">Systemadministrator</span>';
               }
               jQuery('#students').append(
                  '<tr id="student' + value['tblUserID'] + '" class="student">' + 
                  '<td><a href="student-edit?layout=edit&id=' + value['tblUserID'] + '" target="_blank">' + value['name'] + '</a> ' + type + 
                  '<br/>' + value['email'] + '</td>' + 
                  '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Password besitzt.">' + 
                  value['vertify'] +
                  '</td>' + 
                  '<td>' + value['dateOfBooking'] + '</td>' + 
                  '<td><b>Verifizierung am: </b>' + value['dateOfVertify'] + '<br/><b>Einladung am:</b>' + value['dateOfInvitation'] + '</td>' + 
                  '<td>' + 
                  '<i class="fa fa-map-marker p-2" onclick="emailVertify(' + value['id'] + ');" title="E-Mail zur Passwortvergabe"></i>' + 
                  '<i class="fa fa-envelope p-2" onclick="emailInvitation(' + value['id'] + ');" title="E-Mail Einladung"></i>' + 
                  '<i class="fa fa-bell p-2" onclick="emailRemember(' + value['id'] + ');" title="E-Mail Erinnerung"></i>' + 
                  '<i onclick="comment(' + value['id'] + ');" title="Bemerkungen" class="fa fa-comment p-2"></i>' +
                  '<i class="fa fa-trash-o p-2" onclick="deleteStudent(' + value['tblUserID'] + ');" title="Teilnehmer löschen"></i>' + 
                  '</td>' + 
                  '</tr>'
               );
               jQuery('#chooseStudentList').val(null).trigger('change');
            });
            //location.reload();
         }
      });
   }
   function hideSave() {
      setTimeout(hideSaveExecute,800);
   }
   function hideSaveExecute() {
      jQuery('#save').slideUp(200);
      jQuery('#wait').slideUp(200);
   }
   // MOVE SOMETHING
   function moveDay(dayID) {
      jQuery('#dayToMove').val(dayID);
      jQuery('#moveDay').modal('show');
   }
   function moveModule(moduleID) {
      var classroomID   = jQuery('#classroomID').val();
      var currentDay    = jQuery('#dayID').val();
      jQuery('#moduleToMove').val(moduleID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadAvailableDays",
         data: {classroomID:classroomID,currentDay:currentDay},
         dataType: 'json',
         success: function( data ) {
            $('#moveModuleToDay').empty();
            if(data) {
               jQuery(data).each(function(e, value) {
                  $("#moveModuleToDay").append(new Option(value['title'], value['id']));
               });
            }
            /*jQuery('#moveDay').modal('hide');
            jQuery('#loadDay' + dayToMove).html(data);
            jQuery('#cardFor' + dayToMove + ' h2').html(data);
            /*jQuery('#unit' + unitToMove).clone().appendTo('#module' + newModuleID + ' ul');
            jQuery('#module' + oldModuleID + ' #unit' + unitToMove).remove();
            jQuery('#unit' + unitToMove).attr('data-moduleID', newModuleID);*/
            /*jQuery('#wait').css('opacity',0);*/
        }   
      });
      jQuery('#moveModule').modal('show');
      jQuery('#moveModuleToDay').select2({width: '100%'});
   }
   function moveUnit(unitID) {
      var currentModuleID = jQuery('#unit' + unitID).attr('data-moduleID');
      var showDialog = 0;
      $('#moveUnitToTheme').empty();
      loadTheThemesOfClassroom();
      jQuery('#moveUnitToModule').empty();
      jQuery('#moveUnitToModule').append(new Option('Bitte auswählen','-1'));
      jQuery('.module').each(function() {
         var id = jQuery(this).attr('id');
         id = id.substring(6);
         if(id != currentModuleID) {
            showDialog = 1;
            var moduleTitle = jQuery(this).find('.moduleTitle').val();
            jQuery('#moveUnitToModule').append(new Option(moduleTitle, id));
         } 
      });
      //if(showDialog == 1) {
         jQuery('#oldModuleID').val(currentModuleID);
         jQuery('#unitToMove').val(unitID);
         jQuery('#moveUnit').modal('show');
      //} else {
         //alert('Keine weiteren Module gefunden');
         //return false;
      //}
   }
   function loadTheThemesOfClassroom() {
      var classroomID = $('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.loadTheThemesOfClassroom",
         data: {classroomID:classroomID},
         dataType: 'json',
         success: function( data ) {
            $('#moveUnitToTheme').append(new Option('Bitte auswählen', '-1'));
            jQuery(data).each(function(e, value) {
               if(value['type'] == 'optiongroupS') {
                   $('#moveUnitToTheme').append('<optgroup label="' + value['title'] + '">');
               }
               if(value['type'] == 'option') {
                  $('#moveUnitToTheme').append(new Option(value['title'], value['id']));
               }
               if(value['type'] == 'optiongroupE') {
                   $('#moveUnitToTheme').append('</optgroup>');
               }
            });
        }   
      });
   }
   function saveMoveUnitToModule() {
      jQuery('#wait').css('opacity', 1);
      var unitToMove    = jQuery('#unitToMove').val();
      var oldModuleID   = jQuery('#oldModuleID').val();
      var newModuleID   = jQuery('#moveUnitToModule option:selected').val();
      var newThemeModuleID   = jQuery('#moveUnitToTheme option:selected').val();
      var moveModuleID  = 0;
      if(newModuleID != -1) {
         moveModuleID = newModuleID;
      } 
      if(newThemeModuleID != -1) {
         moveModuleID = newThemeModuleID;
      }
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.moveUnitToModule",
         data: {unitToMove:unitToMove,newModuleID:moveModuleID},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#moveUnit').modal('hide');
            jQuery('#unit' + unitToMove).clone().appendTo('#module' + newModuleID + ' ul');
            jQuery('#module' + oldModuleID + ' #unit' + unitToMove).remove();
            jQuery('#unit' + unitToMove).attr('data-moduleID', newModuleID);
            jQuery('#wait').css('opacity',0);
        }   
      });
   }
   function saveMoveDayToDay() {
      jQuery('#wait').css('opacity', 1);
      var dayToMove     = jQuery('#dayToMove').val();
      var moveDayToDay  = jQuery('#moveDayToDay').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.moveDayToDay",
         data: {dayToMove:dayToMove,moveDayToDay:moveDayToDay},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#moveDay').modal('hide');
            jQuery('#loadDay' + dayToMove).html(data);
            jQuery('#cardFor' + dayToMove + ' h2').html(data);
            /*jQuery('#unit' + unitToMove).clone().appendTo('#module' + newModuleID + ' ul');
            jQuery('#module' + oldModuleID + ' #unit' + unitToMove).remove();
            jQuery('#unit' + unitToMove).attr('data-moduleID', newModuleID);*/
            jQuery('#wait').css('opacity',0);
        }   
      });
   }
   function saveMoveModuleToDay() {
      jQuery('#wait').css('opacity', 1);
      var moduleToMove     = jQuery('#moduleToMove').val();
      var moveModuleToDay  = jQuery('#moveModuleToDay option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.moveModuleToDay",
         data: {moduleToMove:moduleToMove,moveModuleToDay:moveModuleToDay},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#moveModule').modal('hide');
            jQuery('#module' + moduleToMove).remove();
            jQuery('#wait').css('opacity',0);
        }   
      });
   }
   // LOAD SOMETHING
   function loadDay(classroomID, dayID) {
    $('#focus').focus();
      jQuery('#wait').css('display', 'flex');
      jQuery('#dayID').val(dayID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.loadDay",
         data: {dayID:dayID, classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#structure').empty();
            jQuery('#structure').append(data);
            //jQuery('.unit .select2').select2({width: '100%'});
            jQuery('.unit .select2-max').select2({width: '100%'});
            jQuery('.classroomDays a').each(function() {
               jQuery(this).removeClass('btn-success').addClass('btn-light');
            });
            jQuery('#loadDay' + dayID).removeClass('btn-light').addClass('btn-success');
            openDay(dayID);
            //jQuery('.summernote').summernote();
            jQuery( ".moduleContent > ul" ).sortable({
               connectWith: '.li',
               handle: '.move',
               axis: 'y',
               update: function (event, ui) {
                  var nodes = jQuery(this).context.childNodes;
                  var order = [];
                  jQuery(nodes).each(function(e) {
                     var id   = jQuery(this).context.id;
                     id       = id.substring(4);
                     order.push(e + '_' + id);
                  });
                  jQuery.ajax({
                     type: "POST",
                     url: "index.php?option=com_jclassroom&task=classroom.writeSortableUnits",
                     data: {order:order},
                     //dataType: 'json',
                     success: function( data ){
                        console.log(data);
                     }
                  });
               }
            });
            jQuery( ".card-body" ).sortable({
               handle: '.move',
               axis: 'y',
               update: function (event, ui) {
                  var nodes = jQuery(this).context.childNodes;
                  var order = [];
                  jQuery(nodes).each(function(e) {
                     var id   = jQuery(this).context.id;
                     id       = id.substring(6);
                     order.push(e + '_' + id);
                  });
                  jQuery.ajax({
                     type: "POST",
                     url: "index.php?option=com_jclassroom&task=classroom.writeSortableModules",
                     data: {order:order},
                     //dataType: 'json',
                     success: function( data ){
                        console.log(data);
                     }
                  });
               }
            });
            jQuery('#wait').css('display','none');

         }
      });
   }
   // CHECK SOMETHING
   jQuery(document).on('change','.moduleTitle', function() {
      var moduleID = jQuery(this).attr('data-id');
      var check = saveModule(moduleID);
      if(check == true) {
         jQuery('#saveIndicatorModule' + moduleID).val(1);
         jQuery('#module' + moduleID + ' .moduleHeader').find('.save').removeClass('text-success').addClass('text-danger');
      }
   });
   jQuery(document).on('change','.moduleDescription', function() {
      var moduleID = $(this).attr('data-id');
      var check = saveModule(moduleID);
      if(check == true) {
         jQuery('#saveIndicatorModule' + moduleID).val(1);
         jQuery('#module' + moduleID + ' .moduleHeader').find('.save').removeClass('text-success').addClass('text-danger');
      }
   });
   jQuery(document).on('change','.title', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   jQuery(document).on('change','.duration', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   jQuery(document).on('change','.link', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   jQuery(document).on('change','.quizz', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   jQuery(document).on('summernote.change','.summernote', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   function chooseElement(id) {
      $('#newPart_dialog .modal-dialog .card').each(function() {
         $(this).removeClass('selected');
      });
      $('#newUnitType').val(id);
      $('#newPart_dialog #card' + id).addClass('selected');
   }
   $('#newUnitSave').on('click', function() {
      var type       = $('#newUnitType').val();
      if(!type) {
         alert('Bitte wählen Sie einen Unittypen aus.');
         return false;
      }
      jQuery('#newPart_dialog').modal('hide');
      getType(type,0,0);
   });
   function createStudent() {
      var companyID  = $('#jform_companyID option:selected').val();
      var companyName= $('#jform_companyID option:selected').text();
      $('#studentCompanyID').val(companyID);
      $('#studentCompany').val(companyName);
      jQuery('#newStudent_dialog').modal('show');
   }
   jQuery('#newStudentSave').on('click', function() {
      var classroomID         = jQuery('#classroomID').val();
      var studentSalutation   = jQuery('#studentSalutation option:selected').val();
      var studentFirstname    = jQuery('#studentFirstname').val();
      var studentLastname     = jQuery('#studentLastname').val();
      var studentCompanyID    = jQuery('#studentCompanyID').val();
      var studentCompany      = jQuery('#studentCompany').val();
      var studentAdress       = jQuery('#studentAdress').val();
      var studentPostcode     = jQuery('#studentPostcode').val();
      var studentCity         = jQuery('#studentCity').val();
      var studentPhone        = jQuery('#studentPhone').val();
      var studentEmail        = jQuery('#studentEmail').val();
      var studentPassword     = jQuery('#studentPassword').val();
      if(!studentFirstname || !studentLastname || !studentEmail) {
         alert('Bitte füllen Sie alle Pflichtfelder (*) aus');
         return false;
      }
      var check_email = checkEmail(studentEmail);
      if(check_email == 1) {
         alert('Die E-Mail-Adresse ' + studentEmail + ' ist bereits registriert. Bitte wählen Sie eine andere E-Mail-Adresse oder wählen Sie den bestehenden Teilnehmer mit dieser E-Mail-Adresse aus.');
         return false;
      }
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.newStudent",
         data: {
            classroomID:classroomID,
            studentSalutation:studentSalutation,
            studentFirstname:studentFirstname,
            studentLastname:studentLastname,
            studentCompany:studentCompany,
            studentCompanyID:studentCompanyID,
            studentAdress:studentAdress,
            studentPostcode:studentPostcode,
            studentCity:studentCity,
            studentPhone:studentPhone,
            studentEmail:studentEmail,
            studentPassword:studentPassword
         },
         dataType: 'json',
         success: function( data ) {
            jQuery('#newStudent_dialog').modal('toggle');
            jQuery('#noStudents').remove();
            jQuery('#students').append(
            '<tr id="student' + data['userID'] + '" class="student">' + 
            '<td>' + 
            '<a href="/student-edit?layout=edit&id=' + data['userID'] + '" target="_blank">' + studentFirstname + ' ' + studentLastname + '</a> ' + 
            '<span class="badge ' + data['badge'] + ' text-white">' + data['usergroup'] + '</span><br/>' +
            studentEmail + '<br/>' +  
            '<span style="font-size: 12px;font-weight: bold;text-style:italic;">' + studentCompany + '</span>' +
            '</td>' +
            '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Passwort besitzt.">' + data['userstate'] + 
            '<small id="comment' + data['userID'] + '" style="display: inline-block;line-height: 14px;"></small>' +
            '</td>' +
            '<td>' + data['created'] + '</td>' +
            '<td>' + '<b>Verifizierung am:</b> <br/><b>Einladung am:</b>' + '</td>' +
            '<td>' + 
            '<i onclick="emailVertify(' + data['userID'] + ');" title="E-Mail zur Passwortvergabe" class="fa fa-map-marker p-2"></i>' + 
            '<i onclick="emailInvitation(' + data['userID'] + ');" title="E-Mail-Einladung" class="fa fa-envelope p-2"></i>' + 
            '<i onclick="emailRemember(' + data['userID'] + ');" title="E-Mail-Erinnerung" class="fa fa-bell p-2"></i>' + 
            '<i onclick="comment(' + data['userID'] + ');" title="Bemerkungen" class="fa fa-comment p-2"></i>' + 
            '<i onclick="deleteStudent(' + data['userID'] + ');" title="Teilnehmer löschen" class="fa fa-trash-o p-2"></i>' +
            '</td>' +
            '</tr>'
            );
            jQuery('#studentSalutation').val('');
            jQuery('#studentFirstname').val('');
            jQuery('#studentLastname').val('');
            jQuery('#studentCompanyID').val('');
            jQuery('#studentCompany').val('');
            jQuery('#studentAdress').val('');
            jQuery('#studentPostcode').val('');
            jQuery('#studentCity').val('');
            jQuery('#studentPhone').val('');
            jQuery('#studentEmail').val('');
            jQuery('#studentPassword').val('');
         }
      });
   });
   function checkEmail(email) {
      var result = '';
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.checkEmail",
         data: {email:email},
         async: false,
         success: function(data) {
            result = data;
         }
      });
      return result;
   }
   function comment(id) {
      var comment = $('#comment' + id).text();
      $('#commentStudentID').val(id);
      $('#comment').val(comment);
      jQuery('#comment_dialog').modal('show');
   }
   $('#saveComment').on('click', function() {
      var id            = $('#commentStudentID').val();
      var classroomID   = $('#classroomID').val();
      var comment       = $('#comment').val();
      jQuery('#comment_dialog').modal('hide');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveComment",
         data: {id:id,comment:comment,classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#comment' + id).html(comment);
         }
      });
   });
   function editDayTitle(id) {
      var comment = $('#cardFor' + id).find('.dayTitle').text();
      var date    = $('#loadDay' + id).attr('data-date');
      $('#titleDayID').val(id);
      $('#newDayDate').val(date);
      $('#newDayTitle').val(comment);
      jQuery('#dayTitle').modal('show');
   }
   function saveNewClassRoomTitle() {
      var id            = $('#titleDayID').val();
      var classroomID   = $('#classroomID').val();
      var title         = $('#newDayTitle').val();
      var date          = $('#newDayDate').val();
      jQuery('#dayTitle').modal('hide');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveDayTitle",
         data: {id:id,title:title,date:date,classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#cardFor' + id).find('.dayTitle').html(title);
            $('#loadDay' + id).html(title);
            $('#newDayTitle').val('');
         }
      });
   }

   jQuery('#openUploader').click(function() {
      jQuery('#uploader').trigger('click');
      
   });
   jQuery('#uploader').on('change', function() {
      jQuery('#wait').css('display','block');
      jQuery('#uploaderForm').submit();
   });
   function uploadUnit(unitID) {
      jQuery('#formUnitID').val(unitID);
      jQuery('#uploaderUnit').trigger('click');
   }
   function introImageUnit(unitID) {
      let countImages = $('#introImage' + unitID + ' .file').length;
      if(countImages < 1) {
         $('#imageUnitID').val(unitID);
         $('#uploadImageUnit').trigger('click');
      } else {
         alert('Es kann nur ein Einleitungsbild geladen werden.');
         return false;
      }
      
   }
   
   jQuery('#uploaderUnit').on('change', function(e) {
      var classroomID   = jQuery('#classroomID').val();
      var unitID        = jQuery('#formUnitID').val();
      var theFiles      = jQuery(this)[0].files;
      var theFileName   = jQuery('#uploaderUnit')[0].files[0].name;
      var form_data     = new FormData();
      for(i = 0; i <= theFiles.length; i++) {
         
      }
      form_data.append('uploadUnit', jQuery('#uploaderUnit')[0].files[0]);
      form_data.append('classroomID', classroomID);
      form_data.append('unitID', unitID);
      $('#save').modal('show');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.loadFile",
         data: form_data,
         processData: false,
         contentType: false,
         //dataType: 'json',
         success: function( data ) {
            var dataW = data.split('__');
            jQuery('#uploads' + unitID).append('<div id="file' + dataW[0] + '" class="row mb-1">' + 
            '<div class="col-12">' + 
               '<div class=" fileRow">' + 
                  '<div class="row">' +
                     '<div class="col-12 col-sm-10">' + 
                        '<a target="_blank" style="font-size: 12px;" href="' + dataW[1] + '">' + dataW[0] + '_' + theFileName + '</a>' + 
                     '</div>' +
                     '<div class="text-right col-12 col-sm-2">' + 
                        '<i class="fa fa-trash-o bg-danger text-white p-1" style="cursor: pointer;border-radius: 3px;" onclick="deleteFile(' + dataW[0] + ');"></i>' +
                     '</div>' +
                  '</div>' +
               '</div>' +
            '</div>'
            );
             $('#save').modal('hide');
         }
      });
   });
   jQuery('#uploadImageUnit').on('change', function(e) {
      var classroomID   = jQuery('#classroomID').val();
      var unitID        = jQuery('#imageUnitID').val();
      var theFiles      = jQuery(this)[0].files;
      var theFileName   = jQuery('#uploadImageUnit')[0].files[0].name;
      var form_data     = new FormData();
      for(i = 0; i <= theFiles.length; i++) {
         
      }
      $('#save').modal('show');
      form_data.append('uploadImageUnit', jQuery('#uploadImageUnit')[0].files[0]);
      form_data.append('classroomID', classroomID);
      form_data.append('unitID', unitID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadImageFile",
         data: form_data,
         processData: false,
         contentType: false,
         //dataType: 'json',
         success: function( data ) {
            $('#save').modal('hide');
            var dataW = data.split('__');
            jQuery('#introImage' + unitID).append('<div id="file' + dataW[0] + '" class="row mb-1">' + 
            '<div class="col-12">' + 
               '<div class=" fileRow">' + 
                  '<div class="row">' +
                     '<div class="col-12 col-sm-10">' + 
                        '<a target="_blank" style="font-size: 12px;" href="' + dataW[1] + '">' + 
                        '<div class="introImageThumb" style="background-image: URL(' + dataW[1] + ');"></div>' +
                        theFileName + '</a>' + 
                     '</div>' +
                     '<div class="text-right col-12 col-sm-2">' + 
                        '<i class="fa fa-trash-o bg-danger text-white p-1" style="cursor: pointer;border-radius: 3px;" onclick="deleteImageFile(' + dataW[0] + ');"></i>' +
                     '</div>' +
                  '</div>' +
               '</div>' +
            '</div>'
            );
           
         }
      });
   });
   
   function deleteFile(fileID) {
      if(confirm("Soll diese Datei gelöscht werden?") == true) {
         $('#save').modal('show');
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteFile",
            data: {fileID:fileID},
            //dataType: 'json',
            success: function( data ) {
               jQuery('#file' + fileID).remove();
                $('#save').modal('hide');
            }
         });
      }
   }
function addTimeblock() {
   var classroomID = $('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.addTimeblock",
      data: {classroomID:classroomID},
      //dataType: 'json',
      success: function( data ) {
         var newTimeblock = jQuery('<div id="timeblock' + data + '" class="timeblock">' + 
         '<span class="theDate"><span class="theDateA"></span><br/>' + 
         '<i class="fa fa-pencil mr-2" onclick="editTimeblock(' + data + ');"></i>' + 
         '<i class="fa fa-trash-o" onclick="deleteTimeblock(' + data + ');"></i></span>' +
         '</div>').appendTo('#timeblocks');
         newTimeblock.droppable({
            accept: ".theme", 
            drop: function(event, ui) {
               $(this).removeClass("border").removeClass("over");
               var dropped = ui.draggable;
               var droppedOn = $(this);
               jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);   
               //addThemeToTimeblock();  
            },
            over: function(event, elem) {
               $(this).addClass("over");
            },
            out: function(event, elem) {
               $(this).removeClass("over");
            }
         });
      }
   });
   
}
var dragFrom = '';
var dragX = 0;
var dragY = 0;
jQuery('.timeblock').droppable({
   accept: ".theme", 
   drop: function(event, ui) {
      $(this).removeClass("border").removeClass("over");
      var dropped = ui.draggable;
      var droppedOn = $(this);
      if(dragFrom == 'themes') {  
         
         jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
         addThemeToTimeblock(ui.draggable.attr('id'),$(this));  
      }  else {
         var classroomID = $('#classroomID').val();
         var parent  = droppedOn;
         var id      = parent.attr('id');
         jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
         var order = jQuery('#' + id).sortable("toArray");
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.writeSortableTimeblocks",
            data: {classroomID:classroomID,order:order},
            //dataType: 'json',
            success: function( data ){
               console.log(data);
            }
         });
      }
   },
   over: function(event, elem) {
      $(this).addClass("over");
   },
   out: function(event, elem) {
      $(this).removeClass("over");
   }
});


jQuery(".theme").draggable({ 
   cursor: "crosshair", 
   revert: "invalid",
   start: function(event, ui) {
      dragX = ui.offset.left;
      dragY = ui.offset.top;
      var parent = jQuery(this).parent().attr('id');
      if(parent == 'themes') {
         dragFrom = 'themes';
      } else {
         dragFrom = 'timeblocks';
      }
   }
});
jQuery(".timeblock").sortable({
   update: function (event, ui) {
      console.log(ui);
      var order = jQuery('.classroomDays').sortable("toArray");
      console.log(order);
      /*
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.writeSortableThemes",
         data: {order:order},
         //dataType: 'json',
         success: function( data ){
            console.log(data);
         }
      });*/
   }
});

jQuery('.classroomDays').sortable({
   update: function (event, ui) {
      var order = jQuery('.classroomDays').sortable("toArray");
      console.log(order);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.writeSortableThemes",
         data: {order:order},
         //dataType: 'json',
         success: function( data ){
            console.log(data);
         }
      });
   }
});
jQuery("#themes").droppable({ 
   accept: ".theme", 
   drop: function(event, ui) {
      $(this).removeClass("border").removeClass("over");
      var dropped = ui.draggable;
      var droppedOn = $(this);
      jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);      
      var $wrapper = $('#themes');

      $wrapper.find('.theme').sort(function(a, b) {
          return +a.dataset.ordering - +b.dataset.ordering;
      })
      .appendTo($wrapper);
      deleteThemeFromTimeblock(ui.draggable.attr('id'),$(this));
   },
   over: function(event, elem) {
      $(this).addClass("over");
   },
   out: function(event, elem) {
      $(this).removeClass("over");
   }
});
function addThemeToTimeblock(droppedID,droppedTo) {
   var classroomID = $('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.addThemeToTimeblock",
      data: {classroomID:classroomID,droppedID:droppedID,droppedTo:droppedTo.attr('id')},
      //dataType: 'json',
      success: function( data ) {
         console.log(data);
      }
   });
}
function deleteThemeFromTimeblock(droppedID,droppedTo) {
   var classroomID = $('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.deleteThemeFromTimeblock",
      data: {classroomID:classroomID,droppedID:droppedID,droppedTo:droppedTo.attr('id')},
      //dataType: 'json',
      success: function( data ) {
         console.log(data);
      }
   });
}
function editTimeblock(id) {
   var dayDate    = $('#timeblock' + id).attr('data-title');
   var color      = $('#timeblock' + id).attr('data-color');
   $('#timeblockID').val(id);
   $('#timeblockDayDate').val(dayDate);
   $('#timeblockColor').val(color);
   jQuery('#editTimeblock').modal('show');
}
function saveEditTimeblock() {
   var timeblockID   = $('#timeblockID').val();
   var dayDate       = $('#timeblockDayDate').val();
   var color         = $('#timeblockColor').val();
    if(!dayDate || !color) {
      alert('Bitte wählen Sie ein Datum und eine Darstellungsfarbe aus.');
      return false;
   }
   $('#timeblock' + timeblockID).attr('data-color',color).attr('data-title',dayDate);
   jQuery('#editTimeblock').modal('hide');
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.saveEditTimeblock",
      data: {dayDate:dayDate,color:color,timeblockID:timeblockID},
      //dataType: 'json',
      success: function( data ) {
         $('#timeblock' + timeblockID).find('.theDateA').html(dayDate);
         $('#timeblock' + timeblockID).css('background-color', color);
      }
   });
}
function deleteTimeblock(id) {
   if(confirm('Soll der Zeitblock ' + id + ' gelöscht werden?') == true) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.deleteTimeblock",
         data: {timeblockID:id},
         //dataType: 'json',
         success: function( data ) {
            $('#timeblock' + id).remove();
         }
      });
   }
}
function reorderRoom() {
   var classroomID = $('#classroomID').val();
   if(confirm('Soll die Struktur neu berechnet werden?') == true) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.reorderRoom",
         data: {classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
           
         }
      });
   }
}
</script>
