<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
/*echo '<pre>';
$arr = array('bin','can','bin','bin',4);
$arr = array_count_values($arr);
foreach($arr as $key => $val):
	if(!is_numeric($key)):
	$result[] = $key.' '.$val;
	endif;
endforeach;
print_r($result);
echo '</pre>';*/
?>

<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
	<?php 
	JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
	$load = new GlobalHelper();
	echo $load->getClassroomHeader($this->learningroom, 'Global');
	?>
	<div id="actions" class="d-inline-block w-100 mb-3">
		<?php if($this->item->id):?>
			<a class="btn btn-primary text-white" href="stage-classroom?id=<?php echo $this->item->id;?>&preview=1" target="_blank"><i class="fa fa-eye"></i> Vorschau</a>
			<a id="exportLearningroom" onclick="exportLearningroom(<?php echo $this->item->id;?>);" class="btn btn-warning text-white"><i class="fa fa-upload"></i> Exportieren</a>
			<a id="exportLearningroom" onclick="libraryLearningroom(<?php echo $this->item->id;?>);" class="btn btn-info text-white" title="Speichern Sie den vollständigen Learningroom in der Bibliothek. Legen Sie fest, wer diesen Learningroom in der Bibliothek verwenden darf."><i class="fa fa-university"></i> In Bibliothek speichern</a>
			<?php if($this->usergroup == 'superuser'):?>
				<a id="exportTemplate" onclick="exportTemplate(<?php echo $this->item->id;?>);" class="btn btn-light border-dark  text-dark" title="Speichern Sie den vollständigen Learningroom als Vorlage."><i class="fa fa-database"></i> Als Vorlage speichern</a>  
			<?php endif; ?>
      <?php endif;?>
		<a id="retour" href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white">Zurück</a>
	</div>
	<?php if(!$this->item->id):?>
      <div class="alert alert-danger" role="alert">
        Um auf alle Parameter des Learningroom zugreifen zu können, müssen Sie zunächst die <b>Basisdaten</b> erfassen und diese speichern.
      </div>
   	<?php endif;?>
	<div class="row">
		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
			<div class="card text-white bg-danger mb-3">
				<div class="card-header">Basisdaten</div>
				<div class="card-body">
					<a href="classroom-edit?layout=basicdata&id=<?php echo $this->item->id;?>" class="btn btn-secondary">Zu den Basisdaten</a>
				</div>
			</div>
		</div>
		<?php if($this->item->id): ?>
		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
			<div class="card text-white mb-3" style="background-color: darkseagreen;">
				<div class="card-header">Versionierung</div>
				<div class="card-body">
					<a href="classroom-edit?layout=version&id=<?php echo $this->item->id;?>" class="btn btn-secondary">Zur Versionierung</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
			<div class="card text-white bg-success mb-3">
				<div class="card-header">Kommunikation</div>
				<div class="card-body">
					<a href="classroom-edit?layout=communication&id=<?php echo $this->item->id;?>" class="btn btn-secondary">Zu den Kommunikationsdaten</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
			<div class="card text-white bg-info mb-3">
				<div class="card-header">Teilnehmer</div>
				<div class="card-body">
					<a href="classroom-edit?layout=students&id=<?php echo $this->item->id;?>" class="btn btn-secondary">Zu den Teilnehmern</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
			<div class="card text-dark bg-light mb-3">
				<div class="card-header">Inhalte</div>
				<div class="card-body">
					<a href="classroom-edit?layout=content&id=<?php echo $this->item->id;?>" class="btn btn-secondary">Zu den Inhaltseinstellungen</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
			<div class="card text-white bg-dark mb-3">
				<div class="card-header">Struktur</div>
				<div class="card-body">
					<a href="classroom-edit?layout=structures&id=<?php echo $this->item->id;?>" class="btn btn-secondary">Zu den Struktureinstellungen</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
			<div class="card text-white bg-warning mb-3">
				<div class="card-header">Arbeitsmaterial</div>
				<div class="card-body">
					<a href="classroom-edit?layout=material&id=<?php echo $this->item->id;?>" class="btn btn-secondary">Zum Arbeitsmaterial</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
			<div class="card text-white bg-primary mb-3">
				<div class="card-header">Auswertungen</div>
				<div class="card-body">
					<a href="classroom-edit?layout=results&id=<?php echo $this->item->id;?>" class="btn btn-secondary">Zu den Auswertungen</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
			<div class="card text-white bg-secondary mb-3">
				<div class="card-header">Freigabe</div>
				<div class="card-body">
					<a href="classroom-edit?layout=rights&id=<?php echo $this->item->id;?>" class="btn btn-primary">Zu den Freigabeeinstellungen</a>
				</div>
			</div>
		</div>
	<?php endif;?>
	</div>
	<?php if($this->item->version): ?>
		<div class="card bg-light">
			<div class="card-header">
				<h3 style="font-size: 24px;">Dokumentation von Änderungen am Learningroom</h3>
				<h4 style="font-size: 18px;">Version: <?php echo $this->item->version;?></h4>
			</div>
			<div class="card-body">
				<?php echo $this->item->changelog;?>
			</div>
		</div>
	<?php endif; ?>	
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<script>
function libraryLearningroom(id) {
	var learningroomID = $('#learningroomID').html();
  	if(confirm("Soll der Learningroom mit der ID " + learningroomID + " in der Bibliothek gespeichert werden?") == true) {
  		jQuery('#save').modal('show');
     	jQuery.ajax({
			url: "index.php?option=com_jclassroom&task=classroom2.libraryLearningroom",
			data: {id:id},
			method: 'POST',
			//dataType: 'json',
			success: function( data ) {
				jQuery('#save').modal('hide');
			}
     	});
  	}
}
function exportLearningroom(id) {
	var learningroomID = $('#learningroomID').html();
	if(confirm("Soll der Learningroom mit der ID " + learningroomID + " als JSON-Datei exportiert werden?") == true) {
		jQuery.ajax({
			url: "index.php?option=com_jclassroom&task=classrooms.export",
			data: {id:id},
			method: 'GET',
			xhrFields: {
			responseType: 'text'
			},
			//dataType: 'json',
			success: function( data ) {
				var element = document.createElement('a');
				//element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
				element.setAttribute('href', data);
				element.setAttribute('download', 'learningroom' + id + '.json');
				element.style.display = 'none';
				document.body.appendChild(element);
				element.click();
				document.body.removeChild(element);
			}
		});
	}
}
function exportTemplate(id) {
  if(confirm("Soll dieser Learningroom als Vorlage gespeichert werden?") == true) {
     jQuery('#wait').css('display', 'block');
     jQuery.ajax({
         url: "index.php?option=com_jclassroom&task=classroom2.exportTemplate",
         data: {id:id},
         method: 'POST',
         //dataType: 'json',
         success: function( data ) {
             alert('Der Learningroom ' + id + ' wurde erfolgreich mit der ID ' + data + ' als Vorlage gespeichert');
         }
     });
     jQuery('#wait').css('display', 'none');
  }
}
</script>