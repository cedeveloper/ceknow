<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newPart.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newStudent.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <div class="row">
      <div class="col-1">
         <?php require_once(JPATH_COMPONENT.'/library/sidebar.php');?>
      </div>
      <div class="col-11">
         <div id="tabContent">
            <?php require_once(JPATH_COMPONENT.'/views/classroom/tmpl/basic.php');?>
            <?php require_once(JPATH_COMPONENT.'/views/classroom/tmpl/students.php');?>
            <?php require_once(JPATH_COMPONENT.'/views/classroom/tmpl/structure.php');?>
         </div>
      </div>
   </div>
   <input type="hidden" id="activeTab" name="activeTab" value="basic" />
</form>
<script type="text/javascript">
jQuery(document).ready(function() {
   var h1 = $('#sp-main-body').height();
   var v1 = $('#sp-header').height();
   $('#classroomSidebar').height(h1).css('top', v1);
   $('#classroomSidebar a#basic').addClass('acitive');
   jQuery('.summernote').summernote();
   jQuery('.select2').select2({width: '100%'});
});
function loadTab(tab) {
   $('#classroomSidebar .tab').each(function() {
      $(this).removeClass('active');
   });
   $('#' + tab).addClass('active');
   closeTabs();
   if(tab == 'basic') {
      $('#basic.classroomTabs').removeClass('hideThis');
   }
   if(tab == 'students') {
      $('#students.classroomTabs').removeClass('hideThis');
   }
   if(tab == 'structure') {
      $('#structureA.classroomTabs').removeClass('hideThis');
   }
}
function closeTabs() {
   $('.classroomTabs').each(function() {
      $(this).addClass('hideThis');
   });
}
</script>