<div id="newPart_dialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Neue Unit einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <select id="newUnitType" class="select2">
               <option value="" selected="selected" disabled>Bitte wählen Sie einen Unittypen aus</option>
               <option value="1">Freitext</option>
               <option value="2">virtual classroom</option>
               <option value="7">Video</option>
               <option value="3">Pause</option>
               <option value="5">Einzelübung</option>
               <option value="6">Gruppenübung</option>
               <option value="4">Quizz</option>
               <option value="8">Feedback</option>
            </select>
         </div>
         <div class="modal-footer">
            <button id="newUnitSave" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>