<div id="structureA" class="classroomTabs hideThis">
   <h2>Struktur bearbeiten</h2>
   <div class="form-horizontal mt-3">
               <?php $unitCounter = 0;?>
               <div id="structure">
                  <?php if($this->item->cards): ?>
                     <?php $i = 1; ?>
                     <?php foreach($this->item->cards as $card): ?>
                     <?php
                        $html = '';
                        $cardData = json_decode($card->structure); 
                        $date    = new Datetime($card->day);
                        $html .= '<div id="cardFor' .$date->format('d').'" class="card bg-light mb-2">';
                           $html .= '<div class="card-header d-flex" style="justify-content: space-between">';
                              $html .= '<i onclick="openDay(&quot;'.$date->format('d').'&quot;);" class="open fa fa-chevron-right"></i>';
                              $html .= '<h2>'.$date->format('l, d.m.Y').'</h2>';
                              $html .= '<a id="addFor'.$date->format('d').'" onclick="addUnit(&quot;'.$date->format('d').'&quot;);"><i style="font-size: 24px;border-radius: 50%;" class="addUnit fa fa-plus bg-success text-white p-1"></i></a>';
                           $html .= '</div>';
                           $html .= '<div class="card-body">';
                              if($cardData):
                                 foreach($cardData[0]->content as $key => $data):
                                    JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
                                    $template = new UnitsHelper();
                                    $html .= $template->getTemplate($i, $date->format('d'), $data->typ, $data);
                                    $i++;
                                 endforeach;
                              else:
                                 $html .= '<p class="cardMessage m-0">Keine Units für diesen Tag gefunden.</p>';
                              endif;
                           $html .= '</div>';
                        $html .= '</div>';
                        $unitCounter = $i;
                        echo $html;
                     ?>
                     <?php endforeach; ?>
                  <?php else: ?>
                     <p>Keine Units gefunden</p>
                     <a id="addDays" onclick="addDays();" class="btn btn-success text-white">Struktur erstellen</a>
                  <?php endif; ?>
               </div>
            </div>
</div>