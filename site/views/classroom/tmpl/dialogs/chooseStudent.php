<div id="chooseStudent_dialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Teilnehmer auswählen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Teilnehmer
               </div>
               <div class="col-12 col-sm-10">
                  <select id="studentSalutation" multiple class="select2">
                     <?php
                     if($this->allStudents):
                        foreach($this->allStudents as $student):
                           echo '<option value="'.$student->id.'">( '.$student->company.' ) '.$student->first_name.' '.$student->last_name.'</option>';
                        endforeach;
                     endif;
                     ?>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button id="chooseStudentSave" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>