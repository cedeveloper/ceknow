<div id="addDayModule" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Modul Name</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-12 col-form-label">
                  <small>Geben Sie einen eindeutigen Namen für dieses Modul ein.</small>
               </div>
               <div class="col-12">
                  <input type="text" id="addDayModuleTitle" value="" placeholder="Geben Sie einen Modulnamen ein." />
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button onclick="addClassroomDayModuleSave();" data-dismiss="modal" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>