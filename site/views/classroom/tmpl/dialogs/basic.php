<div id="basic" class="classroomTabs">
  <h2>Learningroom bearbeiten</h2>
  <div class="form-horizontal mt-3">
      <div class="form-group row">
          <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('title'); ?>
          </div>
          <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('title'); ?>
          </div>
      </div>
      <div class="form-group row">
          <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('main_trainer'); ?>
          </div>
          <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('main_trainer'); ?>
          </div>
      </div>
      <div class="form-group row">
          <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('co_trainer'); ?>
          </div>
          <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('co_trainer'); ?>
          </div>
      </div>
      <div class="form-group row">
          <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('description'); ?>
          </div>
          <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('description'); ?>
          </div>
      </div>
      <div class="form-group row">
          <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('inventa_kursID'); ?>
          </div>
          <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('inventa_kursID'); ?>
          </div>
      </div>
      <div class="form-group row">
          <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('inventa_terminID'); ?>
          </div>
          <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('inventa_terminID'); ?>
          </div>
      </div>
      <div class="form-group row">
          <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('fromDate'); ?>
          </div>
          <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('fromDate'); ?>
          </div>
      </div>
      <div class="form-group row">
          <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('toDate'); ?>
          </div>
          <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('toDate'); ?>
          </div>
      </div>
  </div>
</div>