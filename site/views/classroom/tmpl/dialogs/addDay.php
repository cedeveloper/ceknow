<div id="addDayDialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Tag auswählen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-12 col-form-label">
                  <small>Wählen Sie einen Tag aus, der in die Struktur eingefügt werden soll.</small>
               </div>
               <div class="col-12">
                  <input type="text" class="datepicker" id="addDayValue" value="" placeholder="Wählen Sie einen Tag." />
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button onclick="addClassroomDaySave();" data-dismiss="modal" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>