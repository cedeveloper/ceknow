<?php
/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
$doc->addStyleSheet('components/com_inflow/assets/css/jquery-ui.css');
$doc->addScript('components/com_inflow/assets/js/jquery-ui.min.js');
?>
<form action="" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal">
<div class="row">
	<div class="col-12">
		<?php echo $this->textblock;?>
		<div class="row">
			<div class="col-12">
				<?php echo '<h1>'.$this->item->packet->name.'</h1>';?>
			</div>
			<div class="col-12 col-sm-2">
			<?php
			echo '<img src="'.$this->item->packet->image.'" />';
			?>
			</div>
			<div class="col-sm-1"></div>
			<div class="col-12 col-sm-9">
				<h4>Dieses Paket beinhaltet</h4>
				<?php
				echo $this->item->packet->kurzbeschreibung;
				?>
			</div>
		</div>
		<div class="col-6 mt-3">
	    	<div class="form-group">
				<?php echo $this->form->getLabel('anrede'); ?>
				<?php echo $this->form->getInput('anrede'); ?>
			</div>
			<div class="form-group">
				<?php echo $this->form->getLabel('vorname'); ?>
				<?php echo $this->form->getInput('vorname'); ?>
			</div>
			<div class="form-group">
				<?php echo $this->form->getLabel('nachname'); ?>
				<?php echo $this->form->getInput('nachname'); ?>
			</div>
			<div class="form-group">
				<?php echo $this->form->getLabel('strasse'); ?>
				<?php echo $this->form->getInput('strasse'); ?>
			</div>
			<div class="form-group">
				<?php echo $this->form->getLabel('plz'); ?>
				<?php echo $this->form->getInput('plz'); ?>
			</div>
			<div class="form-group">
				<?php echo $this->form->getLabel('ort'); ?>
				<?php echo $this->form->getInput('ort'); ?>
			</div>
			<div class="form-group">
				<?php echo $this->form->getLabel('telefon'); ?>
				<?php echo $this->form->getInput('telefon'); ?>
			</div>
			<div class="form-group">
				<?php echo $this->form->getLabel('email'); ?>
				<?php echo $this->form->getInput('email'); ?>
				<small>Bitte beachten Sie: Ihr Benutzername zur Anmeldung an Ihrem Kundenkonto ist identisch zu Ihrer E-Mail-Adresse</small>
			</div>
			<div class="form-group">
				<?php echo $this->form->getLabel('delivery_from'); ?>
				<?php echo $this->form->getInput('delivery_from'); ?>
			</div>
			<div class="form-group">
				<?php echo $this->form->getLabel('delivery_day'); ?>
				<?php echo $this->form->getInput('delivery_day'); ?>
			</div>
			<div class="form-group">
				<?php echo $this->form->getInput('dateContact'); ?>
				<?php echo $this->form->getLabel('dateContact'); ?>
				
			</div>
			<div class="form-group">
				<?php echo $this->form->getInput('dataAgreement'); ?>
				<?php echo $this->form->getLabel('dataAgreement'); ?>
				
			</div>
			<button type="submit" class="btn btn-success">Jetzt bestellen</button>
		</div>
	</div>
</div>
<input type="hidden" name="jform[productID]" value="<?php echo $this->item->packet->id;?>"/>
<input type="hidden" name="task" value="welcome.quick" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
	$('#jform_delivery_from').datepicker(
	{
		firstDay: 1,
        prevText: '&#x3c;zurück', prevStatus: '',
        prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
        nextText: 'Vor&#x3e;', nextStatus: '',
        nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
        currentText: 'heute', currentStatus: '',
        todayText: 'heute', todayStatus: '',
        clearText: '-', clearStatus: '',
        closeText: 'schließen', closeStatus: '',
        monthNames: ['Januar','Februar','März','April','Mai','Juni',
        'Juli','August','September','Oktober','November','Dezember'],
        monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun',
        'Jul','Aug','Sep','Okt','Nov','Dez'],
        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
        dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
        dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
        dateFormat: 'dd.mm.yy',
        minDate: 0,
		beforeShowDay: function(date) {
			if(date.getDay() == 6 || date.getDay() == 0) {
				return [true];
			} else {
				return [false];
			}
		}
	}
	);
	function checkPLZ() {
		var plz = $('#jform_plz').val();
		if(isNaN(plz)) {
			alert('Geben Sie die Postleitzahl als Zahl ein.');
			$('#jform_plz').val('').focus();

			return false;
		}
		$.ajax({
			type: "POST",
			url: "index.php?option=com_inflow&task=assistant.checkPLZ",
			data: {plz:plz},
			//dataType: 'json',
			success: function( data ) {
				if(data == 'NOK1' || data == 'NOK2') {
					alert('Leider liefern wir noch nicht an die Postleitzahl ' + plz);
					window.location.href = 'http://www.webdesign-internet.de/genusslieferbar';
					return false;
				}
			}
		});
	}
	function checkEmail() {
		var email = $('#jform_email').val();
		$.ajax({
			type: "POST",
			url: "index.php?option=com_inflow&task=assistant.checkEmail",
			data: {email:email},
			//dataType: 'json',
			success: function( data ) {
				if(data) {
					alert(data);
					$('#jform_email').val('');
					return false;
				}
			}
		});
	}
</script>
