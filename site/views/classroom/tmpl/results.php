<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addStyleSheet('components/com_jclassroom/assets/css/jqcloud.css');
$doc->addScript('components/com_jclassroom/assets/js/jqcloud-1.0.4.js');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
$doc->addScript('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/resultQuizz.php');
?>
<style>
   .aQuestion {
      border-bottom: 2px solid #e1e1e1;
      padding-bottom: 15px;
   }
</style>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <?php 
	JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
	$load = new GlobalHelper();
	echo $load->getClassroomHeader($this->learningroom, 'Auswertungen');
	?>
   <small><i class="fa fa-question-circle"></i> Hier sehen Sie alle Ergebnisse zu Quizzen, die Teilnehmer im Rahmen des Learningrooms durchgeführt haben.</small>
   <div class="buttonleiste">
      <a id="retour" href="classroom-edit?layout=global&id=<?php echo $this->item->id;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div id="theAlerts" class="alert alert-success" style="display: none;"></div>
   <div class="form-horizontal mt-3">
      <?php require_once(JPATH_COMPONENT.'/views/classroom/tmpl/result.php');?>
   </div>
   <input type="hidden" id="classroomID" name="jform[id]" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="quizzID" value="" />
   <input type="hidden" name="task" value="" />
   <?php echo JHtml::_('form.token'); ?>
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<script>
function saveTheImage() {
   var unitID = $('#quizzID').val();
   $('canvas').each(function() {
      var id = $(this).attr('id');
      var canvas = document.getElementById(id);
      var dataURL = canvas.toDataURL();
      saveImage(unitID, dataURL, $(this).attr('id'));
   });
}
function resultQuizz(theResultID, unitID) {
   var classroomID = jQuery('#classroomID').val();
   jQuery('#resultQuizz .modal-body').empty();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom2.loadQuizzResult",
      data: {theResultID:theResultID,classroomID:classroomID,unitID:unitID},
      dataType: 'json',
      success: function( data ) {
         
         jQuery('#resultQuizz #quizzTitle').html(data['quizzTitle']);
         jQuery('#resultQuizz #quizzStudent').html(data['username']);
         jQuery('#resultQuizz .modal-body').append(data['html']);
         jQuery('#resultQuizz #theResultID').val(theResultID);
         jQuery('#resultQuizz #unitID').val(unitID);
         jQuery('#resultQuizz #completeEvaluation').val(0);
         
         loadSingleChart(theResultID);
      }
   });
   jQuery('#resultQuizz').modal('show');
}
function printResult() {
   var classroomID   = jQuery('#classroomID').val();
   var theResultID   = $('#resultQuizz #theResultID').val();
   var completeEvaluation = $('#resultQuizz #completeEvaluation').val();
   var unitID        = $('#resultQuizz #unitID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom2.printResult",
      data: {
         classroomID:classroomID,
         theResultID:theResultID,
         completeEvaluation:completeEvaluation,
         unitID:unitID
      },
      //dataType: 'json',
      success: function( data ) {
         window.open(data);
      }
   });
}
function completeResultQuizz(unitID, unitPosID) {
   var classroomID   = jQuery('#classroomID').val();
   var quizzData     = getQuizzData(unitID);
   $('#quizzID').val(unitID);
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom2.loadQuizzCompleteResult",
      data: {classroomID:classroomID,unitPosID:unitPosID, unitID:unitID},
      dataType: 'json',
      success: function( data ) {
         jQuery('#resultQuizz .modal-body').empty();
         jQuery('#resultQuizz #quizzTitle').html(quizzData['title']);
         jQuery('#resultQuizz #quizzStudent').html('Alle Teilnehmer');
         jQuery('#resultQuizz #theResultID').val(unitPosID);
         jQuery('#resultQuizz #unitID').val(unitID);
         jQuery('#resultQuizz #completeEvaluation').val(1);
         var i = 1;
         deleteTheFolder(classroomID, unitID);
         jQuery.each(data, function(e, value) {
            var strCorrectAnswers = '';
            if(value['chart'] != 'wordCloud') {  
               if(value['chart'] != '' && value['chart'] != '0') {
                  if(value['correctAnswers'] == 0) {
                     strCorrectAnswers = '<p style="margin:5px 0px;font-size: 14px; font-weight: bold;"><i>Nur eine Antwort kann richtig sein</i></p>';
                  }
                  if(value['correctAnswers'] == 2) {
                     strCorrectAnswers = '<p style="margin:5px 0px;font-size: 14px; font-weight: bold;"><i>Mehrere Antworten können richtig sein</i></p>';
                  }
                  var canvasStyle   = 'canvasChartH';
                  var canvasHeight  = '300';
                  if(value['chart'] == 'bar') {
                     canvasStyle    = 'canvasChartV';
                     canvasHeight   = '500';
                  }
                  jQuery('#resultQuizz .modal-body').append(
                     '<div class="aQuestion">' +
                     '<p style="margin-top: 20px;margin-bottom: 5px;font-size: 18px; font-weight: bold;">Frage: <i>' + value['question'] + '</i></p>' +
                     '<p style="margin:5px 0px;font-size: 14px; font-weight: bold;">Anzahl Teilnahmen: <i>' + value['usersFinishedTheQuizz'] + '</i></p>' + 
                     strCorrectAnswers + 
                     '<div style="height: ' + canvasHeight + 'px;">' +
                     '<canvas id="theModalChart' + value['questionID'] + '" class="' + canvasStyle + '"></canvas>' + 
                     '</div>' + 
                     '</div>'
                  );
                  writeChart(value,value['questionID']);
               } else {
                  if(value['questionType'] && value['questionType'] == 7) {
                     jQuery('#resultQuizz .modal-body').append(
                        '<div class="aQuestion">' +
                        '<p style="margin-top: 20px;margin-bottom: 5px;font-size: 18px; font-weight: bold;">Frage: <i>' + value['question'] + '</i></p>' +
                        '<p style="margin-top: 5px;font-size: 14px; font-weight: bold;">Anzahl Teilnahmen: <i>' + value['cCount'] + '</i></p>' +
                        '<p style="margin-top: 6px;font-size: 14px; font-weight: normal;">' + value['indiAnswers'] + '</p>' +
                        '</div>'
                     );
                  } else {
                     jQuery('#resultQuizz .modal-body').append(
                        '<div class="aQuestion">' +
                        '<p style="margin-top: 20px;margin-bottom: 5px;font-size: 18px; font-weight: bold;">Frage: <i>' + value['question'] + '</i></p>' +
                        '<p style="margin-top: 5px;font-size: 14px; font-weight: bold;">Anzahl Teilnahmen: <i>' + value['cCount'] + '</i></p>' +
                        '<p style="margin-top: 6px;font-size: 14px; font-weight: normal;">' + value['indiAnswers'] + '</p>' +
                        '</div>'
                     );
                  }
               }
               i++;
            } else {
               jQuery('#resultQuizz .modal-body').append(
                  '<p style="margin-top: 20px;margin-bottom: 5px;font-size: 18px; font-weight: bold;">Frage:<i>' + value['question'] + '</i></p>' +
                  '<div id="theCloud" height="200" width="400"></div>'
               );
               writeCloud(value['datas'],0);
            }
         }); 
         setTimeout(function() { 
              saveTheImage()
         }, 2000);         
      }
   });
   jQuery('#resultQuizz').modal('show');
}
function deleteTheFolder(classroomID, unitID) {
    jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom2.deleteTheFolder",
      data: {classroomID:classroomID,unitID:unitID},
      async: true,
      //dataType: 'json',
      success: function( data ) {
         //console.log(data);
      }
   });
}
function saveImage(unitID, image, id) {
   var classroomID   = jQuery('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom2.saveChartImage",
      data: {classroomID:classroomID,unitID:unitID,image:image,id:id},
      //dataType: 'json',
      success: function( data ) {
         //console.log(data);
      }
   });
}
function loadSingleChart(theResultID) {
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom2.loadSingleChart",
      data: {theResultID:theResultID},
      dataType: 'json',
      success: function( data ) {
         if(data['calculate'] == 2) {
            jQuery('#resultQuizz .modal-body').append(
            '<div style="width: 400px;height: 400px;">' + 
            '<canvas id="theModalChart" height="200" width="400"></canvas>' + 
            '</div>'
            );
            writeChart(data,'');
         }
      }
   });
}
function writeCloud(data,i) {
   $("#theCloud").jQCloud(data, {
      width: 500,
      height: 350
   });
}
function writeChart(data, i) {
   var labels = new Array();
   jQuery.each(data['labels'], function(e, value) {
      labels.push(value);
   });
   var datas = new Array();
   jQuery.each(data['datas'], function(e, value) {
      datas.push(value);
   });
   var backgroundColor = [
      'rgba(255,0,0,1)',
      'rgba(30,144,255,1)',
      'rgba(255,240,0,1)',
      'rgba(34,139,34,1)',
      'rgba(199,21,133,1)',
      'rgba(32,178,170,1)'
   ]
   var ctx     = document.getElementById('theModalChart' + i).getContext("2d");
   
   if(data['chart'] == 'bar' || data['chart'] == 'horizontalBar') {
      var myChart = new Chart(ctx, {
         type: data['chart'],
         data: {
            labels: labels,
            datasets: [{
               data: datas,
               backgroundColor: backgroundColor
            }]
         },
         options: {
            maintainAspectRatio: false,
            legend: {
               labels: {
                  defaultFontSize: 24
               },
               display: false
            },
            scales: {
               yAxes: [{
                  labels: {
                     defaultFontSize: 24
                  },
                  ticks: {
                     fontSize: 20,
                     stepSize: 1,
                     beginAtZero: true
                  }
               }],
               xAxes: [{
                   ticks: {
                     fontSize: 20,
                     stepSize: 1,
                     beginAtZero: true
                   }
               }]
            }
         }
      });
   }
   if(data['chart'] == 'pie') {
      var myChart = new Chart(ctx, {
         type: data['chart'],
         data: {
            labels: labels,
            datasets: [{
               data: datas,
               backgroundColor: backgroundColor
            }]
         },
         options: {
            maintainAspectRatio: false,
            legend: {
               labels: {
                  fontSize: 24
               }
            },
            scales: {
               yAxes: [{
                  display: false,
                  gridLines: {
                     min: 0,
                     max: 20,
                     stepSize: 1,
                     display: false
                  }
               }],
               xAxes: [{
                  display: false,
                  gridLines: {
                     display: false
                  }
               }]
            }
         }
      });
   }
}
function getQuizzData(unitID) {
   var result = '';
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom2.getQuizzData",
      data: {unitID:unitID},
      dataType: 'json',
      async: false,
      success: function( data ) {
         result = data;
      }
   });
   return result;
}
function deleteResult(rID) {
   if(confirm('Soll das Ergebnis gelöscht werden?') == true) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.deleteResult",
         data: {rID:rID},
         //dataType: '',
         success: function( data ) {
            $('#result' + rID).remove();
         }
      });
   }
}
function hideSave() {
   setTimeout(hideSaveExecute,2500);
}
function hideSaveExecute() {
   jQuery('#theAlerts').slideUp(200);
}
</script>