<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addStyleSheet('https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.css');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
$doc->addScript('https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.js');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/editTimeblock.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <?php 
	JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
	$load = new GlobalHelper();
	echo $load->getClassroomHeader($this->learningroom, 'Struktur');
	?>
   <div id="theAlerts" class="alert alert-success" style="display: none;"></div>
   <div class="form-horizontal mt-3">
      <a onclick="addTimeblock();" class="btn btn-success text-white">Zeitblock einfügen</a>
      <!--<a onclick="reorderRoom();" class="btn btn-primary text-white">Struktur neu berechnen</a>-->
      <a id="retour" href="classroom-edit?layout=global&id=<?php echo $this->item->id;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
      <div class="row mt-5">
         <div class="col-12 col-sm-9">
            <h3 id="theH3" style="font-size: 14px;">Zeitblöcke, in denen Sie Themen einfügen können</h3>
            <div id="timeblocks">
               <?php
               if($this->item->timeblocks):
                  foreach($this->item->timeblocks as $timeblock):
                     $title = $timeblock->title;
                     if(strtotime($timeblock->title)) :
                         $title = date('d.m.Y', strtotime($timeblock->title));
                     endif;
                     echo '<div id="timeblock'.$timeblock->id.'" class="timeblock" style="background-color: '.$timeblock->background_color.';" data-color="'.$timeblock->background_color.'" data-title="'.date('d.m.Y',strtotime($timeblock->title)).'">';
                     echo '<span class="theDate">';
                     echo '<span class="theDateA">'.$title.'</span><br/>';
                     echo '<i class="fa fa-pencil mr-2" onclick="editTimeblock('.$timeblock->id.');"></i>';
                     echo '<i class="fa fa-trash-o mr-2" onclick="deleteTimeblock('.$timeblock->id.');"></i>';
                     echo '<i class="fa fa-plus" title="Einen neuen Zeitblock am Ende einfügen" onclick="addTimeblock();"></i>';
                     echo '</span>';
                     if($this->item->timeblocks_themes):
                        foreach($this->item->timeblocks_themes as $theme):
                           if($timeblock->id == $theme->timeblockID):
                              echo '<a id="theme'.$theme->themeID.'" class="theme mb-2 btn" data-title="'.$theme->title.'" data-ordering="'.$theme->ordering.'">'.$theme->title.'</a>';
                           endif;
                        endforeach;
                     endif;
                     echo '</div>';
                  endforeach;
               endif;
               ?>
            </div>
         </div>
         <div class="col-12 col-sm-3">
            <h3 id="themes-title" style="font-size: 14px;">Themen, die Sie in Zeitblöcke einfügen können</h3>
            <div id="themes" class="themes mb-3">
               <?php if($this->item->extractTimeblocks): ?>
                  <?php foreach($this->item->extractTimeblocks as $card): ?>
                     <?php 
                        $date    = new Datetime($card->day);
                        $title   = '';
                        $title   .= $card->title;
                     ?>
                     <a 
                     id="theme<?php echo $card->id;?>"
                     data-date="<?php echo date('d.m.Y', strtotime($card->day));?>" 
                     data-title="<?php echo $card->title;?>" 
                     data-id="<?php echo $card->id;?>"
                     data-ordering="<?php echo $card->ordering;?>"
                     class="theme btn mb-2"><?php echo $title;?> 
                     </a>
                  <?php endforeach;?>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </div>
   <input type="hidden" id="classroomID" name="jform[id]" value="<?php echo $this->item->id;?>" />
   <input type="hidden" name="task" value="" />
   <?php echo JHtml::_('form.token'); ?>
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<script>
$('.colorpicker').spectrum({

});
var scrollP = 0;
var originW = 0;
originW = $('.col-sm-3').outerWidth();
$(document).on("scroll", function() {
   scrollP = $(document).scrollTop();
   if(scrollP > 300) {
      $('#themes').css('position','fixed').css('top', '130px').css('width', originW + 'px');
      $('#themes-title').css('position','fixed').css('top', '110px').css('width', originW + 'px');
   }
   if(scrollP <= 300) {
      $('#themes').css('position', 'relative').css('top', '0px').css('width','auto');
      $('#themes-title').css('position','relative').css('top', '0px').css('width', 'auto');
   }
});
function addTimeblock() {
   var classroomID = $('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom2.addTimeblock",
      data: {classroomID:classroomID},
      //dataType: 'json',
      success: function( data ) {
         var newTimeblock = jQuery('<div id="timeblock' + data + '" class="timeblock">' + 
         '<span class="theDate"><span class="theDateA">[NEU]</span><br/>' + 
         '<i class="fa fa-pencil mr-2" onclick="editTimeblock(' + data + ');"></i>' + 
         '<i class="fa fa-trash-o mr-2" onclick="deleteTimeblock(' + data + ');"></i>' + 
         '<i class="fa fa-plus" onclick="addTimeblock();"></i></span>' +
         '</div>').appendTo('#timeblocks');
         $('#theAlerts').html('Zeitblock wurde eingefügt').css('display', 'block');
         hideSave();
         newTimeblock.droppable({
            accept: ".theme", 
            drop: function(event, ui) {
               $(this).removeClass("border").removeClass("over");
               var dropped = ui.draggable;
               var droppedOn = $(this);
               if(dragFrom == 'themes') {  
                  jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
                  addThemeToTimeblock(ui.draggable.attr('id'),$(this));  
               }  else {
                  var classroomID = $('#classroomID').val();
                  var parent  = droppedOn;
                  var id      = parent.attr('id');
                  jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
                  var order = jQuery('#' + id).sortable("toArray");
                  jQuery.ajax({
                     type: "POST",
                     url: "index.php?option=com_jclassroom&task=classroom.writeSortableTimeblocks",
                     data: {classroomID:classroomID,order:order},
                     //dataType: 'json',
                     success: function( data ){
                        
                     }
                  });
               }  
            },
            over: function(event, elem) {
               $(this).addClass("over");
            },
            out: function(event, elem) {
               $(this).removeClass("over");
            }
         });
         $('html, body').animate({
            scrollTop: $('#timeblock' + data).offset().top - 100
         },1000);
      }
   });
   
}
var dragFrom = '';
var dragX = 0;
var dragY = 0;
jQuery(".timeblock").sortable({
   update: function (event, ui) {
      alert("HIER");
      console.log(ui);
      var order = jQuery('.classroomDays').sortable("toArray");
      console.log(order);
      /*
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.writeSortableThemes",
         data: {order:order},
         //dataType: 'json',
         success: function( data ){
            console.log(data);
         }
      });*/
   }
});
jQuery('.timeblock').droppable({
   accept: ".theme", 
   drop: function(event, ui) {
      $(this).removeClass("border").removeClass("over");
      var dropped = ui.draggable;
      var droppedOn = $(this);
      if(dragFrom == 'themes') {  
         
         jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
         addThemeToTimeblock(ui.draggable.attr('id'),$(this));  
      }  else {
         var classroomID = $('#classroomID').val();
         var parent  = droppedOn;
         var id      = parent.attr('id');
         jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
         var order = jQuery('#' + id).sortable("toArray");
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.writeSortableTimeblocks",
            data: {classroomID:classroomID,order:order},
            //dataType: 'json',
            success: function( data ){
               console.log(data);
            }
         });
      }
   },
   over: function(event, elem) {
      $(this).addClass("over");
   },
   out: function(event, elem) {
      $(this).removeClass("over");
   }
});
jQuery(".theme").draggable({ 
   cursor: "crosshair", 
   revert: "invalid",
   start: function(event, ui) {
      dragX = ui.offset.left;
      dragY = ui.offset.top;
      var parent = jQuery(this).parent().attr('id');
      if(parent == 'themes') {
         dragFrom = 'themes';
      } else {
         dragFrom = 'timeblocks';
      }
   }
});
jQuery('.classroomDays').sortable({
   update: function (event, ui) {
      var order = jQuery('.classroomDays').sortable("toArray");
      console.log(order);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.writeSortableThemes",
         data: {order:order},
         //dataType: 'json',
         success: function( data ){
            console.log(data);
         }
      });
   }
});
jQuery("#themes").droppable({ 
   accept: ".theme", 
   drop: function(event, ui) {
      $(this).removeClass("border").removeClass("over");
      var dropped = ui.draggable;
      var droppedOn = $(this);
      jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);      
      var $wrapper = $('#themes');

      $wrapper.find('.theme').sort(function(a, b) {
          return +a.dataset.ordering - +b.dataset.ordering;
      })
      .appendTo($wrapper);
      deleteThemeFromTimeblock(ui.draggable.attr('id'),$(this));
   },
   over: function(event, elem) {
      $(this).addClass("over");
   },
   out: function(event, elem) {
      $(this).removeClass("over");
   }
});
function addThemeToTimeblock(droppedID,droppedTo) {
   var classroomID = $('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.addThemeToTimeblock",
      data: {classroomID:classroomID,droppedID:droppedID,droppedTo:droppedTo.attr('id')},
      //dataType: 'json',
      success: function( data ) {
         console.log(data);
      }
   });
}
function deleteThemeFromTimeblock(droppedID,droppedTo) {
   var classroomID = $('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.deleteThemeFromTimeblock",
      data: {classroomID:classroomID,droppedID:droppedID,droppedTo:droppedTo.attr('id')},
      //dataType: 'json',
      success: function( data ) {
         console.log(data);
      }
   });
}
function editTimeblock(id) {
   var dayDate    = $('#timeblock' + id).attr('data-title');
   var color      = $('#timeblock' + id).attr('data-color');
   $('#timeblockID').val(id);
   $('#timeblockDayDate').val(dayDate);
   $('#timeblockColor').val(color);
    $('.colorpicker').spectrum();
   jQuery('#editTimeblock').modal('show');
}
function saveEditTimeblock() {
   var timeblockID   = $('#timeblockID').val();
   var dayDate       = $('#timeblockDayDate').val();
   var color         = $('#timeblockColor').val();
    if(!dayDate || !color) {
      alert('Bitte wählen Sie ein Datum und eine Darstellungsfarbe aus.');
      return false;
   }
   $('#timeblock' + timeblockID).attr('data-color',color).attr('data-title',dayDate);
   jQuery('#editTimeblock').modal('hide');
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.saveEditTimeblock",
      data: {dayDate:dayDate,color:color,timeblockID:timeblockID},
      //dataType: 'json',
      success: function( data ) {
         $('#timeblock' + timeblockID).find('.theDateA').html(dayDate);
         $('#timeblock' + timeblockID).css('background-color', color);
      }
   });
}
function deleteTimeblock(id) {
   if(confirm('Soll der Zeitblock ' + id + ' gelöscht werden?') == true) {

      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.deleteTimeblock",
         data: {timeblockID:id},
         //dataType: 'json',
         success: function( data ) {
            $('#timeblock' + id + ' .theme').each(function() {
               var thisTheme = $(this);
               $('#themes').append(
               '<a id="' + thisTheme.attr('id') + '" class="theme mb-2 btn data-title="' + thisTheme.attr('data-title') + '" data-ordering="' + thisTheme.attr('data-ordering') + '" style="position: relative;left:0px;top: 0px;">' + thisTheme.html() + '</a>'
               );
            });
            jQuery(".theme").draggable({ 
               cursor: "crosshair", 
               revert: "invalid"
            });
            $('#timeblock' + id).remove();
            
         }
      });
   }
}
function reorderRoom() {
   var classroomID = $('#classroomID').val();
   if(confirm('Soll die Struktur neu berechnet werden?') == true) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.reorderRoom",
         data: {classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#theAlerts').html('Struktur wurde neu berechnet').css('display', 'block');
            hideSave();
         }
      });
   }
}
function hideSave() {
   setTimeout(hideSaveExecute,2500);
}
function hideSaveExecute() {
   jQuery('#theAlerts').slideUp(200);
}
</script>