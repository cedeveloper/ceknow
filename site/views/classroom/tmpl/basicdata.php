<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
$doc->addScript('components/com_jclassroom/assets/js/imgForJCE.js');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
    <?php 
	JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
	$load = new GlobalHelper();
	echo $load->getClassroomHeader($this->learningroom, 'Basisdaten');
	?>
   <div class="buttonleiste mb-4">         
      <a class="btn btn-success text-white" onclick="Joomla.submitform('classroom2.simpleSaveBasic')">Speichern</a>
      <a class="btn btn-secondary text-white" onclick="Joomla.submitform('classroom2.saveBasic')">Speichern & Schließen</a>
      <a id="retour" href="<?php echo $this->retourGlobal;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
             <?php echo $this->form->getLabel('title'); ?>
           </div>
           <div class="col-12 col-sm-10">
             <?php echo $this->form->getInput('title'); ?>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
             <?php echo $this->form->getLabel('main_trainer'); ?>
           </div>
           <div class="col-12 col-sm-10">
             <?php echo $this->form->getInput('main_trainer'); ?>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
             <?php echo $this->form->getLabel('co_trainer'); ?>
           </div>
           <div class="col-12 col-sm-10">
             <?php echo $this->form->getInput('co_trainer'); ?>
           </div>
       </div>  
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
             <?php echo $this->form->getLabel('created_by'); ?>
           </div>
           <div class="col-12 col-sm-10">
             <?php echo $this->form->getInput('created_by'); ?>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
             <?php echo $this->form->getLabel('companyID'); ?>
           </div>
           <div class="col-12 col-sm-10">
             <?php echo $this->form->getInput('companyID'); ?>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
               <?php echo $this->form->getLabel('description'); ?>
           </div>
           <div data-editor="jform_description" class="use_editor col-12 col-sm-10">
               <?php echo $this->form->getInput('description'); ?>
               <?php
               require_once(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');
               ?>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
               <?php echo $this->form->getLabel('visibleTo'); ?>
           </div>
           <div class="col-12 col-sm-10">
               <?php echo $this->form->getInput('visibleTo'); ?>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
               <?php echo $this->form->getLabel('presentation'); ?>
           </div>
           <div class="col-12 col-sm-10">
               <?php echo $this->form->getInput('presentation'); ?>
               <br/>
               <small>Roommodus = Der Learningroom wird komplett als Übersicht ohne Zeitzuordnung ausgegeben</small><br/>
               <small>Timetable = Der Learningroom wird komplett als Übersicht mit Zeitzuordnung ausgegeben.</small><br/>
               <small>Katalogmodus = Der Learningroom wird einzeln durchschaltbar ausgegeben.</small>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
               <?php echo $this->form->getLabel('testmode'); ?>
           </div>
           <div class="col-12 col-sm-10">
               <?php echo $this->form->getInput('testmode'); ?>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
               <?php echo $this->form->getLabel('email_testmode'); ?>
           </div>
           <div class="col-12 col-sm-10">
               <?php echo $this->form->getInput('email_testmode'); ?>
           </div>
       </div>
   </div>
   <input type="hidden" id="classroomID" name="jform[id]" value="<?php echo $this->item->id;?>" />
   <input type="hidden" name="task" value="classroom2.saveBasic" />
   <?php echo JHtml::_('form.token'); ?>
</form>
<div id="wait">
   Daten werden geladen
   <img width="80px" src="images/logo_solo.png" />
</div>
<?php 
require_once(JPATH_COMPONENT.'/views/dialogs/imageContent.php');
require_once(JPATH_COMPONENT.'/views/dialogs/formForJCE.php');
?>
<script>
let unitID      = false;
let classroomID = $('#classroomID').val();
let part        = 'classroom';
var form = $("#adminForm");
   $.validator.setDefaults({
   ignore: []
});
form.validate({
   rules: {
      'jform[title]': {
         required: true
      },
      'jform[visibleTo]': {
         required: true
      },
      'jform[presentation]': {
         required: true
      }
      
   },
   messages: {
      'jform[title]': 'Bitte geben Sie einen Namen für den Learningroom ein.',
      'jform[presentation]': 'Bitte wählen Sie eine Ausgabeart aus.',
      'jform[visibleTo]': 'Bitte wählen Sie eine Verwaltungsart aus.'
   },
   submitHandler: function(form) {

      form.submit();
   }
});
</script>