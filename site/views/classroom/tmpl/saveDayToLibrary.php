<div id="saveDayToLibrary" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Tag in der Bibliothek speichern</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-horizontal mt-3">
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <label>Name der Sicherung</label>
                  </div>
                  <div class="col-12 col-sm-9">
                     <input id="saveDayToLibrary_title" type="text" value="" />
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <label>Beschreibung</label>
                  </div>
                  <div class="col-12 col-sm-9">
                     <textarea style="width: 100%;border: 1px solid #e1e1e1;border-radius: 4px;" id="saveDayToLibrary_description"></textarea>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button onclick="saveDayToLibraryExecute();" id="newModuleFromLibrary" type="button" class="btn btn-primary">Speichern</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>