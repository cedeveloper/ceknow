<div id="learningroom-info">
    <?php echo $this->item->testmode_message;?>
    <h2><b id="learningroomID"><?php echo 'LR'.str_pad($this->item->id,8,'0',STR_PAD_LEFT);?></b></h2>
    <h4><?php echo $this->visible.' '.$this->item->title;?></h4>
    <p class="m-0"><b>Firma:</b> <?php echo $this->learningroom['classroom']->company_name;?></p>
    <p class="m-0"><b>Trainer:</b> <?php echo $this->learningroom['main_trainer_name']->company_name;?></p>
</div>