<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addStyleSheet('components/com_jclassroom/assets/css/jqcloud.css');
$doc->addStyleSheet('components/com_jclassroom/assets/css/unicode.css');
$doc->addStyleSheet('https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.css');
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/jqcloud-1.0.4.js');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
$doc->addScript('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js');
$doc->addScript('https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.js');

require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newPart.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newPartLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newDayLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newModuleLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newStudent.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/chooseStudent.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newDay.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/moveDay.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/editDay.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/moveModule.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/moveUnit.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/resultQuizz.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/saveDayToLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/saveModuleToLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/saveUnitToLibrary.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/comment.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/editTimeblock.php');
?>
<style>
canvas {
   max-width: 500px!important;
   max-height: 500px!important;
}
.theDate i {
  cursor: pointer;
}
#save {
 text-align: center;
}
#templatePreview {
   background-color: '#e1e1e1';
   padding: 15px;
}
.deleteButton {
   padding: 3px 7px;
   border-radius: 3px;
   cursor: pointer;
}
.classroomDays .btn-success {
   border-width: 5px;
}
#timeblocks {
   width: 100%;
   min-height: 200px;
   background-color: #b1b1b1;
   padding: 15px;
}
#timeblocks a {
   background-color: steelblue;
   z-index: 999;
   color: #fff;   
}
#timeblocks .timeblock {
   position: relative;
   padding: 5px;
   width: 100%;
   min-height: 170px;
   background-color: #e1e1e1;
   margin-bottom: 15px;
}
.timeblock a {
   margin-right: 10px;
   color: #fff;
   background-color: steelblue;
   width: 100%;
}
.timeblock.over,
#themes.over {
   border: 3px solid forestgreen;
}
#timeblocks .timeblock .theDate {
   font-weight: bold;
   position: absolute;
   display: block;
   left: -100px;
   top: 0px;
   width: 100px;
   text-align: center;
   background-color: lightsteelblue;
}
#themes {
   background-color: #e1e1e1;
   padding:10px;
   min-height: 400px;
}
#themes a {
   background-color: steelblue;
   color: #fff;
   margin-right: 5px;
}
.introImageThumb {
   height: 60px;
   width: 120px;
   background-position: center center;
   background-size: contain;
   background-repeat: no-repeat;
   display: inline-block;
   margin-right: 10px;
}
.fileRow a {
   font-size: 10px;
}
</style>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

<div id="learningroom-info">
      <h2><b><?php echo 'LR'.str_pad($this->item->id,8,'0',STR_PAD_LEFT);?></b><h2>
      <h4><?php echo $this->visible.' '.$this->item->title;?></h4>
   </div>
   <div class="buttonleiste mb-4">         
      <a class="btn btn-success text-white" onclick="Joomla.submitform('classroom.simpleSave')">Speichern</a>
      <a class="btn btn-secondary text-white" onclick="Joomla.submitform('classroom.save')">Speichern & Schließen</a>
      <?php if($this->item->id):?>
         <a class="btn btn-primary text-white" href="stage-classroom?id=<?php echo $this->item->id;?>" target="_blank"><i class="fa fa-eye"></i> Vorschau</a>
         <a id="exportLearningroom" onclick="exportLearningroom(<?php echo $this->item->id;?>);" class="btn btn-warning text-white"><i class="fa fa-upload"></i> Exportieren</a>
         <a id="exportLearningroom" onclick="libraryLearningroom(<?php echo $this->item->id;?>);" class="btn btn-info text-white" title="Speichern Sie den vollständigen Learningroom in der Bibliothek. Legen Sie fest, wer diesen Learningroom in der Bibliothek verwenden darf."><i class="fa fa-university"></i> In Bibliothek speichern</a>
         <?php if($this->usergroup == 'superuser'):?>
            <a id="exportTemplate" onclick="exportTemplate(<?php echo $this->item->id;?>);" class="btn btn-light border-dark  text-dark" title="Speichern Sie den vollständigen Learningroom als Vorlage."><i class="fa fa-database"></i> Als Vorlage speichern</a>  
         <?php endif; ?>
      <?php endif;?>
      <a id="retour" href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <?php if(!$this->item->id):?>
      <div class="alert alert-danger" role="alert">
        Um auf alle Parameter des Learningroom zugreifen zu können, müssen Sie zunächst speichern.
      </div>
   <?php endif;?>
   <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
         <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Basisdaten</a>
      </li>
      <?php if($this->item->id):?>
         <li class="nav-item">
            <a class="nav-link" id="changelog-tab" data-toggle="tab" href="#changelog" role="tab" aria-controls="changelog" aria-selected="false">Changelog</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="communication-tab" data-toggle="tab" href="#communication" role="tab" aria-controls="asp" aria-selected="false">Kommunikation</a>
         </li>
         <!--<li class="nav-item">
            <a class="nav-link" id="customer-tab" data-toggle="tab" href="#customer" role="tab" aria-controls="asp" aria-selected="false">Kunde</a>
         </li>-->
         <!--<li class="nav-item">
            <a class="nav-link" id="trainer-tab" data-toggle="tab" href="#trainer" role="tab" aria-controls="asp" aria-selected="false">Trainer</a>
         </li>-->
         <!--<li class="nav-item">
            <a class="nav-link" id="workflow-tab" data-toggle="tab" href="#workflow" role="tab" aria-controls="asp" aria-selected="false">Workflow</a>
         </li>-->
         <li class="nav-item">
            <a class="nav-link" id="asp-tab" data-toggle="tab" href="#asp" role="tab" aria-controls="asp" aria-selected="false">Teilnehmer</a>
         </li>
         <?php if($this->usergroup == 'customer' || $this->usergroup == 'trainer' || $this->usergroup == 'superuser') { ?>
         <li class="nav-item">
            <a class="nav-link" id="days-tab" data-toggle="tab" href="#days" role="tab" aria-controls="days" aria-selected="true">Inhalte</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="times-tab" data-toggle="tab" href="#times" role="tab" aria-controls="times" aria-selected="false">Struktur</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="material-tab" data-toggle="tab" href="#material" role="tab" aria-controls="material" aria-selected="false">Arbeitsmaterial</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="quizz-tab" data-toggle="tab" href="#quizz" role="tab" aria-controls="quizz" aria-selected="false">Auswertungen Quizz</a>
         </li>
         <?php } ?>
         <li class="nav-item">
            <a class="nav-link" id="freigabe-tab" data-toggle="tab" href="#freigabe" role="tab" aria-controls="freigabe" aria-selected="false">Freigabe</a>
         </li>
      <?php endif;?>
   </ul>
   <div class="tab-content" id="tabContent">
      <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
            <div class="form-horizontal mt-3">
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('title'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('title'); ?>
                    </div>
                    <div id="theRightSide" class="col-sm-3"></div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('main_trainer'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('main_trainer'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('co_trainer'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('co_trainer'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('created_by'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('created_by'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('companyID'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('companyID'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('description'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                        <?php echo $this->form->getInput('description'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('presentation'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                        <?php echo $this->form->getInput('presentation'); ?>
                        <br/>
                        <small>Roommodus = Der Learningroom wird komplett als Übersicht ohne Zeitzuordnung ausgegeben</small><br/>
                        <small>Timetable = Der Learningroom wird komplett als Übersicht mit Zeitzuordnung ausgegeben.</small><br/>
                        <small>Katalogmodus = Der Learningroom wird einzeln durchschaltbar ausgegeben.</small>
                    </div>
                </div>
                <!--<div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php //echo $this->form->getLabel('inventa_kursID'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php //echo $this->form->getInput('inventa_kursID'); ?>
                    </div>
                </div>
                <div class="form-group row hidden">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php //echo $this->form->getLabel('inventa_terminID'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php //echo $this->form->getInput('inventa_terminID'); ?>
                    </div>
                </div>-->
                <!--<div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php //echo $this->form->getLabel('showTo'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php //echo $this->form->getInput('showTo'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php //echo $this->form->getLabel('showToUser'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php //echo $this->form->getInput('showToUser'); ?>
                    </div>
                </div>-->
            </div>
      </div>
      <div class="tab-pane fade" id="changelog" role="tabpanel" aria-labelledby="changelog-tab">
            <div class="form-horizontal mt-3">
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('version'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('version'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('changelog'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('changelog'); ?>
                    </div>
                </div>
            </div>
      </div>
      <div class="tab-pane fade" id="communication" role="tabpanel" aria-labelledby="communication-tab">
         <div class="form-horizontal mt-3">
            <div class="row">
               <div class="col-12 col-sm-6">
                  <div class="form-group row">
                       <div class="col-12 col-sm-3 col-form-label">
                         <?php echo $this->form->getLabel('invitations'); ?>
                       </div>
                       <div class="col-12 col-sm-9">
                         <?php echo $this->form->getInput('invitations'); ?>
                       </div>
                   </div>
                   <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('invitationsBy'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('invitationsBy'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                       <div class="col-12 col-sm-3 col-form-label">
                         <?php echo $this->form->getLabel('offsetRememberMails'); ?>
                       </div>
                       <div class="col-12 col-sm-9">
                         <?php echo $this->form->getInput('offsetRememberMails'); ?>
                       </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('certificate'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('certificate'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('student_feedback'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('student_feedback'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('student_feedback_anonym'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('student_feedback_anonym'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('trainer_feedback'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('trainer_feedback'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('hardware_order'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('hardware_order'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('showChat'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('showChat'); ?>
                    </div>
                </div>
               </div>
               <div class="col-12 col-sm-6">
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('remember_template'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('remember_template'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('email_template'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('email_template'); ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('verification_template'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('verification_template'); ?>
                     </div>
                  </div>
                  <div id="templatePreview">
                     <?php //echo $this->item->invitationTemplate;?>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="customer" role="tabpanel" aria-labelledby="customer-tab">
         <div class="form-horizontal mt-3">
            
         </div>
      </div>
      <div class="tab-pane fade" id="workflow" role="tabpanel" aria-labelledby="workflow-tab">
         <div class="form-horizontal mt-3">
            
         </div>
      </div>
      <div class="tab-pane fade" id="trainer" role="tabpanel" aria-labelledby="trainer-tab">
         <div class="form-horizontal mt-3">
            <p>Auflistung aller Trainer aus dem Manager</p>
            <p>Möglichkeit zur manuellen Eingabe</p>
            <p><b>Workflow</b></p>
            <ul>
               <li>1. Traineranfrage -> Anfragetemplate -> global unter Kommunikation.</li>
               <li>2. Parametersteuerung -> Nicht geantwortet, hat zugesagt, hat abgesagt, Reservierungsbestätigung</li>
               <li>3. Template Reservierungsbestätigung -> Versand an Trainer, die Paramater Reservieungsbestätigung haben</li>
               <li>4. Registerreiter Kunde und Ansprechpartner (beim Kunden)</li>
               <li>5. Statusabfrage -> an Ansprechpartner "Findet das Training statt?". Template Statusabfrage. Status Learning = Reserviert.</li>
               <li>6. Status des Learningroom setzen, wenn Training stattfindet (durch Ansprechpartner) auf gebucht oder storno oder verschieben. Trainerbestellung an den ausgewählten Trainer verschoicken. Template für Trainerbestellung.</li>
               <li>7. Status der Trainerbestellung bestätigen "Trainer hat bestätigt" Status "GEbucht"</li>
               <li>7a. Bestellung der Unterlagen</li>
               <li>7b. Prüfen Verwaltung von Lernmaterial (Lieferant, Anfrage, Bestellung, Lieferung). Hinterlegen von Lernmaterial-Lizenzen beim Teilnehmer.</li>
               <li>8. Zusenden der Zugangsdaten zu ceLEarning nach erfolgter Trainerbestellung.</li>
               <li>9. Zusenden der Teilnehmereinladung und Teilnehmererinnerungen auch an den Trainer</li>
               <li>Auswahlprozess kann auh für mehrere (gleichberechtigte) Trainer gültig sein</li>
               <li>Templates für E-Mail sollten im Learningroom veränderbar sein.</li>
               <li>Speichern von E-Mails in der Datenbank</li>
               <li>Teilnehmerzertifikat -> Template für Teilenehmerzertifikat und E-Mail-Teilnehmerzertifkat</li>
               <li>Trainerfeedback</li>
            </ul>
         </div>
      </div>
      <div class="tab-pane fade" id="asp" role="tabpanel" aria-labelledby="asp-tab">
         <div class="form-horizontal mt-3">
            <small>Sie können folgenden Dateiformate hochladen: CSV<br/>
            Die maximale Uploadgröße beträgt 8MB je Upload.<br/>
            Bitte beachten Sie die korrekte Anordnung der Dateifelder in der CSV-Datei.</small>
            <div class="d-block mt-3">
               <button type="button" class="btn btn-warning text-white d-inline mb-1"id="openUploader">Teilnehmer über CSV-Upload erfassen</button>
               <button type="button" onclick="createStudent();" class="btn btn-info text-white d-inline mb-1">Teilnehmer manuell erfassen</button>
               <button type="button" onclick="chooseStudent();" class="btn btn-success text-white d-inline mb-1">Bestehende Personen einfügen </button>
               <a style="padding: 6px 10px;display: inline-block!important;margin-top: -1px;" href="index.php?option=com_jclassroom&task=classroom.printStudentlist&id=<?php echo $this->item->id;?>" target="_blank" class="btn btn-danger text-white d-inline mb-1">Teilnehmerliste erzeugen</a>
            </div>
            <div id="students-actions" class="d-block">
               <button type="button" class="btn btn-secondary text-white d-inline mt-1 mb-3" onclick="verificationToAllO();">Verifikation an alle Teilnehmer versenden</button>
               <button type="button" class="btn btn-primary text-white d-inline mt-1 mb-3" onclick="invitationToAllO();">Einladung an alle Teilnehmer versenden</button>
               <button type="button" class="border-dark btn btn-light text-dark d-inline mt-1 mb-3" onclick="rememberToAllO();">Erinnerung an alle Teilnehmer versenden</button>
            </div>
            <table id="students" class="table table-striped table-sm">
               <thead>
                  <tr>
                     <th>Name</th>
                     <th style="width: 300px;" class="text-center">Userstate<br/>Bemerkungen</th>
                     <th>Gebucht am</th>
                     <th>E-Mails</th>
                     <th>Aktionen</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  if($this->item->students):
                     $fileID = 0;
                     foreach($this->item->students as $student):
                        echo '<tr id="student'.$student['id'].'" class="student">';
                        switch($student['role']):
                           case 'Students':
                              $role = '<span class="badge badge-success text-white">Teilnehmer</span>';
                              break;
                           case 'Trainer':
                              $role = '<span class="badge badge-danger text-white">Trainer</span>';
                              break;
                           case 'Super Users':
                              $role = '<span class="badge badge-primary text-white">Systemadministrator</span>';
                              break;
                        endswitch;
                        echo '<td>
                        <a href="student-edit?layout=edit&id='.$student['studentID'].'" target="_blank">'.$student['name'].'</a> '.$role.' 
                        <br><i class="fa fa-envelope"></i> '.$student['email'];
                        if($student['company']):
                           echo '<br/>
                           <span style="font-size: 12px;font-weight: bold;text-style:italic;">'.$student['companyID'].' '.$student['company'].'</span>
                           </td>';
                        endif;
                        if($student['password']):
                           $state = '<i class="fa fa-check text-success"></i>';
                        else:
                           $state = '<i class="fa fa-ban text-danger"></i>';
                        endif;
                        echo '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Password besitzt.">';
                        echo $state;
                        echo '<br/><small style="display: inline-block;line-height: 14px;" id="comment'.$student['id'].'">'.$student['comment'].'</small>';
                        echo '</td>';
                        echo '<td>'.date('d.m.Y H:i', strtotime($student['created'])).'</td>';
                        echo '<td>
                        <b>Verifizierung am:</b> '.$student['lastVertify'].'<br/>
                        <b>Einladung am:</b> '.$student['lastInvitation'].'<br/>
                        <b>Erinnerung am:</b> '.$student['lastRemember'].'
                        </td>';
                        echo '<td>';
                        //if(!$student['lastVertify']):
                         echo '<i onclick="emailVertifyO('. $student['id'].');" title="E-Mail zur Passwortvergabe" class="fa fa-map-marker p-2"></i>';
                        //endif;
                        //if(!$student['lastInvitation']):
                        echo '<i onclick="emailInvitationO('. $student['id'].');" title="E-Mail-Einladung" class="fa fa-envelope p-2"></i>';
                        echo '<i onclick="emailRememberO('. $student['id'].');" title="E-Mail-Erinnerung" class="fa fa-bell p-2"></i>';
                        echo '<i onclick="comment('. $student['id'].');" title="Bemerkungen" class="fa fa-comment p-2"></i>';
                        //endif;
                        echo '<i onclick="deleteStudent('. $student['id'].');" title="Teilnehmer löschen" class="fa fa-trash-o p-2"></i>
                        </td>';
                        echo '</tr>';
                        $fileID++;
                     endforeach;
                  else:
                        echo '<tr id="noStudents">';
                        echo '<td colspan="5">Keine Teilnehmer gefunden.</td>';
                        echo '</tr>';
                  endif;
                  ?>
               </tbody>
            </table>
         </div>
    	</div>
      <div class="tab-pane fade" id="days" role="tabpanel" aria-labelledby="days-tab">
            <div class="form-horizontal mt-3">
               <div class="classroomActions mb-3">
                  <a class="btn btn-success text-white" onclick="addNewDay();"><i class="fa fa-plus"></i> Thema einfügen</a>
                  <a class="btn btn-info text-white" onclick="addDayFromLibrary();"><i class="fa fa-plus"></i> Thema aus Bibliothek einfügen</a>
               </div>
               <div class="classroomDays mb-3">
                  <?php if($this->item->cards): ?>
                     <?php foreach($this->item->cards as $card): ?>
                        <?php 
                           $date    = new Datetime($card->day);
                           $title   = '';
                           if($card->title):
                              $title   .= $card->title;
                           else:
                              $title   .=  '<br/>'.$date->format('d.m.Y');
                           endif;
                        ?>
                        <a 
                        id="loadDay<?php echo $card->id;?>" 
                        data-date="<?php echo date('d.m.Y', strtotime($card->day));?>" 
                        data-title="<?php echo $card->title;?>" 
                        class="btn mb-2" 
                        onclick="loadDay(<?php echo $this->item->id;?>,<?php echo $card->id;?>);"><?php echo $title;?>
                        </a>
                     <?php endforeach;?>
                  <?php endif; ?>
               </div>
               <?php $unitCounter = 0;?>
               <div id="structure">
                  
               </div>
            </div>
      </div>
      <div class="tab-pane fade" id="times" role="tabpanel" aria-labelledby="times-tab">
         <div class="form-horizontal mt-3">
            <a onclick="addTimeblock();" class="btn btn-success text-white">Zeitblock einfügen</a>
            <a onclick="reorderRoom();" class="btn btn-primary text-white">Struktur neu berechnen</a>
            <div class="row mt-3">
               <div class="col-12 col-sm-9">
                  <h3 id="theH3" style="font-size: 14px;">Zeitblöcke, in denen Sie Themen einfügen können</h3>
                  <div id="timeblocks">
                     <?php
                     if($this->item->timeblocks):
                        foreach($this->item->timeblocks as $timeblock):
                           echo '<div id="timeblock'.$timeblock->id.'" class="timeblock" style="background-color: '.$timeblock->background_color.';" data-color="'.$timeblock->background_color.'" data-title="'.date('d.m.Y',strtotime($timeblock->title)).'">';
                           echo '<span class="theDate">';
                           echo '<span class="theDateA">'.date('d.m.Y', strtotime($timeblock->title)).'</span><br/>';
                           echo '<i class="fa fa-pencil mr-2" onclick="editTimeblock('.$timeblock->id.');"></i>';
                           echo '<i class="fa fa-trash-o mr-2" onclick="deleteTimeblock('.$timeblock->id.');"></i>';
                           echo '<i class="fa fa-plus" title="Einen neuen Zeitblock am Ende einfügen" onclick="addTimeblock();"></i>';
                           echo '</span>';
                           if($this->item->timeblocks_themes):
                              foreach($this->item->timeblocks_themes as $theme):
                                 if($timeblock->id == $theme->timeblockID):
                                    echo '<a id="theme'.$theme->themeID.'" class="theme mb-2 btn" data-title="'.$theme->title.'" data-ordering="'.$theme->ordering.'">'.$theme->title.'</a>';
                                 endif;
                              endforeach;
                           endif;
                           echo '</div>';
                        endforeach;
                     endif;
                     ?>
                  </div>
               </div>
               <div class="col-12 col-sm-3">
                  <h3 id="themes-title" style="font-size: 14px;">Themen, die Sie in Zeitblöcke einfügen können</h3>
                  <div id="themes" class="themes mb-3">
                     <?php if($this->item->extractTimeblocks): ?>
                        <?php foreach($this->item->extractTimeblocks as $card): ?>
                           <?php 
                              $date    = new Datetime($card->day);
                              $title   = '';
                              $title   .= $card->title;
                           ?>
                           <a 
                           id="theme<?php echo $card->id;?>"
                           data-date="<?php echo date('d.m.Y', strtotime($card->day));?>" 
                           data-title="<?php echo $card->title;?>" 
                           data-id="<?php echo $card->id;?>"
                           data-ordering="<?php echo $card->ordering;?>"
                           class="theme btn mb-2"><?php echo $title;?> 
                           </a>
                        <?php endforeach;?>
                     <?php endif; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="global" role="tabpanel" aria-labelledby="global-tab">
         <div class="form-horizontal mt-3">
             <div class="form-group row">
                 <div class="col-12 col-sm-3 col-form-label">
                 <?php echo $this->form->getLabel('published'); ?>
                 </div>
                 <div class="col-12 col-sm-9">
                 <?php echo $this->form->getInput('published'); ?>
                 </div>
             </div>
             <div class="form-group row">
                 <div class="col-12 col-sm-3 col-form-label">
                 <?php echo $this->form->getLabel('id'); ?>
                 </div>
                 <div class="col-12 col-sm-9">
                 <?php echo $this->form->getInput('id'); ?>
                 </div>
             </div>
         </div>
      </div>
      <div class="tab-pane fade" id="material" role="tabpanel" aria-labelledby="material-tab">
         <div class="form-horizontal mt-3">
            <div class="row mt-2">
               <div class="col-12 col-sm-4">
                  <h5>Verzeichnisstruktur für Learningroom <?php echo $this->item->id;?></h5>
                  <div class="alert alert-danger p-1">Achtung! Sie haben hier vollen Zugriff auf die für diesen Learningroom auf dem Server gespeicherten Dateien. Gelöschte Dateien sind unwiderruflich verloren.</div>
                  <i class="fa fa-question-circle"></i> <br/><small style="display: inline-block; width: 100%;line-height: 14px;">Sie können nur Dateien aus den Ordnern löschen. Die Ordner selbst sind technisch notwendig und können nicht gelöscht werden.</small>
                  <div id="folderTree" class="card bg-light p-1" style="font-size: 12px;">
                     <?php
                     if($this->filesOP):
                        foreach($this->filesOP as $file):
                           echo '<p class="mt-1 mb-1 p-1" style="background-color: #a1a1a1;"><i class="fa fa-folder-o"></i> '.$file['name'].'</p>';
                           if($file['subfiles']):
                              foreach($file['subfiles'] as $subfile):
                                 $theName = str_replace('.', '', $subfile['name']);
                                 echo '<p id="file'.$theName.'" class="mt-1 mb-1 ml-5 p-1" style="background-color: #d1d1d1;"><i class="fa fa-file-o"></i> '.$subfile['name'].'<i class="pt-1 fa fa-trash-o float-right" title="Diese Datei löschen" onclick="deleteFileFD(&quot;'.$subfile['path'].'&quot;,&quot;'.$theName.'&quot;);"></i></p>';
                              endforeach;
                           endif;
                        endforeach;
                     endif;
                     ?>
                  </div>
               </div>
               <div class="col-12 col-sm-8">
                  <h5>Uploadfunktionen für Learningroom <?php echo $this->item->id;?></h5>
                  <div class="alert alert-success p-1">Laden Sie über die nachfolgenden Möglichkeiten Daten für diesen Learningroom auf den Server.</div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('material'); ?>
                     </div>
                     <div class="col-12 col-sm-10">
                         <button type="button" class="btn btn-info text-white d-inline mb-1"id="openUploaderM">Material erfassen</button>
                        <?php
                        if($this->item->files):
                           $iC = 1;
                           foreach($this->item->files as $file):
                              if($file->type == 'material'):
                                 echo '<div id="file'.$file->id.'" class="row">';
                                    echo '<div class="col-12">';
                                       echo '<div style="display: inline-block; width: 100%;" class="bg-light p-1 mb-1 border-dark">';
                                          echo $iC.'. ';
                                          echo '<a href="'.$file->path.'" target="_blank">'.$file->filename.'</a>';
                                          echo '<a style="cursor: pointer;border-radius: 3px;padding: 3px 4px;" class="bg-danger text-white float-right" onClick="deleteFile('.$file->id.');"><i class="fa fa-trash-o"></i></a>';
                                       echo '</div>';
                                    echo '</div>';
                                 echo '</div>';
                                 $iC ++;
                              endif;
                           endforeach;
                        endif;
                        ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <label>Reserve</label>
                     </div>
                     <div class="col-12 col-sm-10">
                        <button type="button" class="btn btn-info text-white d-inline mb-1"id="openUploaderR">Reservematerial erfassen</button>
                        <?php
                        if($this->item->files):
                          $iC = 1;
                           foreach($this->item->files as $file):
                              if($file->type == 'reserve'):
                                 echo '<div class="row">';
                                    echo '<div id="file'.$file->id.'" class="col-12">';
                                       echo '<div class="bg-light p-1 mb-1 border-dark">';
                                          echo '<div class="row">';
                                             echo '<div class="col-8">';
                                                echo $iC.'. ';
                                                echo '<a href="'.$file->path.'" target="_blank">'.$file->filename.'</a>';
                                                echo '</div>';
                                                echo '<div class="col-2">';
                                                   echo $file->folder;
                                                echo '</div>';
                                                echo '<div class="col-2">';
                                                if($file->published == 0):
                                                   $hideStage     = 'd-inline-block';
                                                   $hideUnstage   = 'd-none';
                                                else:
                                                   $hideStage     = 'd-none';
                                                   $hideUnstage   = 'd-inline-block';
                                                endif;
                                                echo '<a id="stageFile_'.$file->id.'" onclick="stageFile('.$file->id.');" class="btn btn-success '.$hideStage.' text-white btn-sm">Freigeben</a>';
                                                echo '<a id="unstageFile_'.$file->id.'" onclick="unstageFile('.$file->id.');" class="btn btn-danger '.$hideUnstage.' text-white btn-sm">Verstecken</a>';
                                                echo '<a class="deleteButton bg-danger text-white pl-1 pr-1 float-right" onClick="deleteFile('.$file->id.');"><i class="fa fa-trash-o"></i></a>';
                                             echo '</div>';
                                          echo '</div>';
                                       echo '</div>';
                                    echo '</div>';
                                 echo '</div>';
                                 $iC++;
                              endif;
                           endforeach;
                        endif;
                        ?>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        Dateien für Teilnehmer
                     </div>
                     <div class="col-12 col-sm-10">
                        <table id="students" class="table table-striped table-sm">
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>Firma</th>
                                 <th></th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php
                                 if($this->item->students):
                                 $fileID = 0;
                                 foreach($this->item->students as $student):
                                    echo '<tr id="student'.$student['id'].'">';
                                    echo '<td><a href="student-edit?layout=edit&id='.$student['id'].'" target="_blank">'.$student['name']. '</a></td>';
                                    echo '<td>'.$student['company'].'</td>';
                                    echo '<td>';
                                    echo '<input type="file" name="jform[material_student]['.$student['id'].'][]" value="" />';
                                    if($this->item->files):
                                       foreach($this->item->files as $file):
                                          if($file->type == 'student' && ($file->studentID == $student['id'])):
                                             echo '<br/><div id="file'.$file->id.'"><a target="_blank" href="'.$file->path.'">'.$file->filename.'</a>';
                                             echo '<a class="bg-danger text-white pl-1 pr-1 float-right" onClick="deleteFile('.$file->id.');"><i class="fa fa-trash-o"></i></a></div>';
                                          endif;   
                                       endforeach;
                                    endif;
                                    echo '</td>';
                                    echo '</tr>';
                                    $fileID++;
                                 endforeach;
                              else:
                                    echo '<tr>';
                                    echo '<td colspan="3">Keine Teilnehmer gefunden.</td>';
                                    echo '</tr>';
                              endif;
                              ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="quizz" role="tabpanel" aria-labelledby="quizz-tab">
         <div class="form-horizontal mt-3">
            <a class="btn btn-success text-white mb-3">Gesamtauswertung anzeigen</a>
            <?php require_once(JPATH_COMPONENT.'/views/classroom/tmpl/result.php');?>
         </div>
      </div>
      <div class="tab-pane fade" id="freigabe" role="tabpanel" aria-labelledby="freigabe-tab">
         <div class="form-horizontal mt-3">
            <div class="form-horizontal mt-3">
               <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('visibleTo'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('visibleTo'); ?>
                    </div>
               </div>
               <?php
               if($this->item->visibleTo == 1):
                  echo 'Link zum öffentlichen Learningroom:<br/>';
                  echo '<a href="stage-classroom?id='.$this->item->id.'">stage-classroom?id='.$this->item->id.'</a>';
               endif;
               ?>
               <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('showTo'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('showTo'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('showToUser'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('showToUser'); ?>
                    </div>
                </div> 
            </div>
         </div>
      </div>
    </div>
<input type="hidden" id="unitCounter" value="<?php echo $unitCounter;?>" />
<input type="hidden" id="dayID" value="" />
<input type="hidden" id="classroomID" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="showTo" value="<?php echo $this->item->showTo;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<style>
  #editContent #editor {
    height: 450px!important;
    overflow-y: scroll;
  }
</style>
<?php
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/editContent.php');
?>
<div id="wait">
   Daten werden geladen
   <img width="80px" src="images/logo_solo.png" />
</div>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<form action="index.php?option=com_jclassroom&task=classroom.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
   <input id="uploader" title="file input" multiple="true" type="file" accept="" name="uploadCSV" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom.loadFile" method="post" name="uploaderFormUnit" id="uploaderFormUnit" enctype="multipart/form-data">
   <input id="uploaderUnit" title="file input" type="file" accept="" name="uploadUnit" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="formUnitID" name="unitID" value="" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom.loadIntroImage" method="post" id="uploaderImageUnit" enctype="multipart/form-data">
   <input id="uploadImageUnit" title="file input" type="file" accept="" name="uploadImageUnit" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="imageUnitID" name="imageUnitID" value="" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom.loadFileM" method="post" name="uploaderFormMaterial" id="uploaderFormMaterial" enctype="multipart/form-data">
   <input id="inputMaterial" title="file input" type="file" accept="" multiple name="inputMaterial[]" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom.loadFileR" method="post" name="uploaderFormReserve" id="uploaderFormReserve" enctype="multipart/form-data">
   <input id="inputReserve" title="file input" type="file" accept="" multiple="" name="inputReserve[]" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>
<script type="text/javascript">
$(document).ready(function() {
   var h1 = jQuery(window).height();
   var w1 = jQuery(window).width();
   jQuery('#editContent .modal-dialog').css('width', w1 -300).css('max-width', '1280px');
   setRights();  
});

$('.colorpicker').spectrum({

});

var check_email = false;
   var form = $("#adminForm");
      $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[email]': {
            required: true,
            email: true
         },
         'jform[presentation]': {
            required: true
         }
         
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[presentation]': 'Bitte wählen Sie eine Ausgabeart aus.'
      },
      submitHandler: function(form) {
         var checkIndicator   = 0;
         jQuery('.indicator').each(function() {
            if(jQuery(this).val() == 1) {
               checkIndicator = 1;
            }
         });
         if(checkIndicator == 1) {
            alert('In der Tageskarte gibt es nicht gesicherte Änderungen');
            return false;
         }
         form.submit();
      }
   });
   var scrollP = 0;
   var originW = 0;
   $('#times-tab').on('click', function() {
      originW = $('#theRightSide').outerWidth();
   });
   $(document).on("scroll", function() {
      scrollP = $(document).scrollTop();
      if(scrollP > 300) {
         $('#themes').css('position','fixed').css('top', '130px').css('width', originW + 'px');
         $('#themes-title').css('position','fixed').css('top', '110px').css('width', originW + 'px');
      }
      if(scrollP <= 300) {
         $('#themes').css('position', 'relative').css('top', '0px').css('width','auto');
         $('#themes-title').css('position','relative').css('top', '0px').css('width', 'auto');
      }
   });
   function exportLearningroom(id) {
      if(confirm("Soll der Learningroom mit der ID " + id + " als JSON-Datei exportiert werden?") == true) {
         jQuery.ajax({
             url: "index.php?option=com_jclassroom&task=classrooms.export",
             data: {id:id},
             method: 'GET',
             xhrFields: {
                 responseType: 'text'
             },
             //dataType: 'json',
             success: function( data ) {
                 var element = document.createElement('a');
                 //element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                 element.setAttribute('href', data);
                 element.setAttribute('download', 'learningroom' + id + '.json');
                 element.style.display = 'none';
                 document.body.appendChild(element);

                 element.click();

                 document.body.removeChild(element);
             }
         });
      }
   }
   function libraryLearningroom(id) {
      if(confirm("Soll der Learningroom mit der ID " + id + " in der Bibliothek gespeichert werden?") == true) {
         jQuery.ajax({
             url: "index.php?option=com_jclassroom&task=classroom.libraryLearningroom",
             data: {id:id},
             method: 'POST',
             //dataType: 'json',
             success: function( data ) {
                 console.log(data);
             }
         });
      }
   }
   function exportTemplate(id) {
      if(confirm("Soll der Learningroom mit der ID " + id + " als Vorlage gespeichert werden?") == true) {
         jQuery('#wait').css('display', 'block');
         jQuery.ajax({
             url: "index.php?option=com_jclassroom&task=classroom.exportTemplate",
             data: {id:id},
             method: 'POST',
             //dataType: 'json',
             success: function( data ) {
                 alert('Der Learningroom ' + id + ' wurde erfolgreich mit der ID ' + data + ' als Vorlage gespeichert');
             }
         });
         jQuery('#wait').css('display', 'none');
      }
   }
   function loadTemplate() {
      var invitationTemplate = $('#jform_email_template option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadInvitationTemplate",
         data: {invitationTemplate:invitationTemplate},
         //dataType: 'json',
         success: function( data ) {
            $('#templatePreview').html(data);
         }
      });
   }
   function loadTermine() {
      var kursID = jQuery('#jform_inventa_kursID option:selected').val();
      loadKursData(kursID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadTermine",
         data: {kursID:kursID},
         dataType: 'json',
         success: function( data ) {
            if(data[0]['title']) {
               alert(data[0]['title']);
            } else {
               jQuery(data).each(function(e, value) {
                  console.log(value['beginn']);
                  var o = new Option(value['beginn'] + ' - ' + value['ende'], value['id']);
                  /// jquerify the DOM object 'o' so we can use the html method
                  //$(o).html("option text");
                  jQuery("#jform_inventa_terminID").append(o);
               });
            }
         }
      });
   }
   function loadKursData(kursID) {
      tinyMCE.activeEditor.execCommand('mceInsertContent',false,'');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadKursData",
         data: {kursID:kursID},
         //dataType: 'json',
         success: function( data ) {
            tinyMCE.activeEditor.execCommand('mceInsertContent',false,'');
            tinyMCE.activeEditor.execCommand('mceInsertContent',false,data);
         }
      });
   }
   function deleteStudent(id) {
      if(confirm('Soll dieser Teilnehmer aus dem Klassenraum entfernt werden?') == true) {
         var classroomID = jQuery('#classroomID').val();
         var cStudents = 0;
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteStudent",
            data: {studentID:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               jQuery('#students tr#student' + id).remove();
               cStudents = $('#students .student').length;
               if(cStudents == 0) {
                  $('#students').append(
                     '<tr id="noStudents">' + 
                     '<td colspan="5">Keine Teilnehmer gefunden.</td>' +
                     '</tr>'
                  );
               }
            }
         });
      }
   }
   function stageFile(id) {
      if(confirm('Soll diese Datei für die Teilnehmer freigegeben werden?') == true) {
         var classroomID = jQuery('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.stageFile",
            data: {id:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               $('#stageFile_' + id).removeClass('d-inline-block').addClass('d-none');
               $('#unstageFile_' + id).removeClass('d-none').addClass('d-inline-block');
            }
         });
      }
   }
   function unstageFile(id) {
      if(confirm('Soll diese Datei für die Teilnehmer versteckt werden?') == true) {
         var classroomID = jQuery('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.unstageFile",
            data: {id:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               $('#stageFile_' + id).removeClass('d-none').addClass('d-inline-block');
               $('#unstageFile_' + id).removeClass('d-inline-block').addClass('d-none');
            }
         });
      }
   }
   function setRights() {
      var set = $('#jform_showTo option:selected').val();
      if(set != 2) {
         $('#jform_showToUser-lbl').hide();
         $('#jform_showToUser').hide();
         $('#jform_showToUser').next(".select2-container").hide();
      } 
      if(set == 2) {
         $('#jform_showToUser-lbl').show();
         $('#jform_showToUser').next(".select2-container").show();
      }
   }
   jQuery('#retour').on('click', function(e) {
      e.preventDefault();
      var href = jQuery(this).attr('href');
      var checkIndicator   = 0;
      jQuery('.indicator').each(function() {
         if(jQuery(this).val() == 1) {
            checkIndicator = 1;
         }
      });
      if(checkIndicator == 1) {
         alert('In der Tageskarte gibt es nicht gesicherte Änderungen');
         return false;
      }
      location.href = href;
   });
   //ADD SOMETHING
   function addDays() {
      var $this   = jQuery(this);
      var start   = jQuery('#jform_fromDate').val();
      var end   = jQuery('#jform_toDate').val();
      if(!start || !end) {
      alert('Bitte wählen Sie ein Anfangs- und Enddatum für diesen Klassenraum aus.');
      return false;
      }
      jQuery('#structure').empty();
      jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=classroom.addDays",
        data: {start:start, end:end},
        //dataType: 'json',
        success: function( data ) {
          jQuery('#structure').append(data);
        }
      });
   }
   function addNewDay() {
      jQuery('#newDay').modal('show');
   }
   function addDayFromLibrary() {
      var dayID  = jQuery('#dayID').val();
      if(!dayID) {
         alert("Bitte wählen Sie einen Tag aus, in den die Elemente aus der Bibliotek eingefügt werden sollen.");
         return false;
      }
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.addDayFromLibrary",
         data: {dayID:dayID},
         dataType: 'json',
         success: function( data ) {
            $('#addDayFromLibraray').empty();
            if(data) {
               jQuery(data).each(function(e, value) {
                  $("#addDayFromLibraray").append(new Option(value['title'] + ' [ ' + value['description'] + ' ]', value['id']));
               });
            }   
            jQuery('#addDayFromLibraray').select2({width: '100%'});
         }
      });
      jQuery('#newDayLibrary').modal('show');
   }
   function addModuleFromLibrary(dayID) {
      var day = jQuery('#structure .card').length;
      if(day == 0) {
         alert('Bitte laden Sie zunächst eine Tageskarte');
         return false;
      }
      $('#newModuleToDayID').val(dayID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.addModuleFromLibrary",
         data: {},
         dataType: 'json',
         success: function( data ) {
            $('#addModuleFromLibraray').empty();
            if(data) {
               jQuery(data).each(function(e, value) {
                  $("#addModuleFromLibraray").append(new Option(value['title'] + ' [ ' + value['description'] + ' ]', value['id']));
               });
            }   
            jQuery('#addModuleFromLibraray').select2({width: '100%'});
         }
      });
      jQuery('#newModuleLibrary').modal('show');
   }
   // Add the choosen day to dayID
   function newDayFromLibraray() {
      var classroomID      = $('#classroomID').val();
      var dayID            = $('#dayID').val();
      var insertDayID      = $('#addDayFromLibraray option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.insertDayFromLibrary",
         data: {classroomID:classroomID,dayID:dayID,insertDayID:insertDayID},
         //dataType: 'json',
         success: function( data ) {
            loadDay(classroomID, dayID);
         }
      });
      
      jQuery('#newDayLibrary').modal('hide');
   }
   // Add the choosen module to new dayID
   function newModuleFromLibraray() {
      var classroomID      = $('#classroomID').val();
      var dayID            = $('#dayID').val();
      var insertModuleID   = $('#addModuleFromLibraray option:selected').val();
      var newModuleID      = 0;
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.insertModuleFromLibrary",
         data: {classroomID:classroomID,dayID:dayID,insertModuleID:insertModuleID},
         //dataType: 'json',
         success: function( data ) {
            newModuleID = data;
            renderNewModule(newModuleID);
         }
      });
      
      jQuery('#newModuleLibrary').modal('hide');
   }
   var newModuleID = 0;
   // Add the choosen module to new dayID
   function newUnitFromLibraray() {
      var classroomID      = $('#classroomID').val();
      var insertUnitID     = $('#addUnitFromLibrary option:selected').val();
      newModuleID          = $('#newUnitLibrarayToModuleID').val();
      var dayID            = $('#newUnitLibrarayToDayID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.insertUnitFromLibrary",
         data: {classroomID:classroomID,newModuleID:newModuleID,dayID:dayID,insertUnitID:insertUnitID},
         dataType: 'json',
         success: function( data ) {
            newModuleID = data['moduleID'];
            getType(data['typeID'], 1, data['unitID']);
         }
      });
      
      jQuery('#newPartLibrary').modal('hide');
   }
   function renderNewModule(newModuleID) {
      var classroomID      = $('#classroomID').val();
      var dayID            = $('#dayID').val();
      jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=classroom.addModule",
        data: {newModuleID:newModuleID,library:1,classroomID:classroomID,dayID:dayID},
        //dataType: 'json',
        success: function( data ) {
            jQuery('#cardFor' + dayID + ' .card-body').append(data);
            jQuery('.select2').select2({width: '300px'});
        }
      });
   }
   function saveNewClassRoomDay() {
      var day     = jQuery('#newClassroomDay').val();
      var title   = jQuery('#newClassroomTitle').val();
      var classroomID = jQuery('#classroomID').val();
      jQuery('#newDay').modal('hide');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.addNewDay",
         data: {classroomID:classroomID,day:day,title:title},
         //dataType: 'json',
         success: function( data ) {
            var textR = data.substring(0,7);
            if(textR == 'Fehler:') {
               alert(data);
            } else {
               if(title) {
                  showT = title;
               } else {
                  showT = day;
               }
               jQuery('.classroomDays').append('<a id="loadDay' + data + '" class="btn mb-2" onclick="loadDay(' + classroomID + ',' + data + ');">' + showT + '</a>');
               $('#themes').append(
                  '<a id="theme' + data + '" class="theme mb-2 btn ui-draggable ui-draggable-handle" data-title="' + showT + '" data-ordering="" style="position: relative;" data-id="' + data + '">' + 
                  showT + 
                  '</a>'
               );
               jQuery(".theme").draggable({ 
                  cursor: "crosshair", 
                  revert: "invalid"
               });
            }
            jQuery('#newClassroomDay').val('');
            $('#newClassroomTitle').val('');
         }
      });
   }
   function addModule(id,saveFromLibrary) {
      var dayID            = jQuery('#dayID').val();
      var classroomID      = jQuery('#classroomID').val();
      var copyFromModuleID = jQuery('#addModuleFromLibraray option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.addModule",
         data: {dayID:dayID,classroomID:classroomID,copyFromModuleID:copyFromModuleID,saveFromLibrary: saveFromLibrary},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#cardFor' + dayID + ' .card-body').append(data);
            if(saveFromLibrary == 1) {
                jQuery('#newModuleLibrary').modal('hide');
            }
         }
      });
   }
   function addUnit(moduleID) {
      var dayID   = $('#dayID').val();
      jQuery('.moduleContent').each(function() {
        jQuery(this).removeClass('show').addClass('hide');
        jQuery(this).parent('.module').find('.moduleHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      });
      openModule(moduleID);
      jQuery('#newPartToModuleID').val(moduleID);
      jQuery('#newPartToDayID').val(dayID);
      jQuery('#newPart_dialog').modal();
   }
   function addUnitFromLibrary(moduleID) {
      var dayID   = $('#dayID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.addUnitFromLibrary",
         data: {},
         dataType: 'json',
         success: function( data ) {
            $('#addUnitFromLibrary').empty();
            if(data) {
               jQuery(data).each(function(e, value) {
                  $("#addUnitFromLibrary").append(new Option(value['title'], value['id']));
               });
            }
            jQuery("#addUnitFromLibrary").select2("destroy");
            jQuery("#addUnitFromLibrary").select2({width: '100%'});
         }
      });
      jQuery('#newUnitLibrarayToModuleID').val(moduleID);
      jQuery('#newUnitLibrarayToDayID').val(dayID);
      jQuery('#newPartLibrary').modal();
   }   
   function editContent(unitID) {
      var content = jQuery('#unit' + unitID).find(' .contentHTML').html();
      tinyMCE.get('jform_content').setContent(content);
      jQuery('#editContentUnitID').val(unitID);
      jQuery('#editContent').modal();
   }
   function editContentModule() {
      //jQuery('#save').fadeIn(200);
      var content = tinyMCE.get('jform_content').getContent();
      var unitID  = jQuery('#editContentUnitID').val();
      jQuery('#unit' + unitID).find(' .contentHTML').html(content);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      saveUnit(unitID);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
      jQuery('#editContent').modal('hide');
      hideSave();
       //jQuery('#save').fadeOut(200);
   }
   function addTable() {
      var content = '<table style="width: 100%;height:50px;" class="table table-bordered"><tr><td></td></tr></table>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function addTestlevel() {
      var content = '<table style="width: 100%; height: 155px;" class="table table-bordered">' +
      '<tbody>' +
      '<tr>' +
         '<td style="width: 20%;"><strong>Schwierigkeitsgrad</strong></td>' +
         '<td><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></td>' +
      '</tr>' +
      '<tr>' +
         '<td style="width: 20%;"><strong>Übungsinhalte</strong></td>' +
         '<td>' +
            '<ul>' +
               '<li>Ordner erstellen</li>' +
               '<li>Dateien verschieben</li>' +
               '<li>Kommentare eingeben</li>' +
            '</ul>' +
         '</td>' +
      '</tr>' +
      '<tr>' +
         '<td style="width: 20%;"><strong>Übungsdatei</strong></td>' +
         '<td>&nbsp;hotel05.html</td>' +
      '</tr>' +
      '<tr>' +
         '<td style="width: 20%;"><strong>Ergebnisdatei</strong></td>' +
         '<td>hotel05-e.html</td>' +
      '</tr>' +
   '</tbody>' +
   '</table>';
   tinyMCE.get('jform_content').setContent(content);
   }
   function addCode() {
      var content = '<!-- HTML generated using hilite.me --><div style="background: #f8f8f8; overflow:auto;width:auto;font-size:18px;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008000; font-weight: bold">&lt;div</span> <span style="color: #7D9029">id=</span><span style="color: #BA2121">&quot;&quot;</span> <span style="color: #7D9029">class=</span><span style="color: #BA2121">&quot;&quot;</span><span style="color: #008000; font-weight: bold">&gt;&lt;/div&gt;</span></pre></td></tr></table></div>'
      tinyMCE.get('jform_content').setContent(content);
   }
   function addRow() {
      var content = '<p></p>';
      tinyMCE.get('jform_content').execCommand('mceInsertContent', false,content);;
   }
   function closeEditContentModule() {
      jQuery('#editContent').modal('hide');
   }
   function getType(typ, library, unitID) {
      jQuery('#wait').css('display', 'block');
      var classroomID   = jQuery('#classroomID').val();
      var dayID         = jQuery('#dayID').val();
      var moduleID      = jQuery('#newPartToModuleID').val();
      if(moduleID == '') {
         moduleID = newModuleID;
      }
      var unitCounter   = jQuery('#unitCounter').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.getTemplate",
         data: {library:library,typ:typ, classroomID:classroomID,dayID:dayID, moduleID:moduleID, unitCounter:unitCounter,unitID:unitID},
         //dataType: 'json',
         success: function( data ) {
            //jQuery('#structure #cardFor' + dayID + ' .card-body .cardMessage').remove();
            jQuery('#module' + moduleID + ' .moduleContent > ul').append(data);
            //jQuery('.summernote').summernote();
            jQuery('.select2').select2({width: '100%'});
            jQuery('#wait').css('display', 'none');
         }
      });
   }
   function chooseStudent() {
      jQuery('#chooseStudent').modal('show');
   }
   //DELETE
   function deleteDay(dayID) {
      if(confirm('Soll dieses Thema gelöscht werden?') == true) {
         var classroomID = $('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteDay",
            data: {classroomID:classroomID,dayID:dayID},
            //dataType: 'json',
            success: function( data ) {
               $('#theme' + dayID).remove();
               jQuery('#cardFor' + dayID).remove();
               jQuery('#idFor' + dayID).remove();
               jQuery('#loadDay' + dayID).remove();
            }
         });
      }
   }
   function deleteModule(moduleID) {
      if(confirm('Soll dieses Modul gelöscht werden?') == true) {
         jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=classroom.deleteModule",
           data: {moduleID:moduleID},
           //dataType: 'json',
           success: function( data ) {
             jQuery('#module' + moduleID).remove();
           }
         });
      }
   }
   function deleteUnit(unitID) {
      if(confirm('Soll diese Unit gelöscht werden?') == true) {
         jQuery('#wait').css('display', 'block');
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteUnit",
            data: {unitID:unitID},
            //dataType: 'json',
            success: function( data ) {
               jQuery('#unit' + unitID).remove();
               jQuery('#wait').css('display', 'none');
            }
         });
      }
   }
   // OPEN AUTOMATIC
   function openDay(id) {
      if(jQuery('#cardFor' + id).hasClass('openDay')) {
         jQuery('#cardFor' + id).removeClass('openDay');
         jQuery('#cardFor' + id + ' .open').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      } else {
         jQuery('#cardFor' + id).addClass('openDay');
         jQuery('#cardFor' + id + ' .open').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      }
   }
   function openModule(id) {
      if(jQuery('#moduleContent' + id).hasClass('hide')) {
         jQuery('#moduleContent' + id).removeClass('hide');
         jQuery('#moduleContent' + id).addClass('show');
         jQuery('#module' + id + ' .moduleHeader .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      } else {
         jQuery('#moduleContent' + id).removeClass('show');
         jQuery('#moduleContent' + id).addClass('hide');
         jQuery('#module' + id + ' .moduleHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      }
   }
   function openUnit(id) {
      if(jQuery('#unitContent' + id).hasClass('openUnit')) {
         jQuery('#unitContent' + id).removeClass('openUnit');
         jQuery('#unit' + id + ' .sectionHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      } else {
         jQuery('#unitContent' + id).addClass('openUnit');
         jQuery('#unit' + id + ' .sectionHeader .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      }
   }
   // HIDE SOMETHING
   function hideDay(dayID) {
      var published  = 0;
      var classHide  = 'fa-eye';
      var text       = '';
      if(jQuery('#cardFor' + dayID + ' .card-header .card-header-action').find('.hide').hasClass('fa-eye-slash')) {
         published   = 1;
         classHide   = 'fa-eye';
         removeClass = 'fa-eye-slash';
         text        = 'Solle dieser Tag aktiviert werden?';
      } else {
         published   = 0;
         classHide   = 'fa-eye-slash';
         removeClass = 'fa-eye';
         text        = 'Solle dieser Tag deaktiviert werden?';
      }
      if(confirm(text) == true) {
         
         jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=classroom.hideDay",
           data: {dayID:dayID,published:published},
           //dataType: 'json',
           success: function( data ) {
             jQuery('#cardFor' + dayID + ' .card-header .card-header-action').find('.hide').removeClass(removeClass).addClass(classHide);
           }
         });
      }
   }
   function hideModule(moduleID) {
      var published  = 0;
      var classHide  = 'fa-eye';
      var text       = '';
      if(jQuery('#module' + moduleID).find('.hide').hasClass('fa-eye-slash')) {
         published   = 1;
         classHide   = 'fa-eye';
         removeClass = 'fa-eye-slash';
         text        = 'Solle dieses Modul aktiviert werden?';
      } else {
         published   = 0;
         classHide   = 'fa-eye-slash';
         removeClass = 'fa-eye';
         text        = 'Solle dieses Modul deaktiviert werden?';
      }
      if(confirm(text) == true) {
         
         jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=classroom.hideModule",
           data: {moduleID:moduleID,published:published},
           //dataType: 'json',
           success: function( data ) {
             jQuery('#module' + moduleID).find('.hide').removeClass(removeClass).addClass(classHide);
           }
         });
      }
   }
   function hideUnit(unitID) {
      var published  = 0;
      var classHide  = 'fa-eye';
      var text       = '';
      if(jQuery('#unit' + unitID).find('.hide').hasClass('fa-eye-slash')) {
         published   = 1;
         classHide   = 'fa-eye';
         removeClass = 'fa-eye-slash';
         text        = 'Solle diese Unit aktiviert werden?';
      } else {
         published   = 0;
         classHide   = 'fa-eye-slash';
         removeClass = 'fa-eye';
         text        = 'Solle diese Unit deaktiviert werden?';
      }
      if(confirm(text) == true) {
         
         jQuery.ajax({
           type: "POST",
           url: "index.php?option=com_jclassroom&task=classroom.hideUnit",
           data: {unitID:unitID,published:published},
           //dataType: 'json',
           success: function( data ) {
             jQuery('#unit' + unitID).find('.hide').removeClass(removeClass).addClass(classHide);
           }
         });
      }
   }
   // SAVE SOMETHING
   function saveModule(moduleID) {
      jQuery('#save').fadeIn(200);
      var title      = jQuery('#module' + moduleID).find('.moduleTitle').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveModule",
         data: {moduleID:moduleID,title:title},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#saveIndicatorModule' + moduleID).val(0);
            jQuery('#module' + moduleID + ' .moduleHeader').find('.save').removeClass('text-danger').addClass('text-success');
            hideSave();
         }
      });
   }
   function saveUnit(unitID) {
      jQuery('#save').fadeIn(200);
      var title      = jQuery('#unit' + unitID).find('.title').val();
      var duration   = jQuery('#unit' + unitID).find('.duration').val();
      var content    = jQuery('#unit' + unitID).find('.contentHTML').html();
      var link       = jQuery('#unit' + unitID).find('.link').val();
      var quizz      = jQuery('#unit' + unitID).find('.quizz option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveUnit",
         data: {unitID:unitID,title:title,duration:duration,content:content,link:link,quizz:quizz},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#saveIndicatorUnit' + unitID).val(0);
            jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-danger').addClass('text-success');
            hideSave();
         }
      });
   }
   function saveDayLibrary(dayID) {
      if(confirm('Soll dieser Tag in Ihrer Bibliotek gespeichert werden?') == true) {
         var title = $('.card').find('.card-header h2').html();
         $('#saveDayToLibrary_title').val(title);
         jQuery('#saveDayToLibrary').modal('show');
         
      }
   }
   function saveDayToLibraryExecute() {
      var dayID = $('#dayID').val();
      var title = $('#saveDayToLibrary_title').val();
      var desc  = $('#saveDayToLibrary_description').val();
      jQuery('#saveDayToLibrary').modal('hide');
      jQuery('#save').fadeIn(200);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveDayLibrary",
         data: {dayID:dayID,title:title,desc:desc},
         //dataType: 'json',
         success: function( data ) {
            $('#saveDayToLibrary_title').val('');
            $('#saveDayToLibrary_description').val('');
            hideSave(); 
         }
      });
   }
   function saveModuleLibrary(moduleID) {
      if(confirm('Soll dieses Modul in Ihrer Bibliotek gespeichert werden?') == true) {
         var moduleTitle = $('#module' + moduleID).find('.moduleTitle').val();
         $('#saveModuleIDToLibrary').val(moduleID);
         $('#saveModuleToLibrary_title').val(moduleTitle);
         jQuery('#saveModuleToLibrary').modal('show');
      }
   }
   function saveModuleToLibraryExecute() {
      var moduleID   = $('#saveModuleIDToLibrary').val();
      var title      = $('#saveModuleToLibrary_title').val();
      var desc       = $('#saveModuleToLibrary_description').val();
      jQuery('#saveModuleToLibrary').modal('hide');
      jQuery('#save').fadeIn(200);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveModuleLibrary",
         data: {moduleID:moduleID,title:title,description:desc},
         //dataType: 'json',
         success: function( data ) {
            $('#saveModuleToLibrary_title').val('');
            $('#saveModuleToLibrary_description').val('');
            hideSave(); 
         }
      });
   }
   function saveUnitLibrary(unitID) {
      if(confirm('Soll diese Unit in Ihrer Bibliotek gespeichert werden?') == true) {
         $('#saveUnitIDToLibrary').val(unitID);
         var unitTitle = $('#unit' + unitID).find('.title').val();
         $('#saveUnitToLibrary_title').val(unitTitle);
         jQuery('#saveUnitToLibrary').modal('show');
      }
   }
   function saveUnitToLibraryExecute() {
      var unitID = $('#saveUnitIDToLibrary').val();
      var title      = $('#saveUnitToLibrary_title').val();
      var desc       = $('#saveUnitToLibrary_description').val();
      jQuery('#saveUnitToLibrary').modal('hide');
      jQuery('#save').fadeIn(200);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveUnitLibrary",
         data: {unitID:unitID,title:title,description:desc},
         //dataType: 'json',
         success: function( data ) {
            hideSave();
         }
      });
   }
   function chooseStudentSave() {
      jQuery('#chooseStudent').modal('hide');
      var students      = jQuery('#chooseStudentList').val();
      if(students) {
         var set = [];
         jQuery(students).each(function(e, id) {
            set.push(id);
         })
      }
      $('#noStudents').remove();
      var classroomID   = jQuery('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.chooseStudent",
         data: {classroomID:classroomID,students:set},
         dataType: 'json',
         success: function( data ) {
            var type = '';
            if(data[0]['id'] == null) {
               alert('Keine Daten zu diesem Teilnehmer gefunden.');
               jQuery('#chooseStudentList').val(null).trigger('change');
               return false;
            }
            jQuery(data).each(function(e, value) { 
               if(value['studentType'] == 'Teilnehmer') {
                  type = '<span class="badge badge-success text-white">Teilnehmer</span>';
               }
               if(value['studentType'] == 'Trainer') {
                 type = '<span class="badge badge-danger text-white">Trainer</span>';
               }
               if(value['studentType'] == 'Systemadministrator') {
                  type = '<span class="badge badge-primary text-white">Systemadministrator</span>';
               }
               jQuery('#students').append(
                  '<tr id="student' + value['tblUserID'] + '" class="student">' + 
                  '<td><a href="student-edit?layout=edit&id=' + value['tblUserID'] + '" target="_blank">' + value['name'] + '</a> ' + type + 
                  '<br/>' + value['email'] + '</td>' + 
                  '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Password besitzt.">' + 
                  value['vertify'] +
                  '</td>' + 
                  '<td>' + value['dateOfBooking'] + '</td>' + 
                  '<td><b>Verifizierung am: </b>' + value['dateOfVertify'] + '<br/><b>Einladung am:</b>' + value['dateOfInvitation'] + '</td>' + 
                  '<td>' + 
                  '<i class="fa fa-map-marker p-2" onclick="emailVertifyO(' + value['id'] + ');" title="E-Mail zur Passwortvergabe"></i>' + 
                  '<i class="fa fa-envelope p-2" onclick="emailInvitationO(' + value['id'] + ');" title="E-Mail Einladung"></i>' + 
                  '<i class="fa fa-bell p-2" onclick="emailRememberO(' + value['id'] + ');" title="E-Mail Erinnerung"></i>' + 
                  '<i onclick="comment(' + value['id'] + ');" title="Bemerkungen" class="fa fa-comment p-2"></i>' +
                  '<i class="fa fa-trash-o p-2" onclick="deleteStudent(' + value['tblUserID'] + ');" title="Teilnehmer löschen"></i>' + 
                  '</td>' + 
                  '</tr>'
               );
               jQuery('#chooseStudentList').val(null).trigger('change');
            });
            //location.reload();
         }
      });
   }
   function hideSave() {
      setTimeout(hideSaveExecute,800);
   }
   function hideSaveExecute() {
      jQuery('#save').slideUp(200);
   }
   // MOVE SOMETHING
   function moveDay(dayID) {
      jQuery('#dayToMove').val(dayID);
      jQuery('#moveDay').modal('show');
   }
   function moveModule(moduleID) {
      var classroomID   = jQuery('#classroomID').val();
      var currentDay    = jQuery('#dayID').val();
      jQuery('#moduleToMove').val(moduleID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadAvailableDays",
         data: {classroomID:classroomID,currentDay:currentDay},
         dataType: 'json',
         success: function( data ) {
            $('#moveModuleToDay').empty();
            if(data) {
               jQuery(data).each(function(e, value) {
                  $("#moveModuleToDay").append(new Option(value['title'], value['id']));
               });
            }
            /*jQuery('#moveDay').modal('hide');
            jQuery('#loadDay' + dayToMove).html(data);
            jQuery('#cardFor' + dayToMove + ' h2').html(data);
            /*jQuery('#unit' + unitToMove).clone().appendTo('#module' + newModuleID + ' ul');
            jQuery('#module' + oldModuleID + ' #unit' + unitToMove).remove();
            jQuery('#unit' + unitToMove).attr('data-moduleID', newModuleID);*/
            /*jQuery('#wait').css('opacity',0);*/
        }   
      });
      jQuery('#moveModule').modal('show');
      jQuery('#moveModuleToDay').select2({width: '100%'});
   }
   function moveUnit(unitID) {
      var currentModuleID = jQuery('#unit' + unitID).attr('data-moduleID');
      var showDialog = 0;
      $('#moveUnitToTheme').empty();
      loadTheThemesOfClassroom();
      jQuery('#moveUnitToModule').empty();
      jQuery('#moveUnitToModule').append(new Option('Bitte auswählen','-1'));
      jQuery('.module').each(function() {
         var id = jQuery(this).attr('id');
         id = id.substring(6);
         if(id != currentModuleID) {
            showDialog = 1;
            var moduleTitle = jQuery(this).find('.moduleTitle').val();
            jQuery('#moveUnitToModule').append(new Option(moduleTitle, id));
         } 
      });
      //if(showDialog == 1) {
         jQuery('#oldModuleID').val(currentModuleID);
         jQuery('#unitToMove').val(unitID);
         jQuery('#moveUnit').modal('show');
      //} else {
         //alert('Keine weiteren Module gefunden');
         //return false;
      //}
   }
   function loadTheThemesOfClassroom() {
      var classroomID = $('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadTheThemesOfClassroom",
         data: {classroomID:classroomID},
         dataType: 'json',
         success: function( data ) {
            $('#moveUnitToTheme').append(new Option('Bitte auswählen', '-1'));
            jQuery(data).each(function(e, value) {
               if(value['type'] == 'optiongroupS') {
                   $('#moveUnitToTheme').append('<optgroup label="' + value['title'] + '">');
               }
               if(value['type'] == 'option') {
                  $('#moveUnitToTheme').append(new Option(value['title'], value['id']));
               }
               if(value['type'] == 'optiongroupE') {
                   $('#moveUnitToTheme').append('</optgroup>');
               }
            });
        }   
      });
   }
   function saveMoveUnitToModule() {
      jQuery('#wait').css('opacity', 1);
      var unitToMove    = jQuery('#unitToMove').val();
      var oldModuleID   = jQuery('#oldModuleID').val();
      var newModuleID   = jQuery('#moveUnitToModule option:selected').val();
      var newThemeModuleID   = jQuery('#moveUnitToTheme option:selected').val();
      var moveModuleID  = 0;
      if(newModuleID != -1) {
         moveModuleID = newModuleID;
      } 
      if(newThemeModuleID != -1) {
         moveModuleID = newThemeModuleID;
      }
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.moveUnitToModule",
         data: {unitToMove:unitToMove,newModuleID:moveModuleID},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#moveUnit').modal('hide');
            jQuery('#unit' + unitToMove).clone().appendTo('#module' + newModuleID + ' ul');
            jQuery('#module' + oldModuleID + ' #unit' + unitToMove).remove();
            jQuery('#unit' + unitToMove).attr('data-moduleID', newModuleID);
            jQuery('#wait').css('opacity',0);
        }   
      });
   }
   function saveMoveDayToDay() {
      jQuery('#wait').css('opacity', 1);
      var dayToMove     = jQuery('#dayToMove').val();
      var moveDayToDay  = jQuery('#moveDayToDay').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.moveDayToDay",
         data: {dayToMove:dayToMove,moveDayToDay:moveDayToDay},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#moveDay').modal('hide');
            jQuery('#loadDay' + dayToMove).html(data);
            jQuery('#cardFor' + dayToMove + ' h2').html(data);
            /*jQuery('#unit' + unitToMove).clone().appendTo('#module' + newModuleID + ' ul');
            jQuery('#module' + oldModuleID + ' #unit' + unitToMove).remove();
            jQuery('#unit' + unitToMove).attr('data-moduleID', newModuleID);*/
            jQuery('#wait').css('opacity',0);
        }   
      });
   }
   function saveMoveModuleToDay() {
      jQuery('#wait').css('opacity', 1);
      var moduleToMove     = jQuery('#moduleToMove').val();
      var moveModuleToDay  = jQuery('#moveModuleToDay option:selected').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.moveModuleToDay",
         data: {moduleToMove:moduleToMove,moveModuleToDay:moveModuleToDay},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#moveModule').modal('hide');
            jQuery('#module' + moduleToMove).remove();
            jQuery('#wait').css('opacity',0);
        }   
      });
   }
   // LOAD SOMETHING
   function loadDay(classroomID, dayID) {
      jQuery('#wait').css('display', 'flex');
      jQuery('#dayID').val(dayID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadDay",
         data: {dayID:dayID, classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#structure').empty();
            jQuery('#structure').append(data);
            jQuery('.unit .select2').select2({width: '100%'});
            jQuery('.classroomDays a').each(function() {
               jQuery(this).removeClass('btn-success').addClass('btn-light');
            });
            jQuery('#loadDay' + dayID).removeClass('btn-light').addClass('btn-success');
            openDay(dayID);
            //jQuery('.summernote').summernote();
            jQuery( ".moduleContent > ul" ).sortable({
               connectWith: '.li',
               handle: '.move',
               axis: 'y',
               update: function (event, ui) {
                  var nodes = jQuery(this).context.childNodes;
                  var order = [];
                  jQuery(nodes).each(function(e) {
                     var id   = jQuery(this).context.id;
                     id       = id.substring(4);
                     order.push(e + '_' + id);
                  });
                  jQuery.ajax({
                     type: "POST",
                     url: "index.php?option=com_jclassroom&task=classroom.writeSortableUnits",
                     data: {order:order},
                     //dataType: 'json',
                     success: function( data ){
                        console.log(data);
                     }
                  });
               }
            });
            jQuery( ".card-body" ).sortable({
               handle: '.move',
               axis: 'y',
               update: function (event, ui) {
                  var nodes = jQuery(this).context.childNodes;
                  var order = [];
                  jQuery(nodes).each(function(e) {
                     var id   = jQuery(this).context.id;
                     id       = id.substring(6);
                     order.push(e + '_' + id);
                  });
                  jQuery.ajax({
                     type: "POST",
                     url: "index.php?option=com_jclassroom&task=classroom.writeSortableModules",
                     data: {order:order},
                     //dataType: 'json',
                     success: function( data ){
                        console.log(data);
                     }
                  });
               }
            });
            jQuery('#wait').css('display','none');

         }
      });
   }
   function resultQuizz(theResultID) {
      var classroomID = jQuery('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadQuizzResult",
         data: {theResultID:theResultID},
         dataType: 'json',
         success: function( data ) {
            jQuery('#resultQuizz .modal-body').empty();
            jQuery('#resultQuizz #quizzTitle').html(data['quizzTitle']);
            jQuery('#resultQuizz #quizzStudent').html(data['username']);
            jQuery('#resultQuizz .modal-body').append(data['html']);
            
            loadSingleChart(theResultID);
         }
      });
      jQuery('#resultQuizz').modal('show');
   }
   function loadSingleChart(theResultID) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadSingleChart",
         data: {theResultID:theResultID},
         dataType: 'json',
         success: function( data ) {
            if(data['calculate'] == 2) {
               jQuery('#resultQuizz .modal-body').append(
               '<canvas id="theModalChart" height="200" width="400"></canvas>'
               );
               writeChart(data,'');
            }
         }
      });
   }
   function completeResultQuizz(quizzResultID, unitID) {
      var classroomID   = jQuery('#classroomID').val();
      var quizzData     = getQuizzData(quizzResultID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadQuizzCompleteResult",
         data: {classroomID:classroomID,quizzResultID:quizzResultID, unitID:unitID},
         dataType: 'json',
         success: function( data ) {
            jQuery('#resultQuizz .modal-body').empty();
            jQuery('#resultQuizz #quizzTitle').html(quizzData['title']);
            jQuery('#resultQuizz #quizzStudent').html('Alle Teilnehmer');
            var i = 1;
            jQuery.each(data, function(e, value) {
               if(value['chart'] != 'wordCloud') {        
                  if(value['chart'] != '') {
                     jQuery('#resultQuizz .modal-body').append(
                     '<p style="margin-top: 20px;font-size: 18px; font-weight: bold;">Frage: <i>' + value['question'] + '</i></p>' +
                     '<canvas id="theModalChart' + i + '" height="200" width="400"></canvas>'
                  );
                     writeChart(value,i);
                  } else {
                     jQuery('#resultQuizz .modal-body').append(
                     '<p style="margin-top: 20px;font-size: 18px; font-weight: bold;">Frage: <i>' + value['question'] + '</i></p>');
                     jQuery('#resultQuizz .modal-body').append(
                        '<p style="margin-top: 6px;font-size: 14px; font-weight: normal;">' + value['indiAnswers'] + '</p>'
                     );
                  }
                  i++;
               } else {
                  jQuery('#resultQuizz .modal-body').append(
                     '<p style="margin-top: 20px;font-size: 18px; font-weight: bold;">Frage:<i>' + value['question'] + '</i></p>' +
                     '<div id="theCloud" height="200" width="400"></div>'
                  );
                  writeCloud(value['datas'],0);
               }
            });         
         }
      });
      jQuery('#resultQuizz').modal('show');
   }
   function writeCloud(data,i) {
      var myTags = [
         {text: "Lorem", weight: 13},
         {text: "Ipsum", weight: 10.5, html: {"class": "vertical"}},
         {text: "Dolor", weight: 9.4},
         {text: "Sit", weight: 8, html: {"class": "vertical"}},
         {text: "Amet", weight: 6.2},
         {text: "Consectetur", weight: 5}
      ];
      $("#theCloud").jQCloud(data, {
         width: 500,
         height: 350
      });
   }
   function writeChart(data, i) {
      var labels = new Array();
      jQuery.each(data['labels'], function(e, value) {
         labels.push(value);
      });
      var datas = new Array();
      jQuery.each(data['datas'], function(e, value) {
         datas.push(value);
      });
      var backgroundColor = [
         'rgba(255,0,0,1)',
         'rgba(30,144,255,1)',
         'rgba(255,240,0,1)',
         'rgba(34,139,34,1)',
         'rgba(199,21,133,1)',
         'rgba(32,178,170,1)'
      ]
      var ctx     = document.getElementById('theModalChart' + i).getContext("2d");
      ctx.canvas.height  = 500;
      ctx.canvas.width   = 500;
      if(data['chart'] == 'bar' || data['chart'] == 'horizontalBar') {
         var myChart = new Chart(ctx, {
            type: data['chart'],
            data: {
               labels: labels,
               datasets: [{
                  data: datas,
                  backgroundColor: backgroundColor
               }]
            },
            options: {
               legend: {
                  display: false
               },
               responsive:true,
               maintainAspectRatio: false,
               scales: {
                  yAxes: [{
                      ticks: {
                           stepSize: 1,
                           beginAtZero: true
                      }
                  }],
                  xAxes: [{
                      ticks: {
                           stepSize: 1,
                           beginAtZero: true
                      }
                  }]
               }
            }
         });
      }
      if(data['chart'] == 'pie') {
         var myChart = new Chart(ctx, {
            type: data['chart'],
            data: {
               labels: labels,
               datasets: [{
                  data: datas,
                  backgroundColor: backgroundColor
               }]
            },
            options: {
               scales: {
                  yAxes: [{
                     display: false,
                     gridLines: {
                        min: 0,
                        max: 20,
                        stepSize: 1,
                        display: false
                     }
                  }],
                  xAxes: [{
                     display: false,
                     gridLines: {
                        display: false
                     }
                  }]
               }
            }
         });
      }
   }
   function getQuizzData(quizzResultID) {
      var result = '';
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.getQuizzData",
         data: {quizzResultID:quizzResultID},
         dataType: 'json',
         async: false,
         success: function( data ) {
            result = data;
         }
      });
      return result;
   }
   function deleteResult(quizzResultID) {
      if(confirm('Möchten Sie das Quizz-Resultat entgültig löschen?') == true) {
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteQuizzResult",
            data: {quizzResultID:quizzResultID},
            dataType: 'json',
            success: function( data ) {
               /*jQuery('#resultQuizz .modal-body').empty();
               jQuery('#resultQuizz #quizzTitle').html(data['quizzTitle']);
               jQuery('#resultQuizz #quizzStudent').html(data['studentName']);
               jQuery('#resultQuizz .modal-body').append(data['html']);*/
            }
         });
      }
   }
   // CHECK SOMETHING
   jQuery(document).on('change','.moduleTitle', function() {
      var moduleID = jQuery(this).attr('data-id');
      saveModule(moduleID);
      jQuery('#saveIndicatorModule' + moduleID).val(1);
      jQuery('#module' + moduleID + ' .moduleHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   jQuery(document).on('change','.title', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   jQuery(document).on('change','.duration', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   jQuery(document).on('change','.link', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   jQuery(document).on('change','.quizz', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   jQuery(document).on('summernote.change','.summernote', function() {
      var unitID = jQuery(this).attr('data-id');
      saveUnit(unitID);
      jQuery('#saveIndicatorUnit' + unitID).val(1);
      jQuery('#unit' + unitID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   });
   function chooseElement(id) {
      $('#newPart_dialog .modal-dialog .card').each(function() {
         $(this).removeClass('selected');
      });
      $('#newUnitType').val(id);
      $('#newPart_dialog #card' + id).addClass('selected');
   }
   $('#newUnitSave').on('click', function() {
      var type       = $('#newUnitType').val();
      if(!type) {
         alert('Bitte wählen Sie einen Unittypen aus.');
         return false;
      }
      jQuery('#newPart_dialog').modal('hide');
      getType(type,0,0);
   });
   function createStudent() {
      var companyID  = $('#jform_companyID option:selected').val();
      var companyName= $('#jform_companyID option:selected').text();
      $('#studentCompanyID').val(companyID);
      $('#studentCompany').val(companyName);
      jQuery('#newStudent_dialog').modal('show');
   }
   jQuery('#newStudentSave').on('click', function() {
      var classroomID         = jQuery('#classroomID').val();
      var studentSalutation   = jQuery('#studentSalutation option:selected').val();
      var studentFirstname    = jQuery('#studentFirstname').val();
      var studentLastname     = jQuery('#studentLastname').val();
      var studentCompanyID    = jQuery('#studentCompanyID').val();
      var studentCompany      = jQuery('#studentCompany').val();
      var studentAdress       = jQuery('#studentAdress').val();
      var studentPostcode     = jQuery('#studentPostcode').val();
      var studentCity         = jQuery('#studentCity').val();
      var studentPhone        = jQuery('#studentPhone').val();
      var studentEmail        = jQuery('#studentEmail').val();
      var studentPassword     = jQuery('#studentPassword').val();
      if(!studentFirstname || !studentLastname || !studentEmail) {
         alert('Bitte füllen Sie alle Pflichtfelder (*) aus');
         return false;
      }
      var check_email = checkEmail(studentEmail);
      if(check_email == 1) {
         alert('Die E-Mail-Adresse ' + studentEmail + ' ist bereits registriert. Bitte wählen Sie eine andere E-Mail-Adresse oder wählen Sie den bestehenden Teilnehmer mit dieser E-Mail-Adresse aus.');
         return false;
      }
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.newStudent",
         data: {
            classroomID:classroomID,
            studentSalutation:studentSalutation,
            studentFirstname:studentFirstname,
            studentLastname:studentLastname,
            studentCompany:studentCompany,
            studentCompanyID:studentCompanyID,
            studentAdress:studentAdress,
            studentPostcode:studentPostcode,
            studentCity:studentCity,
            studentPhone:studentPhone,
            studentEmail:studentEmail,
            studentPassword:studentPassword
         },
         dataType: 'json',
         success: function( data ) {
            jQuery('#newStudent_dialog').modal('toggle');
            jQuery('#noStudents').remove();
            jQuery('#students').append(
            '<tr id="student' + data['userID'] + '" class="student">' + 
            '<td>' + 
            '<a href="/student-edit?layout=edit&id=' + data['userID'] + '" target="_blank">' + studentFirstname + ' ' + studentLastname + '</a> ' + 
            '<span class="badge ' + data['badge'] + ' text-white">' + data['usergroup'] + '</span><br/>' +
            studentEmail + '<br/>' +  
            '<span style="font-size: 12px;font-weight: bold;text-style:italic;">' + studentCompany + '</span>' +
            '</td>' +
            '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Passwort besitzt.">' + data['userstate'] + 
            '<small id="comment' + data['userID'] + '" style="display: inline-block;line-height: 14px;"></small>' +
            '</td>' +
            '<td>' + data['created'] + '</td>' +
            '<td>' + '<b>Verifizierung am:</b> <br/><b>Einladung am:</b>' + '</td>' +
            '<td>' + 
            '<i onclick="emailVertifyO(' + data['userID'] + ');" title="E-Mail zur Passwortvergabe" class="fa fa-map-marker p-2"></i>' + 
            '<i onclick="emailInvitationO(' + data['userID'] + ');" title="E-Mail-Einladung" class="fa fa-envelope p-2"></i>' + 
            '<i onclick="emailRememberO(' + data['userID'] + ');" title="E-Mail-Erinnerung" class="fa fa-bell p-2"></i>' + 
            '<i onclick="comment(' + data['userID'] + ');" title="Bemerkungen" class="fa fa-comment p-2"></i>' + 
            '<i onclick="deleteStudent(' + data['userID'] + ');" title="Teilnehmer löschen" class="fa fa-trash-o p-2"></i>' +
            '</td>' +
            '</tr>'
            );
            jQuery('#studentSalutation').val('');
            jQuery('#studentFirstname').val('');
            jQuery('#studentLastname').val('');
            jQuery('#studentCompanyID').val('');
            jQuery('#studentCompany').val('');
            jQuery('#studentAdress').val('');
            jQuery('#studentPostcode').val('');
            jQuery('#studentCity').val('');
            jQuery('#studentPhone').val('');
            jQuery('#studentEmail').val('');
            jQuery('#studentPassword').val('');
         }
      });
   });
   function checkEmail(email) {
      var result = '';
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.checkEmail",
         data: {email:email},
         async: false,
         success: function(data) {
            result = data;
         }
      });
      return result;
   }
   function comment(id) {
      var comment = $('#comment' + id).text();
      $('#commentStudentID').val(id);
      $('#comment').val(comment);
      jQuery('#comment_dialog').modal('show');
   }
   $('#saveComment').on('click', function() {
      var id            = $('#commentStudentID').val();
      var classroomID   = $('#classroomID').val();
      var comment       = $('#comment').val();
      jQuery('#comment_dialog').modal('hide');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveComment",
         data: {id:id,comment:comment,classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#comment' + id).html(comment);
         }
      });
   });
   function editDayTitle(id) {
      var comment = $('#cardFor' + id).find('.dayTitle').text();
      var date    = $('#loadDay' + id).attr('data-date');
      $('#titleDayID').val(id);
      $('#newDayDate').val(date);
      $('#newDayTitle').val(comment);
      jQuery('#dayTitle').modal('show');
   }
   function saveNewClassRoomTitle() {
      var id            = $('#titleDayID').val();
      var classroomID   = $('#classroomID').val();
      var title         = $('#newDayTitle').val();
      var date          = $('#newDayDate').val();
      jQuery('#dayTitle').modal('hide');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveDayTitle",
         data: {id:id,title:title,date:date,classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#cardFor' + id).find('.dayTitle').html(title);
            $('#loadDay' + id).html(title);
            $('#newDayTitle').val('');
         }
      });
   }

   jQuery('#openUploader').click(function() {
      jQuery('#uploader').trigger('click');
      
   });
   jQuery('#uploader').on('change', function() {
      jQuery('#wait').css('display','block');
      jQuery('#uploaderForm').submit();
   });
   function uploadUnit(unitID) {
      jQuery('#formUnitID').val(unitID);
      jQuery('#uploaderUnit').trigger('click');
   }
   function introImageUnit(unitID) {
      jQuery('#imageUnitID').val(unitID);
      jQuery('#uploadImageUnit').trigger('click');
   }
   jQuery('#uploaderUnit').on('change', function(e) {
      var classroomID   = jQuery('#classroomID').val();
      var unitID        = jQuery('#formUnitID').val();
      var theFiles      = jQuery(this)[0].files;
      var theFileName   = jQuery('#uploaderUnit')[0].files[0].name;
      var form_data     = new FormData();
      for(i = 0; i <= theFiles.length; i++) {
         
      }
      form_data.append('uploadUnit', jQuery('#uploaderUnit')[0].files[0]);
      form_data.append('classroomID', classroomID);
      form_data.append('unitID', unitID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadFile",
         data: form_data,
         processData: false,
         contentType: false,
         //dataType: 'json',
         success: function( data ) {
            var dataW = data.split('__');
            jQuery('#uploads' + unitID).append('<div id="file' + dataW[0] + '" class="row mb-1">' + 
            '<div class="col-12">' + 
               '<div class=" fileRow">' + 
                  '<div class="row">' +
                     '<div class="col-12 col-sm-1">NEU</div>' +
                     '<div class="col-12 col-sm-10">' + 
                        '<a href="' + dataW[1] + '">' + theFileName + '</a>' + 
                     '</div>' +
                     '<div class="text-right col-12 col-sm-1">' + 
                        '<i class="fa fa-trash-o bg-danger text-white p-1" style="cursor: pointer;border-radius: 3px;" onclick="deleteFile(' + dataW[0] + ');"></i>' +
                     '</div>' +
                  '</div>' +
               '</div>' +
            '</div>'
            );
            console.log(data);
         }
      });
   });
   jQuery('#uploadImageUnit').on('change', function(e) {
      var classroomID   = jQuery('#classroomID').val();
      var unitID        = jQuery('#imageUnitID').val();
      var theFiles      = jQuery(this)[0].files;
      var theFileName   = jQuery('#uploadImageUnit')[0].files[0].name;
      var form_data     = new FormData();
      for(i = 0; i <= theFiles.length; i++) {
         
      }
      form_data.append('uploadImageUnit', jQuery('#uploadImageUnit')[0].files[0]);
      form_data.append('classroomID', classroomID);
      form_data.append('unitID', unitID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadImageFile",
         data: form_data,
         processData: false,
         contentType: false,
         //dataType: 'json',
         success: function( data ) {
            var dataW = data.split('__');
            jQuery('#introImage' + unitID).append('<div id="file' + dataW[0] + '" class="row mb-1">' + 
            '<div class="col-12">' + 
               '<div class=" fileRow">' + 
                  '<div class="row">' +
                     '<div class="col-12 col-sm-10">' + 
                        '<a href="' + dataW[1] + '">' + 
                        '<div class="introImageThumb" style="background-image: URL(' + dataW[1] + ');"></div>' +
                        theFileName + '</a>' + 
                     '</div>' +
                     '<div class="text-right col-12 col-sm-2">' + 
                        '<i class="fa fa-trash-o bg-danger text-white p-1" style="cursor: pointer;border-radius: 3px;" onclick="deleteImageFile(' + dataW[0] + ');"></i>' +
                     '</div>' +
                  '</div>' +
               '</div>' +
            '</div>'
            );
            console.log(data);
         }
      });
   });
   function deleteFile(fileID) {
      if(confirm("Soll diese Datei gelöscht werden?") == true) {
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteFile",
            data: {fileID:fileID},
            //dataType: 'json',
            success: function( data ) {
               $('#file' + data).remove();
               $('#file' + fileID).remove();
            }
         });
      }
   }
function addTimeblock() {
   var classroomID = $('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.addTimeblock",
      data: {classroomID:classroomID},
      //dataType: 'json',
      success: function( data ) {
         var newTimeblock = jQuery('<div id="timeblock' + data + '" class="timeblock">' + 
         '<span class="theDate"><span class="theDateA"></span><br/>' + 
         '<i class="fa fa-pencil mr-2" onclick="editTimeblock(' + data + ');"></i>' + 
         '<i class="fa fa-trash-o mr-2" onclick="deleteTimeblock(' + data + ');"></i>' + 
         '<i class="fa fa-plus" onclick="addTimeblock();"></i></span>' +
         '</div>').appendTo('#timeblocks');
         newTimeblock.droppable({
            accept: ".theme", 
            drop: function(event, ui) {
               $(this).removeClass("border").removeClass("over");
               var dropped = ui.draggable;
               var droppedOn = $(this);
               if(dragFrom == 'themes') {  
                  jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
                  addThemeToTimeblock(ui.draggable.attr('id'),$(this));  
               }  else {
                  var classroomID = $('#classroomID').val();
                  var parent  = droppedOn;
                  var id      = parent.attr('id');
                  jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
                  var order = jQuery('#' + id).sortable("toArray");
                  jQuery.ajax({
                     type: "POST",
                     url: "index.php?option=com_jclassroom&task=classroom.writeSortableTimeblocks",
                     data: {classroomID:classroomID,order:order},
                     //dataType: 'json',
                     success: function( data ){
                        console.log(data);
                     }
                  });
               }  
            },
            over: function(event, elem) {
               $(this).addClass("over");
            },
            out: function(event, elem) {
               $(this).removeClass("over");
            }
         });
         $('html, body').animate({
            scrollTop: $('#timeblock' + data).offset().top - 100
         },1000);
      }
   });
   
}
var dragFrom = '';
var dragX = 0;
var dragY = 0;
jQuery(".timeblock").sortable({
   update: function (event, ui) {
      alert("HIER");
      console.log(ui);
      var order = jQuery('.classroomDays').sortable("toArray");
      console.log(order);
      /*
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.writeSortableThemes",
         data: {order:order},
         //dataType: 'json',
         success: function( data ){
            console.log(data);
         }
      });*/
   }
});
jQuery('.timeblock').droppable({
   accept: ".theme", 
   drop: function(event, ui) {
      $(this).removeClass("border").removeClass("over");
      var dropped = ui.draggable;
      var droppedOn = $(this);
      if(dragFrom == 'themes') {  
         
         jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
         addThemeToTimeblock(ui.draggable.attr('id'),$(this));  
      }  else {
         var classroomID = $('#classroomID').val();
         var parent  = droppedOn;
         var id      = parent.attr('id');
         jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
         var order = jQuery('#' + id).sortable("toArray");
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.writeSortableTimeblocks",
            data: {classroomID:classroomID,order:order},
            //dataType: 'json',
            success: function( data ){
               console.log(data);
            }
         });
      }
   },
   over: function(event, elem) {
      $(this).addClass("over");
   },
   out: function(event, elem) {
      $(this).removeClass("over");
   }
});


jQuery(".theme").draggable({ 
   cursor: "crosshair", 
   revert: "invalid",
   start: function(event, ui) {
      dragX = ui.offset.left;
      dragY = ui.offset.top;
      var parent = jQuery(this).parent().attr('id');
      if(parent == 'themes') {
         dragFrom = 'themes';
      } else {
         dragFrom = 'timeblocks';
      }
   }
});


jQuery('.classroomDays').sortable({
   update: function (event, ui) {
      var order = jQuery('.classroomDays').sortable("toArray");
      console.log(order);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.writeSortableThemes",
         data: {order:order},
         //dataType: 'json',
         success: function( data ){
            console.log(data);
         }
      });
   }
});
jQuery("#themes").droppable({ 
   accept: ".theme", 
   drop: function(event, ui) {
      $(this).removeClass("border").removeClass("over");
      var dropped = ui.draggable;
      var droppedOn = $(this);
      jQuery(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);      
      var $wrapper = $('#themes');

      $wrapper.find('.theme').sort(function(a, b) {
          return +a.dataset.ordering - +b.dataset.ordering;
      })
      .appendTo($wrapper);
      deleteThemeFromTimeblock(ui.draggable.attr('id'),$(this));
   },
   over: function(event, elem) {
      $(this).addClass("over");
   },
   out: function(event, elem) {
      $(this).removeClass("over");
   }
});
function addThemeToTimeblock(droppedID,droppedTo) {
   var classroomID = $('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.addThemeToTimeblock",
      data: {classroomID:classroomID,droppedID:droppedID,droppedTo:droppedTo.attr('id')},
      //dataType: 'json',
      success: function( data ) {
         console.log(data);
      }
   });
}
function deleteThemeFromTimeblock(droppedID,droppedTo) {
   var classroomID = $('#classroomID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.deleteThemeFromTimeblock",
      data: {classroomID:classroomID,droppedID:droppedID,droppedTo:droppedTo.attr('id')},
      //dataType: 'json',
      success: function( data ) {
         console.log(data);
      }
   });
}
function editTimeblock(id) {
   var dayDate    = $('#timeblock' + id).attr('data-title');
   var color      = $('#timeblock' + id).attr('data-color');
   $('#timeblockID').val(id);
   $('#timeblockDayDate').val(dayDate);
   $('#timeblockColor').val(color);
   jQuery('#editTimeblock').modal('show');
}
function saveEditTimeblock() {
   var timeblockID   = $('#timeblockID').val();
   var dayDate       = $('#timeblockDayDate').val();
   var color         = $('#timeblockColor').val();
    if(!dayDate || !color) {
      alert('Bitte wählen Sie ein Datum und eine Darstellungsfarbe aus.');
      return false;
   }
   $('#timeblock' + timeblockID).attr('data-color',color).attr('data-title',dayDate);
   jQuery('#editTimeblock').modal('hide');
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=classroom.saveEditTimeblock",
      data: {dayDate:dayDate,color:color,timeblockID:timeblockID},
      //dataType: 'json',
      success: function( data ) {
         $('#timeblock' + timeblockID).find('.theDateA').html(dayDate);
         $('#timeblock' + timeblockID).css('background-color', color);
      }
   });
}
function deleteTimeblock(id) {
   if(confirm('Soll der Zeitblock ' + id + ' gelöscht werden?') == true) {

      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.deleteTimeblock",
         data: {timeblockID:id},
         //dataType: 'json',
         success: function( data ) {
            $('#timeblock' + id + ' .theme').each(function() {
               var thisTheme = $(this);
               $('#themes').append(
               '<a id="' + thisTheme.attr('id') + '" class="theme mb-2 btn data-title="' + thisTheme.attr('data-title') + '" data-ordering="' + thisTheme.attr('data-ordering') + '" style="position: relative;left:0px;top: 0px;">' + thisTheme.html() + '</a>'
               );
            });
            jQuery(".theme").draggable({ 
               cursor: "crosshair", 
               revert: "invalid"
            });
            $('#timeblock' + id).remove();
            
         }
      });
   }
}
function reorderRoom() {
   var classroomID = $('#classroomID').val();
   if(confirm('Soll die Struktur neu berechnet werden?') == true) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.reorderRoom",
         data: {classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
           
         }
      });
   }
}
$('#openUploaderM').click(function() {
   $('#inputMaterial').trigger('click');
   
});
$('#inputMaterial').on('change', function() {
   $('#save').css('display','block');
   $('#uploaderFormMaterial').submit();
});
$('#openUploaderR').click(function() {
   $('#inputReserve').trigger('click');
   
});
$('#inputReserve').on('change', function() {
   $('#save').css('display','block');
   $('#uploaderFormReserve').submit();
});
function deleteFolderFD(id) {

}
function deleteFileFD(path, name) {
   if(confirm('Möchten Sie die Datei ' + name + ' endgültig löschen?') == true) {
      jQuery.ajax({
          url: "index.php?option=com_jclassroom&task=classroom.deleteFileFD",
          data: {path:path},
          method: 'POST',
          //dataType: 'json',
          success: function( data ) {
              $('#file' + name).remove();
          }
      });
   }
}
</script>
