<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <?php 
	JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
	$load = new GlobalHelper();
	echo $load->getClassroomHeader($this->learningroom, 'Version');
	?>
   <div class="buttonleiste mb-4">         
      <a class="btn btn-success text-white" onclick="Joomla.submitform('classroom2.simpleSaveVersion')">Speichern</a>
      <a class="btn btn-secondary text-white" onclick="Joomla.submitform('classroom2.saveVersion')">Speichern & Schließen</a>
      <a id="retour" href="<?php echo $this->retourGlobal;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
             <?php echo $this->form->getLabel('version'); ?>
           </div>
           <div class="col-12 col-sm-10">
             <?php echo $this->form->getInput('version'); ?>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-12 col-sm-2 col-form-label">
             <?php echo $this->form->getLabel('changelog'); ?>
           </div>
           <div class="col-12 col-sm-10">
             <?php echo $this->form->getInput('changelog'); ?>
           </div>
       </div>
   </div>
   <input type="hidden" name="task" value="" />
   <?php echo JHtml::_('form.token'); ?>
</form>
<script>
var form = $("#adminForm");
   $.validator.setDefaults({
   ignore: []
});
form.validate({
   rules: {
      'jform[title]': {
         required: true
      },
      'jform[visibleTo]': {
         required: true
      },
      'jform[presentation]': {
         required: true
      }
      
   },
   messages: {
      'jform[title]': 'Bitte geben Sie einen Namen für den Learningroom ein.',
      'jform[presentation]': 'Bitte wählen Sie eine Ausgabeart aus.',
      'jform[visibleTo]': 'Bitte wählen Sie eine Verwaltungsart aus.'
   },
   submitHandler: function(form) {

      form.submit();
   }
});
</script>