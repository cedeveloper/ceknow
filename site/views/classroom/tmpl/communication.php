<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <?php 
	JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
	$load = new GlobalHelper();
	echo $load->getClassroomHeader($this->learningroom, 'Kommunikation');
	?>
   <div class="buttonleiste mb-4">         
      <a class="btn btn-success text-white" onclick="Joomla.submitform('classroom2.simpleSaveCommunication')">Speichern</a>
      <a class="btn btn-secondary text-white" onclick="Joomla.submitform('classroom2.saveCommunication')">Speichern & Schließen</a>
      <a id="retour" href="classroom-edit?layout=global&id=<?php echo $this->item->id;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
      <div class="row">
         <div class="col-12 col-sm-6">
            <!--<div class="form-group row">
                 <div class="col-12 col-sm-3 col-form-label">
                   <?php //echo $this->form->getLabel('invitations'); ?>
                 </div>
                 <div class="col-12 col-sm-9">
                   <?php //echo $this->form->getInput('invitations'); ?>
                 </div>
             </div>
             <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php //echo $this->form->getLabel('invitationsBy'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php //echo $this->form->getInput('invitationsBy'); ?>
               </div>
            </div>
            <div class="form-group row">
                 <div class="col-12 col-sm-3 col-form-label">
                   <?php //echo $this->form->getLabel('offsetRememberMails'); ?>
                 </div>
                 <div class="col-12 col-sm-9">
                   <?php //echo $this->form->getInput('offsetRememberMails'); ?>
                 </div>
            </div>-->
            <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('certificate'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('certificate'); ?>
               </div>
            </div>
            <!--<div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php //echo $this->form->getLabel('student_feedback'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php //echo $this->form->getInput('student_feedback'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php //echo $this->form->getLabel('student_feedback_anonym'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php //echo $this->form->getInput('student_feedback_anonym'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php //echo $this->form->getLabel('trainer_feedback'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php //echo $this->form->getInput('trainer_feedback'); ?>
               </div>
            </div>-->
            <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('hardware_order'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('hardware_order'); ?>
               </div>
            </div>
            <div class="form-group row">
              <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('showChat'); ?>
              </div>
              <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('showChat'); ?>
              </div>
          </div>
         </div>
         <div class="col-12 col-sm-6">
            <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('verification_template'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('verification_template'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('email_template'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('email_template'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('remember_template'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('remember_template'); ?>
               </div>
            </div>
            <div id="templatePreview">
               <?php //echo $this->item->invitationTemplate;?>
            </div>
         </div>
      </div>
   </div>
   <input type="hidden" name="task" value="" />
   <?php echo JHtml::_('form.token'); ?>
</form>
