<div id="students" class="classroomTabs hideThis">
   <h2>Teilnehmer bearbeiten</h2>
   <div class="form-horizontal mt-3">
   <small>Sie können folgenden Dateiformate hochladen: CSV<br/>
   Die maximale Uploadgröße beträgt 8MB je Upload.<br/>
   Bitte beachten Sie die korrekte Anordnung der Dateifelder in der CSV-Datei.</small>
   <div class="d-block">
   <button type="button" class="btn btn-warning text-white d-inline mt-3 mb-3"id="openUploader">Upload</button>
   <a onclick="createStudent();" class="btn btn-info text-white">Teilnehmer manuell erfassen</a>
   </div>
   <table id="students" class="table table-striped table-sm">
   <thead>
   <tr>
   <th>Name</th>
   <th>Firma</th>
   <th class="text-center">Userstate</th>
   <th>Gebucht am</th>
   <th>E-Mails</th>
   <th>Aktionen</th>
   </tr>
   </thead>
   <tbody>
   <?php
   if($this->item->students):
   $fileID = 0;
   foreach($this->item->students as $student):
   echo '<tr id="student'.$student['id'].'">';
   echo '<td><a href="student-edit?layout=edit&id='.$student['id'].'" target="_blank">'.$student['name'].'</a><br>'.$student['email'].'</td>';
   echo '<td>'.$student['company'].'</td>';
   if($student['password']):
   $state = '<i class="fa fa-check text-success"></i>';
   else:
   $state = '<i class="fa fa-ban text-danger"></i>';
   endif;
   echo '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Password besitzt.">'.$state.'</td>';
   echo '<td></td>';
   echo '<td><b>Verifizierung am:</b> '.$student['lastVertify'].'<br/><b>Einladung am:</b> '.$student['lastInvitation'].'</td>';
   echo '<td>';
   //if(!$student['lastVertify']):
   echo '<i onclick="emailVertify('. $student['id'].');" title="E-Mail zur Passwortvergabe" class="fa fa-map-marker p-2"></i>';
   //endif;
   //if(!$student['lastInvitation']):
   echo '<i onclick="emailInvitation('. $student['id'].');" title="E-Mail-Einladung" class="fa fa-envelope p-2"></i>';
   //endif;
   echo '<i onclick="deleteStudent('. $student['id'].');" title="Teilnehmer löschen" class="fa fa-trash-o p-2"></i>
   </td>';
   echo '</tr>';
   $fileID++;
   endforeach;
   else:
   echo '<tr id="noStudents">';
   echo '<td colspan="5">Keine Teilnehmer gefunden.</td>';
   echo '</tr>';
   endif;
   ?>
   </tbody>
   </table>
   </div>
</div>