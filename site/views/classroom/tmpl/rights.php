<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
$user = JFactory::getUser();
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <?php 
	JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
	$load = new GlobalHelper();
	echo $load->getClassroomHeader($this->learningroom, 'Rechteverwaltung');
	?>
   <div class="buttonleiste mb-4">         
      <a class="btn btn-success text-white" onclick="Joomla.submitform('classroom2.simpleSaveRights')">Speichern</a>
      <a class="btn btn-secondary text-white" onclick="Joomla.submitform('classroom2.saveRights')">Speichern & Schließen</a>
      <a id="retour" href="classroom-edit?layout=global&id=<?php echo $this->item->id;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <p class="mt-3"><i class="fa fa-question"></i> Legen Sie fest, wer diesen Learningroom sehen und bearbeiten darf</p>
   <div class="mb-3 col-12 col-sm-6 card bg-light p-1">
      <p><b>Globale Freigaben</b></p>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('showto'); ?>
         </div>
         <div class="col-12 col-sm-6">
            <?php echo $this->form->getInput('showto'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('editto'); ?>
         </div>
         <div class="col-12 col-sm-6">
            <?php echo $this->form->getInput('editto'); ?>
         </div>
      </div>
      <!--<div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php //echo $this->form->getLabel('choosableto'); ?>
         </div>
         <div class="col-12 col-sm-6">
            <?php //echo $this->form->getInput('choosableto'); ?>
         </div>
      </div>-->
   </div>
   <?php
   $displayTable = 'display: none;';
   if($this->item->showto == 0 && $this->item->editto == 0):
      $displayTable = '';
   endif;
   ?>
   <table id="setRights" style="<?php echo $displayTable;?>" class="table table-striped table-sm">
      <?php if($this->usergroup == 'superuser'): ?>
      <thead>
         <tr style="background-color: lightsalmon;">
            <th>Systemadministratoren</th>
            <th style="text-align: center;">
               <a onclick="allSysadminRead();" class="btn btn-primary text-white btn-sm">Alle  Systemadmin</a>
               <a onclick="noSysadminRead();" class="btn btn-danger text-white btn-sm">Kein Systemadmin</a>
            </th>
            <th style="text-align: center;">
               <a onclick="allSysadminWrite();" class="btn btn-primary text-white btn-sm">Alle Systemadmin</a>
               <a onclick="noSysadminWrite();" class="btn btn-danger text-white btn-sm">Kein Systemadmin</a>
            </th>
         </th>
         <tr>
            <th style="width: 50%;">Name</th>
            <th style="width: 25%;text-align: center;">Ansehen</th>
            <th style="width: 25%;text-align: center;">Bearbeiten</th>
         </tr>
      </thead>
      <tbody>
         <?php
         if($this->systemadmins):
            $showto = $this->item->showto;
            $disable_showto = '';
            if($showto == 1 || $showto == 3):
               $disable_showto = 'disabled';
            endif;
            $editto = $this->item->editto;
            $disable_editto = '';
            if($editto == 1 || $editto == 3):
               $disable_editto = 'disabled';
            endif;
            foreach($this->systemadmins as $sysadmin):
               echo '<tr>';
               echo '<td>'.$sysadmin->name.'</td>';
               $checkedR = '';
               if($this->item->rights):
                  foreach($this->item->rights as $right):
                     if($right->rightType == 1 && $right->userID == $sysadmin->id):
                        $checkedR = 'checked';
                     endif;
                  endforeach;
               endif;
               $checkedW = '';
               if($this->item->rights):
                  foreach($this->item->rights as $right):
                     if($right->rightType == 2 && $right->userID == $sysadmin->id):
                        $checkedW = 'checked';
                     endif;
                  endforeach;
               endif;
               echo '<td style="text-align: center;">
                  <input type="checkbox" id="cread_'.$sysadmin->id.'" onclick="setRead('.$sysadmin->id.');" class="sysadminRead" data-userid="'.$sysadmin->id.'" data-type="read" '.$disable_showto.' '.$checkedR.' />
                  </td>';
               echo '<td style="text-align: center;">
                  <input type="checkbox" id="cwrite_'.$sysadmin->id.'" onclick="setWrite('.$sysadmin->id.');" class="'.$disable_editto.' sysadminWrite" data-userid="'.$sysadmin->id.'" data-type="write" '.$disable_editto.' '.$checkedW.' />
                  </td>';
               echo '</tr>';
            endforeach;
         endif;
         ?>
      </tbody>
      <?php endif;?>
      <thead>
         <tr style="background-color: palegreen;">
            <th>Kundenadministratoren</th>
            <th style="text-align: center;">
               <!--<a onclick="allCusadminRead();" class="cread btn btn-primary text-white btn-sm">Alle Kundenadmin</a>
               <a onclick="noCusadminRead();" class="cread btn btn-danger text-white btn-sm">Kein Kundenadmin</a>-->
            </th>
            <th style="text-align: center;">
               <!--<a onclick="allCusadminWrite();" class="cwrite btn btn-primary text-white btn-sm">Alle Kundenadmin</a>
               <a onclick="noCusadminWrite();" class="cwrite btn btn-danger text-white btn-sm">Kein Kundenadmin</a>-->
            </th>
         </th>
         <tr>
            <th>Name</th>
            <th style="text-align: center;">Ansehen</th>
            <th style="text-align: center;">Bearbeiten</th>
         </tr>
      </thead>
      <tbody>
         <?php
         if($this->customeradmins):
            $showto = $this->item->showto;
            $disable_showto = '';
            if($showto == 1 || $showto == 3):
               $disable_showto = 'disabled';
            endif;
            $editto = $this->item->editto;
            $disable_editto = '';
            if($editto == 1 || $editto == 3):
               $disable_editto = 'disabled';
            endif;
            foreach($this->customeradmins as $cusadmin):
               if($cusadmin->id != $user->id):
                  echo '<tr>';
                  echo '<td>'.$cusadmin->name.' <b>( '.$cusadmin->company_name.' )</b></td>';
                  $checkedR = '';
                  if($this->item->rights):
                     foreach($this->item->rights as $right):
                        if($right->rightType == 1 && $right->userID == $cusadmin->id):
                           $checkedR = 'checked';
                        endif;
                     endforeach;
                  endif;
                  $checkedW = '';
                  if($this->item->rights):
                     foreach($this->item->rights as $right):
                        if($right->rightType == 2 && $right->userID == $cusadmin->id):
                           $checkedW = 'checked';
                        endif;
                     endforeach;
                  endif;
                  echo '<td style="text-align: center;">
                     <input type="checkbox" id="cread_'.$cusadmin->id.'" onclick="setRead('.$cusadmin->id.');" class="cread cusadminRead" data-userid="'.$cusadmin->id.'" data-type="read"'.$disable_showto.' '.$checkedR .' />
                     </td>';
                  echo '<td style="text-align: center;">
                     <input type="checkbox" id="cwrite_'.$cusadmin->id.'" onclick="setWrite('.$cusadmin->id.');" class="'.$disable_editto.' cwrite cusadminWrite" data-userid="'.$cusadmin->id.'" data-type="write"'.$disable_editto.' '.$checkedW. '/>
                     </td>';
                  echo '</tr>';
               endif;
            endforeach;
         endif;
         ?>
      </tbody>
      <thead>
         <tr style="background-color: lightsteelblue;">
            <th>Trainer</th>
            <th style="text-align: center;">
               <!--<a onclick="allTrainerRead();" class="cread btn btn-primary text-white btn-sm">Alle Trainer</a>
               <a onclick="noTrainerRead();" class="cread btn btn-danger text-white btn-sm">Kein Trainer</a>-->
            </th>
            <th style="text-align: center;">
               <!--<a onclick="allTrainerWrite();" class="cwrite btn btn-primary text-white btn-sm">Alle Trainer</a>
               <a onclick="noTrainerWrite();" class="cwrite btn btn-danger text-white btn-sm">Kein Trainer</a>-->
            </th>
         </th>
         <tr>
            <th>Name</th>
            <th style="text-align: center;">Ansehen</th>
            <th style="text-align: center;">Bearbeiten</th>
         </tr>
      </thead>
      <tbody>
         <?php
         if($this->trainer):
            $showto = $this->item->showto;
            $disable_showto = '';
            if($showto == 1 || $showto == 3):
               $disable_showto = 'disabled';
            endif;
            $editto = $this->item->editto;
            $disable_editto = '';
            if($editto == 1 || $editto == 3):
               $disable_editto = 'disabled';
            endif;
            foreach($this->trainer as $trainer):
               echo '<tr>';
               echo '<td>'.$trainer->name.' <b>( '.$trainer->company_name.' )</b></td>';
               $checkedR = '';
               if($this->item->rights):
                  foreach($this->item->rights as $right):
                     if(($right->rightType == 1) && $right->userID == $trainer->id):
                        $checkedR = 'checked';
                     endif;
                  endforeach;
               endif;
               $checkedW = '';
               if($this->item->rights):
                  foreach($this->item->rights as $right):
                     if(($right->rightType == 2) && $right->userID == $trainer->id):
                        $checkedW = 'checked';
                     endif;
                  endforeach;
               endif;
               echo '<td style="text-align: center;">
                  <input type="checkbox" id="cread_'.$trainer->id.'" onclick="setRead('.$trainer->id.');" class="cread trainerRead" data-userid="'.$trainer->id.'" data-type="read" '.$disable_showto.' '.$checkedR. '/>
                  </td>';
               echo '<td style="text-align: center;">
                  <input type="checkbox" id="cwrite_'.$trainer->id.'" onclick="setWrite('.$trainer->id.');" class="'.$disable_editto.' cwrite trainerWrite" data-userid="'.$trainer->id.'" data-type="write" '.$disable_editto.' '.$checkedW .'/>
                  </td>';
               echo '</tr>';
            endforeach;
         endif;
         ?>
      </tbody>
   </table>
<input type="hidden" name="task" value="" />
<input type="hidden" id="classroomID" value="<?php echo $this->item->id;?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<div id="theDialog" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-exclamation-circle text-danger"></i> Hinweis</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="dialogMessage"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<script>
   $(document).ready(function() {
      var showto = $('#jform_showto option:selected').val();
      var editto = $('#jform_editto option:selected').val();
      if(showto == 0 || editto == 0) {
         $('#setRights').fadeIn(200);
      } else {
         $('#setRights').fadeOut(200);
      }
   });
   function calcRights() {
      var showto = $('#jform_showto option:selected').val();
      var editto = $('#jform_editto option:selected').val();
      
      if(showto == 1) {
         $('#jform_editto').val('1');
         jQuery('#jform_editto').select2();//.attr('disabled', true);
         $('#jform_choosableto').val('1');
         //jQuery('#jform_choosableto').select2().attr('disabled', true);
         $('.cread').attr('disabled', 'disabled').addClass('disabled');
      } else {
         jQuery('#jform_editto').attr('disabled', false);
         jQuery('#jform_choosableto').attr('disabled', false);
         $('.cread').removeAttr('disabled').removeClass('disabled');
      }
      if(editto == 1 || editto == 3) {
         $('.cwrite').attr('disabled', 'disabled').addClass('disabled');
      } else {
         $('.cwrite').removeAttr('disabled').removeClass('disabled');
      }
      if(showto == 1 && editto != 1) {
         $('.cwrite').attr('disabled', 'disabled').addClass('disabled');
         $('#setRights').fadeOut(200);
         $('#dialogMessage').html('Wenn Sie als Recht im Feld <i>Ansehen</i> <b>Nur Sie</b> gewählt haben, muss als Recht im Feld <i>Bearbeiten</i> ebenfalls <b>Nur Sie</b> ausgewählt sein.');
         $('#theDialog').modal('show');

         return false;
      }
      if(showto == 0 || editto == 0) {
         $('#setRights').fadeIn(200);
      } else {
         $('#setRights').fadeOut(200);
      }
      if(showto == 3 && editto == 0) {
         $('.cread').attr('disabled', 'disabled').addClass('disabled');
      }
   }
   function setRead(id) {
      var classroomID = $('#classroomID').val();
      if($('#cread_' + id).is(':checked')) {
         $('#save').modal('show');
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.setRead",
            data: {classroomID:classroomID,id:id,right:1},
            //dataType: 'json',
            success: function( data ) {
               hideSave();
            }
         });
      } else { 
         $('#save').modal('show');
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.setRead",
            data: {classroomID:classroomID,id:id,right:0},
            //dataType: 'json',
            success: function( data ) {
               setWriteAutomatic(id);
               hideSave();
            }
         });
      }  
   }
   function setReadAutomatic(id) {
      var classroomID = $('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.setRead",
         data: {classroomID:classroomID,id:id,right:1},
         //dataType: 'json',
         success: function( data ) {
            $('#cread_' + id).prop('checked', true);
         }
      }); 
   }
   function setWriteAutomatic(id) {
      var classroomID = $('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.setWrite",
         data: {classroomID:classroomID,id:id,right:0},
         //dataType: 'json',
         success: function( data ) {
            $('#cwrite_' + id).prop('checked', false);
         }
      }); 
   }
   function setWrite(id) {
      var classroomID = $('#classroomID').val();
      if($('#cwrite_' + id).is(':checked')) {
         $('#save').modal('show');
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.setWrite",
            data: {classroomID:classroomID,id:id,right:1},
            //dataType: 'json',
            success: function( data ) {
               setReadAutomatic(id);
               hideSave();
            }
         });
      } else { 
         $('#save').modal('show');
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom2.setWrite",
            data: {classroomID:classroomID,id:id,right:0},
            //dataType: 'json',
            success: function( data ) {
               hideSave();
            }
         });
      }
   }
   function hideSave() {
      setTimeout(hideSaveExecute,800);
   }
   function hideSaveExecute() {
      $('#save').modal('hide');
   }
   
</script>