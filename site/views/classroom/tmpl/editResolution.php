<div id="editResolution" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content" style="height: 100%;width: 100%;">
         <div class="modal-header">
            <h5 class="modal-title">Inhalt bearbeiten</h5>
            <button type="button" onclick="closeEditContentModule();" class="close" data-dismiss="modala" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div data-editor="jform_resolution" class="use_editor">
               <?php 
               echo $this->form->getInput('content');
               require_once(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');
               require_once(JPATH_COMPONENT.'/views/dialogs/formForJCE.php');
               ?>
            </div>
            <div class="templates">
               <h5>Codetemplates</h5>
               <!--<a onclick="addRow();" class="btn btn-success btn-sm">Neue Zeile</a>-->
               <a onclick="addTable();" class="btn btn-success btn-sm">Tabelle</a>
               <a onclick="addTestlevel();" class="btn btn-success btn-sm">Testlevel</a>
               <a onclick="addCode();" class="btn btn-success btn-sm">Code</a>
               <a onclick="add2Col();" class="btn btn-success btn-sm">2 Spalter</a>
               <a onclick="add3Col();" class="btn btn-success btn-sm">3 Spalter</a>
               <a onclick="add4Col();" class="btn btn-success btn-sm">4 Spalter</a>
               <a onclick="addMessage();" class="btn btn-success btn-sm">Message</a>
               <a onclick="addWarnung();" class="btn btn-success btn-sm">Warnung</a>
               <a onclick="addInfo();" class="btn btn-success btn-sm">Info</a>
               <a onclick="addBox();" class="btn btn-success btn-sm">Box</a>
            </div>
         </div>
         <div class="modal-footer">
            <button onclick="editContentModule();" id="editContentModule" type="button" class="btn btn-primary">Einfügen</button>
            <button onclick="closeEditContentModule();" type="button" class="btn btn-secondary" data-dismiss="">Abbrechen</button>
            <input type="hidden" id="editContentUnitID" value="" />
         </div>
      </div>
   </div>
</div>