<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newPart.php');
require_once(JPATH_COMPONENT.'/views/classroom/tmpl/newStudent.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
    <div class="buttonleiste mb-4">         
        <a class="btn btn-success text-white" onclick="Joomla.submitform('classroom.simpleSave')">Speichern</a>
        <a class="btn btn-secondary text-white" onclick="Joomla.submitform('classroom.save')">Speichern & Schließen</a>
        <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
    </div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Basisdaten</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="asp-tab" data-toggle="tab" href="#asp" role="tab" aria-controls="asp" aria-selected="false">Teilnehmer</a>
        </li>
        <?php if($this->usergroup == 'trainer') { ?>
        <li class="nav-item">
            <a class="nav-link" id="days-tab" data-toggle="tab" href="#days" role="tab" aria-controls="days" aria-selected="true">Struktur</a>
        </li>
        <!--<li class="nav-item">
            <a class="nav-link" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-controls="global" aria-selected="false">Kurse</a>
        </li>-->
        <li class="nav-item">
            <a class="nav-link" id="material-tab" data-toggle="tab" href="#material" role="tab" aria-controls="material" aria-selected="false">Arbeitsmaterial</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="quizz-tab" data-toggle="tab" href="#quizz" role="tab" aria-controls="quizz" aria-selected="false">Auswertungen Quizz</a>
        </li>
        <?php } ?>
        <li class="nav-item">
            <a class="nav-link" id="freigabe-tab" data-toggle="tab" href="#freigabe" role="tab" aria-controls="freigabe" aria-selected="false">Freigabe</a>
        </li>
    </ul>
    <div class="tab-content" id="tabContent">
        <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
            <div class="form-horizontal mt-3">
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('title'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('title'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('main_trainer'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('main_trainer'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('co_trainer'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('co_trainer'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('description'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('description'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('inventa_kursID'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('inventa_kursID'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('inventa_terminID'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('inventa_terminID'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('fromDate'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('fromDate'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('toDate'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('toDate'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="asp" role="tabpanel" aria-labelledby="asp-tab">
            <div class="form-horizontal mt-3">
               <small>Sie können folgenden Dateiformate hochladen: CSV<br/>
               Die maximale Uploadgröße beträgt 8MB je Upload.<br/>
               Bitte beachten Sie die korrekte Anordnung der Dateifelder in der CSV-Datei.</small>
               <div class="d-block">
                  <button type="button" class="btn btn-warning text-white d-inline mt-3 mb-3"id="openUploader">Upload</button>
                  <a onclick="createStudent();" class="btn btn-info text-white">Teilnehmer manuell erfassen</a>
               </div>
               <table id="students" class="table table-striped table-sm">
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Firma</th>
                        <th class="text-center">Userstate</th>
                        <th>Gebucht am</th>
                        <th>E-Mails</th>
                        <th>Aktionen</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     if($this->item->students):
                        $fileID = 0;
                        foreach($this->item->students as $student):
                           echo '<tr id="student'.$student['id'].'">';
                           echo '<td><a href="student-edit?layout=edit&id='.$student['id'].'" target="_blank">'.$student['name'].'</a><br>'.$student['email'].'</td>';
                           echo '<td>'.$student['company'].'</td>';
                           if($student['password']):
                            $state = '<i class="fa fa-check text-success"></i>';
                          else:
                            $state = '<i class="fa fa-ban text-danger"></i>';
                          endif;
                           echo '<td class="text-center" title="Anzeige, ob der Teilnehmer ein gültiges Password besitzt.">'.$state.'</td>';
                           echo '<td></td>';
                           echo '<td><b>Verifizierung am:</b> '.$student['lastVertify'].'<br/><b>Einladung am:</b> '.$student['lastInvitation'].'</td>';
                           echo '<td>';
                           //if(!$student['lastVertify']):
                            echo '<i onclick="emailVertify('. $student['id'].');" title="E-Mail zur Passwortvergabe" class="fa fa-map-marker p-2"></i>';
                           //endif;
                           //if(!$student['lastInvitation']):
                              echo '<i onclick="emailInvitation('. $student['id'].');" title="E-Mail-Einladung" class="fa fa-envelope p-2"></i>';
                           //endif;
                           echo '<i onclick="deleteStudent('. $student['id'].');" title="Teilnehmer löschen" class="fa fa-trash-o p-2"></i>
                           </td>';
                           echo '</tr>';
                           $fileID++;
                        endforeach;
                     else:
                           echo '<tr id="noStudents">';
                           echo '<td colspan="5">Keine Teilnehmer gefunden.</td>';
                           echo '</tr>';
                     endif;
                     ?>
                  </tbody>
               </table>
            </div>
       	</div>
        <div class="tab-pane fade" id="days" role="tabpanel" aria-labelledby="days-tab">
            <div class="form-horizontal mt-3">
               <?php $unitCounter = 0;?>
               <div id="structure">
                  <?php if($this->item->cards): ?>
                     <?php $i = 1; ?>
                     <?php foreach($this->item->cards as $card): ?>
                     <?php
                        $html = '';
                        $cardData = json_decode($card->structure); 
                        $date    = new Datetime($card->day);
                        $html .= '<div id="cardFor' .$date->format('d').'" class="card bg-light mb-2">';
                           $html .= '<div class="card-header d-flex" style="justify-content: space-between">';
                              $html .= '<i onclick="openDay(&quot;'.$date->format('d').'&quot;);" class="open fa fa-chevron-right"></i>';
                              $html .= '<h2>'.$date->format('l, d.m.Y').'</h2>';
                              $html .= '<a id="addFor'.$date->format('d').'" onclick="addUnit(&quot;'.$date->format('d').'&quot;);"><i style="font-size: 24px;border-radius: 50%;" class="addUnit fa fa-plus bg-success text-white p-1"></i></a>';
                           $html .= '</div>';
                           $html .= '<div class="card-body">';
                              if($cardData):
                                 foreach($cardData[0]->content as $key => $data):
                                    JLoader::register('UnitsHelper',JPATH_COMPONENT_SITE.'/helpers/units.php');
                                    $template = new UnitsHelper();
                                    $html .= $template->getTemplate($i, $date->format('d'), $data->typ, $data, $this->item->unitFiles);
                                    $i++;
                                 endforeach;
                              else:
                                 $html .= '<p class="cardMessage m-0">Keine Units für diesen Tag gefunden.</p>';
                              endif;
                           $html .= '</div>';
                        $html .= '</div>';
                        $unitCounter = $i;
                        echo $html;
                     ?>
                     <?php endforeach; ?>
                  <?php else: ?>
                     <p>Keine Units gefunden</p>
                     <a id="addDays" onclick="addDays();" class="btn btn-success text-white">Struktur erstellen</a>
                  <?php endif; ?>
               </div>
            </div>
        </div>
        <div class="tab-pane fade" id="global" role="tabpanel" aria-labelledby="global-tab">
            <div class="form-horizontal mt-3">
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                    <?php echo $this->form->getLabel('published'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                    <?php echo $this->form->getInput('published'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                    <?php echo $this->form->getLabel('id'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                    <?php echo $this->form->getInput('id'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="material" role="tabpanel" aria-labelledby="material-tab">
            <div class="form-horizontal mt-3">
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('material'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('material'); ?>
                     <?php
                     if($this->item->files):
                        $files = json_decode($this->item->files);
                        foreach($files as $file):
                           echo '<div class="row">';
                              echo '<div class="col-12">';
                                 echo '<div class="bg-light p-1 mb-1 border-dark">';
                                    echo '<a href="'.$file->path.'" target="_blank">'.$file->name.'</a>';
                                 echo '</div>';
                              echo '</div>';
                           echo '</div>';
                        endforeach;
                     endif;
                     ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <label>Reserve</label>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('reserve'); ?>
                     <?php
                     if($this->item->reserve):
                        $files = json_decode($this->item->files_reserve);
                        foreach($this->item->reserve as $file):
                           echo '<div class="row">';
                              echo '<div class="col-12">';
                                 echo '<div class="bg-light p-1 mb-1 border-dark">';
                                    echo '<div class="row">';
                                       echo '<div class="col-8">';
                                          echo '<a href="'.$file->path.'" target="_blank">'.$file->filename.'</a>';
                                       echo '</div>';
                                       echo '<div class="col-2">';
                                          echo $file->folder;
                                       echo '</div>';
                                       echo '<div class="col-2">';
                                          if($file->published == 0):
                                             $class = "btn-danger";
                                          else:
                                             $class = "btn-success";
                                          endif;
                                          echo '<a id="freigabe_'.$file->id.'" onclick="stageFile('.$file->id.');" class="btn '.$class.' text-white btn-sm">Freigeben</a>';
                                       echo '</div>';
                                    echo '</div>';
                                 echo '</div>';
                              echo '</div>';
                           echo '</div>';
                        endforeach;
                     endif;
                     ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     Dateien für Teilnehmer
                  </div>
                  <div class="col-12 col-sm-10">
                     <table id="students" class="table table-striped table-sm">
                        <thead>
                           <tr>
                              <th>Name</th>
                              <th>Firma</th>
                              <th></th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              if($this->item->students):
                              $fileID = 0;
                              foreach($this->item->students as $student):
                                 echo '<tr id="student'.$student['id'].'">';
                                 echo '<td><a href="student-edit?layout=edit&id='.$student['id'].'" target="_blank">'.$student['name']. '</a></td>';
                                 echo '<td>'.$student['company'].'</td>';
                                 echo '<td>';
                                 echo '<input type="file" name="jform[material_student]['.$student['id'].'][]" value="" />';
                                 $files = json_decode($this->item->files_students);
                                 if($files):
                                    foreach($files as $file):
                                       if($file->studentID == $student['userID']):
                                          echo '<br/><a href="'.$file->path.'">'.$file->name.'</a>';
                                       endif;   
                                    endforeach;
                                 endif;
                                 echo '</td>';
                                 echo '</tr>';
                                 $fileID++;
                              endforeach;
                           else:
                                 echo '<tr>';
                                 echo '<td colspan="3">Keine Teilnehmer gefunden.</td>';
                                 echo '</tr>';
                           endif;
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
        </div>
        <div class="tab-pane fade" id="quizz" role="tabpanel" aria-labelledby="quizz-tab">
            <div class="form-horizontal mt-3">
               <div class="col-12">
                  <table id="quizze" class="table table-striped table-sm">
                     <thead>
                        <tr>
                           <th>Teilnehmer</th>
                           <th>Quizz</th>
                           <th>Letzter Abschluss</th>
                           <th>Auswertung</th>
                           <th>Score</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        if($this->item->quizze):
                           foreach($this->item->quizze as $quizz):
                              echo '<tr id="student'.$quizz->first_name.'">';
                              echo '<td>'.$quizz->first_name. ' '.$quizz->last_name.'</td>';
                              echo '<td>'.$quizz->title.'</td>';
                              echo '<td>'.date('d.m.Y H:i:s', strtotime($quizz->created)).'</td>';
                              JLoader::register('QuizzresultHelper',JPATH_COMPONENT_SITE.'/helpers/quizzResult.php');
                              $result = new QuizzresultHelper();
                              $qresult = $result->getAuswertung($quizz->quizz);
                              echo '<td>'.$qresult.'</td>';
                              $qresult = $result->getResult($quizz->quizz);
                              echo '<td>'.$qresult.'</td>';
                              echo '</tr>';
                              $fileID++;
                           endforeach;
                           echo '</pre>';
                        else:
                              echo '<tr>';
                              echo '<td colspan="4">Keine Quizze gefunden.</td>';
                              echo '</tr>';
                        endif;
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
        </div>
        <div class="tab-pane fade" id="freigabe" role="tabpanel" aria-labelledby="freigabe-tab">
            <div class="form-horizontal mt-3">
                <div class="form-horizontal mt-3">
                    <div class="form-group row">
                        <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('username'); ?><br/>
                        <small>Der Benutzername ist die E-Mail-Adresse</small>
                        </div>
                        <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('username'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('password'); ?>
                        </div>
                        <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('password'); ?>
                        <?php echo $this->form->getInput('newDS'); ?>
                        <?php echo $this->form->getInput('currentUserID'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<input type="hidden" id="unitCounter" value="<?php echo $unitCounter;?>" />
<input type="hidden" id="dayID" value="" />
<input type="hidden" id="classroomID" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<form action="index.php?option=com_jclassroom&task=classroom.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
   <input id="uploader" title="file input" multiple="true" type="file" accept="" name="uploadCSV" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom.loadFile" method="post" name="uploaderFormUnit" id="uploaderFormUnit" enctype="multipart/form-data">
   <input id="uploaderUnit" title="file input" multiple="true" type="file" accept="" name="uploadUnit[]" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="formUnitID" name="unitID" value="" />
</form>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('.summernote').summernote();
   jQuery('.select2').select2({width: '100%'});
});
   var form = $("#adminForm");
      $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[email]': {
            required: true,
            email: true
         }
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.'
      },
      submitHandler: function(form) {
         var check = 0;
         jQuery('.starttime').each(function() {
            var timeC = jQuery(this).val();
            if (timeC.indexOf(':') == -1) {
               alert("Bitte geben Sie die Zeit im Format 00:00 ein");
               check = 1;
               return false;
            } 
            timeC = timeC.split(':');
            if(timeC[0].length > 2) {
               alert("Bitte geben Sie die Stunden in den Beginnzeiten zweistellig (00) ein.");
               check = 1;
               return false;
            } 
            if(timeC[1].length < 2 || timeC[1].length > 2 || timeC[1] > 59) {
               alert("Bitte geben Sie die Minuten zweistellig (00) ein. Die Minuten dürfen nicht mehr als 59 betragen.");
               check = 1;
               return false;
            } 
         });
         jQuery('.endtime').each(function() {
            var timeC = jQuery(this).val();
            if (timeC.indexOf(':') == -1) {
               alert("Bitte geben Sie die Zeit im Format 00:00 ein");
               check = 1;
               return false;
            } 
            timeC = timeC.split(':');
            if(timeC[0].length > 2) {
               alert("Bitte geben Sie die Stunden in den Endzeiten zweistellig (00) ein.");
               check = 1;
               return false;
            }
            if(timeC[1].length < 2 || timeC[1].length > 2 || timeC[1] > 59) {
               alert("Bitte geben Sie die Minuten zweistellig (00) ein. Die Minuten dürfen nicht mehr als 59 betragen.");
               check = 1;
               return false;
            }
         });
         if(check == 0) {
            form.submit();
         }
      }
   });
   jQuery('#jform_email').on('change', function() {
      var email = jQuery('#jform_email').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
         data: {email:email},
         //dataType: 'json',
         success: function( data ) {
            if(data == 'OK') {
               jQuery('#jform_username').val(email);
            } else {
               alert('Die E-Mail-Adresse wird bereits verwendet.');
               jQuery('#jform_email').val('');
               return false;
            }
         }
      });
   });
   function loadTermine() {
      var kursID = jQuery('#jform_inventa_kursID option:selected').val();
      loadKursData(kursID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadTermine",
         data: {kursID:kursID},
         dataType: 'json',
         success: function( data ) {
            if(data[0]['title']) {
               alert(data[0]['title']);
            } else {
               jQuery(data).each(function(e, value) {
                  console.log(value['beginn']);
                  var o = new Option(value['beginn'] + ' - ' + value['ende'], value['id']);
                  /// jquerify the DOM object 'o' so we can use the html method
                  //$(o).html("option text");
                  jQuery("#jform_inventa_terminID").append(o);
               });
            }
         }
      });
    }
   function loadKursData(kursID) {
      tinyMCE.activeEditor.execCommand('mceInsertContent',false,'');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.loadKursData",
         data: {kursID:kursID},
         //dataType: 'json',
         success: function( data ) {
            tinyMCE.activeEditor.execCommand('mceInsertContent',false,'');
            tinyMCE.activeEditor.execCommand('mceInsertContent',false,data);
         }
      });
    }
   function deleteStudent(id) {
      if(confirm('Soll dieser Teilnehmer aus dem Klassenraum entfernt werden?') == true) {
         var classroomID = jQuery('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteStudent",
            data: {studentID:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               jQuery('#students tr#student' + id).remove();
            }
         });
      }
    }
   function emailVertify(id) {
      if(confirm('Soll dieser Teilnehmer eine Verfizierungsmail erhalten?') == true) {
         var classroomID = jQuery('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.emailVertify",
            data: {studentID:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               alert(data);
            }
         });
      }
    }
   function emailInvitation(id) {
      if(confirm('Soll dieser Teilnehmer eine Einladung zum classroom erhalten?') == true) {
         var classroomID = jQuery('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.emailInvitation",
            data: {studentID:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               alert(data);
            }
         });
      }
    }
   function stageFile(id) {
      if(confirm('Soll diese Datei für die Teilnehmer freigegeben werden?') == true) {
         var classroomID = jQuery('#classroomID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.stageFile",
            data: {id:id, classroomID:classroomID},
            //dataType: 'json',
            success: function( data ) {
               
            }
         });
      }
    }
   function addDays() {
      var $this   = jQuery(this);
      var start   = jQuery('#jform_fromDate').val();
      var end   = jQuery('#jform_toDate').val();
      if(!start || !end) {
      alert('Bitte wählen Sie ein Anfangs- und Enddatum für diesen Klassenraum aus.');
      return false;
      }
      jQuery('#structure').empty();
      jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=classroom.addDays",
        data: {start:start, end:end},
        //dataType: 'json',
        success: function( data ) {
          jQuery('#structure').append(data);
        }
      });
      jQuery('.summernote').summernote();
    }
   function deleteUnit(dayID, key) {
      if(confirm('Soll diese Unit gelöscht werden?') == true) {
         jQuery('#cardFor' + dayID + ' #unit' + key).remove();
      }
   }
   function openDay(id) {
      if(jQuery('#cardFor' + id).hasClass('openDay')) {
         jQuery('#cardFor' + id).removeClass('openDay');
         jQuery('#cardFor' + id + ' .open').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      } else {
         jQuery('#cardFor' + id).addClass('openDay');
         jQuery('#cardFor' + id + ' .open').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      }
   }
   function openUnit(id) {
      console.log(id);
      if(jQuery('#unitContent' + id).hasClass('openUnit')) {
         jQuery('#unitContent' + id).removeClass('openUnit');
         jQuery('#unitContent' + id + ' .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      } else {
         jQuery('#unitContent' + id).addClass('openUnit');
         jQuery('#unitContent' + id + ' .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      }
   }
   function addUnit(id) {
      jQuery('#structure > .card').each(function() {
        jQuery(this).removeClass('openDay');
        jQuery(this).find('.open').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      });
      openDay(id);
      jQuery('#dayID').val(id);
      jQuery('#newUnitType').val('').select2({width: '300px'});
      jQuery('#newPart_dialog').modal();
   }
   jQuery('#newUnitSave').on('click', function(e) {
      var typ = jQuery('#newUnitType option:selected').val();
      getType(typ);
      jQuery('#newPart_dialog').modal('toggle');
   });
   function increaseUnitCounter() { 
      var sections  = parseInt(jQuery('#unitCounter').val()) + 1;
      console.log(sections);
      var label     = ''
      jQuery('#unitCounter').val(sections);
      if(sections == 1) {
         label = 'Sektion';
      } else {
         label = 'Sektionen';
      }
      //$('#sectionsCountDisplay').html(sections + ' ' + label); 
   }
   function getType(typ) {
      var classroomID   = jQuery('#classroomID').val();
      var dayID         = jQuery('#dayID').val();
      var unitCounter   = jQuery('#unitCounter').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.getTemplate",
         data: {typ:typ, classroomID:classroomID,dayID:dayID, unitCounter:unitCounter},
         //dataType: 'json',
         success: function( data ) {
            jQuery('#structure #cardFor' + dayID + ' .card-body .cardMessage').remove();
            jQuery('#structure #cardFor' + dayID + ' .card-body').append(data);
            jQuery('.summernote').summernote();
            increaseUnitCounter();
         }
      });
   }
   function createStudent() {
      jQuery('#newStudent_dialog').modal('show');
   }
   function saveUnit(unitID) {
      var title      = jQuery('#unit' + unitID).find('.title').val();
      var duration   = jQuery('#unit' + unitID).find('.duration').val();
      var content    = jQuery('#unit' + unitID).find('.content').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.saveUnit",
         data: {unitID:unitID,title:title,duration:duration,content:content},
         //dataType: 'json',
         success: function( data ) {
            alert("Unit wurde gespeichert");
         }
      });
   }
   jQuery('#newStudentSave').on('click', function() {
      var classroomID         = jQuery('#classroomID').val();
      var studentSalutation   = jQuery('#studentSalutation option:selected').val();
      var studentFirstname    = jQuery('#studentFirstname').val();
      var studentLastname     = jQuery('#studentLastname').val();
      var studentCompany      = jQuery('#studentCompany').val();
      var studentAdress       = jQuery('#studentAdress').val();
      var studentPostcode     = jQuery('#studentPostcode').val();
      var studentCity         = jQuery('#studentCity').val();
      var studentPhone        = jQuery('#studentPhone').val();
      var studentEmail        = jQuery('#studentEmail').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom.newStudent",
         data: {
            classroomID:classroomID,
            studentSalutation:studentSalutation,
            studentFirstname:studentFirstname,
            studentLastname:studentLastname,
            studentCompany:studentCompany,
            studentAdress:studentAdress,
            studentPostcode:studentPostcode,
            studentCity:studentCity,
            studentPhone:studentPhone,
            studentEmail:studentEmail
         },
         //dataType: 'json',
         success: function( data ) {
            jQuery('#newStudent_dialog').modal('toggle');
            if(data) {
              alert(data);
              return false;
            }
            jQuery('#noStudents').remove();
            jQuery('#students').append(
            '<tr>' + 
            '<td>' + studentFirstname + ' ' + studentLastname + '</td>' +
            '<td>' + studentCompany + '</td>' +
            '<td>' + ' ' + '</td>' +
            '<td>' + ' ' + '</td>' +
            '</tr>'
            );
         }
      });
   });
   jQuery('#openUploader').click(function() {
      jQuery('#uploader').trigger('click');
      
   });
   jQuery('#uploader').on('change', function() {
      jQuery('#uploaderForm').submit();
   });
   function uploadUnit(unitID) {
      jQuery('#formUnitID').val(unitID);
      jQuery('#uploaderUnit').trigger('click');
   }
   jQuery('#uploaderUnit').on('change', function() {
      jQuery('#uploaderFormUnit').submit();
   });
   function deleteFile(fileID) {
      if(confirm("Soll diese Datei gelöscht werden?") == true) {
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=classroom.deleteFile",
            data: {fileID:fileID},
            //dataType: 'json',
            success: function( data ) {
               jQuery('#file' + fileID).remove();
            }
         });
      }
   }
   jQuery( ".card-body" ).sortable({
      handle: '.move',
      axis: 'y',
      update: function (event, ui) {
         var dataSend = jQuery(this).sortable('serialize');
         console.log(dataSend);
         /*jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_plans&task=plan.writeSortable",
            data: {id:id,dataSend:dataSend},
            dataType: 'json',
            success: function( data ){
               //console.log(data);
            }
         });*/
      },
      stop: function(event, ui) {
         var c = 1;
         jQuery('.counter').each(function() {
            jQuery(this).html(c);
            c++;
         });
      }
   });
</script>