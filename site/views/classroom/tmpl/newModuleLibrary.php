<div id="newModuleLibrary" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Neues Modul aus der Bibliothek einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <select id="addModuleFromLibraray">
               <option value="" disabled selected></option>
            </select>
         </div>
         <div class="modal-footer">
            <button onclick="addModule(0,1);" id="newModuleFromLibrary" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="newUnitLibrarayToModuleID" value="" />
         </div>
      </div>
   </div>
</div>