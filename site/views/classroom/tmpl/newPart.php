<style>
   @media(min-width:1280px) AND (max-width: 5120px) {
      .modal-dialog {
         max-width: 900px
      }
      .modal-dialog .card {
         height: 160px;
      }
      .modal-dialog .card a {
         text-align: center;
         padding: 45px 0;
      }
   }
   @media(min-width:768px) AND (max-width: 1024px) {
      .modal-dialog {
         max-width: 700px
      }
      .modal-dialog .card {
         height: 120px;
      }
   }
   .modal-dialog .card {
      cursor: pointer;
      margin-bottom: 10px;
   }
   .modal-dialog .card:hover,
   .modal-dialog .card.selected {
      background-color: #007bff;
      color: #ffffff!important;
   }
   .modal-dialog .card:hover i,
   .modal-dialog .card.selected i {
      color: #ffffff!important;
   }
   .modal-dialog .card i {
      color: #a1a1a1;
   }
</style>
<div id="newPart_dialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Neue Unit einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-12 col-sm-3">
                  <div id="card1" class="card d-flex">
                     <a onclick="chooseElement(1);">
                        <i class="d-block fa fa-file-text-o" style="font-size: 52px;"></i>
                        Freitext
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-3">
                  <div id="card9" class="card d-flex">
                     <a onclick="chooseElement(9);">
                        <i class="d-block fa fa-check-square-o" style="font-size: 52px;"></i>
                        Aufgabe
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-3">
                  <div id="card2" class="card d-flex">
                     <a onclick="chooseElement(2);">
                        <i class="d-block fa fa-graduation-cap" style="font-size: 52px;"></i>
                        virtual classroom
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-3">
                  <div id="card7" class="card d-flex">
                     <a onclick="chooseElement(7);">
                        <i class="d-block fa fa-video-camera" style="font-size: 52px;"></i>
                        Video
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-3">
                  <div id="card3" class="card d-flex">
                     <a onclick="chooseElement(3);">
                        <i class="d-block fa fa-coffee" style="font-size: 52px;"></i>
                        Pause
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-3">
                  <div id="card5" class="card d-flex">
                     <a onclick="chooseElement(5);">
                        <i class="d-block fa fa-user" style="font-size: 52px;"></i>
                        Einzelübung
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-3">
                  <div id="card6" class="card d-flex">
                     <a onclick="chooseElement(6);">
                        <i class="d-block fa fa-users" style="font-size: 52px;"></i>
                        Gruppenübung
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-3">
                  <div id="card4" class="card d-flex">
                     <a onclick="chooseElement(4);">
                        <i class="d-block fa fa-question" style="font-size: 52px;"></i>
                        Quizz
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-3">
                  <div id="card8" class="card d-flex">
                     <a onclick="chooseElement(8);">
                        <i class="d-block fa fa-smile-o" style="font-size: 52px;"></i>
                        Feedback
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button id="newUnitSave" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="newPartToModuleID" value="" />
            <input type="hidden" id="newUnitType" value="0" />
         </div>
      </div>
   </div>
</div>