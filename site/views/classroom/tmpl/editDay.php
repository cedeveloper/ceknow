<div id="dayTitle" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Tag benennen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <!--<div class="modal-body">
            <label>Datum</label>
            <input type="text" id="newDayDate" class="datepicker" placeholder="Tag auswählen" />
         </div>-->
         <div class="modal-body">
            <label>Titel</label>
            <input type="text" id="newDayTitle" class="" placeholder="Titel eingeben" />
            <!--<small>Bitte beachten Sie: bei Eingabe eines Titels wird dieser zuerst angezeigt</small>-->
         </div>
         <div class="modal-footer">
            <button onclick="saveNewClassRoomTitle();" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="titleDayID" value="" />
         </div>
      </div>
   </div>
</div>