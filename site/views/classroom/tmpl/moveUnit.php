<div id="moveUnit" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Unit in anderes Modul verschieben</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="row mb-2">
               <div class="col-12">
                  <small>Verschieben Sie eine Unit in ein anderes Thema</small>
               </div>
               <div class="col-12">
                  <select id="moveUnitToTheme" class="select2-max">
                  </select>
               </div>
            </div>
             <div class="row mb-2">
               <div class="col-12">
                  <small>Verschieben Sie eine Unit in ein anderes Modul in diesem Thema</small>
               </div>
               <div class="col-12">
                  <select id="moveUnitToModule" class="select2-max">
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button onclick="saveMoveUnitToModule();" type="button" class="btn btn-primary">Verschieben</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="oldModuleID" value="" />
            <input type="hidden" id="unitToMove" value="" />
         </div>
      </div>
   </div>
</div>