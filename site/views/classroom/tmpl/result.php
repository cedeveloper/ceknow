
<table id="quizze" class="table table-striped table-bordered table-sm">
   <thead>
      <tr>
         <th width="300"></th>
         <th style="background-color: lightsteelblue; text-align: center;" colspan="<?php echo count($this->item->quizze);?>">Quizze</th>
      </tr>
      <tr>
         <?php if($this->item->quizze): ?>
            <th>Teilnehmer</th>
            <?php foreach($this->item->quizze as $quizz):?>
               <th style="width: 200px;font-size: 12px;background-color: <?php echo $quizz->bgColor;?>; text-align: center;">
                  <i><?php echo $quizz->unitName;?><br/>
                     <span style="color: #000000;">(QID: <?php echo str_pad($quizz->quizzID,6,'0',STR_PAD_LEFT);?> <?php echo $quizz->quizzName;?>)</span>
                  </i><br/>
                  <i>
                     <span style="color: #000000;">(UID: <?php echo str_pad($quizz->unitID,6,'0',STR_PAD_LEFT);?> )</span>
                  </i>
               </th>
            <?php endforeach;?>
         <?php endif;?>
      </tr>
   </thead>
   <tbody>
      <?php
      require_once JPATH_SITE . '/components/com_jclassroom/models/classroom.php';
      $model = JModelLegacy::getInstance('classroom', 'JclassroomModel');
      if($this->item->students):
         $fileID = 0;
         foreach($this->item->students as $student):
            echo '<tr>';
            echo '<td>'.$student['name'].' '.$student['userID'].'</td>';
            if($this->item->quizze):
               foreach($this->item->quizze as $quizz):
                  $checkResult = $model->getResult($student['userID'], $quizz->quizzID, $quizz->unitID);
                  if($checkResult):
                     JLoader::register('ResultHelper',JPATH_COMPONENT_SITE.'/helpers/result.php');
                     $result = new ResultHelper();
                     $html = $result->getQuizzOfUser($quizz->quizzID, $quizz->unitID,$student['userID']);
                     echo $html;
                  else:
                     echo '<td></td>';
                  endif;
                  /*if($quizz->created_by == $student['userID']):
                     
                  else:
                     echo '<td></td>';
                  endif;*/
               endforeach;
            endif;
            echo '</tr>';
         endforeach;
      else:
         echo '<tr>';
         echo '<td>Keine Teilnehmer in diesem Learningroom.</td>';
         echo '</tr>';
      endif;
      ?>
   </tbody>
   <tfoot>
      <tr>
         <?php if($this->item->quizze): ?>
            <td><b>Alle Teilnehmer</b></td>
            <?php foreach($this->item->quizze as $quizz):?>
               <td style="background-color: <?php echo $quizz->bgColor;?>; text-align: center;">
                  <a onclick="completeResultQuizz(<?php echo $quizz->quizzID;?>,<?php echo $quizz->unitID;?>);" class="btn btn-success btn-sm text-white">Auswertung</a>
               </td>
            <?php endforeach;?>
         <?php endif;?>
      </tr>
   </tfoot>
</table>
