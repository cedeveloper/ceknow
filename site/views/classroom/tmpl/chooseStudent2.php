<style>
   .select2-container--default .select2-results > .select2-results__options {
      max-height: 600px!important;
   }
</style>
<div id="chooseStudent2" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 700px!important;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Bestehende Personen einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-12 col-sm-2">
                  <label>Firma</label>

               </div>
               <div class="col-12 col-sm-10">  
                  <select id="chooseCompany" onchange="loadPersons();" class="select2-max">
                     <option value="" selected disabled>Bitte auswählen</option>
                     <?php
                     if($this->item->customersA):
                        echo '<optgroup label="Kunden">';
                        foreach($this->item->customersA as $customersA):
                           $customer_name = '';
                           if($customersA->company_name):
                              $customer_name = $customersA->company_name;
                           else:
                              $customer_name = $customersA->name;
                           endif;
                           echo '<option value="CU'.$customersA->id.'">( '.$customersA->customer_number.' ) '.$customer_name.'</option>';
                        endforeach;
                        echo '</optgroup>';
                     endif;
                     if($this->item->customerAccount):
                        $showName = '';
                        if($this->item->customerAccount->company_name):
                           $showName   = $this->item->customerAccount->company_name;
                        else:
                           $showName   = $this->item->customerAccount->name;
                        endif;
                        echo '<optgroup label="Eigenes Kundenkonto">';
                        echo '<option value="CA'.$this->item->customerAccount->id.'">'.$showName.'</option>';
                        echo '</optgroup>';
                     endif;
                     echo '<optgroup label="Firmen">';
                     if($this->item->companies):
                        foreach($this->item->companies as $company):
                           echo '<option value="CO'.$company->id.'">'.$company->name.'</option>';
                        endforeach;
                     endif;
                     echo '</optgroup>';
                     ?>
                  </select>
                  <small>1. Wählen Sie zunächst eine Firma aus, der die gewünschten Personen zugeordnet sind</small>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2">
                  <label>Personen</label>
               </div>
               <div class="col-12 col-sm-10">   
                  <select id="chooseStudentList" multiple class="select2-max">
                     
                  </select>
                  <small>2. Wählen Sie eine oder mehrere Personen als Teilnehmer aus</small>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button onclick="chooseStudentSave();" id="chooseStudentSave" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>