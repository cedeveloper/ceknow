<div id="comment_dialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Bemerkungen zum Teilnehmer <span id="studentName"></span></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-12 col-sm-12">
                  <textarea id="comment" style="width: 100%;min-height: 300px;"></textarea>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button id="saveComment" onclick="saveComment();" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="commentStudentID" value="" />
         </div>
      </div>
   </div>
</div>