<style>
   .select2-container--default .select2-results > .select2-results__options {
      max-height: 600px!important;
   }
</style>
<div id="chooseStudent" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 700px!important;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Bestehende Personen einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-12 col-sm-12">   
                  <select id="chooseStudentList" multiple class="select2-max">
                     <option value="-2" disabled="true">Bitte auswählen</option>
                     <optgroup label="Teilnehmer">
                     <?php
                     if($this->item->chooseStundents):
                        foreach($this->item->chooseStundents as $student):
                           echo '<option value="'.$student->tblUserID.'">'.$student->first_name.' '.$student->last_name.' ( '.$student->customer_name.' / '.$student->company_name.' )'.'</option>';
                        endforeach;
                     else:
                        echo '<option value="-10" disabled="true">Keine bestehenden Teilnehmer gefunden</option>';
                     endif;
                     ?>
                     </optgroup>
                     <optgroup label="Trainer">
                     <?php
                     if($this->item->chooseTrainer):
                        foreach($this->item->chooseTrainer as $trainer):
                           echo '<option value="'.$trainer->tblUserID.'">'.$trainer->first_name.' '.$trainer->last_name.' ( '.$trainer->company_name.' )'.'</option>';
                        endforeach;
                     else:
                        echo '<option value="-10" disabled="true">Keine Trainer gefunden</option>';
                     endif;
                     ?>
                     </optgroup>
                     <optgroup label="Kundenadministratoren">
                     <?php
                     if($this->item->chooseCustomerAdmins):
                        foreach($this->item->chooseCustomerAdmins as $admin):
                           echo '<option value="'.$admin->tblUserID.'">'.$admin->first_name.' '.$admin->last_name.' ( '.$admin->company_name.' )'.'</option>';
                        endforeach;
                     else:
                        echo '<option value="-10" disabled="true">Keine Administratoren gefunden</option>';
                     endif;
                     ?>
                     </optgroup>
                     <optgroup label="Systemadministratoren">
                     <?php
                     if($this->item->chooseSystemAdmins):
                        foreach($this->item->chooseSystemAdmins as $admin):
                           echo '<option value="'.$admin->tblUserID.'">'.$admin->name.'</option>';
                        endforeach;
                     else:
                        echo '<option value="-10" disabled="true">Keine Administratoren gefunden</option>';
                     endif;
                     ?>
                     </optgroup>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button onclick="chooseStudentSave();" id="chooseStudentSave" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>