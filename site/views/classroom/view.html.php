<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewClassroom extends JViewLegacy {

   	protected $item;
	protected $form;
	protected $state;

    public function display($tpl = null) {
    	$session    = JFactory::getSession();
        $document   = JFactory::getDocument();
        $document->setTitle('Learningroom bearbeiten ['.$session->get('systemname').']');
    	$app 	= JFactory::getApplication();
    	$user   = JFactory::getUser();
    	JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
        $login = new LoginHelper();
        $login->checkLogin();

		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour = 'manager-administrator/learningrooms';
				if($this->item->id):
					$this->retourGlobal = 'classroom-edit?layout=global&id='.$this->item->id;
				else:
					$this->retourGlobal = 'manager-administrator/learningrooms';
				endif;
				$this->usergroup = 'superuser';
				break;
			case 'trainer':
				$this->retour = 'classrooms';
				if($this->item->id):
					$this->retourGlobal = 'classroom-edit?layout=global&id='.$this->item->id;
				else:
					$this->retourGlobal = 'classrooms';
				endif;
				$this->usergroup = 'trainer';
				break;
			case 'student':
				$this->retour = 'manager-student';
				$this->usergroup = 'student';
				break;
			case 'customer':
				$this->retour = 'classrooms';
				if($this->item->id):
					$this->retourGlobal = 'classroom-edit?layout=global&id='.$this->item->id;
				else:
					$this->retourGlobal = 'classrooms';
				endif;
				$this->usergroup = 'customer';
				break;
		}
		
		JLoader::register('LoadpersonsHelper',JPATH_COMPONENT_SITE.'/helpers/loadPersons.php');
        $systemadmins = new LoadpersonsHelper();
        $this->systemadmins = $systemadmins->loadSystemadmins();
        $customeradmins = new LoadpersonsHelper();
        $this->customeradmins = $customeradmins->loadCustomeradmins();
        $trainer = new LoadpersonsHelper();
        $this->trainer = $trainer->loadTrainer();
        // Load the directory
        $path = JPATH_SITE.'/images/learningrooms/LR'.$this->item->id;
        JLoader::register('FilesHelper',JPATH_COMPONENT_SITE.'/helpers/files.php');
        $files = new FilesHelper();
        $this->systemadmins = $systemadmins->loadSystemadmins();
        $this->filesOP = $files->getDirContents($path);
		// Load the learningroom
		$this->learningroom = false;
		if($this->item->id):
			JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
			$load = new GlobalHelper();
			$this->learningroom = $load->getClassroom($this->item->id);
		endif;
		
        parent::display($tpl);
    }
    
}
