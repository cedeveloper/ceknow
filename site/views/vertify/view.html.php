<?php
/**
 *  	DATS Torsten Scheel
 * 	  	JClassroom 1.0
 * 		LMS-System
 * 	 	(C) 2020. All Rights reserved.
 * 		05. April 2020
 *		Development: Torsten Scheel
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class JclassroomViewVertify extends JViewLegacy
{

	function display($tpl = null)
	{
		$input      = JFactory::getApplication()->input;
        $token      = $input->get('SMA', '', 'STR');
        if(!$token):
            echo "CODE 000001: Kein Token gefunden";
        endif;
        JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
        $table      = JTable::getInstance('Configuration','JclassroomTable',array());
        // Load the systemdata
        $load       = array('customerID' => 0, 'parameter' => 'systemLink');
        $table->load($load);
        $this->systemLink = $table->wert;
        JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
        $table      = JTable::getInstance('Configuration','JclassroomTable',array());
        // Load the systemdata
        $load       = array('customerID' => 0, 'parameter' => 'systemName');
        $table->load($load);
        $this->systemName = $table->wert;
        if($token == 'new'):
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('User','JclassroomTable',array());
            $load   = array('email' => $input->get('bnVertify',0,'STR'));
            $check = $table->load($load);
            if($check):
                if($table->password):
                    $this->verified = 1;
                else:
                    $this->verified = 0;
                    $this->welcome = 'Hallo '. $table->name. ',';
                    $this->userID   = $table->id;
                endif;
            endif;
        else:
            $this->closed   = 0;
            $id     = base64_decode($token);
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('User','JclassroomTable',array());
            $check = $table->load($id);
            if($check && $table->password):
                $this->verified = 1;
            else:
                if($id):
                    JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
                    $table1  = JTable::getInstance('Student','JclassroomTable',array());
                    $load1   = array('tblUserID' => $id);
                    $check1  = $table1->load($load1);
                    if($check1):
                        $this->verified = 0;
                        $this->welcome  = 'Hallo '. $table1->first_name.' '.$table1->last_name. ',';
                        $this->userID   = $table1->tblUserID;
                        $this->closed   = 0;
                    endif;
                else:
                    $this->verified = 0;
                    $this->welcome  = 'Wir haben keine Daten zu Ihrer Anfrage gefunden.';
                    $this->userID   = 0;
                    $this->closed   = 1;
                endif;
            endif;
        endif;
		// Display the view
		parent::display($tpl);
	}
}
