<?php
/**
 *  	DATS Torsten Scheel
 * 	  	InVenta 3.0
 * 	 	(C) 2018. All Rights reserved.
 * 		12. Oktober 2018
 *		Editor: Torsten Scheel
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<form action="<?php echo 'index.php?option=com_jclassroom&task=manager.vertify';?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal">
	<?php if($this->verified == 0): ?>
		<div id="intro" class="col-12">
			<h2><?php echo $this->welcome;?></h2>
			<?php if($this->closed == 0): ?>
			<p>bitte erstellen Sie ein Passwort, um Ihren Zugang zu <b style="color: #ff3600;"><?php echo $this->systemName;?></b> zu aktivieren</p>
			<?php endif; ?>
		</div>
		<?php if($this->closed == 0): ?>
		<div class="form-horizontal mt-3 col-12 col-sm-6">
		    <div class="form-group row">
		        <div class="col-12 col-sm-2 col-form-label">
		          Passwort
		        </div>
		        <div class="col-12 col-sm-10">
		        	<input type="password" id="passwort" name="passwort" placeholder="Bitte geben Sie ein Passwort ein" value="" />
		        </div>
		    </div>
		    <div class="form-group row">
		        <div class="col-12 col-sm-2 col-form-label">
		          Passwort wiederholen
		        </div>
		        <div class="col-12 col-sm-10">
		        	<input type="password" id="passwortConfirm" name="passwortConfirm" placeholder="Bitte wiederholen Sie das Passwort" value="" />
		        </div>
		    </div>
		    <div class="form-group row">
		        <div class="col-12 col-sm-10 col-form-label">
		          Bitte bestätigen Sie, dass Sie unsere Datenschutzerklärung zur Kenntnis genommen haben.
		        </div>
		        <div class="col-12 col-sm-2">
		        	<input type="checkbox" value="Datenschutzerklärung" />
		        </div>
		    </div>
		    <div class="form-group row">
		        <div class="col-12 col-sm-10 col-form-label">
		          Bitte bestätigen Sie, dass Sie unsere AGB zur Kenntnis genommen haben.
		        </div>
		        <div class="col-12 col-sm-2">
		        	<input type="checkbox" value="AGB" />
		        </div>
		    </div>
		    <div class="form-group row">
		        <div class="col-12 col-sm-10 col-form-label">
		          Bitte bestätigen Sie, dass Sie damit einverstanden sind, dass wir Webinare aufzeichnen.
		        </div>
		        <div class="col-12 col-sm-2">
		        	<input type="checkbox" value="Aufzeichnung" />
		        </div>
		    </div>
		    <input type="hidden" name="uid" value="<?php echo $this->userID;?>" />
		    <button id="submit" type="submit" class="btn btn-success text-white">Zugang aktivieren</button>
		</div>
		<?php endif; ?>
	<?php else: ?>
		<div id="intro" class="col-12">
			<h2><?php echo $this->welcome;?></h2>
			<p>Sie haben Ihren Zugang zu  <b style="color: #ff3600;"><?php echo $this->systemName;?></b> bereits aktiviert. Über diesen <a style="color: #ff3600;" href="<?php echo $this->systemLink;?>/login">Link</a> gelangen Sie zur Anmeldung.</p>
		</div>
	<?php endif; ?>
</form>
<script type="text/javascript">
	jQuery('#submit').on('click', function(e) {
		
		var p1 = jQuery('#passwort').val();
		var p2 = jQuery('#passwortConfirm').val();
		if(p1 && p2) {
			if(p1 != p2) {
				e.preventDefault();
				alert('Die Passwörter stimmen nicht überein.');
				return false;
			} 
		} else {
			e.preventDefault();
			alert('Bitte füllen Sie beiden Passwortfelder aus. Achten Sie darauf, dass beide Passwörter übereinstimmen.');
			return false;
		}
		jQuery('form').submit();
	});
</script>