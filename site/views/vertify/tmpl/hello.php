<?php
/**
 *  	DATS Torsten Scheel
 * 	  	InVenta 3.0
 * 	 	(C) 2018. All Rights reserved.
 * 		12. Oktober 2018
 *		Editor: Torsten Scheel
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<form action="<?php echo 'index.php?option=com_jclassroom&task=manager.vertify';?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal">
	<br>
	<br>
	<br>
	<br>
	<br>
	<h2>Vielen Dank für Ihre Verifizierung!</h2>
	<p>Sie können sich nun unter <a href="<?php echo $this->systemLink;?>/login"><?php echo $this->systemName;?></a> mit Ihrem Benutzernamen (= E-Mail-Adresse) und dem soeben erstellten Passwort anmelden.</p>
	<p>Viel Erfolg mit <?php echo $this->systemName;?></p>
	
</form>