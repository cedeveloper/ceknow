<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="index.php?option=com_jclassroom&task=students.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept=".csv" name="uploadCSV" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <a class="btn btn-success text-white" href="textmodule-edit">Neuer Textbaustein</a>
        <a class="btn btn-danger text-white m-1" id="deleteTextmodule">Textbaustein(e) löschen</a>
        <!--<button type="button" class="btn btn-info" onclick="Joomla.submitform('students.saveCSV')">Liste exportieren</button>-->
        <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right">Zurück</a>
    </div>
    <table class="table table-striped">	
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Name', 'a.title', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Bereich', 'a.section', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left" width="200">
                    <?php echo JHtml::_('grid.sort', 'Text', 'a.theText', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap left" width="100">
                    Betreff
                </th>
                <th class="nowrap left" width="200">
                    Beschreibung
                </th> 
                <th class="nowrap left">
                    Erstellt am/von<br>Bearbeiten am/von
                </th> 
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Status'), 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>		
        <tbody>
        <?php if($this->items) { ?>
        <?php foreach ($this->items as $i => $item) :
        ?>
            <tr class="row<?php echo $i % 2; ?>">
            	<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
				<td class="right"><?php echo $this->escape($item->id); ?></td>
                <td class="left">
                    <a href="<?php echo JURI::Root().'textmodule-edit?id='. $item->id; ?>">
                    <?php echo $item->title; ?>
                    </a>
                </td>
                <td class="left">
                    <?php
                    switch($item->section):
                        case 1:
                            $section = 'Planbestellung';
                            break;
                        case 2:
                            $section = 'Planänderung';
                            break;

                    endswitch;
                    ?>
                    <?php echo $section; ?>    
                </td>
                <td style="font-size: 9pt;line-height: 11pt;">
                    <?php 
                        $text = strip_tags($item->theText);
                        echo substr($text, 0,100);
                    ?>
                </td>
                <td>
                    <span class="d-inline-block" style="font-size: 12px;line-height: 14px;"><?php echo $item->theSubject;?></span>
                </td>
                <td>
                    <span class="d-inline-block" style="font-size: 12px;line-height: 14px;"><?php echo strip_tags($item->theDescription);?></span>
                </td>
                <td class="left">
                    <?php echo date('d.m.Y H:i', strtotime($item->created)).' / '.$item->creator; ?><br/>
                    <?php echo date('d.m.Y H:i', strtotime($item->modified)).' / '.$item->modifier; ?><br/>
                </td>
                <?php
                if($item->published == 0) {
					$class="badge-danger";
					$wert = "<i class='fa fa-ban'></i>";
				} elseif($item->published == 1) {
					$class="badge-success";
					$wert = "<i class='fa fa-check'></i>";
				}
				?>
                <td class="text-center">
                	<span class="btn <?php echo $class;?>"><?php echo $wert;?></span>
               	</td>
            </tr>
        <?php endforeach ?>
        <?php } else { ?>
        	<tr>
            	<td colspan="8">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
       	<?php } ?>
        </tbody>			
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" id="usergroup" value="<?php echo $this->usergroup;?>" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        var usergroup = $('#usergroup').val();
        if(usergroup == 'customer') {
            $('#filter_customerID').parent('div').remove();
        }
    });
jQuery('#openUploader').click(function() {
    jQuery('#uploader').trigger('click');
    
});
jQuery('#uploader').on('change', function() {
    jQuery('#uploaderForm').submit();
});
$('#deleteTextmodule').on('click', function() {
    if(confirm('Sollen die ausgewählten Textbausteine gelöscht werden?') == true) {
        Joomla.submitform('textmodules.delete');
    }
});
</script>