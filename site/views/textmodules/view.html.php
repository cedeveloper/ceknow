<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewTextmodules extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;
	protected $toolbar;

    public function display($tpl = null) {
    	$session = JFactory::getSession();

    	require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        // Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
	        JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
	        $demo = new LoginHelper();
	        $demo->checkDemo();
	    endif;

		$document = JFactory::getDocument();
		$document->setTitle('Textbausteine ['.$session->get('systemname').']');
		$this->items 		 = $this->get('Items');
		$this->state 		 = $this->get('State');
		$this->pagination 	 = $this->get('Pagination');
		$this->user		 	 = JFactory::getUser();
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour 	= 'manager-administrator';
				$this->usergroup= 'superuser';
				break;
			case 'customer':
				$this->retour 	= 'manager-customer';
				$this->usergroup= 'customer';
				break;
			case 'trainer':

				break;
			case 'student':

				break;
		}
        parent::display($tpl);
    }
}
