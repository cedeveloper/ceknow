<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

    <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a class="btn btn-success text-white" onclick="Joomla.submitform('student.simpleSave')">Speichern</a>
         <a class="btn btn-secondary text-white" onclick="Joomla.submitform('student.save')">Speichern & Schließen</a>
      <?php endif;?>
        <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
    </div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Kundendaten</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="asp-tab" data-toggle="tab" href="#asp" role="tab" aria-controls="asp" aria-selected="false">Ansprechpartner</a>
        </li>
        <?php if($this->usergroup == 'superuser') { ?>
        <li class="nav-item">
            <a class="nav-link" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-controls="global" aria-selected="false">Global</a>
        </li>
        <?php } ?>
        <li class="nav-item">
            <a class="nav-link" id="freigabe-tab" data-toggle="tab" href="#freigabe" role="tab" aria-controls="freigabe" aria-selected="false">Freigabe</a>
        </li>
    </ul>
    <div class="tab-content" id="tabContent">
        <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
            <div class="form-horizontal mt-3">
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('first_name'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('first_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('last_name'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('last_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('customer_number'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('customer_number'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('company'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('company'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('adress'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('adress'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('postcode'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('postcode'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('city'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('city'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('phone'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('phone'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('fax'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('fax'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('email'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('web'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('web'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('logo'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('logo'); ?>
                      <?php
                      if($this->item->logo) {
                         echo '<img style="width: 150px;margin-top: 20px;" src="'.$this->item->logo.'" />';
                      }
                      ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="asp" role="tabpanel" aria-labelledby="asp-tab">
            <div class="form-horizontal mt-3">
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('asp_name'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('asp_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('asp_phone'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('asp_phone'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('asp_email'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('asp_email'); ?>
                    </div>
                </div>
            </div>
       	</div>
        <div class="tab-pane fade" id="global" role="tabpanel" aria-labelledby="global-tab">
            <div class="form-horizontal mt-3">
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                    <?php echo $this->form->getLabel('published'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                    <?php echo $this->form->getInput('published'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                    <?php echo $this->form->getLabel('id'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                    <?php echo $this->form->getInput('id'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="freigabe" role="tabpanel" aria-labelledby="freigabe-tab">
            <div class="form-horizontal mt-3">
                <div class="form-horizontal mt-3">
                    <div class="form-group row">
                        <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('username'); ?><br/>
                        <small>Der Benutzername ist die E-Mail-Adresse</small>
                        </div>
                        <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('username'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('password'); ?>
                        </div>
                        <div class="col-12 col-sm-9">
                        <?php echo $this->form->getInput('password'); ?>
                        <?php echo $this->form->getInput('newDS'); ?>
                        <?php echo $this->form->getInput('currentUserID'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<input type="hidden" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[email]': {
            required: true,
            email: true
         },
         'jform[first_name]': {
            required: true
         },
         'jform[last_name]': {
            required: true
         }
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[first_name]': 'Bitte geben Sie Ihren Vornamen ein.',
         'jform[last_name]': 'Bitte geben Sie Ihren Nachnamen ein.'
      },
      submitHandler: function(form) {
         form.submit();
      }
   });
   jQuery('#jform_email').on('change', function() {
      var email = jQuery('#jform_email').val();
      jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
            data: {email:email},
            //dataType: 'json',
            success: function( data ) {
               if(data == 'OK') {
                  jQuery('#jform_username').val(email);
               } else {
                  alert('Die E-Mail-Adresse wird bereits verwendet.');
                  jQuery('#jform_email').val('');
                  return false;
               }
            }
        });
   });
</script>