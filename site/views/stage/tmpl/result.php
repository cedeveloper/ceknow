<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$doc = JFactory::getDocument();
$session = JFactory::getSession();
$doc->addScript('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js');
$doc->addScript('components/com_jclassroom/assets/js/jQuery.print.js');
$input = JFactory::getApplication()->input;
?>
<style>
    .quizzResult_theQuestion {
        border-bottom: 2px solid #aaa;
    }
    .quizzResult_yourAnswer {
        font-size: 18px;
        font-weight: bold;
        color: cornflowerblue;
    }
   canvas {
      max-width: 400px!important;
      max-height: 400px!important;
   }
   table p {
    margin: 0px;
   }
   .print {
    display: none;
   }
</style>
<style type="text/css" media="print">
   .print { 
    display: inline-block ;
    width: 100%;
  }
</style>
<form action="" method="post" name="adminForm" id="adminForm">
    <div class="col-12">
      <?php
      if($this->preview == 1):
          echo '<span class="badge bg-primary text-white"><i class="fa fa-eye"></i> Preview aktiv</span>';
      endif;
      ?>
      <div class="alert alert-info">
        <i class="fa fa-exclamation-circle text-danger"></i> Bitte schließen Sie diesen Browsertab, um zurückzukehren.
      </div>
    </div>
   	<div class="col-12">
    	<h3>Vielen Dank für Ihre Teilnahme!</h3>
    	<h4>Hier ist Ihr Ergebnis im Detail</h4>
      <div id="theResult">
         
       	<?php echo $this->result;?>
      </div>
      <canvas id="theModalChart" height="200" width="400"></canvas>
      <p class="mt-4">
      <!--<a onclick="quitQuizz();" class="btn btn-primary text-white"><i class="fa fa-sign-out"></i> Quizz verlassen</a>-->
      <a onclick="restartQuizz();" class="btn btn-secondary text-white"><i class="fa fa-refresh"></i> Quizz neu starten</a>
      <a onclick="printResult();" class="btn btn-success text-white"><i class="fa fa-certificate"></i> Auswertung drucken</a>
      <!--<a href="index.php?option=com_jclassroom&task=stage.printCertificate&rID=<?php //echo $this->theResultID;?>" target="_blank" class="btn btn-success text-white"><i class="fa fa-certificate"></i> Zertifikat drucken</a>-->
      </p>
    </div>
    <input type="hidden" id="quizzID" value="<?php echo $this->quizzID;?>" />
    <input type="hidden" id="clr" value="<?php echo $this->clr;?>" />
    <input type="hidden" id="uID" value="<?php echo $this->uID;?>" />
    <input type="hidden" id="stageType" value="quizz" />
    <input type="hidden" id="publishedQuizzID" value="<?php echo $this->publishedQuizzID;?>" />
    <input type="hidden" id="theResultID" name="theResultID" value="<?php echo $this->theResultID;?>" />
    <input type="hidden" id="preview" name="preview" value="<?php echo $this->preview;?>" />
    <input type="hidden" name="option" value="com_auditum" />
    <input type="hidden" name="task" value="audit.stop" />
    <?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   $(document).ready(function() {
      $('ul.menu.nav').css('display', 'none');
      var theResultID = $('#theResultID').val();
      loadSingleChart(theResultID);
   });
   function printReport() {  
      $('#theResult').print({
         globalStyles: true,
         mediaPrint: false
      });
   }
   function printResult() {
      var quizzID     = $('#quizzID').val();
      var theResultID = $('#theResultID').val();
      var publishedQuizzID = $('#publishedQuizzID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=stage.printResult",
         data: {
            quizzID:quizzID,
            theResultID:theResultID,
            publishedQuizzID:publishedQuizzID,
            save: 1
         },
         //dataType: 'json',
         success: function( data ) {
            window.open(data);
         }
      });
   }
   function loadSingleChart(theResultID) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.loadSingleChart",
         data: {theResultID:theResultID},
         dataType: 'json',
         success: function( data ) {
          console.log(data);
            if(data && data['calculate'] == 2) {
               writeChart(data,'');
            }
         }
      });
    }
   function restartQuizz() {
      if(confirm('Möchten sie das Quizz neu starten?') == true) {
         var quizzID          = $('#quizzID').val();
         var publishedQuizzID = $('#publishedQuizzID').val();
         var stageType        = $('#stageType').val();
         var preview          = $('#preview').val();
         var clr              = $('#clr').val();
         var uID              = $('#uID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=stage.restartQuizz",
            data: {publishedQuizzID:publishedQuizzID,stageType:stageType,quizzID:quizzID,preview:preview,clr:clr,uID:uID},
            //async: false,
            success: function( data ) {
               window.location.href = data;
            }
         });
      }
   }
    function writeChart(data, i) {
      var labels = new Array();
      jQuery.each(data['labels'], function(e, value) {
         labels.push(value);
      });
      var datas = new Array();
      jQuery.each(data['datas'], function(e, value) {
         datas.push(value);
      });
      var backgroundColor = [
         'rgba(255,99,132,1)',
         'rgba(54,162,235,1)',
         'rgba(75,192,192,1)',
         'rgba(22,144,166,1)'
      ]
      var ctx     = document.getElementById('theModalChart' + i).getContext("2d");
      ctx.canvas.height  = 500;
      ctx.canvas.width   = 500;
      if(data['chart'] == 'bar' || data['chart'] == 'horizontalBar') {
         var myChart = new Chart(ctx, {
            type: data['chart'],
            data: {
               labels: labels,
               datasets: [{
                  data: datas,
                  backgroundColor: backgroundColor
               }]
            },
            options: {
               legend: {
                  display: false
               },
               responsive:true,
               maintainAspectRatio: false,
               scales: {
                  yAxes: [{
                      ticks: {
                           stepSize: 1,
                           beginAtZero: true
                      }
                  }],
                  xAxes: [{
                      ticks: {
                           stepSize: 1,
                           beginAtZero: true
                      }
                  }]
               }
            }
         });
      }
      if(data['chart'] == 'pie') {
         var myChart = new Chart(ctx, {
            type: data['chart'],
            data: {
               labels: labels,
               datasets: [{
                  data: datas,
                  backgroundColor: backgroundColor
               }]
            },
            options: {
               scales: {
                  yAxes: [{
                     gridLines: {
                        min: 0,
                        max: 20,
                        stepSize: 1,
                        display: false
                     }
                  }],
                  xAxes: [{
                     gridLines: {
                        display: false
                     }
                  }]
               }
            }
         });
      }
   }
  function quitQuizz() {
    if(confirm('Sind Sie sicher, dass Sie das Quizz beenden möchten?') == true) {
      //alert('Sie haben das Quizz beendet. Bitte schließen Sie diesen Browser-Tab um zu Ihrem Learningroom zurück zu kehren.');
        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=stage.quitQuizz",
            //data: {id:id},
            //async: false,
            success: function( data ) {
                window.location.href = data;
            }
        });
    }
}
</script>