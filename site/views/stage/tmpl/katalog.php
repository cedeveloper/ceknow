<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$dokument = JFactory::getDocument();
$dokument->addScript('components/com_jclassroom/assets/js/jquery.validate.js' );
$dokument->addScript('components/com_jclassroom/assets/js/stage.js');
require_once(JPATH_COMPONENT.'/views/stage/tmpl/message.php');
$now = new DateTime();
$session = JFactory::getSession();
$cart = $session->get('quizzes');
?>
<style>
    input[type="checkbox"] {
        height: 30px;
        width: 30px;
        margin: 5px 0;
    }
    .smileys {
        display: flex;
        justify-content: space-between;
        margin-bottom: 50px;
        margin-top: 50px;
    }
    .smiley {
        width: 120px;
        height: 120px;
        margin: 0 25px;
        cursor: pointer;
        transition: all 0.4s ease-in-out;
    }
    .smiley:hover {
        transform: rotate(45deg);
    }
    .hidden {
        display: none;
    }
    textarea {
        width: 100%;
        min-height: 100px;
        border: 1px solid #a1a1a1;
        border-radius: 3px;
    }
    .answer:hover {
        opacity: 0.8;
    }
    .answer.choosen.yes {
        background-color: forestgreen!important;
    }
    .answer.choosen.no {
        background-color: indianred!important;
    }
    .answer.choosen.a {
        background-color: forestgreen!important;
    }
    .answer.choosen.aa {
        background-color: gold!important;
    }
    .answer.choosen.n {
        background-color: indianred!important;
    }
    .smileys .smileyActive img {
        border: 5px solid forestgreen;
        padding: 5px;
        border-radius: 10px;
        transform: scale(150%);
    }
    .theTextInput {
        display: inline!important;
        width: 200px!important;
        margin: 0px!important;
        padding: 2px!important;
        height: 30px;
    }
    @media(min-width: 0px) AND (max-width: 767px) {
        .theCheckboxText {
            width: 90%;
        }
    }
    @media(min-width: 768px) {
        .theCheckboxText {
            width: 30%;
        }
    }
</style>

<form action="<?php JRoute::_('index.php?option=com_jclassroom&task=unit.checkQuestion'); ?>" method="post" name="adminForm" id="adminForm">
	<div class="stage row">
        <div class="col-12">
            <?php
            if($this->preview == 1):
                echo '<span class="badge bg-primary text-white"><i class="fa fa-eye"></i> Preview aktiv</span>';
            endif;
            ?>
        	<h3><i class="fa fa-commenting"></i> <?php echo $this->unit['title']?></h3>
            <?php
                if($this->q == -1):
                    if($this->unit['mustBeAnswered'] == 1):
                        echo '<i class="fa fa-exclamation-circle"></i> Alle Fragen in diesem Quizz müssen beantwortet werden.';
                    endif;
                    echo $this->unit['description'];
                endif;
            ?>
      	</div>
        <?php if($this->q != -1): ?>
           	<div class="col-12">
           		<div class="row">
                    <div class="col-12 "> 
                        <div class="card">
                            <div class="card-body">
                                <?php 
                                    $answered = 0;
                                    if($this->unit['quizzType'] != 1 && $this->unit['quizzType'] != 14 ):
                                        if($this->unit['type'] != 1 && $this->unit['type'] != 14):
                                            echo '<h2>Frage '.($this->quc).' von '. $this->unit['countSections'].'</h2>';
                                        endif;
                                        echo '<h1>'.$this->unit['question'].'</h1>';
                                    endif;
                                    if($this->unit['quizzType'] == 1):
                                        echo '<h2>Karte '.($this->showQ + 1).' von '. $this->unit['countSections'].'</h2>';
                                        if($this->unit['type'] == 14):
                                            echo '<h1><span class="badge bg-success text-white">Prüffeld: '.$this->unit['question'].'</span></h1>';
                                        else:
                                            echo '<h1>'.$this->unit['question'].'</h1>';
                                        endif;
                                    endif;
                                ?>
                                <?php
                                if($this->unit['countSections'] == 0):
                                    echo '<p>Dieses Quizz beinhaltet keine Fragen.</p>';
                                endif;
                                if($this->unit['correctAnswers'] > 1):
                                    echo '<p>Mehrere Antworten sind möglich.</p>';
                                endif;
                                if($this->unit['type'] != 7):
                                    echo html_entity_decode($this->unit['content']);
                                endif;
                                // WWM
                                if($this->unit['type'] == 3):
                                    if($this->unit['answers']):
                                        foreach($this->unit['answers'] as $answer):
                                            $currentAnswerID;
                                            $aColor = 'lightsteelblue';
                                            $aClass ='';
                                            if($cart):
                                                foreach($cart as $item):
                                                    if($item['questionID'] == $this->unit['questionID']):
                                                        foreach($item['content'] as $content):
                                                            $currentAnswers = explode('_',$content);
                                                            $currentAnswerID = $currentAnswers[0];
                                                            if($answer->id == $currentAnswerID):
                                                                $aColor = 'forestgreen';
                                                                $aClass = 'choosen';
                                                                $answered = 1;
                                                            else:
                                                                $aColor = 'lightsteelblue';
                                                                $aClass = '';
                                                            endif;
                                                        endforeach;
                                                    endif;
                                                endforeach;
                                            endif;
                                            if($this->unit['correctAnswers'] == 2):
                                                echo '<h2><a id="answer'.$answer->id.'" onclick="chooseAnswer('.$answer->id.',0);" class="'.$aClass.' answer btn w-100" style="text-align: left;background-color:'.$aColor.';"><b>'.$answer->title.'</b></a></h2>';
                                            else:  
                                                echo '<h2><a id="answer'.$answer->id.'" onclick="checkQuestion('.$answer->id.');" class="'.$aClass.' answer btn w-100" style="text-align: left;background-color:'.$aColor.';"><b>'.$answer->title.'</b></a></h2>';
                                            endif;
                                        endforeach;
                                    endif;
                                endif;
                                // Smiley
                                if($this->unit['type'] == 4):
                                    if($this->unit['existingAnswer']['answer']):
                                        $valueA = $this->unit['existingAnswer']['answer'];
                                        $s1     = '';
                                        $s2     = '';
                                        $s3     = '';
                                        $s4     = '';
                                        $s5     = '';
                                        switch($valueA):
                                            case 1:
                                                 $s1 = 'smileyActive';
                                                break;
                                            case 2:
                                                 $s2 = 'smileyActive';
                                                break;
                                            case 3:
                                                $s3 = 'smileyActive';
                                                break;
                                            case 4:
                                                 $s4 = 'smileyActive';
                                                break;
                                            case 5:
                                                 $s5 = 'smileyActive';
                                                break;
                                        endswitch;
                                    endif;
                                    echo '<div class="smileys">';
                                    echo '<div id="smiley1" class="'.$s1.' smiley" onclick="nextQuestion(1);">
                                        <img src="images/jclassroom/smiley1.png" />
                                        </div>';
                                    echo '<div id="smiley2" class="'.$s2.' smiley" onclick="nextQuestion(2);">
                                        <img src="images/jclassroom/smiley2.png" />
                                        </div>';
                                    echo '<div id="smiley3" class="'.$s3.' smiley" onclick="nextQuestion(3);">
                                        <img src="images/jclassroom/smiley3.png" />
                                        </div>';
                                    echo '<div id="smiley4" class="'.$s4.' smiley"onclick="nextQuestion(4);">
                                        <img src="images/jclassroom/smiley4.png" />
                                        </div>';
                                    echo '<div id="smiley5" class="'.$s5.' smiley" onclick="nextQuestion(5);">
                                        <img src="images/jclassroom/smiley5.png" />
                                        </div>'; 
                                    echo '</div>';
                                endif;
                                // Ja/Nein
                                if($this->unit['type'] == 5):
                                    $aColor = 'lightsteelblue';
                                    $class  = '';
                                    if($this->unit['existingAnswer']['answer']):
                                        $valueA = $this->unit['existingAnswer']['answer'];
                                        $valueA = explode('_', $valueA);
                                        $value  = $valueA[1];
                                        if($value == 'answerJ'):
                                            $class = 'choosen';
                                        endif;
                                    endif;
                                    echo '<h2><a id="answerJ" onclick="JNAnswer(1);" class="'.$class.' answer yes btn w-100" style="text-align: left;background-color:'.$aColor.';"><b>Ja</b></a></h2>';
                                    $class  = '';
                                    if($this->unit['existingAnswer']['answer']):
                                        $valueA = $this->unit['existingAnswer']['answer'];
                                        $valueA = explode('_', $valueA);
                                        $value  = $valueA[1];
                                        if($value == 'answerN'):
                                            $class = 'choosen';
                                        endif;
                                    endif;
                                    echo '<h2><a id="answerN" onclick="JNAnswer(0);" class="'.$class.' answer no btn w-100" style="text-align: left;background-color:'.$aColor.';"><b>Nein</b></a></h2>';
                                endif;
                                // Audit
                                if($this->unit['type'] == 6):
                                    echo '<p style="font-size: 24px;">';
                                    if($this->unit['testfield']):
                                        echo '<span class="badge bg-success text-white mr-1">'.$this->unit['testfield'].'</span>';
                                    endif;
                                    if($this->unit['theme']):
                                        echo '<span class="badge bg-primary text-white"> '.$this->unit['theme'].'</span>';
                                    endif;
                                    echo '</p>';
                                    $class  = '';
                                    $exeption       = 'none';
                                    $exeptionText   = '';
                                    $exeptionPoints = '';
                                    if($this->unit['existingAnswer']['answer']):
                                        $value = $this->unit['existingAnswer']['answer'];
                                        if($value == 'A_'):
                                            $class = 'choosen';
                                        endif;
                                    endif;
                                    echo '<p>'.$this->unit['infotext'].'</p>';
                                    echo '<h2><a id="answerA" onclick="auditAnswer(&quot;A&quot;);" class="'.$class.' answer a hover-blue btn w-100" style="text-align: left;background-color: #e1e1e1;"><b>Ja</b></a></h2>';
                                    $class  = '';
                                    if($this->unit['existingAnswer']['answer']):
                                        $valueA = $this->unit['existingAnswer']['answer'];
                                        $value = explode('_',$valueA);
                                        if($value[0] == 'AA'):
                                            $class = 'choosen';
                                            $exeption = 'block';
                                            $exeptionText = $value[2];
                                            switch($value[1]):
                                                case 1:
                                                    $selected1 = 'selected';
                                                    $selected2 = '';
                                                    $selected3 = '';
                                                    $selected5 = '';
                                                    break;
                                                case 2:
                                                    $selected1 = '';
                                                    $selected2 = 'selected';
                                                    $selected3 = '';
                                                    $selected5 = '';
                                                    break;
                                                case 3:
                                                    $selected1 = '';
                                                    $selected2 = '';
                                                    $selected3 = 'selected';
                                                    $selected5 = '';
                                                    break;
                                                case 5:
                                                    $selected1 = '';
                                                    $selected2 = '';
                                                    $selected3 = '';
                                                    $selected5 = 'selected';
                                                    break;
                                            endswitch;
                                        else:
                                            $exeption = 'none';
                                        endif;
                                    endif;
                                    echo '<h2><a id="answerAA" onclick="auditAnswer(&quot;AA&quot;);" class="'.$class.' answer aa hover-blue btn w-100" style="text-align: left;background-color: #e1e1e1;"><b>Ja, mit Ausnahme</b></a></h2>';
                                    $class  = '';
                                    if($this->unit['existingAnswer']['answer']):
                                        $value = $this->unit['existingAnswer']['answer'];
                                        if($value == 'N_'):
                                            $class = 'choosen';
                                            $exeption = 'none';
                                        endif;
                                    endif;
                                    echo '<h2><a id="answerN" onclick="auditAnswer(&quot;N&quot;);" class="'.$class.' answer n hover-blue btn w-100" style="text-align: left;background-color: #e1e1e1;"><b>Nein</b></a></h2>';
                                    echo '<div id="exeption" style="display: '.$exeption.';">';
                                    echo '<h5>Grund für die Ausnahme</h5>';
                                    echo '<textarea id="exeptionText" class="mb-2">'.$exeptionText.'</textarea>';
                                    echo '<h5>Abweichende Punkte</h5>';
                                    echo '<small class="d-block">Wenn Sie abweichende Punkte vergeben möchten, wählen Sie diese bitte hier aus.</small>';
                                    echo '<select id="exeptionPoints" class="select2">';
                                    echo '<option selected value="">Keine abweichenden Punkte</option>';
                                    echo '<option '.$selected1.' value="1">1</option>';
                                    echo '<option '.$selected2.' value="2">2</option>';
                                    echo '<option '.$selected3.' value="3">3</option>';
                                    echo '<option '.$selected5.' value="5">5</option>';
                                    echo '</select>';
                                    echo '</div>';
                                endif;
                                // Text mit 1 Eingabe
                                if($this->unit['type'] == 7):
                                    $theText    = html_entity_decode($this->unit['content']);
                                    $theInput   = '<input class="theTextInput" type="text" id="textFeld" value=""/>';
                                    if($this->unit['existingAnswer']['answer']):
                                        $valueA = $this->unit['existingAnswer']['answer'];
                                        $valueA = explode('_', $valueA);
                                        $value  = $valueA[1];
                                    else:
                                        $value = '';
                                    endif;
                                    echo str_replace('{}', $theInput, $theText);
                                endif;
                                // Textfeld
                                if($this->unit['type'] == 10):
                                    if($this->unit['existingAnswer']['answer']):
                                        $valueA = $this->unit['existingAnswer']['answer'];
                                        $valueA = explode('_', $valueA);
                                        $value  = $valueA[1];
                                    else:
                                        $value = '';
                                    endif;
                                    echo '<input type="text" id="textFeld" value="'.$value.'" placeholder="Bitte geben Sie hier Ihre Antwort ein."/>';
                                endif;
                                if($this->unit['type'] == 11):
                                    if($this->unit['existingAnswer']['answer']):
                                        $valueA = $this->unit['existingAnswer']['answer'];
                                        $valueA = explode('_', $valueA);
                                        $value  = $valueA[1];
                                    else:
                                        $value = '';
                                    endif;
                                    echo '<textarea style="width: 100%; min-height: 100px;" id="textArea">'.$value.'</textarea>';
                                endif;
                                // Auswahlliste
                                if($this->unit['type'] == 12):
                                    if($this->unit['correctAnswers'] == 2):
                                        $multiple   = 'multiple';
                                        $first      = '';
                                    else:
                                        $multiple   = '';
                                        $first      = '<option value="" selected disabled>Bitte auswählen</option>';
                                    endif;
                                    echo '<select class="select2-max" id="auswahlliste" '.$multiple.'>';
                                    echo $first;
                                    if($this->unit['options']):
                                        foreach($this->unit['options'] as $option):
                                            if($this->unit['existingAnswer']['answer']):
                                                $valueA = $this->unit['existingAnswer']['answer'];
                                                $valueA = explode('_', $valueA);
                                                if($valueA[0] == $option->id):
                                                    $value  = 'selected';
                                                else:
                                                    $value = '';
                                                endif;
                                            else:
                                                $value = '';
                                            endif;
                                            echo '<option '.$value.' value="'.$option->id.'">'.$option->title.'</option>';
                                        endforeach;
                                    endif;
                                    echo '</select>';
                                endif;
                                // Checkboxen
                                if($this->unit['type'] == 13):
                                    if($this->unit['checkboxes']):
                                        foreach($this->unit['checkboxes'] as $checkbox):
                                            if($this->unit['existingAnswer']['answer']):
                                                $valueA = $this->unit['existingAnswer']['answer'];
                                                $valueA = explode('_', $valueA);
                                                if($valueA[0] == $checkbox->id):
                                                    $value  = 'checked';
                                                else:
                                                    $value = '';
                                                endif;
                                            else:
                                                $value = '';
                                            endif;
                                            echo '<div class="checkbox_container d-flex" style="border-bottom: 1px solid #a1a1a1;width: 100%;align-items: center;">';
                                            echo '<span class="theCheckboxText d-inline-block">'.$checkbox->title.'</span>';
                                            echo '<input class="aCheckbox ml-2" '.$value.' name="theCheckboxes[]" type="checkbox" data-answerID="'.$checkbox->id.'" value="'.$checkbox->title.'" />';
                                            echo '</div>';
                                        endforeach;
                                    endif;
                                endif;
                                ?>   
                                <div id="questionResponse"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer mt-3">
                    <?php
                    echo '<a id="quitQuizz" onclick="quitQuizz();" class="hidden float-left btn btn-danger btn-lg text-white">Quizz beenden</a>';
                    echo '<a id="breakQuizz" onclick="breakQuizz();" class="hidden float-left btn btn-danger btn-lg text-white mr-1">Quizz unterbrechen</a>';
                    echo '<a id="restartQuizz" onclick="restartQuizz();" class="hidden float-left btn btn-warning btn-lg text-white mr-1">Quizz neu starten</a>';
                    echo '<a id="prevQuestion" onclick="prevQuestion();" class="hidden float-left btn btn-info btn-lg text-white">Zurück</a>';
                    echo '<a id="nextQuestion" onclick="nextQuestion();" class="hidden float-right btn btn-success btn-lg text-white">Weiter</a>';
                    echo '<a id="moveNextQuestion" onclick="moveNextQuestion();" class="hidden float-right btn btn-success btn-lg text-white">Weiter</a>';
                    // if the first question in quizz, it could be finished
                    if($this->q == 0):
                        
                    endif;
                    if($this->q >= 1):
                        
                    endif;
                    //if(($this->q + 1) != $this->unit['countSections']):
                        if(($this->unit['type'] == 3 && $this->unit['correctAnswers'] == 2) || $this->unit['type'] == 12 || $this->unit['type'] == 10 || $this->unit['type'] == 11 || $this->unit['type'] == 1 || $this->unit['type'] == 13): 
                            
                        endif;
                    //endif;
                    ?>
                </div>
            </div>
        <?php else: ?>
            <div class="col-12">
                <div class="panel-footer mt-3">
                    <?php
                        echo '<a id="quitQuizz" onclick="quitQuizz();" class="hidden float-left btn btn-danger btn-lg text-white">Quizz beenden</a>';
                        echo '<a onclick="nextQuestion();" class="float-right btn btn-success btn-lg text-white">Starten</a>';
                    ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
<input type="hidden" id="quizzID" value="<?php echo $this->unit['quizzID'];?>" />
<input type="hidden" id="publishedQuizzID" value="<?php echo $this->unit['publishedQuizzID'];?>" />

<input type="hidden" id="clr" value="<?php echo $this->clr;?>" />
<input type="hidden" id="uID" value="<?php echo $this->unit['uID'];?>" />
<input type="hidden" id="q" value="<?php echo $this->q;?>" />
<input type="hidden" id="quc" value="<?php echo $this->quc?>" />
<input type="hidden" id="type" value="<?php echo $this->unit['type'];?>" />
<input type="hidden" id="showResultAfter" value="<?php echo $this->unit['showResultAfter'];?>" />
<input type="hidden" id="allowRestart" value="<?php echo $this->unit['allowRestart'];?>" />
<input type="hidden" id="stageType" value="<?php echo $this->unit['stageType'];?>" />
<input type="hidden" id="questionID" value="<?php echo $this->unit['questionID'];?>" />
<input type="hidden" id="classroomID" value="<?php echo $this->clr;?>" />
<input type="hidden" id="theResultID" value="0" />
<input type="hidden" id="correctAnswers" value="<?php echo $this->unit['correctAnswers'];?>" />
<input type="hidden" id="answered" value="<?php echo $answered;?>" />
<input type="hidden" id="preview" value="<?php echo $this->preview;?>" />
<input type="hidden" id="countSections" value="<?php echo $this->unit['countSections'];?>" />
<input type="hidden" id="mustBeAnswered" value="<?php echo $this->unit['mustBeAnswered'];?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="dialog_mustBeAnswered" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"><i class="fa fa-exclamation-circle text-danger"></i> Frage muss beantwortet werden</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <p>Bitte beantworten Sie die aktuelle Frage, erst dann  können Sie zur nächsten Frage gehen.</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
         </div>
      </div>
   </div>
</div>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('ul.menu.nav').css('display', 'none');
    var q           = $('#q').val();
    var answered    = $('#answered').val();
    var showResultAfter = $('#showResultAfter').val();
    var allowRestart    = $('#allowRestart').val();
    var countSections   = $('#countSections').val();
    if(countSections == 0) {
        $('#nextQuestion').fadeOut(200);
        $('#quitQuizz').fadeIn(200);
    }
    if(q == -1) {
        $('#quitQuizz').fadeIn(200);
    }
    if(q == 0 && allowRestart == 1) {
        $('#restartQuizz').fadeIn(200);
    }
    if(countSections != 0 && q == 0 && showResultAfter == 0) {
        $('#nextQuestion').fadeIn(200);
    }
    if(q > 0) {
        $('#prevQuestion').fadeIn(200);
        if(allowRestart == 1) {
            $('#restartQuizz').fadeIn(200);
        }
        if(showResultAfter == 0) {
            $('#nextQuestion').fadeIn(200);
        }
    }
    if(answered == 1) {
        //$('#moveNextQuestion').fadeIn(200);
        //$('#nextQuestion').fadeOut(0);
    }
});
$('input[type="checkbox"]').on('click', function() {
    var correctAnswers = $('#correctAnswers').val();
    if(correctAnswers < 2) {
        $('input[type="checkbox"]').each(function() {
            $(this).prop('checked', false);
        });
        $(this).prop('checked', true);
    }
})
function hideSave() {
    setTimeout(hideSaveExecute,1500);
}
function hideSaveExecute() {
    $('#save').slideUp(200);
}
function restartQuizz() {
    if(confirm('Möchten sie das Quizz neu starten?') == true) {
        var quizzID     = $('#quizzID').val();
        var publishedQuizzID     = $('#publishedQuizzID').val();
        var stageType   = $('#stageType').val();
        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=stage.restartQuizz",
            data: {publishedQuizzID:publishedQuizzID,stageType:stageType,quizzID:quizzID},
            //async: false,
            success: function( data ) {
                window.location.href = data;
            }
        });
    }
}
function nextQuestion(choosenAnswer) {
    
    if($('#answer' + choosenAnswer).hasClass('closed')) {
        $('#message .modal-body').html('<h4>Sie haben diese Frage bereits beantwortet.</h4>');
        $('#message').modal('show');
        return false;
    }
    
    var quizzID     = $('#quizzID').val();
    var publishedQuizzID     = $('#publishedQuizzID').val();
    var questionID  = $('#questionID').val();
    var stageType   = $('#stageType').val();
    var q           = $('#q').val();
    var quc         = $('#quc').val();
    var clr         = $('#clr').val();
    var uID         = $('#uID').val();
    var preview     = $('#preview').val();
    var answers     = [];
    var indAnswers  = [];
    var options     = [];
    var checkChoosenOptions = 0;
    var checkboxes  = [];
    var checkChoosenChecks = 0;
    var smiley      = [];
    var checkSmiley = 0;
    var type        = $('#type').val();
    var exeptionText    = $('#exeptionText').val();
    var exeptionPoints  = $('#exeptionPoints option:selected').val();
    if(type == 4) {
        $('#save').css('display','block');
        var text = '';
        if(choosenAnswer) {
            if(choosenAnswer == 1) {
                text = 'Sehr schlecht';
            }
            if(choosenAnswer == 2) {
                text = 'Schlecht';
            }
            if(choosenAnswer == 3) {
                text = 'Nicht schlecht';
            }
            if(choosenAnswer == 4) {
                text = 'Gut';
            }
            if(choosenAnswer == 5) {
                text = 'Sehr gut';
            }
            smiley = choosenAnswer + '_' + text;
            checkSmiley = 1;
        } else {
            $('.smileyActive').each(function() {
                var id  = $(this).attr('id');
                id      = id.replace('smiley','');
                if(id == 1) {
                    text    = 'Sehr schlecht';
                }
                if(id == 2) {
                    text    = 'Schlecht';
                }
                if(id == 3) {
                    text    = 'Nicht schlecht';
                }
                if(id == 4) {
                    text    = 'Gut';
                }
                if(id == 5) {
                    text    = 'Sehr gut';
                }
                smiley = id + '_' + text;
            });
        }      
    }
    if(type == 5) {
        $('.choosen').each(function() {
            var id  = $(this).attr('id');
            answers.push('0_' + id);
        });
    }
    if(type == 7 || type == 10) {
        if($('#textFeld').val()) {
            indAnswers.push('0_' + $('#textFeld').val());
        }
    }
    if(type == 11) {
        indAnswers.push('0_' + $('#textArea').val());
    }
    if(type == 12) {
        $.each($('#auswahlliste option:selected'), function() {
            options.push($(this).val() + '_' + $(this).text());
            if($(this).val()) {
                checkChoosenOptions = 1;
            }
        });
    }
    if(type == 13) {
        $('.aCheckbox').each(function() {
            if($(this).prop('checked') == true) {
                checkboxes.push($(this).attr('data-answerID') + '_' + $(this).val());
                if($(this).val()) {
                    checkChoosenChecks++;
                }
            }
        });
    }
    var areThereChoosen = $('.choosen').length;
    if(type == 3 || type == 6) {
        if(choosenAnswer) {
            var answer  = getAnswer(choosenAnswer);
            $('.choosen').each(function() {
                 $(this).css('background-color', 'lightsteelblue').removeClass('choosen');
            });
            if($('#answer' + choosenAnswer).hasClass('choosen')) {
                $('#answer' + choosenAnswer).css('background-color', 'lightsteelblue').removeClass('choosen');
            } else {
                $('#answer' + choosenAnswer).css('background-color', 'forestgreen').addClass('choosen');
            }
            $('.choosen').each(function() {
                answers.push(choosenAnswer + '_' + answer);
            });
        } else {
            if(areThereChoosen > 0) {
                $('.choosen').each(function() {
                    var id      = $(this).attr('id');
                    id          = id.replace('answer', '');
                    var answer  = getAnswer(id);
                    answers.push(id + '_' + answer);
                });
            }
        }
    }
    areThereChoosen = $('.choosen').length;
    if(type == 12 && $('#mustBeAnswered').val() == 1) {
        if(checkChoosenOptions == 0) {
            $('#dialog_mustBeAnswered').modal('show');
            return false;
        }
    }
    if(type == 13 && $('#mustBeAnswered').val() == 1) {
        if(checkChoosenChecks == 0) {
            $('#dialog_mustBeAnswered').modal('show');
            return false;
        }
    }
    if(type != 1 && type != 4 && type != 7 && type != 10 && type != 11 && type != 14  && type != 12 && type != 13 && q != -1 && $('#mustBeAnswered').val() == 1) {
        if(areThereChoosen == 0) {
            $('#dialog_mustBeAnswered').modal('show');
            return false;
        }
    }
    if(type == 7 && q != -1 && $('#mustBeAnswered').val() == 1) {
        console.log(indAnswers.length);
        if(indAnswers.length == 0) {
            $('#dialog_mustBeAnswered').modal('show');
            return false;
        }
    }
    if(type == 10 && q != -1 && $('#mustBeAnswered').val() == 1) {
        if(indAnswers.length == 0) {
            $('#dialog_mustBeAnswered').modal('show');
            return false;
        }
    }
    if(type == 11 && q != -1 && $('#mustBeAnswered').val() == 1) {
        if(indAnswers.length == 0) {
            $('#dialog_mustBeAnswered').modal('show');
            return false;
        }
    }
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=stage.nextQuestion",
        data: {
            stageType:stageType,
            type:type,
            quizzID:quizzID, 
            publishedQuizzID:publishedQuizzID,
            questionID:questionID, 
            answers:answers,
            q:q,
            quc:quc,
            clr:clr,
            uID:uID,
            preview:preview,
            indAnswers:indAnswers,
            options:options,
            checkboxes:checkboxes,
            smiley:smiley,
            exeptionText:exeptionText,
            exeptionPoints:exeptionPoints
        },
        //dataType: 'json',
        success: function( data ) {
            if(type == 4) {
                hideSave();
            }
            var path = data;
            path = path.split('_');
            if(path[0] == 'SRA') {
                $('#questionResponse').html(path[2]);
                $('#theResultID').val(path[1]);
                $('#moveNextQuestion').fadeIn(200);
                // remove the links of the questions
                $('.answer').each(function() {
                    $(this).addClass('closed');
                })
            } else {
                window.location.href = data;
            }
        }
    });
}
function checkQuestion(choosenAnswer) {
    
    if($('#answer' + choosenAnswer).hasClass('closed')) {
        $('#message .modal-body').html('<h4>Sie haben diese Frage bereits beantwortet.</h4>');
        $('#message').modal('show');
        return false;
    }
    
    var quizzID     = $('#quizzID').val();
    var publishedQuizzID     = $('#publishedQuizzID').val();
    var questionID  = $('#questionID').val();
    var stageType   = $('#stageType').val();
    var q           = $('#q').val();
    var clr         = $('#clr').val();
    var preview     = $('#preview').val();
    var answers     = [];
    var indAnswers  = [];
    var options     = [];
    var checkboxes  = [];
    var smiley      = [];
    var type        = $('#type').val();
    var exeptionText    = $('#exeptionText').val();
    var exeptionPoints  = $('#exeptionPoints option:selected').val();
    if(type == 4) {
        var text = '';
        if(choosenAnswer) {
            if(choosenAnswer == 1) {
                text = 'Sehr schlecht';
            }
            if(choosenAnswer == 2) {
                text = 'Schlecht';
            }
            if(choosenAnswer == 3) {
                text = 'Nicht schlecht';
            }
            if(choosenAnswer == 4) {
                text = 'Gut';
            }
            if(choosenAnswer == 5) {
                text = 'Sehr gut';
            }
            smiley = choosenAnswer + '_' + text;
        } else {
            $('.smileyActive').each(function() {
                var id  = $(this).attr('id');
                id      = id.replace('smiley','');
                if(id == 1) {
                    text    = 'Sehr schlecht';
                }
                if(id == 2) {
                    text    = 'Schlecht';
                }
                if(id == 3) {
                    text    = 'Nicht schlecht';
                }
                if(id == 4) {
                    text    = 'Gut';
                }
                if(id == 5) {
                    text    = 'Sehr gut';
                }
                smiley = id + '_' + text;
            });
        }      
    }
    if(type == 5) {
        $('.choosen').each(function() {
            var id  = $(this).attr('id');
            answers.push('0_' + id);
        });
    }
    if(type == 10) {
        indAnswers.push('0_' + $('#textFeld').val());
    }
    if(type == 11) {
        indAnswers.push('0_' + $('#textArea').val());
    }
    if(type == 12) {
        $.each($('#auswahlliste option:selected'), function() {
            options.push($(this).val() + '_' + $(this).text());
        });
    }
    if(type == 13) {
        $('.aCheckbox').each(function() {
            if($(this).prop('checked') == true) {
                checkboxes.push($(this).attr('data-answerID') + '_' + $(this).val());
            }
        });
    }
    var areThereChoosen = $('.choosen').length;
    if(type == 3 || type == 6) {
        if(choosenAnswer) {
            var answer  = getAnswer(choosenAnswer);
            $('.choosen').each(function() {
                 $(this).css('background-color', 'lightsteelblue').removeClass('choosen');
            });
            if($('#answer' + choosenAnswer).hasClass('choosen')) {
                $('#answer' + choosenAnswer).css('background-color', 'lightsteelblue').removeClass('choosen');
            } else {
                $('#answer' + choosenAnswer).css('background-color', 'forestgreen').addClass('choosen');
            }
            $('.choosen').each(function() {
                answers.push(choosenAnswer + '_' + answer);
            });
        } else {
            if(areThereChoosen > 0) {
                $('.choosen').each(function() {
                    var id      = $(this).attr('id');
                    id          = id.replace('answer', '');
                    var answer  = getAnswer(id);
                    answers.push(id + '_' + answer);
                });
            }
        }
    }
    areThereChoosen = $('.choosen').length;
    if(q != -1 && $('#mustBeAnswered').val() == 1) {
        if(areThereChoosen == 0) {
            alert('Die Frage muss beantwortet werden, bevor Sie die nächste Frage laden können.');
            return false;
        }
    }
    /*jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=stage.nextQuestion",
        data: {
            stageType:stageType,
            type:type,
            quizzID:quizzID, 
            publishedQuizzID:publishedQuizzID,
            questionID:questionID, 
            answers:answers,
            q:q,
            clr:clr,
            preview:preview,
            indAnswers:indAnswers,
            options:options,
            checkboxes:checkboxes,
            smiley:smiley,
            exeptionText:exeptionText,
            exeptionPoints:exeptionPoints
        },
        //dataType: 'json',
        success: function( data ) {
            var path = data;
            path = path.split('_');
            if(path[0] == 'SRA') {
                $('#questionResponse').html(path[2]);
                $('#theResultID').val(path[1]);
                $('#moveNextQuestion').fadeIn(200);
                // remove the links of the questions
                $('.answer').each(function() {
                    $(this).addClass('closed');
                })
            } else {
                window.location.href = data;
            }
        }
    });*/
}
function prevQuestion() {
    var quizzID     = $('#quizzID').val();
    var publishedQuizzID     = $('#publishedQuizzID').val();
    var q           = $('#q').val();
    var theResultID = $('#theResultID').val();
    var stageType   = $('#stageType').val();
    var preview     = $('#preview').val();
    var clr         = $('#clr').val();
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=stage.movePrevQuestion",
        data: { 
            publishedQuizzID:publishedQuizzID,
            q:q,
            theResultID:theResultID,
            quizzID:quizzID,
            stageType:stageType,
            clr:clr,
            preview:preview
        },  
        //dataType: 'json',
        success: function( data ) {
            window.location.href = data;
        }
    });
}
function moveNextQuestion() {
    var quizzID     = $('#quizzID').val();
    var publishedQuizzID     = $('#publishedQuizzID').val();
    var q           = $('#q').val();
    var clr         = $('#clr').val();
    var theResultID = $('#theResultID').val();
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=stage.moveNextQuestion",
        data: { 
            publishedQuizzID:publishedQuizzID,
            q:q,
            clr:clr,
            theResultID:theResultID,
            quizzID:quizzID
        },
        //dataType: 'json',
        success: function( data ) {
            window.location.href = data;
        }
    });
}
function getAnswer(id) {
    var answer = '';
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=stage.getAnswerText",
        data: {id:id},
        async: false,
        success: function( data ) {
            answer = data;
        }
    });
    return answer;
}
function quitQuizz() {
    if(confirm('Sind Sie sicher, dass Sie das Quizz beenden möchten?') == true) {
        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=stage.quitQuizz",
            //data: {id:id},
            //async: false,
            success: function( data ) {
                alert("Das Quizz wurde abgeschlossen. Bitte schließen Sie das Browserfenster.")
            }
        });
    }
}
function chooseAnswer(choosenAnswer, multi) {
    if(multi == 0) {
        if($('#answer' + choosenAnswer).hasClass('choosen')) {
            $('#answer' + choosenAnswer).css('background-color', 'lightsteelblue').removeClass('choosen');
        } else {
            $('#answer' + choosenAnswer).css('background-color', 'forestgreen').addClass('choosen');
        }
    }
    if(multi == 1) {
        $('.answer').each(function() {
            $(this).css('background-color', '#e1e1e1').removeClass('choosen');
        });
        $('#answer' + choosenAnswer).css('background-color', 'forestgreen').addClass('choosen');
    }
}
function auditAnswer(choosenAnswer) {
    $('.answer').each(function() {
        $(this).css('background-color', '#e1e1e1').removeClass('choosen');
    });
    if(choosenAnswer == 'AA') {
        $('#answer' + choosenAnswer).css('background-color', '#ffc107').addClass('choosen');
        $('#exeption').fadeIn(200);
    } else {
        $('#exeption').fadeOut(200).val('');
    }
    if(choosenAnswer == 'A') {
        $('#answer' + choosenAnswer).css('background-color', 'forestgreen').addClass('choosen');
    }
    if(choosenAnswer == 'N') {
        $('#answer' + choosenAnswer).css('background-color', '#dc3545').addClass('choosen');
    }
}
function JNAnswer(choosenAnswer) {
    $('.answer').each(function() {
        $(this).css('background-color', '#e1e1e1').removeClass('choosen');
    });
    if(choosenAnswer == '1') {
        $('#answerJ').css('background-color', 'forestgreen').addClass('choosen');
    } 
    if(choosenAnswer == '0') {
        $('#answerN').css('background-color', 'indianred').addClass('choosen');
    }
}
</script>