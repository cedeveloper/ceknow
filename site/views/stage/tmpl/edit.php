<?php
/**
 * @author		datsDORTMUND
 * @copyright	(c) 2016 datsDORTMUND. All rights reserved.
 * @product		datsAUDITUM		
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.tooltip');
?>
<style>
   .sroll {
      overflow-x: scroll;
   }
</style>
<form action="<?php echo JRoute::_('index.php?option=com_auditum&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
   <div class="actionboard mt-3 mb-3">
      <a class="btn btn-danger" href="index.php?option=com_auditum&view=quizzes"><?php echo JText::_('Zurück') ?></a>
   </div>
   <div class="quizz-content">
      <h2><?php echo $this->item->name;?></h2>
      <p><?php echo $this->item->description;?></p>
      <div class="card bg bg-light p-1">
         <?php echo $this->item->text;?>
      </div>
      <div id="result" class="card bg bg-light p-1 mt-2">
         <h3>Ihr Endergebnis</h3>
         <div class="result-inner">
            <p>Bitte beantworten Sie die Fragen im Text.</p>
         </div>
      </div>
   </div>
<input type="hidden" id="quizzTyp" value="<?php echo $this->item->type;?>" />
<input type="hidden" id="countQue" value="<?php echo $this->item->que;?>" />
<input type="hidden" id="quizzID" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   jQuery(function ($) {
      var beginDrag = 0;
      var endDrag = 0;
      $('.drop').each(function() {
         $(this).html('');
      });
      $( ".drags .drag" ).draggable({
         snap: ".drop, .draghome",
         start: function() {
            beginDrag = 1;
            console.log(beginDrag);
         },
         drag: function() {

         },
         stop: function(event, ui) {
            if(beginDrag == 1) {
               var draghome = $('.draghome.' + $(this).attr('id'));
               var position = draghome.position();
               console.log(position);
               $(this).animate( {
                  top: position.top,
                  left: position.left
               }, 500, function() {

               });
               console.log('Kein Draghome');
            } else {
               console.log(beginDrag);
            }
         }
      });
      var t2Choices = new Array();
      $( ".qtext .drop" ).droppable({
         drop: function( event, ui ) {
            beginDrag = 0;
            t2Choices.push($(this).attr('id') + '_' + ui.draggable.attr('id'));
         }
      });
       $( ".draghome" ).droppable({
         drop: function( event, ui ) {
            var remove = ui.draggable.attr('id');
            $(t2Choices).each(function(e, value) {
               var part = value.split('_');
               if(part[1] == remove) {
                   t2Choices.splice(e, 1);
               }
            });
            console.log(t2Choices);
         }
      });
      $('.drags .drag').each(function() {
         var classes = $(this).attr('class');
         var drag    = $(this);
         classes = classes.split(" ");
         $(classes).each(function(e, value) {
            choice = value.substring(0,6);
            if(choice == 'choice') {
               position = $('.draghome.' + value).position();
               drag.css('top', position.top).css('left', position.left);
            }
         })
      });
      $('#reset').on('click', function() {
         location.reload();
      });
      $('#check').on('click', function() {
         var quizzID = $('#quizzID').val();
         if($('#quizzTyp').val() == 1) {
            var answers = new Array();
            $('.custom-select').each(function() {
               if($(this).val()) {
                  answers.push($(this).val());
               }
            })
            if(answers.length != $('#countQue').val()) {
               alert("Bitte beantworten Sie zunächst alle Fragen");
               return false;
            }
            jQuery.ajax({
               type: "POST",
               url: "index.php?option=com_auditum&task=quizz.checkQuizz",
               data: {quizzID:quizzID, answers:answers},
               dataType: 'json',
               success: function( data ) {
                  $('#result .result-inner').empty();
                  var hs      = 0;
                  $(data).each(function(e, value) {
                     var antwort = '';
                     var color   = '';
                     
                     if(value.result == 0) {
                        antwort  = 'Leider Falsch';
                        color    = 'text-danger';
                     }
                     if(value.result == 1) {
                        antwort  = 'Richtig';
                        color    = 'text-success';
                        hs++;
                     }
                     
                     $('#result .result-inner').append(
                        '<p class="mt-1 mb-1 ' + color + '"><b>Frage ' + value.antwort + ': ' + antwort + '</b></p>'
                     );
                     
                  });
                  hs = (hs * 100) / $('#countQue').val();
                  hs = Math.ceil(hs);
                  $('#result .result-inner').append(
                     '<h2>Highscore: ' + hs + '%</h2>'
                  );
                  $('#check').addClass('disabled');
               }
            });
         }
         if($('#quizzTyp').val() == 2) {
            if(t2Choices.length != $('#countQue').val()) {
               alert("Bitte beantworten Sie zunächst alle Fragen");
               return false;
            }
            jQuery.ajax({
               type: "POST",
               url: "index.php?option=com_auditum&task=quizz.checkQuizz2",
               data: {quizzID:quizzID, t2Choices:t2Choices},
               dataType: 'json',
               success: function( data ) {
                  $('#result .result-inner').empty();
                  var hs      = 0;
                  $(data).each(function(e, value) {
                     var antwort = '';
                     var color   = '';
                     
                     if(value.result == 0) {
                        antwort  = 'Leider Falsch';
                        color    = 'text-danger';
                     }
                     if(value.result == 1) {
                        antwort  = 'Richtig';
                        color    = 'text-success';
                        hs++;
                     }
                     
                     $('#result .result-inner').append(
                        '<p class="mt-1 mb-1 ' + color + '"><b>Frage ' + value.antwort + ': ' + antwort + '</b></p>'
                     );
                     
                  });
                  hs = (hs * 100) / $('#countQue').val();
                  hs = Math.ceil(hs);
                  $('#result .result-inner').append(
                     '<h2>Highscore: ' + hs + '%</h2>'
                  );
                  t2Choices = new Array();
                  $('#check').addClass('disabled');
               }
            });
         }
      });
   });
</script>