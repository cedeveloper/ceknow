<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
 
/**
 * Kunde item view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewStage extends JViewLegacy
{
	protected $unit;
	protected $result;
	
	public function display($tpl = null)
	{
		$app 		= JFactory::getApplication();
		$input 		= JFactory::getApplication()->input;
		$uID 		= $input->get('uID', 0, 'INT');
		$unitID 	= $input->get('unitID', 0, 'INT');
		$session 	= JFactory::getSession();
		$cart 		= $session->get('quizzes');
		$stageType 	= $input->get('sT','','STR');
		$preview 	= $input->get('preview','','INT');
		$clr 		= $input->get('clr','','INT');
		$user 		= JFactory::getUser();
		$this->preview = $preview;
		// check which type of staging is asked for
		switch($stageType) {
			case 'quizz':
				$this->unit = $this->get('QuizzFromStageing');
				$session->set('quizztype', 'quizz');
				break;
			case 'lr':
				if(!$user->id):
					$app->enqueueMessage('Dieses Quizz steht Ihnen erst nach der Anmeldung zur Verfügung.', 'error');
					$app->redirect(JURI::Root());
				else:
					JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
					$result = JTable::getInstance('Theresults','JclassroomTable',array());
					$load 	= array(
						'classroomID' 	=> $clr,
						'quizzID' 		=> $unitID,
						'unitID' 		=> $uID,
						'created_by' 	=> $user->id,
						'preview' 		=> 0
					);
					$check = $result->load($load);
					if($check):
						$app->enqueueMessage('Sie haben dieses Quizz bereits durchgeführt.', 'error');
						$app->redirect(JURI::Root().'stage-classroom?layout=edit&id='.$clr);
					endif;
					$this->unit = $this->get('Unit');
					$session->set('quizztype', 'lr');
					$session->set('uID', $this->unit['uID']);
					$session->set('classroomID', $input->get('clr', 0, 'INT'));
				endif;
				break;
		}
		//echo 'UID: '.$session->get('uID');
		$document = JFactory::getDocument();
		$document->setTitle($this->unit['title'] .' [ceKnow]');
		$q			= $input->get('q',0,'INT');
		//If not page 1 and no entrys in session, move to page 1
		if($q != -1 && !$cart):
			//$app->enqueueMessage('Das Quizz wurde neu gestartet', 'error');
			//$app->redirect(JURI::Root().'stage?sT=quizz&qID='.$this->unit['publishedQuizzID'].'&q=-1');
		endif;
		if($q == -1):
			
		endif;

		$this->clr 					= $input->get('clr',0,'INT');
		$this->q 					= $input->get('q',0,'INT');
		$this->showQ 				= $this->q - $this->unit['countNoSections'];
		$ordering 					= $this->unit['ordering'];
		$noB 						=  $this->unit['noBefore'];
		$this->quc 					= ($ordering + 1) - $noB;
		$user 	= JFactory::getUser();
		$input 	= JFactory::getApplication()->input;
		$layout = $input->get('lid','','INT');
		switch($layout) {
			case 1:
				$layout = 'audit-erstellen';
				break;
			case 2:
				$layout = 'katalog';
				break;
			case 3:
				$layout = 'auswertung';
				break;
			case 4:
				$layout = 'vorwort';
				break;
			case 6:
				$layout = 'gueltigkeit';
				break;
			case 7:
				$layout = 'scripts';
				break;
			case 9:
				$layout = 'ende';
				break;
			case 10:
				$this->result 				= $this->get('Result');
				$theResultID = $input->get('rID', 0, 'INT');
				$this->theResultID = $theResultID;
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$result 		= JTable::getInstance('Theresults','JclassroomTable',array());
				$result->load($theResultID);
				$this->quizzID = $session->get('quizzID');
				//$publishedQuizzID = $input->get('pqid',0,'INT');
				if($result->publishedQuizzID):
					$this->publishedQuizzID = $result->publishedQuizzID;
				else:
					$this->publishedQuizzID = 0;
				endif;
				$layout = 'result';
				$document = JFactory::getDocument();
				$document->setTitle('Ihr Ergebniss [ceKnow]');
				break;
		}
		$this->setLayout($layout);
		
		parent::display($tpl);
	}
}
?>