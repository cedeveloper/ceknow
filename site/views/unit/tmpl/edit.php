<?php
/**
 * @author		datsDORTMUND
 * @copyright	(c) 2016 datsDORTMUND. All rights reserved.
 * @product		datsAUDITUM		
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addStyleSheet('https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.css');
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/imgForJCE.js');
$doc->addScript('https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.js');
$user    = JFactory::getUser();
?>
<style>
   .sroll {
      overflow-x: scroll;
   }
</style>
<form action="" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
   <h2><b><?php echo 'QID'.str_pad($this->item->id,8,'0',STR_PAD_LEFT);?></b><h2>
   <h1 class="jclassroomHeader">Quizz: <i><?php echo $this->item->title;?></i></h1>
   <div class="actionboard mt-3 mb-5">
      <button type="button" class="btn btn-primary" onclick="Joomla.submitform('unit.apply')"><?php echo JText::_('JAPPLY') ?></button>
      <button type="button" class="btn btn-secondary" onclick="Joomla.submitform('unit.save')"><?php echo JText::_('Speichern & schließen') ?></button>
      <?php if($this->usergroup == 'superuser'): ?>
         <button type="button" class="btn btn-warning text-white" onclick="saveAsTemplate();">Als Vorlage speichern</button>
      <?php endif; ?>
      <a class="float-right btn btn-danger" href="quizze"><?php echo JText::_('Zurück') ?></a>
   </div>
   <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
         <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Basisdaten</a>
      </li>
      <?php //if($this->usergroup != 'superuser'): ?>
      <li class="nav-item">
         <a class="nav-link" id="communication-tab" data-toggle="tab" href="#communication" role="tab" aria-controls="asp" aria-selected="false">Freigabe</a>
      </li>
      <?php //endif; ?>
   </ul>
   <div class="tab-content" id="tabContent">
      <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
         <div class="form-horizontal mt-3">
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('id'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('id'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('title'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('title'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('description'); ?>
               </div>
               <div data-editor="jform_description" class="use_editor col-12 col-sm-10">
                  <?php
                  if($this->disabled == 1):
                     echo '<div class="card bg-light p-1">'.$this->item->description.'</div>';
                     echo '<small>Diese Beschreibung können Sie nicht ändern, da sie von ceKnow als Standardinhalt fest vorgegeben ist.</small>';
                  else:   
                     echo $this->form->getInput('description'); 
                     require_once(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');
                  endif;
                  ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('type'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('type'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('mustBeAnswered'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('mustBeAnswered'); ?>
               </div>
            </div>
            <!--<div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php //echo $this->form->getLabel('sendToMail'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php //echo $this->form->getInput('sendToMail'); ?>
               </div>
            </div>-->
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('bgColor'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('bgColor'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('calculate'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('calculate'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('created_by'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('created_by'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('published'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('published'); ?>
               </div>
            </div>
         </div>
      </div>
      <?php //if($this->usergroup != 'superuser'): ?>
      <div class="tab-pane fade show" id="communication" role="tabpanel" aria-labelledby="communication-tab">
         <p class="mt-3"><i class="fa fa-question"></i> Legen Sie fest, wer dieses Quizz sehen und bearbeiten darf</p>
         <div class="mb-3 col-12 col-sm-6 card bg-light p-1">
            <p><b>Globale Freigaben</b></p>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('showto'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('showto'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('editto'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php echo $this->form->getInput('editto'); ?>
               </div>
            </div>
            <!--<div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php //echo $this->form->getLabel('choosableto'); ?>
               </div>
               <div class="col-12 col-sm-6">
                  <?php //echo $this->form->getInput('choosableto'); ?>
               </div>
            </div>-->
         </div>
         <?php
         $displayTable = 'display: none;';
         if($this->item->showto == 0 && $this->item->editto == 0):
            $displayTable = '';
         endif;
         ?>
         <table id="setRights" style="<?php echo $displayTable;?>" class="table table-striped table-sm">
            <?php if($this->usergroup == 'superuser'): ?>
            <thead>
               <tr style="background-color: lightsalmon;">
                  <th>Systemadministratoren</th>
                  <th style="text-align: center;">
                     <a onclick="allSysadminRead();" class="btn btn-primary text-white btn-sm">Alle  Systemadmin</a>
                     <a onclick="noSysadminRead();" class="btn btn-danger text-white btn-sm">Kein Systemadmin</a>
                  </th>
                  <th style="text-align: center;">
                     <a onclick="allSysadminWrite();" class="btn btn-primary text-white btn-sm">Alle Systemadmin</a>
                     <a onclick="noSysadminWrite();" class="btn btn-danger text-white btn-sm">Kein Systemadmin</a>
                  </th>
               </th>
               <tr>
                  <th style="width: 50%;">Name</th>
                  <th style="width: 25%;text-align: center;">Ansehen</th>
                  <th style="width: 25%;text-align: center;">Bearbeiten</th>
               </tr>
            </thead>
            <tbody>
               <?php
               if($this->item->systemadmins):
                  foreach($this->item->systemadmins as $sysadmin):
                     echo '<tr>';
                     echo '<td>'.$sysadmin->name.'</td>';
                     $checkedR = '';
                     if($this->item->readRights):
                        $reads = explode(',', $this->item->readRights);
                        if(in_array($sysadmin->id, $reads)):
                           $checkedR = 'checked';
                        endif;
                     endif;
                     $checkedW = '';
                     if($this->item->writeRights):
                        $writes = explode(',', $this->item->writeRights);
                        if(in_array($sysadmin->id, $writes)):
                           $checkedW = 'checked';
                        endif;
                     endif;
                     echo '<td style="text-align: center;"><input type="checkbox" class="sysadminRead" data-userid="'.$sysadmin->id.'" data-type="read" '.$checkedR.' /></td>';
                     echo '<td style="text-align: center;"><input type="checkbox" class="sysadminWrite" data-userid="'.$sysadmin->id.'" data-type="write" '.$checkedW.' /></td>';
                     echo '</tr>';
                  endforeach;
               endif;
               ?>
            </tbody>
            <?php endif;?>
            <thead>
               <tr style="background-color: palegreen;">
                  <th>Kundenadministratoren</th>
                  <th style="text-align: center;">
                     <a onclick="allCusadminRead();" class="btn btn-primary text-white btn-sm">Alle Kundenadmin</a>
                     <a onclick="noCusadminRead();" class="btn btn-danger text-white btn-sm">Kein Kundenadmin</a>
                  </th>
                  <th style="text-align: center;">
                     <a onclick="allCusadminWrite();" class="btn btn-primary text-white btn-sm">Alle Kundenadmin</a>
                     <a onclick="noCusadminWrite();" class="btn btn-danger text-white btn-sm">Kein Kundenadmin</a>
                  </th>
               </th>
               <tr>
                  <th>Name</th>
                  <th style="text-align: center;">Ansehen</th>
                  <th style="text-align: center;">Bearbeiten</th>
               </tr>
            </thead>
            <tbody>
               <?php
               if($this->item->customeradmins):
                  foreach($this->item->customeradmins as $cusadmin):
                     echo '<tr>';
                     echo '<td>'.$cusadmin->name.' <b>( '.$cusadmin->company_name.' )</b></td>';
                     $checkedR = '';
                     if($this->item->readRights):
                        $reads = explode(',', $this->item->readRights);
                        if(in_array($cusadmin->id, $reads)):
                           $checkedR = 'checked';
                        endif;
                     endif;
                     $checkedW = '';
                     if($this->item->writeRights):
                        $writes = explode(',', $this->item->writeRights);
                        if(in_array($cusadmin->id, $writes)):
                           $checkedW = 'checked';
                        endif;
                     endif;
                     echo '<td style="text-align: center;"><input id="cusadminRead'.$cusadmin->id.'" type="checkbox" class="cusadminRead" data-userid="'.$cusadmin->id.'" data-type="read"'.$checkedR .' /></td>';
                     echo '<td style="text-align: center;"><input id="cusadminWrite'.$cusadmin->id.'" type="checkbox" class="cusadminWrite" data-userid="'.$cusadmin->id.'" data-type="write"'.$checkedW. '/></td>';
                     echo '</tr>';
                  endforeach;
               endif;
               ?>
            </tbody>
            <thead>
               <tr style="background-color: lightsteelblue;">
                  <th>Trainer</th>
                  <th style="text-align: center;">
                     <a onclick="allTrainerRead();" class="btn btn-primary text-white btn-sm">Alle Trainer</a>
                     <a onclick="noTrainerRead();" class="btn btn-danger text-white btn-sm">Kein Trainer</a>
                  </th>
                  <th style="text-align: center;">
                     <a onclick="allTrainerWrite();" class="btn btn-primary text-white btn-sm">Alle Trainer</a>
                     <a onclick="noTrainerWrite();" class="btn btn-danger text-white btn-sm">Kein Trainer</a>
                  </th>
               </th>
               <tr>
                  <th>Name</th>
                  <th style="text-align: center;">Ansehen</th>
                  <th style="text-align: center;">Bearbeiten</th>
               </tr>
            </thead>
            <tbody>
               <?php
               if($this->item->trainer):
                  foreach($this->item->trainer as $trainer):
                     echo '<tr>';
                     echo '<td>'.$trainer->name.' <b>( '.$trainer->company_name.' )</b></td>';
                     $checkedR = '';
                     if($this->item->readRights):
                        $reads = explode(',', $this->item->readRights);
                        if(in_array($trainer->id, $reads)):
                           $checkedR = 'checked';
                        endif;
                     endif;
                     $checkedW = '';
                     if($this->item->writeRights):
                        $writes = explode(',', $this->item->writeRights);
                        if(in_array($trainer->id, $writes)):
                           $checkedW = 'checked';
                        endif;
                     endif;
                     echo '<td style="text-align: center;"><input id="trainerRead' . $trainer->id.'" type="checkbox" class="trainerRead" data-userid="'.$trainer->id.'" data-type="read" '.$checkedR. '/></td>';
                     echo '<td style="text-align: center;"><input id="trainerWrite' . $trainer->id.'" type="checkbox" class="trainerWrite" data-userid="'.$trainer->id.'" data-type="write" '.$checkedW .'/></td>';
                     echo '</tr>';
                  endforeach;
               endif;
               ?>
            </tbody>
         </table>
      </div>
      <?php //endif;?>
   </div>
<input type="hidden" id="quizzTyp" value="<?php echo $this->item->type;?>" />
<input type="hidden" id="countQue" value="<?php echo $this->item->que;?>" />
<input type="hidden" id="quizzID" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="disabled" value="<?php echo $this->disabled;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<?php
require_once(JPATH_COMPONENT.'/views/dialogs/imageContent.php');
require_once(JPATH_COMPONENT.'/views/dialogs/formForJCE.php');
?>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<div id="theDialog" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-exclamation-circle text-danger"></i> Hinweis</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="dialogMessage"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
let unitID = $('#quizzID').val();
let classroomID = false;
let part = 'quizz';
   $(document).ready(function() {
      var disabled = $('#disabled').val();
      if(disabled == 1) {
         $('.form-horizontal input').each(function() {
            $(this).attr('readonly',true);
         });
         $('.form-horizontal select').each(function() {
            $(this).attr('disabled',true);
         });
      }
      calcRights();
   });
   $('.colorpicker').spectrum({

   });
   var form = $("#adminForm");
      $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[calculate]': {
            required: true
         },
         'jform[type]': {
            required: true
         }
         
      },
      messages: {
         'jform[calculate]': 'Bitte wählen Sie aus, ob dieses Quizz bewertet werden soll.',
         'jform[type]': 'Bitte wählen einen Typen für dieses Quizz aus.'
      },
      submitHandler: function(form) {
         
         form.submit();
      }
   });
   function calcRights() {
      var showto = $('#jform_showto option:selected').val();
      var editto = $('#jform_editto option:selected').val();
      if(showto == 1 && editto != 1) {
         $('.cwrite').attr('disabled', 'disabled').addClass('disabled');
         $('#setRights').fadeOut(200);
         $('#dialogMessage').html('Wenn Sie als Recht im Feld <i>Ansehen</i> <b>Nur Sie</b> gewählt haben, muss als Recht im Feld <i>Bearbeiten</i> ebenfalls <b>Nur Sie</b> ausgewählt sein.');
         $('#theDialog').modal('show');
         $('#jform_editto').val('1');
         jQuery('#jform_editto').select2({width: '100%'});
         return false;
      }
      if(showto == 0 || editto == 0) {
         $('#setRights').fadeIn(200);
         if(editto != 0) {
            $('.cusadminWrite').attr('disabled', 'disabled').addClass('disabled');
            $('.trainerWrite').attr('disabled', 'disabled').addClass('disabled');
         } else {
            $('.cusadminWrite').removeAttr('disabled').removeClass('disabled');
            $('.trainerWrite').removeAttr('disabled').removeClass('disabled');
         }
      } 
      if(showto != 0 && editto != 0) {
         $('#setRights').fadeOut(200);
      }
   }
   $('.cusadminWrite').on('change', function() {
      var dataS = $(this).attr('data-userid');
      var dataT = $(this).attr('data-type'); 
      if($('#cusadminWrite' + dataS).is(':checked')) {
         if(!$('#cusadminRead' + dataS).is(':checked')) {
            $('#cusadminRead' + dataS).prop('checked', true);
            setActive(dataS,'read');
         }
      }
   });
   $('.cusadminRead').on('change', function() {
      var dataS = $(this).attr('data-userid');
      var dataT = $(this).attr('data-type'); 
      if(!$('#cusadminRead' + dataS).is(':checked')) {
         $('#cusadminWrite' + dataS).prop('checked', false);
         setInactive(dataS,'write');
      }
   });
   $('.trainerWrite').on('change', function() {
      var dataS = $(this).attr('data-userid');
      var dataT = $(this).attr('data-type'); 
      if($('#trainerWrite' + dataS).is(':checked')) {
         if(!$('#trainerRead' + dataS).is(':checked')) {
            $('#trainerRead' + dataS).prop('checked', true);
            setActive(dataS,'read');
         }
      }
   });
   $('.trainerRead').on('change', function() {
      var dataS = $(this).attr('data-userid');
      var dataT = $(this).attr('data-type'); 
      if(!$('#trainerRead' + dataS).is(':checked')) {
         $('#trainerWrite' + dataS).prop('checked', false);
         setInactive(dataS,'write');
      }
   });
   function saveAsTemplate() {
      if(confirm('Soll dieses Quizz als Vorlage in die Bibliothek gespeichert werden?') == true) {
         Joomla.submitform('unit.library');
      }
   }
   function allSysadminRead() {
      $('.sysadminRead').each(function() {
         $(this).prop('checked', true); 
         var dataS = $(this).attr('data-userid');
         var dataT = $(this).attr('data-type'); 
         setActive(dataS, dataT);
      });
   }
   function noSysadminRead() {
      $('.sysadminRead').each(function() {
         $(this).prop('checked', false); 
         var dataS = $(this).attr('data-userid');
         var dataT = $(this).attr('data-type'); 
         setInactive(dataS, dataT);
      });
   }
   function allSysadminWrite() {
      $('.sysadminWrite').each(function() {
         $(this).prop('checked', true); 
         var dataS = $(this).attr('data-userid');
         var dataT = $(this).attr('data-type'); 
         setActive(dataS, dataT);
      });
   }
   function noSysadminWrite() {
      $('.sysadminWrite').each(function() {
         $(this).prop('checked', false); 
         var dataS = $(this).attr('data-userid');
         var dataT = $(this).attr('data-type'); 
         setInactive(dataS, dataT);
      });
   }
   function allCusadminRead() {
      $('.cusadminRead').each(function() {
         $(this).prop('checked', true); 
         var dataS = $(this).attr('data-userid');
         var dataT = $(this).attr('data-type'); 
         setActive(dataS, dataT);
      });
   }
   function noCusadminRead() {
      $('.cusadminRead').each(function() {
         if($(this).is(':checked')) {
            $(this).prop('checked', false);
            var dataS = $(this).attr('data-userid');
            var dataT = $(this).attr('data-type'); 
            setInactive(dataS, dataT);
            setAutomaticInactive(dataS, 'write','customer');
         }
      });
   }
   function allCusadminWrite() {
      $('.cusadminWrite').each(function() {
         if(!$(this).is(':checked')) {
            $(this).prop('checked', true);          
            var dataS = $(this).attr('data-userid');
            var dataT = $(this).attr('data-type'); 
            setActive(dataS, dataT);
            setAutomaticActive(dataS, 'read','customer');
         }
      });
   }
   function noCusadminWrite() {
      $('.cusadminWrite').each(function() {
         $(this).prop('checked', false);
         var dataS = $(this).attr('data-userid');
         var dataT = $(this).attr('data-type'); 
         setInactive(dataS, dataT);
      });
   }
   function allTrainerRead() {
      $('.trainerRead').each(function() {
         $(this).prop('checked', true); 
         var dataS = $(this).attr('data-userid');
         var dataT = $(this).attr('data-type'); 
         setActive(dataS, dataT);
      });
   }
   function noTrainerRead() {
      $('.trainerRead').each(function() {
         if($(this).is(':checked')) {
            $(this).prop('checked', false); 
            var dataS = $(this).attr('data-userid');
            var dataT = $(this).attr('data-type'); 
            setInactive(dataS, dataT);
            setAutomaticInactive(dataS, 'write');
         }
      });
   }
   function allTrainerWrite() {
      $('.trainerWrite').each(function() {
         if(!$(this).is(':checked')) {
            $(this).prop('checked', true); 
            var dataS = $(this).attr('data-userid');
            var dataT = $(this).attr('data-type'); 
            setActive(dataS, dataT);
            setAutomaticActive(dataS, 'read','trainer');
         }
      });
   }
   function noTrainerWrite() {
      $('.trainerWrite').each(function() {
         $(this).prop('checked', false); 
         var dataS = $(this).attr('data-userid');
         var dataT = $(this).attr('data-type'); 
         setInactive(dataS, dataT);
      });
   }
   $('input[type="checkbox"]').on('click', function() {
      var dataS = $(this).attr('data-userid');
      var dataT = $(this).attr('data-type');
      if($(this).is(':checked')) {
         setActive(dataS, dataT);
      } else {
         setInactive(dataS, dataT);
      }
   });
   function setActive(userID, type) {
      $('#save').modal('show');
      var quizzID    = $('#quizzID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=unit.setRights",
         data: {quizzID:quizzID, userID:userID, type:type},
         //dataType: 'json',
         async: false,
         success: function( data ) {
            hideSave();
         }
      });
   }
   function setAutomaticActive(userID, type, section) {
      $('#save').modal('show');
      var quizzID    = $('#quizzID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=unit.setRights",
         data: {quizzID:quizzID, userID:userID, type:type},
         //dataType: 'json',
         async: false,
         success: function( data ) {
            if(section == 'customer' && type == 'read') {
               $('#cusadminRead' + userID).prop('checked', true); 
            }
            if(section == 'trainer' && type == 'read') {
               $('#trainerRead' + userID).prop('checked', true); 
            }
            hideSave();
         }
      });
   }
   function setInactive(userID, type) {
      $('#save').modal('show');
      var quizzID    = $('#quizzID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=unit.setNoRights",
         data: {quizzID:quizzID, userID:userID, type:type},
         //dataType: 'json',
         async: false,
         success: function( data ) {
            hideSave();
         }
      });
   }
   function setAutomaticInactive(userID, type) {
      $('#save').modal('show');
      var quizzID    = $('#quizzID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=unit.setNoRights",
         data: {quizzID:quizzID, userID:userID, type:type},
         //dataType: 'json',
         async: false,
         success: function( data ) {
            if(type == 'write') {
               $('#cusadminWrite' + userID).prop('checked', false); 
            }
            hideSave();
         }
      });
   }
   function hideSave() {
      setTimeout(hideSaveExecute,500);
   }
   function hideSaveExecute() {
      $('#save').modal('hide');
   }
   jQuery(function ($) {
      var beginDrag = 0;
      var endDrag = 0;
      $('.drop').each(function() {
         $(this).html('');
      });
      $( ".drags .drag" ).draggable({
         snap: ".drop, .draghome",
         start: function() {
            beginDrag = 1;
            console.log(beginDrag);
         },
         drag: function() {

         },
         stop: function(event, ui) {
            if(beginDrag == 1) {
               var draghome = $('.draghome.' + $(this).attr('id'));
               var position = draghome.position();
               console.log(position);
               $(this).animate( {
                  top: position.top,
                  left: position.left
               }, 500, function() {

               });
               console.log('Kein Draghome');
            } else {
               console.log(beginDrag);
            }
         }
      });
      var t2Choices = new Array();
      $( ".qtext .drop" ).droppable({
         drop: function( event, ui ) {
            beginDrag = 0;
            t2Choices.push($(this).attr('id') + '_' + ui.draggable.attr('id'));
         }
      });
       $( ".draghome" ).droppable({
         drop: function( event, ui ) {
            var remove = ui.draggable.attr('id');
            $(t2Choices).each(function(e, value) {
               var part = value.split('_');
               if(part[1] == remove) {
                   t2Choices.splice(e, 1);
               }
            });
            console.log(t2Choices);
         }
      });
      $('.drags .drag').each(function() {
         var classes = $(this).attr('class');
         var drag    = $(this);
         classes = classes.split(" ");
         $(classes).each(function(e, value) {
            choice = value.substring(0,6);
            if(choice == 'choice') {
               position = $('.draghome.' + value).position();
               drag.css('top', position.top).css('left', position.left);
            }
         })
      });
      $('#reset').on('click', function() {
         location.reload();
      });
      $('#check').on('click', function() {
         var quizzID = $('#quizzID').val();
         if($('#quizzTyp').val() == 1) {
            var answers = new Array();
            $('.custom-select').each(function() {
               if($(this).val()) {
                  answers.push($(this).val());
               }
            })
            if(answers.length != $('#countQue').val()) {
               alert("Bitte beantworten Sie zunächst alle Fragen");
               return false;
            }
            jQuery.ajax({
               type: "POST",
               url: "index.php?option=com_auditum&task=quizz.checkQuizz",
               data: {quizzID:quizzID, answers:answers},
               dataType: 'json',
               success: function( data ) {
                  $('#result .result-inner').empty();
                  var hs      = 0;
                  $(data).each(function(e, value) {
                     var antwort = '';
                     var color   = '';
                     
                     if(value.result == 0) {
                        antwort  = 'Leider Falsch';
                        color    = 'text-danger';
                     }
                     if(value.result == 1) {
                        antwort  = 'Richtig';
                        color    = 'text-success';
                        hs++;
                     }
                     
                     $('#result .result-inner').append(
                        '<p class="mt-1 mb-1 ' + color + '"><b>Frage ' + value.antwort + ': ' + antwort + '</b></p>'
                     );
                     
                  });
                  hs = (hs * 100) / $('#countQue').val();
                  hs = Math.ceil(hs);
                  $('#result .result-inner').append(
                     '<h2>Highscore: ' + hs + '%</h2>'
                  );
                  $('#check').addClass('disabled');
               }
            });
         }
         if($('#quizzTyp').val() == 2) {
            if(t2Choices.length != $('#countQue').val()) {
               alert("Bitte beantworten Sie zunächst alle Fragen");
               return false;
            }
            jQuery.ajax({
               type: "POST",
               url: "index.php?option=com_auditum&task=quizz.checkQuizz2",
               data: {quizzID:quizzID, t2Choices:t2Choices},
               dataType: 'json',
               success: function( data ) {
                  $('#result .result-inner').empty();
                  var hs      = 0;
                  $(data).each(function(e, value) {
                     var antwort = '';
                     var color   = '';
                     
                     if(value.result == 0) {
                        antwort  = 'Leider Falsch';
                        color    = 'text-danger';
                     }
                     if(value.result == 1) {
                        antwort  = 'Richtig';
                        color    = 'text-success';
                        hs++;
                     }
                     
                     $('#result .result-inner').append(
                        '<p class="mt-1 mb-1 ' + color + '"><b>Frage ' + value.antwort + ': ' + antwort + '</b></p>'
                     );
                     
                  });
                  hs = (hs * 100) / $('#countQue').val();
                  hs = Math.ceil(hs);
                  $('#result .result-inner').append(
                     '<h2>Highscore: ' + hs + '%</h2>'
                  );
                  t2Choices = new Array();
                  $('#check').addClass('disabled');
               }
            });
         }
      });
   });
</script>