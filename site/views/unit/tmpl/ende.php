<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$doc = JFactory::getDocument();
$doc->addStyleSheet( 'components/com_auditum/assets/css/jquery-ui.css' );
$doc->addScript('components/com_auditum/assets/js/jquery-1.11.2.js');
$doc->addScript('components/com_auditum/assets/js/jquery-ui.js');
$doc->addScript( 'components/com_auditum/assets/js/jquery.validate.js' );
//GET FRAGE
?>
<form action="" method="post" name="adminForm" id="adminForm">
   	<div class="col-12">
    	<h3>Herzlichen Glückwunsch!</h3>
    	<h4>Sie haben alle Fragen dieser Unit beantwortet.</h4>
    	<?php echo $this->item->result;?>
        <a class="btn btn-primary" href="screenmaster/units">Zurück zu Ihren Units</a>
    </div>
   	<input type="hidden" id="auditID" name="auditID" value="" />
    <input type="hidden" name="option" value="com_auditum" />
    <input type="hidden" name="task" value="audit.stop" />
    <?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
 </script>