<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$dokument = JFactory::getDocument();
$dokument->addStyleSheet( 'components/com_auditum/assets/css/jquery-ui.css' );
$dokument->addScript('components/com_auditum/assets/js/jquery-1.11.2.js');
$dokument->addScript('components/com_auditum/assets/js/jquery-ui.js');
$dokument->addScript( 'components/com_auditum/assets/js/jquery.validate.js' );
$dokument->addScript('components/com_auditum/assets/js/unit.js');

$now = new DateTime();
?>
<style>
    #btn-container .btn {
        border: none;
    }
    #bt-ja.aktiv {
        background-color: #28a745;
    }
    #bt-jaA.aktiv {
        background-color: #ffc107;;
    }
    #bt-nein.aktiv {
        background-color: #dc3545;
    }
.titel {
	font-size: 20px;
	font-weight: 600;	
}
.frage {
	font-size: 16px;
	font-weight: 400;	
}
.ko {
	position: absolute;
	top: 11px;
	right: 16px;	
	color: darkred;
	font-size: 24px;
}
#punkteReason_chosen {
	float: left;
	margin-top: 10px;	
}
</style>
<form action="<?php JRoute::_('index.php?option=com_auditum&task=unit.checkQuestion'); ?>" method="post" name="adminForm" id="adminForm">
	<div class="row">
        <div class="col-12">
        	<?php if(!$this->item->name) {
    			echo "Sie haben das Ende des Audits erreicht";
    		} ?>
        	<h1>Unit: <i><?php echo $this->item->name;?></i></h1>
    		<p class=" text-info"><?php echo $now->format('d.m.Y H:i');?></p>
      	</div>
       	<div class="col-12 col-sm-9">
            <div class="panel-body">
           		<div class="row">
                    <div class="col-12 "> 
                        <div class="card">
                            <div class="card-body">
                            	<h3><?php echo $this->item->frage->name;?></h3>
                                <?php echo $this->item->frage->text;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer mt-3">
                <?php if($this->item->frage->position == 1) { ?>
                     <a class="btn bg-danger btn-lg text-white" href="index.php?option=com_auditum&task=unit.stop">Audit beenden</a>
                <?php } else { ?>
                    <a class="btn bg-warning btn-lg text-white" href="index.php?option=com_auditum&task=unit.prevF&id=<?php echo $this->item->frage->unitID;?>&q=<?php echo $this->item->frage->position;?>">Zurück<a/>
                    <a onclick="return confirm('Möchten Sie das Audit unterbrechen?')" class="btn bg-danger btn-lg text-white" href="index.php?option=com_auditum&task=audit.storno">Audit unterbrechen</a>
                <?php } ?>
                <a id="check" class="float-right btn btn-success btn-lg text-white">Weiter</a>
            </div>
        </div>
        <div class="col-12 col-sm-3">
            <div class="card bg-light">
                <div class="card-body">
                    <h3>Auswertung</h3>
                    <table class="table table-striped table-sm">
                        <tr>
                            <td>Frage</td>
                            <td>
                                <span id="frageIst">
                                    <b><?php echo $this->item->frage->position;?></b>
                                </span> von <b>
                                <span id="frageGesamt"><?php echo $this->item->getCountFragen;?></span>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>Punktzahl</td>
                            <td><?php echo $this->item->getPoints;?></td>
                        </tr>
                        <tr>
                            <td>Beginn Audit</td>
                            <td><?php echo $this->item->start;?></td>
                        </tr>
                        <tr>
                            <td>Auditdauer</td>
                            <td><?php echo $this->item->dauer;?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
<input type="hidden" id="position" value="<?php echo $this->item->frage->questionNo;?>" />
<input type="hidden" id="quizzTyp" value="<?php echo $this->item->frage->type;?>" />
<input type="hidden" id="countQue" value="<?php echo $this->item->frage->que;?>" />
<input type="hidden" id="resultID" value="<?php echo $this->item->getResult;?>" />
<input type="hidden" id="unitID" value="<?php echo $this->item->frage->unitID;?>" />
<input type="hidden" id="quizzID" value="<?php echo $this->item->frage->quizzID;?>" />
<input type="hidden" id="t5answer" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>