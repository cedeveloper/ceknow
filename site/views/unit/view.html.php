<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
 
/**
 * Kunde item view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewUnit extends JViewLegacy
{
	protected $item;
	protected $form;
	protected $state;
	
	public function display($tpl = null)
	{
		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		$session 	= JFactory::getSession();
		$customer 	= $session->get('customerID');
		if($customer):
			if($this->item->created_by == 0):
				$this->disabled 		= 1;
			else:
				$this->disabled 		= 0;
			endif;
		else:
			$this->disabled 		= 0;
		endif;
		$this->usergroup 	= $session->get('group');
		parent::display($tpl);
	}
}
?>