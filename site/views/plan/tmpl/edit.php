<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

    <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a class="btn btn-success text-white" onclick="Joomla.submitform('plan.simpleSave')">Speichern</a>
         <a class="btn btn-secondary text-white" onclick="Joomla.submitform('plan.save')">Speichern & Schließen</a>
      <?php endif;?>
        <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
    </div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Allgemein</a>
        </li>
        <!--<li class="nav-item">
            <a class="nav-link" id="asp-tab" data-toggle="tab" href="#asp" role="tab" aria-controls="asp" aria-selected="false">Ansprechpartner</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-controls="global" aria-selected="false">Global</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="freigabe-tab" data-toggle="tab" href="#freigabe" role="tab" aria-controls="freigabe" aria-selected="false">Freigabe</a>
        </li>-->
    </ul>
    <div class="tab-content" id="tabContent">
        <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
            <div class="form-horizontal mt-3">
               <div class="row">
                  <div class="col-12 col-sm-6">
                     <div class="form-group row">
                        <div class="col-12 col-sm-2 col-form-label">
                         <?php echo $this->form->getLabel('title'); ?>
                        </div>
                        <div class="col-12 col-sm-10">
                         <?php echo $this->form->getInput('title'); ?>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12 col-sm-2 col-form-label">
                         <?php echo $this->form->getLabel('detail'); ?>
                        </div>
                        <div class="col-12 col-sm-10">
                         <?php echo $this->form->getInput('detail'); ?>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12 col-sm-2 col-form-label">
                         <?php echo $this->form->getLabel('description'); ?>
                        </div>
                        <div class="col-12 col-sm-10">
                         <?php echo $this->form->getInput('description'); ?>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12 col-sm-2 col-form-label">
                         <?php echo $this->form->getLabel('description_f'); ?>
                        </div>
                        <div class="col-12 col-sm-10">
                         <?php echo $this->form->getInput('description_f'); ?>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6">
                      <div class="form-group row">
                          <div class="col-12 col-sm-4 col-form-label">
                            <?php echo $this->form->getLabel('price'); ?>
                          </div>
                          <div class="col-12 col-sm-8">
                            <?php echo $this->form->getInput('price'); ?>
                          </div>
                      </div>
                      <div class="form-group row">
                          <div class="col-12 col-sm-4 col-form-label">
                            <?php echo $this->form->getLabel('count_admin'); ?>
                          </div>
                          <div class="col-12 col-sm-8">
                            <?php echo $this->form->getInput('count_admin'); ?>
                          </div>
                      </div>
                      <div class="form-group row">
                          <div class="col-12 col-sm-4 col-form-label">
                            <?php echo $this->form->getLabel('count_trainer'); ?>
                          </div>
                          <div class="col-12 col-sm-8">
                            <?php echo $this->form->getInput('count_trainer'); ?>
                          </div>
                      </div>
                      <div class="form-group row">
                          <div class="col-12 col-sm-4 col-form-label">
                            <?php echo $this->form->getLabel('count_learningrooms'); ?>
                          </div>
                          <div class="col-12 col-sm-8">
                            <?php echo $this->form->getInput('count_learningrooms'); ?>
                          </div>
                      </div>
                      <div class="form-group row">
                          <div class="col-12 col-sm-4 col-form-label">
                            <?php echo $this->form->getLabel('count_modules'); ?>
                          </div>
                          <div class="col-12 col-sm-8">
                            <?php echo $this->form->getInput('count_modules'); ?>
                          </div>
                      </div>
                      <div class="form-group row">
                          <div class="col-12 col-sm-4 col-form-label">
                            <?php echo $this->form->getLabel('count_units'); ?>
                          </div>
                          <div class="col-12 col-sm-8">
                            <?php echo $this->form->getInput('count_units'); ?>
                          </div>
                      </div>
                      <div class="form-group row">
                          <div class="col-12 col-sm-4 col-form-label">
                            <?php echo $this->form->getLabel('published'); ?>
                          </div>
                          <div class="col-12 col-sm-8">
                            <?php echo $this->form->getInput('published'); ?>
                          </div>
                      </div>
                  </div>
               </div>
            </div>
        </div>
    </div>
<input type="hidden" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[title]': {
            required: true
         },
         'jform[first_name]': {
            required: true
         },
         'jform[last_name]': {
            required: true
         }
      },
      messages: {
         'jform[title]': 'Bitte geben Sie eine Bezeichnung für den Plan ein.',
         'jform[first_name]': 'Bitte geben Sie Ihren Vornamen ein.',
         'jform[last_name]': 'Bitte geben Sie Ihren Nachnamen ein.'
      },
      submitHandler: function(form) {
         form.submit();
      }
   });
   $('#jform_price').on('change', function() {
      var price = $('#jform_price').val();
      if(!$.isNumeric(price)) {
         if(price.indexOf(',') > -1) {
            
         } else {
            alert('Bitte geben Sie den Preis als Zahl ein. Verwenden Sie als Dezimaltrennzeichen ein Komma (,)');
            $('#jform_price').val(0);
            return false;
         }
      }
      if(price.indexOf('.') != -1) {
         price = price.replace('.', ',');
         $('#jform_price').val(price);
      }
      cPrice = price.split(',');
      $('#jform_price').val(price);
   });
</script>