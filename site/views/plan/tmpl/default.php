<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;
require_once JPATH_SITE . '/components/com_inclure/models/global.php';
$model = JModelLegacy::getInstance('global', 'InclureModel');

$doc = JFactory::getDocument();
$doc->addStyleSheet( 'components/com_inclure/assets/css/inclure.css' );
$doc->addStyleSheet('components/com_inclure/assets/fontawesome/css/font-awesome.css');
$doc->addStyleSheet( 'components/com_inclure/assets/css/bootstrap.css' );
$doc->addStyleSheet( 'components/com_inclure/assets/css/jquery-ui.css' );
$doc->addScript('components/com_inclure/assets/js/jquery-1.11.2.js');
$doc->addScript('components/com_inclure/assets/js/jquery-ui.js');
$doc->addScript('components/com_inclure/assets/js/inclure.js');
$doc->addScript( 'components/com_inclure/assets/js/jquery.validate.js' );
$userData = JFactory::getUser();
$user = $userData->name;
$userid = $userData->id;
$rolle = $this->rolle;
if(!$user) {
	$app = JFactory::getApplication();
	$msg = 'Sie haben keine Berechtigung für diese Seite';
	$app->redirect('index.php', $msg, $msgType='message');	
}
?>
<div class="row">
<div class="col-xs-12">
<div class="uebersicht">
	<div class="user">
		<?php require_once JPATH_COMPONENT.'/lib/header.php';?>
	</div>
    <div class="titelzeile">
        <span><h1 style="float: left;">Neuer Kunde</h1></span>
        <span><h2 style="float: right;"></h2></span>
    </div>
    <div class="buttonleiste">
    	<nav>
          <ul>
            <li><a href="#Beispiel">Funktionen</a>
                <ul>
                  <li><a href="#"id="kundeKopieren" class="">Kunde kopieren</a></li>
                  <li><a id="delete" href="index.php?option=com_inclure&task=kunde.delete&id=<?php echo $this->kunde->id;?>"  class="">Kunde löschen</a></li>
                </ul>
            </li>
            <li><a href="#Beispiel">Reports</a>
                <ul>
                  <li><a href="index.php?option=com_inclure&task=kunde.printBeilagenauftrag&id=<?php echo $this->auftrag->id;?>" class="" target="_blank">Kundendaten drucken</a></li>
                </ul>
              </li>
              <li style="float: right;"><a href="index.php?option=com_inclure&view=kunden" class="btn-red-1">Zurück</a></li>
          </ul>
    	</nav>
    </div>
    <div id="inclureMessages"></div>
   	<form action="<?php echo JRoute::_('index.php?option=com_inclure&view=mandant'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="row">
   	<div class="col-xs-12">
        <div class="detail_content">
            <table class="table table-striped">
            	<thead>
                	<tr><th colspan="2">Auftragsdaten</th></tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="2">
                        <div class="form_group">
                        <label for"auftragnr">Kunden-Nr.:*</label>
                        <input type="text" id="kundennr" name="kundennr" value="<?php echo $this->kunde->kundennr; ?>" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="form_group">
                        <label for"auftragnr">Name*:</label>
                        <input type="text" id="name" name="name" value="<?php echo $this->kunde->name; ?>" />
                        </div>
                    </td>
                </tr>
               	<tr>
                    <td colspan="2">
                        <div class="form_group">
                        <label for"sapnr">Adresse*:</label>
                        <input type="text" id="adresse" name="adresse" value="<?php echo $this->kunde->adresse; ?>" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                    	<label for="kunde">PLZ*:</label>
                        <input type="text" id="plz" name="plz" value="<?php echo $this->kunde->plz; ?>" />
                    </td>
                    <td colspan="1">
                        <div class="form_group">
                        <label for"sapnr">Ort*:</label>
                        <input type="text" id="ort" name="ort" value="<?php echo $this->kunde->ort; ?>" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="form_group">
                        <label for"sapnr">Ansprechpartner:</label>
                        <input type="text" id="asp_name" name="asp_name" value="<?php echo $this->kunde->asp_name; ?>" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                    	<label for="kunde">Telefon*:</label>
                        <input type="text" id="telefon" name="telefon" value="<?php echo $this->kunde->telefon; ?>" />
                    </td>
                    <td colspan="1">
                        <label for"sapnr">Fax:</label>
                        <input type="text" id="fax" name="fax" value="<?php echo $this->kunde->fax; ?>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                    	<label for="kunde">E-Mail:</label>
                        <input type="text" id="email" name="email" value="<?php echo $this->kunde->email; ?>" />
                    </td>
                    <td colspan="1">
                        <label for"sapnr">Homepage:</label>
                        <input type="text" id="homepage" name="homepage" value="<?php echo $this->kunde->web; ?>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    	<label for="kunde">Automatisch entsorgen bei:</label>
                        <input type="text" id="automatisch_entsorgen" name="automatisch_entsorgen" value="0" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">	
                        <div class="form_group">
                        <label for"auftragnr">Publiziert:</label>
                        <select id="published" name="published">
                            <option <?php if($this->kunde->published == 0) { echo 'selected';} ?> value="0">Nein</option>
                            <option <?php if($this->kunde->published == 1) { echo 'selected';} ?> value="1">Ja</option>
                        </select>
                        </div>
                    </td>
                </tr>
                <tr>
                	<td colspan="2">
                    	<button onClick="" type="submit" class="btn btn-success">Speichern</button>
                    </td>
                </tr>
           		</tbody>
            </table>
       	</div>
  	</div>
    </div>
   	<input type="hidden" name="option" value="com_inclure" />
    <input type="hidden" name="task" value="kunde.edit" />
    <?php echo JHtml::_('form.token'); ?>
    </form>
</div>
</div>
</div>
<script type="text/javascript">
	var form = $("#adminForm");
	form.validate({
		rules: {
			//sap_kunde: "required",
			kunde: "required",
			et: "required",
			projekt: "required",
			beilagentyp: "required",
			//vertreter: "required",
			//vertreternr: "required",
			auflage: {
				number: true
			},
			paket_size: {
				number: true
			}
		},
		messages: {
			sap_kunde: "Bitte geben Sie eine SAP-Kunden-Nr. ein.",
			kunde: "Bitte geben Sie einen Kunden ein.",
			et: "Bitte wählen Sie einen Erscheinungstermin aus.",
			projekt: "Bitte wählen Sie ein Projekt aus.",
			beilagentyp: "Bitte wählen Sie einen Beilagentypen aus.",
			vertreter: "Bitte geben Sie den Vertreter ein.",
			vertreternr: "Bitte geben Sie die Vertreter-Nr. ein."
		},
		submitHandler: function(form) {
			selectBox = document.getElementById("tourenGebucht");
			for (var i = 0; i < selectBox.options.length; i++) 
			{ 
				 selectBox.options[i].selected = true; 
			} 
			form.submit();
		}
	});
	jQuery('#auftragKopieren').click(fnOpenNormalDialog);
	function fnOpenNormalDialog() {
		jQuery("#dialog-confirm").dialog({
			resizable: false,
			modal: true,
			title: "Auftrag kopieren",
			height: 250,
			width: 400,
			buttons: {
				"Ja": function () {
					jQuery(this).dialog('close');
					auftragKopieren();
				},
				"Nein": function () {
					jQuery(this).dialog('close');
					//callback(false);
				}
			}
		});
	}
	function auftragKopieren() {
		var id = jQuery('#auftragID').val();
		var datum = jQuery('#copyDate').val();
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_inclure&task=ajax.auftragKopieren",
			data: {id:id,datum:datum},
			//dataType: 'json',
			success: function( data ){
				
				//alert(data);
				alert("Der Auftrag wurde kopiert. Sie werden automatisch zum neuen Auftrag weitergeleitet.");
				window.location.href = "index.php?option=com_inclure&view=auftrag&layout=edit&id=" + data;
			}
		});
	}
</script>