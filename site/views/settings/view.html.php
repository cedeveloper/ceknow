<?php
/**
 *  	DATS Torsten Scheel
 * 	  	JClassroom 1.0
 * 		LMS-System
 * 	 	(C) 2020. All Rights reserved.
 * 		05. April 2020
 *		Development: Torsten Scheel
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class JclassroomViewSettings extends JViewLegacy
{
    protected $item;
    protected $form;
    protected $state;

	function display($tpl = null)
	{
        require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        $this->state    = $this->get('State');
        $this->item     = $this->get('Item');
        $this->form     = $this->get('Form');
		$session = JFactory::getSession();
    	$app = JFactory::getApplication();
    	$user = JFactory::getUser();
    	$groups = JAccess::getGroupsByUser($user->id);
        $session = JFactory::getSession();
        switch($session->get('group')) {
            case 'superuser':
                $this->retour   = 'manager-administrator/customers';
                break;
            case 'customer':
                $this->retour   = 'manager-customer';
                break;
            case 'trainer':

                break;
            case 'student':

                break;
        }
		// Display the view
		parent::display($tpl);
	}
}
