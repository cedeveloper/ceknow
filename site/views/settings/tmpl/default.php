<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

    <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a class="btn btn-success text-white" onclick="Joomla.submitform('settings.simpleSave')">Speichern</a>
         <a class="btn btn-secondary text-white" onclick="Joomla.submitform('settings.save')">Speichern & Schließen</a>
      <?php endif;?>
        <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
    </div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
         <li class="nav-item">
            <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Systemeinstellungen</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="plan-tab" data-toggle="tab" href="#plan" role="tab" aria-controls="plan" aria-selected="false">Kundeneinstellungen</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="pay-tab" data-toggle="tab" href="#pay" role="tab" aria-controls="pay" aria-selected="false">Reporteinstellungen</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="false">Statistiken</a>
         </li>
    </ul>
    <div class="tab-content" id="tabContent">
         <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
            <div class="form-horizontal mt-3 w-50">
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('emailSender'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                        <?php 
                        $parameter  = '';
                        $wert       = '';
                        if($this->item->parameter):
                            foreach($this->item->parameter as $pa):
                                if($pa['parameter'] == 'emailSender'):
                                    $parameter  = $pa['parameter'];
                                    $wert       = $pa['wert'];
                                endif;
                            endforeach;
                        endif;
                        ?>
                        <input type="email" id="jform_emailSender" name="jform[emailSender]" value="<?php echo $wert;?>" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('emailSenderName'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                        <?php 
                        $parameter  = '';
                        $wert       = '';
                        if($this->item->parameter):
                            foreach($this->item->parameter as $pa):
                                if($pa['parameter'] == 'emailSenderName'):
                                    $parameter  = $pa['parameter'];
                                    $wert       = $pa['wert'];
                                endif;
                            endforeach;
                        endif;
                        ?>
                        <input type="text" id="emailSenderName" name="jform[emailSenderName]" value="<?php echo $wert;?>" />
                    </div>
                </div>
            </div>
         </div>
         <div class="tab-pane fade" id="plan" role="tabpanel" aria-labelledby="plan-tab">

         </div>
         <div class="tab-pane fade" id="pay" role="tabpanel" aria-labelledby="pay-tab">

         </div>
         <div class="tab-pane fade" id="users" role="tabpanel" aria-labelledby="users-tab">

         </div>
    </div>
<input type="hidden" id="id" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>