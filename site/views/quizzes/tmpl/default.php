<?php
/**
 * @author      
 * @copyright   
 * @license     
 */

defined("_JEXEC") or die("Restricted access");

// necessary libraries
JHtml::_('bootstrap.tooltip');

$doc = JFactory::getDocument();
$doc->addStyleSheet( 'components/com_jwarehouse/assets/css/jw.css' );
$doc->addStyleSheet('components/com_jwarehouse/assets/css/select2.min.css');
$doc->addStyleSheet('components/com_jwarehouse/assets/css/jquery-ui.css');
$doc->addScript('components/com_jwarehouse/assets/js/select2.min.js');
$doc->addScript('components/com_jwarehouse/assets/js/jquery-ui.min.js');

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
$user = JFactory::getUser();
?>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm">
<div class="auditum form-content">
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionboard mt-3 mb-3">
        <button type="button" class="btn btn-success" onclick="Joomla.submitform('fragenkatalog.add')"><?php echo JText::_('JNEW') ?></button>
        <?php if (isset($this->items[0]->published)) : ?>

        <?php elseif (isset($this->items[0])) : ?>
            <button type="button" class="btn btn-danger" onclick="Joomla.submitform('fragenkataloge.delete')"><?php echo JText::_('Löschen') ?></button>
        <?php endif; ?>
        <?php if (isset($this->items[0]->published)) : ?>
            <button type="button" class="btn btn-danger" onclick="Joomla.submitform('fragenkataloge.delete')"><?php echo JText::_('Löschen') ?></button>
        <?php endif; ?>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="nowrap left" style="width: 50px;"></th>
                <th class="nowrap left" style="width: 50px;">
                    <?php echo JHtml::_('grid.sort', JText::_('ID'), 'a.id', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', JText::_('Name'), 'a.name', $listDirn, $listOrder) ?>
                </th>
                <th>Beschreibung</th>
                <th class="nowrap center">Status</th>
            </tr>
        </thead>
        <tbody>
        <?php if($this->items) { ?>
            <?php foreach($this->items as $item) { ?>
                <?php $i = 0;?>
                <tr>
                    <td><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
                    <td><?php echo $item->id;?></td>
                    <td>
                        <a href="<?php echo JRoute::_("index.php?option=com_auditum&view=quizz&layout=edit&id=" . $item->id); ?>">
                            <?php echo $item->name;?>
                        </a>
                    </td>
                    <td>
                        <?php 
                            echo $item->description;
                        ?>
                    </td>
                    <td class="center">
                    <?php 
                        switch($item->published) { 
                            case 1:
                                echo '<h1 class="badge badge-success m-0"><i class="fa fa-check"></i></h1>';
                                break;
                            case 0:
                                echo '<h1 class="badge badge-danger m-0"><i class="fa fa-ban"></i></h1>';
                                break;
                            case 2:
                                echo '<h1 class="badge badge-primary m-0"><i class="fa fa-archive"></i></h1>';
                                break;
                            case -2:
                                echo '<h1 class="badge badge-warning m-0"><i class="fa fa-trash"></i></h1>';
                                break;
                        } 
                    ?>
                    </td>
                </tr>
                <?php $i++;?>
            <?php } ?>
        <?php }  else { ?>
            <tr>
                <td colspan="5">
                    <p>Es wurden keine Fragenkataloge gefunden. Bitte überprüfen Sie die Filtereinstellungen</p>
                </td>
            </tr>
        </div>
        <?php } ?>
        </tbody>
    </table>
    <?php echo $this->pagination->getPagesLinks(); ?>
</div>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
