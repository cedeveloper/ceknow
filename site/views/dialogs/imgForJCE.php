<div id="theImageBuilder">
   <h5>Bilderverwaltung</h5>
   <a id="imageUpload" onclick="uploadContentImage();" class="btn btn-warning text-white">Bild hochladen</a>
   <a id="imageChoose" onclick="insertContentImage();" class="imageChoose btn btn-info text-white">Bild einfügen</a>
   <div>
      <small>Bilder können nicht in der <b>Code-Ansicht</b> des Editors eingefügt werden</small>
   </div>   
   <div id="waitForWork" class="wait">
      Bilder werden verarbeitet
      <img width="80px" src="images/logo_solo.png" />
   </div>
   <div id="readyForWork" class="wait">
      Bilder wurden erfolgreich hochgeladen
      <img width="80px" src="images/logo_solo.png" />
   </div>
</div>
