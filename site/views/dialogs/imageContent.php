<div id="insertImage" class="modal" tabindex="-1" role="dialog" style="top: 0px;">
   <div class="modal-dialog" role="document" style="margin-top: 50px; max-width: 900px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Bild einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" onclick="showContent();" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         
            <div id="contentImages"></div>
         </div>
         <div class="modal-footer">
            <div class="row w-100" style="align-items: end;">
               <div class="col-12 col-lg-8" style="display: flex;justify-content: space-between;">
                  <div style="">
                     <small>Breite</small>
                     <input id="width"  class="form-control" type="number" value="0" style="width: 100px;" placeholder="Breite in px" />
                     <small style="font-size: 11px;">Eingabe in Pixel</small>
                  </div>
                  <div style="">
                     <small>Höhe</small>
                     <input id="height"  class="form-control" type="number" value="0" style="width: 100px;" placeholder="Höhe in px" />    
                     <small style="font-size: 11px;">Eingabe in Pixel</small>
                  </div>
                  <div style="">
                     <small>Rahmen</small>
                     <input id="border"  class="form-control" type="number" value="0" style="width: 100px;" placeholder="Höhe in px" />
                     <small style="font-size: 11px;">Eingabe in Pixel</small>
                  </div>     
               </div>
               <div class="col-12 col-lg-4 text-right">
                  <button onclick="insertImage();" id="editContentModule" type="button" class="btn btn-primary">Einfügen</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="showContent();" >Abbrechen</button>
                  <input type="hidden" id="editor" value="" />
                  <input type="hidden" id="setInAsFile" value="" />
                  <input type="hidden" id="setInAsFileID" value="" />
                  <input type="hidden" id="theChoosenImageID" value="" />
               </div>
            </div>
         </div>
      </div>
   </div>
</div>