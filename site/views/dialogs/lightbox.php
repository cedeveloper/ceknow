<div id="lightbox" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Dateibearbeitung</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <img id="lightbox_theImage" src="" />
            <p class="m-0"><small>Größe: <span id="lightbox_size"></span> KB</small></p>
            <p class="m-0"><small>Dateityp: <span id="lightbox_extension"></span></small></p>
         </div>
         <div class="modal-footer">
            <button id="lightbox_deleteImage" onClick="lightbox_deleteImage();" type="button" class="btn btn-danger">Löschen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>