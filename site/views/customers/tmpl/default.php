<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="index.php?option=com_jclassroom&task=students.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept=".csv" name="uploadCSV" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <a class="btn btn-success text-white" onclick="Joomla.submitform('customer.add')">Neuer Kunde</a>
        <a class="btn btn-danger text-white" id="deleteCustomer">Kunde(n) löschen</a>
        <!--<button type="button" class="btn btn-info" onclick="Joomla.submitform('students.saveCSV')">Liste exportieren</button>-->
        <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right">Zurück</a>
    </div>
    <table class="table table-striped"> 
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Kunde<br>Kunden-Nr.', 'a.company_name', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    Adresse
                </th> 
                <th class="nowrap left">
                    Administratoren/Ansprechpartner
                </th> 
                <th class="nowrap left">
                    Registriert am/von<br/>Bearbeitet am/von
                </th> 
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Plan'), 'a.planID', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Aktiviert'), 'a.password', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Status'), 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>        
        <tbody>
        <?php if($this->items) { ?>
        <?php foreach ($this->items as $i => $item) :?>
            <tr class="row<?php echo $i % 2; ?>">
                <td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
                <td class="right"><?php echo $this->escape($item->id); ?></td>
                <td>
                    <a href="<?php echo JURI::Root().'customer-edit?id='. $item->id; ?>">
                        <?php
                        if($item->company_name):
                            echo $item->company_name.'<br/>';
                        else:
                            echo $item->name.'<br/>';
                        endif;
                        ?>
                    </a>
                    <?php echo $this->escape($item->customer_number); ?>
                </td>
                <td class="left">
                    <?php echo $this->escape($item->address); ?><br/>
                    <?php echo $item->postcode.' '.$item->city; ?> 
                </td>               
                <td class="left">
                    <?php
                        if($item->customer_administratoren):
                            foreach($item->customer_administratoren as $admin):
                                echo '<i class="fa fa-graduation-cap" data-toggle="popover" title="Hinweis" data-content="Administrator"></i> <span style="font-size: 12px;">'.$admin->first_name.' '.$admin->last_name.'</span> <span class="badge bg-success text-white">'.$admin->phone.'</span> / <span class="badge bg-danger text-white">'.$admin->email.'</span><br/>';
                            endforeach;
                        endif;
                        echo '<hr>';
                        if($item->customer_contacts):
                            foreach($item->customer_contacts as $contact):
                                echo '<i class="fa fa-address-card"></i> <span style="font-size: 12px;">'.$contact->first_name.' '.$contact->last_name.'</span> <span class="badge bg-success text-white">'.$contact->phone.'</span> / <span class="badge bg-danger text-white">'.$contact->email.'</span><br/>';
                            endforeach;
                        endif;
                    ?>      
                </td>
                <td>
                    <span style="font-size: 12px;">
                        <?php echo date('d.m.Y H:i', strtotime($item->created)).'/'.$item->creator;?>
                    </span>
                    <br/>
                    <?php if($item->modifier): ?>
                        <span style="font-size: 12px;">
                            <?php echo date('d.m.Y H:i', strtotime($item->modified)).'/'.$item->modifier;?>
                        </span>
                    <?php endif; ?>
                </td>
                <td class="text-center">
                    <?php
                    $zeitraum = '';
                    $zeitraum = '<p style="font-size: 12px;">'.date('d.m.Y', strtotime($item->plan_from)).'- '.date('d.m.Y', strtotime($item->plan_to)).'</p>';
                    if($item->planID == 1) :
                        $class  = "badge-success";
                        $title  = 'Basis-Plan';
                    endif;
                    if($item->planID == 2) :
                        $class  = 'badge-warning';
                        $title  = 'Premium-Plan';
                    endif;
                    if($item->planID == 3) :
                        $class  = 'badge-danger';
                        $title  = 'Unlimited-Plan';
                    endif;
                    if($item->planID == 4) :
                        $class  = 'badge-secondary';
                        $title  = 'Demozugang';
                        $zeitraum = '<p style="font-size: 12px;">'.date('d.m.Y', strtotime($item->plan_from)).'- '.date('d.m.Y', strtotime($item->plan_to)).'</p>';
                    endif;
                    ?>
                    <span class="badge <?php echo $class;?>" title="<?php echo $title;?>"><?php echo $item->planID;?></span>
                    <?php echo $zeitraum;?>
                </td>
                <td class="text-center">
                    <?php
                        $class  = '';
                        $wert   = '';
                        if($item->password == '') :
                            $class  = "";
                        else:
                            $class  = "badge badge-success";
                            $wert   = "<i class='fa fa-check' title='Kunde hat Zugang bestätigt und ein Passwort vergeben.'></i>";
                        endif;
                    ?>
                    <span class="<?php echo $class;?>"><?php echo $wert;?></span>
                </td>
                <?php
                if($item->published == 0) {
                    $class="badge-danger";
                    $wert = "<i class='fa fa-ban'></i>";
                } elseif($item->published == 1) {
                    $class="badge-success";
                    $wert = "<i class='fa fa-check'></i>";
                }
                ?>
                <td class="text-center">
                    <span class="badge <?php echo $class;?>"><?php echo $wert;?></span>
                </td>
            </tr>
        <?php endforeach ?>
        <?php } else { ?>
            <tr>
                <td colspan="8">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
        <?php } ?>
        </tbody>            
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
jQuery('#openUploader').click(function() {
    jQuery('#uploader').trigger('click');
    
});
jQuery('#uploader').on('change', function() {
    jQuery('#uploaderForm').submit();
});
$('#deleteCustomer').on('click', function() {
    if(confirm('Sollen die ausgewählten Kunden gelöscht werden?') == true) {
        Joomla.submitform('customers.delete');
    }
});
</script>