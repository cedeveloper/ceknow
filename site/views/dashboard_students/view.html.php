<?php
/**
 *  	DATS Torsten Scheel
 * 	  	JClassroom 1.0
 * 		LMS-System
 * 	 	(C) 2020. All Rights reserved.
 * 		05. April 2020
 *		Development: Torsten Scheel
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class JclassroomViewDashboard_students extends JViewLegacy
{

	function display($tpl = null)
	{
		$session    = JFactory::getSession();
        $document   = JFactory::getDocument();
        $document->setTitle('Ihre Learningrooms ['.$session->get('systemname').']');
        $this->systemname 	= $session->get('systemname');
		$this->user 		= JFactory::getUser();
		$this->classrooms  	= $this->get('StudentsClassrooms');
		$this->verification = false;
		//Check if student has confirmed datenschutz, agb and aufzeichnung
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Classroom_verification','JclassroomTable',array());
		$load 	= array('studentID' => $this->user->id);
		$check 	= $table->load($load);
		if($check):
			$this->verification = true;
			$this->verificationAGB 	= $table->agb;
			$this->verificationDS 	= $table->datenschutz;
			$this->verificationFilm = $table->aufzeichnung;
		endif;
		$this->theStudent 	= $this->get('StudentsData');
		$hardware = 0;
		if($this->classrooms):
			foreach($this->classrooms as $classroom):
				if($classroom->hardware_order == 1):
					$hardware = 1;
				endif;
			endforeach;
		endif;
		$this->hardware = $hardware;
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemname');
		$table->load($load);
		$this->systemname = $table->wert;
		// Display the view
		parent::display($tpl);
	}
}
