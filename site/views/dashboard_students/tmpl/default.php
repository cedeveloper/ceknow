<?php
/**
 *  	DATS Torsten Scheel
 * 	  	InVenta 3.0
 * 	 	(C) 2018. All Rights reserved.
 * 		12. Oktober 2018
 *		Editor: Torsten Scheel
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<style>
	input[type=checkbox] {
		transform: scale(2);
		margin-top: 38px;
	}
</style>
<div class="col-12">
	<div class="row">
		<div class="col-12">
			<h1 style="font-size: 18px;">Herzlichen Willkommen, <span style="color: #ff3600;"><?php echo $this->theStudent->salutation.' '.$this->theStudent->first_name.' '.$this->theStudent->last_name;?></span>, bei <?php echo $this->systemname;?>!</h1>
		</div>
		<div class="col-12">
			<div class="card text-white bg-light mb-3" style="font-size: 12px;">
				<div class="card-body p-1">
					<p class="card-title text-dark"><b>Ihre Kundendaten</b></p>
					<div class="row">
						<div class="-col-12 col-sm-4">
							<div class="card-text text-dark p-0">
								<div class="row">
									<div class="col-12 col-sm-4">
										Name:
									</div>
									<div class="col-12 col-sm-8">
										<i><?php echo $this->theStudent->first_name.' '.$this->theStudent->last_name;?></i>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-4">
										Firma:
									</div>
									<div class="col-12 col-sm-8">
										<i><?php echo $this->theStudent->companyName;?></i>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-4">
										Kunden-Nr.:
									</div>
									<div class="col-12 col-sm-8">
										<i><?php echo $this->theStudent->customer_number;?></i>
									</div>
								</div>
							</div>
						</div>
						<div class="-col-12 col-sm-4">
							<div class="card-text text-dark p-0">
								<div class="row">
									<div class="col-12 col-sm-3">
										Anschrift:
									</div>
									<div class="col-12 col-sm-9">
										<i><?php echo $this->theStudent->adress;?></i>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-3">
									</div>
									<div class="col-12 col-sm-9">
										<i><?php echo $this->theStudent->postcode.' '.$this->theStudent->city;?></i>
									</div>
								</div>
							</div>
						</div>
						<div class="-col-12 col-sm-4">
							<div class="card-text text-dark p-0">
								<div class="row">
									<div class="col-12 col-sm-3">
										Telefon:
									</div>
									<div class="col-12 col-sm-9">
										<i><?php echo $this->theStudent->phone;?></i>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-3">
										Fax:
									</div>
									<div class="col-12 col-sm-9">
										<i><?php echo $this->theStudent->fax;?></i>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-3">
										E-Mail:
									</div>
									<div class="col-12 col-sm-9">
										<i><?php echo $this->theStudent->email;?></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 col-lg-4">
			<div class="card text-white bg-primary mb-3">
				<div class="card-header"><i class="fa fa-address-book"></i> Ihr Konto</div>
				<div class="card-body">
					<a href="my-account" class="btn btn-secondary">Zu Ihrem Konto</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 col-lg-4">
			<div class="card text-white mb-3" style="background-color: lightskyblue;">
				<div class="card-header"><i class="fa fa-graduation-cap"></i> Learningrooms</div>
				<div class="card-body">
					<a href="dashboard-students?option=com_jclassroom&layout=list" class="btn btn-primary">Zu Ihren gebuchten Learningrooms</a>
				</div>
			</div>
		</div>
		<!--<div class="col-12 col-sm-6 col-md-4 col-lg-4">
			<div class="card text-white mb-3" style="background-color: coral;">
				<div class="card-header"><i class="fa fa-question-circle"></i> Hilfe</div>
				<div class="card-body">
					<a href="dashboard-students?option=com_jclassroom&layout=list" class="btn btn-primary">Zum Hilfesystem</a>
				</div>
			</div>
		</div>-->
		<div class="col-12">
			<div class="card text-white bg-light mb-3">
				<div class="card-header text-dark">Shortcuts</div>
				<div class="card-body">
					<a href="abmelden" class="float-right btn btn-danger">Abmelden</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	if(
		($this->theStudent->showVertifyAGB == 1 && $this->verificationAGB == 0) ||
		($this->theStudent->showVertifyDS == 1 && $this->verificationDS == 0) ||
		($this->theStudent->showVertifyFilm == 1 && $this->verificationFilm == 0)):
		echo '<div id="verifications">';
			echo '<div class="col-12">';
			echo '<h4>Bitte bestätigen Sie folgende Punkte:</h4>';
			require_once('verification.php');
			require_once('modalDS.php');
			require_once('modalAGB.php');
			//require_once('modalFilm.php');
			require_once('modalError.php');
			echo '</div>';
		echo '</div>';
	endif;
?>
<input type="hidden" id="clr" name="clr" value="<?php echo $this->classrooms[0]->id;?>" />
<input type="hidden" id="showVertifyAGB" value="<?php echo $this->theStudent->showVertifyAGB;?>" />
<input type="hidden" id="showVertifyDS" value="<?php echo $this->theStudent->showVertifyDS;?>" />
<input type="hidden" id="showVertifyFilm" value="<?php echo $this->theStudent->showVertifyFilm;?>" />
<input type="hidden" id="isVertifyAGB" value="<?php echo $this->verificationAGB;?>" />
<input type="hidden" id="isVertifyDS" value="<?php echo $this->verificationDS;?>" />
<input type="hidden" id="isVertifyFilm" value="<?php echo $this->verificationFilm;?>" />
<script type="text/javascript">
function openModal(type) {
	if(type == 1) {
		jQuery('#modalDS').modal();
	}
	if(type == 2) {
		jQuery('#modalAGB').modal();
	}
}
function checkVerification() {
	var cA = 1;
	var cD = 1;
	var cF = 1;
	if($('#showVertifyAGB').val() == 1) {
		if($('#isVertifyAGB').val() == 0) {
			if(!jQuery('#agb').is(':checked')) {
				cA = 0;
				jQuery('#modalError').modal();
				return false;
			}
		}
	} else {
		$cA = 0;
	}
	if($('#showVertifyDS').val() == 1) {
		if($('#isVertifyDS').val() == 0) {
			if(!jQuery('#ds').is(':checked')) {
				cD = 0;
				jQuery('#modalError').modal();
				return false;
			}
		}
	} else {
		$cD = 0;
	}
	if($('#showVertifyFilm').val() == 1) {
		if($('#isVertifyFilm').val() == 0) {
			if(!jQuery('#film').is(':checked')) {
				cF = 0;
				jQuery('#modalError').modal();
				return false;
			}
		}
	} else {
		$cF = 0;
	}
	jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=dashboard_students.verification",
		data: {cA:cA,cD:cD,cF:cF},
		//dataType: 'json',
		success: function( data ) {
			$('#verifications').fadeOut('200'); 
		}
	});
	
}
jQuery('#closeDS').on('click', function() {
	jQuery('#modalDS').modal('toggle');
});
jQuery('#closeAGB').on('click', function() {
	jQuery('#modalAGB').modal('toggle');
});
jQuery('#closeError').on('click', function() {
	jQuery('#modalError').modal('toggle');
});
</script>

