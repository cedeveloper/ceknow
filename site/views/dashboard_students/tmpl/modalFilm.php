<?php
/**
 *  	DATS Torsten Scheel
 * 	  	InVenta 3.0
 * 	 	(C) 2018. All Rights reserved.
 * 		12. Oktober 2018
 *		Editor: Torsten Scheel
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<h4>Sie benötigen Hardware</h4>
<p>Wenn Sie für die Teilnahme am Training Hardware benötigen, können Sie diese gerne bei uns bestellen. Nutzen Sie hierfür den nachfolgen Link.</p>
<a href="hardwareanforderung?id=<?php echo $this->user->id;?>" class="btn btn-primary">Zum Anforderungsformular</a>
<br><br>
<h4>Ihre gebuchten classrooms</h4>
<?php 
	if($this->classrooms):
		foreach($this->classrooms as $classroom):
			echo '<h5>'.$classroom->title.'</h5><br/>';
			echo '<p><b>Beginn: </b>'.date('d.m.Y',strtotime($classroom->fromDate)).'<br>';
			echo '<b>Ende: </b>'.date('d.m.Y', strtotime($classroom->toDate)).'<br>';
			echo '<b>Trainer: </b>'.$classroom->trainer_name.'</p>';
			echo '<a href="stage-classroom?layout=edit&id='.$classroom->id.'" target="_blank" class="btn btn-success">Zum classroom</a>';
		endforeach;
	endif;
?>


