<?php
/**
 *  	DATS Torsten Scheel
 * 	  	InVenta 3.0
 * 	 	(C) 2018. All Rights reserved.
 * 		12. Oktober 2018
 *		Editor: Torsten Scheel
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
require_once('order.php');
?>
<style>
	input[type=checkbox] {
		transform: scale(2);
		margin-top: 38px;
	}
</style>
<div class="row">
<?php 
	if($this->verification->datenschutz == 0 || $this->verification->agb == 0):
		echo '<div class="col-12 mb-3" style="jusitfy-content: space-between;align-items: flex-start;">';
		echo '<div class="introText">';
			echo '<h4 class="">Ihre gebuchten Learningrooms</h4>';
			echo '<small><i class="fa fa-question-circle"></i> Hier finden Sie alle Learningrooms, die Sie bei '. $this->systemname.' gebucht haben.</small>';
			if($this->hardware == 1):
				echo '<br/><small><i class="fa fa-exclamation-circle"></i> Wenn Sie zu einem Learningroom <b>Hardware</b> benötigen, verwenden Sie bitte den entsprechenden blauen Button <b>Hardware bestellen</b> um zum Bestellvorgang zu gelangen.</small>';
			endif;
			echo '<a href="dashboard-students" class="btn btn-danger text-white float-right">Zurück</a>';
		echo '</div>';	
		echo '</div>';
		if($this->classrooms):
			foreach($this->classrooms as $classroom):
				if(!$classroom->trainer_name):
					$trainer = $classroom->trainer_name2;
				else:		
					$trainer = $classroom->trainer_name;
				endif;
				echo '<div class="col-12 col-sm-4 mb-2">';
				echo '<div class="card p-2 text-dark" style="background-color: skyblue;">';
				echo '<h5 style="min-height: 72px;"><small style="font-size: 12px; font-weight: bold;">(LR'.str_pad($classroom->classroomID,8,'0', STR_PAD_LEFT).')</small><br/>'.$classroom->title.'</h5>';
				if($classroom->fromDate && $classroom->toDate):
					echo '<p><b class="d-inline-block" style="width: 100px;">Beginn: </b>'.date('d.m.Y',strtotime($classroom->fromDate)).'<br>';
					echo '<b class="d-inline-block" style="width: 100px;">Ende: </b>'.date('d.m.Y', strtotime($classroom->toDate)).'<br>';
				endif;
				echo '<b class="d-inline-block" style="width: 100px;">Trainer: </b>'.$trainer.'</p>';
				if($classroom->orderID && $classroom->orderpositions):
					$showEdit = 'block';
					$showNew  = 'none';
					$showChange = 'none';
				else:
					$showEdit = 'none';
					if($classroom->hardware_order == 1 && $classroom->orderID):
						$showNew  = 'none';
						$showChange = 'block';
					else:
						$showNew  = 'block';
						$showChange = 'none';
					endif;
				endif;
				echo '<a onclick="openOrder('.$classroom->orderID.')"; class="mb-1 btn btn-warning" style="display: '.$showEdit.';"><i class="fa fa-truck"></i> Bestellte Hardware</a>';
				echo '<a href="order-edit?id='.$classroom->orderID.'&type=fromSt&uid='.$this->user->id.'&clr='.$classroom->id.'" class="mb-1 btn btn-primary" style="display: '.$showChange.';"><i class="fa fa-hdd-o"></i> Hardwarebestellung ändern</a>';
				if($classroom->hardware_order == 1):
					echo '<a href="order-edit?type=fromSt&uid='.$this->user->id.'&clr='.$classroom->classroomID.'" class="mb-1 btn btn-primary" style="display: '.$showNew.';"><i class="fa fa-hdd-o"></i> Hardware bestellen</a>';
				endif;
				echo '<a href="stage-classroom?layout=edit&id='.$classroom->classroomID.'" target="_blank" class="mb-1 btn btn-secondary"><i class="fa fa-address-book-o"></i> Zum Learningroom</a>';
				echo '</div>';
				echo '</div>';
			endforeach;
		else:
			echo '<p>Keine Learningrooms gefunden.</p>';
		endif;
	else:
		echo '<div class="col-12">';
		echo '<h4>Bitte bestätigen Sie folgende Punkte:</h4>';
		require_once('verification.php');
		require_once('modalDS.php');
		require_once('modalAGB.php');
		require_once('modalError.php');
		echo '</div>';
	endif;
?>
</div>
<input type="hidden" id="clr" name="clr" value="<?php echo $this->classrooms[0]->id;?>" />
<script type="text/javascript">
function openModal(type) {
	if(type == 1) {
		jQuery('#modalDS').modal();
	}
	if(type == 2) {
		jQuery('#modalAGB').modal();
	}
}
function openOrder(id) {
	$('#order').modal('show');
	$('#saveOrder').css('display', 'block');
	jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=dashboard_students.loadOrderpositions",
		data: {id:id},
		//dataType: 'json',
		success: function( data ) {
			$('#order #orderpositions tbody').empty();
			$('#saveOrder').attr('onclick', 'saveOrder(' + id + ');')
			$('#order #orderpositions tbody').append(data);
		}
	});
}
function checkPositionValue(id) {
	var value 	= $('#position' + id).val();
	if(!value) {
		alert('Bitte geben Sie eine Zahl ein.');
		$('#position' + id).val(0);
		return false;
	}
}
function saveOrder(id) {
	var positions = $('#orderpositions tbody tr input');
	$(positions).each(function() {
		var position 	= $(this).attr('id');
		var positionID 	= position.substr(8);
		var value 		= $(this).val();
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=dashboard_students.savePositions",
			data: {positionID:positionID,value:value},
			//dataType: 'json',
			success: function( data ) {

			}
		});
	});
	$('#order').modal('hide');
	alert("Ihre Bestellung wurde gespeichert.");
}
function checkVerification() {
	if(!jQuery('#ds').is(':checked')) {
		jQuery('#modalError').modal();
		return false;
	}
	if(!jQuery('#agb').is(':checked')) {
		jQuery('#modalError').modal();
		return false;
	}
	if(!jQuery('#film').is(':checked')) {
		jQuery('#modalError').modal();
		return false;
	}
	var clr = jQuery('#clr').val();
	jQuery.ajax({
		type: "POST",
		url: "index.php?option=com_jclassroom&task=dashboard_students.verification",
		data: {clr:clr},
		//dataType: 'json',
		success: function( data ) {
			location.reload();
		}
	});
	
}
function deletePosition(id) {
  if(confirm('Soll die Bestellposition mit der ID ' + id + ' gelöscht werden?') == true) {
     $.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=order.deletePosition",
        data: {
           id:id
        },
        //dataType: 'json',
        success: function( data ) {
           $('#position_' + id).remove();
           var cPos = $('#orderpositions tr.positions').length;
           if(!cPos || cPos == 0) {
           		$('#orderpositions tbody').append('<tr><th colspan="3">Keine Bestellpositionen</th></tr>');
           		$('#saveOrder').css('display', 'none');
           }
        }
     });
  }
}
jQuery('#closeDS').on('click', function() {
	jQuery('#modalDS').modal('toggle');
});
jQuery('#closeAGB').on('click', function() {
	jQuery('#modalAGB').modal('toggle');
});
jQuery('#closeError').on('click', function() {
	jQuery('#modalError').modal('toggle');
});
jQuery('#closeOrder').on('click', function() {
	jQuery('#order').modal('toggle');
});
</script>

