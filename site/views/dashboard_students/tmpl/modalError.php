<style>
   #modalError .modal-open .modal {
      height: 300px;
   }
   #modalError .modal-dialog {
      max-width: 800px;
      height: 100%;
   }
   #modalError .modal-body {
      height: 100px;
      overflow-y: hidden;
   }
</style>
<div id="modalError" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Nachricht</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
          	<p>Bitte bestätigen Sie, das Sie alle Bereiche zur Kenntnis genommen haben.</p>
         </div>
         <div class="modal-footer">
            <button id="closeError" type="button" class="btn btn-primary">Schließen</button>
         </div>
      </div>
   </div>
</div>