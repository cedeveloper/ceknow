<?php if($this->theStudent->showVertifyDS == 1 && $this->verificationDS == 0): ?>
<div class="form-group row">
    <div class="col-12 col-sm-2 col-form-label">
      Bitte bestätigen Sie, dass Sie unsere <a onclick="openModal(1);" style="cursor:pointer;font-weight: bold;" class="text-primary">Datenschutzerklärung </a>zur Kenntnis genommen haben.
    </div>
    <div class="col-12 col-sm-10">
    	<input type="checkbox" id="ds" value="Datenschutzerklärung" />
    </div>
</div>
<?php endif; ?>
<?php if($this->theStudent->showVertifyAGB == 1 && $this->verificationAGB == 0): ?>
<div class="form-group row">
    <div class="col-12 col-sm-2 col-form-label">
      Bitte bestätigen Sie, dass Sie unsere <a onclick="openModal(2);" style="cursor:pointer;font-weight: bold;" class="text-primary">AGB </a> zur Kenntnis genommen haben.
    </div>
    <div class="col-12 col-sm-10">
    	<input type="checkbox" id="agb" value="AGB" />
    </div>
</div>
<?php endif; ?>
<?php if($this->theStudent->showVertifyFilm == 1 && $this->verificationFilm == 0): ?>
    <div class="form-group row">
        <div class="col-12 col-sm-2 col-form-label">
          Bitte bestätigen Sie, dass Sie damit einverstanden sind, dass wir Webinare aufzeichnen.
        </div>
        <div class="col-12 col-sm-10">
        	<input type="checkbox" id="film" value="Aufzeichnung" />
        </div>
    </div>
<?php endif; ?>
<div class="form-group row">
    <div class="col-12">
    	<a class="btn btn-primary text-white" onclick="checkVerification();">Bestätigen</a>
    </div>
</div>

