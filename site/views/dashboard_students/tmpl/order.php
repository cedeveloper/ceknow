<style>
   #order .modal-open .modal {
      height: 300px;
   }
   #order .modal-dialog {
      max-width: 800px;
      height: 100%;
   }
</style>
<div id="order" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Hardwarebestellung</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         	<small><i class="fa fa-question-circle"></i> Für den Learningroom haben Sie folgende Hardware bestellt.</small>
          	<table id="orderpositions" class="table table-striped table-sm mt-3">
          		<thead>
          			<tr>
          				<th>Artikel</th>
          				<th class="text-right">Menge</th>
                  <th class="text-center">Entfernen</th>
          			</tr>
          		</thead>
          		<tbody>

          		</tbody>
          	</table>
         </div>
         <div class="modal-footer">
         	<button onclick="saveOrder();" id="saveOrder" type="button" class="btn btn-primary">Speichern</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
         </div>
      </div>
   </div>
</div>

