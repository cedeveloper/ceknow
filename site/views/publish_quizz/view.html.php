<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
 
/**
 * Kunde item view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewPublish_quizz extends JViewLegacy
{
	protected $item;
	protected $form;
	protected $state;

	public function display($tpl = null)
	{
		require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        $session 	= JFactory::getSession();
		$document 	= JFactory::getDocument();
		$document->setTitle('Quizz veröffentlichen ['.$session->get('systemname').']');
		// Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
	        JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
	        $demo = new LoginHelper();
	        $demo->checkDemo();
	    endif;

		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour = 'manager-administrator';
				$this->usergroup = 'superuser';
				break;
			case 'customer':
				$this->retour = 'manager-customer';
				$this->usergroup = 'customer';
				break;
			case 'trainer':
				$this->retour = 'veroeffentlichen/veroeffentlichte-quizze';
				$this->usergroup = 'trainer';
				break;
			case 'student':
				$this->retour = 'veroeffentlichen/veroeffentlichte-quizze';
				$this->usergroup = 'student';
				break;
		}
		$text = file_get_contents(JURI::Root().'images/jclassroom/configuration/publishedQuizz_infotext.txt');
		$this->description = $text;
		parent::display($tpl);
	}
}
?>