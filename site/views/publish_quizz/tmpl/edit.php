<?php
/**
 * @author     
 * @copyright  
 * @license    
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');

$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js' );
$doc->addScript('components/com_jclassroom/assets/js/imgForJCE.js');
require_once(JPATH_COMPONENT.'/views/stage/tmpl/message.php');
require_once(JPATH_COMPONENT.'/views/dialogs/imageContent.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <div class="content row">
      <div class="col-12 mb-3">
            <a onclick="Joomla.submitform('publish_quizz.save')" class="btn btn-success text-white">Speichern</a>
            <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right">Zurück</a>
      </div>
      <div class="col-12">
         <?php echo $this->description;?>
      </div>
         <div class="col-12">
            <div class="form-horizontal mt-3">
                <div class="form-group row">
                   <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('quizzID'); ?>
                   </div>
                   <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('quizzID'); ?>
                   </div>
                </div>
                <div class="form-group row">
                   <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('title'); ?>
                   </div>
                   <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('title'); ?>
                   </div>
                </div>

                <div class="form-group row">
                   <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('description'); ?>
                   </div>
                     <div class="use_editor col-12 col-sm-10" data-editor="jform_description">
                     <?php 
                        if($this->item->id):
                           echo $this->form->getInput('description'); 
                           require(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');
                        else:
                           echo 'Zur Eingabe einer Beschreibung speichern Sie zunächst die Veröffentlichung';
                        endif;
                     ?>
                   </div>
                </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('internalDescription'); ?>
                  </div>
                  <div class="use_editor col-12 col-sm-10" data-editor="jform_internalDescription">
                     <?php
                        if($this->item->id):
                           echo $this->form->getInput('internalDescription'); 
                           //require(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');
                        else:
                           echo 'Zur Eingabe einer Beschreibung speichern Sie zunächst die Veröffentlichung';
                        endif;
                     ?>
                  </div>
               </div>
               <div class="form-group row">
                   <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('companyID'); ?>
                   </div>
                   <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('companyID'); ?>
                   </div>
                </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('classroomID'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('classroomID'); ?>
                  </div>
               </div>
              <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('quizzType'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('quizzType'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('openQuizz'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('openQuizz'); ?>
                  </div>
               </div>
               <div id="publishDates" style="display: none;">
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('publish_up'); ?>
                     </div>
                     <div class="col-12 col-sm-10 d-flex">
                        <?php echo $this->form->getInput('publish_up'); ?>
                        <div class="d-inline-block ml-2">
                           <?php echo $this->form->getInput('publish_up_time'); ?>
                           <div id="validationServer03Feedback" class="invalid-feedback">
                              Bitte geben Sie eine gültige Uhrzeit ein (xx:xx).
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-12 col-sm-2 col-form-label">
                        <?php echo $this->form->getLabel('publish_down'); ?>
                     </div>
                     <div class="col-12 col-sm-10 d-flex">
                        <?php echo $this->form->getInput('publish_down'); ?>
                        <div class="d-inline-block ml-2">
                        <?php echo $this->form->getInput('publish_down_time'); ?>
                           <div id="validationServer03Feedback" class="invalid-feedback">
                              Bitte geben Sie eine gültige Uhrzeit ein (xx:xx).
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('published'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('published'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <label>Link zur Veröffentlichung</label>
                  </div>
                  <div class="col-12 col-sm-10">
                     <a target="_blank" href="stage?sT=quizz&qID=<?php echo $this->item->id;?>&q=-1">stage?sT=quizz&qID=<?php echo $this->item->id;?>&q=-1</a>
                  </div>
               </div>
            </div>
        </div>
    </div>
<input type="hidden" id="id" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<?php require_once(JPATH_COMPONENT.'/views/dialogs/formForJCE.php'); ?>
<script type="text/javascript">
let unitID = $('#id').val();
let classroomID = false;
let part = 'quizz';
$(document).ready(function() {
   showPublishDates();
});
var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[quizzID]': {
            required: true
         },
         'jform[title]': {
            required: true
         },
         'jform[last_name]': {
            required: true
         }
      },
      messages: {
         'jform[quizzID]': 'Bitte wählen Sie ein Quizz aus.',
         'jform[title]': 'Bitte geben Sie einen Namen für dieses Quizz ein.',
         'jform[last_name]': 'Bitte geben Sie Ihren Nachnamen ein.'
      },
      submitHandler: function(form) {
         var submit = 0;
         var type = $('#jform_openQuizz option:selected').val();
         if(type == 1) {
            var from = $('#jform_publish_up').val();
            var to   = $('#jform_publish_down').val();
            if(from && to) {
               submit = 1;
            } else {
               alert('Bitte legen Sie fest, in welchem Zeitraum das Quizz verfügbar sein soll.');
               return false;
            }
         }
         if(type == 0) {
            submit = 1;
         }
         if(submit == 1) {
            form.submit();
         }
      }
   });
function showPublishDates() {
   var type = $('#jform_openQuizz option:selected').val();
   if(type == 1) {
      $('#publishDates').slideDown(400); 
   } else {
      $('#publishDates').slideUp(400);
   }
}
function checkTime(id) {
   if(id == 1) {
      var fromT   = $('#jform_publish_up_time').val();
      var c1      = 0;
      if(fromT.indexOf(':') == -1) {
         c1 = 1;
      } else {
         var fromT1 = fromT.split(':');
         if(isNaN(fromT1[0])) {
            c1 = 1;
         } else {
            if(fromT1[0] > 23 || fromT1[0] < 0) {
               c1 = 1;
            }
         }
         if(isNaN(fromT1[1])) {
            c1 = 1;
         } else {
            if(fromT1[1] > 59 || fromT1[1] < 0) {
               c1 = 1;
            }
         }
      }
      if(c1 == 1) {
         $('#jform_publish_up_time').addClass('is-invalid').removeClass('is-valid');
         $('#jform_publish_down').blur();
         return false;
      } else {
         $('#jform_publish_up_time').addClass('is-valid').removeClass('is-invalid');
      }
   }
   if(id == 2) {
      var fromT   = $('#jform_publish_down_time').val();
      var c2      = 0;
      if(fromT.indexOf(':') == -1) {
         c2 = 1;
      } else {
         var fromT1 = fromT.split(':');
         if(isNaN(fromT1[0])) {
            c2 = 1;
         } else {
            if(fromT1[0] > 23 || fromT1[0] < 0) {
               c2 = 1;
            }
         }
         if(isNaN(fromT1[1])) {
            c2 = 1;
         } else {
            if(fromT1[1] > 59 || fromT1[1] < 0) {
               c2 = 1;
            }
         }
      }
      if(c2 == 1) {
         $('#jform_publish_down_time').addClass('is-invalid').removeClass('is-valid');
         $('#jform_published').blur();
         return false;
      } else {
         $('#jform_publish_down_time').addClass('is-valid').removeClass('is-invalid');
      }
   }
}
function setTime(id) {
  if(id == 1) {
    var fromT = $('#jform_publish_up_time').val();
    if(!fromT) {
      $('#jform_publish_up_time').val('00:00');
    }
  }
  if(id == 2) {
    var fromT = $('#jform_publish_down_time').val();
    if(!fromT) {
      $('#jform_publish_down_time').val('00:00');
    }
  }
}
</script>