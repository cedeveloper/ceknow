<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewStudent extends JViewLegacy {

   	protected $item;
	protected $form;
	protected $state;

    public function display($tpl = null) {
    	require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();

		$user = JFactory::getUser();
		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');

		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour = 'manager-administrator/students';
				$this->usergroup = 'superuser';
				break;
			case 'customer':
				$this->retour = 'manager-customer/students-customer';
				$this->usergroup = 'superuser';
				break;
			case 'trainer':
				$this->retour = 'manager-trainer/students';
				$this->usergroup = 'trainer';
				// Check if current User is allowed to edit Trainer (as Student)
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$student 	= JTable::getInstance('Student','JclassroomTable',array());
				$student->load($this->item->id);
				$userID 	= $student->tblUserID;
				if($user->id != $userID) {
					$this->closed = 1;
					echo '<div class="alert alert-danger p-1"><i class="fa fa-exclamation-circle"></i> Sie können diesen Teilnehmer nicht editieren.</div>';
				}
				break;
			case 'student':
				$this->retour = 'manager-student';
				$this->usergroup = 'student';
				$this->closed = 0;
				break;
		}
        parent::display($tpl);
    }
}
