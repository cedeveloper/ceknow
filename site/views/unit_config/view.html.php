<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewUnit_config extends JViewLegacy {

   	protected $item;
	protected $form;
	protected $state;

    public function display($tpl = null) {

    	JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
        $login = new LoginHelper();
        $login->checkLogin();

		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour = 'quizze';
				$this->usergroup = 'superuser';
				break;
			case 'customer':
				$this->retour = 'quizze';
				$this->usergroup = 'customer';
				break;
			case 'trainer':
				$this->retour = 'quizze';
				$this->usergroup = 'trainer';
				break;
			case 'student':
				$this->retour = 'manager-student';
				$this->usergroup = 'student';
				break;
		}
		$this->showEdit = 0;
		if($this->usergroup == 'superuser'): 
			$this->showEdit = 1;
		else:
			if($this->item->created_by != 0): 
				$this->showEdit = 1;
			endif;
		endif;
        parent::display($tpl);
    }
}
