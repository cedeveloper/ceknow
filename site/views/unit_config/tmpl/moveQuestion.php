<div id="moveQuestion" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title d-block">Frage in eine Gruppe verschieben</h5>
           
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div id="messageTrue" class="alert alert-info" style="display: none;">Wählen Sie eine Gruppe aus, in die Sie die aktuelle Frage verschieben möchten.</div>
            <div id="messageFalse" class="alert alert-danger" style="display: none;">Sie können Fragen nicht innerhalb der gleichen Gruppe verschieben oder es gibt keine weiteren Gruppen.</div>
            <select id="moveQuestionToGroup" class="select2-max">
               <option value="0" disabled>Bitte auswählen</option>
            </select>
         </div>
         <div class="modal-footer">
            <button id="saveMoveQuestionToGroup" onclick="saveMoveQuestionToGroup();" type="button" class="btn btn-primary">Verschieben</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="oldModuleID" value="" />
            <input type="hidden" id="questionToMove" value="" />
         </div>
      </div>
   </div>
</div>