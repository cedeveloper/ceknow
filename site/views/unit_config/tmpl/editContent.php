<div id="editContent" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog mt-3" role="document">
      <div class="modal-content" style="height: 100%;width: 100%;">
         <div class="modal-header">
            <h5 class="modal-title">Inhalt bearbeiten</h5>
            <button type="button" onclick="closeEditContentModule();" class="close" data-dismiss="modala" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="use_editor" data-editor="jform_content">
            <?php 
            echo $this->form->getInput('content');
            require_once(JPATH_COMPONENT.'/views/dialogs/imgForJCE.php');
            require_once(JPATH_COMPONENT.'/views/dialogs/formForJCE.php');
            ?>
            </div>
         </div>
         <div class="modal-footer" style="display: flex; justify-content: space-between;">
            <div class="float-left"></div>
            <div class="float-right">
            <button onclick="editContentModule();" id="editContentModule" type="button" class="btn btn-primary">Einfügen</button>
            <button onclick="closeEditContentModule();" type="button" class="btn btn-secondary" data-dismiss="">Abbrechen</button>
            </div>
            <input type="hidden" id="editContentUnitID" value="" />
            <input type="hidden" id="editContentAnswerID" value="" />
            <input type="hidden" id="contentType" value="" />
         </div>
      </div>
   </div>
</div>