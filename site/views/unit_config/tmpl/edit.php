<?php
/**
 * @author		datsDORTMUND
 * @copyright	(c) 2016 datsDORTMUND. All rights reserved.
 * @product		datsAUDITUM		
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/section.js');
$doc->addScript('components/com_jclassroom/assets/js/imgForJCE.js');
require_once(JPATH_COMPONENT.'/views/unit_config/tmpl/newPart.php');
require_once(JPATH_COMPONENT.'/views/unit_config/tmpl/editContent.php');
require_once(JPATH_COMPONENT.'/views/unit_config/tmpl/moveQuestion.php');
require_once(JPATH_COMPONENT.'/views/dialogs/imageContent.php');
?>
<style>
	.mceListBoxMenu {
		top: 423px!important;
	}
</style>
<form action="" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<div id="quizz-info">
		<h4><?php echo $this->item->title;?></h4>
	</div>
	<div class="actionboard mt-3 mb-3 d-inline-block w-100">
		<?php if($this->showEdit == 1): ?>
			<button type="button" class="btn btn-primary" onclick="Joomla.submitform('unit_config.simpleSave')"><?php echo JText::_('JAPPLY') ?></button>
			<button type="button" class="btn btn-secondary" onclick="Joomla.submitform('unit_config.save')"><?php echo JText::_('Speichern & schließen') ?></button>
		<?php endif; ?>
		<a class="float-right btn btn-danger" href="<?php echo $this->retour;?>"><?php echo JText::_('Zurück') ?></a>
	</div>
	<?php if($this->showEdit == 1): ?>
		<div class="">
			<a id="newGroup" class="newGroup btn btn-warning text-white"><i class="fa fa-plus"></i> Neue Gruppierung</a>
			<a id="newPart" class="newPart btn btn-success text-white"><i class="fa fa-plus"></i> Neue Quizzfrage</a>
		</div>
	<?php endif; ?>
	<h2 class="mt-3">Inhalte dieses Quizzes</h2>
	<div id="sections" class="mt-2">
		<ul id="theSections" class="p-0 m-0">
			<?php
			/*if($this->item->structure) {
				$sectionCount = 0;
				$structure = json_decode($this->item->structure);
				if($structure):
					foreach($structure as $item):
						$sectionCount++;
						$html = '';
						JLoader::register('SectionsHelper',JPATH_COMPONENT_SITE.'/helpers/sections.php');
	                    $template = new SectionsHelper();
	                    $html = $template->getTemplate($item->sectionID, $item->sectionType, $item, $item->id);
	                    //echo $html;
					endforeach;
				else:
					echo 'Keine Quizzfragen gefunden.';
				endif;
			} else {
				$sectionCount = 0;
			}*/
			if($this->item->questions):
				foreach($this->item->questions as $question):
					JLoader::register('SectionsHelper',JPATH_COMPONENT_SITE.'/helpers/sections.php');
					$template = new SectionsHelper();
					$html = $template->getTemplate($item->sectionID, $question->type, $item,$question->id,'',0);
					echo $html;
				endforeach;
			endif;
			?>
		</ul>
	</div>
	<?php if($this->showEdit == 1): ?>
		<div class="mt-3">
			<a id="newGroup" class="newGroup btn btn-warning text-white"><i class="fa fa-plus"></i> Neue Gruppierung</a>
			<a id="newPart" class="newPart btn btn-success text-white"><i class="fa fa-plus"></i> Neue Quizzfrage</a>
		</div>
	<?php endif; ?>
	<div class="actionboard mt-3 mb-3">
		<?php if($this->showEdit == 1): ?>
			<button type="button" class="btn btn-primary" onclick="Joomla.submitform('unit_config.simpleSave')"><?php echo JText::_('JAPPLY') ?></button>
			<button type="button" class="btn btn-secondary" onclick="Joomla.submitform('unit_config.save')"><?php echo JText::_('Speichern & schließen') ?></button>
		<?php endif; ?>
		<a class="float-right btn btn-danger" href="<?php echo $this->retour;?>"><?php echo JText::_('Zurück') ?></a>
	</div>
<input type="hidden" id="sectionsCount" name="sectionsCount" value="<?php echo $sectionCount;?>" />
<input type="hidden" id="unitID" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="tempAnswerString" value="" />
<input type="hidden" id="showEdit" value="<?php echo $this->showEdit;?>" />
<input type="hidden" id="isGroup" value="" />
<input type="hidden" name="task" value="" />
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="quizzconfig_dialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            
         </div>
         <div class="modal-footer">
         	<button id="toDo" type="button" class="btn btn-primary">OK</button>
            <button type="button" class="btn btn-success" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<style>
	.section.group {
		border-top-left-radius: 0px;
		border-bottom-left-radius: 0px;
		border-left: 6px solid red;
	}
	.section.group::before {
		content: 'Gruppe';
		position: absolute;
		left: -72px;
		top: 12px;
		background-color: red;
		color: #fff;
		padding: 0px 4px 2px 4px;
	}
	.sectionBody.show {
		display: flex!important;
	}
	.answerTyp3_correct {
		width: 30px;
		height: 30px;
	}
	.sectionBody textarea {
		width: 100%;
		border-radius: 3px;
		border: 1px solid #a1a1a1;
		font-size: 14px;
		line-height: 18px;
	}
	#save {
		text-align: center;
	}
	.sectionBody.groupBody {
		min-height: 50px;
		margin-top: 5px;
		background-color: #f1f1f1;
		border-radius: 3px;
		padding: 5px;
	}
</style>
<script type="text/javascript">
	let unitID = $('#unitID').val();
	let classroomID = false;
	let part = 'quizz';
	jQuery(document).ready(function() {
		var showEdit 	= $('#showEdit').val();
		if(showEdit == 0) {
			$('.newQuestionToPart').css('display', 'none');
			$('.save').css('display', 'none');
			$('.fa-trash-o').css('display', 'none');
			$('.fa-files-o').css('display', 'none');
			$('.fa-arrows-alt').css('display', 'none');
			$('.fa-ellipsis-v').css('display', 'none');
			$('.sectionTitle').attr('readonly', true).attr('disabled', true);
			$('.sectionPoints').attr('readonly', true).attr('disabled', true);
			$('.section select').attr('readonly', true).attr('disabled', true);

		}
		var v1 			= $(window).width();
		jQuery('#save').css('left', (v1 - 300) / 2);
		jQuery('select.select2-max').select2({width: '100%'});
	});
	// ADD SOMETHING
	function newAnswerTyp3(questionID) {
		jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=unit.loadAnswerTyp3",
         data: {questionID:questionID},
         //dataType: 'json',
         success: function( data ) {
         	jQuery('#wwm_noanswers').remove();
         	jQuery('#answersTyp3_body_' + questionID).append(data);
         	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
         	jQuery('select.select2').select2({width: '300px'});
         	jQuery('select.select2-max').select2({width: '100%'});
         }
      });
	}
	function newOption(questionID) {
		jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=unit.loadOption",
         data: {questionID:questionID},
         //dataType: 'json',
         success: function( data ) {
         	jQuery('#option_body_' + questionID).append(data);
         	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
         }
      });
	}
	function newCheckbox(questionID) {
		jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=unit.loadCheckbox",
         data: {questionID:questionID},
         //dataType: 'json',
         success: function( data ) {
         	jQuery('#checkbox_body_' + questionID).append(data);
         	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
         }
      });
	}
	function editContent(questionID, type) {
		let showEdit 	= $('#showEdit').val();
		//$('#editContent #imageUpload').attr('onclick','uploadContentImage("' + quizz + '");');
		//$('#editContent #imageUpload').attr('onclick','uploadContentImage("' + quizz + '");');
		if(showEdit == 1) {
			$('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
			var content = '';
			if(type == 'wwmanswer') {
				var sectionID 	= $('#answerTyp3row' + questionID).find('.wwmanswerHTML').attr('data-sectionid');
				content = $('#answerTyp3row' + questionID).find('textarea.wwmanswer').val();
				$('#contentType').val('wwmanswer');
				$('#editContentAnswerID').val(questionID);
				$('#editContentUnitID').val(sectionID);
			}
			if(type == 'content') {
				content = $('#section' + questionID).find(' .contentHTML').html();
				$('#contentType').val('content');
				$('#editContentUnitID').val(questionID);
			}
			if(type == 'zusatz') {
				content = $('#section' + questionID).find(' .contentHTML').html();
				$('#contentType').val('zusatz');
				$('#editContentUnitID').val(questionID);
			}
			if(type == 'infotextPositiv') {
				content = $('#section' + questionID).find(' .infotextPositivHTML').html();
				$('#contentType').val('infotextPositiv');
					$('#editContentUnitID').val(questionID);
			}
			if(type == 'infotextNegativ') {
				content = $('#section' + questionID).find(' .infotextNegativHTML').html();
				$('#contentType').val('infotextNegativ');
					$('#editContentUnitID').val(questionID);
			}
			tinyMCE.get('jform_content').setContent(content);
		   	$('#editContent').modal();
		   	var top = $('#jform_content_toolbar1').position();
		   	$('.mceListBoxMenu').css('top',top.top + '!important');
		}
	}
	function editContentModule() {
		var questionID  = $('#editContentUnitID').val();
		var answerID 	= $('#editContentAnswerID').val();
		var content 	= tinyMCE.get('jform_content').getContent();
		var contentType = $('#contentType').val();
		if(contentType == 'wwmanswer') {
			if (content.indexOf('_') != -1) {
				alert('In den Antworten dürfen keine Unterstriche (_) verwendet werden.');
				return false;
			}
			$('#answerTyp3row' + answerID).find('.wwmanswerHTML').html(content);
			$('#answerTyp3row' + answerID).find('.wwmanswer').val(content);
		}
		if(contentType == 'content') {
			$('#section' + questionID).find(' .contentHTML').html(content);
			$('#section' + questionID).find(' .summernote').val(content);
		}
		if(contentType == 'zusatz') {
			$('#section' + questionID).find(' .zusatz.contentHTML').html(content);
			$('#section' + questionID).find(' .zusatz.summernote').val(content);
		}
		if(contentType == 'infotextPositiv') {
			$('#section' + questionID).find(' .infotextPositivHTML').html(content);
			$('#section' + questionID).find(' .infotextPositiv').val(content);
		}
		if(contentType == 'infotextNegativ') {
			$('#section' + questionID).find(' .infotextNegativHTML').html(content);
			$('#section' + questionID).find(' .infotextNegativ').val(content);
		}
   		saveQuestion(questionID);
		$('#editContent').modal('hide');
   	}
   	function closeEditContentModule() {
		let questionID = $('#editContentUnitID').val();
		$('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-danger').addClass('text-success');
    	$('#editContent').modal('hide');
   	}
	// COPY SOMETHING
	function copyQuestion(questionID) {
		var title = $('#sectionTitle' + questionID).val();
		$('#toDo').attr('onclick', 'copyQuestionExecute(' + questionID + ');');
		$('#quizzconfig_dialog .modal-title').html('<i class="fa fa-question-circle"></i> Frage');
		$('#quizzconfig_dialog .modal-body').html('Soll die Frage <i>(' + questionID + ') ' + title +'</i> kopiert werden?');
		$('#quizzconfig_dialog').modal('show');
	}
	function copyQuestionExecute(questionID) {
		$('#quizzconfig_dialog').modal('hide');
		$('#save').slideDown(200);
		var quizzID = $('#unitID').val();
	   	jQuery.ajax({
	        type: "POST",
	        url: "index.php?option=com_jclassroom&task=unit.copyQuestion",
	        data: {quizzID:quizzID,questionID:questionID},
	         //dataType: 'json',
	        success: function( data ) {
	        	$('#sections ul#theSections').append(data);
	        	hideSave();
	        }
	    });
	}
	// OPEN AUTOMATIC
	function openQuestion(id) {
	   	if(jQuery('#sectionBody' + id).hasClass('show')) {
	     	jQuery('#sectionBody' + id).removeClass('show');
	      	jQuery('#section' + id).find(' .open').removeClass('fa-chevron-down').addClass('fa-chevron-right');
	   	} else {
	      	jQuery('#sectionBody' + id).addClass('show');
	      	jQuery('#section' + id).find(' .open').removeClass('fa-chevron-right').addClass('fa-chevron-down');
	   	}
	}
	// SAVE SOMETHING
	function saveQuestion(questionID) {
		var showEdit 	= $('#showEdit').val();
		if(showEdit == 1) {
			var title     	= jQuery('#section' + questionID).find('.sectionTitle').val();
			var points   	= jQuery('#section' + questionID).find('.sectionPoints').val();
			var textT7   	= jQuery('#section' + questionID).find('.sectionCorrectText').val();
			var calculates  = jQuery('#section' + questionID).find('.sectionCalculate').val();
			var content   	= jQuery('#section' + questionID).find('.summernote').val();
			var infotextPositiv   	= jQuery('#section' + questionID).find('.infotextPositiv').val();
			var infotextNegativ   	= jQuery('#section' + questionID).find('.infotextNegativ').val();
			var todo   		= jQuery('#section' + questionID).find('.todo').val();
			var testfield   = jQuery('#section' + questionID).find('.testfield').val();
			var theme   	= jQuery('#section' + questionID).find('.theme').val();
			var infotext   	= jQuery('#section' + questionID).find('.infotext').val();
			var kotext   	= jQuery('#section' + questionID).find('.kotext').val();
			var link      	= jQuery('#section' + questionID).find('.link').val();
			var quizz     	= jQuery('#section' + questionID).find('.quizz option:selected').val();
			var correctAnswers = jQuery('#section' + questionID).find('.correctAnswers option:selected').val();
			var sectionCorrects = jQuery('#section' + questionID).find('.sectionCorrect').val();
			var answers 	= [];
			var options 	= [];
			var checkboxes = [];
			var getAnswersT3 	= jQuery('#section' + questionID + ' .answersTyp3 .answerTyp3_row');
			var correct = 0;
			jQuery(getAnswersT3).each(function() {
				correct = 0;
				var answerID = jQuery(this).attr('id');
				answerID = answerID.replace('answerTyp3_row_', '');
				var answer 	= jQuery(this).find('.wwmanswerHTML').html();
				if(jQuery(this).find('.answerTyp3_correct').prop('checked')) {
					correct = 1;
				}
				answers.push('3_' + answerID + '_' + answer + '_' + correct);
			});

			var getAnswersT5 = jQuery('#section' + questionID + ' .answersTyp5 .answerTyp5row');
			jQuery(getAnswersT5).each(function() {
				var yes 	= jQuery(this).find('.answerTyp5_yes').val();
				var no 		= jQuery(this).find('.answerTyp5_no').val();
				answers.push('5_' + yes + '_' + no);
			});

			var getOptions 	= jQuery('#section' + questionID + ' .optionrow');
			jQuery(getOptions).each(function() {
					correct = 0;
				var optionID	= jQuery(this).attr('id');
				optionID 		= optionID.replace('optionrow', '');
				var option 		= jQuery(this).find('.option_text').val();
				if(jQuery(this).find('.optionCorrect').prop('checked')) {
					correct = 1;
				} 
				options.push('12_' + optionID + '_' + option + '_' + correct);
			});

			var getCheckboxes 	= jQuery('#section' + questionID + ' .checkboxrow');
			jQuery(getCheckboxes).each(function() {
					correct = 0;
				var checkboxID	= jQuery(this).attr('id');
				checkboxID 		= checkboxID.replace('checkboxrow', '');
				var checkbox 		= jQuery(this).find('.checkbox_text').val();
				if(jQuery(this).find('.checkboxCorrect').prop('checked')) {
					correct = 1;
				}
				checkboxes.push('13*/*' + checkboxID + '*/*' + checkbox + '*/*' + correct);
			});
			jQuery.ajax({
				type: "POST",
				url: "index.php?option=com_jclassroom&task=unit.saveQuestion",
				data: {
				 	questionID:questionID,
				 	title:title,
				 	points:points,
				 	textT7:textT7,
				 	calculates:calculates,
				 	content:content,
				 	infotextPositiv:infotextPositiv,
				 	infotextNegativ:infotextNegativ,
				 	todo:todo,
				 	testfield:testfield,
				 	theme:theme,
				 	infotext:infotext,
				 	kotext:kotext,
				 	answers:answers,
				 	quizz:quizz,
				 	correctAnswers:correctAnswers,
				 	options:options,
				 	checkboxes:checkboxes,
				 	sectionCorrects:sectionCorrects
				},
				//dataType: 'json',
				success: function( data ) {
					$('#save').slideDown(200);
					jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-danger').addClass('text-success');
					hideSave();
				}
      		});
      	}
   	} 
	function hideSave() {
		setTimeout(hideSaveExecute,1500);
	}
	function hideSaveExecute() {
		$('#save').slideUp(200);
	}
   	// DELETE SOMETHING
   	function deleteQuestion(questionID) {
		if(confirm('Soll diese Frage aus diesem Quizz entfernt werden?') == true) {
			var cQuestions = $('#section' + questionID + ' .section').length;
			if(cQuestions != 0) {
				alert('Bitte entfernen Sie zunächst alle Fragen aus der Gruppe.');
				return false;
			}
			var quizzID = $('#unitID').val();
			jQuery.ajax({
	         type: "POST",
	         url: "index.php?option=com_jclassroom&task=unit.deleteTheQuestion",
	         data: {quizzID:quizzID, questionID:questionID},
	         //dataType: 'json',
	         success: function( data ) {
	         	jQuery('#section' + questionID).remove();
	         }
	      });
			
		}
	}
	function deleteAnswer(answerID) {
		if(confirm('Soll diese Antwort aus der Frage entfernt werden?') == true) {
			jQuery.ajax({
	         type: "POST",
	         url: "index.php?option=com_jclassroom&task=unit.deleteAnswer",
	         data: {answerID:answerID},
	         //dataType: 'json',
	         success: function( data ) {
	         	jQuery('#answerTyp3row' + answerID).remove();
	         }
	      });
			
		}
	}
	function deleteOption(optionID) {
		if(confirm('Soll diese Option entfernt werden?') == true) {
			jQuery.ajax({
	         type: "POST",
	         url: "index.php?option=com_jclassroom&task=unit.deleteOption",
	         data: {optionID:optionID},
	         //dataType: 'json',
	         success: function( data ) {
	         	jQuery('#optionrow' + optionID).remove();
	         }
	      });
			
		}
	}
	function deleteCheckbox(checkboxID) {
		if(confirm('Soll diese Checkbox entfernt werden?') == true) {
			jQuery.ajax({
	         type: "POST",
	         url: "index.php?option=com_jclassroom&task=unit.deleteCheckbox",
	         data: {checkboxID:checkboxID},
	         //dataType: 'json',
	         success: function( data ) {
	         	jQuery('#checkboxrow' + checkboxID).remove();
	         }
	      });
			
		}
	}
	// CHECK SOMETHING
	jQuery(document).on('change','.correctAnswers', function() {
   		var questionID 		= jQuery(this).attr('id');
   		questionID 			= questionID.replace('correctAnswers_', '');
   		var correctAnswers 	= $('#correctAnswers_' + questionID + ' option:selected').val();
   		if(correctAnswers == 1) {
   			$('#checkbox_body_' + questionID + ' .checkboxCorrect').each(function() {
   				$(this).prop('checked', false);
   			});
   		}
    	saveQuestion(questionID);
   		//jQuery('#saveIndicatorModule' + moduleID).val(1);
   		jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
	jQuery(document).on('change','.sectionCalculate', function() {
      var questionID = jQuery(this).attr('id');
      questionID = questionID.replace('sectionCalculate', '');
       saveQuestion(questionID);
      //jQuery('#saveIndicatorModule' + moduleID).val(1);
      jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
	jQuery(document).on('change','.sectionTitle', function() {
      var questionID = jQuery(this).attr('id');
      questionID = questionID.replace('sectionTitle', '');
      saveQuestion(questionID);
      //jQuery('#saveIndicatorModule' + moduleID).val(1);
      jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
	jQuery(document).on('change','.sectionPoints', function() {
	  var questionID = jQuery(this).attr('id');
	  questionID = questionID.replace('sectionPoints', '');
	  saveQuestion(questionID);
	  //jQuery('#saveIndicatorModule' + moduleID).val(1);
	  jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
	});
	jQuery(document).on('change','.sectionCorrectText', function() {
	  var questionID = jQuery(this).attr('id');
	  questionID = questionID.replace('sectionCorrectText', '');
	  saveQuestion(questionID);
	  //jQuery('#saveIndicatorModule' + moduleID).val(1);
	  jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
	});
	jQuery(document).on('change','.answerTyp5_yes', function() {
	  var questionID = jQuery(this).attr('id');
	  questionID = questionID.replace('sectionAnswerYes', '');
	  saveQuestion(questionID);
	  //jQuery('#saveIndicatorModule' + moduleID).val(1);
	  jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
	});
	jQuery(document).on('change','.answerTyp3_answer', function() {
	  var questionID = jQuery(this).attr('data-id');
	  questionID = questionID.replace('sectionAnswerYes', '');
	  saveQuestion(questionID);
	  //jQuery('#saveIndicatorModule' + moduleID).val(1);
	  jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
	});
	jQuery(document).on('click','.answerTyp3_correct', function() {
	  var questionID = jQuery(this).attr('data-id');
	  var correctAnswers = $('#correctAnswers_' + questionID + ' option:selected').val();
	  console.log("CA: " + correctAnswers);
	  if(correctAnswers == 1) {
	  	$('#section' + questionID + ' .answerTyp3_correct').each(function(){
	  		$(this).prop('checked', false);
	  	});
	  	$(this).prop('checked', true);
	  }
	  saveQuestion(questionID);
	  //jQuery('#saveIndicatorModule' + moduleID).val(1);
	  jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
	});
	jQuery(document).on('change','.answerTyp5_no', function() {
	  var questionID = jQuery(this).attr('id');
	  questionID = questionID.replace('sectionAnswerNo', '');
	  saveQuestion(questionID);
	  //jQuery('#saveIndicatorModule' + moduleID).val(1);
	  jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
	});
	jQuery(document).on('change','.option_text', function() {
	  var questionID = jQuery(this).attr('id');
	  questionID = questionID.replace('option_text', '');
	  saveQuestion(questionID);
	  //jQuery('#saveIndicatorModule' + moduleID).val(1);
	  jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
	});
	jQuery(document).on('click','.optionCorrect', function() {
	  	var questionID = jQuery(this).attr('data-id');
	  	var correctAnswers = $('#correctAnswers_' + questionID + ' option:selected').val();
		if(correctAnswers == 1) {
			$('#section' + questionID + ' .optionCorrect').each(function(){
				$(this).prop('checked', false);
			});
			$(this).prop('checked', true);
		}
	  saveQuestion(questionID);
	  //jQuery('#saveIndicatorModule' + moduleID).val(1);
	  jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
	});
   	jQuery(document).on('change','.checkbox_text', function() {
		var questionID 	= jQuery(this).attr('id');
		questionID 		= questionID.replace('checkbox_text', '');
		
      	saveQuestion(questionID);
      	//jQuery('#saveIndicatorModule' + moduleID).val(1);
      	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
   	jQuery(document).on('click','.checkboxCorrect', function() {
      	var questionID = jQuery(this).attr('data-id');
      	var correctAnswers = $('#correctAnswers_' + questionID + ' option:selected').val();
		if(correctAnswers == 1) {
			$('#section' + questionID + ' .checkboxCorrect').each(function(){
				$(this).prop('checked', false);
			});
			$(this).prop('checked', true);
		}
      saveQuestion(questionID);
      //jQuery('#saveIndicatorModule' + moduleID).val(1);
      jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
   	jQuery(document).on('change','.infotextPositiv', function() {
      	var questionID = jQuery(this).attr('id');
      	questionID = questionID.replace('infotextPositiv', '');
      	saveQuestion(questionID);
      	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
	jQuery(document).on('change','.infotextNegativ', function() {
      	var questionID = jQuery(this).attr('id');
      	questionID = questionID.replace('infotextNegativ', '');
      	saveQuestion(questionID);
      	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
   	jQuery(document).on('change','.todo', function() {
      	var questionID = jQuery(this).attr('id');
      	questionID = questionID.replace('todo', '');
      	saveQuestion(questionID);
      	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
   	jQuery(document).on('change','.testfield', function() {
      	var questionID = jQuery(this).attr('id');
      	questionID = questionID.replace('testfield', '');
      	saveQuestion(questionID);
      	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
   	jQuery(document).on('change','.theme', function() {
      	var questionID = jQuery(this).attr('id');
      	questionID = questionID.replace('theme', '');
      	saveQuestion(questionID);
      	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
   	jQuery(document).on('change','.infotext', function() {
      	var questionID = jQuery(this).attr('id');
      	questionID = questionID.replace('infotext', '');
      	saveQuestion(questionID);
      	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
   	jQuery(document).on('change','.kotext', function() {
      	var questionID = jQuery(this).attr('id');
      	questionID = questionID.replace('kotext', '');
      	saveQuestion(questionID);
      	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
   	jQuery(document).on('change','.sectionCorrect', function() {
      	var questionID = jQuery(this).attr('id');
      	questionID = questionID.replace('sectionCorrect', '');
      	saveQuestion(questionID);
      	jQuery('#section' + questionID + ' .sectionHeader').find('.save').removeClass('text-success').addClass('text-danger');
   	});
	jQuery('#theSections').sortable({
      handle: '.move',
      axis: 'y',
      update: function (event, ui) {
         var nodes = jQuery(this).context.children;
         var cid   = ui.item.attr('id');
         console.log(cid);
         cid       = cid.substring(7);
         var order = [];
         jQuery(nodes).each(function(e) {
            var id   = jQuery(this).context.id;
            id       = id.substring(7);
            order.push(e + '_' + id);
         });
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=unit.writeSortable",
            data: {id:cid,order:order},
            //dataType: 'json',
            success: function( data ){
            }
         });
      }
   	});
   	jQuery('.sectionBody.groupBody ul').sortable({
	    handle: '.move',
	    axis: 'y',
	    update: function (event, ui) {
	    	var groupID = jQuery(this).closest('.group').attr('id');
	    	var quizzID = jQuery('#unitID').val();
	    	groupID  	= groupID.substring(7);
			var nodes = jQuery(this).context.children;
			var order = [];
			jQuery(nodes).each(function(e) {
				var id   = jQuery(this).context.id;
				id       = id.substring(7);
				order.push(e + '_' + id);
			});
         	jQuery.ajax({
	            type: "POST",
	            url: "index.php?option=com_jclassroom&task=unit.writeSortableGroup",
	            data: {quizzID:quizzID,groupID:groupID,order:order},
	            //dataType: 'json',
	            success: function( data ){
	            }
	        });
      	}
   	}); 
	$('.newPart').on('click', function() {
		$('#isGroup').val('');
		$('#newPart_dialog').modal();
	});
	function newQuestionToPart(groupID) { 
		$('#isGroup').val(groupID);
		$('#newPart_dialog').modal();
	}
	$('.newGroup').on('click', function() {
		var sectionID 	= $('#sectionsCount').val();
		var quizzID 	= $('#unitID').val();
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=unit.newGroup",
			data: {quizzID:quizzID,sectionID:sectionID,isGroup:1},
			//dataType: 'json',
			success: function( data ) {
				$('#sections ul#theSections').append(data);
			}
		});
	});
	$('#newSectionSave').on('click', function() {
		var type 		= $('#newSectionType').val();
		if(!type) {
			alert('Bitte wählen Sie eine Quizzfrage aus.');
			return false;
		}
		var groupID 	= $('#isGroup').val();
		getType(type, groupID);

	});
	function getType(type,groupID) {
		var sectionID 	= $('#sectionsCount').val();
		var quizzID 	= $('#unitID').val();
		var isGroup 	= $('#isGroup').val();
		$('#newPart_dialog').modal('hide');
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=unit.getTemplate",
			data: {
				type:type,
				sectionID:sectionID,
				quizzID:quizzID,
				groupID:groupID,
				isGroup:isGroup
			},
			//dataType: 'json',
			success: function( data ) {
				if(!groupID) {
					$('#sections ul#theSections').append(data);
				}
				if(groupID) {
					$('#section' + groupID + ' .sectionBody ul').append(data);
				}
				var newID 	= $('#theSections li').last().attr('id');
				newID 		= newID.substring(7); 
				jQuery('#sectionBody' + newID + ' select.select2').select2({width: '300px'});
				jQuery('#sectionBody' + newID + ' select.select2-max').select2({width: '100%'});
			}
		});
	}
	function moveQuestion(questionID) {
		var inGroup 	= 0;
		if($('#section' + questionID).hasClass('inGroup')) {
			inGroup = 1;
		};
		if(inGroup == 0) {
			var cGroups = $('.group').length;
			if(cGroups == 0) {
				alert('Keine Gruppen gefunden, in die diese Frage verschobene werden kann.');
				return false;
			} 
			if(cGroups != 0) {
				$('#moveQuestionToGroup').empty();
				$('#moveQuestionToGroup').attr('disabled', false);
				$('#saveMoveQuestionToGroup').css('display', 'block');
				$('#messageTrue').css('display','block');
				$('#messageFalse').css('display','none');
				jQuery('.group').each(function() {
					var id = jQuery(this).attr('id');
					id = id.substring(7);
					var groupTitle = jQuery(this).find('.sectionTitle').val();
					jQuery('#moveQuestionToGroup').append(new Option(groupTitle, id));
				});
			}
		}
		if(inGroup == 1) {
			var cGroups = $('.group').length;
			if((cGroups -1) == 0) {
				alert('Keine weiteren Gruppen gefunden, in die diese Frage verschobene werden kann.');
				return false;
			} 
			if((cGroups -1) != 0) {
				$('#moveQuestionToGroup').empty();
				var ownGroup 	= $('#section' + questionID).attr('data-groupid');
				$('#moveQuestionToGroup').attr('disabled', false);
				$('#saveMoveQuestionToGroup').css('display', 'block');
				$('#messageTrue').css('display','block');
				$('#messageFalse').css('display','none');
				jQuery('#moveQuestionToGroup').append(new Option('In Hauptebene', 0));
				jQuery('.group').each(function() {
					var id = jQuery(this).attr('id');
					id = id.substring(7);
					if(id != ownGroup) {
						var groupTitle = jQuery(this).find('.sectionTitle').val();
						jQuery('#moveQuestionToGroup').append(new Option(groupTitle, id));
					} 
				});
			} 
		}
		jQuery('#questionToMove').val(questionID);
		jQuery('#moveQuestion').modal('show');
   }
   function saveMoveQuestionToGroup() {
      jQuery('#wait').css('opacity', 1);
      var questionToMove    = jQuery('#questionToMove').val();
      var groupID   		= jQuery('#moveQuestionToGroup option:selected').val();
      if(groupID == 0) {
      	jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=unit.moveQuestionToMainfield",
			data: {questionToMove:questionToMove,groupID:0},
			//dataType: 'json',
			success: function( data ) {
	            jQuery('#moveQuestion').modal('hide');
	            var clone = jQuery('#section' + questionToMove).clone();
	            jQuery('#section' + questionToMove).remove();
	            clone.appendTo('#sections ul#theSections');
	            clone.removeClass('inGroup');
	            clone.attr('data-groupid', 0);
	            jQuery('#wait').css('opacity',0);
	        }   
      	});
      }
      if(groupID != 0) {
      	jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_jclassroom&task=unit.moveQuestionToGroup",
			data: {questionToMove:questionToMove,groupID:groupID},
			//dataType: 'json',
			success: function( data ) {
	            jQuery('#moveQuestion').modal('hide');
	            var clone = jQuery('#section' + questionToMove).clone();
	            jQuery('#section' + questionToMove).remove();
	            clone.appendTo('#section' + groupID + ' ul');
	            clone.addClass('inGroup');
	            clone.attr('data-groupid', groupID);
	            jQuery('#wait').css('opacity',0);
	        }   
      	});
      } 
   	}
    function chooseElement(id) {
      	$('.modal-dialog .card').each(function() {
         	$(this).removeClass('selected');
      	});
      	$('#newSectionType').val(id);
      	$('#card' + id).addClass('selected');
   	}
</script>