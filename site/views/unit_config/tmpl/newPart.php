<style>
   @media(min-width:0px) AND (max-width: 575px) {
      .modal-dialog {
         max-width: 400px
      }
      .modal-dialog .card {
         height: 60px;
      }
      .modal-dialog .card a {
         text-align: center;
         padding: 10px 0;
      }
   }
   @media(min-width:576px) AND (max-width: 767px) {
      .modal-dialog {
         max-width: 700px
      }
      .modal-dialog .card {
         height: 80px;
      }
      .modal-dialog .card a {
         text-align: center;
         padding: 20px 0;
      }
   }
   @media(min-width:768px) AND (max-width: 1279px) {
      .modal-dialog {
         max-width: 90%;
      }
      .modal-dialog .card {
         height: 120px;
      }
      .modal-dialog .card a {
         text-align: center;
         padding: 20px 0;
      }
   }
   @media(min-width:1280px) AND (max-width: 5120px) {
      .modal-dialog {
         max-width: 900px
      }
      .modal-dialog .card {
         height: 160px;
      }
      .modal-dialog .card a {
         text-align: center;
         padding: 50px 0;
      }
   }
   .modal-dialog {
      margin: 0 auto;
   }
   .modal-dialog .card {
      cursor: pointer;
      margin-bottom: 10px;
   }
   .modal-dialog .card:hover,
   .modal-dialog .card.selected {
      background-color: #007bff;
      color: #ffffff!important;
   }
   .modal-dialog .card:hover i,
   .modal-dialog .card.selected i {
      color: #ffffff!important;
   }
   .modal-dialog .card i {
      color: #a1a1a1;
   }
</style>
<div id="newPart_dialog" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Neue Quizzfrage einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                  <div id="card1" class="card d-flex">
                     <a onclick="chooseElement(1);">
                        <i class="d-block fa fa-file-text-o" style="font-size: 24px;"></i>
                        Freitext
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                  <div id="card5" class="card d-flex">
                     <a onclick="chooseElement(5);">
                        <i class="d-block fa fa-question-circle" style="font-size: 24px;"></i>
                        Ja/Nein-Frage
                     </a>
                  </div>
               </div>
               <!--<div class="col-12 col-sm-3">
                  <div id="card6" class="card d-flex">
                     <a onclick="chooseElement(6);">
                        <i class="d-block fa fa-user" style="font-size: 24px;"></i>
                        Audit-Frage
                     </a>
                  </div>
               </div>-->
               <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                  <div id="card3" class="card d-flex">
                     <a onclick="chooseElement(3);">
                        <i class="d-block fa fa-superpowers" style="font-size: 24px;"></i>
                        WWM-Frage
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                  <div id="card7" class="card d-flex">
                     <a onclick="chooseElement(7);">
                        <i class="d-block fa fa-inbox" style="font-size: 24px;"></i>
                        Texteingabefeld mit Auswertung
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                  <div id="card10" class="card d-flex">
                     <a onclick="chooseElement(10);">
                        <i class="d-block fa fa-keyboard-o" style="font-size: 24px;"></i>
                        Texteingabefeld
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                  <div id="card11" class="card d-flex">
                     <a onclick="chooseElement(11);">
                        <i class="d-block fa fa-book" style="font-size: 24px;"></i>
                        Texteingabebereich
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                  <div id="card12" class="card d-flex">
                     <a onclick="chooseElement(12);">
                        <i class="d-block fa fa-list" style="font-size: 24px;"></i>
                        Auswahlliste
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                  <div id="card13" class="card d-flex">
                     <a onclick="chooseElement(13);">
                        <i class="d-block fa fa-check-square-o" style="font-size: 24px;"></i>
                        Checkboxen
                     </a>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                  <div id="card4" class="card d-flex">
                     <a onclick="chooseElement(4);">
                        <i class="d-block fa fa-smile-o" style="font-size: 24px;"></i>
                        Smileys
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button id="newSectionSave" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="isGroup" value="" />
            <input type="hidden" id="newSectionType" value="0" />
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">

</script>