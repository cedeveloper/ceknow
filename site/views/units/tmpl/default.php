<?php
/**
 * @author      
 * @copyright   
 * @license     
 */

defined("_JEXEC") or die("Restricted access");

// necessary libraries
JHtml::_('bootstrap.tooltip');

$doc = JFactory::getDocument();

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
$user = JFactory::getUser();
?>
<style>
    table tr {
        position: relative;
    }
    .hideme {
        position: absolute;
        background-color: rgba(255,255,255,0.95);
        height: calc(100% - 10px);
        top: 5px;
        left: 5px;
        display: none;
        color: red;
        font-size: 16px;
        font-weight: bold;
        font-style: italic;
        padding: 10px;
    }

</style>
<form action="index.php?option=com_jclassroom&task=units.loadJSON" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept="application/json" name="uploadJSON" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm">
<div class="auditum form-content">
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionboard mt-3 mb-3">
        <button type="button" class="btn btn-success" onclick="Joomla.submitform('unit.add')"><?php echo JText::_('Neues Quizz') ?></button>
        <?php if (isset($this->items[0]->published)) : ?>

        <?php elseif (isset($this->items[0])) : ?>
            <button type="button" class="btn btn-danger" onclick="Joomla.submitform('units.delete')"><?php echo JText::_('Quizz(e) löschen') ?></button>
        <?php endif; ?>
        <?php if (isset($this->items[0]->published)) : ?>
            <button id="deleteQuizz" type="button" class="btn btn-danger"><?php echo JText::_('Quizz(e) löschen') ?></button>
        <?php endif; ?>
        <?php if($this->usergroup != 'trainer'): ?>
        <button type="button" class="btn btn-warning text-dark"id="openUploader">Quizz Upload</button>
        <?php endif; ?>
        <a href="<?php echo $this->return;?>" class="btn btn-danger text-white float-right">Zurück</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="nowrap left" style="width: 50px;"></th>
                <th class="nowrap left" style="width: 50px;">
                    <?php echo JHtml::_('grid.sort', JText::_('ID'), 'a.id', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', JText::_('Name'), 'a.title', $listDirn, $listOrder) ?>
                </th>
                <th>Beschreibung</th>
                <?php if($this->usergroup == 'superuser'): ?>
                    <th>Kunde</th>
                <?php endif; ?>
                <th class="nowrap left" style="width: 200px;">
                    <?php echo JHtml::_('grid.sort', 'Erstellt von<br/>Erstellt am', 'a.created', $listDirn, $listOrder) ?>
                </th>
                <th width="200" class="text-center">Bearbeiten</th>
                <th class="text-center">Status</th>
            </tr>
        </thead>
        <tbody>
        <?php if($this->items) { ?>
            <?php foreach($this->items as $item) { ?>
                <?php 
                $hideTheContent = '';
                $hideEdit = '';
                if($this->usergroup == 'customer' || $this->usergroup == 'customeradmin'):
                    // show just to creator
                    if($item->showto == 0):
                        // if show quizz to individual users
                        $allowR = explode(',',$item->readRights);
                        if(in_array($user->id,$allowR)):
                            $hideTheContent = '';
                            $hideEdit       = 'disabled';
                        else:
                            $hideTheContent = 'hide';
                            $hideEdit       = 'disabled';
                        endif;
                        if($user->id == $item->created_by):
                            // if current user is creator, show item
                            $hideTheContent = '';
                        endif;
                        $allowE = explode(',',$item->writeRights);
                        if(in_array($user->id,$allowE)):
                            $hideEdit       = '';
                        else:
                            $hideEdit       = 'disabled';
                        endif;
                        if($user->id == $item->created_by):
                            // if current user is creator, show item
                            $hideEdit = '';
                        endif;
                    endif;
                    if($item->showto == 1):
                        if($item->created_by == $user->id):
                            // if current user is creator, show item
                            $hideTheContent = '';
                        endif;
                    endif;
                    if($item->showto == 1):
                        if($item->created_by != $user->id):
                            // if current user is not creator, show item if creator is a trainer of customer
                            $groups = JAccess::getGroupsByUser($item->created_by);
                            if(in_array(12,$groups) || in_array(13,$groups)):
                                // if creator is another admin of customer and 
                                $hideTheContent = 'hide';
                            else:
                                $hideTheContent = '';
                            endif;
                        endif;
                    endif;
                    if($item->showto == 3):
                        if($user->id != $item->created_by):
                            // if current user is creator, show item
                            $hideTheContent = '';
                        endif;
                    endif;
                    if($item->editto == 1):
                        if($user->id != $item->created_by):
                            $hideEdit = 'disabled';
                        endif;
                    endif;
                endif;
                if($this->usergroup == 'trainer'):
                    if($item->showto == 0):
                        $hideTheContent = 'hide';
                        $allowR = explode(',',$item->readRights);
                        if(in_array($user->id,$allowR)):
                            $hideTheContent = '';
                            $hideEdit       = 'disabled';
                        else:
                            $hideTheContent = 'hide';
                            $hideEdit       = 'disabled';
                        endif;
                        if($user->id == $item->created_by):
                            // if current user is creator, show item
                            $hideTheContent = '';
                        endif;
                        $allowE = explode(',',$item->writeRights);
                        if(in_array($user->id,$allowE)):
                            $hideEdit       = '';
                        else:
                            $hideEdit       = 'disabled';
                        endif;
                        if($user->id == $item->created_by):
                            // if current user is creator, show item
                            $hideEdit = '';
                        endif; 
                    endif;
                    if($item->showto == 1):
                        if($item->created_by == $user->id):
                            // if current user is creator, show item
                            $hideTheContent = '';
                        else:
                            $hideTheContent = 'hide';
                        endif;
                    endif;
                    if($item->editto == 0):
                        $hideEdit = 'disabled';
                        $allowR = explode(',',$item->readRights);
                        if(in_array($user->id,$allowR)):
                            $hideTheContent = '';
                            $hideEdit       = 'disabled';
                        else:
                            $hideTheContent = 'hide';
                            $hideEdit       = 'disabled';
                        endif;
                        if($user->id == $item->created_by):
                            // if current user is creator, show item
                            $hideTheContent = '';
                        endif;
                        $allowE = explode(',',$item->writeRights);
                        if(in_array($user->id,$allowE)):
                            $hideEdit       = '';
                        else:
                            $hideEdit       = 'disabled';
                        endif;
                        if($user->id == $item->created_by):
                            // if current user is creator, show item
                            $hideEdit = '';
                        endif; 
                    endif;
                    if($item->editto == 1):
                        if($item->created_by == $user->id):
                            $hideEdit = '';
                        else:
                            $hideEdit = 'disabled';
                        endif;
                    endif;
                    if($item->editto == 3):
                        $hideEdit = '';
                    endif;
                endif;
                ?>
                <tr class="row<?php echo $i % 2; ?> <?php echo $hideTheContent;?>">
                    <td class="cid" data-id="<?php echo $item->id;?>" style="position: relative;">
                        <?php 
                            echo JHtml::_('grid.id', $i, $item->id);
                            if(($this->usergroup != 'superuser' && $this->usergroup != 'customer') && $item->showto == 1 && $item->created_by != $user->id):
                                echo '<div style="display: block;" class="hideme">Nur für Urheber sichtbar</div>';
                            elseif(($this->usergroup != 'superuser' && $this->usergroup != 'customer') && $item->editto == 0):
                                
                                if($item->readRights):
                                    $users = explode(',', $item->readRights);
                                    $showHideMe = 1;
                                    foreach($users as $setFree):
                                        if($user->id == $setFree):
                                            $showHideMe = 0;
                                        endif;
                                    endforeach;
                                    if($showHideMe == 1):
                                        echo '<div style="display: block;" class="hideme">Nur für Urheber sichtbar</div>';
                                    endif;
                                else:
                                    echo '<div style="display: block;" class="hideme">Nur für Urheber sichtbar</div>';
                                endif;
                            endif;
                        ?>
                    </td>
                    <td><?php echo $item->id;?></td>
                    <td>
                        <?php
                            echo '<i class="fa fa-users text-success" title="Freigegebenes Quizz. Für alle Administratoren und Trainer zu bearbeiten."></i> ';
                            echo '<a class="link '.$hideEdit.'" href="quizz-edit?layout=edit&id='.$item->id.'">
                            '.$item->title.'</a>';
                        ?>
                        
                    </td>
                    <td>
                        <?php 
                            echo $item->description;
                        ?>
                    </td>
                    <?php if($this->usergroup == 'superuser'): ?>
                        <td><?php echo $item->company_name;?></td>
                    <?php endif; ?>
                    <td>
                        <?php
                        echo $item->author_name.'<br/>'.date('d.m.Y H:i', strtotime($item->created));
                        ?>
                    </td>
                    <td>
                        <?php
                        if(($this->usergroup != 'superuser' && $this->usergroup != 'customer') && $item->editto == 1 && $item->created_by != $user->id):
                        elseif(($this->usergroup != 'superuser' && $this->usergroup != 'customer') && $item->editto == 0):
                            if($item->writeRights):
                                $users = explode(',', $item->writeRights);
                                $showHideMe = 1;
                                foreach($users as $setFree):
                                    if($user->id == $setFree):
                                        $showHideMe = 0;
                                    endif;
                                endforeach;
                                if($showHideMe == 0):
                                    echo '<a href="section-config?id='.$item->id.'" class="btn btn-success btn-sm w-100 mb-1 '.$hideEdit.'"><i class="fa fa-gears"></i> Konfigurieren</a>';
                                    echo '<a onclick="return confirm(&quot;Möchten Sie dieses Quizz kopieren?&quot;)" href="index.php?option=com_jclassroom&task=unit.copyQuizz&id='.$item->id.'" class="btn btn-danger btn-sm w-100 mb-1 '.$hideEdit.'"><i class="fa fa-copy"></i> Quizz kopieren</a>';
                                endif;
                            endif;
                        else:
                            echo '<a href="section-config?id='.$item->id.'" class="btn btn-success btn-sm w-100 mb-1 '.$hideEdit.'"><i class="fa fa-gears"></i> Konfigurieren</a>';
                            echo '<a onclick="return confirm(&quot;Möchten Sie dieses Quizz kopieren?&quot;)" href="index.php?option=com_jclassroom&task=unit.copyQuizz&id='.$item->id.'" class="btn btn-danger btn-sm w-100 mb-1 '.$hideEdit.'"><i class="fa fa-copy"></i> Quizz kopieren</a>';
                        endif;
                        ?>
                        <?php if($this->usergroup != 'trainer'): ?>
                            <a onclick="exportQuizz(<?php echo $item->id;?>);" class="btn btn-warning btn-sm w-100 mb-1 <?php echo $hideEdit;?>"><i class="fa fa-download"></i> Quizz exportieren</a>
                        <?php endif; ?>
                        <a  href="stage?sT=quizz&qID=<?php echo $item->id;?>&q=-1&preview=1" target="_blank" class="btn btn-primary btn-sm w-100 <?php echo $hideEdit;?>"><i class="fa fa-eye"></i> Quizz Vorschau</a>
                    </td>
                    <td class="text-center">
                    <?php 
                    if($item->created_by != 0):
                        switch($item->published) { 
                            case 1:
                                echo '<h1 class="badge badge-success m-0"><i class="fa fa-check"></i></h1>';
                                break;
                            case 0:
                                echo '<h1 class="badge badge-danger m-0"><i class="fa fa-ban"></i></h1>';
                                break;
                            case 2:
                                echo '<h1 class="badge badge-primary m-0"><i class="fa fa-archive"></i></h1>';
                                break;
                            case -2:
                                echo '<h1 class="badge badge-warning m-0"><i class="fa fa-trash"></i></h1>';
                                break;
                        } 
                    else:
                        echo '<h1 class="badge badge-primary m-0"><i title="Vom Systemadministrator erstellt, nicht änderbar" class="fa fa-handshake-o"></i></h1>';
                    endif;
                    ?>
                    </td>
                </tr>
                <?php $i++;?>
            <?php } ?>
        <?php }  else { ?>
            <tr>
                <td colspan="8">
                    <p>Es wurden keine Quizze gefunden. Bitte überprüfen Sie die Filtereinstellungen</p>
                </td>
            </tr>
        </div>
        <?php } ?>
        </tbody>
    </table>
    <?php echo $this->pagination->getPagesLinks(); ?>
</div>
<input type="hidden" id="usergroup" value="<?php echo $this->usergroup;?>" />
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="theDialog" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title"><i class="fa fa-exclamation-circle text-danger"></i> Löschkonflikte</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p id="dialogMessage"></p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var usergroup = $('#usergroup').val();
        if(usergroup != 'superuser') {
            $('#filter_customerID').css('display', 'none');
            $('#filter_customerID').next('.select2').css('display', 'none');
        }
        var trW = $('table tr').width();
        trW = trW - 10;
        $('.hideme').each(function() {
            $(this).css('width', trW + 'px');
        });
        $('tr.hide').each(function() {
            var id = $(this).attr('data-id');
            $(this).empty();
            $(this).append('<td colspan="7" class="text-danger" style="font-weight: bold;">Nur für Urheber sichtbar (Kein Leserecht)</td>');
        });
        $('a.link.disabled').each(function() {
            $(this).removeAttr('href', '');
        });
    });

    jQuery('#openUploader').click(function() {
        jQuery('#uploader').trigger('click');
        
    });
    jQuery('#uploader').on('change', function() {
        jQuery('#uploaderForm').submit();
    });
    function exportQuizz(id) {
        if(confirm("Soll das Quizz mit der ID " + id + " als JSON-Datei exportiert werden?") == true) {
            jQuery.ajax({
                url: "index.php?option=com_jclassroom&task=units.export",
                data: {id:id},
                method: 'GET',
                xhrFields: {
                    responseType: 'text'
                },
                //dataType: 'json',
                success: function( data ) {
                    var element = document.createElement('a');
                    //element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                    element.setAttribute('href', data);
                    element.setAttribute('download', 'quizz' + id + '.json');
                    element.style.display = 'none';
                    document.body.appendChild(element);

                    element.click();

                    document.body.removeChild(element);
                }
            });
        }
    }
    $('#deleteQuizz').on('click', function() {
        var cPos = 0;
        $('input[name="cid[]"]').each(function() {
            if($(this).is(':checked')) {
                cPos++;
            }
        });
        if(cPos == 0) {
            alert('Bitte wählen Sie mindestens ein Quizz aus.');
            return false;
        }
        if(confirm("Sollen die ausgewählten Quizze gelöscht werden?") == true) {
            var cid     = $('.cid input');
            var html    = '';
            if(cid) {
                cid.each(function() {
                    if($(this).is(':checked')) {
                        var unitID = $(this).parent('td').attr('data-id');
                        jQuery.ajax({
                            url: "index.php?option=com_jclassroom&task=units.loadContent",
                            data: {unitID:unitID},
                            method: 'POST',
                            async: false,
                            //dataType: 'json',
                            success: function( data ) {
                                html += data;
                            }
                        });
                    }
                });
                if(html) {
                    html += '<p><i class="fa fa-exclamation-circle text-danger"></i> Bitte löschen Sie in den aufgelisteten Learningrooms zunächst das zu löschende Quizz und führen Sie im Anschluss den Löschprozess erneut aus.</p>';
                    showDialog(html);
                } else {
                    Joomla.submitform('units.delete');
                }
            }
        }
    });
    function showDialog(html) {
        $('#dialogMessage').html(html);
        $('#theDialog').modal('show');
    }
</script>