<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Kunden list view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewUnits extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $toolbar;

	public function display($tpl = null)
	{
		require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        
		$session = JFactory::getSession();
		$document 	= JFactory::getDocument();
		$document->setTitle('Quizze ['.$session->get('systemname').']');
		// Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
	        JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
	        $demo = new LoginHelper();
	        $demo->checkDemo();
	    endif;

		$app = JFactory::getApplication();

		$user = JFactory::getUser();
		$this->showChecklist = $showChecklist;
		$this->items 		 = $this->get('Items');
		$this->state 		 = $this->get('State');
		$this->pagination 	 = $this->get('Pagination');
		$this->user		 	 = JFactory::getUser();
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$user = JFactory::getUser();
        $groups = JAccess::getGroupsByUser($user->id);
        if(in_array(8,$groups)) {
            $this->return 		= 'dashboard';
            $this->usergroup 	= 'superuser';
        }
        if(in_array(11,$groups)) {
            $this->return = 'celearning/classrooms';
        }
        if(in_array(12,$groups)) {
            $this->return 		= 'manager-customer';
            $this->usergroup 	= 'customer';
        }
        if(in_array(13,$groups)) {
            $this->return 		= 'manager-customer';
            $this->usergroup 	= 'customer';
        }
        if(in_array(10,$groups)) {
            $this->return 		= 'manager-trainer';
            $this->trainer 		= 1;
            $this->usergroup 	= 'trainer';
        }
		if($session->get('customerID') && $session->get('customerID') != 0):
			$this->customer = 1;
		else:
			$this->customer = 0;
		endif;
		parent::display($tpl);
	}
}
?>