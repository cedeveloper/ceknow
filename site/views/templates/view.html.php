<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewTemplates extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;
	protected $toolbar;

    public function display($tpl = null) {
    	$session 	= JFactory::getSession();
		$document 	= JFactory::getDocument();
		$document->setTitle('Templates ['.$session->get('systemname').']');
		// Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
	        JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
	        $demo = new LoginHelper();
	        $demo->checkDemo();
	    endif;

		$this->items 		 = $this->get('Items');
		$this->state 		 = $this->get('State');
		$this->pagination 	 = $this->get('Pagination');
		$this->user		 	 = JFactory::getUser();
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$session 			 = JFactory::getSession();

		if($session->get('group') == 'superuser') {
            $this->return = 'manager-administrator';
        }
        if($session->get('group') == 'customer') {
            $this->return = 'manager-customer';
        }
        if($session->get('group') == 'trainer') {
            $this->return = 'manager-trainer';
        }
        
        parent::display($tpl);
    }
}
