<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;

?>
<form action="index.php?option=com_jclassroom&task=students.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept=".csv" name="uploadCSV" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <a class="btn btn-success text-white" onclick="Joomla.submitform('template.add')">Neues Template</a>
        <a class="btn btn-danger text-white m-1" id="deleteTemplates">Template(s) löschen</a>
        <a href="<?php echo $this->return;?>" class="btn btn-danger text-white float-right">Zurück</a>
    </div>
    <table class="table table-striped">	
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Titel', 'a.title', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Typ', 'a.type', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Beschreibung', 'a.description', $listDirn, $listOrder) ?>
                </th>  
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Erstellt von<br/>Erstellt am', 'a.created', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Geändert von<br/>Geändert am', 'a.modified', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Status'), 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>		
        <tbody>
        <?php if($this->items) { ?>
        <?php foreach ($this->items as $i => $item) :
        ?>
            <tr class="row<?php echo $i % 2; ?>">
            	<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
				<td class="right"><?php echo $this->escape($item->id); ?></td>
                <td class="left">
                    <a href="template-edit?id=<?php echo $item->id;?>">
                    <?php echo $this->escape($item->title); ?>
                    </a>    
                </td>
                <td>
                    <?php 
                    switch($item->type):
                        case 1:
                            $type = '<span class="badge text-white" style="background-color: orchid;"><i class="fa fa-birthday-cake"></i> Einladung</span>';
                            break;
                        case 2:
                            $type = '<span class="badge text-white" style="background-color: cornflowerblue;"><i class="fa fa-check"></i> Verifizierung</span>';
                            break;
                        case 3: 
                            $type = '<span class="badge text-white" style="background-color: lightseagreen;"><i class="fa fa-lightbulb-o"></i> Erinnerung</span>';
                            break;
                        case 4: 
                            $type = '<span class="badge text-white" style="background-color: burlywood;"><i class="fa fa-certificate"></i> Zertifikat</span>';
                            break;
                        case 5:
                            $type = '<span class="badge text-white" style="background-color: darkkhaki;"><i class="fa fa-question-circle"></i> Passwort vergessen</span>';
                            break;
                        case 6:
                            $type = '<span class="badge text-white" style="background-color: coral;"><i class="fa fa-sitemap"></i> Planänderung</span>';
                            break;
                        case 7:
                            $type = '<span class="badge text-white" style="background-color: firebrick;"><i class="fa fa-smile-o"></i> Demozugang</span>';
                            break;
                    endswitch;
                        echo $type;
                    ?>
                </td>
                <td>
                    <?php echo substr($item->description, 0,150);?>
                </td>
                <td>
                    <?php echo date('d.m.Y H:i', strtotime($item->created));?><br/>
                    <?php echo $item->created_by_name;?>
                </td>
                <td>
                    <?php 
                    if($item->modified != '0000-00-00 00:00:00'):
                        echo date('d.m.Y H:i', strtotime($item->modified));
                        echo '<br/>'.$item->modified_by_name;
                    endif;
                    ?>
                </td>
                <?php
                if($item->published == 0) {
					$class="badge-danger";
					$wert = "<i class='fa fa-ban'></i>";
				} elseif($item->published == 1) {
					$class="badge-success";
					$wert = "<i class='fa fa-check'></i>";
                } elseif($item->published == 2) {
                    $class="badge-primary";
                    $wert = "<i class='fa fa-file-o'></i>";
				}
				?>
                <td class="text-center">
                	<span class="btn <?php echo $class;?>"><?php echo $wert;?></span>
               	</td>
            </tr>
        <?php endforeach ?>
        <?php } else { ?>
        	<tr>
            	<td colspan="8">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
       	<?php } ?>
        </tbody>			
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
jQuery('#deleteTemplates').on('click', function() {
    if(confirm('Sollen die gewählten Templates gelöscht werden?') == true) {
        Joomla.submitform('templates.delete');
    }
});
jQuery('#openUploader').click(function() {
    jQuery('#uploader').trigger('click');
    
});
jQuery('#uploader').on('change', function() {
    jQuery('#uploaderForm').submit();
});
</script>