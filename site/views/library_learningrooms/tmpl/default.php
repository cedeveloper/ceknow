<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
$user   = JFactory::getUser();
?>
<style>
    .teaser {
        background-color: #ff3600;
        color: #fff;
        position: absolute;
        transform: rotate(45deg);
        text-align: center;
        padding: 0px 15px;
        font-size: 11px;
        top: 12px;
        right: -29px;
    }
    .ownteaser {
        background-color: cornflowerblue;
        color: #fff;
        position: absolute;
        transform: rotate(45deg);
        text-align: center;
        padding: 0px 15px;
        font-size: 14px;
        top: 7px;
        right: -32px;
        width: 120px;
    }
    .card-link {
        color: #007bff!important;
        cursor: pointer;
    }
    .card-header {
        max-height: 220px;
        overflow-y: scroll;
    }
    .price {
        position: absolute;
        top: 20px;
        background-color: #ff3600;
        padding: 4px 10px;
        font-weight: bold;
        border-radius: 4px;
        color: #ffffff;
        font-size: 24px;
        border: 0.5px solid #ffffff;
    }
    .imageContainer {
        height: 240px;
        background-position: center center;
        background-size: cover;
    }
</style>
<form action="index.php?option=com_jclassroom&task=students.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept=".csv" name="uploadCSV" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste d-inline-block w-100 mt-1 mb-1">
        <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right mb-3">Zurück</a>
    </div>
    <?php if($this->items) { ?>
        <div id="cards" style="margin-left: -15px;margin-right: -15px;">
            <div class="row">
            <?php foreach ($this->items as $i => $item) : ?>
                <div id="template<?php echo $item->id;?>" class="col-12 col-sm-6 col-md-3">
                    <div class="card mb-2" style="overflow: hidden;">
                        <?php 
                        if($item->title_image): 
                            $image = JURI::Root().$item->title_image;
                        else: 
                            $image = JURI::Root().'images/jclassroom/ceknow-learningroom.jpg';
                        endif;
                        ?>
                        <div class="imageContainer" style="background-image: URL('<?php echo $image;?>');"></div>
                        <div class="card-header" style="min-height: 220px;">
                            <?php
                            //if($item->customerID == 0):
                            echo $item->teaser;
                            //endif;
                            if($item->customerID == 0 && $item->freeLR == 0):
                                echo '<span class="price">'.number_format($item->price,2,',','.') .'€</span>';
                            endif;
                            ?>
                            <h5 class="card-title" style="padding-top: 20px;padding-right: 25px;">
                                <!--<a href="<?php //echo JURI::Root().'classroom-edit?layout=global&id='. $item->id.'&noS&lib=1'; ?>">
                                <?php echo $item->title;?>
                                </a>-->
                                <?php echo $item->title;?>
                            </h5>
                            <p class="card-text" style="font-size: 12px;line-height: 16px;"><?php echo $item->title_description;?></p>
                        </div>
                        <div class="card-body">
                            <?php if($this->usergroup != 'superuser' && $item->customerID == 0 && $item->freeLR == 0): ?>
                                <p class="mb-1"><a title="Erstellen Sie aus dieser Vorlage einen neuen Learningroom und passen Sie diesen an Ihre Wünsche an." onclick="createQuizz(<?php echo $item->id;?>);" class="w-100 btn btn-success text-white btn-sm">Learningroom in den Warenkorb legen</a></p>
                            <?php else: ?>
                                <a title="Erstellen Sie aus dieser Vorlage einen neuen Learningroom und passen Sie diesen an Ihre Wünsche an." onclick="createLearningroom(<?php echo $item->id;?>);" class="btn btn-primary text-white btn-sm w-100 mb-1">Learningroom aus Vorlage erstellen</a>
                            <?php endif;?>
                            <?php if($this->usergroup == 'superuser' || $item->created_by == $user->id):?>
                                <a onclick="removeTemplate(<?php echo $item->id;?>);" class="btn btn-danger text-white btn-sm w-100 mb-1">Löschen</a>
                            <?php endif;?>
                            <?php if($this->usergroup == 'superuser'):?>
                                <a href="library-learningroom-edit?id=<?php echo $item->id;?>" class="btn btn-success text-white btn-sm w-100">Bearbeiten</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            </div>
        </div>
    <?php } else { ?>
        <p>Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</p>
   	<?php } ?>			
    <?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
function createLearningroom(id) {
    if(confirm('Möchten Sie aus der Vorlage ' + id + ' einen neuen Learningroom erstellen?') == true) {
        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=library_learningrooms.createLearningroom",
            data: {id:id},
            //dataType: 'json',
            success: function( data ) {
               alert('Die Vorlage ' + id + ' wurde erfolgreich als Learningroom ' + data + ' gespeichert.');
            }
         });
    }
}
function removeTemplate(id) {
    if(confirm('Möchten Sie die Vorlage ' + id + ' entgültig löschen?') == true) {
        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=library_learningrooms.removeTemplate",
            data: {id:id},
            //dataType: 'json',
            success: function( data ) {
               $('#template' + id).remove();
            }
         });
    }
}
</script>