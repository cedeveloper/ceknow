<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

   <div class="buttonleiste d-inline-block w-100 mb-4">   
      <a class="btn btn-success text-white" onclick="Joomla.submitform('library_learningroom.simpleSave')">Speichern</a>
      <a class="btn btn-secondary text-white" onclick="Joomla.submitform('library_learningroom.save')">Speichern & Schließen</a>
      <a href="library-learningrooms" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('title'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('title'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('title_description'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('title_description'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('title_image'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('title_image'); ?>
            <?php if($this->item->title_image): ?>
               <img src="<?php echo JURI::Root().$this->item->title_image;?>" />
            <?php endif; ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('freeLR'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('freeLR'); ?>
         </div>
      </div>
      <div class="form-group row">
         <div class="col-12 col-sm-2 col-form-label">
            <?php echo $this->form->getLabel('price'); ?>
         </div>
         <div class="col-12 col-sm-10">
            <?php echo $this->form->getInput('price'); ?>
         </div>
      </div>
   </div>
   <div>
      <?php echo $this->unit;?>
   </div>

<input type="hidden" id="id" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<script type="text/javascript">
   
</script>