<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
 
/**
 * Kunde item view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewEvaluate_quizz extends JViewLegacy
{
	protected $item;
	protected $form;
	protected $state;

	public function display($tpl = null)
	{
		require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();

		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		
		$fh = file_get_contents(JPATH_COMPONENT.'/library/contents/publish_quizz.txt');
		$this->description = $fh;
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour 	= 'evaluate-quizze';
				break;
			case 'customer':
				$this->retour 	= 'evaluate-quizze';
				break;
			case 'trainer':
				$this->retour 	= 'evaluate-quizze';
				break;
		}
		parent::display($tpl);
	}
}
?>