<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$dokument = JFactory::getDocument();
$dokument->addScript('components/com_jclassroom/assets/js/jquery.validate.js' );
$dokument->addScript('components/com_jclassroom/assets/js/stage.js');

$now = new DateTime();
?>
<form action="<?php JRoute::_('index.php?option=com_jclassroom&task=unit.checkQuestion'); ?>" method="post" name="adminForm" id="adminForm">
	<div class="stage row">
        <div class="col-12">
        	<h1>Unit: <i><?php echo $this->unit['title']?></i></h1>
    		<p class=" text-info"><?php echo $now->format('d.m.Y H:i');?></p>
      	</div>
       	<div class="col-12">
       		<div class="row">
                <div class="col-12 "> 
                    <div class="card">
                        <div class="card-body">
                            <h2>Sektion <?php echo $this->unit['section']->sectionID + 1;?></h2>
                            <?php
                            if($this->unit['section']->sectionType == 2) {
                                echo $this->unit['section']->content;
                            } else {
                        	   echo '<h3 class="sectionTitle">'.$this->unit['section']->content.'</h3>';
                            }
                            ?>   
                            <?php
                                if($this->unit['section']->sectionType == 5):
                                    echo '<a class="btn btn-danger text-white mr-1">Nein</a>';
                                    echo '<a class="btn btn-success text-white">Ja</a>';
                                endif;
                                if($this->unit['section']->sectionType == 3):
                                    if($this->unit['section']->answers) {
                                        echo '<div class="answerBody">';
                                        $btn = 'btn-light';
                                        foreach($this->unit['section']->answers as $key => $answer) {
                                            echo '<a id="answer'.$key.'" class="theAnswer w-100 mt-1 mb-2 btn '.$btn.'">'.html_entity_decode($answer->answer).'</a>';
                                            //echo '<input type="hidden" id="hiddenAnswer'.$key.'" class="hiddenAnswer" value="'.$answer['answer'].'" />';
                                        }
                                        echo '</div>';
                                    }
                                endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer mt-3">
                <?php if($this->unit['section']->sectionID == 0) { ?>
                     <a class="btn bg-danger btn-lg text-white" href="index.php?option=com_auditmaker&task=unit.stop">Audit beenden</a>
                <?php } else { ?>
                    <a class="btn bg-warning btn-lg text-white" href="index.php?option=com_auditmaker&task=unit.prevF&id=<?php echo $this->unit['unitID'];?>&q=<?php echo $this->unit['section']->sectionID;?>">Zurück<a/>
                    <a onclick="return confirm('Möchten Sie das Audit unterbrechen?')" class="btn bg-danger btn-lg text-white" href="index.php?option=com_auditmaker&task=audit.storno">Audit unterbrechen</a>
                <?php } ?>
                <a id="check" class="float-right btn btn-success btn-lg text-white">Weiter</a>
            </div>
        </div>
    </div>
<input type="hidden" id="unitID" value="<?php echo $this->unit['unitID'];?>" />
<input type="hidden" id="sectionID" value="<?php echo $this->unit['section']->sectionID;?>" />
<input type="hidden" id="sectionType" value="<?php echo $this->unit['section']->sectionType;?>" />
<input type="hidden" id="position" value="<?php echo $this->unit['section']->sectionID;?>" />
<?php echo JHtml::_('form.token'); ?>
</form>