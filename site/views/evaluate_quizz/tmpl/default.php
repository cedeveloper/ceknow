<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');

$dokument = JFactory::getDocument();
$dokument->addScript('components/com_jclassroom/assets/js/jquery.validate.js' );

?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&task=publish_quizz.publishQuizz'); ?>" method="post" name="adminForm" id="adminForm">
	<div class="content row">
        <div class="col-12">
            <?php echo $this->description;?>
      	</div>
       	<div class="col-12">
            <div class="form-horizontal mt-3">
                <div class="form-group row">
                   <div class="col-12 col-sm-2 col-form-label">
                      1. Wählen Sie ein Quizz aus
                   </div>
                   <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('chooseQuizz'); ?>
                   </div>
                </div>
                <div class="form-group row">
                   <div class="col-12 col-sm-2 col-form-label">
                      ... oder...
                   </div>
                </div>
                <div class="form-group row">
                   <div class="col-12 col-sm-2 col-form-label">
                      2. Erstellen Sie ein neues Quizz
                   </div>
                   <div class="col-12 col-sm-10">
                      <a href="" class="btn btn-primary">Neues Quizz erstellen</a>
                   </div>
                </div>
                <div class="form-group row">
                   <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('title'); ?>
                   </div>
                   <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('title'); ?>
                   </div>
                </div>
                <div class="form-group row">
                   <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('description'); ?>
                   </div>
                   <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('description'); ?>
                   </div>
                </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('publish_up'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('publish_up'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('publish_down'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('publish_down'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('published'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('published'); ?>
                  </div>
               </div>
                <div class="form-group row">
                   <div class="col-12 col-sm-10">
                      <a onClick="saveStageQuizz();" class="btn btn-success">Quizz veröffentlichen</a>
                      <a href="stage?layout=edit&unitID=11&clr=&q=0" class="btn btn-danger">Stage Quizz</a>
                   </div>
                </div>
            </div>
        </div>
    </div>
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
    function saveStageQuizz() {
        if(confirm("Soll das Quizz veröffentlicht werden?") == true) {
            $('#adminForm').submit();
        }
    }
</script>