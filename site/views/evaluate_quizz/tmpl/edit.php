<?php
/**
 * @author     
 * @copyright  
 * @license    
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');

$dokument = JFactory::getDocument();
$dokument->addScript('components/com_jclassroom/assets/js/jquery.validate.js' );
$dokument->addScript('components/com_jclassroom/assets/js/jqcloud-1.0.4.js');
$dokument->addScript('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js');
require_once(JPATH_COMPONENT.'/views/stage/tmpl/message.php');
?>
<style>
   .resultContainer {
      background-color: #e1e1e1;
      padding: 15px;
      margin-bottom: 10px;
   }
</style>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&task=publish_quizz.publishQuizz'); ?>" method="post" name="adminForm" id="adminForm">
   <div class="buttonleiste d-inline-block w-100 mb-4"> 
      <?php if($this->item->quizzType == 1): ?>
         <a href="index.php?option=com_jclassroom&task=evaluate_quizz.printQuizz&rID=<?php echo $this->item->id;?>" target="_blank" class="btn btn-primary text-white">Quizzbericht erstellen</a>
      <?php endif;?>
      <?php if($this->item->quizzType == 2): ?>
         <a href="index.php?option=com_jclassroom&task=evaluate_quizz.printUmfrage&rID=<?php echo $this->item->id;?>" target="_blank" class="btn btn-primary text-white">Umfragebericht erstellen</a>
      <?php endif;?>
      <?php if($this->item->quizzType == 3): ?>
         <a href="index.php?option=com_jclassroom&task=evaluate_quizz.printAudit&rID=<?php echo $this->item->id;?>" target="_blank" class="btn btn-primary text-white">Auditbericht erstellen</a>
      <?php endif;?>
      <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right m-1">Zurück</a>
   </div>
   <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
         <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Ergebnis</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="vorwort-tab" data-toggle="tab" href="#vorwort" role="tab" aria-controls="vorwort" aria-selected="false">Vorwort</a>
      </li>
   </ul>
   <div class="tab-content" id="tabContent">
      <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
         <div class="form-horizontal mt-3 w-50">
            <h2>Auswertung für Quizz <b><?php echo $this->item->title;?></b></h2>
            <div class="card bg-dark p-2 text-white mb-3">
               <table class="table table-sm text-white mb-0">
                  <tbody>
                     <tr>
                        <td>Quizz-ID:</td>
                        <td><?php echo 'QID'.str_pad($this->item->quizzID,6,'0',STR_PAD_LEFT);?></td>
                     </tr>
                     <tr>
                        <td>Veröffentlichung-ID:</td>
                        <td><?php echo 'PID'.str_pad($this->item->id,6,'0',STR_PAD_LEFT);?></td>
                     </tr>
                     <tr>
                        <td>Zu Learningroom:</td>
                        <td>
                           <?php 
                           if($this->item->classroomID):
                              echo 'LR'.str_pad($this->item->classroomID,8,'0',STR_PAD_LEFT);
                           endif;
                           ?>
                        </td>
                     </tr>
                     <tr>
                        <td>Art des Quizzes:</td>
                        <td>
                           <?php
                           switch($this->item->quizzType):
                              case 1:
                                 echo 'Quizz';
                                 break;
                              case 2:
                                 echo 'Umfrage';
                                 break;
                              case 3:
                                 echo 'Audit';
                                 break;
                           endswitch;
                           ?>
                        </td>
                     </tr>
                     <tr>
                        <td>Für Firma:</td>
                        <td><?php echo $this->item->companyName;?></td>
                     </tr>
                     <tr>
                        <td>Anzahl Teilnehmer:</td>
                        <td><?php echo $this->item->participations;?></td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         <div id="theResult">
            <?php echo $this->item->result;?>
         </div>
      </div>
      <div class="tab-pane fade show" id="vorwort" role="tabpanel" aria-labelledby="vorwort-tab">
         <div class="form-horizontal mt-3 w-100">
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('vorwort'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('vorwort'); ?>
               </div>
            </div>
            <a onclick="savePreface();" class="btn btn-primary text-white">Speichern</a>
         </div>
      </div>
   </div>
<?php echo JHtml::_('form.token'); ?>
<input type="hidden" id="publishedQuizzID" value="<?php echo $this->item->id;?>" />
</form>
<script type="text/javascript">
$(document).ready(function() {
   completeResultQuizz();
   
});
function saveTheImage() {
   var publishedQuizzID = $('#publishedQuizzID').val();
   $('canvas').each(function() {
      var id = $(this).attr('id');
      var canvas = document.getElementById(id);
      var dataURL = canvas.toDataURL();
      saveImage(publishedQuizzID, dataURL, $(this).attr('id'));
   });
}
function completeResultQuizz() {
   var publishedQuizzID = $('#publishedQuizzID').val();
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=evaluate_quizz.loadQuizzCompleteResult",
      data: {publishedQuizzID:publishedQuizzID},
      dataType: 'json',
      async: false,
      success: function( data ) {
         jQuery.each(data, function(e, value) {
            if(value['chart'] != 'wordCloud') {        
               if(value['chart'] != '') {
                  var questionID = value['questionID'];
                  var canvasStyle   = 'canvasChartH';
                  var canvasHeight  = '300';
                  if(value['chart'] == 'bar') {
                     canvasStyle    = 'canvasChartV';
                     canvasHeight   = '500';
                  }
                  $('#theModalChartDiv' + questionID).css('height', canvasHeight);
                  writeChart(value);
               } else {
                 
               }
            } else {
               jQuery('#resultQuizz .modal-body').append(
                  '<p style="margin-top: 20px;font-size: 18px; font-weight: bold;">Frage:<i>' + value['question'] + '</i></p>' +
                  '<div id="theCloud" height="200" width="400"></div>'
               );
               writeCloud(value['datas'],0);
            }
         });  
         setTimeout(function() { 
              saveTheImage()
         }, 2000);  
      }
   });
}
function writeCloud(data,i) {
   var myTags = [
      {text: "Lorem", weight: 13},
      {text: "Ipsum", weight: 10.5, html: {"class": "vertical"}},
      {text: "Dolor", weight: 9.4},
      {text: "Sit", weight: 8, html: {"class": "vertical"}},
      {text: "Amet", weight: 6.2},
      {text: "Consectetur", weight: 5}
   ];
   $("#theCloud").jQCloud(data, {
      width: 500,
      height: 350
   });
}
function writeChart(data) {
   var publishedQuizzID = $('#publishedQuizzID').val();
   var questionID = data['questionID'];
   var labels = new Array();
   jQuery.each(data['labels'], function(e, value) {
      labels.push(value);
   });
   var datas = new Array();
   jQuery.each(data['datas'], function(e, value) {
      datas.push(value);
   });
   var backgroundColor = [
      'rgba(255,0,0,1)',
      'rgba(30,144,255,1)',
      'rgba(255,240,0,1)',
      'rgba(34,139,34,1)',
      'rgba(199,21,133,1)',
      'rgba(32,178,170,1)',
      'rgba(205,92,92,1)',
      'rgba(135,206,235,1)',
      'rgba(255,127,80,1)',
      'rgba(0,250,127,1)'
   ]
   if(document.getElementById('theModalChart' + questionID)) {
      var ctx     = document.getElementById('theModalChart' + questionID).getContext("2d");
   
      if(data['chart'] == 'bar' || data['chart'] == 'horizontalBar') {
         var myChart = new Chart(ctx, {
            type: data['chart'],
            data: {
               labels: labels,
               datasets: [{
                  data: datas,
                  backgroundColor: backgroundColor
               }]
            },
            options: {
               maintainAspectRatio: false,
               legend: {
                  labels: {
                     defaultFontSize: 24
                  },
                  display: false
               },
               scales: {
                  yAxes: [{
                     labels: {
                        defaultFontSize: 24
                     },
                     ticks: {
                        fontSize: 20,
                        stepSize: 1,
                        beginAtZero: true
                     }
                  }],
                  xAxes: [{
                      ticks: {
                        fontSize: 20,
                        stepSize: 1,
                        beginAtZero: true
                      }
                  }]
               }
            }
         });
      }
      if(data['chart'] == 'pie') {
         var myChart = new Chart(ctx, {
            type: data['chart'],
            data: {
               labels: labels,
               datasets: [{
                  data: datas,
                  backgroundColor: backgroundColor
               }]
            },
            options: {
               maintainAspectRatio: false,
               legend: {
                  labels: {
                     fontSize: 24
                  }
               },
               scales: {
                  yAxes: [{
                     display: false,
                     gridLines: {
                        min: 0,
                        max: 20,
                        stepSize: 1,
                        display: false
                     }
                  }],
                  xAxes: [{
                     display: false,
                     gridLines: {
                        display: false
                     }
                  }]
               }
            }
         });
      }
   }
}
function saveImage(publishedQuizzID, image, id) {
   jQuery.ajax({
      type: "POST",
      url: "index.php?option=com_jclassroom&task=evaluate_quizz.saveChartImage",
      data: {publishedQuizzID:publishedQuizzID,image:image,id:id},
      //dataType: 'json',
      success: function( data ) {
         //console.log(data);
      }
   });
}
function savePreface() {
   var publishedQuizzID   = $('#publishedQuizzID').val();
   var content    = tinyMCE.get('jform_vorwort').getContent();
   jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=evaluate_quizz.savePreface",
         data: {publishedQuizzID:publishedQuizzID,content:content},
         //dataType: 'json',
         success: function( data ) {
            alert("Vorwort wurde erfolgreich gespeichert.");
         }
      });
}

</script>