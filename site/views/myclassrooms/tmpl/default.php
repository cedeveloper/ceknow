<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste d-inline-block w-100 mt-1 mb-1">
        <a href="dashboard-trainer" class="btn btn-danger text-white m-1 float-right">Zurück</a>
    </div>
    <table class="table table-striped">	
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                 <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Name<br/>Beginn am', 'a.title', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Haupttrainer', 'a.created_by', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Co-Trainer', 'a.city', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Teilnehmer', 'a.email', $listDirn, $listOrder) ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Erstellt am<br/>Von/Bis', 'a.created', $listDirn, $listOrder) ?>
                </th>
                <th></th> 
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', 'Status', 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>		
        <tbody>
        <?php if($this->items) { ?>
        <?php foreach ($this->items as $i => $item) :
        ?>
            <tr class="row<?php echo $i % 2; ?>">
            	<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
				<td class="right"><?php echo $this->escape($item->id); ?></td>
                <td>
                    <?php echo $item->title;?>
                </td>
                <td class="left">
                    <?php echo $this->escape($item->main_trainer); ?> 
                </td>
                <td class="left">
                    <?php echo $this->escape($item->co_trainer); ?>    
                </td>
                <td class="left">
                    
                </td>
                <td class="left">
                    <?php echo $this->escape($item->created); ?><br/>
                    <?php echo $this->escape($item->fromDate); ?> / <?php echo $this->escape($item->toDate); ?>        
                </td>
                <td class="text-center">
                    <a href="stage-classroom?id=<?php echo $item->id;?>&cel=1" class="btn btn-primary">Stage Learningroom</a>
                </td>
                <?php
                if($item->published == 0) {
					$class="badge-danger";
					$wert = "<i class='fa fa-ban'></i>";
				} elseif($item->published == 1) {
					$class="badge-success";
					$wert = "<i class='fa fa-check'></i>";
				}
				?>
                <td class="text-center">
                	<span class="btn <?php echo $class;?>"><?php echo $wert;?></span>
               	</td>
            </tr>
        <?php endforeach ?>
        <?php } else { ?>
        	<tr>
            	<td colspan="9">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
       	<?php } ?>
        </tbody>			
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
