<?php

/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewInvitation extends JViewLegacy {

    protected $item;
    protected $form;
    protected $state;

    public function display($tpl = null) {

        $this->state    = $this->get('State');
        $this->item     = $this->get('Item');
        $this->form     = $this->get('Form');
        $user = JFactory::getUser();
        $this->user     = JFactory::getUser();

   		$app = JFactory::getApplication();
    	$user = JFactory::getUser();
    	$groups = JAccess::getGroupsByUser($user->id);

    	if(in_array(8,$groups)) {
    		$this->role = 8;
    	}
    	if(in_array(10,$groups)) {
            $this->data = $this->item->text;
            $this->role = 10;
    	}

        parent::display($tpl);
    }
}
