<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <div class="buttonleiste mb-4">         
      <a class="btn btn-success text-white" onclick="Joomla.submitform('invitation.save')">Speichern</a>
      <a href="dashboard" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
         <a class="nav-link active" id="firmendaten-tab" data-toggle="tab" href="#firmendaten" role="tab" aria-controls="firmendaten" aria-selected="true">Hilfetext</a>
      </li>
   </ul>
   <div class="tab-content" id="tabContent">
      <div class="tab-pane fade show active" id="firmendaten" role="tabpanel" aria-labelledby="firmendaten-tab">
         <div class="form-horizontal mt-3">
            <div class="form-group row">
               <div class="col-12">
                  <?php if($this->role == 8): ?>
                     <?php echo $this->form->getInput('text'); ?>
                  <?php endif; ?>
                  <?php if($this->role == 10): ?>
                     <?php echo $this->data; ?>
                  <?php endif; ?>
               </div>
            </div>
         </div>
      </div>
   </div> 
<input type="hidden" name="id" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>