<div id="theText" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="top: 200px;max-width: 80%; width: 80%;height: 500px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Text bearbeiten</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <textarea id="theTextarea" class="summernote"></textarea>
         </div>
         <div class="modal-footer">
            <button id="saveTheText" type="button" class="btn btn-primary">Speichern</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>