<div id="newText" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 1000px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Freitext einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('freetext'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('freetext'); ?>
                  </div>
               </div>
         </div>
         <div class="modal-footer">
            <button onclick="saveFreetext();" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>