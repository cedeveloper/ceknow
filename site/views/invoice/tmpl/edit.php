<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
require_once(JPATH_COMPONENT.'/views/invoice/tmpl/newPosition.php');
require_once(JPATH_COMPONENT.'/views/invoice/tmpl/newText.php');
?>
<style>
   .position {
      background-color: #e1e1e1;
      margin: 2px 0;
   }
   .position.free {
      background-color: lightsteelblue;
      margin: 2px 0;
   }
</style>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

    <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a class="btn btn-success text-white" onclick="Joomla.submitform('invoice.simpleSave')">Speichern</a>
         <a class="btn btn-secondary text-white" onclick="Joomla.submitform('invoice.save')">Speichern & Schließen</a>
         <a class="btn btn-primary text-white" href="index.php?option=com_jclassroom&task=invoice.printPDF&id=<?php echo $this->item->id;?>" target="_blank"><i class="fa fa-print"></i> Drucken</a>
      <?php endif;?>
        <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="card p-2 mt-3" style="background-color: #f1f1f1;">
      <p><b>Kundenangaben</b></p>
      <div class="form-horizontal mt-3">
         <div class="row">
            <div class="col-12 col-sm-6">
               <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('customerID'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('customerID'); ?>
                    </div>
                </div>
               
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('name'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('address'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('address'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      PLZ/Ort
                    </div>
                    <div class="col-12 col-sm-2">
                      <?php echo $this->form->getInput('postcode'); ?>
                    </div>
                    <div class="col-12 col-sm-8">
                      <?php echo $this->form->getInput('city'); ?>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('email'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('email'); ?>
                    </div>
               </div>
               <a class="float-right" style="font-weight: bold; color: #007bff;" onclick="editCustomer();">Kunde bearbeiten</a>
            </div>
            <div class="col-12 col-sm-6">
               <div class="form-group row">
                  <div class="col-12 col-sm-4 col-form-label">
                     <?php echo $this->form->getLabel('invoice_number'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <?php echo $this->form->getInput('invoice_number'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-4 col-form-label">
                     <?php echo $this->form->getLabel('customer_number'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <?php echo $this->form->getInput('customer_number'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-4 col-form-label">
                     <?php echo $this->form->getLabel('invoice_date'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <?php echo $this->form->getInput('invoice_date'); ?>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-4">
                     <?php echo $this->form->getInput('performance_type'); ?>
                  </div>
                  <div class="col-12 col-sm-8">
                     <div id="performance1" class="row" style="display: none;">
                        <div class="col-12 col-sm-6">
                           <?php echo $this->form->getInput('performance_date1'); ?>
                        </div>
                        <div class="col-12 col-sm-6">
                           <?php echo $this->form->getInput('performance_date2'); ?>
                        </div>
                     </div>
                     <div id="performance2" class="row" style="display: none;">
                        <div class="col-12">
                           <?php echo $this->form->getInput('performance_date3'); ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="card p-2 mt-3" style="background-color: #f1f1f1;">
      <p><b>Kopfbereich</b></p>
      <div class="form-horizontal mt-3">
         <div class="row">
            <div class="col-12 col-sm-12">
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('invoice_title'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('invoice_title'); ?>
                     <!--<a class="float-right btn btn-primary btn-sm text-white mt-1">Titeltexte verwalten</a>-->
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('invoice_intro'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('invoice_intro'); ?>
                     <!--<a class="float-right btn btn-primary btn-sm text-white mt-1">Einleitungstexte verwalten</a>-->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="card p-2 mt-3">
      <p><b>Belegpositionen</b></p>
      <div class="card">
         <div class="card-header">
            <div class="row">
               <div class="col-12 col-sm-1"></div>
               <div class="col-12 col-sm-1"><b>Pos.</b></div>
               <div class="col-12 col-sm-4"><b>Produkt/Service</b></div>
               <div class="col-12 col-sm-1 text-center">Menge</div>
               <div class="col-12 col-sm-1 text-center">Einheit</div>
               <div class="col-12 col-sm-1 text-right">Preis (€)</div>
               <div class="col-12 col-sm-1 text-right">Steuer</div>
               <div class="col-12 col-sm-1 text-right">Rabatt (%)</div>
               <div class="col-12 col-sm-1 text-right">Gesamt</div>
            </div>
         </div>
         <div id="contacts" class="card-body p-0">
            <?php
            if($this->item->positions):
               $i = 1;
               $gp = 0;
               foreach($this->item->positions as $position):
                  $free = '';
                  if($position->planID == 0 && $position->amount == 0):
                     $free = 'free';
                  endif;
                  echo '<div id="position'.$position->id.'" class="'.$free.' position" style="padding: 8px 15px;">';
                  echo '<div class="row">';
                     echo '<div class="col-12 col-sm-1">';
                        echo '<i class="fa fa-ellipsis-v mr-2"></i>';
                        echo '<i onclick="editPosition('.$position->id.');" class="fa fa-pencil mr-2"></i>';
                        echo '<i onclick="deletePosition('.$position->id.');" class="fa fa-trash-o"></i>';
                     echo '</div>';
                     if($position->planID == 0 && $position->amount == 0):
                        echo '<div class="col-12 col-sm-11">';
                        echo $position->plan;
                        echo '</div>';
                     else:
                        echo '<div class="col-12 col-sm-1 number">'.$i.'</div>';
                        echo '<div class="col-12 col-sm-4">';
                        echo $position->plan;
                        echo '</div>';
                        echo '<div class="col-12 col-sm-1 text-center">';
                        echo $position->amount;
                        echo '</div>';
                        echo '<div class="col-12 col-sm-1 text-center">';
                        echo $position->unit;
                        echo '</div>';
                        echo '<div class="col-12 col-sm-1 text-right">';
                        echo number_format($position->ep,2,',','.').' €';
                        echo '</div>';
                        echo '<div class="col-12 col-sm-1 text-right">';
                        echo number_format($position->tax,0,',','.').'%';
                        echo '</div>';
                        echo '<div class="col-12 col-sm-1 text-right">';
                        echo number_format($position->discount,2,',','.').'%';
                        echo '</div>';
                        echo '<div class="col-12 col-sm-1 text-right gp">';
                        echo number_format($position->gp,2,',','.').' €';
                        echo '</div>';
                     endif;
                  echo '</div>';
                  echo '</div>';
                  $i++;
                  $gp += $position->gp;
               endforeach;
            else:
               echo '<div id="contactPlaceholder" class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
               echo 'Keine Rechnungspositionen gefunden';
               echo '</div>';
            endif;
            ?>
         </div>
         <div class="row mt-2 p-2">
            <div class="col-12 col-sm-6">
               <a class="btn btn-success text-white mb-3" onclick="newPosition();">Neue Position</a>
               <a class="btn btn-success text-white mb-3" onclick="newText();">Neuer Freitext</a>
            </div>
            <div class="col-12 col-sm-6">
               <div class="card">
                  <div class="row p-2">
                     <div class="col-12 col-sm-10"><b>Zwischensumme netto</b></div>
                     <div id="invoice_GP" class="col-12 col-sm-2 text-right">
                        <?php echo number_format($gp,2,',','.');?> €
                     </div>
                  </div>
                  <div class="row p-2">
                     <?php
                     $ust = ($gp * 16) / 100;
                     ?>
                     <div class="col-12 col-sm-10"><b>USt 16%</b></div>
                     <div id="invoice_GP" class="col-12 col-sm-2 text-right">
                        <?php echo number_format($ust,2,',','.');?> €
                     </div>
                  </div>
                  <div class="card-footer p-2">
                     <div class="row">
                        <?php
                        $gep = ($gp * 116) / 100;
                        ?>
                        <div class="col-12 col-sm-10"><b>Gesamtbetrag</b></div>
                        <div id="invoice_GP" class="col-12 col-sm-2 text-right">
                           <?php echo number_format($gep,2,',','.');?> €
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </div>
   <div class="card p-2 mt-3" style="background-color: #f1f1f1;">
      <p><b>Fußbereich</b></p>
      <div class="form-horizontal mt-3">
         <div class="row">
            <div class="col-12 col-sm-12">
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('invoice_paymentterms'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('invoice_paymentterms'); ?>
                     <!--<a class="float-right btn btn-primary btn-sm text-white mt-1">Zahlungsbedingungen verwalten</a>-->
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12 col-sm-2 col-form-label">
                     <?php echo $this->form->getLabel('invoice_outro'); ?>
                  </div>
                  <div class="col-12 col-sm-10">
                     <?php echo $this->form->getInput('invoice_outro'); ?>
                     <!--<a class="float-right btn btn-primary btn-sm text-white mt-1">Nachbemerkungen verwalten</a>-->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<input type="hidden" id="id" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="planID" value="" />
<input type="hidden" id="countPositions" value="<?php echo count($this->item->positions);?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   $(document).ready(function() {
      loadPerformanceData();
      jQuery('#article').select2({
         dropdownParent: jQuery('#newPosition'),
         tags: true,
         width: '100%'
      });
      jQuery('#unit').select2({
         dropdownParent: jQuery('#newPosition'),
         width: '100%'
      });
      jQuery('#tax').select2({
         dropdownParent: jQuery('#newPosition'),
         width: '100%'
      });
   });
   function loadPerformanceData() {
      var performance_type = $('#jform_performance_type option:selected').val();
      if(performance_type == 1 || performance_type == 2) {
         $('#performance1').css('display', 'none');
         $('#performance2').css('display', 'flex');
      }
      if(performance_type == 3 || performance_type == 4) {
         $('#performance2').css('display', 'none');
         $('#performance1').css('display', 'flex');
      }
      if(performance_type == 5) {
         $('#performance1').css('display', 'none');
         $('#performance2').css('display', 'none');
      }
      if(performance_type == 1) {
         $('#performance2 input').attr('placeholder', 'Lieferdatum');
      }
      if(performance_type == 2) {
         $('#performance2 input').attr('placeholder', 'Leistungsdatum');
      }
      if(performance_type == 3 || performance_type == 4) {
         $('#performance1 #jform_performance_date1').attr('placeholder', 'tt.mm.jjjj');
         $('#performance1 #jform_performance_date2').attr('placeholder', 'tt.mm.jjjj');
      }
   }
   var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[email]': {
            required: false,
            email: true
         },
         'jform[customerID]': {
            required: true
         },
         'jform[last_name]': {
            required: true
         }
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[customerID]': 'Bitte wählen Sie einen Kunden aus.',
         'jform[last_name]': 'Bitte geben Sie Ihren Nachnamen ein.'
      },
      submitHandler: function(form) {
         form.submit();
      }
   });
   function newPosition() {
      var id    = $('#id').val();
      var customerID    = $('#jform_customerID option:selected').val();
      if(!id || !customerID) {
         alert('Bitte wählen Sie zunächst die Kundendaten und speichern anschließend den Datensatz.');
         return false;
      }
      $('#newPosition').modal('show');
   }
   function newText() {
      var id    = $('#id').val();
      var customerID    = $('#jform_customerID option:selected').val();
      if(!id || !customerID) {
         alert('Bitte wählen Sie zunächst die Kundendaten und speichern anschließend den Datensatz.');
         return false;
      }
      $('#newText').modal('show');
   }
   function editPosition(id) {
      $('#newPosition').modal('show');
   }
   function loadCustomerData() {
      var customerID    = $('#jform_customerID').val();
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=invoice.loadCustomerData",
         data: {
            customerID:customerID
         },
         dataType: 'json',
         success: function( data ) {
            $('#jform_customer_number').val(data['customer_number']);
            $('#jform_name').val(data['company_name']);
            $('#jform_address').val(data['address']);
            $('#jform_postcode').val(data['postcode']);
            $('#jform_city').val(data['city']);
            $('#jform_email').val(data['email']);
            $('#planID').val(data['planID']);
         }
      });
   }
   function loadPrice() {
      var planID  = $('#article option:selected').val();
      $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=invoice.loadPrice",
            data: {
               planID:planID
            },
            //dataType: 'json',
            success: function( data ) {
               $('#price').val(data);
            }
         });
   }
   function savePosition() {
      var invoiceID  = $('#id').val();
      var article    = $('#article option:selected').text();
      var articleID  = $('#article option:selected').val();
      var amount     = $('#count').val();
      var einheit    = $('#unit option:selected').val();
      var einheitF   = $('#unit option:selected').text();
      var tax        = $('#tax').val();
      var discount   = $('#discount').val();
      var priceF     = $('#price').val();
      var price      = $('#price').val();
      price = price.replace(',','.');
      price = price.replace(' €','');

      var GP = parseFloat(amount) * parseFloat(price);
      if(discount && discount > 0) {
         GP = (parseFloat(GP) * (100 - discount)) / 100;
      }
      GP = GP.toString();
      GP = GP.replace('.',',');
      places = GP.split(',');
      if(places.length > 1) {
         if(places[1].length == 1) {
            GP = GP + '0';
         }
      }
      if(places.length == 1) {
         GP = GP + ',00';
      }
      var position   = $('#countPositions').val();
      $('#contactPlaceholder').remove();
      if(article && price && amount) {
         position       = parseInt(position) + 1;
         setFromPlan(position,invoiceID,articleID,article,amount,price,priceF,GP,einheit,einheitF,tax,discount);
      }
      $('#countPositions').val(position);
      $('#newPosition').modal('hide');
   }
   function setFromPlan(position,invoiceID,articleID,article,amount,price,priceF,GP,einheit,einheitF,tax,discount) {
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=invoice.addPosition",
         data: {
            invoiceID:invoiceID,
            articleID:articleID,
            article:article,
            amount:amount,
            price:price,
            einheitF:einheitF,
            tax:tax,
            discount:discount
         },
         async: true,
         //dataType: 'json',
         success: function( data ) {
            priceF = priceF.replace(' ', '');
            priceF = priceF.replace('€', '');
            places = priceF.split(',');
            if(places.length > 1) {
               if(places[1].length == 1) {
                  priceF = priceF + '0';
               }
            }
            if(places.length == 1) {
               priceF = priceF + ',00';
            }
            $('#contacts').append(
               '<div id="position' + data + '" class="position" style="padding: 8px 15px;">' + 
               '<div class="row">' + 
               '<div class="col-12 col-sm-1">' + 
                  '<i class="fa fa-ellipsis-v mr-2"></i>' + 
                  '<i onclick="editPosition(' + data + ');" class="fa fa-pencil mr-2"></i>' + 
                  '<i onclick="deletePosition(' + data + ');" class="fa fa-trash-o mr-2"></i>' + 
               '</div>' +
               '<div class="col-12 col-sm-1">' + 
                  position +  
               '</div>' +
               '<div class="col-12 col-sm-4">' + 
                  article +  
               '</div>' +
               '<div class="col-12 col-sm-1 text-center">' + 
                  amount +  
               '</div>' +
               '<div class="col-12 col-sm-1 text-center">' + 
                  einheitF +
               '</div>' +
               '<div class="col-12 col-sm-1 text-right">' + 
                  priceF +  ' €' +
               '</div>' +
               '<div class="col-12 col-sm-1 text-right">' + 
                  tax +  '%' +
               '</div>' +
               '<div class="col-12 col-sm-1 text-right">' + 
                  discount +  '%' +
               '</div>' +
               '<div class="col-12 col-sm-1 text-right gp">' + 
                  GP + ' €' +  
               '</div>' +
               '</div>' +
               '</div>'
            );
            
            invoiceSum();
         }
      });
   }
   function saveFreetext() {
      var invoiceID  = $('#id').val();
      var text       = $('#jform_freetext').val();
      text           = text.replace(/\n/g, '<br>\n');
      $('#newText').modal('hide');
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=invoice.addFreetext",
         data: {
            invoiceID:invoiceID,
            text:text
         },
         async: true,
         //dataType: 'json',
         success: function( data ) {
            $('#contacts').append(
               '<div id="position' + data + '" class="position free" style="padding: 8px 15px;">' + 
               '<div class="row">' + 
               '<div class="col-12 col-sm-1">' + 
                  '<i class="fa fa-ellipsis-v mr-2"></i>' + 
                  '<i onclick="editPosition(' + data + ');" class="fa fa-pencil mr-2"></i>' + 
                  '<i onclick="deletePosition(' + data + ');" class="fa fa-trash-o mr-2"></i>' + 
               '</div>' +
               '<div class="col-12 col-sm-11">' + 
                  text +  
               '</div>' +
               '</div>' +
               '</div>'
            );
         }
      });
   }
   function invoiceSum() {
      var gp = 0;
      $('.position').each(function() {
         var price      = $(this).find('.gp').html();
         price = price.replace(',','.');
         price = price.replace(' €','');
         gp = gp + parseFloat(price);
      });
      gpF = round(gp,2);
      gpF = gpF.toString();
      gpF = gpF.replace('.',',');
      var places = gpF.split(',');
      if(places.length > 1) {
         if(places[1].length == 1) {
            gpF = gpF + '0';
         }
      }
      if(places.length == 1) {
         gpF = gpF + ',00';
      }
      $('#invoice_GP').html(gpF + ' €');
   }
   function reorder() {
      var i = 1;
      $('.position').each(function() {
         console.log($(this).find('.number').attr('id'));
         $(this).find('.number').html(i);
         i++;
      });
   }
   //Funktion zum Runden der Beträge
   function round(wert, dez) {
      wert = parseFloat(wert);
      if (!wert) return 0;

      dez = parseInt(dez);
      if (!dez) dez=0;

      var umrechnungsfaktor = Math.pow(10,dez);

      return Math.round(wert * umrechnungsfaktor) / umrechnungsfaktor;
   } 
   function deletePosition(id) {
      if(confirm('Soll die Position ' + id + ' gelöscht werden?') == true) {
         $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=invoice.deletePosition",
            data: {
               id:id
            },
            async: false,
            //dataType: 'json',
            success: function( data ) {
               $('#position' + id).remove();
               invoiceSum();
               
            }
         });
         var position = $('#countPositions').val();
         $('#countPositions').val(parseInt(position) - 1);
         setTimeout(reorder(),2000);
      }
   }
   jQuery('#jform_email').on('change', function() {
      var email = jQuery('#jform_email').val();
      jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
            data: {email:email},
            //dataType: 'json',
            success: function( data ) {
               if(data == 'OK') {
                  jQuery('#jform_username').val(email);
               } else {
                  alert('Die E-Mail-Adresse wird bereits verwendet.');
                  jQuery('#jform_email').val('');
                  return false;
               }
            }
        });
   });
</script>