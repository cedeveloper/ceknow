<div id="newPosition" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 1000px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Neue Rechnungsposition einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
                <div class="col-12 col-sm-12 col-form-label">
                  Artikel
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-12">
                  <select id="article" class="select2-max" onchange="loadPrice();">
                     <option value="" selected="selected" disabled>Bitte auswählen oder neu eingeben</option>
                     <?php
                     if($this->item->plans):
                        foreach($this->item->plans as $plan):
                           echo '<option value="'.$plan->id.'">'.$plan->detail.'</option>';
                        endforeach;
                     endif;
                     ?>
                  </select>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2">
                  Menge
               </div>
               <div class="col-12 col-sm-3">
                  Einheit
               </div>
               <div class="col-12 col-sm-3 text-right">
                  Preis (€/netto)
               </div>
               <div class="col-12 col-sm-2 text-right">
                  Steuer (%)
               </div>
               <div class="col-12 col-sm-2 text-right">
                  Rabatt (%)
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2">
                  <input type="number" style="height: 38px;" min="0" id="count" value="0" />
               </div>
               <div class="col-12 col-sm-3">
                  <select id="unit" class="select2-max">
                     <option value="" selected="selected" disabled>Bitte auswählen</option>
                     <?php
                     if($this->item->einheiten):
                        foreach($this->item->einheiten as $einheit):
                           echo '<option value="'.$einheit->id.'">'.$einheit->title.'</option>';
                        endforeach;
                     endif;
                     ?>
                  </select>
               </div>
               <div class="col-12 col-sm-3 text-right">
                  <input type="text" class="text-right" style="height: 38px;" id="price" value="" />
               </div>
               <div class="col-12 col-sm-2 text-right">
                  <select id="tax" class="select2-max">
                     <option value="" selected="selected" disabled>Bitte auswählen</option>
                     <?php
                     if($this->item->taxes):
                        foreach($this->item->taxes as $tax):
                           echo '<option value="'.$tax->rate.'">'.$tax->title.'</option>';
                        endforeach;
                     endif;
                     ?>
                  </select>
               </div>
               <div class="col-12 col-sm-2 text-right">
                  <input type="text" class="text-right" style="height: 38px;" id="discount" value="0" />
               </div>
              
            </div>
            <!--<div class="form-group row">
               <div class="col-12 col-sm-8">
                  <input type="text" id="ownArticle" placeholder="Eigener Artikel" value="" />
               </div>
               <div class="col-12 col-sm-2">
                  <input type="number"  min="0" id="ownAmount" value="0" />
               </div>
               <div class="col-12 col-sm-2">
                  <input type="text" id="ownPrice" value="" />
               </div>
            </div>-->
         </div>
         <div class="modal-footer">
            <button onclick="savePosition();" type="button" class="btn btn-primary">Einfügen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>