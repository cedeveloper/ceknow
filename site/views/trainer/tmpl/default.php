<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
$user = JFactory::getUser();
$now = new DateTime();
$canEdit	= $user->authorise('core.edit',       'com_auditum');
$canEditOwn	= $user->authorise('core.edit.own',   'com_auditum') && $this->item->created_by == $user->id;
$canDelete	= $user->authorise('core.delete',       'com_auditum');
?>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3">
    	<h1><?php echo JText::_('Bereich'); ?></h1>
		<p class=" text-info"><?php echo $now->format('d.m.Y H:i');?></p>
  	</div>
   	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
    	<div class="panel panel-info">
			<div class="panel-heading">
           		<h3 class="panel-title"><?php echo $this->item->bereich;?></h3>
            </div>
            <div class="panel-body">
           		<div class="row">
                	<div class="col-md-3 col-lg-3 " align="center"> 
                    <?php if($this->item->logo) { ?>
                    	<img src="<?php echo $this->item->logo;?>" />
                   	<?php } else { ?>
                    	<img alt="User Pic" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" class="img-circle img-responsive">
                    <?php } ?>
                    </div>
                    <div class=" col-md-9 col-lg-9 "> 
                        <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>Bereich:</td>
                                    <td><?php echo $this->item->bereich;?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
                	<div class="panel-footer">
                     	<a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                       	<span class="pull-right">
                        	<?php if ($canEdit || $canEditOwn) : ?>
                        	<a href="index.php?option=com_auditum&view=mandant&layout=edit&id=<?php echo $this->item->id;?>" data-original-title="Diesen Mandanten bearbeiten" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                            <?php endif; ?>
                            <?php if ($canDelete) : ?>
                           	<a data-original-title="Diesen Mandaten löschen" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                            <?php endif; ?>
                       	</span>
                   	</div>
          		</div>
        	</div>
       	</form>