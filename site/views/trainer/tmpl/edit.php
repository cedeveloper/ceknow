<?php
/**
 * @author		datsDORTMUND
 * @copyright	(c) 2016 datsDORTMUND. All rights reserved.
 * @product		datsAUDITUM		
 */

defined("_JEXEC") or die("Restricted access");
$doc = JFactory::getDocument();
$doc->addScript( 'components/com_jclassroom/assets/js/jquery.validate.js' );
?>

<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
   <div class="actionboard mt-3 mb-3">
      <button type="button" class="btn btn-primary" onclick="Joomla.submitform('trainer.simpleSave')"><?php echo JText::_('JAPPLY') ?></button>
      <button type="button" class="btn btn-secondary" onclick="Joomla.submitform('trainer.save')"><?php echo JText::_('Speichern & schließen') ?></button>
      <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger">Zurück</a>
   </div>
   <div id="theAlerts" class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> Bitte prüfen Sie die Pflichtfelder</div>
   <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
         <a class="nav-link active" id="firmendaten-tab" data-toggle="tab" href="#firmendaten" role="tab" aria-controls="firmendaten" aria-selected="true">Allgemein</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="asp-tab" data-toggle="tab" href="#asp" role="tab" aria-controls="asp" aria-selected="false">Ansprechpartner</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="desc-tab" data-toggle="tab" href="#desc" role="tab" aria-controls="desc" aria-selected="false">Informationen</a>
      </li>
      <?php if($this->usergroup == 'superuser') { ?>
      <li class="nav-item">
         <a class="nav-link" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-controls="global" aria-selected="false">Global</a>
      </li>
      <?php } ?>
      <li class="nav-item">
         <a class="nav-link" id="freigabe-tab" data-toggle="tab" href="#freigabe" role="tab" aria-controls="freigabe" aria-selected="false">Freigabe</a>
      </li>
   </ul>
   <div class="tab-content" id="tabContent">
      <div class="tab-pane fade show active" id="firmendaten" role="tabpanel" aria-labelledby="firmendaten-tab">
         <div class="form-horizontal mt-3 w-50">
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('customerID'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('customerID'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('salutation'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('salutation'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('first_name'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('first_name'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('last_name'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('last_name'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('company'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('company'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('adress'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('adress'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('postcode'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('postcode'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('city'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('city'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('phone'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('phone'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('email'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('email'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('web'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('web'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  <?php echo $this->form->getLabel('logo'); ?>
               </div>
               <div class="col-12 col-sm-10">
                  <?php echo $this->form->getInput('logo'); ?>
                  <?php
                  if($this->item->logo) {
                     echo '<img style="width: 150px;margin-top: 20px;" src="'.$this->item->logo.'" />';
                  }
                  ?>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="asp" role="tabpanel" aria-labelledby="asp-tab">
         <div class="form-horizontal mt-3 w-50">
             <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('asp'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('asp'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('asp_phone'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('asp_phone'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-4 col-form-label">
                  <?php echo $this->form->getLabel('asp_email'); ?>
               </div>
               <div class="col-12 col-sm-8">
                  <?php echo $this->form->getInput('asp_email'); ?>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="desc" role="tabpanel" aria-labelledby="desc-tab">
         <div class="form-horizontal mt-3">
             <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('description'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('description'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('skills'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('skills'); ?>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="global" role="tabpanel" aria-labelledby="global-tab">
         <div class="form-horizontal mt-3">
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('published'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('published'); ?>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('id'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('id'); ?>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="freigabe" role="tabpanel" aria-labelledby="freigabe-tab">
         <div class="form-horizontal mt-3 w-50">
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('username'); ?><br/>
                  
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('username'); ?>
                  <small>Der Benutzername ist die E-Mail-Adresse</small>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-3 col-form-label">
                  <?php echo $this->form->getLabel('password'); ?>
               </div>
               <div class="col-12 col-sm-9">
                  <?php echo $this->form->getInput('password'); ?>
                  <?php echo $this->form->getInput('newDS'); ?>
                  <?php echo $this->form->getInput('currentUserID'); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
<input type="hidden" id="id" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="user" value="<?php echo $this->item->username;?>" />
<input type="hidden" id="userID" value="<?php echo $this->item->tblUserID;?>" />
<input type="hidden" id="usergroup" value="<?php echo $this->usergroup;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   $(document).ready(function() {
      var usergroup = $('#usergroup').val();
      if(usergroup != 'superuser') {
         $('#jform_customerID').attr('disabled', true);
      } else {
         $('#jform_customerID').attr('disabled', false);
      }
   });
   var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      invalidHandler: function(form, validator) {
         //var map = $('name="' + validator.errorMap + '"');
         var iEL  = validator.invalidElements();
         var name = iEL[0].id;
         var tab  = jQuery('#' + name).closest('div.tab-pane');
         $('#myTab li a').each(function() {
            $(this).removeClass('active');
         });
         $('.tab-pane').each(function() {
            $(this).removeClass('show').removeClass('active');
         });
         $('#' + tab.attr('id') + '-tab').addClass('active');
         $('#' + tab.attr('id')).addClass('show').addClass('active');
         $('#theAlerts').css('display', 'block');
      },
      rules: {
         'jform[email]': {
            required: true,
            email: true
         },
         'jform[salutation]':{
            required: true
         },
         'jform[postcode]': {
            required: true,
            number: true
         },
         'jform[customerID]': {
            required: true
         }
      },
      messages: {
         'jform[salutation]': 'Bitte wählen Sie eine Anrede aus.',
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[postcode]': 'Bitte geben Sie die Postleitzahl als Zahl ein.',
         'jform[customerID]': 'Bitte wählen Sie einen Customer aus.'
      },
      submitHandler: function(form) {
         var email   = $('#jform_email').val();
         var userID  = $('#userID').val();
         if(!userID) {
            jQuery.ajax({
               type: "POST",
               url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
               data: {email:email,userID:userID},
               async: false,
               //dataType: 'json',
               success: function( data ) {
                  if(data == 'OK') {
                     form.submit();
                  } else {
                     alert('Die E-Mail-Adresse wird bereits verwendet.');
                     $('#jform_email').val('');
                     return false;
                  }
               }
            });     
         } else {
            form.submit();
         }
      }
   });
   function checkEmail() {
      var email1 = $('#jform_email').val();
      var email2 = $('#jform_username').val();
      if((email1 && email2) && (email1 != email2)) {
         alert('Der Benutzername muss identisch zur E-Mail-Adresse sein.');
         $('#jform_username').val(email1);
         return false;
      }
   }
   function checkEmailUnique() {
      var email   = $('#jform_email').val();
      if(email) {
         var userID  = $('#userID').val();
         jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
            data: {email:email,userID,userID},
            async: false,
            //dataType: 'json',
            success: function( data ) {
               if(data == 'OK') {
                  jQuery('#jform_username').val(email);
                  return true;
               } else {
                  alert('Die E-Mail-Adresse wird bereits verwendet.');
                  jQuery('#jform_email').val('');
                  return false;
               }
            }
         });
      }
   }
   jQuery('#jform_email').on('change', function() {
      checkEmailUnique();
   });
</script>