<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
 
/**
 * Kunde item view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewTrainer extends JViewLegacy
{
	protected $item;
	protected $form;
	protected $state;
	
	public function display($tpl = null)
	{

		require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        $session 	= JFactory::getSession();
		$document 	= JFactory::getDocument();
		$document->setTitle('Trainer bearbeiten ['.$session->get('systemname').']');
		// Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
	        JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
	        $demo = new LoginHelper();
	        $demo->checkDemo();
	    endif;

		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');

		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		switch($session->get('group')) {
			case 'superuser':
				$this->retour = JURI::Root().'manager-administrator/trainer';
				$this->usergroup = 'superuser';
				break;
			case 'customer':
				$this->retour = 'manager-customer/trainer-customer';
				$this->usergroup = 'customer';
				break;
			case 'trainer':
				$this->retour = 'manager-trainer';
				$this->usergroup = 'trainer';
				break;
			case 'student':
				$this->retour = 'manager-student';
				$this->usergroup = 'student';
				break;
		}
		// Check if item is empty
		if (empty($this->item))
		{
			$app->redirect(JRoute::_('index.php?option=com_plans&view=resellers'), JText::_('JERROR_NO_ITEMS_SELECTED'));
		}
		// Is the user allowed to create an item?
		/*if (!$this->item->id && !$user->authorise("core.create", "com_auditum"))
		{
			throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
		}*/

		// Get menu params
		$menu = $app->getMenu();
		$active = $menu->getActive();
		
		if (is_object($active))
		{
			$this->state->params = $active->params;
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			//throw new Exception(implode("\n", $errors));
			//return false;
		}		

		// Increment hits
		$model = $this->getModel();
		$model->hit($this->item->id);##{end_hits}##
		
		parent::display($tpl);
	}
}
?>