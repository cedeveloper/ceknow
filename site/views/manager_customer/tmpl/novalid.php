<?php
/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

$doc = JFactory::getDocument();
?>
<div class="inflow admin row">
	<div class="col-12">
		<h1>NO VALID</h1>
	</div>
</div>