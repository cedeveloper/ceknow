<?php
/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

$doc = JFactory::getDocument();
?>
<div class="inflow admin row">
	<div class="col-12">
		<div class="row">
			<div class="col-12">
				<div class="card text-white bg-light mb-3" style="font-size: 12px;">
					<div class="card-body p-1">
						<p class="card-title text-dark"><b>Ihre Kundendaten</b></p>
						<div class="row">
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-4">
											Kunde:
										</div>
										<div class="col-12 col-sm-8">
											<i><?php echo $this->customerData->company_name;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-4">
											Kunden-Nr.:
										</div>
										<div class="col-12 col-sm-8">
											<i><?php echo $this->customerData->customer_number;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-4">
											Name:
										</div>
										<div class="col-12 col-sm-8">
											<i><?php echo $this->customerData->first_name.' '.$this->customerData->last_name;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-4">
											Plan:
										</div>
										<div class="col-12 col-sm-8">
											<span class="badge badge-primary text-white">
												<i>
													<?php echo $this->customerData->plan_name;?>
												</i>
											</span>
											<?php if($this->customerData->planID == 4): ?>
												<span> gültig bis: <?php echo date('d.m.Y', strtotime($this->customerData->plan_to));?></span>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<?php if(!$this->customerData->address || !$this->customerData->postcode || !$this->customerData->city): ?>
										<div class="row">
											<div class="col-12 col-sm-3">
												Anschrift:
											</div>
											<div class="col-12 col-sm-9">
												<span style="font-size: 14px;" class="card bg-danger text-white p-1">Bitte vervollständigen Sie Ihr Kundenkonto
												<a style="font-size: 14px;font-weight: bold;color: white;" class="" href="mein-konto-customer/mein-konto">Zum Kundenkonto</a>
												</span>
											</div>
										</div>
									<?php else: ?>
									<div class="row">
										<div class="col-12 col-sm-3">
											Anschrift:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->customerData->address;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->customerData->postcode.' '.$this->customerData->city;?></i>
										</div>
									</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-3">
											Telefon:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->customerData->phone;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
											Fax:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->customerData->fax;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
											E-Mail:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->customerData->email;?></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: lightseagreen;">
					<div class="card-header">Firmen</div>
					<div class="card-body">
						<a href="manager-customer/companies-customer" class="btn btn-secondary">Firmen</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-primary mb-3">
					<div class="card-header">Trainer</div>
					<div class="card-body">
						<a href="manager-customer/trainer-customer" class="btn btn-secondary">Zu den Trainern</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-success mb-3">
					<div class="card-header">Teilnehmer</div>
					<div class="card-body">
						<a href="manager-administrator/students" class="btn btn-secondary">Zur Teilnehmerverwaltung</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-warning mb-3">
					<div class="card-header">Learningrooms</div>
					<div class="card-body">
						<a href="classrooms" class="btn btn-secondary">Zur Learningroom-Verwaltung</a>
					</div>
				</div>
			</div>
			<!--<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: sandybrown;">
					<div class="card-header">Audits</div>
					<div class="card-body">
						<a href="library-days" class="btn btn-secondary">Audits</a>
					</div>
				</div>
			</div>-->
			<!--<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: teal;">
					<div class="card-header">Tutorials</div>
					<div class="card-body">
						<a href="library-days" class="btn btn-secondary">Tutorials</a>
					</div>
				</div>
			</div>-->
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-danger mb-3">
					<div class="card-header">Quizze</div>
					<div class="card-body">
						<a href="quizze" class="btn btn-secondary">Zu den Quizzen</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: cornflowerblue;">
					<div class="card-header">Vorlagen</div>
					<div class="card-body">
						<a href="library-learningrooms" class="btn btn-secondary">Learningrooms</a>
						<a href="bibliothek-quizze" class="btn btn-secondary">Quizze</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-info mb-3">
					<div class="card-header">Bibliothek</div>
					<div class="card-body">
						<a href="library-days" class="btn btn-secondary">Themen</a>
						<a href="library-modules" class="btn btn-secondary">Module</a>
						<a href="library-units" class="btn btn-secondary">Units</a>
					</div>
				</div>
			</div>
			<!--<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: indianred;">
					<div class="card-header">Veröffentlichen</div>
					<div class="card-body">
						<a href="library-days" class="btn btn-secondary">Quizze</a>
						<a href="library-modules" class="btn btn-secondary">Audits</a>
						<a href="library-units" class="btn btn-secondary">Learningrooms</a>
					</div>
				</div>
			</div>-->
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: orchid;">
					<div class="card-header">Auswertungen</div>
					<div class="card-body">
						<a href="evaluate-quizze" class="btn btn-secondary">Quizze</a>
						<!--<a href="evaluate-learningrooms" class="btn btn-secondary">Learningrooms</a>-->
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white mb-3" style="background-color: darksalmon;">
					<div class="card-header">Templates</div>
					<div class="card-body">
						<a href="email-templates" class="btn btn-secondary">Zu den Templates</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3">
				<div class="card text-white bg-secondary mb-3">
					<div class="card-header">Einstellungen</div>
					<div class="card-body">
						<a href="configuration" class="btn btn-light">Zu den Einstellungen</a>
					</div>
				</div>
			</div>
		</div>
		<!--<div class="card text-white bg-dark mb-3">
			<div class="card-header text-white">Staging</div>
			<div class="card-body">
				<div class="col-12">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-4 col-lg-4">
							<div class="card text-white bg-primary mb-3">
								<div class="card-header">Learningroom erstellen und veröffentlichen</div>
								<div class="card-body">
									<p>Erstellen Sie einen Learningroom oder wählen Sie einen bestehenden Learningroom aus und veröffentlichen Sie ihn</p>
									<a href="manager-administrator/trainer" class="btn btn-secondary">Zum Learningroom</a>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-4 col-lg-4">
							<div class="card text-white bg-primary mb-3">
								<div class="card-header">Quizz erstellen und veröffentlichen</div>
								<div class="card-body">
									<p>Erstellen Sie ein Quizz oder wählen Sie ein bestehendes Quizz aus und veröffentlichen Sie es</p>
									<a href="veroeffentlichen-customer/quizz-veroeffentlichen" class="btn btn-secondary"> Quizz veröffentlichen</a>
									<a href="veroeffentlichen-customer/veroeffentlichte-quizze" class="btn btn-secondary">Veröffentlichte Quizze</a>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-4 col-lg-4">
							<div class="card text-white bg-primary mb-3">
								<div class="card-header">Audit erstellen und veröffentlichen</div>
								<div class="card-body">
									<p>Erstellen Sie ein Audit oder wählen Sie ein bestehendes Audit aus und veröffentlichen Sie es</p>
									<a href="veroeffentlichen/quizz-veroeffentlichen" class="btn btn-secondary"> Audit veröffentlichen</a>
									<a href="veroeffentlichen/veroeffentlichte-quizze" class="btn btn-secondary">Veröffentlichte Audits</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>-->
		<div class="card text-white bg-light mb-3">
			<div class="card-header text-dark">Shortcuts</div>
			<div class="card-body">
				<a href="mein-konto-customer/mein-konto" class="float-left btn btn-primary">Mein Konto</a>
				<a href="abmelden" class="float-right btn btn-danger">Abmelden</a>
			</div>
		</div>
	</div>
</div>