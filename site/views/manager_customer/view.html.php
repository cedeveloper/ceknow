<?php

/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewManager_customer extends JViewLegacy {

    public function display($tpl = null) {
        require_once(JPATH_COMPONENT_SITE.'/helpers/login.php');
        $checkPreviousDelivery = LoginHelper::checkLogin();
        $testmode = LoginHelper::checkTestmode();
        if($testmode):
            echo $testmode;
        endif;
        $session    = JFactory::getSession();
        $document   = JFactory::getDocument();
        $document->setTitle('Manager [Kunde]['.$session->get('systemname').']');
        // Check if Demotime is valid, if user is customer
        if($session->get('customerID')):
            JLoader::register('LoginHelper',JPATH_COMPONENT_SITE.'/helpers/login.php');
            $demo = new LoginHelper();
            $demo->checkDemo();
        endif;


        $this->customerData = $this->get('Customer');
        
        parent::display($tpl);
    }
}
