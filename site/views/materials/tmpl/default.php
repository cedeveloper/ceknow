<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <a class="btn btn-success text-white" onclick="Joomla.submitform('order.add')">Neue Bestellung</a>
        <a class="btn btn-danger text-white m-1" id="deleteOrder">Bestellung(en) löschen</a>
        <!--<button type="button" class="btn btn-info" onclick="Joomla.submitform('students.saveCSV')">Liste exportieren</button>-->
        <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right">Zurück</a>
    </div>
    <table class="table table-striped">	
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Bestellnummer', 'a.id', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Learningroom', 'a.classroomID', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Kunde', 'a.name', $listDirn, $listOrder) ?>
                </th> 
                <th>
                    Bestelldaten
                </th>
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Status'), 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>		
        <tbody>
        <?php if($this->items) { ?>
        <?php foreach ($this->items as $i => $item) :?>
            <tr class="row<?php echo $i % 2; ?>">
            	<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
				<td class="right"><?php echo $this->escape($item->id); ?></td>
                <td>
                    <a href="<?php echo JURI::Root().'order-edit?id='. $item->id; ?>">
                        <?php echo 'BST'.str_pad($item->id,8,'0', STR_PAD_LEFT); ?>
                    </a>
                </td>
                <td>
                    <?php 
                    if($item->classroom_name):
                        echo $item->classroom_name; 
                        if($item->theData):
                            echo '<br><b>Beginn: </b> '.date('d.m.Y', strtotime($item->theData->start));
                            echo '<br><b>Ende: </b> '.date('d.m.Y', strtotime($item->theData->end));
                        endif;
                    endif;
                    ?>
                </td>
                <td class="left">
                    <?php echo $item->salutation.' '.$item->first_name.' '.$item->last_name; ?><br/>
                    <?php echo $this->escape($item->adress); ?><br/>
                    <?php echo $item->postcode.' '.$item->city; ?> 
                </td>
                <td>
                    <?php
                    if($item->positions):
                        foreach($item->positions as $position):
                            echo $position->amount.' x '.$position->title.'<br/>';
                        endforeach;
                    endif;
                    ?>
                </td>
                <?php
                if($item->published == 0) {
					$class="badge-danger";
					$wert = "<i class='fa fa-ban'></i>";
				} elseif($item->published == 1) {
					$class="badge-success";
					$wert = "<i class='fa fa-check'></i>";
				}
				?>
                <td class="text-center">
                	<span class="btn <?php echo $class;?>"><?php echo $wert;?></span>
               	</td>
            </tr>
        <?php endforeach ?>
        <?php } else { ?>
        	<tr>
            	<td colspan="8">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
       	<?php } ?>
        </tbody>			
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
jQuery('#openUploader').click(function() {
    jQuery('#uploader').trigger('click');
    
});
jQuery('#uploader').on('change', function() {
    jQuery('#uploaderForm').submit();
});
$('#deleteOrder').on('click', function() {
    if(confirm('Sollen die ausgewählten Bestellungen gelöscht werden?') == true) {
        Joomla.submitform('orders.delete');
    }
});
</script>