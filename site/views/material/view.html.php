<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JclassroomViewMaterial extends JViewLegacy {

   	protected $item;
	protected $form;
	protected $state;

    public function display($tpl = null) {

		$user = JFactory::getUser();
		$this->state 	= $this->get('State');
		$this->item 	= $this->get('Item');
		$this->form 	= $this->get('Form');
		$session = JFactory::getSession();
		JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
		$load = new GlobalHelper();
		$login = $load->getLogindata();
		switch($login['group']) {
			case 'superuser':
				$this->retour 	= 'manager-administrator/orders-admin';
				$this->student 	= 0;
				break;
			case 'customer':
				$this->retour 	= 'manager-customer/orders-customer';
				$this->student 	= 0;
				JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
				$load = new GlobalHelper();
				$this->customer = $load->getCustomer($login['customerID']);
				break;
			case 'trainer':
				$this->student 	= 0;
				break;
			case 'student':
				$this->retour 	= 'dashboard-students?option=com_jclassroom&layout=list';
				$this->student 	= 1;
				break;
		}

		

        parent::display($tpl);
    }
}
