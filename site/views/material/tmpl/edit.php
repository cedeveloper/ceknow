<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");
JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
$doc->addScript('components/com_jclassroom/assets/js/classroom.js');
$doc->addScript('components/com_jclassroom/assets/js/lightbox.js');
JLoader::register('GlobalHelper',JPATH_COMPONENT_SITE.'/helpers/global.php');
$load = new GlobalHelper();
require_once(JPATH_COMPONENT.'/views/dialogs/lightbox.php');
?>
<style>
 i.fa {
   cursor: pointer;
 }
</style>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
   <div class="d-inline-block w-100">
   <h5>
      Bilderverwaltung für Kunde: <?php echo 'CU'.str_pad($this->customer['customerID'],8,'0', STR_PAD_LEFT);?><br>
      <?php 
      echo $this->customer['customer']->name ?  $this->customer['customer']->name :  $this->customer['customer']->company_name;
      ?>
   </h5>
   <a id="retour" href="classroom-edit?layout=global&id=<?php echo $this->item->id;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
   </div>
   <div class="form-horizontal mt-3">
      <div class="row mt-2">
         <div class="col-12 col-sm-6">
            <div class="alert alert-danger p-1 ftn-14">Achtung! Sie haben hier uneingeschränkten Zugriff auf alle von Ihnen auf dem Server gespeicherten Dateien. Gelöschte Dateien sind unwiderruflich verloren.</div>
            <?php
            $sumSize = 0;
            ?>
            <div id="folderTreeLearningrooms" class="card bg-light p-1" style="font-size: 12px;">
               <?php
               if($this->customer['learningrooms']):
                  foreach($this->customer['learningrooms'] as $lr):
                     $size = 0;
                     echo '<div id="lr'.$lr->id.'">';
                     echo '<b>LR'.str_pad($lr->id,8,'0',STR_PAD_LEFT).' </b>'.$lr->title;
                     $files = $load->getFiles($this->customer['customerID'], 'classroom',$lr->id);
                     if($files):
                        echo '<div class="row pl-3 pr-3">';
                        foreach($files as $file):
                           echo '<div id="file'.$file->id.'" class="col-12 col-sm-1 p-1">';
                           echo '<img onclick="openLightbox(&quot;'.$file->path.'&quot;,'.$file->id.','.$file->size.',&quot;'.$file->extention.'&quot;);" max-width="80px" src="'.$file->path.'" />';
                           echo '</div>';
                           $size += $file->size;
                           $sumSize += $file->size;
                        endforeach;
                        echo '</div>';
                     endif;
                     echo '</div>';
                     if($size > 0):
                        echo '<div id="lr'.$lr->id.'">';
                        $imageSize = round(($size/1000),2);
                        echo '<p class="m-0 p-0" style="background-color: lightsteelblue;"><b>Speichergröße Learningroom</b>: '.$imageSize.' KB</p>';
                        echo '</div>';
                     endif;
                  endforeach;
                  
               endif;
               ?>
            </div>
            <div id="folderTreeQuizze" class="card bg-light p-1 mt-2" style="font-size: 12px;">
               <?php
               if($this->customer['quizze']):
                  foreach($this->customer['quizze'] as $qz):
                     $size = 0;
                     echo '<div id="qz'.$qz->id.'">';
                     echo 'QZ'.str_pad($qz->id,8,'0',STR_PAD_LEFT).' <b>'.$qz->title.'</b>';
                     $files = $load->getFiles($this->customer['customerID'], 'quizz',$qz->id);
                     if($files):
                        echo '<div class="row pl-3 pr-3">';
                        foreach($files as $file):
                           echo '<div id="file'.$file->id.'" class="col-12 col-sm-1 p-1">';
                           echo '<img onclick="openLightbox(&quot;'.$file->path.'&quot;,'.$file->id.','.$file->size.',&quot;'.$file->extention.'&quot;);" max-width="80px" src="'.$file->path.'" />';
                           echo '</div>';
                           $size += $file->size;
                           $sumSize += $file->size;
                        endforeach;
                        echo '</div>';
                     endif;
                     echo '</div>';
                     if($size > 0):
                        echo '<div id="lr'.$lr->id.'">';
                        $imageSize = round(($size/1000),2);
                        echo '<p class="m-0 p-0" style="background-color: lightsteelblue;"><b>Speichergröße Learningroom</b>: '.$imageSize.' KB</p>';
                        echo '</div>';
                     endif;
                  endforeach;
               endif;
               ?>
            </div>
            <div id="folderTreeTemplates" class="card bg-light p-1 mt-2" style="font-size: 12px;">
               <?php
               if($this->customer['templates']):
                  foreach($this->customer['templates'] as $tm):
                     $size = 0;
                     echo '<div id="tm'.$tm->id.'">';
                     echo 'TM'.str_pad($tm->id,8,'0',STR_PAD_LEFT).' <b>'.$tm->title.'</b>';
                     $files = $load->getFiles($this->customer['customerID'], 'template',$tm->id);
                     if($files):
                        echo '<div class="row pl-3 pr-3">';
                        foreach($files as $file):
                           echo '<div id="file'.$file->id.'" class="col-12 col-sm-1 p-1">';
                           echo '<img onclick="openLightbox(&quot;'.$file->path.'&quot;,'.$file->id.','.$file->size.',&quot;'.$file->extention.'&quot;);" max-width="80px" src="'.$file->path.'" />';
                           echo '</div>';
                           $size += $file->size;
                           $sumSize += $file->size;
                        endforeach;
                        echo '</div>';
                     endif;
                     echo '</div>';
                     if($size > 0):
                        echo '<div id="lr'.$lr->id.'">';
                        $imageSize = round(($size/1000),2);
                        echo '<p class="m-0 p-0" style="background-color: lightsteelblue;"><b>Speichergröße Learningroom</b>: '.$imageSize.' KB</p>';
                        echo '</div>';
                     endif;
                  endforeach;
               endif;
               ?>
            </div>
            <a class="btn btn-sm btn-success text-white pt-0 pb-0 float-left mt-2" onClick="newFolder()">Neuer Ordner</a>
         </div>
         <div class="col-12 col-sm-6">
         <div class="card bg-light p-1 d-inline-block w-100">
               <div class="card-header">
                  <h5 class="m-0">Speichergröße</h5>
               </div>
               <div class="card-body>">
                  <div class="row">
                     <div class="col-12 col-md-8">Maximal</div>
                     <div class="col-12 col-md-4 text-right">40.000 KB (40 MB)</div>
                  </div>
                  <div class="row">
                     <?php
                     $resultF = $sumSize / 1000;
                     ?>
                     <div class="col-12 col-md-8">Aktuell</div>
                     <div class="col-12 col-md-4 text-right"><?php echo number_format($resultF,0,',','.');?> KB (<?php echo number_format($resultF/1000,2,',','.');?> MB)</div>
                  </div>
                  <div class="row">
                     <div class="col-12">
                        <div class="progress">
                           <?php
                           $max  = 40000000;
                           $cu   = $sumSize;
                           $percent = (100 * $cu) / $max;
                           if($percent >= 100):
                              $percent = 100;
                           endif;   
                           ?>
                           <div class="progress-bar" role="progressbar" style="width: <?php echo $percent;?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?php echo round($percent,0);?>%</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<input type="hidden" id="currentSize" value="<?php echo $this->item->currentSize;?>" />
<input type="hidden" name="task" value="" />
</form>
<div id="save">
   Daten werden gespeichert
   <img width="80px" src="images/logo_solo.png" />
</div>
<form action="index.php?option=com_jclassroom&task=classroom2.loadFileM" method="post" name="uploaderFormMaterial" id="uploaderFormMaterial" enctype="multipart/form-data">
   <input id="inputMaterial" title="file input" type="file" accept="" multiple name="inputMaterial[]" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom2.loadFileR" method="post" name="uploaderFormReserve" id="uploaderFormReserve" enctype="multipart/form-data">
   <input id="inputReserve" title="file input" type="file" accept="" multiple="" name="inputReserve[]" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
</form>
<form action="index.php?option=com_jclassroom&task=classroom2.loadFileS" method="post" name="uploaderFormStudent" id="uploaderFormStudent" enctype="multipart/form-data">
   <input id="inputStudent" title="file input" type="file" accept="" multiple="" name="inputStudent[]" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" id="uploadStudentUserID" name="uploadStudentUserID" value="<?php echo $this->item->id;?>" />
</form>
<script>
let classroomID = $('#classroomID').val();
$('#openUploader').click(function() {
   $('#inputMaterial').trigger('click');
});
$('#inputMaterial').on('change', function() {
   $('#save').css('display','block');
   $('#uploaderFormMaterial').submit();
});
$('#openUploaderR').click(function() {
   $('#inputReserve').trigger('click');
});
$('#inputReserve').on('change', function() {
   $('#save').css('display','block');
   $('#uploaderFormReserve').submit();
});
$('.openStudentUploader').click(function() {
   var studentID = $(this).attr('data-id');
   $('#uploadStudentUserID').val(studentID);
   $('#inputStudent').trigger('click');
});
$('#inputStudent').on('change', function() {
   $('#save').css('display','block');
   $('#uploaderFormStudent').submit();
});
function newFolder() {
   let newfolder = prompt('Bitte geben Sie einen Namen ein.');
   jQuery.ajax({
         url: "index.php?option=com_jclassroom&task=classroom2.newFolder",
         data: {
         newfolder:newfolder,
         classroomID:classroomID
      },
         method: 'POST',
         //dataType: 'json',
         success: function( data ) {
            $('#folderTree').append(
               '<p id="theFolderName_' + newfolder + '" class="mt-1 mb-1 p-1" style="background-color: #a1a1a1;">' + 
               '<i class="fa fa-folder-o" title="Verzeichnis umbenennen" onclick="renameFolder(&quot;' + newfolder + '&quot;);"></i> ' +
               '<span>' + newfolder + '</span>' + 
               '<i class="fas fa-trash-alt float-right pt-1" title="Verzeichnis löschen" onclick="deleteFolder(&quot;' + newfolder + '&quot;);"></i>' +
               '</p>'
            );
         }
   });
}
function deleteFolder(id) {
   if(confirm("Soll dieses Verzeichnis gelöscht werden?") == true) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.deleteFolder",
         data: {
            classroomID:classroomID,
            folder:id
         },
         //dataType: 'json',
         success: function( data ) {
            $('#theFolderName_' + id).remove();
         }
      });
   }
}
function deleteFile(fileID) {
   if(confirm("Soll diese Datei gelöscht werden?") == true) {
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.deleteFile",
         data: {fileID:fileID},
         //dataType: 'json',
         success: function( data ) {
            $('#file' + data).remove();
            $('#file' + fileID).remove();
         }
      });
   }
}
function deleteFileFD(path, name, id) {
   if(confirm('Möchten Sie die Datei ' + name + ' endgültig löschen?') == true) {
      jQuery.ajax({
          url: "index.php?option=com_jclassroom&task=classroom2.deleteFileFD",
          data: {path:path,id:id},
          method: 'POST',
          //dataType: 'json',
          success: function( data ) {
               $('#file' + name).remove();
               if(id) {
                  $('#file' + id).remove();
               }
          }
      });
   }
}
function renameFolder(filename) {
   if(confirm('Möchten Sie das Verzeichnis ' + filename + ' umbenennen?') == true) {
      let newName = prompt('Bitte geben Sie den neuen Verzeichnisnamen ein.', filename);
      if(newName.indexOf(" ") != -1) {
         alert('Bitte keine Leerzeichen verwenden');
         return false;
      }
      jQuery.ajax({
          url: "index.php?option=com_jclassroom&task=classroom2.renameFolder",
          data: {
            filename:filename,
            newName:newName,
            classroomID:classroomID
         },
          method: 'POST',
          //dataType: 'json',
          success: function( data ) {
               $('#theFolderName_' + filename + ' span').html(newName);
               $('#theFolderName_' + filename + ' i.fa-file-o').attr('onClick', 'renameFolder("' + newName + '")');
               $('#theFolderName_' + filename + ' i.fa-trash-alt').attr('onClick', 'deleteFolder("' + newName + '")');
          }
      });
   }
}
function renameFile(filename) {
   if(confirm('Möchten Sie die Datei ' + filename + ' umbenennen?') == true) {
      let newName = prompt('Bitte geben Sie den neuen Dateinamen ein.', filename);
      
      jQuery.ajax({
          url: "index.php?option=com_jclassroom&task=classroom2.renameFile",
          data: {
            filename:filename,
            newName:newName,
            classroomID:classroomID
         },
          method: 'POST',
          //dataType: 'json',
          success: function( data ) {
               let searchName = filename.replace('.','');
               $('#file' + searchName + ' .theFileName').html(newName);
               $('#file' + searchName + ' i.fa-file-o').attr('onClick', 'renameFile("' + newName + '")');
               $('#file' + searchName + ' i.fa-trash-o').attr('onClick', 'deleteFile("' + newName + '")');
               $('#file' + searchName).attr('id', 'file' + newName);
          }
      });
   }
}
function stageFile(id) {
   if(confirm('Soll diese Datei für die Teilnehmer freigegeben werden?') == true) {
      var classroomID = jQuery('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.stageFile",
         data: {id:id, classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#stageFile_' + id).removeClass('d-inline-block').addClass('d-none');
            $('#unstageFile_' + id).removeClass('d-none').addClass('d-inline-block');
         }
      });
   }
}
function unstageFile(id) {
   if(confirm('Soll diese Datei für die Teilnehmer versteckt werden?') == true) {
      var classroomID = jQuery('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=classroom2.unstageFile",
         data: {id:id, classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#stageFile_' + id).removeClass('d-none').addClass('d-inline-block');
            $('#unstageFile_' + id).removeClass('d-inline-block').addClass('d-none');
         }
      });
   }
}
</script>