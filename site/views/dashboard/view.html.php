<?php
/**
 *  	DATS Torsten Scheel
 * 	  	JClassroom 1.0
 * 		LMS-System
 * 	 	(C) 2020. All Rights reserved.
 * 		05. April 2020
 *		Development: Torsten Scheel
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class JclassroomViewDashboard extends JViewLegacy
{

	function display($tpl = null)
	{
		$session = JFactory::getSession();
        $session->set('group','superuser');
        // Load the systemname
        JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
        $table      = JTable::getInstance('Configuration','JclassroomTable',array());
        // Load the systemdata
        $load       = array('customerID' => 0, 'parameter' => 'systemLink');
        $table->load($load);
        $systemLink = $table->wert;
        $load       = array('customerID' => 0, 'parameter' => 'systemName');
        $table->load($load);
        $systemName = $table->wert;
        $session->set('systemname', $systemName);
    	$app = JFactory::getApplication();
    	$user = JFactory::getUser();
    	$groups = JAccess::getGroupsByUser($user->id);
        // IF User is Systemadministrator
        if(in_array(8,$groups)) {
            $session->set('group','superuser');
            $session->set('customerID',0);
            $app->redirect(JURI::Root().'manager-administrator');
        }
        // IF User is Trainer
    	if(in_array(10,$groups)) {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('Trainer','JclassroomTable',array());
            $load   = array('tblUserID' => $user->id);
            $table->load($load);
            $session->set('customerID',$table->customerID);
            $session->set('group','trainer');
            // If User like use a new plan
            if($session->get('setAccount') == 1):
                $app->redirect(JURI::Root().'newplan?r=4');
            else:
                $app->redirect(JURI::Root().'manager-trainer');
            endif;
    	}
        // IF User is Student
    	if(in_array(11,$groups)) {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('Student','JclassroomTable',array());
            $load   = array('tblUserID' => $user->id);
            $table->load($load);
            $session->set('customerID',$table->customerID);
            $session->set('group','student');
            // If User like use a new plan
            if($session->get('setAccount') == 1):
                $app->redirect(JURI::Root().'newplan?r=4');
            else:
                $app->redirect(JURI::Root().'dashboard-students');
            endif;
    	}
        // IF User is Customer
        if(in_array(12,$groups)) {
            $session->set('group','customer');
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('Customer_administratoren','JclassroomTable',array());
            $load   = array('userID' => $user->id);
            $table->load($load);
            $session->set('customerID',$table->customerID);
            // If User like use a new plan
            if($session->get('setAccount') == 1):
                $app->redirect(JURI::Root().'newplan?r=4');
            else:
                $app->redirect(JURI::Root().'manager-customer');
            endif;
            
        }
        // IF User is Customer
        if(in_array(13,$groups)) {
            $session->set('group','customer');
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('Customer_administratoren','JclassroomTable',array());
            $load   = array('userID' => $user->id);
            $table->load($load);
            $session->set('customerID',$table->customerID);
            // If User like use a new plan
            if($session->get('setAccount') == 1):
                $app->redirect(JURI::Root().'newplan?r=4');
            else:
                $app->redirect(JURI::Root().'manager-customer');
            endif;
        }
		// Display the view
		parent::display($tpl);
	}
}
