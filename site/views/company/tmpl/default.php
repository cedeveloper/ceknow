<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     2.3.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2015. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
?>
<form action="<?php echo JRoute::_('index.php?option=com_inclure&view=mandant'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="row" style="margin-top: 100px;">
        <div class="col-12 col-sm-3"></div>
        <div class="col-12 col-sm-6">
            <h3 class="text-center text-white">Ihr kostenloser 14-Tage-Test</h3>
            <h4 class="text-center text-white">Absolut kostenlos und unverbindlich. Keine Zahlungsdaten erforderlich</h4>
            <div class="form-horizontal mt-5">
                <div class="form-group row">
                    <div class="col-12">
                      <?php echo $this->form->getInput('first_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                      <?php echo $this->form->getInput('last_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                      <?php echo $this->form->getInput('email'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-3"></div>
    </div>
<input type="hidden" name="option" value="com_inclure" />
<input type="hidden" name="task" value="kunde.edit" />
<?php echo JHtml::_('form.token'); ?>
</form>