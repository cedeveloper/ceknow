<?php
/**
 *  	DATS Torsten Scheel
 * 	  	InVenta 3.0
 * 	 	(C) 2018. All Rights reserved.
 * 		12. Oktober 2018
 *		Editor: Torsten Scheel
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<form action="<?php echo 'index.php?option=com_jclassroom&task=manager.vertify';?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal">
	<h1 style="font-size: 32px;">Willkommen, <?php echo $this->name;?>, bei ceKnow!</h1>
	<p>Vielen Dank für Ihre Bestätigung.<br/>
	Ihr Demozugang ist nun aktiviert und freigeschaltet.</p>
	<span class="card bg-success p-1 d-inline-block mb-3 text-white"><?php echo $this->plan_time;?></span>
	<p>Bitte gehen Sie zur Anmeldung und melden Sie sich mit Ihrem Kundenkonto an. Ihr Benutzername ist Ihre E-Mail-Adresse, die Sie bei der Registrierung angegeben haben.</p>
	<a href="login" class="btn btn-primary text-white">Zur Anmeldung</a>
</form>
<script type="text/javascript">
	
</script>