<?php
/**
 *  	DATS Torsten Scheel
 * 	  	JClassroom 1.0
 * 		LMS-System
 * 	 	(C) 2020. All Rights reserved.
 * 		05. April 2020
 *		Development: Torsten Scheel
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class JclassroomViewDoit extends JViewLegacy
{

	function display($tpl = null)
	{
		$input      = JFactory::getApplication()->input;
        $token      = $input->get('SMA', '', 'STR');

        $id     = base64_decode($token);
        $id     = explode('__', $id);
        if($id):
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('User','JclassroomTable',array());
            $table->load($id[1]);
            $table->block = 0;
            $table->store();
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('Customer_administratoren','JclassroomTable',array());
            $load   = array('userID' => $id[1]);
            $table->load($load);
            $this->name = $table->first_name.' '.$table->last_name;
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $table  = JTable::getInstance('Customer','JclassroomTable',array());
            $load   = array('id' => $id[0]);
            $table->load($load);
            $this->plan_time = 'Ihr Demozugang ist gültig vom: <b>'.date('d.m.Y',strtotime($table->plan_from)).'</b> bis <b>'.date('d.m.Y', strtotime($table->plan_to)).'</b>.';
        endif;
		// Display the view
		parent::display($tpl);
	}
}
