<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
require_once(JPATH_COMPONENT.'/views/customer/tmpl/newContact.php');
require_once(JPATH_COMPONENT.'/views/customer/tmpl/editContact.php');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

    <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a class="btn btn-success text-white" onclick="Joomla.submitform('company.simpleSave')">Speichern</a>
         <a class="btn btn-secondary text-white" onclick="Joomla.submitform('company.save')">Speichern & Schließen</a>
      <?php endif;?>
        <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
    </div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
         <li class="nav-item">
            <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Allgemein</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="asp-tab" data-toggle="tab" href="#asp" role="tab" aria-controls="asp" aria-selected="false">Ansprechpartner</a>
         </li>
    </ul>
    <div class="tab-content" id="tabContent">
         <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
            <div class="form-horizontal mt-3 w-50">
               <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('customer_number'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('customer_number'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('name'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('address'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('address'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('postcode'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('postcode'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('city'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('city'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('phone'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('phone'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('email'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-2 col-form-label">
                      <?php echo $this->form->getLabel('web'); ?>
                    </div>
                    <div class="col-12 col-sm-10">
                      <?php echo $this->form->getInput('web'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-6 col-form-label">
                      <?php echo $this->form->getLabel('showVertifyAGB'); ?>
                    </div>
                    <div class="col-12 col-sm-6">
                      <?php echo $this->form->getInput('showVertifyAGB'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-6 col-form-label">
                      <?php echo $this->form->getLabel('showVertifyDS'); ?>
                    </div>
                    <div class="col-12 col-sm-6">
                      <?php echo $this->form->getInput('showVertifyDS'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-6 col-form-label">
                      <?php echo $this->form->getLabel('showVertifyFilm'); ?>
                    </div>
                    <div class="col-12 col-sm-6">
                      <?php echo $this->form->getInput('showVertifyFilm'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-6 col-form-label">
                      <?php echo $this->form->getLabel('published'); ?>
                    </div>
                    <div class="col-12 col-sm-6">
                      <?php echo $this->form->getInput('published'); ?>
                    </div>
                </div>
            </div>
         </div>
         <div class="tab-pane fade" id="asp" role="tabpanel" aria-labelledby="asp-tab">
             <div class="form-horizontal mt-3">
               <a class="btn btn-success text-white mb-3" onclick="newContact();">Neuer Ansprechpartner</a>
               <h5 class="m-0">Ansprechpartner</h5>
               <div class="row">
                  <div class="col-12 col-sm-2">Aktionen</div>
                  <div class="col-12 col-sm-4">Name</div>
                  <div class="col-12 col-sm-2">Funktion</div>
                  <div class="col-12 col-sm-4">Kontaktdaten</div>
               </div>
               <div id="contacts" class="bg-light p-1">
                  <?php
                  if($this->item->company_contacts):
                     foreach($this->item->company_contacts as $contact):
                        echo '<div id="contact'.$contact->id.'" class="contact" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                        echo '<div class="row">';
                        echo '<div class=" col-12 col-sm-2">';
                        echo '<span class="badge bg-dark text-white mr-2">ID: '.$contact->id.'</span>';
                        echo '<i title="Ansprechpartner bearbeiten" onclick="editContact('.$contact->id.');" class="fa fa-pencil mr-2"></i>';
                        echo '<i title="Ansprechpartner löschen" onclick="deleteContact('.$contact->id.');" class="fa fa-trash-o"></i>';
                        echo '</div>';
                        echo '<div id="edit_contact_feld1" class=" col-12 col-sm-4">';
                        echo $contact->salutation.' '.$contact->first_name.' '.$contact->last_name;
                        echo '</div>';
                        echo '<div id="edit_contact_feld2" class=" col-12 col-sm-2">';
                        echo $contact->function;
                        echo '</div>';
                        echo '<div id="contact_contactdata" class=" col-12 col-sm-4">';
                        if($contact->phone):
                           echo '<span id="edit_contact_feld3" class="badge bg-success text-white"><i class="fa fa-phone"></i> '.$contact->phone.'</span>';
                        endif;
                        if($contact->phone && $contact->email):
                           echo ' | ';
                        endif;
                        if($contact->email):
                           echo '<span id="edit_contact_feld4" class="badge bg-danger text-white"><i class="fa fa-envelope"></i> '.$contact->email.'</span>';
                        endif;
                        echo '</div>';
                        echo '</div>';
                        echo '</div>';
                     endforeach;
                  else:
                     echo '<div id="contactPlaceholder" class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                     echo 'Keine Ansprechpartner gefunden';
                     echo '</div>';
                  endif;
                  ?>
               </div>
            </div>
       	</div>
    </div>
<input type="hidden" id="id" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({
      rules: {
         'jform[email]': {
            required: true,
            email: true
         },
         'jform[company_name]': {
            required: true
         },
         'jform[last_name]': {
            required: true
         }
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[company_name]': 'Bitte geben Sie einen Firmennamen ein.',
         'jform[last_name]': 'Bitte geben Sie Ihren Nachnamen ein.'
      },
      submitHandler: function(form) {
         form.submit();
      }
   });
   function newContact() {
      var id    = $('#id').val();
      if(!id) {
         alert('Bitte erfassen und speichern Sie zunächst die Firmendaten');
         return false;
      }
      $('#newContact').modal('show');
   }
   $('#saveNewContact').on('click', function() {
      saveNewContact();
   });
   function saveNewContact() {
      var customerID    = $('#id').val();
      var salutation    = $('#contact_salutation option:selected').val();
      var first_name    = $('#contact_first_name').val();
      var last_name     = $('#contact_last_name').val();
      var functionS     = $('#contact_function').val();
      var phone         = $('#contact_phone').val();
      var email         = $('#contact_email').val();
      $('#newContact').modal('hide');
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=company.addContact",
         data: {
            customerID:customerID,
            salutation:salutation,
            first_name:first_name,
            last_name:last_name,
            functionS:functionS,
            phone:phone,
            email:email
         },
         //dataType: 'json',
         success: function( data ) {
            $('#contactPlaceholder').remove();
            $('#contacts').append(
               '<div id="contact' + data + ' class="contact" style="border-bottom: 1px solid #aaa;padding: 4px 0 4px 0;">' + 
               '<div class="row">' + 
               '<div class="col-12 col-sm-2">' + 
               '<span class="badge bg-dark text-white mr-2">ID: ' + data + '</span>' +
               '<i title="Ansprechpartner bearbeiten" onclick="editContact(' + data + ');" class="fa fa-pencil mr-2"></i>'+
               '<i title="Ansprechpartner löschen" onclick="deleteContact(' + data + ');" class="fa fa-trash-o"></i>' +
               '</div>' +
               '<div class="col-12 col-sm-4">' + 
               salutation + ' ' + first_name + ' ' + last_name + 
               '</div>' +
               '<div class="col-12 col-sm-2">' + 
               functionS +
               '</div>' +
               '<div class="col-12 col-sm-4">' + 
               '<span class="badge bg-success text-white"><i class="fa fa-phone"></i> ' + phone + '</span> | <span class="badge bg-danger text-white"><i class="fa fa-envelope"></i> ' + email + '</span>' + 
               '</div>' +
               '</div>' +
               '</div>'
            );
         }
      });
   }
   function editContact(id) {
      $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=company.getContact",
            data: {
               id:id
            },
            dataType: 'json',
            success: function( data ) {
               $('#edit_contact_salutation').val(data['salutation']);
               jQuery('#edit_contact_salutation').select2({
                  placeholder: 'Bitte auswählen'
               });
               $('#edit_contact_first_name').val(data['first_name']);
               $('#edit_contact_last_name').val(data['last_name']);
               $('#edit_contact_phone').val(data['phone']);
               $('#edit_contact_email').val(data['email']);
               $('#edit_contact_function').val(data['function']);
               $('#editContactID').val(id);
            }
         });
      $('#editContact').modal('show');
   }
   $('#saveEditContact').on('click', function() {
      var contactID     = $('#editContactID').val();
      var salutation    = $('#edit_contact_salutation option:selected').val();
      var first_name    = $('#edit_contact_first_name').val();
      var last_name     = $('#edit_contact_last_name').val();
      var functionS     = $('#edit_contact_function').val();
      var phone         = $('#edit_contact_phone').val();
      var email         = $('#edit_contact_email').val(); 
      $('#editContact').modal('hide'); 
      $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=company.editContact",
            data: {
               contactID:contactID,
               salutation:salutation,
               first_name:first_name,
               last_name:last_name,
               functionS:functionS,
               phone:phone,
               email:email
            },
            dataType: 'json',
            success: function( data ) {
               $('#contact' + contactID + ' #edit_contact_feld1').html(salutation + ' ' + first_name + ' ' + last_name);
               $('#contact' + contactID + ' #edit_contact_feld2').html(functionS);
               if(phone) {
                  if($('#contact' + contactID + ' #edit_contact_feld3')) {
                     $('#contact' + contactID + ' #edit_contact_feld3').html('<i class="fa fa-phone"></i>' + phone );
                  } else {
                     $('#contact' + contactID + ' #contact_contactdata').append(
                        '<span id="contact_feld3" class="badge bg-success text-white">' + 
                        '<i class="fa fa-phone></i>' +
                        phone + 
                        '</span>'
                     );
                  }
               }
               if(email) {
                  if($('#contact' + contactID + ' #edit_contact_feld4')) {
                     $('#contact' + contactID + ' #edit_contact_feld4').html('<i class="fa fa-envelope"></i>' + email);
                  } else {
                     $('#contact' + contactID + ' #contact_contactdata').append(
                        '<span id="contact_feld4" class="badge bg-success text-white">' + 
                        '<i class="fa fa-envelope></i>' +
                        email + 
                        '</span>'
                     );
                  }
               }
            }
         });
   });
   function deleteContact(id) {
      if(confirm('Soll der Ansprechpartner mit der ID ' + id + ' gelöscht werden?') == true) {
         $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=company.deleteContact",
            data: {
               id:id
            },
            //dataType: 'json',
            success: function( data ) {
               $('#contact' + id).remove();
            }
         });
      }
   }
   jQuery('#jform_email').on('change', function() {
      var email = jQuery('#jform_email').val();
      jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
            data: {email:email},
            //dataType: 'json',
            success: function( data ) {
               if(data == 'OK') {
                  jQuery('#jform_username').val(email);
               } else {
                  alert('Die E-Mail-Adresse wird bereits verwendet.');
                  jQuery('#jform_email').val('');
                  return false;
               }
            }
        });
   });
</script>