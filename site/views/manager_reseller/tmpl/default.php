<?php
/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

$doc = JFactory::getDocument();
?>
<div class="inflow admin row">
	<div class="col-12">
		<div class="row">
			<div class="col-12">
				<div class="card text-white bg-light mb-3" style="font-size: 14px;">
					<div class="card-body p-1">
						<p class="card-title text-dark"><b>Ihre Kundendaten</b></p>
						<div class="row">
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-4">
											Name:
										</div>
										<div class="col-12 col-sm-8">
											<i><?php echo $this->trainerData->name;?></i><br/>
											<i><?php echo $this->trainerData->company;?></i>
										</div>
									</div>
								</div>
							</div>
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-3">
											Anschrift:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->trainerData->adress;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->trainerData->postcode.' '.$this->trainerData->city;?></i>
										</div>
									</div>
								</div>
							</div>
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-3">
											Telefon:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->trainerData->phone;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
											E-Mail:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->trainerData->email;?></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-primary mb-3">
					<div class="card-header">Mein Konto</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier Ihr Konto</h5>
						<p class="card-text">Beschreibung zu Trainerkonto.</p>
						<a href="trainer-bearbeiten?layout=edit&id=<?php echo $this->trainerData->id;?>" class="btn btn-secondary">Zum ihrem Konto</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-success mb-3">
					<div class="card-header">Teilnehmer</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier ihre Teilnehmer</h5>
						<p class="card-text">Beschreibung zu Teilnehmer.</p>
						<a href="celearning/students" class="btn btn-secondary">Zur Teilnehmerverwaltung</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-warning mb-3">
					<div class="card-header">Learningrooms</div>
					<div class="card-body">
						<h5 class="card-title">Verwaltung der Learningrooms</h5>
						<p class="card-text">Beschreibungstext zu Learningrooms</p>
						<a href="celearning/classrooms" class="btn btn-secondary">Zur Learningroom-Verwaltung</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-danger mb-3">
					<div class="card-header">Quizze</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier Ihre Quizze</h5>
						<p class="card-text">Beschreibung zu Quizze.</p>
						<a href="celearning/sections" class="btn btn-secondary">Zu den Quizzen</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-secondary mb-3">
					<div class="card-header">Einstellungen</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier Ihre Einstellungen</h5>
						<p class="card-text">Beschreibung zu Kurse.</p>
						<a href="konfiguration" class="btn btn-light">Zu den Einstellungen</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-info mb-3" style="min-height: 205px;">
					<div class="card-header">Ihre Learningrooms als Teilnehmer</div>
					<div class="card-body">
						<h5 class="card-title">Hier finden Sie alle Learningrooms, zu denen Sie als Teilnehmer eingeladen wurden.</h5>
						<a href="meine-learningrooms" class="btn btn-secondary">Zum Ihren Learningrooms</a>
					</div>
				</div>
			</div>
		</div>
		<div class="card text-white bg-light mb-3">
			<div class="card-header text-dark">Shortcuts</div>
			<div class="card-body">
				<a href="abmelden" class="btn btn-danger">Abmelden</a>
			</div>
		</div>
	</div>
</div>