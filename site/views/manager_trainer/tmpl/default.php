<?php
/**
 * @version     1.0.0
 * @package     com_neuetermine
 * @copyright   Copyright (C) 2014. Alle Rechte vorbehalten.
 * @license     GNU General Public License Version 2 oder später; siehe LICENSE.txt
 * @author      Torsten <ts@torstenscheel.de> - http://www.torstenscheel.de
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

$doc = JFactory::getDocument();
?>
<div class="inflow admin row">
	<div class="col-12">
		<div class="row">
			<div class="col-12">
				<div class="card text-white bg-light mb-3" style="font-size: 14px;">
					<div class="card-body p-1">
						<p class="card-title text-dark"><b>Ihre Trainerdaten</b></p>
						<div class="row">
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-4">
											Name:
										</div>
										<div class="col-12 col-sm-8">
											<i><?php echo $this->trainerData->first_name.' '.$this->trainerData->last_name;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-4">
											Kunde:
										</div>
										<div class="col-12 col-sm-8">
											<i><?php echo $this->trainerData->company_name;?></i>
										</div>
									</div>
								</div>
							</div>
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-3">
											Anschrift:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->trainerData->adress;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->trainerData->postcode.' '.$this->trainerData->city;?></i>
										</div>
									</div>
								</div>
							</div>
							<div class="-col-12 col-sm-4">
								<div class="card-text text-dark p-0">
									<div class="row">
										<div class="col-12 col-sm-3">
											Telefon:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->trainerData->phone;?></i>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-3">
											E-Mail:
										</div>
										<div class="col-12 col-sm-9">
											<i><?php echo $this->trainerData->trainerEmail;?></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-primary mb-3">
					<div class="card-header">Mein Konto</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier Ihr Konto</h5>
						<a href="my-account" class="btn btn-secondary">Zum ihrem Konto</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-success mb-3">
					<div class="card-header">Teilnehmer</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier ihre Teilnehmer</h5>
						<a href="manager-trainer/students" class="btn btn-secondary">Zur Teilnehmerverwaltung</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-warning mb-3">
					<div class="card-header">Learningrooms</div>
					<div class="card-body">
						<h5 class="card-title">Verwaltung der Learningrooms</h5>
						<a href="classrooms" class="btn btn-secondary">Zur Learningroom-Verwaltung</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-danger mb-3">
					<div class="card-header">Quizze</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier Ihre Quizze</h5>
						<a href="quizze" class="btn btn-secondary">Zu den Quizzen</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-info mb-3">
					<div class="card-header">Bibliothek</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier Ihre Bibliothek</h5>
						<a href="library-days" class="btn btn-secondary">Themen</a>
						<a href="library-modules" class="btn btn-secondary">Module</a>
						<a href="library-units" class="btn btn-secondary">Units</a>
					</div>
				</div>
			</div>
			<!--<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-secondary mb-3">
					<div class="card-header">Einstellungen</div>
					<div class="card-body">
						<h5 class="card-title">Verwalten Sie hier Ihre Einstellungen</h5>
						<p class="card-text">Beschreibung zu Kurse.</p>
						<a href="konfiguration" class="btn btn-light">Zu den Einstellungen</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4">
				<div class="card text-white bg-info mb-3" style="min-height: 205px;">
					<div class="card-header">Ihre Learningrooms als Teilnehmer</div>
					<div class="card-body">
						<h5 class="card-title">Hier finden Sie alle Learningrooms, zu denen Sie als Teilnehmer eingeladen wurden.</h5>
						<a href="meine-learningrooms" class="btn btn-secondary">Zum Ihren Learningrooms</a>
					</div>
				</div>
			</div>-->
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card text-white bg-light mb-3">
					<div class="card-header text-dark">
						Einladungen zu Learningrooms
					</div>
					<div class="card-body">
						<div class="row">
						<?php 
							if($this->trainerAsStudent):
								foreach($this->trainerAsStudent as $classroom):
									require_once(JPATH_COMPONENT_SITE.'/helpers/global.php');
        							$theData = GlobalHelper::getClassroomFromTo($classroom->classroomID);
									if(!$classroom->trainer_name):
										$trainer = 'Keine Trainerdaten';
									else:		
										$trainer = $classroom->trainer_name;
									endif;
									echo '<div class="col-12 col-sm-4 mb-2">';
										echo '<div class="card p-2 text-dark" style="background-color: skyblue;">';
										echo '<h5 style="min-height: 72px;"><small style="font-size: 12px; font-weight: bold;">(LR'.str_pad($classroom->classroomID,8,'0', STR_PAD_LEFT).')</small><br/>'.$classroom->title.'</h5>';
										if($theData->start && strtotime($theData->start)):
											echo '<p class="m-0"><b class="d-inline-block" style="width: 100px;">Beginn: </b>'.date('d.m.Y',strtotime($theData->start)).'</p>';
										else:
											echo '<p class="m-0"><b class="d-inline-block" style="width: 100px;">Beginn: </b>Keine Strukturdaten</p>';
										endif;
										if($theData->end && strtotime($theData->end)):
											echo '<p class="m-0"><b class="d-inline-block" style="width: 100px;">Ende: </b>'.date('d.m.Y',strtotime($theData->end)).'</p>';
										else:
											echo '<p class="m-0"><b class="d-inline-block" style="width: 100px;">Ende: </b>Keine Strukturdaten</p>';
										endif;
										echo '<p class="m-0"><b class="d-inline-block" style="width: 100px;">Trainer: </b>'.$trainer.'</p>';
										if($classroom->orderID && $classroom->orderpositions):
											$showEdit = 'block';
											$showNew  = 'none';
											$showChange = 'none';
										else:
											$showEdit = 'none';
											if($classroom->hardware_order == 1 && $classroom->orderID):
												$showNew  = 'none';
												$showChange = 'block';
											else:
												$showNew  = 'block';
												$showChange = 'none';
											endif;
										endif;
										echo '<a href="stage-classroom?layout=edit&id='.$classroom->classroomID.'" target="_blank" class="mb-1 btn btn-secondary"><i class="fa fa-address-book-o"></i> Zum Learningroom</a>';
										echo '</div>';
									echo '</div>';
								endforeach;
							else:
								echo '<p>Keine Learningrooms gefunden.</p>';
							endif;
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card text-white bg-light mb-3">
					<div class="card-header text-dark">Shortcuts</div>
					<div class="card-body">
						<a href="my-account" class="float-left btn btn-primary">Mein Konto</a>
						<a href="abmelden" class="float-right btn btn-danger">Abmelden</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>