<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived   = $this->state->get('filter.published') == 2 ? true : false;
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="index.php?option=com_jclassroom&task=students.loadCSV" method="post" name="uploaderForm" id="uploaderForm" enctype="multipart/form-data">
    <input id="uploader" title="file input" multiple="" type="file" accept=".csv" name="uploadCSV" style="display: none;">
    <!--<button type="button" class="btn btn-warning text-white mb-3"id="openUploader">CSV Upload</button>-->
</form>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm"> 
    <?php
        // Search tools bar
        echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
    ?>
    <div class="actionleiste mt-1 mb-1">
        <!--<a class="btn btn-success text-white" onclick="Joomla.submitform('student.add')">Neuer Teilnehmer</a>-->
        <a class="btn btn-danger text-white m-1" id="deleteCustomer" onclick="deletePublished();">Einträge löschen</a>
        <!--<button type="button" class="btn btn-info" onclick="Joomla.submitform('students.saveCSV')">Liste exportieren</button>-->
        <a href="<?php echo $this->retour;?>" class="btn btn-danger text-white float-right mb-3">Zurück</a>
    </div>
    <table class="table table-striped">	
        <thead>
            <tr>
                <th width="1%" class="hidden-phone">
                    <?php echo JHtml::_('grid.checkall'); ?>
                </th>
                <th id="itemlist_header_title" class="right">
                    <?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                 <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Name', 'a.title', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Beschreibung<br>Freigabe von/bis', 'a.description', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Firma', 'a.companyID', $listDirn, $listOrder) ?>
                </th> 
                <th>Link zum Staging</th>
                <th width="200" class="nowrap left">
                    <?php echo JHtml::_('grid.sort', 'Erstellt von<br/>Erstellt am', 'a.created', $listDirn, $listOrder) ?>
                </th> 
                <th class="nowrap text-center">
                    <?php echo JHtml::_('grid.sort', JText::_('Status'), 'a.published', $listDirn, $listOrder) ?>
                </th>
            </tr>
        </thead>		
        <tbody>
        <?php if($this->items) { ?>
        <?php foreach ($this->items as $i => $item) :
        ?>
            <tr class="row<?php echo $i % 2; ?>">
            	<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
				<td class="right"><?php echo $this->escape($item->id); ?></td>
                <td>
                    <a href="<?php echo JURI::Root().'publish-quizz?layout=edit&id='. $item->id; ?>">
                        
                        <?php echo $item->title;?>
                    </a>
                </td>
                <td class="left">
                    <small><i class="fa fa-file-text"></i> <?php 
                    if($item->description):
                        echo strip_tags($item->description);
                    else:
                        echo 'Keine Beschreibung';
                    endif; 
                    ?></i></small> 
                    <hr>
                    <?php if($item->openQuizz == 1): ?>
                        <i class="fa fa-calendar-plus-o"></i> 
                        <span style="color: forestgreen;font-weight: bold;">
                            <?php echo date('d.m.Y H:i', strtotime($item->publish_up));?>
                        </span>
                         / 
                        <i class="fa fa-calendar-minus-o"></i> 
                        <span style="color: firebrick;font-weight: bold;">
                            <?php echo date('d.m.Y H:i',strtotime($item->publish_down));?>
                        </span>
                    <?php else: ?>
                        <i class="fa fa-ban"></i> 
                        <span style="color: firebrick;font-weight: bold;">
                            Privates Quizz
                        </span>
                    <?php endif; ?>
                </td>
                <td class="left">
                    <?php echo $item->companyName; ?> 
                </td>
                <td class="left"><a href="<?php echo $item->link;?>" target="_blank"><?php echo $item->link;?></a>
                </td>
                <td class="left">
                    <?php echo $item->author_name;?><br/>
                    <?php echo date('d.m.Y H:i', strtotime($item->created)); ?><br/>      
                </td>
                <?php
                if($item->published == 0) {
					$class="badge-danger";
					$wert = "<i class='fa fa-ban text-danger'></i>";
				} elseif($item->published == 1) {
					$class="badge-success";
					$wert = "<i class='fa fa-check text-success'></i>";
				}
				?>
                <td class="text-center"><?php echo $wert;?></td>
            </tr>
        <?php endforeach ?>
        <?php } else { ?>
        	<tr>
            	<td colspan="8">Es wurden keine Datensätze gefunden. Bitte überprüfen Sie die Filtereinstellungen.</td>
            </tr>
       	<?php } ?>
        </tbody>			
    </table>
<?php echo $this->pagination->getPagesLinks(); ?>
<input type="hidden" name="task" value=" " />
<input type="hidden" name="boxchecked" value="0" />
<!-- Sortierkriterien -->
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
jQuery('#openUploader').click(function() {
    jQuery('#uploader').trigger('click');
    
});
jQuery('#uploader').on('change', function() {
    jQuery('#uploaderForm').submit();
});
function deletePublished() {
    var cID = 0;
    $('[name="cid[]"]').each(function() {
        if($(this).is(':checked')) {
            cID = 1;
        }
    });
    if(cID == 0) {
        alert('Bitte wählen Sie die zu löschenden Einträge aus.');
        return false;
    }
    if(confirm('Sollen die ausgewählten Einträge gelöscht werden?') == true) {
        Joomla.submitform('published_quizzes.delete');
    }
}
</script>