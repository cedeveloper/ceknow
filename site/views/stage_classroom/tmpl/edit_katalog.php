<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$dokument = JFactory::getDocument();
$dokument->addScript('components/com_jclassroom/assets/js/jquery.validate.js' );
$dokument->addScript('components/com_jclassroom/assets/js/stage.js');

$now = new DateTime();
?>
<style>
    .introimage {
      height: 60px;
      width: 100%;
      background-position: left center;
      background-size: contain;
      background-repeat: no-repeat;
      margin: 15px 0;
   }
</style>
<form action="<?php JRoute::_('index.php?option=com_jclassroom&task=unit.checkQuestion'); ?>" method="post" name="adminForm" id="adminForm">
	<div class="stage row">
        <div class="col-12 mb-3">
            <div class="panel-footer">
                <a href="index.php?option=com_jclassroom&task=stage_classroom.prev&id=<?php echo $this->item->id;?>&st=<?php echo $this->item->step;?>" class="float-left btn btn-danger btn-lg text-white">Zurück</a>
                <?php if($this->item->step != $this->item->stepsCount): ?>
                 <a href="index.php?option=com_jclassroom&task=stage_classroom.stop&id=<?php echo $this->item->id;?>&st=<?php echo $this->item->step;?>" onclick="return confirm('Möchten Sie den Learningroom beenden?')" class="float-left ml-1 btn btn-primary btn-lg text-white">Beenden</a>
                <?php endif;?>
                <?php if($this->item->step < $this->item->stepsCount): ?>
                <a href="index.php?option=com_jclassroom&task=stage_classroom.next&id=<?php echo $this->item->id;?>&st=<?php echo $this->item->step;?>" class="float-right btn btn-success btn-lg text-white">Weiter</a>
                <?php endif; ?>
                <?php if($this->item->step == $this->item->stepsCount): ?>
                <a href="index.php?option=com_jclassroom&task=stage_classroom.stop&id=<?php echo $this->item->id;?>&st=<?php echo $this->item->step;?>" class="float-right btn btn-primary btn-lg text-white">Beenden</a>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-12">
            <span style="float: none!important;" class="d-inline-block sectionCounter badge badge-primary">Inhalt <?php echo $this->item->step;?> von <?php echo $this->item->stepsCount;?></span>
        	<h1>Learningroom: <i><?php echo $this->item->title?></i></h1>
            <?php 
            if($this->item->step == 1 && $this->item->description):
                echo '<div class="p-1 mb-2 card bg-light">';       
                echo $this->item->description;
                echo '</div>';
            endif;
            ?>
      	</div>
       	<div class="col-12">
       		<div class="row">
                <div class="col-12 "> 
                    <div class="card">
                        <div class="card-body">
                            <?php
                            if($this->item->structure->day_title):
                                $title = $this->item->structure->day_title;
                            else:
                                $title = date('d.m.Y', strtotime($this->item->structure->day_day));
                            endif;
                            ?>
                            <h2 class="badge bg-primary text-white d-block text-left" style="white-space: unset;font-size: 32px;">Thema: <i><?php echo $title?></i></h2>
                            <h3 class="badge bg-secondary text-white d-block text-left" style="white-space: unset;font-size: 24px;">Modul: <span style="font-style:italic;"><?php echo $this->item->structure->module_title?></span></h3>
                            <?php
                            if($this->item->structure->module_description):
                                echo '<div class="card bg-light p-1">'.$this->item->structure->module_description.'</div>';
                            endif;
                            ?>
                            <?php
                            $db   = JFactory::getDbo();
                            // Search for Introimage
                            $query = $db->getQuery(true);
                            $query->select(array('a.id,a.filename,a.path'));
                            $query->from($db->quoteName('#__jclassroom_files','a'));
                            $query->where('a.classroomID = '.$this->item->id);
                            $query->where('a.unitID = '.$this->item->structure->unitID);
                            $query->where('a.published = 1');
                            $query->where('a.type = "imageUnit"');
                            $db->setQuery($query);
                            $introimage = $db->loadObject();
                            if($this->item->structure->unitType == 1):
                                echo '<span class="d-inline-block" style="font-size: 12px;font-weight: bold;"><i class="fa fa-tv"></i> Freitext</span>';
                                echo '<h3 class="badge text-dark d-block text-left" style="white-space:unset;font-size: 24px;background-color: palegreen;">'.$this->item->structure->unit_title.'</h3>';
                                if($introimage):
                                    echo '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
                                endif;
                                echo '<div class="card p-2">';
                                if($this->item->structure->duration):
                                    echo '<div class="duration mb-3"><i class="fa fa-clock"></i> '.$this->item->structure->duration.' Minuten</div>';
                                endif;
                                echo $this->item->structure->content;
                                if($this->item->structure->link):
                                    echo '<a target="_blank" href="'.$this->item->structure->link.'" class="mt-1 mb-1"><span class="badge bg-primary text-white" style="font-size: 16px;"><i class="fa fa-link"></i> '.$this->item->structure->link.'</span></a>';
                                endif;
                                if($this->item->files):
                                    foreach($this->item->files as $file):
                                        echo '<a href="'.JURI::Root().$file->path.'" target="_blank" class="mt-1 mb-1"><span class="badge bg-secondary text-white" style="font-size: 16px;"><i class="fa fa-file"></i> '.$file->filename.'</span></a>';
                                    endforeach;
                                endif;
                                echo '</div>';
                            endif;
                            if($this->item->structure->unitType == 9):
                                echo '<span class="d-inline-block" style="font-size: 12px;font-weight: bold;"><i class="fa fa-wrench"></i> Aufgabe</span>';
                                echo '<h3 class="badge text-dark d-block text-left" style="min-height: 30px;white-space:unset;font-size: 24px;background-color: paleturquoise;">'.$this->item->structure->unit_title.'</h3>';
                                if($introimage):
                                    echo '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
                                endif;
                                echo '<div class="card p-2">';
                                if($this->item->structure->duration):
                                    echo '<div class="duration mb-3"><i class="fa fa-clock"></i> '.$this->item->structure->duration.' Minuten</div>';
                                endif;
                                echo $this->item->structure->content;
                                echo '<textarea id="userresolution" style="height: 150px;" class="mb-2" placeholder="Gib hier Deine Lösung ein">'.$this->item->resolution->result.'</textarea>';
                                echo '<button type="button" class="btn btn-sm btn-warning text-white mb-2 d-inline-block" onclick="saveUserResolution();">Deine Lösung speichern</button>';
                                echo '<button type="button" class="btn btn-sm btn-success text-white mb-2" onclick="uploadUnit();">Deine Lösung hochladen</button>';
                                if($this->item->structure->resolution):
                                    echo '<button type="button" class="btn btn-sm btn-primary text-white" onclick="openResolution();">offizielle Lösung anzeigen</button>';
                                    echo '<div id="resolution" class="resolution mt-3" style="display: none;">';
                                    echo '<h5>Lösung</h5>';
                                    echo '<div class="card bg-light p-1">'.$this->item->structure->resolution.'</div>';
                                    echo '</div>';
                                endif;
                                if($this->item->structure->link):
                                    echo '<a target="_blank" href="'.$this->item->structure->link.'" class="mt-1 mb-1"><span class="badge bg-primary text-white" style="font-size: 16px;"><i class="fa fa-link"></i> '.$this->item->structure->link.'</span></a>';
                                endif;
                                if($this->item->files):
                                    foreach($this->item->files as $file):
                                        echo '<a href="'.JURI::Root().$file->path.'" target="_blank" class="mt-1 mb-1"><span class="badge bg-secondary text-white" style="font-size: 16px;"><i class="fa fa-file"></i> '.$file->filename.'</span></a>';
                                    endforeach;
                                endif;
                                echo '</div>';
                            endif;
                            if($this->item->structure->unitType == 2):
                                echo '<h3 class="badge text-dark d-block text-left" style="font-size: 18px;background-color: #00c9ff;">'.$this->item->structure->unit_title.'<span style="font-size: 12px;font-weight: bold;"> (virtual classroom)</span></h3>';
                                    if($introimage):
                                        echo '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
                                    endif;
                                echo '<div class="card p-2">';
                                if($this->item->structure->duration):
                                    echo '<div class="duration mb-3"><i class="fa fa-clock"></i> '.$this->item->structure->duration.' Minuten</div>';
                                endif;
                                echo $this->item->structure->content;
                                if($this->item->structure->link):
                                    echo '<a target="_blank" href="'.$this->item->structure->link.'" class="mt-1 mb-1"><span class="badge bg-primary text-white" style="font-size: 16px;"><i class="fa fa-link"></i> '.$this->item->structure->link.'</span></a>';
                                endif;
                                if($this->item->files):
                                    foreach($this->item->files as $file):
                                        echo '<a href="'.JURI::Root().$file->path.'" target="_blank" class="mt-1 mb-1"><span class="badge bg-secondary text-white" style="font-size: 16px;"><i class="fa fa-file"></i> '.$file->filename.'</span></a>';
                                    endforeach;
                                endif;
                                echo '</div>';
                            endif;
                            if($this->item->structure->unitType == 3):
                                echo '<h3 class="badge text-white d-block text-left" style="font-size: 18px;background-color: #343A40;">'.$this->item->structure->unit_title.'<span style="font-size: 12px;font-weight: bold;"> (Pause)</span></h3>';
                                if($introimage):
                                    echo '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
                                endif;
                                echo '<div class="card p-2">';
                                if($this->item->structure->duration):
                                    echo '<div class="duration mb-3"><i class="fa fa-clock"></i> '.$this->item->structure->duration.' Minuten</div>';
                                endif;
                                echo $this->item->structure->content;
                                if($this->item->structure->link):
                                    echo '<a target="_blank" href="'.$this->item->structure->link.'" class="mt-1 mb-1"><span class="badge bg-primary text-white" style="font-size: 16px;"><i class="fa fa-link"></i> '.$this->item->structure->link.'</span></a>';
                                endif;
                                if($this->item->files):
                                    foreach($this->item->files as $file):
                                        echo '<a href="'.JURI::Root().$file->path.'" target="_blank" class="mt-1 mb-1"><span class="badge bg-secondary text-white" style="font-size: 16px;"><i class="fa fa-file"></i> '.$file->filename.'</span></a>';
                                    endforeach;
                                endif;
                                echo '</div>';
                            endif;
                            if($this->item->structure->unitType == 4):
                                echo '<h3 class="badge text-dark d-block text-left" style="font-size: 18px;background-color: darkorange;">'.$this->item->structure->unit_title.'<span style="font-size: 12px;font-weight: bold;"> (Quizz)</span></h3>';
                                if($introimage):
                                    echo '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
                                endif;
                                echo '<div class="card p-2">';
                                if($this->item->structure->duration):
                                    echo '<div class="duration mb-3"><i class="fa fa-clock"></i> '.$this->item->structure->duration.' Minuten</div>';
                                endif;
                                echo $this->item->structure->content;
                                if($this->item->structure->quizzID):
                                    echo '<a href="'.JURI::Root().'stage?layout=edit&sT=lr&unitID='.$this->item->structure->quizzID.'&clr='.$this->item->id.'&q=-1'.$this->item->structure->link.'aaa" target="_blank" class="mt-1 mb-1"><span class="badge bg-primary text-white" style="font-size: 16px;"><i class="fa fa-link"></i> Zum Quizz</span></a>';
                                endif;
                                echo '</div>';
                            endif;
                            if($this->item->structure->unitType == 5):
                                echo '<h3 class="badge text-dark d-block text-left" style="font-size: 18px;background-color: #ff3600;">'.$this->item->structure->unit_title.'<span style="font-size: 12px;font-weight: bold;"> (Video)</span></h3>';
                                if($introimage):
                                    echo '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
                                endif;
                                echo '<div class="card p-2">';
                                if($this->item->structure->duration):
                                    echo '<div class="duration mb-3"><i class="fa fa-clock"></i> '.$this->item->structure->duration.' Minuten</div>';
                                endif;
                                echo $this->item->structure->content;
                                if($this->item->structure->link):
                                    echo '<a target="_blank" href="'.$this->item->structure->link.'" class="mt-1 mb-1"><span class="badge bg-primary text-white" style="font-size: 16px;"><i class="fa fa-link"></i> '.$this->item->structure->link.'</span></a>';
                                endif;
                                if($this->item->files):
                                    foreach($this->item->files as $file):
                                        echo '<a href="'.JURI::Root().$file->path.'" target="_blank" class="mt-1 mb-1"><span class="badge bg-secondary text-white" style="font-size: 16px;"><i class="fa fa-file"></i> '.$file->filename.'</span></a>';
                                    endforeach;
                                endif;
                                echo '</div>';
                            endif;
                            if($this->item->structure->unitType == 6):
                                echo '<h3 class="badge text-dark d-block text-left" style="font-size: 18px;background-color: lightseagreen;">'.$this->item->structure->unit_title.'<span style="font-size: 12px;font-weight: bold;"> (Gruppenübung)</span></h3>';
                                if($introimage):
                                    echo '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
                                endif;
                                echo '<div class="card p-2">';
                                if($this->item->structure->duration):
                                    echo '<div class="duration mb-3"><i class="fa fa-clock"></i> '.$this->item->structure->duration.' Minuten</div>';
                                endif;
                                echo $this->item->structure->content;
                                if($this->item->structure->link):
                                    echo '<a target="_blank" href="'.$this->item->structure->link.'" class="mt-1 mb-1"><span class="badge bg-primary text-white" style="font-size: 16px;"><i class="fa fa-link"></i> '.$this->item->structure->link.'</span></a>';
                                endif;
                                if($this->item->files):
                                    foreach($this->item->files as $file):
                                        echo '<a href="'.JURI::Root().$file->path.'" target="_blank" class="mt-1 mb-1"><span class="badge bg-secondary text-white" style="font-size: 16px;"><i class="fa fa-file"></i> '.$file->filename.'</span></a>';
                                    endforeach;
                                endif;
                                echo '</div>';
                            endif;
                            if($this->item->structure->unitType == 7):
                                echo '<h3 class="badge text-dark d-block text-left" style="font-size: 18px;background-color: lightsalmon;">'.$this->item->structure->unit_title.'<span style="font-size: 12px;font-weight: bold;"> (Video)</span></h3>';
                                if($introimage):
                                    echo '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
                                endif;
                                echo '<div class="card p-2">';
                                if($this->item->structure->duration):
                                    echo '<div class="duration mb-3"><i class="fa fa-clock"></i> '.$this->item->structure->duration.' Minuten</div>';
                                endif;
                                echo $this->item->structure->content;
                                if($this->item->structure->link):
                                    echo '<a target="_blank" href="'.$this->item->structure->link.'" class="mt-1 mb-1"><span class="badge bg-primary text-white" style="font-size: 16px;"><i class="fa fa-link"></i> '.$this->item->structure->link.'</span></a>';
                                endif;
                                echo '</div>';
                            endif;
                            if($this->item->structure->unitType == 8):
                                echo '<h3 class="badge text-dark d-block text-left" style="font-size: 18px;background-color: #DC143C;">'.$this->item->structure->unit_title.'<span style="font-size: 12px;font-weight: bold;"> (Feedback)</span></h3>';
                                    if($introimage):
                                        echo '<div class="introimage" style="background-image: URL(&quot;'.$introimage->path.'&quot;);"></div>';
                                    endif;
                                echo '<div class="card p-2">';
                                if($this->item->structure->duration):
                                    echo '<div class="duration mb-3"><i class="fa fa-clock"></i> '.$this->item->structure->duration.' Minuten</div>';
                                endif;
                                echo $this->item->structure->content;
                                if($this->item->structure->link):
                                    echo '<a target="_blank" href="'.$this->item->structure->link.'" class="mt-1 mb-1"><span class="badge bg-primary text-white" style="font-size: 16px;"><i class="fa fa-link"></i> '.$this->item->structure->link.'</span></a>';
                                endif;
                                if($this->item->files):
                                    foreach($this->item->files as $file):
                                        echo '<a href="'.JURI::Root().$file->path.'" target="_blank" class="mt-1 mb-1"><span class="badge bg-secondary text-white" style="font-size: 16px;"><i class="fa fa-file"></i> '.$file->filename.'</span></a>';
                                    endforeach;
                                endif;
                                echo '</div>';
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer mt-3">
                <a href="index.php?option=com_jclassroom&task=stage_classroom.prev&id=<?php echo $this->item->id;?>&st=<?php echo $this->item->step;?>&unitID=<?php echo $this->item->structure->unitID;?>" class="float-left btn btn-danger btn-lg text-white">Zurück</a>
                <?php if($this->item->step != $this->item->stepsCount): ?>
                <a href="index.php?option=com_jclassroom&task=stage_classroom.stop&id=<?php echo $this->item->id;?>&st=<?php echo $this->item->step;?>&unitID=<?php echo $this->item->structure->unitID;?>" onclick="return confirm('Möchten Sie den Learningroom beenden?')" class="float-left ml-1 btn btn-primary btn-lg text-white">Beenden</a>
                <?php endif; ?>
                <?php if($this->item->step < $this->item->stepsCount): ?>
                <a href="index.php?option=com_jclassroom&task=stage_classroom.next&id=<?php echo $this->item->id;?>&st=<?php echo $this->item->step;?>&unitID=<?php echo $this->item->structure->unitID;?>" class="float-right btn btn-success btn-lg text-white">Weiter</a>
                <?php endif; ?>
                <?php if($this->item->step == $this->item->stepsCount): ?>
                <a href="index.php?option=com_jclassroom&task=stage_classroom.stop&id=<?php echo $this->item->id;?>&st=<?php echo $this->item->step;?>&unitID=<?php echo $this->item->structure->unitID;?>" onclick="return confirm('Möchten Sie den Learningroom beenden?')" class="float-right btn btn-primary btn-lg text-white">Beenden</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
<input type="hidden" id="theLearningroomID" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="theUnitID" value="<?php echo $this->item->structure->unitID;?>" />
<input type="hidden" id="unitID" value="<?php echo $this->unit['unitID'];?>" />
<input type="hidden" id="sectionID" value="<?php echo $this->unit['section']->sectionID;?>" />
<input type="hidden" id="sectionType" value="<?php echo $this->unit['section']->sectionType;?>" />
<input type="hidden" id="position" value="<?php echo $this->unit['section']->sectionID;?>" />
<?php echo JHtml::_('form.token'); ?>
</form>
<form action="index.php?option=com_jclassroom&task=stage_classroom.loadFile" method="post" name="uploaderFormUnit" id="uploaderFormUnit" enctype="multipart/form-data">
   <input id="uploaderUnit" title="file input" type="file" accept="" name="uploadUnit" style="display: none;">
   <input type="hidden" name="classroomID" value="<?php echo $this->item->id;?>" />
   <input type="hidden" name="unitID" value="<?php echo $this->item->structure->unitID;?>" />
</form>
<script>
$(document).ready(function() {
    $('#sp-menu').css('display','none');
});
function uploadUnit() {
    jQuery('#uploaderUnit').trigger('click');
}
jQuery('#uploaderUnit').on('change', function(e) {
      var classroomID   = jQuery('#theLearningroomID').val();
      var unitID        = jQuery('#theUnitID').val();
      var theFiles      = jQuery(this)[0].files;
      var theFileName   = jQuery('#uploaderUnit')[0].files[0].name;
      var form_data     = new FormData();
      for(i = 0; i <= theFiles.length; i++) {
         
      }
      form_data.append('uploadUnit', jQuery('#uploaderUnit')[0].files[0]);
      form_data.append('classroomID', classroomID);
      form_data.append('unitID', unitID);
      $('#save').modal('show');
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=stage_classroom.loadFile",
         data: form_data,
         processData: false,
         contentType: false,
         //dataType: 'json',
         success: function( data ) {
            
             $('#save').modal('hide');
         }
      });
   });
    </script>