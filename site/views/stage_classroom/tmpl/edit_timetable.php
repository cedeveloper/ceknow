<?php
/**
 * @author     
 * @copyright  
 * @license    
 */

defined("_JEXEC") or die("Restricted access");

$doc = JFactory::getDocument();
$doc->addStyleSheet('components/com_jclassroom/assets/css/unicode.css');
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js' );
//$dokument->addScript('components/com_jclassroom/assets/js/stage_classroom.js');

$now     = new DateTime();
$user    = JFactory::getUser();
?>
<style>
   h2 {
      font-family: 'Open Sans';
      font-size: 24px;
      font-weight: bold;
   }
   h4 {
      font-family: 'Open Sans';
      font-size: 16px;
      font-weight: bold;
   }
   #descriptionHeader,
   .unitHeader {
      cursor: pointer;
   }
   .contentOuter {
      cursor: pointer;
   }
   .contentInner {
      overflow-y: scroll;
      max-height: 600px;
   }
   .stage {
      position: relative;
   }
   @media (min-width: 0px) AND (max-width: 430px) {
      .theContent {
         position: fixed;
         top: 90px;
         left: calc(50% - 200px);
         box-shadow: 0px 0px 12px #000;
         width: 400px;
         z-index: 9;
         padding: 20px;
         max-height: 700px;
      }
      .tttheme {
         margin-left: 0px;
      }
      .ttmodule {
         margin-left: 0px;
      }
      .ttunit {
         margin-left: 0px;
      }
   }
   @media (min-width: 1024px) {
      .theContent {
         position: fixed;
         top: 90px;
         left: calc(50% - 600px);
         box-shadow: 0px 0px 12px #000;
         width: 1200px;
         z-index: 9;
         padding: 20px;
         max-height: 700px;
      }
      .tttheme {
         margin-left: 50px;
      }
      .ttmodule {
         margin-left: 100px;
      }
      .ttunit {
         margin-left: 150px;
      }
      #chatbox {
         background-color: #e1e1e1;
         padding: 10px;
         position: fixed;
      }  
      #chatbox #theChatContent {
         position: absolute;
         bottom: 50px;
         width: calc(100% - 20px);
         font-size: 12px;
         line-height: 14px;
         height: 90px;
         padding: 10px;
         border: 1px solid #a1a1a1;
      }
      #chatbox #sendTheChat {
         position: absolute;
         bottom: 10px;
         right: 10px;
      }
      #chatbox #theChats {
         background-color: #f5f5f5;
         border: 1px solid #a1a1a1;
         height: 76%;
         width: 100%;
         padding: 10px;
         overflow-y: scroll;
      }
      #chatbox #theChats .chat {
         font-size: 12px;
         line-height: 14px;
         padding: 5px;
         
         border-radius: 3px;
         display: inline-block;
      }
      #chatbox #theChats .chatContainer {
         display: inline-block;
         width: 100%;
         margin-bottom: 2px;
      }
      #chatbox #theChats .chat.fromother {
         float: left;
         background-color: white;
      }
      #chatbox #theChats .chat.fromme {
         float: right;
         background-color: lightsteelblue;
      }
      .chatinfo {
         font-size: 11px;
         font-weight: bold;
         margin-bottom: 2px;
      }
      .chatinfo span {
         display: inline-block;
         margin-right: 8px;
         font-size: 11px;
         font-weight: bold;
      }
      .introimage {
         height: 60px;
         width: 100%;
         background-position: left center;
         background-size: contain;
         background-repeat: no-repeat;
      }
   }
</style>
<form action="<?php JRoute::_('index.php?option=com_jclassroom&task=unit.checkQuestion'); ?>" method="post" name="adminForm" id="adminForm">
   <div class="row">
      <?php if($this->item->showChat == 1): ?>
      <div class="col-12 col-sm-9">
      <?php else: ?>
      <div class="col-12">
      <?php endif;?>
         <div class="buttonleiste d-inline-block w-100 mb-4">         
            <?php if($this->group == 'student'):?>       
              <a href="<?php echo $this->return;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
            <?php else: ?>
               <p><i class="fa fa-exclamation-circle text-danger"></i> Bitte schließen Sie diesen Browsertab, um zum Hauptsystem zurückzukehren.</p>
            <?php endif; ?>
         </div>
         <div id="stage" class="stage">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" id="trainingsplan-tab" data-toggle="tab" href="#trainingsplan" role="tab" aria-controls="trainingsplan" aria-selected="true">Trainingsplan</a>
               </li>
               <!--<li class="nav-item">
                  <a class="nav-link" id="content-tab" data-toggle="tab" href="#content" role="tab" aria-controls="content" aria-selected="true">Inhalte</a>
               </li>-->
               <!--<li class="nav-item">
                  <a class="nav-link" id="stundenplan-tab" data-toggle="tab" href="#stundenplan" role="tab" aria-controls="stundenplan" aria-selected="true">TrainingsplanSAVE</a>
               </li>-->
               <li class="nav-item">
                  <a class="nav-link" id="students-tab" data-toggle="tab" href="#students" role="tab" aria-controls="students" aria-selected="false">Personen</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" id="files-tab" data-toggle="tab" href="#files" role="tab" aria-controls="files" aria-selected="false">Arbeitsmaterial</a>
               </li>
               <?php if($this->item->visibleTo == 0 || $this->item->visibleTo == 2): ?>
               <li class="nav-item">
                  <a class="nav-link" id="results-tab" data-toggle="tab" href="#results" role="tab" aria-controls="results" aria-selected="false">Ergebnisse</a>
               </li>
                <li class="nav-item">
                  <a class="nav-link" id="documents-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="documents" aria-selected="false">Dokumente</a>
               </li>
               <?php endif; ?>
            </ul>
            <div class="tab-content mt-3" id="tabContent">
               <div class="tab-pane fade show active" id="trainingsplan" role="tabpanel" aria-labelledby="trainingsplan-tab">
                  <div class="col-12">
                     <h1>Trainingsplan Learningroom: <i><?php echo $this->item->title?></i></h1>
                     <p class=" text-info"><?php echo $now->format('d.m.Y');?> <span style="font-size: 24px;" id="clockTime"></span></p>
                  </div>
                  <div class="col-12">
                     <?php
                     if($this->item->structure):
                        echo $this->item->structure;
                     else:
                        echo '<p><b>Keine Struktur für diesen Learningroom gefunden.</b></p>';
                     endif;
                     ?>
                  </div>
               </div>
               <div class="tab-pane fade show" id="students" role="tabpanel" aria-labelledby="students-tab">
                  <h1>Andere Personen in diesem Learningroom</h1>
                  <p class=" text-info"><?php echo $now->format('d.m.Y H:i');?></p>
                  <?php
                  if($this->item->attendees):
                     echo '<h5>Teilnehmer</h5>';
                     foreach($this->item->attendees as $student):
                        echo '<div class="card bg bg-light p-1 mb-1">';
                        echo $student->first_name.' '.$student->last_name;
                        echo '</div>';
                     endforeach;
                  endif;
                  /*f($this->item->trainers):
                     echo '<h5>Teilnehmende Trainer</h5>';
                     foreach($this->item->trainers as $trainer):
                        echo '<div class="card bg bg-warning p-1 mb-1">';
                        echo $trainer->first_name.' '.$trainer->last_name;
                        echo '</div>';
                     endforeach;
                  endif;*/
                  if($this->item->mainTrainer):
                     echo '<h5>Ausführende Trainer</h5>';
                     foreach($this->item->mainTrainer as $trainer):
                        echo '<div class="card bg bg-success p-1 mb-1">';
                        echo $trainer->name;
                        echo '</div>';
                     endforeach;
                  endif;
                  ?>
               </div>
               <div class="tab-pane fade show" id="files" role="tabpanel" aria-labelledby="files-tab">
                  <h1>Ihre Arbeitsmaterialien</h1>
                  <?php
                  if($this->item->files):
                     echo '<h2>Allgemeine Arbeitsmaterialien</h2>';
                     $noSFiles = 0;
                     foreach($this->item->files as $file):
                        if($file->type == 'material'):
                           $noSFiles = 1;
                           echo '<div class="row">';
                              echo '<div class="col-12">';
                                 echo '<div class="bg-light p-1 mb-1 border-dark">';
                                    echo '<a href="'.$file->path.'" target="_blank">'.$file->filename.'</a>';
                                 echo '</div>';
                              echo '</div>';
                           echo '</div>';
                        endif;
                     endforeach;
                     if($noSFiles == 0):
                        echo '<div class="row">';
                           echo '<div class="col-12">';
                              echo '<div class="bg-light p-1 mb-1 border-dark">';
                                 echo 'Kein Arbeitsmaterial gefunden';
                              echo '</div>';
                           echo '</div>';
                        echo '</div>';
                     endif;
                  endif;
                  if($this->item->files):
                     echo '<h2 class="mt-2">Ihr zusätzliches Arbeitsmaterial</h2>';
                     $noSFiles = 0;
                     foreach($this->item->files as $file):
                        if($file->type == 'reserve'):
                           $noSFiles = 1;
                           echo '<div class="row">';
                              echo '<div class="col-12">';
                                 echo '<div class="bg-light p-1 mb-1 border-dark">';
                                    echo '<a href="'.$file->path.'" target="_blank">'.$file->filename.'</a>';
                                 echo '</div>';
                              echo '</div>';
                           echo '</div>';
                        endif;
                     endforeach;
                     if($noSFiles == 0):
                        echo '<div class="row">';
                           echo '<div class="col-12">';
                              echo '<div class="bg-light p-1 mb-1 border-dark">';
                                 echo 'Kein Arbeitsmaterial gefunden';
                              echo '</div>';
                           echo '</div>';
                        echo '</div>';
                     endif;
                  endif;
                  if($this->item->files):
                     echo '<h2 class="mt-2">Ihre persönlichen Arbeitsmaterialien</h2>';
                     $noSFiles = 0;
                     foreach($this->item->files as $file):
                        if($file->studentID == $this->item->user):
                           $noSFiles = 1;
                           echo '<div class="row">';
                              echo '<div class="col-12">';
                                 echo '<div class="bg-light p-1 mb-1 border-dark">';
                                    echo '<a href="'.$file->path.'" target="_blank">'.$file->filename.'</a>';
                                 echo '</div>';
                              echo '</div>';
                           echo '</div>';
                        endif;
                     endforeach;
                     if($noSFiles == 0):
                        echo '<div class="row">';
                           echo '<div class="col-12">';
                              echo '<div class="bg-light p-1 mb-1 border-dark">';
                                 echo 'Kein Arbeitsmaterial gefunden';
                              echo '</div>';
                           echo '</div>';
                        echo '</div>';
                     endif;
                  endif;

                  ?>
               </div>
               <div class="tab-pane fade show" id="results" role="tabpanel" aria-labelledby="results-tab">
                  <h1>Ihre Ergebnisse</h1>
                  <small><i class="fa fa-question"></i> Hier finden Sie die Auswertungen für alle Quizze, die Sie in diesem Learningroom bearbeitet haben.</small>
                  
                     <?php
                     if($this->item->results):
                        foreach($this->item->results as $result):
                           echo '<div class="row mt-3">';
                           echo '<div class="col-12">';
                              echo '<div class="card p-1">';
                                 echo '<h2 class="" style="font-size: 24px;">'.$result->title.'</h2>';
                                 echo '<p>vom <b>'.date('d.m.Y H:i', strtotime($result->created)).'</b></p>';
                                 // Load the HTML-Result
                                 JLoader::register('StageresultHelper',JPATH_COMPONENT_SITE.'/helpers/stageResult.php');
                                 $template = new StageresultHelper();
                                 echo '<div id="resultContainer" style="overflow-x:scroll;">';
                                 // getResult($rID, $fromLR, $userID, $completeEvaluation, $classroomID, $publishedQuizzID, $unitID, $pdf);
                                 echo  $template->getResult($result->theResultID, 1, 0, 0,$this->item->id,0, $result->quizzID, 0);
                                 echo '</div>';
                                 echo '<a class="mt-1" href="index.php?option=com_jclassroom&task=stage.printResult&theResultID='.$result->id.'&quizzID='.$result->quizzID.'&save=0" target="_blank">Auswertung als PDF exportieren</a>';
                              echo '</div>';
                           echo '</div>';
                           echo '</div>';
                        endforeach;
                     endif;
                     ?>
               </div>
               <div class="tab-pane fade show" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                  <h1>Ihre Dokumente</h1>
                  <small><i class="fa fa-question"></i> Hier finden Sie alle Dokumente, wie Zertifikate, Teilnehmerbescheingungen oder Vouchers, die mit diesem Learningroom in Zusammenhang stehen.</small>
                  <div class="row mt-3">
                     <!--<div class="col-12 col-sm-3">
                        <a href="index.php?option=com_jclassroom&task=stage_classroom.printCertificate&id=<?php //echo $this->item->id;?>">
                        <div class="card d-flex" style="height: 200px;justify-content: center;text-align: center;">
                           <i style="font-size: 48px;" class="fa fa-certificate"></i>Zertifikat
                        </div>
                        </a>
                     </div>-->
                     <!--<div class="col-12 col-sm-3">
                        <div class="card d-flex" style="height: 200px;justify-content: center;text-align: center;">
                           <i style="font-size: 48px;" class="fa fa-car"></i>Parkschein
                        </div>
                     </div>
                     <div class="col-12 col-sm-3">
                        <div class="card d-flex" style="height: 200px;justify-content: center;text-align: center;">
                           <i style="font-size: 48px;" class="fa fa-bed"></i>Hotelvoucher
                        </div>
                     </div>-->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php if($this->item->showChat == 1): ?>
      <div class="col-12 col-sm-3">
         <div id="chatbox">
            <h2>Chatbox</h2>
            <div id="theChats">
               <?php 
               if($this->item->chats):
                  foreach($this->item->chats as $chat):
                     if($user->id == $chat->userID):
                        echo '<div class="chatContainer">';
                        echo '<div class="chat fromme">';
                        echo '<div class="chatinfo">';
                        echo '<span>'.$chat->author.'</span>';
                        echo '<span>'.$chat->created.'</span>';
                        echo '</div>';
                        echo $chat->content;
                        echo '</div>';
                        echo '</div>';
                     else:
                        echo '<div class="chatContainer">';
                        echo '<div class="chat fromother">';
                        echo '<div class="chatinfo">';
                        echo '<span>'.$chat->author.'</span>';
                        echo '<span>'.$chat->created.'</span>';
                        echo '</div>';
                        echo $chat->content;
                        echo '</div>';
                        echo '</div>';
                     endif;
                  endforeach;
               endif;
               ?>
            </div>
            <textarea id="theChatContent" placeholder="Nachricht eingeben"></textarea>
            <a id="sendTheChat" class="btn btn-primary text-white btn-sm"><i class="fa fa-paper-plane-o"></i></a>
         </div>
      </div>
   <?php endif;?>
   </div>
<?php echo JHtml::_('form.token'); ?>
<input type="hidden" id="classroomID" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="group" value="<?php echo $this->group;?>" />
<input type="hidden" id="preview" value="<?php echo $this->preview;?>" />
</form>
<script type="text/javascript">
   $(document).ready(function() {
      var group = $('#group').val();
      if(group != 'student') {
         $('ul.menu.nav').css('display', 'none');
      }
      var h1 = $(window).height();
      var stageT  = $('#stage').offset();
      var wCol3   = $('.col-sm-3').width();
      var h2 = h1 - stageT.top + 150;
      h1 = h1 - 120;
      $('.theContent').css('max-height', h1 + 'px').css('height', h1 + 'px');
      h1 = h1 - 80;
      $('.contentInner').css('max-height', h1 + 'px').css('height', h1 + 'px');
      $('#chatbox').css('top',stageT.top - 180).css('width', wCol3 + 'px').css('height', h2 + 'px');
      $('#theChats').scrollTop(h1 + 160);
      setInterval(function() { 
         loadChat1()
         }, 1000);
      setInterval(function(){ 
         loadChat()
         }, 5000);
      
   });
   function loadChat1() {
      var a = new Date();
      b = a.getHours(); 
      b = b.toString();
      if(b.length == 1) {
         b = '0' + b;
      }
      c = a.getMinutes(); 
      c = c.toString();
      if(c.length == 1) {
         c = '0' + c;
      }
      d = a.getSeconds();
      d = d.toString();
      if(d.length == 1) {
         d = '0' + d;
      }
      $('#clockTime').html(b + ':' + c + ':' + d);
   }
   function loadChat() {
      var classroomID   = $('#classroomID').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=stage_classroom.loadChat",
         data: {classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#theChats').empty();
            $('#theChats').append(data);
            var h1 = $('#theChats').scrollTop();
            $('#theChats').scrollTop(h1 + 80);
         }
      });
   }
   $('.moduleHeader').on('click', function() {
      alert("HHHH");
   })
   $('#sendTheChat').on('click', function() {
      sendTheChat();
      
   });
   jQuery('#theChatContent').on('keypress', function(event) {
      if(event.which == 13) {
         sendTheChat();
      }
   });
   function sendTheChat() {
      var classroomID   = $('#classroomID').val();
      var content       = $('#theChatContent').val();
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=stage_classroom.writeChat",
         data: {content:content, classroomID:classroomID},
         //dataType: 'json',
         success: function( data ) {
            $('#theChats').append(data);
            var h1 = $('#theChats').scrollTop();
            $('#theChats').scrollTop(h1 + 80);
            $('#theChatContent').val('');
         }
      });
   }
   jQuery('.stageUnitHeader').on('click', function() {
      var unitID  = jQuery(this).parent('.card').attr('id');
      unitID      = unitID.replace('unit', '');
      openUnit(unitID);
   });
   function openDescription() {
      if(jQuery('#descriptionContent').hasClass('hide')) {
         jQuery('#descriptionContent').removeClass('hide');
         jQuery('#descriptionContent').addClass('show');
         jQuery('#descriptionHeader .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      } else {
         jQuery('#descriptionContent').removeClass('show');
         jQuery('#descriptionContent').addClass('hide');
         jQuery('#descriptionHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      }
   }
   function openUnit(id) {
      if(jQuery('#unit' + id + ' .card-body').hasClass('show')) {
         jQuery('#unit' + id + ' .card-body').removeClass('show').addClass('hide');
         jQuery('#unit' + id + ' .duration .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      } else {
         jQuery('#unit' + id + ' .card-body').addClass('show').removeClass('hide');
         jQuery('#unit' + id + ' .duration .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
      }
   }
   function openModule(id) {
      if(jQuery('#moduleContent' + id).hasClass('hide')) {
         jQuery('#moduleContent' + id).removeClass('hide');
         jQuery('#moduleContent' + id).addClass('show');
         jQuery('#module' + id + ' .moduleHeader .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
         jQuery('#module' + id).animate({scrollTop: 0},200);
      } else {
         jQuery('#moduleContent' + id).removeClass('show');
         jQuery('#moduleContent' + id).addClass('hide');
         jQuery('#module' + id + ' .moduleHeader .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
      }

   }
      // LOAD SOMETHING
   function loadDay(classroomID, dayID) {
      var preview = $('#preview').val();
      jQuery('#wait').css('display', 'flex');
      jQuery('#dayID').val(dayID);
      jQuery.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=stage.loadDay",
         data: {
            dayID:dayID,
            classroomID:classroomID,
            preview:preview
         },
         //dataType: 'json',
         success: function( data ) {
            jQuery('#stage').empty();
            jQuery('#stage').append(data);
            jQuery('.classroomDays a').each(function() {
               jQuery(this).removeClass('btn-success').addClass('btn-light');
            });
            jQuery('#loadDay' + dayID).removeClass('btn-light').addClass('btn-success');
            jQuery('#wait').css('display','none');
         }
      });
   }
   $('.contentOuter').on('click', function() {
      /*$('.theContent').each(function() {
         $(this).fadeOut(200);
         $(this).parent('.contentOuter').removeClass('shown');
      });*/
      $(this).find('.theContent').css('display', 'block').parent('.contentOuter').addClass('shown');
   });
   function closeContent(id) {
      $('#content' + id).fadeOut(200);
   }
</script>