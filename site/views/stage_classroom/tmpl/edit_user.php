<?php
/**
 * @author     
 * @copyright  
 * @license    
 */

defined("_JEXEC") or die("Restricted access");

$dokument = JFactory::getDocument();
$dokument->addScript('components/com_jclassroom/assets/js/jquery.validate.js' );
//$dokument->addScript('components/com_jclassroom/assets/js/stage_classroom.js');
?>

<form action="<?php JRoute::_('index.php?option=com_jclassroom&task=unit.checkQuestion'); ?>" method="post" name="adminForm" id="adminForm">
   <div class="buttonleiste d-inline-block w-100 mb-4">  

   </div>
   <div class="stage">
      <div class="row">
         <div class="col-12 col-sm-6">
         <input type="text" id="first_name" name="first_name" class="mb-2" placeholder="Bitte gib Deinen Vornamen ein" value="" />
         <input type="text" id="last_name" name="last_name" class="mb-2" placeholder="Bitte gib Deinen Nachnamen ein" value="" />
         <button type="button" onclick="saveTempUser();" class="btn btn-success text-white">Speichern</button>
      </div>
      </div>
    </div>
<?php echo JHtml::_('form.token'); ?>
<input type="hidden" id="group" value="<?php echo $this->group;?>" />
<input type="hidden" id="preview" value="<?php echo $this->preview;?>" />
</form>
<script>
function saveTempUser() {
   let first_name = $('#first_name').val();
   let last_name = $('#last_name').val();
   if(!first_name || !last_name) {
      alert('Bitte gib Deinen Vor- und Nachnamen an');
      return false;
   }
}
</script>
