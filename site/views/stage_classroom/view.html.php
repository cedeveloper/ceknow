<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
 
/**
 * Kunde item view class.
 *
 * @package     Auditum
 * @subpackage  Views
 */
class JclassroomViewStage_classroom extends JViewLegacy
{
	protected $item;
	protected $unit;
	protected $result;
	
	public function display($tpl = null)
	{
		$session = JFactory::getSession();
		$input 		= JFactory::getApplication()->input;
		$preview	= $input->get('preview', '', 'INT');
		$this->preview = $preview;
		$user 	= JFactory::getUser();
		$app 	= JFactory::getApplication();
		if($this->item->visibleTo == 2):
			if(!$user->id):
				$app->enqueueMessage('Der Learningroom <b><i>'.$this->item->title.'</i></b> ist geschützt. Bitte melden Sie sich mit Ihrem Trainingszugang an.', 'Error');
				$app->redirect(JURI::Root().'login');
			endif;
		endif;
        // Superuser
        if($session->get('group') == 'superuser') {
        	$this->group 	= 'superuser';
            $this->return 	= 'classrooms';
        }
        // Customer
        if($session->get('group') == 'customer') {
        	$this->group 	= 'customer';
          	$this->return 	= 'manager-customer';
        }
        // Trainer
        if($session->get('group') == 'trainer') {
        	$this->group 	= 'trainer';
            $this->return 	= 'manager-trainer';
        }
        // Students
        if($session->get('group') == 'student') {
        	$this->group 	= 'student';
            $this->return 	= 'dashboard-students';
        }
		$loggedIn = false;
		if($user->id):
			$loggedIn = true;
		endif;
		
		$this->item 				= $this->get('Item');
		$this->unit 				= $this->get('Unit');
		$this->result 				= $this->get('Result');
		if($this->item->presentation == 1):
			$tpl = 'katalog';
		endif;
		if($this->item->presentation == 2):
			$tpl = 'timetable';
		endif;
		if($loggedIn == false):
			//$tpl = 'user';
		endif;
		parent::display($tpl);
	}
}
?>