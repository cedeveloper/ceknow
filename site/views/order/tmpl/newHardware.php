<div id="newHardware" class="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document" style="max-width: 800px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Neue Bestellposition einfügen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Hardware
               </div>
               <div class="col-12 col-sm-10">
                  <select id="hardware_id" class="select2-max">
                     <option value="" selected="selected" disabled>Bitte auswählen</option>
                     <?php
                     if($this->item->hardware):
                        foreach($this->item->hardware as $hardware):
                           echo '<option value="'.$hardware->id.'">'.$hardware->title.'</option>';
                        endforeach;
                     endif;
                     ?>
                  </select>
               </div>
            </div>
            <div class="form-group row">
               <div class="col-12 col-sm-2 col-form-label">
                  Menge
               </div>
               <div class="col-12 col-sm-10">
                  <input type="number" id="hardware_amount" min="0" value="" />
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button id="saveNewHardwarePosition" type="button" class="btn btn-primary">Einfügen</button>
            <button id="editNewHardwarePosition" type="button" class="btn btn-primary">Ändern</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
            <input type="hidden" id="positionID" value="" />
         </div>
      </div>
   </div>
</div>