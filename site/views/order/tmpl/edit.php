<?php
/**
 * InClure - Beilagenverwaltungssystem
 * @version     4.6.0
 * @package     com_inclure
 * @copyright   Copyright (C) 2020. Alle Rechte vorbehalten.
 * @author      dats | Druckagentur Torsten Scheel http://www.torstenscheel.de
 * @email      kontakt@torstenscheel.de
 */

defined("_JEXEC") or die("Restricted access");

JHtml::_('behavior.keepalive');
$doc = JFactory::getDocument();
$doc->addScript('components/com_jclassroom/assets/js/jquery.validate.js');
require_once(JPATH_COMPONENT.'/views/order/tmpl/newHardware.php');

?>
<form action="<?php echo JRoute::_('index.php?option=com_jclassroom&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">

    <div class="buttonleiste d-inline-block w-100 mb-4">   
      <?php if($this->closed == 0):?>      
         <a class="btn btn-success text-white" onclick="Joomla.submitform('order.simpleSave')">Speichern</a>
         <?php if($this->student == 0): ?>
            <a class="btn btn-secondary text-white" onclick="Joomla.submitform('order.save')">Speichern & Schließen</a>
            <a class="btn btn-warning text-white" href="index.php?option=com_jclassroom&task=order.printOrder&id=<?php echo $this->item->id;?>" target="_blank">Lieferschein drucken</a>
         <?php endif; ?>
      <?php endif;?>
        <a href="<?php echo $this->retour;?>" class="float-right btn btn-danger text-white m-1">Zurück</a>
    </div>
    <div id="theAlerts" class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> Bitte prüfen Sie die Pflichtfelder</div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
         <li class="nav-item">
            <a class="nav-link active" id="customersdata-tab" data-toggle="tab" href="#customersdata" role="tab" aria-controls="customersdata" aria-selected="true">Allgemein</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" id="asp-tab" data-toggle="tab" href="#asp" role="tab" aria-controls="asp" aria-selected="false">Bestellpositionen</a>
         </li>
    </ul>
    <div class="tab-content" id="tabContent">
         <div class="tab-pane fade show active" id="customersdata" role="tabpanel" aria-labelledby="customersdata-tab">
            <div class="form-horizontal mt-3 w-50">
               <?php if($this->student == 0): ?>
               <div class="form-group row mb-0">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('customerID'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo '<span class="d-inline-block pt-2"><b>'.str_pad($this->item->customerID,6,'0', STR_PAD_LEFT) .'</b> '.$this->item->customerName.'</span>';?>
                  </div>
               </div>
               <?php else: ?>
                  <input type="hidden" id="jform_customerID" name="jform[customerID" value="<?php echo $this->item->customerID;?>" />
               <?php endif; ?>
                  <div class="form-group row mb-0">
                     <div class="col-12 col-sm-3 col-form-label">
                        <?php echo $this->form->getLabel('classroomID'); ?>
                     </div>
                     <div class="col-12 col-sm-9">
                     <?php if($this->student == 0): ?>
                        <?php echo '<span class="d-inline-block pt-2"><b>(LR'.str_pad($this->item->classroomID,6,'0', STR_PAD_LEFT) .')</b> '.$this->item->classroomName.'</span>';?>
                     <?php else: ?>
                        <?php echo '<span class="d-inline-block pt-2"><b>(LR'.str_pad($this->item->classroomID,6,'0', STR_PAD_LEFT) .')</b> '.$this->item->classroomName.'</span>';?>
                        <input type="hidden" id="jform_classroomID" name="jform[classroomID" value="<?php echo $this->item->classroomID;?>" />
                     <?php endif; ?>
                  </div>
               </div>
               <?php if($this->item->companyName):?>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     Firma
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo '<span class="d-inline-block pt-2">'.$this->item->companyID.' '.$this->item->companyName.'</span>';?>
                  </div>
               </div>
               <?php endif;?>
                <?php if($this->student == 0): ?>
                <!--<div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php //echo $this->form->getLabel('userID'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php //echo $this->form->getInput('userID'); ?>
                    </div>
                </div>-->
                <?php else: ?>
                  <!--<input type="hidden" id="jform_userID" name="jform[userID" value="<?php echo $this->item->userID;?>" />-->
                <?php endif; ?>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('deliveryDate'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('deliveryDate'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('salutation'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('salutation'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('first_name'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('first_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('last_name'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('last_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('adress'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('adress'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('postcode'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('postcode'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('city'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('city'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('phone'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('phone'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-sm-3 col-form-label">
                      <?php echo $this->form->getLabel('mobile'); ?>
                    </div>
                    <div class="col-12 col-sm-9">
                      <?php echo $this->form->getInput('mobile'); ?>
                    </div>
               </div>
               <?php if($this->student == 0): ?>
               <div class="form-group row">
                  <div class="col-12 col-sm-3 col-form-label">
                     <?php echo $this->form->getLabel('published'); ?>
                  </div>
                  <div class="col-12 col-sm-9">
                     <?php echo $this->form->getInput('published'); ?>
                  </div>
               </div>
            <?php endif; ?>
            </div>
         </div>
         <div class="tab-pane fade" id="asp" role="tabpanel" aria-labelledby="asp-tab">
             <div class="form-horizontal mt-3">
               <a class="btn btn-success text-white mb-3" onclick="newHardware();">Neue Hardware bestellen</a>
               <h5 class="m-0">Bestellte Hardware</h5>
               <div class="row">
                  <div class="col-12 col-sm-2"></div>
                  <div class="col-12 col-sm-8">Bezeichnung</div>
                  <div class="text-center col-12 col-sm-2">Menge</div>
               </div>
               <div id="positions" class="bg-light p-1">
                  <?php
                  if($this->item->positions):
                     foreach($this->item->positions as $position):
                        echo '<div id="position'.$position->id.'" class="position" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                        echo '<div class="row">';
                           echo '<div class=" col-12 col-sm-2">';
                              echo '<span class="badge bg-dark text-white mr-2">ID: '.$position->id.'</span>';
                              echo '<i title="Ansprechpartner bearbeiten" onclick="editContact('.$position->id.');" class="fa fa-pencil mr-2"></i>';
                              echo '<i title="Ansprechpartner löschen" onclick="deleteContact('.$position->id.');" class="fa fa-trash-o"></i>';
                              echo '</div>';
                              echo '<div id="edit_contact_feld1" class=" col-12 col-sm-8">';
                              echo $position->title;
                              echo '</div>';
                              echo '<div id="edit_contact_feld2" class="text-center col-12 col-sm-2">';
                              echo $position->amount;
                              echo '</div>';
                           echo '</div>';
                        echo '</div>';
                     endforeach;
                  else:
                     echo '<div id="contactPlaceholder" class="admin" style="border-bottom: 1px solid #aaa;padding: 4px 0px 4px 0;">';
                     echo 'Keine Bestellpositionen gefunden';
                     echo '</div>';
                  endif;
                  ?>
               </div>
            </div>
       	</div>
    </div>
<input type="hidden" id="id" name="jform[id]" value="<?php echo $this->item->id;?>" />
<input type="hidden" id="uid" name="jform[uid]" value="<?php echo $this->item->userID;?>" />
<input type="hidden" id="clr" name="jform[clr]" value="<?php echo $this->item->classroomID;?>" />
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
<script type="text/javascript">
   $(document).ready(function() {
      $('#jform_customerID').attr('disabled', 'true');
      $('#jform_classroomID').attr('disabled', 'true');
      $('#jform_userID').attr('disabled', 'true');
      $('#jform_userID').attr('disabled', 'true');
   });
   var form = $("#adminForm");
   $.validator.setDefaults({
      ignore: []
   });
   form.validate({

      invalidHandler: function(form, validator) {
         $('#theAlerts').css('display', 'block');
      },
      rules: {
         'jform[email]': {
            required: false,
            email: true
         },
         'jform[userID]': {
            required: true
         }
      },
      messages: {
         'jform[email]': 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
         'jform[userID]': 'Bitte wählen Sie einen Teilnehmer aus.'
      },
      submitHandler: function(form) {
         var id = $('#id').val();
         var cOrder = $('#positions .position').length;
         if(id && cOrder == 0) {
            alert("Bitte wählen Sie die Hardware aus, die Sie bestellen möchten.");
            return false;
         }
         form.submit();
      }
   });
   function newHardware() {
      var id = $('#id').val();
      if(!id) {
         alert("Bitte speichern Sie zunächst Ihre persönlichen Daten und wählen Sie im Anschluss die gewünschte Hardware aus.");
         return false;
      }
      $('#newHardware .modal-title').html('Neue Bestellposition einfügen');
      $('#saveNewHardwarePosition').css('display', 'block');
      $('#editNewHardwarePosition').css('display', 'none');
      $('#newHardware').modal('show');
      jQuery('#hardware_id').trigger('change.select2');
   }
   $('#saveNewHardwarePosition').on('click', function() {
      saveNewHardwarePosition();
   });
   $('#editNewHardwarePosition').on('click', function() {
      editNewHardwarePosition();
   });
   $('#hardware_amount').on('change', function() {
      var amount = $(this).val();
      if(!amount || amount == 0 || amount < 0) {
         alert('Bitte wählen Sie eine Anzahl aus. Die Mindestmenge ist 1.');
         return false;
      }
   });
   function saveNewHardwarePosition() {
      var orderID    = $('#id').val();
      if(!orderID) {
         alert('Bitte speichern Sie die Bestellung zunächst und fügen dann die Bestellpositionen hinzu.');
         $('#hardware_id').val('').trigger('change.select2');
         $('#hardware_amount').val(0);
         $('#newHardware').modal('hide');
         return false;
      }
      var hardwareID    = $('#hardware_id option:selected').val();
      var hardwareAmount= $('#hardware_amount').val();
      
      if(!hardwareID || !hardwareAmount) {
         alert("Bitte wählen Sie die gewünschte Hardware und die Anzahl aus.");
         return false;
      }
      if(hardwareAmount == 0) {
         alert("Bitte geben Sie eine Anzahl ein.");
         return false;
      }
      $('#newHardware').modal('hide');
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=order.addPosition",
         data: {
            orderID:orderID,
            hardwareID:hardwareID,
            hardwareAmount:hardwareAmount
         },
         //dataType: 'json',
         success: function( data ) {
            $('#contactPlaceholder').remove();
            var orderposition = data.split('_');
            $('#positions').append(
               '<div id="position' + orderposition[0] + '" class="position" style="border-bottom: 1px solid #aaa;padding: 4px 0 4px 0;">' + 
               '<div class="row">' + 
               '<div class="col-12 col-sm-2">' + 
               '<span class="badge bg-dark text-white mr-2">ID: ' + orderposition[0] + '</span>' +
               '<i title="Ansprechpartner bearbeiten" onclick="editContact(' + orderposition[0] + ');" class="fa fa-pencil mr-2"></i>'+
               '<i title="Ansprechpartner löschen" onclick="deleteContact(' + orderposition[0] + ');" class="fa fa-trash-o"></i>' +
               '</div>' +
               '<div class="col-12 col-sm-8">' + 
               orderposition[1] +
               '</div>' +
               '<div class="text-center col-12 col-sm-2">' + 
               hardwareAmount +
               '</div>' +
               '</div>' +
               '</div>'
            );
            $('#hardware_id').val('');
            $('#hardware_amount').val(0);
         }
      });
   }
   function editNewHardwarePosition() {
      var orderID    = $('#id').val();
      var positionID = $('input#positionID').val();
      var hardwareID    = $('#hardware_id option:selected').val();
      var hardwareAmount= $('#hardware_amount').val();
      
      if(!hardwareID || !hardwareAmount) {
         alert("Bitte wählen Sie die gewünschte Hardware und die Anzahl aus.");
         return false;
      }
      if(hardwareAmount == 0) {
         alert("Bitte geben Sie eine Anzahl ein.");
         return false;
      }
      console.log(orderID + ' ' + positionID + ' ' + hardwareID + ' ' + hardwareAmount);
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=order.editPosition",
         data: {
            positionID:positionID,
            orderID:orderID,
            hardwareID:hardwareID,
            hardwareAmount:hardwareAmount
         },
         //dataType: 'json',
         success: function( data ) {
            $('#position' + positionID + ' #edit_contact_feld1').html(data);
            $('#position' + positionID + ' #edit_contact_feld2').html(hardwareAmount);
            $('#hardware_id').val('');
            $('#hardware_amount').val(0);
            $('#newHardware').modal('hide');
         }
      });
   }
   function editContact(id) {
      $.ajax({
         type: "POST",
         url: "index.php?option=com_jclassroom&task=order.getOrder",
         data: {
            positionID:id
         },
         dataType: 'json',
         success: function( data ) {
            $('#hardware_id').val(data['hardware_id']);
            jQuery('#hardware_id').select2({
               placeholder: 'Bitte auswählen'
            });
            $('#hardware_amount').val(data['hardware_amount']);
         }
      });
      $('#newHardware .modal-title').html('Bestellposition ändern');
      $('input#positionID').val(id);
      $('#saveNewHardwarePosition').css('display', 'none');
      $('#editNewHardwarePosition').css('display', 'block');
      $('#newHardware').modal('show');
   }
   $('#saveEditContact').on('click', function() {
      var contactID     = $('#editContactID').val();
      var salutation    = $('#edit_contact_salutation option:selected').val();
      var first_name    = $('#edit_contact_first_name').val();
      var last_name     = $('#edit_contact_last_name').val();
      var functionS     = $('#edit_contact_function').val();
      var phone         = $('#edit_contact_phone').val();
      var email         = $('#edit_contact_email').val(); 
      $('#editContact').modal('hide'); 
      $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=company.editContact",
            data: {
               contactID:contactID,
               salutation:salutation,
               first_name:first_name,
               last_name:last_name,
               functionS:functionS,
               phone:phone,
               email:email
            },
            dataType: 'json',
            success: function( data ) {
               $('#contact' + contactID + ' #edit_contact_feld1').html(salutation + ' ' + first_name + ' ' + last_name);
               $('#contact' + contactID + ' #edit_contact_feld2').html(functionS);
               if(phone) {
                  if($('#contact' + contactID + ' #edit_contact_feld3')) {
                     $('#contact' + contactID + ' #edit_contact_feld3').html('<i class="fa fa-phone"></i>' + phone );
                  } else {
                     $('#contact' + contactID + ' #contact_contactdata').append(
                        '<span id="contact_feld3" class="badge bg-success text-white">' + 
                        '<i class="fa fa-phone></i>' +
                        phone + 
                        '</span>'
                     );
                  }
               }
               if(email) {
                  if($('#contact' + contactID + ' #edit_contact_feld4')) {
                     $('#contact' + contactID + ' #edit_contact_feld4').html('<i class="fa fa-envelope"></i>' + email);
                  } else {
                     $('#contact' + contactID + ' #contact_contactdata').append(
                        '<span id="contact_feld4" class="badge bg-success text-white">' + 
                        '<i class="fa fa-envelope></i>' +
                        email + 
                        '</span>'
                     );
                  }
               }
            }
         });
   });
   function deleteContact(id) {
      if(confirm('Soll die Bestellposition mit der ID ' + id + ' gelöscht werden?') == true) {
         $.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=order.deletePosition",
            data: {
               id:id
            },
            //dataType: 'json',
            success: function( data ) {
               $('#position' + id).remove();
            }
         });
      }
   }
   function loadLearningroomDate() {
      var learningroomID   = $('#jform_classroomID option:selected').val();
      jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=order.loadLearningroom",
            data: {learningroomID:learningroomID},
            //dataType: 'json',
            success: function( data ) {
               var ret = data.split('_');
               if(ret[0] == 'NOK') {
                  $('#jform_deliveryDate').val('');
                  alert(ret[1]);
               } else {
                  $('#jform_deliveryDate').val(data);
               }
            }
        });
   }
   jQuery('#jform_email').on('change', function() {
      var email = jQuery('#jform_email').val();
      jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_jclassroom&task=trainer.checkEmail",
            data: {email:email},
            //dataType: 'json',
            success: function( data ) {
               if(data == 'OK') {
                  jQuery('#jform_username').val(email);
               } else {
                  alert('Die E-Mail-Adresse wird bereits verwendet.');
                  jQuery('#jform_email').val('');
                  return false;
               }
            }
        });
   });
   $('#hardware_amount').on('change', function() {
      var hardwareID = $('#hardware_id option:selected').val();
      if(!hardwareID) {
         alert('Bitte wählen Sie zunächst einen Hardwaretyp aus.');
         $('#hardware_amount').val('');
         return false;
      }
   });
</script>