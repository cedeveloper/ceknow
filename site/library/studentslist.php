<?php
defined('_JEXEC') or die;

if (!class_exists('TCPDF')) {
	require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
}

class printList {
	
	public function studentlist($id) {
		echo $id;
		//require_once(JPATH_SITE.'/components/com_jclassroom/controllers/tcpdf_include.php');
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('dats | Druckagentur Torsten Scheel');
		$pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE,PDF_HEADER_STRING);
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		// set margins
		$pdf->SetMargins(10, 30, 10,true);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->SetFont('helvetica', 'R', 14, '', false);
		$pdf->AddPage();
		$html = $this->getResult();
		$pdf->writeHTML($html, true, false, true, false, '');
		ob_end_clean();
		$pdf->Output('components/com_inclure/pdf/Dispo1_BLG'.$id.'.pdf', 'I');
	}

	function getResult() {
		$user 			= JFactory::getUser();
		$input 			= JFactory::getApplication()->input;
		$learningroomID = $input->get('id',0,'INT');
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		// Load the result of the quizz
		$query 		= $db->getQuery(true);
		$query->select(array('
			a.*,
			min(b.title) as startdate,
			max(b.title) as enddate,
			c.name as company_name,
			c.customer_number
		'));
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_timeblocks', 'b') . ' ON (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('a.id') . ')');
         $query->join('LEFT', $db->quoteName('#__jclassroom_companys', 'c') . ' ON (' . $db->quoteName('a.companyID') . ' = ' . $db->quoteName('c.id') . ')');
		$query->where($db->quoteName('a.id').' = '.$db->quote($learningroomID));
		$query->order('b.title ASC');
		$query->setLimit(1);
		$db->setQuery($query);
		$learningroom 	= $db->loadObject();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$student 	= JTable::getInstance('Student','JclassroomTable',array());
		$load 		= array('tblUserID' => $user->id);
		$student->load($load);
		$html = '';
		$html .= '<table border="0" width="100%" style="font-size: 10px;text-align: left;">';
		$html .= '<tbody>';
		$html .= '<tr style="background-color: #d1d1d1;">';
		$html .= '<td style="border: 0.2px solid #000;" colspan="3">Learningroom: <b>LR'.str_pad($learningroomID, 8, '0', STR_PAD_LEFT).'</b></td>';
		$html .= '</tr>';
		if($learningroom->companyID != 0):
			$html .= '<tr style="background-color: #e1e1e1;">';
			$html .= '<td style="border: 0.2px solid #000;" colspan="3">(<i>'.$learningroom->customer_number.'</i>) '.$learningroom->company_name.'</td>';
			$html .= '</tr>';
		endif;
		$html .= '<tr style="background-color: #e1e1e1;">';
		$theDate = '';
		if($learningroom->startdate && $learningroom->enddate):
			$theDate = date('d.m.Y', strtotime($learningroom->startdate)).' - '.date('d.m.Y', strtotime($learningroom->enddate));
		elseif($learningroom->startdate):
			$theDate = date('d.m.Y', strtotime($learningroom->startdate));
		endif;
		$html .= '<td style="border: 0.2px solid #000;" colspan="3">Datum: <b>'.$theDate.'</b></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td style="border: 0.2px solid #000;" colspan="3" height="20"></td>';
		$html .= '</tr>';
		$html .= '<tr style="background-color: #e1e1e1;">';
		$html .= '<td style="border: 0.2px solid #000;" width="30"><b>Nr.</b></td>';
		$html .= '<td style="border: 0.2px solid #000;" width="300"><b>Name</b></td>';
		$html .= '<td style="border: 0.2px solid #000;" width="208.5"><b>Bemerkungen</b></td>';
		$html .= '</tr>';
		$query 		= $db->getQuery(true);
		$query->select(array('
			a.comment, 
			b.*
		'));
        $query->from($db->quoteName('#__jclassroom_classroom_students','a'));
        $query->join('INNER', $db->quoteName('#__users', 'b') . ' ON (' . $db->quoteName('a.userID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('a.classroomID').' = '.$db->quote($learningroomID));
		$db->setQuery($query);
		$students 	= $db->loadObjectList();
		$i = 1;
		foreach($students as $student):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$st 		= JTable::getInstance('Student','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->id);
			$checkST 	= $st->load($load);
			if($checkST):
				$salutation 	= $st->salutation;
				$studentName 	= $st->first_name.' '.$st->last_name;
				$email 			= $st->email;
				$studentType	= 'Teilnehmer';
			endif;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$tr 		= JTable::getInstance('Trainer','JclassroomTable',array());
			$load 		= array('tblUserID' => $student->id);
			$checkTR 	= $tr->load($load);
			if($checkTR):
				$salutation 	= $tr->salutation;
				$studentName 	= $tr->first_name.' '.$tr->last_name;
				$email 			= $tr->email;
				$studentType	= 'Trainer';
			endif;
			if(!$checkST && !$checkTR):
				JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
				$us 		= JTable::getInstance('User','JclassroomTable',array());
				$load 		= array('id' => $student->id);
				$checkUS 	= $us->load($load);
				if($checkUS):
					$studentID 		= $us->id;
					$studentName 	= $us->name;
					$email 			= $us->email;
					$studentType	= 'Systemadministrator';
				endif;
			endif;
			$html .= '<tr>';
			$html .= '<td style="border: 0.2px solid #000;" width="30">'.$i.'</td>';
			$html .= '<td style="border: 0.2px solid #000;" width="300">'.$salutation.' '.$studentName.'<br/>'.$email.'</td>';
			$html .= '<td style="border: 0.2px solid #000;font-size: 10px;line-height: 12px;">'.$student->comment.'</td>';
			$html .= '</tr>';
			$i++;
		endforeach;
		$html .= '</table>';
		
		return $html;
	}
}

class MYPDF extends TCPDF {
	public function Header() {
		$this->Image('images/logo_black.png', 10, 10, 40, 15, 'PNG','' , '', true, 300, '', false, false, 0, false, false, false);
		$this->writeHTMLCell(90, 30, 110,11.5, '<h1 style="font-size: 24pt;">Teilnehmerliste</h1>', 0, 0, 0, 0, 'R', true, true, 0, false, true, 30, 'M', true);
	}
	public function Footer() {
		$now = JFactory::getDate();
		$date = new JDate($now);
		$now = $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$datumSTR = $now->format('d.m.Y');
		$datum = explode(' ',$now);
		$this->SetXY(10,-15);
		// Set font
		$this->SetFont('helvetica', '', 7);
		// Page number
		$this->Cell(0, 10, $mandant, 0, false, 'L', 0, '', 0, false, 'T', 'M');
		$this->MultiCell(50, 5, 'ceKnow (c) '.date('Y'), '0', 'L', 0, 1, '10', '285', true);
		$this->MultiCell(50, 5, 'Druck am: '.$datumSTR.' '.$datum[1], '', 'L', 0, 1, '10', '288', true);
		$this->MultiCell(50, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), '0', 'R', 0, 1, '160', '288', true);
	}
}

