<?php
defined('_JEXEC') or die;

class printCertificate {
	
	public function certificate($id) {
		require_once(JPATH_SITE.'/components/com_jclassroom/controllers/tcpdf_include.php');
		require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('dats | Druckagentur Torsten Scheel');
		$pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE,PDF_HEADER_STRING);
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		// set margins
		$pdf->SetMargins(10, 90, 10,true);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->SetFont('helvetica', 'R', 14, '', false);
		$pdf->AddPage();
		$html = $this->getResult();
		$pdf->writeHTML($html, true, false, true, false, '');
		ob_end_clean();
		$pdf->Output('components/com_inclure/pdf/Dispo1_BLG'.$id.'.pdf', 'I');
	}

	function getResult() {
		$user 			= JFactory::getUser();
		$input 			= JFactory::getApplication()->input;
		$learningroomID = $input->get('id',0,'INT');
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		// Load the result of the quizz
		$query 		= $db->getQuery(true);
		$query->select(array('a.*,b.day'));
        $query->from($db->quoteName('#__jclassroom_classrooms','a'));
        $query->join('INNER', $db->quoteName('#__jclassroom_classroom_days', 'b') . ' ON (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('a.id') . ')');
		$query->where($db->quoteName('a.id').' = '.$db->quote($learningroomID));
		$query->order('b.day ASC');
		$query->setLimit(1);
		$db->setQuery($query);
		$learningroom 	= $db->loadObject();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$student 	= JTable::getInstance('Student','JclassroomTable',array());
		$load 		= array('tblUserID' => $user->id);
		$student->load($load);
		$html = '';
		$html .= '<table border="0" width="100%" style="font-size: 12px;text-align: center;">';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td><h3>Hiermit bescheinigen wir <br/><br/><span style="font-size: 32px;">'.$student->salutation.' '.$student->first_name.' '.$student->last_name.'</span><br/><br/>die Teilnahme an folgendem Training</h3></td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<table border="0" width="100%" style="font-size: 12px;text-align: left;">';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td colspan="4" height="100"></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td width="120"></td>';
		$html .= '<td>Training:</td>';
		$html .= '<td><b>'.$learningroom->title.'</b></td>';
		$html .= '<td width="100"></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td width="120"></td>';
		$html .= '<td>Datum:</td>';
		$html .= '<td><b>'.date('d.m.Y', strtotime($learningroom->day)).'</b></td>';
		$html .= '<td width="100"></td>';
		$html .= '</tr>';
		$html .= '</tbody>';	
		return $html;
	}
}
require_once(JPATH_SITE.'/components/com_jclassroom/controllers/tcpdf_include.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');

class MYPDF extends TCPDF {
	public function Header() {
		$this->Image('images/logo-black.png', 10, 10, 90, 30, 'PNG','' , '', true, 300, '', false, false, 0, false, false, false);
		$this->writeHTMLCell(90, 30, 110,17.5, '<h1 style="font-size: 38pt;">Zertifikat</h1>', 0, 0, 0, 0, 'R', true, true, 0, false, true, 30, 'M', true);
	}
	public function Footer() {
		$now = JFactory::getDate();
		$date = new JDate($now);
		$now = $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$datumSTR = $now->format('d.m.Y');
		$datum = explode(' ',$now);
		$this->SetXY(10,-15);
		// Set font
		$this->SetFont('helvetica', '', 7);
		// Page number
		$this->Cell(0, 10, $mandant, 0, false, 'L', 0, '', 0, false, 'T', 'M');
		$this->MultiCell(50, 5, 'ceKnow (c) '.date('Y'), '0', 'L', 0, 1, '10', '285', true);
		$this->MultiCell(50, 5, 'Druck am: '.$datumSTR.' '.$datum[1], '', 'L', 0, 1, '10', '288', true);
		$this->MultiCell(50, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), '0', 'R', 0, 1, '160', '288', true);
	}
}

