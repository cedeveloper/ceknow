<?php
defined('_JEXEC') or die;

if (!class_exists('TCPDF')) {
	require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
}
$aktSite = 0;

class printAuditEngine {
	
	public function printAudit($rID) {
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('dats | Druckagentur Torsten Scheel');
		$pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE,PDF_HEADER_STRING);
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		
		// set margins
		$pdf->SetFillColor(255,255,255);
		$pdf->SetDrawColor(0,0,0);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 18);
		$pdf->SetMargins(20,50,10,false);
		$pdf->SetFont('helvetica', 'B', 12, '', false);
		$pdf->AddPage();
		$aktSite = $pdf->getAliasNumPage();
		if($aktSite = 1) {
			$pdf->getFooterFirst($rID);
			//$pdf->getAuditiert($ad);	
		}
		$pdf->SetFont('helvetica', 'R', 48, '', false);
		$pdf->SetTextColor(120,120,120);
		$pdf->MultiCell(190, 25,'Auditreport', '', 'L', 0, 1, 10, 120, true,0,false,false,25,'M',false);
		$pdf->SetFont('helvetica', 'R', 18, '', false);
		
		$pdf->AddPage();
      	$pdf->Bookmark('1. Vorwort', 0, 0, '', '', array(0,0,0));
		$IV = $pdf->getVorwort($rID);
		$pdf->AddPage();
		$pdf->Bookmark('2. Inhaltsverzeichnis', 0, 0,'', '', array(0,0,0));
		$pdf->AddPage();
      	/*$pdf->Bookmark('3. Management Summary', 0, 0, '', '', array(128,0,0));
		$pdf->getSummary($ad,$IV);
		$pdf->AddPage();
      	$pdf->Bookmark('4. Auditergebnis', 0, 0, '', '', array(128,0,0));
		$pdf->getErgebnis($ad);
		$pdf->AddPage();
      	$pdf->Bookmark('5. Maßnahmenkatalog', 0, 0, '', '', array(128,0,0));
		$pdf->getMassnahmen($ad);*/
      	$pdf->getInhaltV($rID, $IV);
		ob_end_clean();
		$pdf->Output('Auditbericht_'.$rID.'.pdf', 'I');
	}
}

class MYPDF extends TCPDF {
	
	public function Header() {
		$image_file = JPATH_SITE.'/images/logo_ceknow1.png';
		$this->Image($image_file, 10, 10, 50, 18.66, 'PNG', '', 'T', true, 300, '', false, false, 0, false, false, false);
	}	
	/*public function Footer() {
		$now = JFactory::getDate();
		$date = new JDate($now);
		$now = $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$datumSTR = $now->format('d.m.Y');
		$datum = explode(' ',$now);
		$this->SetXY(10,-15);
		// Set font
		$this->SetFont('helvetica', '', 6);
		// Page number
      	$page = $this->PageNo();
      	if($page >= 2) {
			$this->MultiCell(50, 5, 'FLEXXSOFTWARE (c) '.date('Y'), '0', 'L', 0, 1, '10', '285', true);
			$this->MultiCell(50, 5, 'Druck am: '.$datumSTR.' '.$datum[1], '', 'L', 0, 1, '10', '288', true);
			$this->MultiCell(50, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), '0', 'R', 0, 1, '160', '285', true);
       	}
	}*/
	public function getFooterFirst($rID) {
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$audit 	= JTable::getInstance('Theresults','JclassroomTable',array());
		$audit->load($rID);
		$createdBy 		= $audit->created_by;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('User','JclassroomTable',array());
		$table->load($createdBy);
		$auditCreator = $table->name;
		/*$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('CONCAT_WS(" ",b.vorname, b.nachname) as auditorenName,
			c.*,
			d.name as companyName,
			d.adresse as companyAdresse,
			CONCAT_WS(" ",d.plz,d.ort) as companyOrt'));
        $query->from($db->quoteName('#__audit_audits','a'));
		$query->join('LEFT', $db->quoteName('#__audit_auditoren', 'b') . ' ON (' . $db->quoteName('a.tblAuditorenID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->join('LEFT', $db->quoteName('#__audit_reseller', 'c') . ' ON (' . $db->quoteName('b.tblResellerID') . ' = ' . $db->quoteName('c.id') . ')');
		$query->join('LEFT', $db->quoteName('#__audit_unternehmen', 'd') . ' ON (' . $db->quoteName('a.tblUnternehmenID') . ' = ' . $db->quoteName('d.id') . ')');
		$query->where($db->quoteName('a.id').'='.$db->quote($ad));
		$db->setQuery($query);
		$reseller = $db->loadObject();
		//print_r($reseller);
		//GET RESELLER-DATA
		$resellerName = $reseller->firma;
		$adresse = $reseller->adresse;
		$ort = $reseller->plz.' '.$reseller->ort;
		$logo = $reseller->logo;
		$auditorenName = $reseller->auditorenName;
		//GET AUDIT-DATA
		/*$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*','b.name'));
        $query->from($db->quoteName('#__audit_audits','a'));
		$query->join('LEFT', $db->quoteName('#__audit_auditoren', 'b') . ' ON (' . $db->quoteName('a.auditor') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('a.id').'='.$db->quote($id));
		$db->setQuery($query);
		$result = $db->loadObject();
		
		$image_file = JPATH_SITE.'/images/Auditum/footer_auditum.jpg';
		$this->Image($image_file, 10, 229, 190, 50, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$logo_path = JPATH_SITE.'/images/auditum/logo/system/flexxsoftware_logo.png';
		$this->Image($logo_path, 175, 240, 20, 20, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);*/
		//GET THE AUDIT
		$this->SetTextColor(0,0,0);
		$this->SetFont('helvetica', '', 12);
		$this->MultiCell(170, 6, "Dieses Audit wurde erstellt von: ".$auditCreator, '0', 'L', 0, 1, '20', '262', true);
		$this->SetFont('helvetica', '', 10);
		$this->MultiCell(170, 3, date('d.m.Y H:i', strtotime($audit->created)), '0', 'L', 0, 1, '20', '268', true);
	}
	/*public function getAuditiert($ad) {
		//GET AUDIT-KUNDE
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Audit','AuditumTable',array());
		$table->load($ad);
		$kunde = $table->kunde;
		$beginn = $table->beginn;
		$ende = $table->ende;
		$unternehmen = $table->unternehmen;
		$auditkatalogID = $table->auditkatalog;
		$auditierte = $table->auditierte;
		//GET AUDIT-DATA
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.name'));
        $query->from($db->quoteName('#__audit_auditkataloge','a'));
		$query->where($db->quoteName('a.id').'='.$db->quote($auditkatalogID));
		$db->setQuery($query);
		$auditkatalogName = $db->loadResult();
		//GET KUNDE-DATA
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__audit_kunden','a'));
		$query->where($db->quoteName('a.id').'='.$db->quote($kunde));
		$db->setQuery($query);
		$result = $db->loadObject();
		//GET UNTERNEHMEN-DATA
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__audit_unternehmen','a'));
		$query->where($db->quoteName('a.id').'='.$db->quote($unternehmen));
		$db->setQuery($query);
		$unternehmen = $db->loadObject();
		
		$this->SetTextColor(0,0,0);
		$this->SetFont('helvetica', '', 12);
		$this->MultiCell(150, 6, $unternehmen->name, '0', 'L', 0, 1, '10', '45', true);
		$beginn = new DateTime($beginn);
      	$ende = new DateTime($ende);
      	$checkBeginn = $beginn->format('d.m.Y');
      	$checkEnde = $ende->format('d.m.Y');
      	if($checkBeginn == $checkEnde) {
        	$auditzeitraum = $beginn->format('d.m.Y').' von '.$beginn->format('H:i').' bis '.$ende->format('H:i');
        } else {
          	$auditzeitraum = $beginn->format('d.m.Y H:i').' - '.$ende->format('d.m.Y H:i');
        }
		$this->MultiCell(150, 6, 'Auditzeitraum: '.$auditzeitraum, '0', 'L', 0, 1, '10', '51', true);
		$this->MultiCell(150, 6, 'Auditnummer: '.str_pad($id,6,'0',STR_PAD_LEFT), '0', 'L', 0, 1, '10', '57', true);
		$this->SetFont('helvetica', '', 24);
		$this->MultiCell(190, 10, $auditkatalogName, '', 'L', 0, 1, 10, 140, true,0,false,false,20,'M',false);
	}*/
	public function getVorwort($rID) {
		$session = JFactory::getSession();
		$remove = array('<p>','</p>');
		$auditierteS = '';
		//GET AUDIT-KUNDE
		$this->SetMargins(20,40,10,false);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Theresults','JclassroomTable',array());
		$result->load($rID);
		$createdBy 		= $result->created_by;
		$preface 		= $result->preface;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$table->load($createdBy);
		$auditBy 	= $table->name;
		/*
		//if(count($auditierteS) == 1) {
		$len = strlen($auditierteS);
		$auditierteS = substr($auditierteS, 0,$len -2);
		//}
		$tblAuditorenID = $table->tblAuditorenID;
		$y = 40;
		//Globales Vorwort laden
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Fragenkatalog','AuditumTable',array());
		$table->load($tblFragenkatalogeID);
		$vorwort 	= $table->vorwort;
      	//GET UNTERNEHMEN-DATA
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Company','AuditumTable',array());
		$table->load($tblCompaniesID);
		$unternehmen_name 	= $table->name;
      	$unternehmen_beschreibung = $table->beschreibung;
      	$unternehmen_beschreibung = str_replace($remove,'',$unternehmen_beschreibung);
		//GET AUDITOR
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Auditor','AuditumTable',array());
		$table->load($tblAuditorenID);
		$auditorname = $table->vorname.' '.$table->nachname;*/
		//$this->SetTextColor(182,201,49);
		$this->SetTextColor(0,0,0);
		$this->SetFillColor(255,255,255);
		$this->SetDrawColor(255,255,255);
		$this->SetFont('helvetica', 'R', 10);
		$html = '';
		$html .= '<h2>1. Vorwort</h2>';
		
		$preface = str_replace('{unternehmen_name}',$unternehmen_name,$preface);
		$preface = str_replace('{unternehmen_beschreibung}',$unternehmen_beschreibung,$preface);

		//$vorwort = str_replace('{audit_start}',$beginn->format('d.m.Y H:i'),$vorwort);
		//$vorwort = str_replace('{audit_ende}',$ende->format('d.m.Y H:i'),$vorwort);
		$preface = str_replace('{auditor_name}',$auditorname,$preface);
		$preface = str_replace('{auditierte}',$auditierteS,$preface);
		$preface = str_replace('{unterlagen}',$unterlagen,$preface);
		$preface = explode('<p>--</p>',$preface);
		if($preface) {
			foreach($preface as $teilstring) {
				$parsed = $this->get_string_between($teilstring, '**', '**');
				$this->Bookmark($parsed, 1, 0, '', '', array(0,0,0));
				$teilstring = str_replace('**','',$teilstring);
				$html .= $teilstring;
			}
		}
		/*if($indVorwort) {
			$indVorwort = str_replace('{unternehmen_name}',$unternehmen_name,$indVorwort);
			$indVorwort = str_replace('{unternehmen_beschreibung}',$unternehmen_beschreibung,$indVorwort);

			$indVorwort = str_replace('{audit_start}',$beginn->format('d.m.Y H:i'),$indVorwort);
			$indVorwort = str_replace('{audit_ende}',$ende->format('d.m.Y H:i'),$indVorwort);
			$indVorwort = str_replace('{auditor_name}',$auditorname,$indVorwort);
			$indVorwort = str_replace('{auditierte}',$auditierteS,$indVorwort);
			$indVorwort = explode('<p>--</p>',$indVorwort);
			if($indVorwort) {
				foreach($indVorwort as $teilstring) {
					$parsed = $this->get_string_between($teilstring, '**', '**');
					$this->Bookmark($parsed, 1, 0, '', '', array(128,0,0));
					$teilstring = str_replace('**','',$teilstring);
					$html .= $teilstring;
				}
			}
		}*/
		$this->writeHTMLCell(170, 200, 20,40, $html, 0, 1, 1, true, 'J', true);
		$y = $this->getY() + 4;

		$currentPage = $this->PageNo();
		return $currentPage;
	}
	function get_string_between($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}
	public function getInhaltV($ad, $page) {
		//GET AUDIT-KUNDE
		$this->SetMargins(10,30,10,false);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Theresults','JclassroomTable',array());
		$table->load($ad);
		$this->SetMargins(20,30,10,false);		
		$this->SetTextColor(0,0,0);
		$y = 40;
      	// add a new page for TOC
        $this->addTOCPage();

        // write the TOC title and/or other elements on the TOC page
        $this->SetTextColor(120,120,120);
		$this->SetFont('helvetica', 'B', 14);
       	$this->MultiCell(150, 6, '2. Inhaltsverzeichnis', '0', 'L', 0, 1, '20', '30', true);
       	//$this->Bookmark('2. Inhaltsverzeichnis', 1, 0, '', '', array(128,0,0));
      	$this->SetTextColor(0,0,0);
        $this->SetFont('helvetica', 'R', 10);

        // define styles for various bookmark levels
        $bookmark_templates = array();
        // A monospaced font for the page number is mandatory to get the right alignment
        $bookmark_templates[0] = '<table border="0" cellpadding="0" cellspacing="5" style="background-color:#FFFFFF">
        <tr>
		<td width="145mm">
			<span style="font-family:helvetica;font-weight:bold;font-size:12pt;color:black;">#TOC_DESCRIPTION#</span>
		</td>
        <td width="28mm"><span style="font-family:helvetica;font-weight:bold;font-size:12pt;color:black;" align="right">#TOC_PAGE_NUMBER#</span></td>
        </tr>
        </table>';
        $bookmark_templates[1] = '<table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td width="10mm">&nbsp;</td>
        <td width="140mm"><span style="color:black;">#TOC_DESCRIPTION#</span></td>
        <td width="26mm"><span style="font-size: 11pt;font-weight:bold;color:black;" align="right">#TOC_PAGE_NUMBER#</span></td>
        </tr>
        </table>';
        // add other bookmark level templates here ...

        // add table of content at page 1
        // (check the example n. 45 for a text-only TOC
        $this->addHTMLTOC($page + 1, 'INDEX', $bookmark_templates, true, 'B', array(128,0,0));

        // end of TOC page
        $this->endTOCPage();
	}
	/*public function getSummary($ad,$page) {
		//GET UNTERNEHMEN
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('audit','AuditumTable',array());
		$table->load($ad);
		$tblUnternehmenID 	= $table->tblUnternehmenID;
		$tblFragenkatalogeID= $table->tblFragenkatalogeID;
		//GET UNTERNEHMEN-DATA
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Company','AuditumTable',array());
		$table->load($tblUnternehmenID);
		$unternehmen_name = $table->name;
		//GET AUDIT-KUNDE
		$this->SetMargins(10,30,10,false);
		$this->SetTextColor(182,201,49);
		$this->SetFont('helvetica', 'B', 14);
		$this->MultiCell(150, 6, '3. Management Summary', '0', 'L', 0, 1, '20', '30', true);
		$this->SetFont('helvetica', 'B', 12);
		$this->SetTextColor(0,0,0);
		$this->MultiCell(180, 6, 'Auditergebnis in der grafischen Übersicht', '0', 'C', 0, 1, '20', '40', true);
		$this->getChart($ad);
		$y = $this->getY() + 150;
		$this->SetTextColor(0,0,0);
		$this->SetFont('helvetica', '', 10);
      	//GET MAX PUNKTZAHL
      	$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select(array('Sum(a.punkte) as maxpunkte'));
		$query->from($db->quoteName('#__audit_fragen','a'));
      	$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
		$db->setQuery($query);
		$i = $db->loadResult();
		$html = 'Maximal erreichbare Punkte im Audit: '.$i.' Punkte.<br/>';
		//GET IST PUNKTZAHL
		$query = $db->getQuery(true);
		$query->select(array('Sum(a.punkte) as istpunkte'));
		$query->from($db->quoteName('#__audit_antworten','a'));
		$query->join('right', $db->quoteName('#__audit_fragen', 'b') . ' ON (' . $db->quoteName('a.tblFragenID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('a.tblAuditsID').'='.$db->quote($ad));
		$db->setQuery($query);
		$s = $db->loadResult();
      	$reifegrad = ($s * 100) / $i;
      	$html .= 'Tatsächlich erreichte Punkte im Audit: '.$s.' Punkte.<br/>';
      	$html .= 'Daraus ergibt sich ein Reifegrad von: '.number_format($reifegrad,2,',','.').'%.<br/><br/>';
		//GET KO
		$query = $db->getQuery(true);
		$query->select(array('ko'));
		$query->from($db->quoteName('#__audit_antworten','a'));
		$query->where($db->quoteName('a.tblAuditsID').'='.$db->quote($ad));
		$query->where($db->quoteName('a.ko').'='.$db->quote(1));
		$query->group('a.ko');
		$db->setQuery($query);
		$db->execute();
		$ko = $db->getNumRows();
		if($ko) {
			$html .= 'Mindestens eine der negativ beantworteten Fragen ist ein KO-Kriterium<br/>';
		}
		$html .= '<b><i>KOMMENTAR TS: Der nachfolgende Text wird aus der Tabelle "Grade" abgefragt und wird anhand des ermittelten Reifegrades von '. number_format($reifegrad,2,',','.').'% ermittelt.</i></b><br/>';
		//SET GESAMTBETRACHTUNG
		$grade = '';
      	if($reifegrad >= 80) {
        	$query = $db->getQuery(true);
			$query->select(array('a.bewertung'));
			$query->from($db->quoteName('#__audit_grade','a'));
			$query->where($db->quoteName('a.name').'='.$db->quote('Gesamt'));
			$query->where($db->quoteName('a.grad').'='.$db->quote('100'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$db->setQuery($query);
			$grade .= $db->loadResult();
        } elseif($reifegrad >= 60 && $reifegrad < 80) {
          	$query = $db->getQuery(true);
			$query->select(array('a.bewertung'));
			$query->from($db->quoteName('#__audit_grade','a'));
			$query->where($db->quoteName('a.name').'='.$db->quote('Gesamt'));
			$query->where($db->quoteName('a.grad').'='.$db->quote('80'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$db->setQuery($query);
			$grade .= $db->loadResult();
        } elseif($reifegrad >= 40 && $reifegrad < 60) {
          	$query = $db->getQuery(true);
			$query->select(array('a.bewertung'));
			$query->from($db->quoteName('#__audit_grade','a'));
			$query->where($db->quoteName('a.name').'='.$db->quote('Gesamt'));
			$query->where($db->quoteName('a.grad').'='.$db->quote('60'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$db->setQuery($query);
			$grade .= $db->loadResult();
        } elseif($reifegrad >= 21 && $reifegrad < 40) {
          	$query = $db->getQuery(true);
			$query->select(array('a.bewertung'));
			$query->from($db->quoteName('#__audit_grade','a'));
			$query->where($db->quoteName('a.name').'='.$db->quote('Gesamt'));
			$query->where($db->quoteName('a.grad').'='.$db->quote('40'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$db->setQuery($query);
			$grade .= $db->loadResult();
        } elseif($reifegrad <= 20) {
          	$query = $db->getQuery(true);
			$query->select(array('a.bewertung'));
			$query->from($db->quoteName('#__audit_grade','a'));
			$query->where($db->quoteName('a.name').'='.$db->quote('Gesamt'));
			$query->where($db->quoteName('a.grad').'='.$db->quote('20'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$db->setQuery($query);
			$grade .= $db->loadResult();
        }
        //Erweiterung
        if($reifegrad < 80):
			$query = $db->getQuery(true);
			$query->select(array('a.bewertung'));
			$query->from($db->quoteName('#__audit_grade','a'));
			$query->where($db->quoteName('a.name').'='.$db->quote('Erweiterung'));
			$query->where($db->quoteName('a.grad').'='.$db->quote('< 80'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$db->setQuery($query);
			$erweiterung = $db->loadResult();
		endif;
		if($reifegrad >= 80):
			$query = $db->getQuery(true);
			$query->select(array('a.bewertung'));
			$query->from($db->quoteName('#__audit_grade','a'));
			$query->where($db->quoteName('a.name').'='.$db->quote('Erweiterung'));
			$query->where($db->quoteName('a.grad').'='.$db->quote('> 80'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$db->setQuery($query);
			$erweiterung = $db->loadResult();
		endif;
        $html .= htmlentities($grade).'<br/>';
        $html .= '<b><i>KOMMENTAR TS: Der nachfolgende Text wird aus der Tabelle "Grade" -> "Erweiterung" abgefragt und wird in Abhängigkeit des Reifegrades < 80% oder > 80% berechnet.</b><br/>';
        $erweiterung = str_replace('{Reifegrad}', number_format($reifegrad,2,'.',','), $erweiterung);
        $html .= '<span style="font-weight: normal;">'.$erweiterung.'</span>';
        $this->writeHTMLCell(180, '', 20, $y, $html, 0, 1, 1, true, 'J', true);
	}
	public function getChart($ad) {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('audit','AuditumTable',array());
		$table->load($ad);
		$tblFragenkatalogeID = $table->tblFragenkatalogeID;
		//GET FRAGEN-DATA
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.prueffeld'));
        $query->from($db->quoteName('#__audit_fragen','a'));
		$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
		$query->group('a.prueffeld');
		$db->setQuery($query);
		$prueffelder = $db->loadObjectList();
		require_once JPATH_SITE.'/components/com_auditum/assets/php/SVGGraph.php';
		$settings = array(
		  'back_colour'       => '#eee',    'stroke_colour'     => '#e1e1e1',
		  'back_stroke_width' => 0,         'back_stroke_colour'=> '#eee',
		  'axis_colour'       => '#666', 	'axis_overlap'      => 10,
		  'axis_font'         => 'Georgia', 'axis_font_size'    => 8,
		  'grid_colour'       => '#666',    //'grid_division_v' 	=> array(10,20,30,40,50,60,70,80,90, 100),
		  'label_colour'       => '#000',
		  'pad_right'         => 5,         'pad_left'           => 5,
		  'link_base'         => '/',       'link_target'        => '_top',
		  'fill_under'        => true
		);
		$data = array();
		foreach($prueffelder as $prueffeld) {
          	$query = $db->getQuery(true);
			$query->select(array('Sum(a.punkte) as istpunkt'));
			$query->from($db->quoteName('#__audit_fragen','a'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$query->where($db->quoteName('a.prueffeld').'='.$db->quote($prueffeld->prueffeld));
			$db->setQuery($query);
			$i = $db->loadResult();
          	//
			$query = $db->getQuery(true);
			$query->select(array('Sum(a.punkte) as maxpunkte'));
			$query->from($db->quoteName('#__audit_antworten','a'));
			$query->join('LEFT', $db->quoteName('#__audit_fragen', 'b') . ' ON (' . $db->quoteName('a.tblFragenID') . ' = ' . $db->quoteName('b.id') . ')');
			$query->where($db->quoteName('a.tblAuditsID').'='.$db->quote($ad));
			$query->where($db->quoteName('b.prueffeld').'='.$db->quote($prueffeld->prueffeld));
			$db->setQuery($query);
			$s = $db->loadResult();
          	if(!$i) {$i = 1;}
          	if(!$s) {$s = 0;}
          	if(!$s) {
            	$s = 0;
     			$reifegrad = 0;
            } else {
              	$reifegrad = ($s * 100) / $i;
              	$data[$prueffeld->prueffeld] = $reifegrad;
            }
		}
		if($data) {
			$colours = array('rgb(182,201,49)','white');
			$graph = new SVGGraph(500, 400,$settings);
			$graph->Values($data); 
			$graph->colours = $colours;
			$output = $graph->fetch('MultiRadarGraph');
			$this->SetDrawColor(120,120,120);
			$this->ImageSVG('@' . $output, $x=20, $y=50, $w='', $h='', '', $align='', $palign='', $border=1, $fitonpage=false);
		} else {
			$this->SetFont('helvetica', 'R', 12);
			$html = '<br/><br/>Nicht genug Daten für eine grafische Darstellung des Auditergebnis.';
			$this->writeHTMLCell(180, '', 20, $y, $html, 0, 1, 0, true, 'C', true);
		}
	}
	public function getErgebnis($ad) {
		$style2 = array('width' => 0.2,'color' => array(157, 167, 177));
		//GET AUDIT
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('audit','AuditumTable',array());
		$table->load($ad);
		$indVorwort = $table->vorwort;
		$beginn 	= new Datetime($table->beginn);
		$ende 		= new Datetime($table->ende);
      	$beginn_zertifizierung 	= new Datetime($table->beginn_zertifizierung);
      	$ende_zertifizierung 	= new Datetime($table->ende_zertifizierung);
      	$frist_zertifizierung 	= new Datetime($table->frist_zertifizierung);
      	$tblUnternehmenID 	= $table->tblUnternehmenID;
      	$tblAuditorenID 	= $table->tblAuditorenID;
      	$tblFragenkatalogeID = $table->tblFragenkatalogeID;
      	//GET Auditoren-DATA
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Auditor','AuditumTable',array());
		$table->load($tblAuditorenID);
		$auditorname 	= $table->vorname.' '.$table->nachname;
		//GET UNTERNEHMEN-DATA
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Company','AuditumTable',array());
		$table->load($tblUnternehmenID);
		$unternehmen_name 			= $table->name;
		$unternehmen_beschreibung 	= $table->beschreibung;

		$this->SetTextColor(182,201,49);
		$this->SetFillColor(200,200,200);
		$this->SetDrawColor(255,255,255);
		$this->SetFont('helvetica', 'B', 16);
		$this->MultiCell(190, 8,'4. Auditergebnis', '', 'L', 0, 1, 10, 30, true,0,false,false,8,'M',false);
		$y = 40;
		if($indVorwort) {
			$indVorwort = str_replace('{unternehmen_name}',$unternehmen_name,$indVorwort);
			$indVorwort = str_replace('{unternehmen_beschreibung}',$unternehmen_beschreibung,$indVorwort);
			$indVorwort = str_replace('{audit_start}',$beginn->format('d.m.Y H:i'),$indVorwort);
			$indVorwort = str_replace('{audit_ende}',$ende->format('d.m.Y H:i'),$indVorwort);
			$indVorwort = str_replace('{auditor_name}',$auditorname,$indVorwort);
			//$indVorwort = str_replace('{auditierte}',$auditierteS,$indVorwort);
			$this->SetFont('helvetica', 'R', 10);
			$this->SetTextColor(0,0,0);
			$this->writeHTMLCell(180, '', 20, $y, $indVorwort, 0, 1, 0, true, 'J', true);
			$y = $this->GetY() + 4;
		} else {
			$y = $this->getY();
		}
		//GET EINLEITUNG
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('a.einleitung,a.reifegrad_negativ'));
		$query->from($db->quoteName('#__audit_fragenkataloge','a'));
		$query->where($db->quoteName('a.id').'='.$db->quote($tblFragenkatalogeID));
		$db->setQuery($query);
		$textbaustein = $db->loadObject();
		//GET MAX PUNKTZAHL
		$query = $db->getQuery(true);
		$query->select(array('Sum(a.punkte) as maxpunkte'));
		$query->from($db->quoteName('#__audit_fragen','a'));
      	$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
		$db->setQuery($query);
		$i = $db->loadObject();
		//GET IST PUNKTZAHL
		$query = $db->getQuery(true);
		$query->select(array('Sum(a.punkte) as istpunkte'));
		$query->from($db->quoteName('#__audit_antworten','a'));
		$query->join('right', $db->quoteName('#__audit_fragen', 'b') . ' ON (' . $db->quoteName('a.tblFragenID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('a.tblAuditsID').'='.$db->quote($ad));
		$db->setQuery($query);
		$s = $db->loadObject();
		//GET KO
		$query = $db->getQuery(true);
		$query->select(array('COUNT(a.ko)'));
		$query->from($db->quoteName('#__audit_antworten','a'));
		$query->join('right', $db->quoteName('#__audit_fragen', 'b') . ' ON (' . $db->quoteName('a.tblFragenID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('a.tblAuditsID').'='.$db->quote($ad));
		$query->where($db->quoteName('a.ko').'='.$db->quote(1));
		$db->setQuery($query);
		$ko = $db->loadResult();
      	$reifegrad = ($s->istpunkte * 100) / $i->maxpunkte;
		
		//WRITE EINLEITUNG
		if($reifegrad < 80 || $ko > 0) {
			//$einleitungAudit = str_replace('{kunde}',$kunde,$textbaustein->reifegrad_negativ);
			$einleitungAudit = str_replace('{gesamtreifegrad}',number_format($reifegrad,1,',','.'),$textbaustein->reifegrad_negativ);
			$einleitungAudit = str_replace('{unternehmen_name}',$unternehmen_name,$einleitungAudit);
			$einleitungAudit = str_replace('{anfang_zertifizierung}',$beginn_zertifizierung->format('d.m.Y'),$einleitungAudit);
			$einleitungAudit = str_replace('{ende_zertifizierung}',$ende_zertifizierung->format('d.m.Y'),$einleitungAudit);
			$einleitungAudit = str_replace('{frist_audit}',$frist_zertifizierung->format('d.m.Y'),$einleitungAudit);
			if($ko > 0) {
				$einleitungAudit .= '<b>Aufgrund einer Verletzung eines KO-Kriteriums kann das DIITS-Zertifikat derzeit nicht vergeben werden.</b>';
			}
			$this->SetFont('helvetica', 'R', 10);
			$this->SetTextColor(0,0,0);
			$this->writeHTMLCell(180, '', 20, $y, $einleitungAudit, 0, 1, 0, true, 'J', true);
			$y = $this->getY() + 4;
			$i = 1;
		} 
		if($reifegrad >= 80 && $ko == 0) {
			$einleitungAudit = str_replace('{kunde}',$kunde,$textbaustein->einleitung);
			$einleitungAudit = str_replace('{gesamtreifegrad}',number_format($reifegrad,1,',','.'),$einleitungAudit);
			$einleitungAudit = str_replace('{unternehmen_name}',$unternehmen_name,$einleitungAudit);
			$einleitungAudit = str_replace('{anfang_zertifizierung}',$beginn_zertifizierung->format('d.m.Y'),$einleitungAudit);
			$einleitungAudit = str_replace('{ende_zertifizierung}',$ende_zertifizierung->format('d.m.Y'),$einleitungAudit);
			$einleitungAudit = str_replace('{frist_audit}',$frist_zertifizierung->format('d.m.Y'),$einleitungAudit);
			$this->SetFont('helvetica', 'R', 10);
			$this->SetTextColor(0,0,0);
			$this->writeHTMLCell(180, '', 20, $y, $einleitungAudit, 0, 1, 0, true, 'J', true);
			$y = $this->getY() + 4;
			$i = 1;
		}
		//SET EINZELBETRACHTUNG JE PRÜFFELD
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('DISTINCT prueffeld');
        $query->from($db->quoteName('#__audit_fragen'));
		$query->where($db->quoteName('tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
		$query->order('ordering asc');
		$query->group('prueffeld');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		//WRITE FOR EACH PRÜFFELD
		$html = '';
		$html .= '<p><b>KOMMENTAR TS: Die folgenden Texte werden zunächst aus den Feldern Kapitel im Fragenkatalog abgefragt. Sie können nur über Änderungen im Text in den importierten Excel-Tabellen geändert werden. Sie werden statisch ausgegeben und stehen in keinem Verhältnis zum Auditergebnis.<br/>Dem Einleitungstext folgt eine Auflistung der Ergebnisse aus dem Audit je Frage. Hier wird entschieden, ob die Antwort ja, ja mit Ausnahme oder nein war und der entsprechende Text aus den Felder <i>negativ</i> oder <i>positiv</i> in den importierten Fragen aus den Excel-Tabellen ausgegeben. Diese Texte können nur in den Excel-Tabellen geändert werden.</p>';
		foreach($result as $fragenkatalog) {
			$this->SetFont('helvetica', 'B', 12);
			$this->SetTextColor(0,0,0);
			$this->Bookmark('4.'.$i.' '.$fragenkatalog->prueffeld, 1, 0, '', '', array(128,0,0));
			$html .= '<h3>4.'.$i.'. '.$fragenkatalog->prueffeld.'</h3>';
			//Laden der Einleitung je Kapitel
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
			$table = JTable::getInstance('Kapitel','AuditumTable',array());
			$load = array('tblFragenkatalogeID' => $tblFragenkatalogeID, 'name' => (string)$fragenkatalog->prueffeld);
			$table->load($load);
			if($table->einleitung) {
				$html .= '<b>KOMMENTAR TS: Der Einleitungstext stammt aus der Import-CSV-Datei "Kapitel" und wird beim Import dem jeweiligen Prüffeld zugeordnet.</b>';
				$html .= '<p style="font-size: 10px;font-weight: normal;">'.$table->einleitung.'</p>';
			}
			$i++;
			$this->SetFont('helvetica', 'R', 10);
			//GET ALL FRAGEN FROM PRÜFFELD
			$query = $db->getQuery(true);
			$query->select(array('id,frage'));
			$query->from($db->quoteName('#__audit_fragen','a'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$query->where($db->quoteName('a.prueffeld').'='.$db->quote($fragenkatalog->prueffeld));
			$query->order('ordering asc');
			$db->setQuery($query);
			$fragen = $db->loadObjectList();
			if($fragen) {
				$html .= '<b>KOMMENTAR TS: Hier beginnt der dynamische Teil aus negativ/positiv. Der Positiv-Teil wird aus der Spalte "Positiv" der Import-CSV-Datei "Fragen" entnommen, der Negativ-Teil aus der Spalte "Negativ". Änderungen am Text müssen in der Import-CSV-Datei "Fragen" erfolgen.</b><br/>';
				$countAbsatz = 0;
				foreach($fragen as $frage) {
					//GET ISTPUNKTE
					$query = $db->getQuery(true);
					$query->select(array('ergebnisText,ko,kotext,antwort,ausnahme'));
					$query->from($db->quoteName('#__audit_antworten','a'));
					$query->where($db->quoteName('a.tblAuditsID').'='.$db->quote($ad));
					$query->where($db->quoteName('a.tblFragenID').'='.$db->quote($frage->id));
					$db->setQuery($query);
					$antwort = $db->loadObject();
					
					//GET THE POSITIV / NEGATIV FIELDS
					if($antwort) {
						if($antwort->ko == 1) {
							$html .= $antwort->kotext.' ';
						} elseif($antwort->antwort == 'jaA') {
							$html .= $antwort->ausnahme.' ';
						} else {
							$html .= $antwort->ergebnisText.'<br/>';
						}
					} 
					if($countAbsatz < 3) {
						$countAbsatz++;
					} else {
						$html .= '<br/>';
						$countAbsatz = 0;
					}	
				}
				//GET Grad for Prüffeld
				$query = $db->getQuery(true);
				$query->select(array('bewertung'));
				$query->from($db->quoteName('#__audit_grade','a'));
				$query->where($db->quoteName('a.name').'='.$db->quote($fragenkatalog->prueffeld));
				$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
				$query->where($db->quoteName('a.grad').'<'.$db->quote($reifegrad));
				$db->setQuery($query);
				$gradPrueffeld = $db->loadObject();
				$html .= '<br/><b>KOMMENTAR TS: Die Einzelbewertung jedes Prüffeldes wird anhand des Reifegrades aus der Import-CSV-Datei "Grad" ermittelt.</b><br/>';
				$html .= $gradPrueffeld->bewertung;
			}
			$i++;
			$y = $this->getY() + 2;
		}

		$this->writeHTMLCell(180, '', 20, $y, $html, 0, 1, 0, true, 'J', true);
	}
	public function getMassnahmen($ad) {
		//GET AUDIT-KUNDE
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('audit','AuditumTable',array());
		$table->load($ad);
		$tblFragenkatalogeID = $table->tblFragenkatalogeID;
		//GET MAX PUNKTZAHL Gesamtaudit
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('Sum(a.punkte) as maxpunkte'));
		$query->from($db->quoteName('#__audit_fragen','a'));
      	$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
		$db->setQuery($query);
		$i = $db->loadObject();
		//GET IST PUNKTZAHL
		$query = $db->getQuery(true);
		$query->select(array('Sum(a.punkte) as istpunkte'));
		$query->from($db->quoteName('#__audit_antworten','a'));
		$query->join('right', $db->quoteName('#__audit_fragen', 'b') . ' ON (' . $db->quoteName('a.tblFragenID') . ' = ' . $db->quoteName('b.id') . ')');
		$query->where($db->quoteName('a.tblAuditsID').'='.$db->quote($ad));
		$db->setQuery($query);
		$istpunkte = $db->loadObject();
      	$reifegrad = ($istpunkte->istpunkte * 100) / $i->maxpunkte;
		/*if($reifegrad == 100) {
			return false;	
		}
		//GET FRAGEN
		$query = $db->getQuery(true);
		$query->select(array('a.prueffeld'));
        $query->from($db->quoteName('#__audit_fragen','a'));
		$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
		$query->order('ordering asc');
		$query->group('prueffeld');
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$this->SetTextColor(182,201,49);
		$this->SetFillColor(200,200,200);
		$this->SetDrawColor(255,255,255);
		$this->SetFont('helvetica', 'B', 16);
		$this->MultiCell(190, 8,'5. Maßnahmenkatalog', '', 'L', 0, 1, 10, 30, true,0,false,false,8,'M',false);
		$y = 40;
		$i = 1;
		$html = '';
		$html .= '<p><b>Die folgenden Texte werden aus den Graden im Fragenkatalog nach Prüffeld ermittelt und können nur über Änderungen in den importierten Excel-Tabellen geändert werden. Sie werden in Abhängigkeit zum erreichten Auditergebnis berechnet.</b></p><br/>';
		foreach($result as $fragenkatalog) {
			$query = $db->getQuery(true);
			$query->select(array('Sum(a.punkte) as maxpunkte'));
			$query->from($db->quoteName('#__audit_fragen','a'));
	      	$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
	      	$query->where($db->quoteName('a.prueffeld').'='.$db->quote($fragenkatalog->prueffeld));
			$db->setQuery($query);
			$maxPrueffeld = $db->loadObject();
			$prueffeld = $fragenkatalog->prueffeld;
			$prueffeldText = '5.'.$i.' '.$prueffeld;
			$this->SetFont('helvetica', 'B', 12);
			$this->SetTextColor(0,0,0);
			$this->Bookmark($prueffeldText, 1, 0, '', '', array(128,0,0));
			$html .= '<h3>'.$prueffeldText.'</h3>';
			//$this->MultiCell(180, 8, $prueffeldText, '', 'L', 0, 1, 20, $y, true,0,false,false,8,'M',false);
			$y = $this->getY();
			$this->SetFont('helvetica', 'R', 10);
			$this->SetTextColor(0,0,0);
			//GET ALL FRAGEN FROM PRÜFFELD
			$query = $db->getQuery(true);
			$query->select(array('id,ausnahme'));
			$query->from($db->quoteName('#__audit_fragen','a'));
			$query->where($db->quoteName('a.tblFragenkatalogeID').'='.$db->quote($tblFragenkatalogeID));
			$query->where($db->quoteName('a.prueffeld').'='.$db->quote($fragenkatalog->prueffeld));
			$query->order('ordering asc');
			$db->setQuery($query);
			$fragen = $db->loadObjectList();
			if($fragen) {
				$katalog 		= '';
				$countAbsatz 	= 0;
				$punkte 		= 0;
				foreach($fragen as $frage) {
					$query = $db->getQuery(true);
					$query->select(array('punkte'));
					$query->from($db->quoteName('#__audit_antworten','a'));
					$query->where($db->quoteName('a.tblAuditsID').'='.$db->quote($ad));
					$query->where($db->quoteName('a.tblFragenID').'='.$db->quote($frage->id));
					$db->setQuery($query);
					$fragePunkte = $db->loadResult();
					$punkte += $fragePunkte;
					if($fragePunkte == 0) {
						$gs = 0;
						$katalog .= '<p>'.$frage->ausnahme.'</p><br/>';
					} 
				}
			}
			$i++;
			if($maxPrueffeld->maxpunkte == $punkte) {
				$html .= 'Keine Maßnahmen erforderlich.<br/>';
			} else {
				$html .= $katalog;
			}
		}
		$this->writeHTMLCell(180, '', 20, $y, $html, 0, 1, 0, true, 'J', true);
	}*/
}
?>