<?php
defined('_JEXEC') or die;
if (!class_exists('TCPDF')) {
	require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
	require_once JPATH_SITE . '/components/com_jclassroom/models/stage.php';
}

class printOrder {
	
	public function order($id) {
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('dats | Druckagentur Torsten Scheel');
		$pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE,PDF_HEADER_STRING);
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		// set margins
		$pdf->SetMargins(25, 10, 10, true);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->SetFont('helvetica', 'R', 14, '', false);
		$pdf->AddPage();
		//$pdf->Rect(0, 0, 40, 90, 'DF', '', array(220, 220, 200));
		$adress = $this->getAdress($id);
		$pdf->writeHTMLCell(85, 5, 25, 50, $adress, 0,0, 0, 1,false, 'left', true);
		$company = $this->getCompany($id);
		$pdf->writeHTMLCell(85, 5, 125, 50, $company, 0,0, 0, 1,false, 'left', true);
		$html = $this->getResult($id);
		$pdf->writeHTMLCell(165, 100, 25, 97, $html, 0,0, 0, 1,false, 'left', true);
		//$pdf->writeHTML($html, true, false, true, false, '');*/
		ob_end_clean();
		$pdf->Output('components/com_inclure/pdf/Dispo1_BLG'.$id.'.pdf', 'I');
	}
	function getAdress($id) {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Order','JclassroomTable',array());
		$table->load($id);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$student 	= JTable::getInstance('Student','JclassroomTable',array());
		$load 		= array('tblUserID' => $table->userID);
		$student->load($load);
		// Load customer as adress for sending
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$customer 	= JTable::getInstance('Customer','JclassroomTable',array());
		$customer->load($table->customerID);
		$html = '<div style="font-size:12px;">';
		$html .= '<small style="font-size: 6px;">'.$customer->company_name.' | '.$customer->address.' | ' .$customer->postcode.' '.$customer->city.'</small><br/>';
		$html .= '<span>'.$student->salutation.'</span><br/>';
		$html .= '<span>'.$student->first_name.' '.$student->last_name.'</span><br/>';
		$html .= '<span>'.$student->adress.'</span><br/>';
		$html .= '<span>'.$student->postcode.' '.$student->city.'</span>';
		$html .= '</div>';
		return $html;
	}
	function getCompany($id) {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Order','JclassroomTable',array());
		$table->load($id);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$student 	= JTable::getInstance('Student','JclassroomTable',array());
		$load 		= array('tblUserID' => $table->userID);
		$student->load($load);
		// Load customer as adress for sending
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$customer 	= JTable::getInstance('Customer','JclassroomTable',array());
		$customer->load($table->customerID);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$company 	= JTable::getInstance('Company','JclassroomTable',array());
		$company->load($student->companyID);
		$html = '<div style="font-size:10px;">';
		$html .= '<span>'.$customer->company_name.'</span><br/>';
		$html .= '<span>'.$customer->address.'</span><br/>';
		$html .= '<span>'.$customer->postcode . ' '.$customer->city.'</span><br/>';
		$html .= '<span>Telefon: '.$customer->phone.'</span><br/>';
		$html .= '<span>E-Mail: '.$customer->email.'</span><br/>';
		$html .= '<span>Bestellnummer: BST'.str_pad($id, 8, '0',STR_PAD_LEFT).'</span><br/>';
		$html .= '<span>Kundennummer</span>: CO'.str_pad($company->id, 8, '0',STR_PAD_LEFT).'</span><br/>';
		$html .= '<span>Datum: '.date('d.m.Y').'</span><br/>';
		$html .= '</div>';
		return $html;
	}
	function getResult($id) {
		$user 			= JFactory::getUser();
		$input 			= JFactory::getApplication()->input;
		$learningroomID = $input->get('id',0,'INT');
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		// Load the result of the quizz
		$query 		= $db->getQuery(true);
		$query->select(array('a.*,b.*,c.*'));
        $query->from($db->quoteName('#__jclassroom_orders','a'));
        $query->join('INNER', $db->quoteName('#__jclassroom_order_positions', 'b') . ' ON (' . $db->quoteName('b.orderID') . ' = ' . $db->quoteName('a.id') . ')');
        $query->join('INNER', $db->quoteName('#__jclassroom_hardware', 'c') . ' ON (' . $db->quoteName('b.hardwareID') . ' = ' . $db->quoteName('c.id') . ')');
		$query->where($db->quoteName('a.id').' = '.$db->quote($id));
		$query->order('b.id ASC');
		$db->setQuery($query);
		$positions 	= $db->loadObjectList();
		$html = '';
		$html .= '<table cellpadding="2" border="0" width="100%" style="font-size: 10px;text-align: left;">';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td colspan="3"><br/><h3>Hiermit bescheinigen wir die Lieferung Ihrer bestellten Hardware</h3><br/></td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<table cellpadding="2" border="1" width="100%" style="font-size: 10px;text-align: left;">';
		$html .= '<tbody>';
		$html .= '<tr style="background-color: #e1e1e1;">';
		$html .= '<td width="40">Pos.</td>';
		$html .= '<td width="322">Artikel</td>';
		$html .= '<td width="100" align="right">Menge</td>';
		$html .= '</tr>';
		if($positions):
			$i = 1;
			foreach($positions as $position):
				$html .= '<tr>';
				$html .= '<td>'.$i.'</td>';
				$html .= '<td>'.$position->title.'</td>';
				$html .= '<td align="right">'.$position->amount.'</td>';
				$html .= '</tr>';
				$i++;
			endforeach;
		endif;
	
		$html .= '</table>';
		return $html;
	}
}
class MYPDF extends TCPDF {

	public function Header() {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'logoCustomer');
		$result->load($load);
		$path = JPATH_SITE.'/'.$result->wert;
		$this->Image($path, 10, 10, 56, 18, 'PNG','' , '', true, 300, '', false, false, 0, false, false, false);
		$this->writeHTMLCell(90, 20, 110,9, '<h1 style="font-size: 38pt;">Lieferschein</h1>', 0, 0, 0, 0, 'R', true, true, 0, false, true, 30, 'M', true);
	}
	public function Footer() {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'systemName');
		$result->load($load);
		$now = JFactory::getDate();
		$date = new JDate($now);
		$now = $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$datumSTR = $now->format('d.m.Y');
		$datum = explode(' ',$now);
		$this->SetXY(10,-15);
		// Set font
		$this->SetFont('helvetica', '', 7);
		// Page number
		$this->Cell(0, 10, $mandant, 0, false, 'L', 0, '', 0, false, 'T', 'M');
		$this->MultiCell(50, 5, $result->wert.' (c) '.date('Y'), '0', 'L', 0, 1, '10', '285', true);
		$this->MultiCell(50, 5, 'Druck am: '.$datumSTR.' '.$datum[1], '', 'L', 0, 1, '10', '288', true);
		$this->MultiCell(50, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), '0', 'R', 0, 1, '160', '288', true);
	}
}
