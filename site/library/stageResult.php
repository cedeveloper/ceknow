<?php

/** ceLearning 29.06.2020
 * HelperClass TESTS
 * Used to load all quizzes, which are available for
 *  the currently logged in user. SuperUser has full access on all quizzes.
 *  Trainers has only acces on their own quizzes or on approved quizzes.
 *    Developer: Torsten Scheel
 */

defined('_JEXEC') or die();

class StageresultHelper {

   function getResult($rID, $fromLR, $userID, $completeEvaluation, $classroomID, $publishedQuizzID, $unitID, $pdf) {
      
      $user       = JFactory::getUser();
      $db      = JFactory::getDbo();
      // Load the result of the quizz, if exist
      $query      = $db->getQuery(true);
      $query->select(array('
         a.*,
         a.id as theResultID,
         b.id as resultPositionID,
         b.questionID
         '));
      $query->from($db->quoteName('#__jclassroom_theresults','a'));
      $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
      $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('b.questionID') . ' = ' . $db->quoteName('c.id') . ')');
      if($fromLR == 0 && $completeEvaluation == 1):
         $query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
      else:
         $query->where($db->quoteName('a.id').' = '.$db->quote($rID));
      endif;
      $query->group('b.questionID');
      $query->order('c.ordering');
      $db->setQuery($query);
      $questions  = $db->loadObjectList();
      //Load the quizz
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quizz      = JTable::getInstance('Unit','JclassroomTable',array());
      $quizz->load($unitID);
      $html       = false;
      if($quizz->type == 0):
         $html .= '<p>Auswertung für Quizz</p>';
         $html .= $this->getQuizzResult($unitID, $rID, $fromLR, $completeEvaluation, $pdf, $classroomID, $publishedQuizzID);
      endif;
      if($quizz->type == 1):
         $html .= '<p>Auswertung für Audit</p>';
         if($questions):
            $html .= $this->getAuditResult($unitID, $rID);
         endif;
      endif;

      
      return $html;
   }
   function getQuizzResult($unitID, $rID, $fromLR, $completeEvaluation, $pdf, $classroomID, $publishedQuizzID) {
      // Size of question
      $h1         = '16px';
      // Seize of all other, except question
      $p          = '12px';
      $session    = JFactory::getSession();
      $cart       = $session->get('quizzes');
      if(!$cart):
         $cart = $this->loadCart($rID);
      endif;
      // Load the quizz
      $calculate = 1;
      if($unitID):
         JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
         $unit      = JTable::getInstance('Unit','JclassroomTable',array());
         $unit->load($unitID);
         $calculate  = $unit->calculate;
      endif;
      $counter    = 1;
      $points  = 0;
      $maxPoints  = 0;
      $html = '';
      if($calculate == 2):
         $f1   = '30%';
         $w1   = '30%';
         $w2   = '30%';
         $w3   = '10%';
         $g1   = '60%';
         $cols = 4;
      else:
         if($completeEvaluation == 0):
            $f1   = '30%';
            $w1   = '35%';
            $w2   = '35%';
            $w3   = '0%';
            $g1   = '70%';
            $cols = 3;
         endif;
         if($completeEvaluation == 1):
            $f1   = '30%';
            $w1   = '70%';
            $w2   = '0%';
            $w3   = '0%';
            $g1   = '70%';
            $cols = 3;
         endif;
      endif;
      if($pdf == 0):
         $html .= '<table class="table table-striped" width="100%">';
         $html .= '<thead>';
         $html .= '<tr>';
            if($completeEvaluation == 0):
               $html .= '<th class="p-0" width="'.$f1.'">Frage</th>';
               $html .= '<th class="p-0" width="'.$g1.'">';
               $html .= '<table width="100%">';
               $html .= '<tr class="bg-white">';
               $html .= '<th class="p-0" style="border: none;" width="'.$w1.'" class="text-center">Antwortmöglichkeiten</th>';
               $html .= '<th class="p-0" style="border: none;" width="'.$w2.'" class="text-center">Ihre Antworten</th>';
               $html .= '</table>';
               $html .= '</th>';
            endif;
            if($completeEvaluation == 1):
               $html .= '<th class="p-0" width="'.$f1.'">Frage</th>';
               $html .= '<th class="p-0" width="'.$g1.'">';
               $html .= '<table width="100%">';
               $html .= '<tr class="bg-white">';
               $html .= '<th class="p-0" style="border: none;" width="'.$w1.'" class="text-center">Antworten</th>';
               $html .= '</table>';
               $html .= '</th>';
            endif;
            if($completeEvaluation == 0 && $calculate == 2):
               $html .= '<th class="p-0" width="'.$w3.'" class="text-center">Punkte</th>';
            endif;
         $html .= '</tr>';
         $html .= '</thead>';
        
         $html .= '</th>';
      endif;
      if($pdf == 1):
         $thStyle = 'style="background-color: lightsteelblue;font-size: 12px;color: black;"';
         if($completeEvaluation == 0):
            if($calculate < 2):     
               $html .= '<table width="550px" border="1" cellpadding="2">';
               $html .= '<thead>';
               $html .= '<tr>';
               $html .= '<th '.$thStyle.' width="180px">Frage</th>';
               $html .= '<th '.$thStyle.' width="180px">Antwortmöglichkeiten</th>';
               $html .= '<th '.$thStyle.' width="180px">Ihre Antworten</th>';
               $f1   = '180px';
               $w1   = '180px';
               $w2   = '180px';
               $w3   = '';
            endif;
            if($calculate == 2):
               $html .= '<table width="500px" border="1" cellpadding="2">';
               $html .= '<thead>';
               $html .= '<tr>';
               $html .= '<th '.$thStyle.' width="150px">Frage</th>';
               $html .= '<th '.$thStyle.' width="150px">Antwortmöglichkeiten</th>';
               $html .= '<th '.$thStyle.' width="150px">Ihre Antworten</th>';
               $html .= '<th '.$thStyle.' width="50px">Punkte</th>';
               $f1   = '150px';
               $w1   = '150px';
               $w2   = '150px';
               $w3   = '50px';
            endif;
         endif;
         if($completeEvaluation == 1):
            $html .= '<table width="530px" border="1" cellpadding="2">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th '.$thStyle.' width="160px">Frage'.$quizzID.'</th>';
            $html .= '<th '.$thStyle.' width="380px">Antworten</th>';
            $f1   = '160px';
            $w1   = '380px';
            $w2   = '0px';
            $w3   = '0px';
         endif;
      endif;
      $html .= '</tr>';
      $html .= '</thead>';
      //Load the quizzPositions 
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
      $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
      $query->where($db->quoteName('a.quizzID').' = '.$db->quote($unitID));
      $query->where($db->quoteName('a.type').' <> '.$db->quote(1));
      $query->order('a.ordering asc');
      $db->setQuery($query);
      $quizzPositions   = $db->loadObjectList();
      if($quizzPositions):
         $html .= '<tbody>';
         foreach($quizzPositions as $quizzPosition):
            // If result is not for students from a learningroom
            if($fromLR == 0):
               if($pdf == 0):
                  $h1 = '18px';
                  $lh1 = '18px';
               endif;
               if($pdf == 1):
                  $h1 = '8pt';
                  $lh1 = '10px';
               endif;
               $colspan    = 1;
               $theBG      = '';
               if($quizzPosition->type == 14):
                  $theBG = 'style="background-color: lightskyblue;"';
                  if($quizz->calculate == 2):
                     $colspan = 4;
                  else:
                     $colspan = 2;
                  endif;
               endif;
               $content = '';
               switch($quizzPosition->type):
                  case '5':
                     $ct = html_entity_decode($quizzPosition->content);
                     $content .= '<br/><span style="font-size: 14px;font-weight: normal;line-height: 14px;">'.strip_tags($ct).'</span>';
                     break;
               endswitch;
               if($quizzPosition->type != 4 && $quizzPosition->type != 10 && $quizzPosition->type != 11):
                  $maxPoints += $quizzPosition->points;
               endif;
               // Questiontitle for all questiontypes
               $html .= '<tr nobr="true">';
               $html .= '<td width="'.$f1.'"' .$theBG.' colspan="'.$colspan.'"><h5 style="font-size: '.$h1.';line-height: '.$lh1.';color: black;"><b>'.$counter.'. '.$quizzPosition->title.$content.'</b></h5></td>';
               if($completeEvaluation == 0 || ($completeEvaluation == 1 && ($quizzPosition->chart == '' || $quizzPosition->chart == '0'))):
                  switch($quizzPosition->type):
                     case '3':
                        // WWM-Fragen
                        $return = $this->loadType3($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        if($completeEvaluation == 0):
                           $html .= $return['html'];
                           $points += $return['points'];
                        endif;
                        if($completeEvaluation == 1):
                           if($return):
                              $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                              $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                              foreach($return['labels'] as $key => $value):
                                 $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                              endforeach;
                              $html .= '</td>';
                           endif;
                        endif;
                        break;
                     case '4':
                        // Smiley-Fragen
                        $return = $this->loadType4($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        if($completeEvaluation == 0):
                           $html .= $return['html'];
                        endif;
                        if($completeEvaluation == 1):
                           if($return):
                              $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                              $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                              foreach($return['labels'] as $key => $value):
                                 $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                              endforeach;
                              $html .= '</td>';
                           endif;
                        endif;
                        break;
                     case '5':
                        // Ja/Nein-Fragen
                        $return = $this->loadType5($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        if($completeEvaluation == 0):
                           $html .= $return['html'];
                           $points += $return['points'];
                        endif;
                        if($completeEvaluation == 1):
                           if($return):
                              $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                              $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                              foreach($return['labels'] as $key => $value):
                                 $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                              endforeach;
                              $html .= '</td>';
                           endif;
                        endif;
                        break;
                     case '7':
                        // Textfeld-Fragen
                        $return = $this->loadType7($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        if($completeEvaluation == 0):
                                 $html .= $return['html'];
                              endif;
                              if($completeEvaluation == 1):
                                 if($return):
                                    $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                    $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                    $html .= '<p style="line-height: 12pt;">'.$return['indiAnswers'].'</p>';
                                    $html .= '</td>';
                                 endif;
                              endif;
                        break;
                     case '10':
                        // Textfeld-Fragen
                        $return = $this->loadType10($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID);
                        if($completeEvaluation == 0):
                                 $html .= $return['html'];
                              endif;
                              if($completeEvaluation == 1):
                                 if($return):
                                    $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                    $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                    $html .= '<p style="line-height: 12pt;">'.$return['indiAnswers'].'</p>';
                                    $html .= '</td>';
                                 endif;
                              endif;
                        break;
                     case '11':
                        // Textarea-Fragen
                        $return = $this->loadType11($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID);
                        if($completeEvaluation == 0):
                                 $html .= $return['html'];
                              endif;
                              if($completeEvaluation == 1):
                                 if($return):
                                    $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                    $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                    $html .= '<p style="line-height: 12pt;">'.$return['indiAnswers'].'</p>';
                                    $html .= '</td>';
                                 endif;
                              endif;
                        break;
                     case '12':
                        // Auswahlliste-Fragen
                        $return = $this->loadType12($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        if($completeEvaluation == 0):
                                 $html .= $return['html'];
                                 $points += $return['points'];
                              endif;
                              if($completeEvaluation == 1):
                                 if($return):
                                    $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                    $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                    foreach($return['labels'] as $key => $value):
                                       $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                    endforeach;
                                    $html .= '</td>';
                                 endif;
                              endif;
                        break;
                     case '13':
                        // Checkbox-Fragen
                        $return = $this->loadType13($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        if($completeEvaluation == 0):
                                 $html .= $return['html'];
                                 $points += $return['points'];
                              endif;
                              if($completeEvaluation == 1):
                                 if($return):
                                    $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                    $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                    foreach($return['labels'] as $key => $value):
                                       $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                    endforeach;
                                    $html .= '</td>';
                                 endif;
                              endif;
                        break;
                  endswitch;
               endif;
               if($completeEvaluation == 1 && $quizzPosition->chart != '' && $quizzPosition->chart != '0'):
                     if(($quizzPosition->type != 10 && $quizzPosition->type != 11 && $quizzPosition->type != 14)):
                        if($pdf == 0):
                           $html .= '<td width="'.$w1.'"><div id="theModalChartDiv' .$quizzPosition->id.'"><canvas id="theModalChart'.$quizzPosition->id.'"></canvas></div></td>';
                        endif;
                        if($pdf == 1):
                            $path       = '/images/evaluates/eva'.$publishedQuizzID.'/theModalChart'.$quizzPosition->id.'.png';
                           $html .= '<td width="'.$w1.'"><br/><br/><img src="'.$path.'" /><br/></td>';
                        endif;
                     endif;
                  endif;
               $html .= '</tr>';
               $counter++;
            endif;
            // If result is for students from a learningroom
            if($fromLR == 1):
               if($pdf == 0):

                  $colspan    = 1;
                  $theBG      = '';
                  if($quizzPosition->type == 14):
                     $theBG = 'style="background-color: lightskyblue;"';
                     if($quizz->calculate == 2):
                        $colspan = 4;
                     else:
                        $colspan = 3;
                     endif;
                  endif;
                  $content = '';
                  switch($quizzPosition->type):
                     case '5':
                        $ct = html_entity_decode($quizzPosition->content);
                        $content .= '<br/><span style="font-size: 14px;font-weight: normal;line-height: 14px;">'.strip_tags($ct).'</span>';
                        break;
                  endswitch;
                  if($quizzPosition->type != 4 && $quizzPosition->type != 10 && $quizzPosition->type != 11):
                     $maxPoints += $quizzPosition->points;
                  endif;
                  // Questiontitle for all questiontypes
                  $html .= '<tr>';
                  $html .= '<td width="'.$f1.'"' .$theBG.'><h5 style="font-size:'.$h1.';"><b>'.$counter.'. '.$quizzPosition->title.$content.'</b></h5></td>';
                  switch($quizzPosition->type):
                     case '3':
                        // WWM-Fragen
                        $return = $this->loadType3($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '4':
                        // Smiley-Fragen
                        $return = $this->loadType4($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        $html .= $return['html'];
                        break;
                     case '5':
                        // Ja/Nein-Fragen
                        $return = $this->loadType5($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '10':
                        // Textfeld-Fragen
                        $return = $this->loadType10($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID);
                        $html .= $return['html'];
                        break;
                     case '11':
                        // Textarea-Fragen
                        $return = $this->loadType11($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID);
                        $html .= $return['html'];
                        break;
                     case '12':
                        // Auswahlliste-Fragen
                        $return = $this->loadType12($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '13':
                        // Checkbox-Fragen
                        $return = $this->loadType13($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                  endswitch;
                  $html .= '</tr>';
                  $counter++;
               endif;
               if($pdf == 1):
                  $chart = $quizzPosition->chart;
                  if($quizzPosition->type != 4 && $quizzPosition->type != 10 && $quizzPosition->type != 11):
                     $maxPoints += $quizzPosition->points;
                  endif;
                  $html .= '<tr nobr="true">';
                  $html .= '<td width="'.$f1.'" style="font-size: 8px;line-height: 10px;color: black;"><b>'.$counter.'. '.$quizzPosition->title.$content.'</b></td>';
                  if($completeEvaluation == 0 || ($completeEvaluation == 1 && ($quizzPosition->chart == '' || $quizzPosition->chart == '0'))):
                     switch($quizzPosition->type):
                        case '3':
                           // WWM-Fragen
                           $return = $this->loadType3($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                           if($completeEvaluation == 0):
                              $html .= $return['html'];
                              $points += $return['points'];
                           endif;
                           if($completeEvaluation == 1):
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                           endif;
                           break;
                        case '4':
                           // Smiley-Fragen
                           $return = $this->loadType4($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                           if($completeEvaluation == 0):
                              $html .= $return['html'];
                           endif;
                           if($completeEvaluation == 1):
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                           endif;
                           break;
                        case '5':
                           // Ja/Nein-Fragen
                           $return = $this->loadType5($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                           if($completeEvaluation == 0):
                              $html .= $return['html'];
                              $points += $return['points'];
                           endif;
                           if($completeEvaluation == 1):
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                           endif;
                           break;
                        case '10':
                           // Textfeld-Fragen
                           $return = $this->loadType10($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID);
                           if($completeEvaluation == 0):
                              $html .= $return['html'];
                           endif;
                           if($completeEvaluation == 1):
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 $html .= '<p style="line-height: 12pt;">'.$return['indiAnswers'].'</p>';
                                 $html .= '</td>';
                              endif;
                           endif;
                           break;
                        case '11':
                           // Textarea-Fragen
                           $return = $this->loadType11($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID);
                           if($completeEvaluation == 0):
                              $html .= $return['html'];
                           endif;
                           if($completeEvaluation == 1):
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 $html .= '<p style="line-height: 12pt;">'.$return['indiAnswers'].'</p>';
                                 $html .= '</td>';
                              endif;
                           endif;
                           break;
                        case '12':
                           // Auswahlliste-Fragen
                           $return = $this->loadType12($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID);
                           if($completeEvaluation == 0):
                              $html .= $return['html'];
                              $points += $return['points'];
                           endif;
                           if($completeEvaluation == 1):
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                           endif;
                           break;
                        case '13':
                           // Checkbox-Fragen
                           $return = $this->loadType13($rID, $quizzPosition->id, $cart, $quizzPosition->points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID);
                           if($completeEvaluation == 0):
                              $html .= $return['html'];
                              $points += $return['points'];
                           endif;
                           if($completeEvaluation == 1):
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                           endif;
                           break;
                     endswitch;
                  endif;
                  if($completeEvaluation == 1 && $quizzPosition->chart != '' && $quizzPosition->chart != '0'):
                     $path       = '/images/evaluates/LR'.$classroomID.'unit'.$unitID.'/theModalChart'.$quizzPosition->id.'.png';
                     if(($quizzPosition->type != 10 && $quizzPosition->type != 11 && $quizzPosition->type != 14) && $path):
                        $html .= '<td width="'.$w1.'"><br/><br/><img src="'.$path.'" /><br/></td>';
                     endif;
                  endif;

                  $html .= '</tr>';
                  $counter++;
               endif;
            endif;
         endforeach;
         $html .= '</tbody>';
      endif;
      if($completeEvaluation == 0 && $calculate == 2):
         if($pdf == 0):
            $html .= '<tfoot>';
            $html .= '<tr>';
            $html .= '<td><b>Erreichte Gesamtpunktzahl</b></td>';
            $html .= '<td colspan="1"></td>';
            $html .= '<td style="text-align: center;"><b><span style="color: forestgreen;">'.$points.'</span>/<span style="color: steelblue;">'.$maxPoints.'</span></b></td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td><b>Dies entspricht einem Score von</b></td>';
            $html .= '<td colspan="1"></td>';
            $score = 0;
            if($points && $maxPoints):
               $score = round($points * 100 / $maxPoints);
            endif;
            $html .= '<td style="text-align: center;"><b>'.$score.'%</b></td>';
            $html .= '</tr>';
            $html .= '</tfoot>';
         endif;
         if($pdf == 1):
            $html .= '<tfoot>';
            $html .= '<tr>';
            $html .= '<td style="font-size: 8px;line-height: 8px;color: black;"><b>Erreichte Gesamtpunktzahl</b></td>';
            $html .= '<td colspan="2"></td>';
            $html .= '<td style="text-align: center;"><b><span style="color: forestgreen;">'.$points.'</span>/<span style="color: steelblue;">'.$maxPoints.'</span></b></td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td style="font-size: 8px;line-height: 8px;color: black;"><b>Dies entspricht einem Score von</b></td>';
            $html .= '<td colspan="2"></td>';
            $score = 0;
            if($points && $maxPoints):
               $score = round(($points * 100 / $maxPoints),0);
            endif;
            $html .= '<td style="text-align: center;"><b>'.$score.'%</b></td>';
            $html .= '</tr>';
            $html .= '</tfoot>';
         endif;
      endif;
      $html .= '</table>';

      $session->clear('quizzes');
      $session->clear('answers');
      return $html;
   }
   function loadCart($resultID) {
      $user = JFactory::getUser();
      //Load the quizzPositions 
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('
         a.classroomID,
         a.quizzID,
         a.publishedQuizzID,
         b.content,
         b.exeptionText,
         b.exeptionPoints,
         b.answerID,
         c.id as questionID,
         c.type
      '));
      $query->from($db->quoteName('#__jclassroom_theresults','a'));
      $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
      $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('b.questionID') . ' = ' . $db->quoteName('c.id') . ')');
      $query->where($db->quoteName('a.id').' = '.$db->quote($resultID));
      $query->order('c.ordering asc');
      $db->setQuery($query);
      $positions   = $db->loadObjectList();
      if($positions):
         foreach($positions as $position):
            $cart[]  = array(
               'classroomID'        => $position->classroomID,
               'userID'             => $user->id, 
               'quizzID'            => $position->quizzID, 
               'publishedQuizzID'   => $position->publishedQuizzID,
               'questionID'         => $position->questionID,
               'type'               => $position->type,
               'content'            => array($position->answerID.'_'.$position->content),
               'exeptionText'       => $position->exeptionText,
               'exeptionPoints'     => $position->exeptionPoints
            );
         endforeach;
      endif;

      return $cart;
   }
   // for WWM-Fragen
   function loadType3($rID, $questionID, $cart, $points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID) {
      if($completeEvaluation == 0):
         if($pdf == 0):
            $tc = 'black';
            if($calculate == 2):
               $g1 = '60%';
               $tc = 'white';
            endif;
            $html = '';
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('a.*'));
            $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $db->setQuery($query);
            $answers = $db->loadObjectList();
            //Count correct answers
            $query   = $db->getQuery(true);
            $query->select(array('COUNT(correct)'));
            $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
            $db->setQuery($query);
            $correctSoll   = $db->loadResult();
            $correctIst    = 0;
            if($answers):
               $html .= '<td width="'.$g1.'" style="padding:5px;">';
               $html .= '<table style="width: 100%;">';
               foreach($answers as $answer):
                  if($calculate == 2):
                     if($answer->correct == 1):
                        $bgS = 'bg-success';
                     else:
                        $bgS = 'bg-danger';
                     endif;
                  else:
                     $bgS = '';
                  endif;
                  $html .= '<tr>';
                  $html .= '<td width="'.$w1.'" style="color: '.$tc.';padding: 3px;"><span class="d-flex p-1 d-block badge '.$bgS.'" style="justify-content: left;">'.$answer->title.'</span></td>';
                  $setCorrect = $answer->correct;
                  $theTD      = '<td width="'.$w2.'"></td>';
                  $theAnswer  = '';
                  foreach($cart as $part):
                     $eparts  = $part['content'];
                     if($eparts):
                        foreach($eparts as $epart):
                           $EPS = explode('_', $epart);
                           if($EPS[0] == $answer->id):
                              if($calculate == 2):
                                 if($answer->correct == 1):
                                    $bgS = 'bg-success';
                                    $correctIst++;
                                    $thePoints += $points;
                                 else: 
                                    $bgS = 'bg-danger';
                                 endif;
                              else:
                                 $bgS = '';
                              endif;
                              $theTD = '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block badge '.$bgS.'" style="color: '.$tc.';justify-content: left;">'.$answer->title.' '.$theID.'</span></td>';
                           endif;
                        endforeach;
                     endif;
                  endforeach;
                  $html .= $theTD;
               endforeach;
               $html .= '</tr>';
               $html .= '</table>';
               $html .= '</td>';
               if($correctSoll > $correctIst):
                  $thePoints = ($correctIst * $points / $correctSoll);
               endif;
               if($correctSoll == $correctIst):
                  $thePoints = $points;
               endif;
               if($calculate == 2):
                  $thePoints = round($thePoints);
                  $html .= '<td width="'.$w3.'" style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
               endif;
            endif;
         endif;
         if($pdf == 1):
            $tc = 'black';
            if($calculate == 2):
               $tc = 'white';
            endif;
            $html = '';
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);

            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('a.*'));
            $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $db->setQuery($query);
            $answers = $db->loadObjectList();
            //Count correct answers
            $query   = $db->getQuery(true);
            $query->select(array('COUNT(correct)'));
            $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
            $db->setQuery($query);
            $correctSoll   = $db->loadResult();
            $correctIst    = 0;
            $query   = $db->getQuery(true);
            $query->select(array('
               a.answerID,
               a.content
            '));
            $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
            $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $db->setQuery($query);
            $quizzPositions   = $db->loadObjectList();
            $thePoints = 0;
            if($answers):
               $html .= '<td width="'.$w1.'" style="width:'.$w1.';" style="color: '.$tc.';">';
               $html .= '<table border="1">';
               foreach($answers as $answer):
                  $bgs = '';
                  // If quizz should be calculate
                  if($calculate == 2):
                     $tc = 'white';
                     if($answer->correct == 1):
                        $bgs = 'background-color: forestgreen';
                     else:
                        $bgs = 'background-color: red';
                     endif;
                  endif;
                  $html .= '<tr>';
                  $html .= '<td width="'.$w1.'" style="'.$bgs.'"><span style="text-align: left; font-size: 8pt; line-height: 8pt;">'.strip_tags($answer->title).'</span></td>';
                  $html .= '</tr>';
               endforeach;
               $html .= '</table>';
               $html .= '</td>';
            endif;
            //if cart is not empty
            if($quizzPositions):
               $html .= '<td width="'.$w2.'" style="width:'.$w2.'" style="'.$tc.'">';
               $html .= '<table style="width: 100%;" border="1">';
               foreach($answers as $answer):
                  $bgI = '';
                  $theTD      = '<td width="'.$w2.'" style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';;
                  foreach($quizzPositions as $part):
                     if($part->answerID == $answer->id):
                        if($answer->correct == 1):
                           if($calculate == 2):
                              $bgI = 'background-color: forestgreen';
                           endif;
                           $correctIst++;
                           $thePoints += $quest->points;
                        else: 
                           if($calculate == 2):
                              $bgI = 'background-color: red';
                           endif;
                        endif;
                        $theTD = '<td width="'.$w2.'" style="color: '.$tc.';'.$bgI.'"><span style="text-align: left; font-size: 8pt; line-height: 8pt;">'.strip_tags($answer->title).'</span></td>';
                     endif;
                  endforeach;
                  $html .= '<tr>';
                  $html .= $theTD;
                  $html .= '</tr>';
               endforeach;
               $html .= '</table>';
               $html .= '</td>';
               if($correctSoll > $correctIst):
                  $thePoints = floor(($correctIst * $quest->points / $correctSoll));
               endif;
               if($correctSoll == $correctIst):
                  $thePoints = $quest->points;
               endif;
               if($calculate == 2):
                  $html .= '<td width="'.$w3.'" style="text-align: center;vertical-align: center;">'.$thePoints.'/'.$quest->points;
                  $html .= '</td>';
               endif;
            endif;
         endif;
         $return = array('html' => $html, 'points' => $thePoints);
         return $return;
      endif;
      if($completeEvaluation == 1):
         $cCount = 0;
         $db      = JFactory::getDbo();
         $query   = $db->getQuery(true);
         $query->select(array('
            a.title,a.id
         '));
         $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
         $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
         $db->setQuery($query);
         $answersSOLL = $db->loadObjectList();
         foreach($answersSOLL as $answer):
            //echo $question->questionID.' '.$answer->id.' '.$input->get('classroomID',0,'INT').' '.$input->get('unitID',0,'INT').'<br/>';
            $titles     = strip_tags($answer->title);
            $titles     = htmlspecialchars_decode($titles);
            $labels[]   = $titles;

            $query = $db->getQuery(true);
            $query->select(array('
               a.id
            '));
            $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
            $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
            if($classroomID && $classroomID != 0):
               $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
            endif;
            $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
            $query->where($db->quotename('a.answerID').' = '.$db->quote($answer->id));
            if($classroomID && $classroomID != 0):
               $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
            endif;
            //$query->where($db->quotename('b.unitID').' = '.$db->quote($quizzID));
            $query->order('a.created DESC');
            $db->setQuery($query);
            $db->execute();
            $numRows    = $db->getNumRows();
            $answersIS  = $db->loadObject();
            $datas[]    = $numRows;
            $cCount     += $numRows;
         endforeach;
         $theQuestion = array(
            'questionID'=> $question->questionID,
            'question'  => $question->title,
            'content'   => $question->content,
            'chart'     => $question->chart,
            'labels'    => $labels,
            'datas'     => $datas,
            'indiAnswers'  => $indiAnswers,
            'cCount'    => $cCount,
            'type'      => $type
         );
         return $theQuestion;
      endif;
   }
   // for Smiley-Fragen
   function loadType4($rID, $questionID, $cart, $points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID) {
      if($completeEvaluation == 0):
         if($pdf == 0):
            $g1 = '70%';
            if($calculate == 2):
               $g1 = '60%';
            endif;
            $thePoints = 0;
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);
            $html = '';
            $db   = JFactory::getDbo();
            $html .= '<td width="'.$g1.'" style="padding:5px;">';
            $html .= '<table style="width: 100%;">';
            $html .= '<tr>';
            $html .= '<td width="'.$w1.'" style="padding: 3px;"><span class="w-100 d-flex p-1 d-block text-white badge" style="background-color: forestgreen;justify-content: center;">Sehr gut</span></td>';
            foreach($cart as $part):
               if($part['questionID'] == $questionID):
                  $content = explode('_', $part['content'][0]);
                  if($content[0] == '5'):
                     $AT = '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: forestgreen;justify-content: center;">Sehr gut</span></td>';
                  else:
                     $AT = '<td width="'.$w2.'" style="height: 27px;"></td>';
                  endif;
               endif;
            endforeach;
            $html .= $AT;
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td width="'.$w1.'" style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: yellowgreen;justify-content: center;">Gut</span></td>';
            foreach($cart as $part):
               if($part['questionID'] == $questionID):
                  $content = explode('_', $part['content'][0]);
                  if($content[0] == '4'):
                     $AT = '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: yellowgreen;justify-content: center;">Gut</span></td>';
                  else:
                     $AT = '<td width="'.$w2.'" style="height: 27px;"></td>';
                  endif;
               endif;
            endforeach;
            $html .= $AT;
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td width="'.$w1.'" style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: gold;justify-content: center;">Nicht schlecht</span></td>';
            foreach($cart as $part):
               if($part['questionID'] == $questionID):
                  $content = explode('_', $part['content'][0]);
                  if($content[0] == '3'):
                     $AT = '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: gold;justify-content: center;">Nicht schlecht</span></td>';
                  else:
                     $AT = '<td width="'.$w2.'" style="height: 27px;"></td>';
                  endif;
               endif;
            endforeach;
            $html .= $AT;
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td width="'.$w1.'" style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: red; justify-content: center;">Schlecht</span></td>';
            foreach($cart as $part):
               if($part['questionID'] == $questionID):
                  $content = explode('_', $part['content'][0]);
                  if($content[0] == '2'):
                     $AT = '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: red;justify-content: center;">Schlecht</span></td>';
                  else:
                     $AT = '<td width="'.$w2.'" style="height: 27px;"></td>';
                  endif;
               endif;
            endforeach;
            $html .= $AT;
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td width="'.$w1.'" style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: firebrick;justify-content: center;">Sehr schlecht</span></td>';
            foreach($cart as $part):
               if($part['questionID'] == $questionID):
                  $content = explode('_', $part['content'][0]);
                  if($content[0] == '1'):
                     $AT = '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge" style="background-color: firebrick;justify-content: center;">Sehr schlecht</span></td>';
                  else:
                     $AT = '<td width="'.$w2.'" style="height: 27px;"></td>';
                  endif;
               endif;
            endforeach;
            $html .= $AT;
            $html .= '</tr>';
            $html .= '</table>';
            $html .= '</td>';
            if($calculate == 2):
               $html .= '<td width="'.$w3.'" style="text-align: center; font-weight: bold;"><span style="color:forestgreen"></span><span style="color: steelblue;"></span></td>';
            endif;
         endif;
         if($pdf == 1):
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);
            $html = '';
            $db   = JFactory::getDbo();
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('
               a.answerID
            '));
            $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
            $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->group('a.content');
            $db->setQuery($query);
            $quizzPositions   = $db->loadObject();
            $html .= '<td width="'.$w1.'">';
            $html .= '<table style="width: 100%;" border="1">';
            for($i = 5;$i >= 1;$i--):
               switch($i):
                  case 5:
                     $bg = 'forestgreen';
                     $tt = 'Sehr gut';
                     break;
                  case 4: 
                     $bg = 'yellowgreen';
                     $tt = 'Gut';
                     break;
                  case 3:
                     $bg = 'gold';
                     $tt = 'Nicht schlecht';
                     break;
                  case 2:
                     $bg = 'red';
                     $tt = 'Schlecht';
                     break;
                  case 1: 
                     $bg = 'firebrick';
                     $tt = 'Sehr schlecht';
                     break;
               endswitch;
               $html .= '<tr>';
               $html .= '<td style="color: black;background-color: '.$bg.';font-size: 8pt: line-height: 40px;text-align: left;"><span style="line-height: 17px;">'.$tt.'</span></td>';
               $html .= '</tr>';
            endfor;
            $html .= '</table>';
            $html .= '</td>';
            $html .= '<td width="'.$w2.'">';
            $html .= '<table style="width: 100%;" border="1">';
            
            for($i = 5;$i >= 1;$i--):
               $show = 0;
               switch($i):
                  case 5:
                     $bg = 'forestgreen';
                     $tt = 'Sehr gut';
                     if($quizzPositions->answerID == 5):
                        $show = 1;
                     endif;
                     break;
                  case 4: 
                     $bg = 'yellowgreen';
                     $tt = 'Gut';
                     if($quizzPositions->answerID == 4):
                        $show = 1;
                     endif;
                     break;
                  case 3:
                     $bg = 'gold';
                     $tt = 'Nicht schlecht';
                     if($quizzPositions->answerID == 3):
                        $show = 1;
                     endif;
                     break;
                  case 2:
                     $bg = 'red';
                     $tt = 'Schlecht';
                     if($quizzPositions->answerID == 2):
                        $show = 1;
                     endif;
                     break;
                  case 1: 
                     $bg = 'firebrick';
                     $tt = 'Sehr schlecht';
                     if($quizzPositions->answerID == 1):
                        $show = 1;
                     endif;
                     break;
               endswitch;
               if($show == 1):
                  $html .= '<tr>';
                  $html .= '<td style="color: black;background-color: '.$bg.';font-size: 8pt: line-height: 17pt;text-align: left;"><span style="line-height: 17px;">'.$tt.'</span></td>';
                  $html .= '</tr>';
               else:
                  $html .= '<tr>';
                  //$html .= '<td style="background-color: red;font-size: 8pt;line-height: 17pt;">'.$tt.'</td>';
                  $html .= '<td style="background-color: white;font-size: 8pt: line-height: 17pt;text-align: left;"><span style="line-height: 17px;"></span></td>';
                  $html .= '</tr>';
               endif;
               
            endfor;
            $html .= '</table>';
            $html .= '</td>';
            if($calculate == 2):
               $html .= '<td style="width: '.$w3.';"></td>';
            endif;
         endif;
         $return = array('html' => $html, 'points' => 0);
         return $return;
      endif;
      if($completeEvaluation == 1):
         $answersIS = 0;
         $labels  = array('Sehr schlecht', 'Schlecht','Nicht schlecht','Gut','Sehr gut');
         $cCount = 0;
         $db      = JFactory::getDbo();
         for($i = 1;$i <= 5;$i++) {
            $query = $db->getQuery(true);
            $query->select(array('
               count(b.answerID) as data
            '));
            $query->from($db->quoteName('#__jclassroom_theresults','a'));
            $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
            if($classroomID && $classroomID != 0):
               $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
            endif;
            $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
            $query->where($db->quotename('b.answerID').' = '.$db->quote($i));
            if($classroomID && $classroomID != 0):
               $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
            endif;
            $db->setQuery($query);
            $answersIS = $db->loadResult();
            if($answersIS):
               $datas[]    = $answersIS;
               $cCount     += $answersIS;
            else:
               $datas[]    = 0;
            endif;
         }
         $theQuestion = array(
            'questionID'=> $question->questionID,
            'question'  => $question->title,
            'content'   => $question->content,
            'chart'     => $question->chart,
            'labels'    => $labels,
            'datas'     => $datas,
            'indiAnswers'  => $indiAnswers,
            'cCount'    => $cCount,
            'type'      => $type
         );
         return $theQuestion;
      endif;
   }
   // for Ja/Nein-Fragen
   function loadType5($rID, $questionID, $cart, $points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID) {
      if($completeEvaluation == 0):
         if($pdf == 0):
            if($calculate == 2):
               $g1 = '60%';
            endif;
            $thePoints = 0;
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);
            if($calculate < 2):
               $bgsJ = '';
               $bgsN = '';
               $tc   = 'black'; 
            else:
               $tc   = 'white';
               if($quest->correctAnswers == 0):
                  $bgsJ = 'bg-success';
                  $bgsN = 'bg-danger';
               else:
                  $bgsJ = 'bg-danger';
                  $bgsN = 'bg-success';
               endif;
            endif;
            $html = '';
            $db   = JFactory::getDbo();
            $html .= '<td width="'.$g1.'" style="padding:5px;">';
            $html .= '<table style="width: 100%;">';
            $html .= '<tr>';
            $html .= '<td width="'.$w1.'" style="color: '.$tc.';padding: 3px;"><span class="d-flex p-1 d-block badge '.$bgsJ.'" style="color: '.$tc.';justify-content: left;">Ja</span></td>';
            foreach($cart as $part):
               if($part['questionID'] == $questionID):
                  $content = explode('_', $part['content'][0]);
                  if($content[1] == 'answerJ'):
                     $html .= '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block w-100 badge '.$bgsJ.'" style="color: '.$tc.';justify-content: left;">Ja</span></td>';
                     if($quest->correctAnswers == 0):
                        $thePoints = $points;
                     endif;
                  else:
                     $html .= '<td></td>';
                  endif;
               endif;
            endforeach;
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td width="'.$w1.'" style="padding: 3px;"><span class="d-flex p-1 d-block badge '.$bgsN.'" style="color: '.$tc.';justify-content: left;">Nein</span></td>';
            foreach($cart as $part):
               if($part['questionID'] == $questionID):
                  $content = explode('_', $part['content'][0]);
                  if($content[1] == 'answerN'):
                     $html .= '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block  w-100 badge '.$bgsN.'" style="color: '.$tc.';justify-content: left;">Nein</span></td>';
                     if($quest->correctAnswers == 1):
                        $thePoints = $points;
                     endif;
                  else:
                     $html .= '<td></td>';
                  endif;
               endif;
            endforeach;
            $html .= '</tr>';
            $html .= '</table>';
            $html .= '</td>';
            if($correctSoll > $correctIst):
               $thePoints = ($correctIst * $points / $correctSoll);
            endif;
            if($calculate == 2):
               $html .= '<td width="'.$w3.'" style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
            endif;
         endif;
         if($pdf == 1):
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('
               a.content
            '));
            $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
            $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->group('a.content');
            $db->setQuery($query);
            $quizzPositions   = $db->loadObjectList();
            $thePoints = 0;
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);
            $bgsJ = '';
            $bgsN = '';
            // If quizz should be calculate
            if($calculate == 2):
               $tc   = 'white';
               if($quest->correctAnswers == 0):
                  $bgsJ = 'background-color: forestgreen';
                  $bgsN = 'background-color: red';
               else:
                  $bgsJ = 'background-color: red';
                  $bgsN = 'background-color: forestgreen';
               endif;
            endif;
            $html = '';
            $html .= '<td width="'.$w1.'" style="color: '.$tc.';">';
            $html .= '<table style="width: 100%;" border="1">';
            $html .= '<tr>';
            $html .= '<td style="'.$bgsJ.';"><span class="" style="text-align: left;font-size: 8pt; line-height: 8pt;">Ja</span></td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td style="'.$bgsN.';"><span class="" style="text-align: left;font-size: 8pt; line-height: 8pt;">Nein</span></td>';
            $html .= '</tr>';
            $html .= '</table>';
            $html .= '</td>';
            $thePoints = 0;
            //if cart is not empty
            if($quizzPositions):
               $html .= '<td width="'.$w2.'" style="color: '.$tc.';">';
               $html .= '<table style="width: 100%;" border="1">';
               foreach($quizzPositions as $part):
                  $bgiJ = '';
                  $bgiN = '';
                  $ATJ = '<td style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';
                  $ATN = '<td style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';
                  if($part->content == 'answerJ'):
                     if($quest->correctAnswers == 0):
                        if($calculate == 2):
                           $bgiJ = 'background-color: forestgreen';
                           $bgiN = 'background-color: red';
                        endif;
                        $thePoints = $quest->points;
                     endif;
                     if($quest->correctAnswers == 1):
                        if($calculate == 2):
                           $bgiJ = 'background-color: red';
                           $bgiN = 'background-color: forestgreen';
                        endif;
                        $thePoints = 0;
                     endif;
                     $ATJ = '<td style="'.$bgiJ.';"><span style="text-align: left;font-size: 8pt;line-height: 8pt;">Ja</span></td>';

                  endif;
                  if($part->content == 'answerN'):
                     if($quest->correctAnswers == 0):
                        if($calculate == 2):
                           $bgiJ = 'background-color: forestgreen';
                           $bgiN = 'background-color: red';
                        endif;
                        $thePoints = 0;
                     endif;
                     if($quest->correctAnswers == 1):
                        if($calculate == 2):
                           $bgiJ = 'background-color: red';
                           $bgiN = 'background-color: forestgreen';
                        endif;
                        $thePoints = $quest->points;
                     endif;
                     $ATN = '<td style="'.$bgiN.';"><span style="text-align: left;font-size: 8pt;line-height: 8pt;">Nein</span></td>';
                  endif;
               endforeach;
               $html .= '<tr>';
               $html .= $ATJ;
               $html .= '</tr>';
               $html .= '<tr>';
               $html .= $ATN;
               $html .= '</tr>';
               $html .= '</table>';
               $html .= '</td>';
            endif;
            if($calculate == 2):
               $html .= '<td width="'.$w3.'" style="text-align: center;vertical-align: center;">'.$thePoints.'/'.$quest->points;
               $html .= '</td>';
            endif;
         endif;
         $return = array('html' => $html, 'points' => $thePoints);
         return $return;
      endif;
      if($completeEvaluation == 1):
         $cCount  = 0;
         $db      = JFactory::getDbo();
         $query   = $db->getQuery(true);
         $query->select(array('
         count(a.id) as cJ
         '));
         $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
         $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
         if($classroomID && $classroomID != 0):
            $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
         endif;
         $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
         $query->where($db->quotename('a.content').' = '.$db->quote('answerJ'));
         if($classroomID && $classroomID != 0):
            $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
         endif;
         $db->setQuery($query);
         $answersIS  = $db->loadObject();
         $datas[]    = $answersIS->cJ;
         $labels[]   = 'Ja';
         $cCount += $answersIS->cJ;

         $query = $db->getQuery(true);
         $query->select(array('
         count(a.id) as cN
         '));
         $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
         $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
         if($classroomID):
            $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
         endif;
         $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
         $query->where($db->quotename('a.content').' = '.$db->quote('answerN'));
         if($classroomID && $classroomID != 0):
            $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
         endif;
         $db->setQuery($query);
         $answersIS  = $db->loadObject();
         $datas[]    = $answersIS->cN;
         $labels[]   = 'Nein';
         $cCount += $answersIS->cN;
         $theQuestion = array(
            'questionID'=> $question->questionID,
            'question'  => $question->title,
            'content'   => $question->content,
            'chart'     => $question->chart,
            'labels'    => $labels,
            'datas'     => $datas,
            'indiAnswers'  => $indiAnswers,
            'cCount'    => $cCount,
            'type'      => $type
         );
         return $theQuestion;
      endif;
   }
   // for Text mit 1 Eingabe
   function loadType7($rID, $questionID, $cart, $points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID) {
      if($completeEvaluation == 0):
         if($pdf == 0):
            $thePoints = 0;
            if($calculate == 2):
               $g1 = '70%';
            else:
               $g1 = '60%';
            endif;
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('a.*'));
            $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $db->setQuery($query);
            $answer = $db->loadObject();

            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);
            $theText = html_entity_decode($quest->content);
            $html = '';
            $db   = JFactory::getDbo();
            //if cart is not empty
            if($cart):
               $html .= '<td width="'.$g1.'" style="padding:5px;">';
               $html .= '<table width="100%">';
               $html .= '<tr>';
               $html .= '<td width="'.$w1.'" style="padding:5px;">';
               foreach($cart as $part):
                  if($part['questionID'] == $questionID):
                     $content = explode('_',$part['content'][0]);
                     $theAlias = str_replace('{}', '<span style="font-weight: bold;font-size: 16px;color: forestgreen;">'.$answer->title.'</span>', $theText);
                     $html .= $theAlias;
                  endif;
               endforeach;
               $html .= '</td>';
               $html .= '<td width="'.$w2.'" style="padding:5px;">';
               foreach($cart as $part):
                  if($part['questionID'] == $questionID):
                     $content = explode('_',$part['content'][0]);

                     if($content[1] == $answer->title):
                        $thePoints = $points;
                     endif;
                     $theAlias = str_replace('{}', '<span style="font-weight: bold;font-size: 16px;color: forestgreen;">'.$content[1].'</span>', $theText);
                     $html .= $theAlias;
                  endif;
               endforeach;
               $html .= '</tr>';
               $html .= '</table>';
               $html .= '</td>';
               if($correctSoll > $correctIst):
                  $thePoints = ($correctIst * $points / $correctSoll);
               endif;
               if($calculate == 2):
                  $html .= '<td width="'.$w3.'" style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
               endif;
            endif;
         endif;
         if($pdf == 1):
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('
               a.content
            '));
            $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
            $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->group('a.content');
            $db->setQuery($query);
            $quizzPositions   = $db->loadObject();
            $html = '';
            if($quizzPositions):
               $html .= '<td colspan="2"><span style="color: black;font-size:8pt;line-height: 8pt; text-align: left;">'.$quizzPositions->content.'</span></td>';
            endif;
            if($calculate == 2):
               $html .= '<td style="width:'.$w3.';"></td>';
            endif;
         endif;
         $return = array('html' => $html, 'points' => 0);
         return $return;
      endif;
      if($completeEvaluation == 1):
         $answersIS = 0;
         $db      = JFactory::getDbo();
         $query = $db->getQuery(true);
         $query->select(array('
         group_concat(b.content SEPARATOR "<br/>") as indiAnswers,
         count(a.id) as cCount
         '));
         $query->from($db->quoteName('#__jclassroom_theresults','a'));
         $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
         $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
         if($classroomID && $classroomID != 0):
            $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
         endif;
         $query->where('b.content <> ""');
         $db->setQuery($query);
         $answersIS = $db->loadObject();
         if($answersIS):
            $indiAnswers   = $answersIS->indiAnswers;
         else:
            $indiAnswers   = '';
         endif;
         $cCount = $answersIS->cCount;
         $theQuestion = array(
            'questionID'=> $question->questionID,
            'question'  => $question->title,
            'content'   => $question->content,
            'chart'     => $question->chart,
            'labels'    => $labels,
            'datas'     => $datas,
            'indiAnswers'  => $indiAnswers,
            'cCount'    => $cCount,
            'type'      => $type
         );
         return $theQuestion;
      endif;
   }
   // for Textfeld-Fragen
   function loadType10($rID, $questionID, $cart, $points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID) {
      if($completeEvaluation == 0):
         if($pdf == 0):
            $thePoints = 0;
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);
            $html = '';
            $db   = JFactory::getDbo();
            //if cart is not empty
            if($cart):
               $html .= '<td style="width: '.$g1.';padding:5px;">';
               foreach($cart as $part):
                  if($part['questionID'] == $questionID):
                     $content = explode('_',$part['content'][0]);
                     if($content[1]):
                        $thePoints = $points;
                     endif;
                     $html .= '<span class="badge" style="font-size: 12px; line-height: 14px;">'.$content[1].'</span>';
                  endif;
               endforeach;
               $html .= '</td>';
               if($calculate == 2):
                  $html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen"></span><span style="color: steelblue;"></span></td>';
               endif;
            endif;
         endif;
         if($pdf == 1):
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('
               a.content
            '));
            $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
            $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->group('a.content');
            $db->setQuery($query);
            $quizzPositions   = $db->loadObject();
            $html = '';
            if($quizzPositions):
               $html .= '<td colspan="2"><span style="color: black;font-size:8pt;line-height: 8pt; text-align: left;">'.$quizzPositions->content.'</span></td>';
            endif;
            if($calculate == 2):
               $html .= '<td style="width:'.$w3.';"></td>';
            endif;
         endif;
         $return = array('html' => $html, 'points' => 0);
         return $return;
      endif;
      if($completeEvaluation == 1):
         $answersIS = 0;
         $db      = JFactory::getDbo();
         $query = $db->getQuery(true);
         $query->select(array('
         group_concat(b.content SEPARATOR "<br/>") as indiAnswers,
         count(a.id) as cCount
         '));
         $query->from($db->quoteName('#__jclassroom_theresults','a'));
         $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
         $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
         if($classroomID && $classroomID != 0):
            $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
         endif;
         $query->where('b.content <> ""');
         $db->setQuery($query);
         $answersIS = $db->loadObject();
         if($answersIS):
            $indiAnswers   = $answersIS->indiAnswers;
         else:
            $indiAnswers   = '';
         endif;
         $cCount = $answersIS->cCount;
         $theQuestion = array(
            'questionID'=> $question->questionID,
            'question'  => $question->title,
            'content'   => $question->content,
            'chart'     => $question->chart,
            'labels'    => $labels,
            'datas'     => $datas,
            'indiAnswers'  => $indiAnswers,
            'cCount'    => $cCount,
            'type'      => $type
         );
         return $theQuestion;
      endif;
   }
   // for Textarea-Fragen
   function loadType11($rID, $questionID, $cart, $points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID) {
      if($completeEvaluation == 0):
         if($pdf == 0):
            $thePoints = 0;
            $html = '';
            $db   = JFactory::getDbo();

            //if cart is not empty
            if($cart):
               $html .= '<td style="width:'.$g1.';padding:5px;">';
               foreach($cart as $part):
                  if($part['questionID'] == $questionID):
                     $content = explode('_', $part['content'][0]);
                     if($content[1] != ''):
                        $ATJ = $content[1];
                     else:
                        $ATJ = '';
                     endif;
                  endif;
               endforeach;
               $html .= '<span class="badge" style="font-size: 12px; line-height: 14px;">'.$ATJ.'</span>';
               $html .= '</td>';
               if($calculate == 2):
                  $html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen"></span><span style="color: steelblue;"></span></td>';
               endif;
            endif;
         endif;
         if($pdf == 1):
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('
               a.content
            '));
            $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
            $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->group('a.content');
            $db->setQuery($query);
            $quizzPositions   = $db->loadObject();
            $html = '';
            if($quizzPositions):
               $html .= '<td colspan="2"><span style="color: black;font-size:8pt;line-height: 8pt; text-align: left;">'.$quizzPositions->content.'</span></td>';
            endif;
            if($calculate == 2):
               $html .= '<td style="width:'.$w3.';"></td>';
         endif;
         endif;
         $return = array('html' => $html, 'points' => 0);
         return $return;
      endif;
      if($completeEvaluation == 1):
         $answersIS = 0;
         $db      = JFactory::getDbo();
         $query = $db->getQuery(true);
         $query->select(array('
         group_concat(b.content SEPARATOR "<br/>") as indiAnswers,
         count(a.id) as cCount
         '));
         $query->from($db->quoteName('#__jclassroom_theresults','a'));
         $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
         $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
         if($classroomID && $classroomID != 0):
            $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
         endif;
         $query->where('b.content <> ""');
         $db->setQuery($query);
         $answersIS = $db->loadObject();
         if($answersIS):
            $indiAnswers   = $answersIS->indiAnswers;
         else:
            $indiAnswers   = '';
         endif;
         $cCount = $answersIS->cCount;
         $theQuestion = array(
            'questionID'=> $question->questionID,
            'question'  => $question->title,
            'content'   => $question->content,
            'chart'     => $question->chart,
            'labels'    => $labels,
            'datas'     => $datas,
            'indiAnswers'  => $indiAnswers,
            'cCount'    => $cCount,
            'type'      => $type
         );
         return $theQuestion;
      endif;
   }
   // for Auswahlliste-Fragen
   function loadType12($rID, $questionID, $cart, $points, $calculate, $pdf, $f1, $w1, $w2, $w3, $g1, $completeEvaluation, $classroomID) {
      if($completeEvaluation == 0):
         if($pdf == 0):
            $tc = 'black';
            if($calculate == 2):
               $g1 = '60%';
               $tc = 'white';
            endif;
            $html = '';
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('a.*'));
              $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $db->setQuery($query);
            $answers = $db->loadObjectList();
            //Count correct answers
            $query   = $db->getQuery(true);
            $query->select(array('COUNT(correct)'));
              $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
            $db->setQuery($query);
            $correctSoll   = $db->loadResult();
            $correctIst    = 0;
            if($answers):
               $html .= '<td width="'.$g1.'" style="padding:5px;">';
               $html .= '<table style="width: 100%;">';
               foreach($answers as $answer):
                  if($calculate == 2):
                     if($answer->correct == 1):
                        $bgS = 'bg-success';
                     else:
                        $bgS = 'bg-danger';
                     endif;
                  else:
                     $bgS = '';
                  endif;
                  $html .= '<tr>';
                  $html .= '<td width="'.$w1.'" style="padding: 3px;"><span class="d-flex p-1 d-block badge '.$bgS.'" style="color: '.$tc.';justify-content: left;">'.$answer->title.'</span></td>';
                  $setCorrect = $answer->correct;
                  $theTD      = '<td width="'.$w2.'" style="height: 27px;"></td>';
                  $theAnswer  = '';
                  foreach($cart as $part):
                     $eparts  = $part['content'];
                     if($eparts):
                        foreach($eparts as $epart):
                           $EPS = explode('_', $epart);
                           if($EPS[0] == $answer->id):
                              if($calculate == 2):
                                 if($answer->correct == 1):
                                    $bgS = 'bg-success';
                                    $correctIst++;
                                    $thePoints += $points;
                                 else: 
                                    $bgS = 'bg-danger';
                                 endif;
                              else:
                                 $bgS = '';
                              endif;
                              $theTD = '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block badge '.$bgS.'" style="color: '.$tc.';justify-content: left;">'.$EPS[1].' '.$theID.'</span></td>';
                           endif;
                           
                        endforeach;
                     endif;
                  endforeach;
                  $html .= $theTD;
                  $html .= '</tr>';
               endforeach;
               $html .= '</table>';
               $html .= '</td>';
            endif;
            if($correctSoll > $correctIst):
               $thePoints = floor(($correctIst * $points / $correctSoll));
            endif;
            if($correctSoll == $correctIst):
               $thePoints = $points;
            endif;
            if($calculate == 2):
               $html .= '<td width="'.$w3.'" style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
            endif;
         endif;
         if($pdf == 1):
            $html = '';
            $tc = 'black';
            if($calculate == 2):
               $tc = 'white';
            endif;
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);

            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('a.*'));
            $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $db->setQuery($query);
            $answers = $db->loadObjectList();
            //Count correct answers
            $query   = $db->getQuery(true);
            $query->select(array('COUNT(correct)'));
            $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
            $db->setQuery($query);
            $correctSoll   = $db->loadResult();
            $correctIst    = 0;
            if($answers):
               $html .= '<td style="'.$w1.';" style="color: '.$tc.';">';
               $html .= '<table style="width: 100%;" border="1">';
               foreach($answers as $answer):
                  $bgS = '';
                  if($calculate == 2):
                     if($answer->correct == 1):
                        $bgS = 'background-color: forestgreen';
                     else:
                        $bgS = 'background-color: red';
                     endif;
                  endif;
                  $html .= '<tr>';
                  $html .= '<td style="color: '.$tc.';'.$bgS.'"><span class="" style="text-align: left;font-size: 8pt;line-height: 8pt;">' .$answer->title.'</span></td>';
                  $html .= '</tr>';
               endforeach;
               $html .= '</table>';
               $html .= '</td>';
            endif;
            $html .= '<td style="'.$w2.';" style="color: '.$tc.';">';
            $html .= '<table style="width: 100%;" border="1">';
            $thePoints = 0;
            foreach($answers as $answer):
               $theTD      = '<td style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';
               JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
               $r    = JTable::getInstance('Quizz_results','JclassroomTable',array());
               $load = array(
                  'theResultID'  => $rID,
                  'questionID'   => $questionID,
                  'answerID'     => $answer->id
               );
               $check = $r->load($load);
               if($check):
                  $bgI = '';
                  if($calculate == 2):
                     if($answer->correct == 1 && $r):
                        $bgI = 'background-color: forestgreen';
                        $thePoints = $quest->points;
                        $correctIst++;
                     else:
                        $bgI = 'background-color: red';
                     endif;
                  endif;
                  $theTD = '<td style="color: '.$tc.';'.$bgI.'"><span class="" style="font-size: 8pt;line-height: 8pt;text-align: left;">'.$r->content.'</span></td>';
               else:

               endif;
               $html .= '<tr>';
               $html .= $theTD;
               $html .= '</tr>';
            endforeach;
            $html .= '</table>';
            $html .= '</td>';
            if($correctSoll > $correctIst):
               $thePoints = floor(($correctIst * $quest->points / $correctSoll));
            endif;
            if($correctSoll == $correctIst):
               $thePoints = $quest->points;
            endif;
            if($calculate == 2):
               $html .= '<td width="'.$w3.'" style="text-align: center;"><span>'.$thePoints.'</span>/<span>'.$quest->points.'</span></td>';
            endif;
         endif;
         $return = array('html' => $html, 'points' => $thePoints);
         return $return;
      endif;
      if($completeEvaluation == 1):
         $cCount     = 0;
         $answersIS  = 0;
         $db         = JFactory::getDbo();
         $query      = $db->getQuery(true);
         $query->select(array('
            a.id,
            a.title
         '));
         $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
         $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
         $db->setQuery($query);
         $answersSOLL = $db->loadObjectList();
         foreach($answersSOLL as $answer):
            $labels[]   = strip_tags($answer->title);
            $query = $db->getQuery(true);
            $query->select(array('
               count(a.id) as data
            '));
            $query->from($db->quoteName('#__jclassroom_theresults','a'));
            $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
            if($classroomID && $classroomID != 0):
               $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
            endif;
            $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
            $query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
            if($classroomID && $classroomID != 0):
               $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
            endif;
            $query->order('a.created DESC');
            $query->setLimit(1);
            $db->setQuery($query);
            $answersIS = $db->loadResult();
            $datas[]    = $answersIS;
            $cCount  += $answersIS;
         endforeach;
         $theQuestion = array(
            'questionID'=> $questionID,
            'question'  => $question->title,
            'content'   => $question->content,
            'chart'     => $question->chart,
            'labels'    => $labels,
            'datas'     => $datas,
            'indiAnswers'  => $indiAnswers,
            'cCount'    => $cCount,
            'type'      => $type
         );
         return $theQuestion;
      endif;
   }
   // for Checkbox-Fragen
   function loadType13($rID, $questionID, $cart, $points, $calculate, $pdf, $f1, $w1, $w2, $w3, $completeEvaluation, $classroomID) {
      if($completeEvaluation == 0):
         if($pdf == 0):
            $tc = 'black';
            if($calculate == 2):
               $g1 = '60%';
               $tc = 'white';
            endif;
            $html = '';
            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('a.*'));
            $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $db->setQuery($query);
            $answers = $db->loadObjectList();
            //Count correct answers
            $query   = $db->getQuery(true);
            $query->select(array('COUNT(correct)'));
            $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
            $db->setQuery($query);
            $correctSoll   = $db->loadResult();
            $correctIst    = 0;
            if($answers):
               $html .= '<td width="'.$g1.'" style="padding:5px;">';
               $html .= '<table style="width: 100%;">';
               foreach($answers as $answer):
                  if($calculate == 2):
                     if($answer->correct == 1):
                        $bgS = 'bg-success';
                     else:
                        $bgS = 'bg-danger';
                     endif;
                  else:
                     $bgS = '';
                  endif;
                  $html .= '<tr>';
                  $html .= '<td width="'.$w1.'"" style="padding: 3px;"><span class="d-flex p-1 d-block badge '.$bgS.'" style="color: '.$tc.';justify-content: left;">'.$answer->title.'</span></td>';
                  $setCorrect = $answer->correct;
                  $theTD      = '<td width="'.$w2.'" style="height: 27px;"></td>';
                  $theAnswer  = '';
                  foreach($cart as $part):
                     $eparts  = $part['content'];
                     if($eparts):
                        foreach($eparts as $epart):
                           $EPS = explode('_', $epart);
                           if($EPS[0] == $answer->id):
                              if($calculate == 2):
                                 if($answer->correct == 1):
                                    $bgS = 'bg-success';
                                    $correctIst++;
                                    //$thePoints += $points;
                                 else: 
                                    $bgS = 'bg-danger';
                                 endif;
                              else:
                                 $bgS = '';
                              endif;
                              $theTD = '<td width="'.$w2.'" style="padding: 3px;"><span class="d-flex p-1 d-block badge '.$bgS.'" style="color: '.$tc.';justify-content: left;">'.$answer->title.' '.$theID.'</span></td>';
                           endif;
                        endforeach;
                     endif;
                  endforeach;
                  $html .= $theTD;

                  $html .= '</tr>';
               endforeach;
               $html .= '</table>';
               $html .= '</td>';
            endif;
            if($correctSoll > $correctIst):
               $thePoints = floor(($correctIst * $points / $correctSoll));
            endif;
            if($correctSoll == $correctIst):
               $thePoints = $points;
            endif;
            if($calculate == 2):
               $html .= '<td width="'.$w3.'" style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
            endif;
         endif;
         if($pdf == 1):
            $html = '';
            $tc = 'black';
            if($calculate == 2):
               $tc = 'white';
            endif;
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
            $quest->load($questionID);

            $db   = JFactory::getDbo();
            $query   = $db->getQuery(true);
            $query->select(array('a.*'));
            $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $db->setQuery($query);
            $answers = $db->loadObjectList();
            //Count correct answers
            $query   = $db->getQuery(true);
            $query->select(array('COUNT(correct)'));
            $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
            $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
            $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
            $db->setQuery($query);
            $correctSoll   = $db->loadResult();
            $correctIst    = 0;
            if($answers):
               $html .= '<td width="'.$w1.'" style="color:'.$tc.';">';
               $html .= '<table style="width: 100%;" border="1">';
               foreach($answers as $answer):
                  $bgS = '';
                  if($calculate == 2):
                     if($answer->correct == 1):
                        $bgS = 'background-color: forestgreen';
                     else:
                        $bgS = 'background-color: red';
                     endif;
                  endif;
                  $html .= '<tr>';
                  $html .= '<td style="color: '.$tc.';'.$bgS.';"><span style="text-align: left;font-size: 8pt;line-height: 8pt;">'.$answer->title.'</span></td>';
                  $html .= '</tr>';
               endforeach;
               $html .= '</table>';
               $html .= '</td>';
            endif;
            $html .= '<td width="'.$w2.'" style="color: '.$tc.';">';
            $html .= '<table style="width: 100%;" border="1">';
            $thePoints = 0;
            foreach($answers as $answer):
               $setCorrect = $answer->correct;
               $theTD      = '<td style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';
               $theAnswer  = '';
               JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
               $r    = JTable::getInstance('Quizz_results','JclassroomTable',array());
               $load = array(
                  'theResultID'  => $rID,
                  'questionID'   => $questionID,
                  'answerID'     => $answer->id
               );
               $check = $r->load($load);
               if($check):
                  $bgI = '';
                  if($calculate == 2):
                     if($answer->correct == 1 && $r):
                        $bgI = 'background-color: forestgreen';
                        $thePoints = $quest->points;
                     else:
                        $bgI = 'background-color: red';
                     endif;
                  endif;
                  $theTD = '<td style="color: '.$tc.';'.$bgI.';"><span class="" style="font-size: 8pt;line-height: 8pt;text-align: left;">'.$r->content.' '.$theID.'</span></td>';
               else:

               endif;
               $html .= '<tr>';
               $html .= $theTD;
               $html .= '</tr>';
            endforeach;
            $html .= '</table>';
            $html .= '</td>';
            if($correctSoll > $correctIst):
               //$thePoints = ($correctIst * $points / $correctSoll);
            endif;
            if($calculate == 2):
               $html .= '<td width="'.$w3.'" style="text-align: center;"><span>'.$thePoints.'</span>/<span>'.$quest->points.'</span></td>';
            endif;
         endif;
         $return = array('html' => $html, 'points' => $thePoints);
         return $return;
      endif;
      if($completeEvaluation == 1):
         $cCount     = 0;
         $answersIS  = 0;
         $db         = JFactory::getDbo();
         $query      = $db->getQuery(true);
         $query->select(array('
            a.id,
            a.title
         '));
         $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
         $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
         $db->setQuery($query);
         $answersSOLL = $db->loadObjectList();
         foreach($answersSOLL as $answer):
            $labels[]   = strip_tags($answer->title);
            $query = $db->getQuery(true);
            $query->select(array('
               count(a.id) as data
            '));
            $query->from($db->quoteName('#__jclassroom_theresults','a'));
            $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
            if($classroomID && $classroomID != 0):
               $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
            endif;
            $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
            $query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
            if($classroomID && $classroomID != 0):
               $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
            endif;
            $query->order('a.created DESC');
            $query->setLimit(1);
            $db->setQuery($query);
            $answersIS = $db->loadResult();
            $datas[]    = $answersIS;
            $cCount  += $answersIS;
         endforeach;
         $theQuestion = array(
            'questionID'=> $questionID,
            'question'  => $question->title,
            'content'   => $question->content,
            'chart'     => $question->chart,
            'labels'    => $labels,
            'datas'     => $datas,
            'indiAnswers'  => $indiAnswers,
            'cCount'    => $cCount,
            'type'      => $type
         );
         return $theQuestion;
      endif;
   }
   // ***************************************
   // Results for PDF
   // ***************************************
   function getResultPDF($rID, $fromLR, $userID, $completeEvaluation, $classroomID, $publishedQuizzID, $quizzID) {

      $user       = JFactory::getUser();
      $db      = JFactory::getDbo();
      // Load the result of the quizz
      $query      = $db->getQuery(true);
      $query->select(array('
         COUNT(a.id) as participations,
         a.*, 
         b.*
      '));
      $query->from($db->quoteName('#__jclassroom_theresults','a'));
      $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
      if($fromLR == 1 && $completeEvaluation == 0):
         $query->where($db->quoteName('a.id').' = '.$db->quote($rID));
      endif;
      if($fromLR == 1 && $completeEvaluation == 1):
         $query->where($db->quoteName('a.classroomID').' = '.$db->quote($classroomID));
         $query->where($db->quoteName('a.quizzID').' = '.$db->quote($rID));
      endif;
      if($fromLR == 0 && $publishedQuizzID):
         $query->where($db->quoteName('a.quizzID').' = '.$db->quote($rID));
      endif;
      $db->setQuery($query);
      $questions  = $db->loadObjectList();
      //$quizzID    = $questions[0]->quizzID;
      //$theResultID= $questions[0]->theResultID;
      if($fromLR == 1):
         $html .= $this->getQuizzResultPDF($quizzID, $rID, $fromLR, $userID, $completeEvaluation, $classroomID);
      else:
         if($publishedQuizzID):
             //Load the quizz
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quizz      = JTable::getInstance('Quizz','JclassroomTable',array());
            $quizz->load($publishedQuizzID);
         else:
            //Load the quizz
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
            $quizz      = JTable::getInstance('Quizz','JclassroomTable',array());
            $quizz->load($rID);
         endif;
         $html       = false;
         if($quizz->quizzType == 1):
            if($questions):
               echo $quizzID;
               $html .= $this->getQuizzResultPDF($quizzID, $rID, $fromLR, $userID, $completeEvaluation, $classroomID);
            endif;
         endif;
         if($quizz->quizzType == 2):
            $html .= '<h2>Auswertung für Umfrage</h2>';
            if($questions):
               $html .= $this->getUmfrageResult($quizzID, $rID);
            endif;
         endif;
         if($quizz->quizzType == 3):
            $html .= '<p>Auswertung für Audit</p>';
            if($questions):
               $html .= $this->getAuditResult($quizzID, $rID);
            endif;
         endif;
      endif;

      return $html;
   }
   function getQuizzResultPDF($quizzID, $rID, $fromLR, $userID, $completeEvaluation, $classroomID) {
      $session    = JFactory::getSession();
      $cart       = $session->get('quizzes');
      // Load the quizz
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quizz      = JTable::getInstance('Unit','JclassroomTable',array());
      $quizz->load($quizzID);
      $counter    = 1;
      $points     = 0;
      $maxPoints  = 0;
      $calculate  = $quizz->calculate;

      $html = '';
      $html .= '<table border="1" cellpadding="2">';
      $html .= '<thead>';
      $html .= '<tr>';
      if($fromLR == 1 && $completeEvaluation == 0):
         if($calculate < 2):
            $html .= '<th style="background-color: lightsteelblue;" width="160px">Frageddd'.$quizzID.'</th>';
            $html .= '<th style="background-color: lightsteelblue;" width="160px">Antwortmöglichkeiten</th>';
            $html .= '<th style="background-color: lightsteelblue;" width="190px">Ihre Antworten</th>';
            $f1   = '160px';
            $w1   = '160px';
            $w2   = '190px';
            $w3   = '';
         endif;
         if($calculate == 2):
            $html .= '<th style="background-color: lightsteelblue;" width="160px">Frage'.$quizzID.'</th>';
            $html .= '<th style="background-color: lightsteelblue;" width="150px">Antwortmöglichkeiten</th>';
            $html .= '<th style="background-color: lightsteelblue;" width="150px">Ihre Antworten</th>';
            $html .= '<th style="background-color: lightsteelblue;text-align: center;" width="50px">Punkte</th>';
            $f1   = '160px';
            $w1   = '150px';
            $w2   = '150px';
            $w3   = '50px';
         endif;
      endif;
      if($fromLR == 1 && $completeEvaluation == 1):
         $html .= '<th style="background-color: lightsteelblue;" width="160px">Frage</th>';
         $html .= '<th style="background-color: lightsteelblue;" width="350px">Diagramm</th>';
         $f1   = '160px';
         $w1   = '350px';
         $w2   = '';
         $w3   = '';
      endif;
      if($fromLR == 0 && $completeEvaluation == 1):
         $html .= '<th style="background-color: lightsteelblue;" width="160px">Frage</th>';
         $html .= '<th style="background-color: lightsteelblue;" width="350px">Diagramm</th>';
         $f1   = '160px';
         $w1   = '350px';
         $w2   = '';
         $w3   = '';
      endif;
      $html .= '</tr>';
      $html .= '</thead>';
      //Load the quizzPositions 
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
      $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
      $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
      $query->where($db->quoteName('a.type').' <> '.$db->quote(1));
      $query->order('a.ordering asc');
      $db->setQuery($query);
      $quizzPositions   = $db->loadObjectList();
      if($quizzPositions):
         $html .= '<tbody>';
         $i = 1;
         foreach($quizzPositions as $quizzPosition):
            if($fromLR == 1):
               $theBG = 'style="background-color: lightgray;"';
               if($quizzPosition->type == 14):
                  $theBG = 'style="background-color: lightgray;"';
                  $html .= '<tr nobr="true" height="40px">';
                  $html .= '<td style="text-align: left;" width="510px" '.$theBG.' colspan="3"><h5 style="text-align: left;"><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
               endif;
               if($completeEvaluation == 0 && $quizzPosition->type != 14):
                  $theBG = 'style="background-color: wheat;"';
                  $colspan = 1;
                  $html .= '<tr nobr="true" height="40px">';
                  $html .= '<td style="text-align: left;" width="'.$f1.'" '.$theBG.' colspan="'.$colspan.'"><h5 style="text-align: left;"><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
                  switch($quizzPosition->type):
                     case '3':
                        // WWM-Fragen
                        $return = $this->loadType3PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '4':
                        // Smiley-Fragen
                        $return = $this->loadType4PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '5':
                        // Ja/Nein-Fragen
                        $return = $this->loadType5PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '10':
                        // Textfeld-Fragen
                        $return = $this->loadType10PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '11':
                        // Textarea-Fragen
                        $return = $this->loadType11PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '12':
                        // Auswahlliste-Fragen
                        $return = $this->loadType12PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '13':
                        // Checkbox-Fragen
                        $return = $this->loadType13PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break; 
                  endswitch;
               endif;
               if($completeEvaluation == 1 && $quizzPosition->type != 14):
                  $theBG = 'style="background-color: wheat;"';
                  $colspan = 1;
                  $html .= '<tr nobr="true" height="40px">';
                  $html .= '<td style="text-align: left;" width="'.$f1.'" '.$theBG.' colspan="'.$colspan.'"><h5 style="text-align: left;"><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
                  if($calculate == 2):
                     $path       = '/images/evaluates/LR'.$classroomID.'unit'.$quizzID.'/theModalChart'.$quizzPosition->id.'.png';
                     if(($quizzPosition->type != 10 && $quizzPosition->type != 11 && $quizzPosition->type != 14) && $path):
                        $html .= '<td width="'.$w1.'"><br/><br/><img src="'.$path.'" /><br/></td>';
                     else:
                        switch($quizzPosition->type):
                           case '10':
                              // Textfeld-Fragen
                              //$return = $this->loadType10($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR, $completeEvaluation, $classroomID);
                              $html .= $return['html'];
                              $points += $return['points'];
                              break;
                           case '11':
                              // Textarea-Fragen
                              //$return = $this->loadType11($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR, $completeEvaluation, $classroomID);
                              $html .= $return['html'];
                              $points += $return['points'];
                              break;
                        endswitch;
                     endif;
                  else:
                     switch($quizzPosition->type):
                           case '3':
                              // WWM-Fragen
                              $return = $this->loadType3PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                           case '4':
                              // Smiley-Fragen
                              $return = $this->loadType4PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                           case '5':
                              // Ja/Nein-Fragen
                              $return = $this->loadType5PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                           case '10':
                              // Auswahllisten-Fragen
                              $return = $this->loadType10PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 $html .= '<p style="line-height: 12pt;">'.$return['indiAnswers'].'</p>';
                                 $html .= '</td>';
                              endif;
                              break;
                           case '11':
                              // Auswahllisten-Fragen
                              $return = $this->loadType11PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 $html .= '<p style="line-height: 12pt;">'.$return['indiAnswers'].'</p>';
                                 $html .= '</td>';
                              endif;
                              break;
                           case '12':
                              // Auswahllisten-Fragen
                              $return = $this->loadType12PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                           case '13':
                              // Checklisten-Fragen
                              $return = $this->loadType13PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                        endswitch;
                  endif;
               endif;
            endif;
            if($fromLR == 0):
               $theBG = 'style="background-color: lightgray;"';
               if($quizzPosition->type == 14):
                  $theBG = 'style="background-color: lightgray;"';
                  $html .= '<tr nobr="true" height="40px">';
                  $html .= '<td style="text-align: left;" width="510px" '.$theBG.' colspan="3"><h5 style="text-align: left;"><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
               endif;
               if($completeEvaluation == 1 && $quizzPosition->type != 14):
                  $theBG = 'style="background-color: wheat;"';
                  $colspan = 1;
                  $html .= '<tr nobr="true" height="40px">';
                  $html .= '<td style="text-align: left;" width="'.$f1.'" '.$theBG.' colspan="'.$colspan.'"><h5 style="text-align: left;"><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
                  switch($quizzPosition->type):
                     case '3':
                        // WWM-Fragen
                        $return = $this->loadType3PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '4':
                        // Smiley-Fragen
                        $return = $this->loadType4PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '5':
                        // Ja/Nein-Fragen
                        echo "RID".$rID;
                        $return = $this->loadType5PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '10':
                        // Textfeld-Fragen
                        $return = $this->loadType10PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '11':
                        // Textarea-Fragen
                        $return = $this->loadType11PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '12':
                        // Auswahlliste-Fragen
                        $return = $this->loadType12PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break;
                     case '13':
                        // Checkbox-Fragen
                        $return = $this->loadType13PDF($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3);
                        $html .= $return['html'];
                        $points += $return['points'];
                        break; 
                  endswitch;
               endif;
               if($completeEvaluation == 1 && $quizzPosition->type != 14):
                  $theBG = 'style="background-color: wheat;"';
                  $colspan = 1;
                  $html .= '<tr nobr="true" height="40px">';
                  $html .= '<td style="text-align: left;" width="'.$f1.'" '.$theBG.' colspan="'.$colspan.'"><h5 style="text-align: left;"><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
                  if($calculate == 2):
                     $path       = '/images/evaluates/LR'.$classroomID.'unit'.$quizzID.'/theModalChart'.$quizzPosition->id.'.png';
                     if(($quizzPosition->type != 10 && $quizzPosition->type != 11 && $quizzPosition->type != 14) && $path):
                        $html .= '<td width="'.$w1.'"><br/><br/><img src="'.$path.'" /><br/></td>';
                     else:
                        switch($quizzPosition->type):
                           case '10':
                              // Textfeld-Fragen
                              //$return = $this->loadType10($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR, $completeEvaluation, $classroomID);
                              $html .= $return['html'];
                              $points += $return['points'];
                              break;
                           case '11':
                              // Textarea-Fragen
                              //$return = $this->loadType11($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR, $completeEvaluation, $classroomID);
                              $html .= $return['html'];
                              $points += $return['points'];
                              break;
                        endswitch;
                     endif;
                  else:
                     switch($quizzPosition->type):
                           case '3':
                              // WWM-Fragen
                              $return = $this->loadType3PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                           case '4':
                              // Smiley-Fragen
                              $return = $this->loadType4PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                           case '5':
                              // Ja/Nein-Fragen
                              $return = $this->loadType5PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                           case '10':
                              // Auswahllisten-Fragen
                              $return = $this->loadType10PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 $html .= '<p style="line-height: 12pt;">'.$return['indiAnswers'].'</p>';
                                 $html .= '</td>';
                              endif;
                              break;
                           case '11':
                              // Auswahllisten-Fragen
                              $return = $this->loadType11PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 $html .= '<p style="line-height: 12pt;">'.$return['indiAnswers'].'</p>';
                                 $html .= '</td>';
                              endif;
                              break;
                           case '12':
                              // Auswahllisten-Fragen
                              $return = $this->loadType12PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                           case '13':
                              // Checklisten-Fragen
                              $return = $this->loadType13PDFComplete($rID, $quizzPosition->id, $quizzID, $quizz->calculate, $fromLR, $w1, $w2, $w3, $classroomID);
                              if($return):
                                 $html .= '<td width="'.$w1.'" style="font-size: 8pt;">';
                                 $html .= '<p style="line-height: 5pt;"><b>Anzahl Teilnehmer: '.$return['cCount'].'</b></p>';
                                 foreach($return['labels'] as $key => $value):
                                    $html .= '<span style="line-height: 5pt;">'.$value.': <b>' .$return['datas'][$key].'</b></span><br/>';
                                 endforeach;
                                 $html .= '</td>';
                              endif;
                              break;
                        endswitch;
                  endif;
               endif;
            endif;
            $html .= '</tr>';
            $counter++;
            if($quizzPosition->type != 4 && $quizzPosition->type != 10 && $quizzPosition->type != 11):
               $maxPoints += $quizzPosition->points;
            endif;
            $i++;
         endforeach;
         $html .= '</tbody>';
      endif;
      $html .= '</table>';
      if($completeEvaluation == 0 && $calculate == 2):
         $html .= '<br/>';
         $html .= '<table width="510px;" border="1" cellpadding="2">';
         $html .= '<tr>';
         $html .= '<th width="460px;">Gesamtpunktzahl:</th>';
         $html .= '<th width="50px" style="text-align: center;">'.$maxPoints.'</th>';
         $html .= '</tr>';
         $html .= '<tr>';
         $html .= '<th width="460px;">Erreichte Punktzahl:</th>';
         $html .= '<th width="50px" style="text-align: center;">'.$points.'</th>';
         $html .= '</tr>';
         $html .= '<tr>';
         $html .= '<th width="460px;">Gesamtscore:</th>';
         $percent = (100 * $points) / $maxPoints;
         $html .= '<th width="50px" style="text-align: center;">'.number_format($percent,0,',','.').'%</th>';
         $html .= '</tr>';
         $html .= '</table>';
      endif;
      return $html;
   }
   // for WWM-Fragen
   function loadType3PDF($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3) {
      $html = '';
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);

      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
      $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $answers = $db->loadObjectList();
      //Count correct answers
      $query   = $db->getQuery(true);
      $query->select(array('COUNT(correct)'));
      $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
      $db->setQuery($query);
      $correctSoll   = $db->loadResult();
      $correctIst    = 0;
      $query   = $db->getQuery(true);
      $query->select(array('
         a.answerID,
         a.content
      '));
      $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
      $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $quizzPositions   = $db->loadObjectList();
      $thePoints = 0;
      if($answers):
         $html .= '<td style="width:'.$w1.'">';
         $html .= '<table style="width: 100%;" border="1">';
         foreach($answers as $answer):
            $bgs = '';
            // If quizz should be calculate
            if($calculate == 2):
               if($answer->correct == 1):
                  $bgs = 'background-color: forestgreen';
               else:
                  $bgs = 'background-color: red';
               endif;
            endif;
            $html .= '<tr>';
            $html .= '<td style="'.$bgs.'"><span style="text-align: left; font-size: 8pt; line-height: 8pt;">'.strip_tags($answer->title).'</span></td>';
            $html .= '</tr>';
         endforeach;
         $html .= '</table>';
         $html .= '</td>';
      endif;
      //if cart is not empty
      if($quizzPositions):
         $html .= '<td style="width:'.$w2.'">';
         $html .= '<table style="width: 100%;" border="1">';
         foreach($answers as $answer):
            $bgI = '';
            $theTD      = '<td style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';;
            foreach($quizzPositions as $part):
               if($part->answerID == $answer->id):
                  if($answer->correct == 1):
                     if($calculate == 2):
                        $bgI = 'background-color: forestgreen';
                     endif;
                     $correctIst++;
                     $thePoints += $quest->points;
                  else: 
                     if($calculate == 2):
                        $bgI = 'background-color: red';
                     endif;
                  endif;
                  $theTD = '<td style="'.$bgI.'"><span style="text-align: left; font-size: 8pt; line-height: 8pt;">'.$part->content.'</span></td>';
               endif;
            endforeach;
            $html .= '<tr>';
            $html .= $theTD;
            $html .= '</tr>';
         endforeach;
         $html .= '</table>';
         $html .= '</td>';
         if($correctSoll > $correctIst):
            $thePoints = floor(($correctIst * $quest->points / $correctSoll));
         endif;
         if($correctSoll == $correctIst):
            $thePoints = $quest->points;
         endif;
         if($calculate == 2):
            $html .= '<td width="'.$w3.'" style="text-align: center;vertical-align: center;">'.$thePoints.'/'.$quest->points;
            $html .= '</td>';
         endif;
      endif;
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   function loadType3PDFComplete($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3, $classroomID) {
      $cCount = 0;
      $db      = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('
         a.title,a.id
      '));
      $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
      $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $answersSOLL = $db->loadObjectList();
      foreach($answersSOLL as $answer):
         //echo $question->questionID.' '.$answer->id.' '.$input->get('classroomID',0,'INT').' '.$input->get('unitID',0,'INT').'<br/>';
         $labels[]   = strip_tags($answer->title);
         $query = $db->getQuery(true);
         $query->select(array('
            a.id
         '));
         $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
         $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
         $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
         $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
         $query->where($db->quotename('a.answerID').' = '.$db->quote($answer->id));
         $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
         //$query->where($db->quotename('b.unitID').' = '.$db->quote($quizzID));
         $query->order('a.created DESC');
         $db->setQuery($query);
         $db->execute();
         $numRows    = $db->getNumRows();
         $answersIS  = $db->loadObject();
         $datas[]    = $numRows;
         $cCount     += $numRows;
      endforeach;
      $theQuestion = array(
         'questionID'=> $question->questionID,
         'question'  => $question->title,
         'content'   => $question->content,
         'chart'     => $question->chart,
         'labels'    => $labels,
         'datas'     => $datas,
         'indiAnswers'  => $indiAnswers,
         'cCount'    => $cCount,
         'type'      => $type
      );
      return $theQuestion;
   }
   // for Smiley-Fragen
   function loadType4PDF($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3) {
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);
      $html = '';
      $db   = JFactory::getDbo();
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('
         a.answerID
      '));
      $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
      $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->group('a.content');
      $db->setQuery($query);
      $quizzPositions   = $db->loadObject();
      $html .= '<td width="'.$w1.'">';
      $html .= '<table style="width: 100%;" border="1">';
      for($i = 5;$i >= 1;$i--):
         switch($i):
            case 5:
               $bg = 'forestgreen';
               $tt = 'Sehr gut';
               break;
            case 4: 
               $bg = 'yellowgreen';
               $tt = 'Gut';
               break;
            case 3:
               $bg = 'gold';
               $tt = 'Nicht schlecht';
               break;
            case 2:
               $bg = 'red';
               $tt = 'Schlecht';
               break;
            case 1: 
               $bg = 'firebrick';
               $tt = 'Sehr schlecht';
               break;
         endswitch;
         $html .= '<tr>';
         $html .= '<td style="background-color: '.$bg.';font-size: 8pt: line-height: 40px;text-align: left;"><span style="line-height: 17px;">'.$tt.'</span></td>';
         $html .= '</tr>';
      endfor;
      $html .= '</table>';
      $html .= '</td>';
      $html .= '<td width="'.$w2.'">';
      $html .= '<table style="width: 100%;" border="1">';
      
      for($i = 5;$i >= 1;$i--):
         $show = 0;
         switch($i):
            case 5:
               $bg = 'forestgreen';
               $tt = 'Sehr gut';
               if($quizzPositions->answerID == 5):
                  $show = 1;
               endif;
               break;
            case 4: 
               $bg = 'yellowgreen';
               $tt = 'Gut';
               if($quizzPositions->answerID == 4):
                  $show = 1;
               endif;
               break;
            case 3:
               $bg = 'gold';
               $tt = 'Nicht schlecht';
               if($quizzPositions->answerID == 3):
                  $show = 1;
               endif;
               break;
            case 2:
               $bg = 'red';
               $tt = 'Schlecht';
               if($quizzPositions->answerID == 2):
                  $show = 1;
               endif;
               break;
            case 1: 
               $bg = 'firebrick';
               $tt = 'Sehr schlecht';
               if($quizzPositions->answerID == 1):
                  $show = 1;
               endif;
               break;
         endswitch;
         if($show == 1):
            $html .= '<tr>';
            $html .= '<td style="background-color: '.$bg.';font-size: 8pt: line-height: 17pt;text-align: left;"><span style="line-height: 17px;">'.$tt.'</span></td>';
            $html .= '</tr>';
         else:
            $html .= '<tr>';
            //$html .= '<td style="background-color: red;font-size: 8pt;line-height: 17pt;">'.$tt.'</td>';
            $html .= '<td style="background-color: white;font-size: 8pt: line-height: 17pt;text-align: left;"><span style="line-height: 17px;"></span></td>';
            $html .= '</tr>';
         endif;
         
      endfor;
      $html .= '</table>';
      $html .= '</td>';
      if($calculate == 2):
         $html .= '<td style="width: '.$w3.';"></td>';
      endif;

      $return = array('html' => $html, 'points' => 0);
      return $return;
   }
   function loadType4PDFComplete($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3, $classroomID) {
      $answersIS = 0;
      $labels  = array('Sehr schlecht', 'Schlecht','Nicht schlecht','Gut','Sehr gut');
      $cCount = 0;
      $db      = JFactory::getDbo();
      for($i = 1;$i <= 5;$i++) {
         $query = $db->getQuery(true);
         $query->select(array('
            count(b.answerID) as data
         '));
         $query->from($db->quoteName('#__jclassroom_theresults','a'));
         $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
         $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
         $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
         $query->where($db->quotename('b.answerID').' = '.$db->quote($i));
         $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
         $db->setQuery($query);
         $answersIS = $db->loadResult();
         if($answersIS):
            $datas[]    = $answersIS;
            $cCount     += $answersIS;
         else:
            $datas[]    = 0;
         endif;
      }
      $theQuestion = array(
         'questionID'=> $question->questionID,
         'question'  => $question->title,
         'content'   => $question->content,
         'chart'     => $question->chart,
         'labels'    => $labels,
         'datas'     => $datas,
         'indiAnswers'  => $indiAnswers,
         'cCount'    => $cCount,
         'type'      => $type
      );
      return $theQuestion;
   }
   // for Ja/Nein-Fragen
   function loadType5PDF($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3) {
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('
         a.content
      '));
      $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
      $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->group('a.content');
      $db->setQuery($query);
      $quizzPositions   = $db->loadObjectList();
      $thePoints = 0;
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);
      $bgsJ = '';
      $bgsN = '';
      // If quizz should be calculate
      if($calculate == 2):
         if($quest->correctAnswers == 0):
            $bgsJ = 'background-color: forestgreen';
            $bgsN = 'background-color: red';
         else:
            $bgsJ = 'background-color: red';
            $bgsN = 'background-color: forestgreen';
         endif;
      endif;
      $html = '';
      $html .= '<td width="'.$w1.'">';
      $html .= '<table style="width: 100%;" border="1">';
      $html .= '<tr>';
      $html .= '<td style="'.$bgsJ.';"><span class="" style="text-align: left;font-size: 8pt; line-height: 8pt;">Ja</span></td>';
      $html .= '</tr>';
      $html .= '<tr>';
      $html .= '<td style="'.$bgsN.';"><span class="" style="text-align: left;font-size: 8pt; line-height: 8pt;">Nein</span></td>';
      $html .= '</tr>';
      $html .= '</table>';
      $html .= '</td>';
      $thePoints = 0;
      //if cart is not empty
      if($quizzPositions):
         $html .= '<td width="'.$w2.'">';
         $html .= '<table style="width: 100%;" border="1">';
         foreach($quizzPositions as $part):
            $bgiJ = '';
            $bgiN = '';
            $ATJ = '<td style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';
            $ATN = '<td style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';
            if($part->content == 'answerJ'):
               if($quest->correctAnswers == 0):
                  if($calculate == 2):
                     $bgiJ = 'background-color: forestgreen';
                     $bgiN = 'background-color: red';
                  endif;
                  $thePoints = $quest->points;
               endif;
               if($quest->correctAnswers == 1):
                  if($calculate == 2):
                     $bgiJ = 'background-color: red';
                     $bgiN = 'background-color: forestgreen';
                  endif;
                  $thePoints = 0;
               endif;
               $ATJ = '<td style="'.$bgiJ.';"><span style="text-align: left;font-size: 8pt;line-height: 8pt;">Ja</span></td>';

            endif;
            if($part->content == 'answerN'):
               if($quest->correctAnswers == 0):
                  if($calculate == 2):
                     $bgiJ = 'background-color: forestgreen';
                     $bgiN = 'background-color: red';
                  endif;
                  $thePoints = 0;
               endif;
               if($quest->correctAnswers == 1):
                  if($calculate == 2):
                     $bgiJ = 'background-color: red';
                     $bgiN = 'background-color: forestgreen';
                  endif;
                  $thePoints = $quest->points;
               endif;
               $ATN = '<td style="'.$bgiN.';"><span style="text-align: left;font-size: 8pt;line-height: 8pt;">Nein</span></td>';
            endif;
         endforeach;
         $html .= '<tr>';
         $html .= $ATJ;
         $html .= '</tr>';
         $html .= '<tr>';
         $html .= $ATN;
         $html .= '</tr>';
         $html .= '</table>';
         $html .= '</td>';
      endif;
      if($calculate == 2):
         $html .= '<td width="'.$w3.'" style="text-align: center;vertical-align: center;">'.$thePoints.'/'.$quest->points;
         $html .= '</td>';
      endif;
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   function loadType5PDFComplete($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3, $classroomID) {
      $cCount  = 0;
      $db      = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('
      count(a.id) as cJ
      '));
      $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
      $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
      $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
      $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
      $query->where($db->quotename('a.content').' = '.$db->quote('answerJ'));
      $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
      $db->setQuery($query);
      $answersIS  = $db->loadObject();
      $datas[]    = $answersIS->cJ;
      $labels[]   = 'Ja';
      $cCount += $answersIS->cJ;
      $query = $db->getQuery(true);
      $query->select(array('
      count(a.id) as cN
      '));
      $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
      $query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
      $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('b.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
      $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
      $query->where($db->quotename('a.content').' = '.$db->quote('answerN'));
      $query->where($db->quotename('b.classroomID').' = '.$db->quote($classroomID));
      $db->setQuery($query);
      $answersIS  = $db->loadObject();
      $datas[]    = $answersIS->cN;
      $labels[]   = 'Nein';
      $cCount += $answersIS->cN;
      $theQuestion = array(
         'questionID'=> $question->questionID,
         'question'  => $question->title,
         'content'   => $question->content,
         'chart'     => $question->chart,
         'labels'    => $labels,
         'datas'     => $datas,
         'indiAnswers'  => $indiAnswers,
         'cCount'    => $cCount,
         'type'      => $type
      );
      return $theQuestion;
   }
   // for Textfeld-Fragen
   function loadType10PDF($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3) {
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('
         a.content
      '));
      $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
      $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->group('a.content');
      $db->setQuery($query);
      $quizzPositions   = $db->loadObject();
      $html = '';
      if($quizzPositions):
         $html .= '<td colspan="2"><span style="font-size:8pt;line-height: 8pt; text-align: left;">'.$quizzPositions->content.'</span></td>';
      endif;
      if($calculate == 2):
         $html .= '<td style="width:'.$w3.';"></td>';
      endif;
      $return = array('html' => $html, 'points' => 0);
      return $return;
   }
   function loadType10PDFComplete($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3, $classroomID) {
      $answersIS = 0;
      $db      = JFactory::getDbo();
      $query = $db->getQuery(true);
      $query->select(array('
      group_concat(b.content SEPARATOR "<br/>") as indiAnswers,
      count(a.id) as cCount
      '));
      $query->from($db->quoteName('#__jclassroom_theresults','a'));
      $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
      $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
      $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
      $query->where('b.content <> ""');
      $db->setQuery($query);
      $answersIS = $db->loadObject();
      if($answersIS):
         $indiAnswers   = $answersIS->indiAnswers;
      else:
         $indiAnswers   = '';
      endif;
      $cCount = $answersIS->cCount;
      $theQuestion = array(
         'questionID'=> $question->questionID,
         'question'  => $question->title,
         'content'   => $question->content,
         'chart'     => $question->chart,
         'labels'    => $labels,
         'datas'     => $datas,
         'indiAnswers'  => $indiAnswers,
         'cCount'    => $cCount,
         'type'      => $type
      );
      return $theQuestion;
   }
   // for Textarea-Fragen
   function loadType11PDF($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3) {
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);
      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('
         a.content
      '));
      $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
      $query->where($db->quoteName('a.theResultID').' = '.$db->quote($rID));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->group('a.content');
      $db->setQuery($query);
      $quizzPositions   = $db->loadObject();
      $html = '';
      if($quizzPositions):
         $html .= '<td colspan="2"><span style="font-size:8pt;line-height: 8pt; text-align: left;">'.$quizzPositions->content.'</span></td>';
      endif;
      if($calculate == 2):
         $html .= '<td style="width:'.$w3.';"></td>';
      endif;
      $return = array('html' => $html, 'points' => 0);
      return $return;
   }
   function loadType11PDFComplete($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3, $classroomID) {
      $answersIS = 0;
      $db      = JFactory::getDbo();
      $query = $db->getQuery(true);
      $query->select(array('
      group_concat(b.content SEPARATOR "<br/>") as indiAnswers,
      count(a.id) as cCount
      '));
      $query->from($db->quoteName('#__jclassroom_theresults','a'));
      $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
      $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
      $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
      $query->where('b.content <> ""');
      $db->setQuery($query);
      $answersIS = $db->loadObject();
      if($answersIS):
         $indiAnswers   = $answersIS->indiAnswers;
      else:
         $indiAnswers   = '';
      endif;
      $cCount = $answersIS->cCount;
      $theQuestion = array(
         'questionID'=> $question->questionID,
         'question'  => $question->title,
         'content'   => $question->content,
         'chart'     => $question->chart,
         'labels'    => $labels,
         'datas'     => $datas,
         'indiAnswers'  => $indiAnswers,
         'cCount'    => $cCount,
         'type'      => $type
      );
      return $theQuestion;
   }
   // for Auswahlliste-Fragen
   function loadType12PDF($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3) {
      $html = '';
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);

      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
      $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $answers = $db->loadObjectList();
      //Count correct answers
      $query   = $db->getQuery(true);
      $query->select(array('COUNT(correct)'));
      $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
      $db->setQuery($query);
      $correctSoll   = $db->loadResult();
      $correctIst    = 0;
      if($answers):
         $html .= '<td style="'.$w1.';">';
         $html .= '<table style="width: 100%;" border="1">';
         foreach($answers as $answer):
            $bgS = '';
            if($calculate == 2):
               if($answer->correct == 1):
                  $bgS = 'background-color: forestgreen';
               else:
                  $bgS = 'background-color: red';
               endif;
            endif;
            $html .= '<tr>';
            $html .= '<td style="'.$bgS.'"><span class="" style="text-align: left;font-size: 8pt;line-height: 8pt;">' .$answer->title.'</span></td>';
            $html .= '</tr>';
         endforeach;
         $html .= '</table>';
         $html .= '</td>';
      endif;
      $html .= '<td style="'.$w2.';">';
      $html .= '<table style="width: 100%;" border="1">';
      $thePoints = 0;
      foreach($answers as $answer):
         $theTD      = '<td style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';
         JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
         $r    = JTable::getInstance('Quizz_results','JclassroomTable',array());
         $load = array(
            'theResultID'  => $rID,
            'questionID'   => $questionID,
            'answerID'     => $answer->id
         );
         $check = $r->load($load);
         if($check):
            $bgI = '';
            if($calculate == 2):
               if($answer->correct == 1 && $r):
                  $bgI = 'background-color: forestgreen';
                  $thePoints = $quest->points;
                  $correctIst++;
               else:
                  $bgI = 'background-color: red';
               endif;
            endif;
            $theTD = '<td style="'.$bgI.'"><span class="" style="font-size: 8pt;line-height: 8pt;text-align: left;">'.$r->content.'</span></td>';
         else:

         endif;
         $html .= '<tr>';
         $html .= $theTD;
         $html .= '</tr>';
      endforeach;
      $html .= '</table>';
      $html .= '</td>';
      if($correctSoll > $correctIst):
         $thePoints = floor(($correctIst * $quest->points / $correctSoll));
      endif;
      if($correctSoll == $correctIst):
         $thePoints = $quest->points;
      endif;
      if($calculate == 2):
         $html .= '<td width="'.$w3.'" style="text-align: center;"><span>'.$thePoints.'</span>/<span>'.$quest->points.'</span></td>';
      endif;
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   function loadType12PDFComplete($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3, $classroomID) {
      $cCount     = 0;
      $answersIS  = 0;
      $db         = JFactory::getDbo();
      $query      = $db->getQuery(true);
      $query->select(array('
         a.id,
         a.title
      '));
      $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
      $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $answersSOLL = $db->loadObjectList();
      foreach($answersSOLL as $answer):
         $labels[]   = strip_tags($answer->title);
         $query = $db->getQuery(true);
         $query->select(array('
            count(a.id) as data
         '));
         $query->from($db->quoteName('#__jclassroom_theresults','a'));
         $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
         $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
         $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
         $query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
         $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
         $query->order('a.created DESC');
         $query->setLimit(1);
         $db->setQuery($query);
         $answersIS = $db->loadResult();
         $datas[]    = $answersIS;
         $cCount  += $answersIS;
      endforeach;
      $theQuestion = array(
         'questionID'=> $question->questionID,
         'question'  => $question->title,
         'content'   => $question->content,
         'chart'     => $question->chart,
         'labels'    => $labels,
         'datas'     => $datas,
         'indiAnswers'  => $indiAnswers,
         'cCount'    => $cCount,
         'type'      => $type
      );
      return $theQuestion;
   }
   // for Checkbox-Fragen
   function loadType13PDF($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3) {
      $html = '';
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quest      = JTable::getInstance('Quizz_positions','JclassroomTable',array());
      $quest->load($questionID);

      $db   = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.*'));
      $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $answers = $db->loadObjectList();
      //Count correct answers
      $query   = $db->getQuery(true);
      $query->select(array('COUNT(correct)'));
      $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
      $query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
      $query->where($db->quoteName('a.correct').' = '.$db->quote(1));
      $db->setQuery($query);
      $correctSoll   = $db->loadResult();
      $correctIst    = 0;
      if($answers):
         $html .= '<td width="'.$w1.'">';
         $html .= '<table style="width: 100%;" border="1">';
         foreach($answers as $answer):
            $bgS = '';
            if($calculate == 2):
               if($answer->correct == 1):
                  $bgS = 'background-color: forestgreen';
               else:
                  $bgS = 'background-color: red';
               endif;
            endif;
            $html .= '<tr>';
            $html .= '<td style="'.$bgS.';"><span style="text-align: left;font-size: 8pt;line-height: 8pt;">'.$answer->title.'</span></td>';
            $html .= '</tr>';
         endforeach;
         $html .= '</table>';
         $html .= '</td>';
      endif;
      $html .= '<td width="'.$w2.'">';
      $html .= '<table style="width: 100%;" border="1">';
      $thePoints = 0;
      foreach($answers as $answer):
         $setCorrect = $answer->correct;
         $theTD      = '<td style="color: white;"><span style="text-align: left;font-size: 8pt; line-height: 8pt;">NN</span></td>';
         $theAnswer  = '';
         JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
         $r    = JTable::getInstance('Quizz_results','JclassroomTable',array());
         $load = array(
            'theResultID'  => $rID,
            'questionID'   => $questionID,
            'answerID'     => $answer->id
         );
         $check = $r->load($load);
         if($check):
            $bgI = '';
            if($calculate == 2):
               if($answer->correct == 1 && $r):
                  $bgI = 'background-color: forestgreen';
                  $thePoints = $quest->points;
               else:
                  $bgI = 'background-color: red';
               endif;
            endif;
            $theTD = '<td style="'.$bgI.';"><span class="" style="font-size: 8pt;line-height: 8pt;text-align: left;">'.$r->content.' '.$theID.'</span></td>';
         else:

         endif;
         $html .= '<tr>';
         $html .= $theTD;
         $html .= '</tr>';
      endforeach;
      $html .= '</table>';
      $html .= '</td>';
      if($correctSoll > $correctIst):
         //$thePoints = ($correctIst * $points / $correctSoll);
      endif;
      if($calculate == 2):
         $html .= '<td width="'.$w3.'" style="text-align: center;"><span>'.$thePoints.'</span>/<span>'.$quest->points.'</span></td>';
      endif;
      $return = array('html' => $html, 'points' => $thePoints);
      return $return;
   }
   function loadType13PDFComplete($rID, $questionID, $quizzID, $calculate, $fromLR, $w1, $w2, $w3, $classroomID) {
      $cCount     = 0;
      $answersIS  = 0;
      $db         = JFactory::getDbo();
      $query      = $db->getQuery(true);
      $query->select(array('
         a.id,
         a.title
      '));
      $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
      $query->where($db->quotename('a.questionID').' = '.$db->quote($questionID));
      $db->setQuery($query);
      $answersSOLL = $db->loadObjectList();
      foreach($answersSOLL as $answer):
         $labels[]   = strip_tags($answer->title);
         $query = $db->getQuery(true);
         $query->select(array('
            count(a.id) as data
         '));
         $query->from($db->quoteName('#__jclassroom_theresults','a'));
         $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
         $query->join('INNER', $db->quoteName('#__jclassroom_classroom_students', 'c') . ' ON (' . $db->quoteName('a.created_by') . ' = ' . $db->quoteName('c.userID') . ') AND (' . $db->quoteName('a.classroomID') . ' = ' . $db->quoteName('c.classroomID') . ')');
         $query->where($db->quotename('b.questionID').' = '.$db->quote($questionID));
         $query->where($db->quotename('b.answerID').' = '.$db->quote($answer->id));
         $query->where($db->quotename('a.classroomID').' = '.$db->quote($classroomID));
         $query->order('a.created DESC');
         $query->setLimit(1);
         $db->setQuery($query);
         $answersIS = $db->loadResult();
         $datas[]    = $answersIS;
         $cCount  += $answersIS;
      endforeach;
      $theQuestion = array(
         'questionID'=> $question->questionID,
         'question'  => $question->title,
         'content'   => $question->content,
         'chart'     => $question->chart,
         'labels'    => $labels,
         'datas'     => $datas,
         'indiAnswers'  => $indiAnswers,
         'cCount'    => $cCount,
         'type'      => $type
      );
      return $theQuestion;
   }
   function getAuditResult($quizz, $theResultID) {
      echo $quizz->id;
      $html = '';
      $theScoreMax   = 0;
      $theScorePoints = 0;
      // LOAD the textfields
      $db      = JFactory::getDbo();
      $query   = $db->getQuery(true);
      $query->select(array('a.title'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
      $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizz->id));
      $query->where($db->quoteName('a.type').' = 14');
      $query->group('a.testfield');
      $query->order('a.groupOrdering ASC');
      $db->setQuery($query);
      $testfields    = $db->loadObjectList();
      echo '<pre>';
      print_r($testfields);
      echo '</pre>';
      if($testfields):
         foreach($testfields as $testfield):
            $html .= '<h1 style="font-size: 24px;"><span class="badge bg-success text-white mr-1">'.$testfield->title.'</span></h1>';
            $html .= $testfield->content;
            $query   = $db->getQuery(true);
            $query->select(array('a.*'));
              $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
            $query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizz->id));
            $query->where($db->quoteName('a.testfield').' = '.$db->quote($testfield->title));
            $query->where($db->quoteName('a.type').' <> '.$db->quote(14));
            //$query->group('a.theme');
            $db->setQuery($query);
            $questions  = $db->loadObjectList();
            if($questions):
               $html .= '<table class="table table-striped">';
               $html .= '<thead>';
               $html .= '<tr>';
               $html .= '<th>Frage</th>';
               if($quizz->type != 1 && $quizz->calculate == 2):
                  $html .= '<th>Richtige Antwort</th>';
               endif;
               $html .= '<th>Ihre Antwort</th>';
               if($quizz->calculate == 2):
                  $html .= '<th>Punkte</th>';
               endif;
               $html .= '</tr>';
               $html .= '</thead>';
               $html .= '<tbody>';
               $maxPoints  = 0;
               $points  = 0;
               foreach($questions as $question):
                  $maxPoints += $question->points;
                  $theScoreMax += $question->points;
                  $html .= '<tr>';
                  //Get the Question
                  JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
                  $result  = JTable::getInstance('Quizz_results','JclassroomTable',array());
                  $load       = array('theResultID' => $theResultID,'questionID' => $question->id);
                  $result->load($load);
                  $html .= '<td><h5 style="font-size: 16px;"><b>'.$question->title.'</b></h5>'.$question->content.'<h2 style="font-size: 18px;"><span class="badge bg-primary text-white">'.$question->theme.'</span></h2></td>';
                  if($result->content == "A"):
                     $html .= '<td><span class="card bg-success p-1 text-white">'.$question->infotextPositiv.'</span></td>';
                     $html .= '<td class="text-center">'.$question->points.'</td>';
                     $points += $question->points;
                  endif;
                  if($result->content == "AA"):
                     $html .= '<td>';
                     $html .= '<span class="card bg-warning p-1 text-dark">'.$question->infotextPositiv.'</span>';
                     $html .= '<small style="font-weight: bold;font-style: italic;">'.$result->exeptionText.'</small>';
                     $html .= '</td>';
                     if($result->exeptionPoints != 0):
                        $thePoints = $result->exeptionPoints;
                     else:
                        $thePoints = $question->points;
                     endif; 
                     $html .= '<td class="text-center">'.$thePoints.'</td>';
                     $points += $thePoints;
                  endif;
                  if($result->content == "N"):
                     $html .= '<td><span class="card bg-danger p-1 text-white">'.$question->infotextNegativ.'</span></td>';
                     $html .= '<td class="text-center">0</td>';
                     $points += 0;
                  endif;
                  $html .'</tr>';
               endforeach;
               $html .= '</tbody>';
               if($quizz->calculate == 2):
                  $theScorePoints += $points;
                  $html .= '<tfoot>';
                  $html .= '<tr>';
                  $html .= '<td><b>Ihre erreicht Punktzahl im Thema '.$question->theme.' (von maximal '.$maxPoints.')</b></td>';
                  if($quizz->type != 1 && $quizz->calculate == 2):
                     $html .= '<td></td>';
                     $html .= '<td></td>';
                  else:
                     $html .= '<td></td>';
                  endif;
                  $html .= '<td class="text-center"><b>'.$points.'</b></td>';
                  $html .= '</tr>';
                  $html .= '<tr>';
                  $html .= '<td><b>Dies entspricht einem Score von</b></td>';
                  if($quizz->type != 1 && $quizz->calculate == 2):
                     $html .= '<td></td>';
                     $html .= '<td></td>';
                  else:
                     $html .= '<td></td>';
                  endif;
                  $score = $points * 100/ $maxPoints;
                  $html .= '<td class="text-center"><b>'.round($score,2).'%</b></td>';
                  $html .= '</tr>';
                  $html .= '</tfoot>';
               endif;
               $html .= '</table>';
            endif;
         endforeach;
      endif;
      if($quizz->calculate == 2):
         $html .= '<table class="table table-striped">';
            $html .= '<tfoot>';
            $html .= '<tr>';
            $html .= '<td><b>Gesamtscore</b></td>';f;
            $score = $theScorePoints * 100/ $theScoreMax;
            $html .= '<td class="text-right"><b style="font-size: 32px;" class="badge bg-success text-white">'.round($score,2).'%</b></td>';
            $html .= '</tr>';
            $html .= '</tfoot>';
         $html .= '</table>';
      endif;

      return $html;
   }
}