<?php
defined('_JEXEC') or die;
if (!class_exists('TCPDF')) {
	require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
	require_once JPATH_SITE . '/components/com_jclassroom/models/stage.php';
}
class printCertificate {
	
	public function certificate($rID, $save, $quizzID, $publishedQuizzID) {
		$stage = JModelLegacy::getInstance('stage', 'JclassroomModel');
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('ce - corporate education GmbH');
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		// set margins
		$pdf->SetMargins(10, 50, 10,true);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->SetFont('helvetica', 'R', 12, '', false);
		$pdf->AddPage();
      $html    = '';
      $html    .= $pdf->getIntrotext($quizzID, $publishedQuizzID, $rID);
      JLoader::register('StageResultHelper',JPATH_COMPONENT_SITE.'/helpers/stageResult.php');
      $result     = new StageResultHelper(); 
      $html    .= $result->getResult($rID, 1, 0, 0, 0, 0, $quizzID, 1);
		$pdf->writeHTMLCell(190, '', 10, 40, $html, 0, 0, 0, true, 'L', true);
      if($save == 1):
         $pdf->Output(JPATH_BASE.'/images/evaluates/Evaluation'.$rID.'.pdf', 'F');
      else:
         ob_end_clean();
         $pdf->Output('Evaluation'.$rID.'.pdf', 'I');
      endif;
	}
}

class MYPDF extends TCPDF {

   function getIntrotext($quizzID, $publishedQuizzID, $rID) {
      $text = file_get_contents(JURI::Root().'images/jclassroom/configuration/evaluation_introtext.txt');
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $quizz   = JTable::getInstance('Unit','JclassroomTable',array());
      $quizz->load($quizzID);
      $quizzID    = 'UID'.str_pad($quizz->id,8,'0',STR_PAD_LEFT);
      $resultID   = 'RID'.str_pad($rID,8,'0',STR_PAD_LEFT);
      $quizzName  = $quizz->title;
      $text = str_replace('{quizzID}',$quizzID, $text);
      $text = str_replace('{quizzname}',$quizzName, $text);
      $text = str_replace('{rID}',$resultID, $text);
      return $text;
   }
   function getSystemName() {
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $table      = JTable::getInstance('Configuration','JclassroomTable',array());
      // Load the systemdata
      $load       = array('customerID' => 0, 'parameter' => 'systemName');
      $table->load($load);
      $systemName = $table->wert;
      return $systemName;
   }
   function getSystemLogo() {
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $table      = JTable::getInstance('Configuration','JclassroomTable',array());
      // Load the systemdata
      $load       = array('customerID' => 0, 'parameter' => 'systemLogo');
      $table->load($load);
      $logo = $table->wert;
      return $logo;
   }
   public function Header() {
      $image_file = JPATH_SITE.$this->getSystemLogo();
      list($width, $height, $type) = getimagesize($image_file);
      // calculate the logo-size
      $referenceH = 16;
      $ratio      = $height / $referenceH;
      $calcW      = $width / $ratio;
      switch($type):
         case 1:
            $calcT = 'GIF';
            break;
         case 2: 
            $calcT = 'JPG';
            break;
         case 3: 
            $calcT = 'PNG';
            break;
      endswitch;
      $this->Image($image_file, 10, 10, $calcW, 16, $calcT, '', 'T', true, 300, '', false, false, 0, false, false, false);
      $this->writeHTMLCell(90, 20, 110,9, '<h1 style="font-size: 38pt;">Auswertung</h1>', 0, 0, 0, 0, 'R', true, true, 0, false, true, 30, 'M', true);
   }  
	public function Footer() {
		// Load Configs
      JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
      $table      = JTable::getInstance('Configuration','JclassroomTable',array());
      // Load the systemdata
      $load       = array('customerID' => 0, 'parameter' => 'systemName');
      $table->load($load);
      $systemName = $table->wert;

      $this->SetXY(10,-15);
      // Set font
      $this->SetFont('helvetica', '', 6);
      // Page number
      $this->MultiCell(50, 5, $systemName.' (c) '.date('Y'), '0', 'L', 0, 1, '10', '285', true);
      $this->MultiCell(50, 5, 'Druck am: '.date('d.m.Y H:i'), '', 'L', 0, 1, '10', '288', true);
      $this->MultiCell(50, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), '0', 'R', 0, 1, '160', '285', true);
	}
}

