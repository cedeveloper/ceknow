<?php
defined('_JEXEC') or die;
if (!class_exists('TCPDF')) {
	require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
	require_once JPATH_SITE . '/components/com_jclassroom/models/stage.php';
}
class printList {
	
	public function result($header, $body) {
		$input 		= JFactory::getApplication()->input;
		$resultID 	= $input->get('rID',0,'INT');
		$stage = JModelLegacy::getInstance('stage', 'JclassroomModel');
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('dats | Druckagentur Torsten Scheel');
		$pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE,PDF_HEADER_STRING);
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		// set margins
		$pdf->SetMargins(10, 50, 10,true);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->SetFont('helvetica', 'R', 8, '', false);
		$pdf->AddPage();
		//$html = $stage->getResult($resultID);
		$pdf->writeHTML($header, true, false, true, false, '');
      $pdf->writeHTML($body, true, false, true, false, '');
		//ob_end_clean();
		$pdf->Output(JPATH_BASE.'/images/learningrooms/LR77/Auswertung.pdf', 'F');
	}
}

class MYPDF extends TCPDF {

	public function Header() {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'logoCustomer');
		$result->load($load);
		$path = JPATH_SITE.'/'.$result->wert;
		$this->Image($path, 10, 10, 56, 18, 'PNG','' , '', true, 300, '', false, false, 0, false, false, false);
		$this->writeHTMLCell(90, 20, 110,9, '<h1 style="font-size: 38pt;">Auswertung</h1>', 0, 0, 0, 0, 'R', true, true, 0, false, true, 30, 'M', true);
	}
	public function Footer() {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Configuration','JclassroomTable',array());
		$load 	= array('parameter' => 'systemName');
		$result->load($load);
		$now = JFactory::getDate();
		$date = new JDate($now);
		$now = $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$datumSTR = $now->format('d.m.Y');
		$datum = explode(' ',$now);
		$this->SetXY(10,-15);
		// Set font
		$this->SetFont('helvetica', '', 7);
		// Page number
		$this->Cell(0, 10, $mandant, 0, false, 'L', 0, '', 0, false, 'T', 'M');
		$this->MultiCell(50, 5, $result->wert.' (c) '.date('Y'), '0', 'L', 0, 1, '10', '285', true);
		$this->MultiCell(50, 5, 'Druck am: '.$datumSTR.' '.$datum[1], '', 'L', 0, 1, '10', '288', true);
		$this->MultiCell(50, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), '0', 'R', 0, 1, '160', '288', true);
	}
}

