<?php
defined('_JEXEC') or die;

if (!class_exists('TCPDF')) {
	require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
}
$aktSite = 0;

class printQuizzEngine {
	
	public function printQuizz($rID, $save, $fromLR, $completeEvaluation, $classroomID, $publishedQuizzID, $unitID) {
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('ce - corporate education GmbH');
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		
		// set margins
		$pdf->SetFillColor(255,255,255);
		$pdf->SetDrawColor(0,0,0);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 18);
		$pdf->SetMargins(10,35,10,true);
		$pdf->SetFont('helvetica', 'B', 12, '', false);
		$pdf->AddPage();
		$aktSite = $pdf->getAliasNumPage();
		if($aktSite = 1) {
			//$pdf->getFooterFirst($rID);
			//$pdf->getAuditiert($ad);	
		}
		
		$pdf->SetFont('helvetica', 'R', 48, '', false);
		$pdf->SetTextColor(120,120,120);
		$pdf->Bookmark('Titel', 0, 0,'', '', array(0,0,0));
		$pdf->MultiCell(190, 25,'Quizzreport', '', 'L', 0, 1, 10, 80, true,0,false,false,25,'M',false);
		$data = $pdf->loadTheResult($rID, $fromLR, $completeEvaluation, $classroomID, $unitID, $publishedQuizzID);
		$pdf->SetFont('helvetica', 'R', 14, '', false);
		$y = $pdf->getY();
		if($classroomID && $classroomID != 0):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$LR 		= JTable::getInstance('Classroom','JclassroomTable',array());
			$LR->load($classroomID);
			$pdf->MultiCell(190, 15,'LearningroomID: LR'. str_pad($classroomID,8,'0', STR_PAD_LEFT).' '.$LR->title, '', 'L', 0, 1, 10, $y, true,0,false,false,25,'M',false);
			$y = $pdf->getY();
		endif;
		$pdf->MultiCell(190, 15,$data['quizzID'].' '.$data['quizzName'], '', 'L', 0, 1, 10, $y, true,0,false,false,25,'M',false);
		$y = $pdf->getY();
		$html1 = '';
		if($data['companyName']):
			$html1 .= 'Kunde: '.$data['companyName'];
		endif;
		if($data['userID'] != 0):
			$html1 .= '<br/>Teilnehmer: '.$data['userName'];
		else:
			$html1 .= '<br/>Anzahl Teilnehmer: '.$data['participations'];
		endif;
		if($completeEvaluation == 1):
			$html1 .= '<br/><span style="font-size: 18pt;font-weight: bold;color: #ff3600;">Gesamtauswertung</span>';
		else:
			$html1 .= '<br/><span style="font-size: 18pt;font-weight: bold;color: #ff3600;">Einzelauswertung</span>';
		endif;
		$pdf->writeHTMLCell(190, 0, 10, $y, $html1, 0, 0, 0, true, 'L', true);
		$pdf->AddPage();
		if($data['preface']):
	      	JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$quizz 		= JTable::getInstance('Quizz','JclassroomTable',array());
			$quizz->load($publishedQuizzID);
			//$IV = $pdf->getVorwort($rID);
			$preface = '<h2>Vorwort</h2>';
			$preface .= $quizz->preface;
			$pdf->writeHTMLCell(190, 0, 10, 35, $preface, 0, 0, 0, true, 'L', true);
			$pdf->AddPage();
		endif;

		//$pdf->Bookmark('2. Inhaltsverzeichnis', 0, 0, 2, 2, array(0,0,0));
      	//$pdf->Bookmark('Quizzergebnis', 0, 0, '', '', array(128,0,0));
      	JLoader::register('StageResultHelper',JPATH_COMPONENT_SITE.'/helpers/stageResult.php');
      	$result     = new StageResultHelper();
      	$html       = $result->getResult($rID, $fromLR, 0, $completeEvaluation, $classroomID, $publishedQuizzID, $unitID, 1);
      	$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
      	//$pdf->writeHTMLCell(190, '', 10, 40, $html, 0, 0, 0, true, 'L', true);
		//$pdf->getErgebnis($rID, $fromLR, $data['userID'],$completeEvaluation, $classroomID);
		$pdf->AddPage();
		//$pdf->Bookmark('Kontakt', 0, 0, '', '', array(128,0,0));
		$html = '';
		$html .= '<p style="line-height: 10pt;text-align: center;"><strong>'.$pdf->getSystemName().'</strong> ist ein Produkt der <span style="color: #ff3600;"><strong>ce</strong></span></p>';
		$html .= '<img style="width: 20px;text-align: center;" src="/images/flag-german.png" alt="flag german" />';
		$html .= '<p></p>';
		$html .= '<p style="text-align: center;">Made &amp; Hosted in Germany</p>';
		$html .= '<p style="font-size: 12pt;line-height: 280px;"></p>';
		$html .= '<span style="font-size: 10pt;"><b>ce - corporate education GmH</b><br/>';
		$html .= 'Bonner Str. 178<br/>';
		$html .= '50968 Köln<br/><br/>';
		$html .= 'FON: +49 221 | 29 21 16 - 0<br/>';
		$html .= 'MAIL: info@ce.de<br/>';
		$html .= 'WEB: www.ce.de</span>';
		$pdf->writeHTMLCell(170, '', 20, 80, $html, 0, 1, 1, true, 'J', true);
      	//$pdf->Bookmark('5. Maßnahmenkatalog', 0, 0, '', '', array(128,0,0));
		//$pdf->getMassnahmen($ad);
      	//$pdf->getInhaltV($rID, 2);
		
		if($save == 1):
			$pdf->Output(JPATH_BASE.'/images/evaluates/Quizzreport'.$rID.'.pdf', 'F');
		else:
			ob_end_clean();
			$pdf->Output('Quizzreport_'.$rID.'.pdf', 'I');
		endif;
	}
}
class MYPDF extends TCPDF {
	function getSystemName() {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemName');
		$table->load($load);
		$systemName = $table->wert;
		return $systemName;
	}
	function getSystemLogo() {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemLogo');
		$table->load($load);
		$logo = $table->wert;
		return $logo;
	}
	public function Header() {
		$image_file = JPATH_SITE.$this->getSystemLogo();
		list($width, $height, $type) = getimagesize($image_file);
		// calculate the logo-size
		$referenceH = 16;
		$ratio 		= $height / $referenceH;
		$calcW 		= $width / $ratio;
		switch($type):
			case 1:
				$calcT = 'GIF';
				break;
			case 2: 
				$calcT = 'JPG';
				break;
			case 3: 
				$calcT = 'PNG';
				break;
		endswitch;
		$this->Image($image_file, 10, 10, $calcW, $referenceH, $calcT, '', 'T', true, 300, '', false, false, 0, false, false, false);
	}	
	public function getFooterFirst($rID) {
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$audit 	= JTable::getInstance('Theresults','JclassroomTable',array());
		$audit->load($rID);
		$createdBy 		= $audit->created_by;
		$publishedQID 	= $audit->publishedQuizzID;
		//Load the Quizz-Data
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quizz 	= JTable::getInstance('Quizz','JclassroomTable',array());
		$quizz->load($publishedQID);
		$quizzID 	= $quizz->id;
		$quizzName 	= $quizz->title;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('User','JclassroomTable',array());
		$table->load($user->id);
		$auditCreator = $table->name;
		//GET THE AUDIT
		$this->SetTextColor(0,0,0);
		//$this->SetFont('helvetica', '', 12);
		//$this->MultiCell(170, 6, "Dieser Quizzreport wurde erstellt von: ".$user->name, '0', 'L', 0, 1, '10', '262', true);
		$this->SetFont('helvetica', '', 10);
		$this->MultiCell(170, 3, date('d.m.Y H:i'), '0', 'L', 0, 1, '10', '268', true);
	}
	public function Footer() {
		// Load Configs
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 		= JTable::getInstance('Configuration','JclassroomTable',array());
		// Load the systemdata
		$load 		= array('customerID' => 0, 'parameter' => 'systemName');
		$table->load($load);
		$systemName = $table->wert;

		$this->SetXY(10,-15);
		// Set font
		$this->SetFont('helvetica', '', 6);
		// Page number
      	$page = $this->PageNo();
      	if($page >= 1) {
			$this->MultiCell(50, 5, $systemName.' (c) '.date('Y'), '0', 'L', 0, 1, '10', '285', true);
			$this->MultiCell(50, 5, 'Druck am: '.date('d.m.Y H:i'), '', 'L', 0, 1, '10', '288', true);
			$this->MultiCell(50, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), '0', 'R', 0, 1, '160', '285', true);
       	}
	}
	function loadTheResult($rID, $fromLR, $completeEvaluation, $classroomID, $unitID, $publishedQuizzID) {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$audit 	= JTable::getInstance('Theresults','JclassroomTable',array());
		if($fromLR == 1 && $completeEvaluation == 0):
			$audit->load($rID);
			$userID = $audit->created_by;
		endif;
		if($fromLR == 1 && $completeEvaluation == 1):
			$load = array('classroomID' => $classroomID,'quizzID' => $rID);
			$audit->load($load);
			$userID 		= 0;
		endif;
		if($fromLR == 0):
			$load = array('publishedQuizzID' => $publishedQuizzID);
			$audit->load($load);
			$userID 		= 0;
		endif;
		$createdBy 		= $audit->created_by;
		$publishedQID 	= $audit->publishedQuizzID;
		
		//Load the Quizz-Data
		
		if($fromLR == 1):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$quizz 	= JTable::getInstance('Unit','JclassroomTable',array());
			$quizz->load($unitID);
			$quizzID 	= 'UID'.str_pad($quizz->id,8,'0',STR_PAD_LEFT);
			$quizzName 	= $quizz->title;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$cl 	= JTable::getInstance('Classroom','JclassroomTable',array());
			$cl->load($classroomID);
			$companyID = $cl->companyID;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$company 		= JTable::getInstance('Company','JclassroomTable',array());
			$company->load($companyID);
			$companyName 	= $company->name;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$user 		= JTable::getInstance('User','JclassroomTable',array());
			$user->load($userID);
			$participations = 0;
			if($completeEvaluation == 1):
				$db 	= JFactory::getDbo();
				$query 	= $db->getQuery(true);
				$query->select(array('count(a.id) as participations'));
		        $query->from($db->quoteName('#__jclassroom_theresults','a'));
				$query->where($db->quoteName('a.classroomID').' = '.$db->quote($classroomID));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($unitID));
				$db->setQuery($query);
				$participations = $db->loadResult();
			endif;
			$return = array(
				'quizzID' => $quizzID, 
				'quizzName' => $quizzName,
				'companyName' => $companyName,
				'preface' 	=> '',
				'userID' 	=> $userID,
				'userName' 	=> $user->name,
				'participations' => $participations
			);
		endif;
		if($fromLR == 0):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$quizz 	= JTable::getInstance('Quizz','JclassroomTable',array());
			$quizz->load($publishedQID);
			$quizzID 	= 'QID'.str_pad($quizz->id,8,'0',STR_PAD_LEFT);
			$quizzName 	= $quizz->title;
			$preface 	= $quizz->preface;
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$company 		= JTable::getInstance('Company','JclassroomTable',array());
			$company->load($quizz->companyID);
			$companyName 	= $company->name;
			$participations = 0;
			if($completeEvaluation == 1):
				$db 	= JFactory::getDbo();
				$query 	= $db->getQuery(true);
				$query->select(array('count(a.id) as participations'));
		        $query->from($db->quoteName('#__jclassroom_theresults','a'));
				$query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
				$db->setQuery($query);
				$participations = $db->loadResult();
			endif;
			$return = array(
				'quizzID' => $quizzID, 
				'quizzName' => $quizzName,
				'companyName' => $companyName,
				'preface' 	=> $preface,
				'userID' 	=> 0,
				'userName' 	=> $fromLR,
				'participations' => $participations
			);
		endif;
		
		return $return;
	}
	function getResult($rID, $fromLR, $userID, $completeEvaluation, $classroomID) {
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		// Load the result of the quizz
		$query 		= $db->getQuery(true);
		$query->select(array('
			COUNT(a.id) as participations,
			a.*, 
			b.*
		'));
        $query->from($db->quoteName('#__jclassroom_theresults','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
        if($fromLR == 1 && $completeEvaluation == 0):
        	$query->where($db->quoteName('a.id').' = '.$db->quote($rID));
        endif;
        if($fromLR == 1 && $completeEvaluation == 1):
        	$query->where($db->quoteName('a.classroomID').' = '.$db->quote($classroomID));
        	$query->where($db->quoteName('a.quizzID').' = '.$db->quote($rID));
        endif;
        if($fromLR == 0):
			$query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($rID));
		endif;
		$db->setQuery($query);
		$questions 	= $db->loadObjectList();
		$quizzID 	= $questions[0]->quizzID;
		$theResultID= $questions[0]->theResultID;
		if($fromLR == 1):
			$html .= $this->getQuizzResult($quizzID, $rID, $fromLR, $userID, $completeEvaluation, $classroomID);
		else:
			//Load the quizz
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$quizz 		= JTable::getInstance('Quizz','JclassroomTable',array());
			$quizz->load($rID);
			$html 		= false;
			if($quizz->quizzType == 1):
				if($questions):
					$html .= $this->getQuizzResult($quizzID, $rID, $fromLR, $userID, $completeEvaluation, $classroomID);
				endif;
			endif;
			/*if($quizz->quizzType == 2):
				$html .= '<h2>Auswertung für Umfrage</h2>';
				if($questions):
					$html .= $this->getUmfrageResult($quizzID, $rID);
				endif;
			endif;
			if($quizz->quizzType == 3):
				$html .= '<p>Auswertung für Audit</p>';
				if($questions):
					$html .= $this->getAuditResult($quizzID, $rID);
				endif;
			endif;*/
		endif;

		
		return $html;
	}
	function getQuizzResult($quizzID, $publishedQuizzID, $fromLR, $userID, $completeEvaluation, $classroomID) {
		$session 	= JFactory::getSession();
		$cart 		= $session->get('quizzes');
		// Load the quizz
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quizz 		= JTable::getInstance('Unit','JclassroomTable',array());
		$quizz->load($quizzID);
		$counter 	= 1;
		$points 	= 0;
		$maxPoints 	= 0;

		$html = '';
		$html .= '<table border="1" cellpadding="2">';
		$html .= '<thead>';
		$html .= '<tr>';
		if($fromLR == 1 && $completeEvaluation == 0):
			$html .= '<th style="background-color: lightsteelblue;" width="160px">Frage</th>';
			$html .= '<th style="background-color: lightsteelblue;" width="160px">Antwortmöglichkeiten</th>';
			$html .= '<th style="background-color: lightsteelblue;" width="190px" class="text-center">Ihre Antworten</th>';
		endif;
		if($fromLR == 1 && $completeEvaluation == 1):
			$html .= '<th style="background-color: lightsteelblue;" width="200px">Frage</th>';
			$html .= '<th style="background-color: lightsteelblue;" width="310px" class="text-center">Diagramm</th>';
		endif;
		if($fromLR == 0):
			$html .= '<th style="background-color: lightsteelblue;" width="200px">Frage</th>';
			$html .= '<th style="background-color: lightsteelblue;" width="110px"></th>';
			$html .= '<th style="background-color: lightsteelblue;" width="200px" class="text-center">Diagramm</th>';
		endif;
		$html .= '</tr>';
		$html .= '</thead>';
		//Load the quizzPositions 
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->where($db->quoteName('a.type').' <> '.$db->quote(1));
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$quizzPositions 	= $db->loadObjectList();
		if($quizzPositions):
			$html .= '<tbody>';
			$i = 1;
			foreach($quizzPositions as $quizzPosition):
				$this->Bookmark('3.'.$i.' '.$quizzPosition->title, 1, 0, '', '', array(128,0,0));
				//$this->MultiCell(180, 5, '3.'.$i.' '.$quizzPosition->title, '0', 'L', 0, 1, '10', '10', true);
				if($fromLR == 1 && $completeEvaluation == 0):
					$theBG = 'style="background-color: lightskyblue;"';
					$colspan 	= 3;
					$w1 		= 510;
					$html .= '<tr nobr="true" height="40px">';
					$html .= '<td style="text-align: left;" width="160px" '.$theBG.' colspan="'.$colspan.'"><h5 style="text-align: left;"><b>'.$counter.'. '.$quizzPosition->id.' '.$quizzPosition->title.'</b></h5></td>';
					switch($quizzPosition->type):
						case '3':
							// WWM-Fragen
							//$return = $this->loadType3($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate, $fromLR);
							$html .= $return['html'];
							$points += $return['points'];
							break;
						case '5':
							// Ja/Nein-Fragen
							//$return = $this->loadType5($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR);
							$html .= $return['html'];
							$points += $return['points'];
							break;
						case '10':
							// Textfeld-Fragen
							$return = $this->loadType10($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR, $completeEvaluation, $classroomID);
							$html .= $return['html'];
							$points += $return['points'];
							break;
						case '11':
							// Textarea-Fragen
							$return = $this->loadType11($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR, $completeEvaluation, $classroomID);
							$html .= $return['html'];
							$points += $return['points'];
							break;
						case '12':
							// Auswahlliste-Fragen
							$return = $this->loadType12($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR);
							$html .= $return['html'];
							$points += $return['points'];
							break;
						case '13':
							// Checkbox-Fragen
							$return = $this->loadType13($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR);
							$html .= $return['html'];
							$points += $return['points'];
							break;
					endswitch;
				endif;
				// Render if report is called by LR and its called for all students
				if($fromLR == 1 && $completeEvaluation == 1):
					$colspan 	= 1;
					$theBG 		= '';
					if($quizzPosition->type == 14):
						$theBG = 'style="background-color: lightskyblue;"';
						$colspan 	= 3;
						$w1 		= 510;
						$html .= '<tr height="40px">';
						$html .= '<td width="'.$w1.'px" '.$theBG.' colspan="'.$colspan.'"><h5><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
						$position 	= false;
						$path 		= false;
					endif;
					if($quizzPosition->type != 14):
						$theBG 	 	= 'style="background-color: white;"';
						$colspan 	= 1;
						$w1 		= 200;
						$html .= '<tr nobr="true">';
						$html .= '<td width="'.$w1.'px" '.$theBG.' colspan="'.$colspan.'"><p style="text-align: left;font-size: 7pt;line-height: 8pt;"><b>'.$counter.'. '.$quizzPosition->title.'</b></p></td>';
						$position 	= $quizzPosition->id;
						$path 		= '/images/evaluates/LR'.$classroomID.'unit'.$quizzID.'/theModalChart'.$position.'.png';
						if(($quizzPosition->type != 10 && $quizzPosition->type != 11 && $quizzPosition->type != 14) && $path):
							$html .= '<td width="310px"><br/><br/><img src="'.$path.'" /><br/></td>';
						else:
							switch($quizzPosition->type):
								case '10':
									// Textfeld-Fragen
									$return = $this->loadType10($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR, $completeEvaluation, $classroomID);
									$html .= $return['html'];
									$points += $return['points'];
									break;
								case '11':
									// Textarea-Fragen
									$return = $this->loadType11($quizzPosition->id, $publishedQuizzID, $quizz->calculate, $fromLR, $completeEvaluation, $classroomID);
									$html .= $return['html'];
									$points += $return['points'];
									break;
							endswitch;
						endif;
						
					endif;
				endif;
				if($fromLR == 0):
					$colspan 	= 1;
					$theBG 		= '';
					if($quizzPosition->type == 14):
						$theBG = 'style="background-color: lightskyblue;"';
						$colspan 	= 3;
						$w1 		= 510;
						$html .= '<tr height="40px">';
						$html .= '<td width="'.$w1.'px" '.$theBG.' colspan="'.$colspan.'"><h5><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
						$position 	= false;
						$path 		= false;
					endif;
					if($quizzPosition->type != 14):
						$theBG 	 	= 'style="background-color: white;"';
						$colspan 	= 1;
						$w1 		= 200;
						$html .= '<tr nobr="true">';
						$html .= '<td width="'.$w1.'px" '.$theBG.' colspan="'.$colspan.'"><p style="text-align: left;font-size: 7pt;line-height: 8pt;"><b>'.$counter.'. '.$quizzPosition->title.'</b></p></td>';
						$html .= '<td width="110px"></td>';
						$position 	= $quizzPosition->id;
						$path 		= '/images/evaluates/eva'.$publishedQuizzID.'/quizzID_'.$position.'.png';
						if($quizzPosition->type != 14 && $path):
							$html .= '<td width="200px"><br/><br/><img src="'.$path.'" /><br/></td>';
						else:
							$html .= '<td width="200px"></td>';
						endif;
						
					endif;
				endif;
				/*$maxPoints += $quizzPosition->points;*/
				
				$html .= '</tr>';
				$counter++;
				$i++;
			endforeach;
			$html .= '</tbody>';
		endif;
		/*$html .= '<tfoot>';
		$html .= '<tr>';
		$html .= '<td><b>Erreichte Gesamtpunktzahl</b></td>';
		$html .= '<td colspan="2"></td>';
		$html .= '<td style="text-align: center;"><b><span style="color: forestgreen;">'.$points.'</span>/<span style="color: steelblue;">'.$maxPoints.'</span></b></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td><b>Dies entspricht einem Score von</b></td>';
		$html .= '<td colspan="2"></td>';
		$score = 0;
		if($points && $maxPoints):
			$score = round($points * 100 / $maxPoints);
		endif;
		$html .= '<td style="text-align: center;"><b>'.$score.'%</b></td>';
		$html .= '</tr>';
		$html .= '</tfoot>';*/
		$html .= '</table>';
		return $html;
	}
	// for WWM-Fragen
	function loadType3($questionID, $cart, $points, $calculate, $fromLR) {

		$html = '';
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$answers = $db->loadObjectList();
		//Count correct answers
		$query 	= $db->getQuery(true);
		$query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->where($db->quoteName('a.correct').' = '.$db->quote(1));
		$db->setQuery($query);
		$correctSoll 	= $db->loadResult();
		$correctIst 	= 0;
		if($answers):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			foreach($answers as $answer):
				if($answer->correct == 1):
					$bgS = 'bg-success';
				else:
					$bgS = 'bg-danger';
				endif;
				$html .= '<tr>';
				$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$answer->title.' '.$answer->id.'</span></td>';
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		//if cart is not empty
		if($cart):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			// 
			foreach($answers as $answer):
				$setCorrect = $answer->correct;
				$theTD 		= '<td style="height: 27px;"></td>';
				$theAnswer 	= '';
				foreach($cart as $part):
					$eparts 	= $part['content'];
					if($eparts):
						foreach($eparts as $epart):
							$EPS = explode('_', $epart);
							if($EPS[0] == $answer->id):
								if($answer->correct == 1):
									$bgS = 'bg-success';
									$correctIst++;
									$thePoints += $points;
								else:	
									$bgS = 'bg-danger';
								endif;
								$theTD = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$EPS[1].' '.$theID.'</span></td>';
							endif;
						endforeach;
					endif;
				endforeach;
				$html .= '<tr>';
				$html .= $theTD;
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
			if($correctSoll > $correctIst):
				$thePoints = ($correctIst * $points / $correctSoll);
			endif;
			$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Ja/Nein-Fragen
	function loadType5($questionID, $publishedQuizzID, $calculate, $fromLR) {
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('
			COUNT(a.id ) as countAnswers,
			a.content
		'));
        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
		$query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->group('a.content');
		$db->setQuery($query);
		$quizzPositions 	= $db->loadObjectList();

		$thePoints = 0;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quest 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$quest->load($questionID);
		if($quest->correctAnswers == 0):
			$bgsJ = 'bg-success';
			$bgsN = 'bg-danger';
		else:
			$bgsJ = 'bg-danger';
			$bgsN = 'bg-success';
		endif;
		$html = '';
		$db 	= JFactory::getDbo();
		$html .= '<td style="padding:5px;">';
		$html .= '<table style="width: 100%;">';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '</td>';
		//if cart is not empty
		if($quizzPositions):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			foreach($quizzPositions as $part):
				if($part->content == 'answerJ'):
					$ATJ = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">'.$part->countAnswers.' x Ja</span></td>';
				endif;
				if($part->content == 'answerN'):
					$ATN = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">'.$part->countAnswers.' x Nein</span></td>';
				endif;
			endforeach;
			$html .= '<tr>';
			$html .= $ATJ;
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= $ATN;
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '</td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Textfeld-Fragen
	function loadType10($questionID, $publishedQuizzID, $calculate, $fromLR, $completeEvaluation, $classroomID) {
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('
			COUNT(a.id ) as countAnswers,
			a.content
		'));
        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
        if($fromLR == 1 && $completeEvaluation == 0):
        	$query->where($db->quoteName('a.theResultID').' = '.$db->quote($publishedQuizzID));
        	$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
        endif;
        if($fromLR == 1 && $completeEvaluation == 1):
        	$query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
        	$query->where($db->quoteName('b.classroomID').' = '.$db->quote($classroomID));
        	$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
        	$query->group('a.content');
        endif;
        if($fromLR == 0):
			$query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
			$query->group('a.content');
		endif;
		$db->setQuery($query);
		$quizzPositions 	= $db->loadObjectList();
		$w1 = '';
		$w2 = '';
		$thePoints = 0;
		$html = '';
		if($fromLR == 1 && $completeEvaluation == 1):
			$w1 = 'width: 160px';
			$w2 = 'width: 310px';
		endif;
		if($fromLR == 1 && $completeEvaluation == 0):
			$w1 = 'width: 160px';
			$w2 = 'width: 190px';
			$html .= '<td style="'.$w1.';">';
			$html .= '</td>';
		endif;
		if($fromLR == 0):
			$w1 = 'width: 160px';
			$w2 = 'width: 190px';
			$html .= '<td style="'.$w1.';">';
			$html .= '</td>';
		endif;
		//if cart is not empty
		if($quizzPositions):
			$html .= '<td style="'.$w2.';">';
			$html .= '<table class="table table-striped table-sm">';
			foreach($quizzPositions as $part):
				if($part->content != ''):
					$html .= '<tr>';
					$html .= '<td style="font-size: 8pt;text-align: left;">'.$part->content.'</td>';
					$html .= '</tr>';
				endif;
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Textarea-Fragen
	function loadType11($questionID, $publishedQuizzID, $calculate, $fromLR, $completeEvaluation, $classroomID) {
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('
			COUNT(a.id ) as countAnswers,
			a.content
		'));
        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
        if($fromLR == 1 && $completeEvaluation == 0):
        	$query->where($db->quoteName('a.theResultID').' = '.$db->quote($publishedQuizzID));
        	$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
        endif;
        if($fromLR == 1 && $completeEvaluation == 1):
        	$query->join('LEFT', $db->quoteName('#__jclassroom_theresults', 'b') . ' ON (' . $db->quoteName('a.theResultID') . ' = ' . $db->quoteName('b.id') . ')');
        	$query->where($db->quoteName('b.classroomID').' = '.$db->quote($classroomID));
        	$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
        	$query->group('a.content');
        endif;
        if($fromLR == 0):
			$query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
			$query->group('a.content');
		endif;
		$db->setQuery($query);
		$quizzPositions 	= $db->loadObjectList();
		$w1 = '';
		$w2 = '';
		$thePoints = 0;
		$html = '';
		if($fromLR == 1 && $completeEvaluation == 1):
			$w1 = 'width: 160px';
			$w2 = 'width: 310px';
		endif;
		if($fromLR == 1 && $completeEvaluation == 0):
			$w1 = 'width: 160px';
			$w2 = 'width: 190px';
			$html .= '<td style="'.$w1.';">';
			$html .= '</td>';
		endif;
		if($fromLR == 0):
			$w1 = 'width: 160px';
			$w2 = 'width: 190px';
			$html .= '<td style="'.$w1.';">';
			$html .= '</td>';
		endif;
		//if cart is not empty
		if($quizzPositions):
			$html .= '<td style="'.$w2.';">';
			$html .= '<table class="table table-striped table-sm">';
			foreach($quizzPositions as $part):
				if($part->content != ''):
					$html .= '<tr>';
					$html .= '<td style="font-size: 8pt;text-align: left;">'.$part->content.'</td>';
					$html .= '</tr>';
				endif;
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Auswahlliste-Fragen
	function loadType12($questionID, $publishedQuizzID, $calculate, $fromLR) {
		$html = '';
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$answers = $db->loadObjectList();
		//Count correct answers
		$query 	= $db->getQuery(true);
		$query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->where($db->quoteName('a.correct').' = '.$db->quote(1));
		$db->setQuery($query);
		$correctSoll 	= $db->loadResult();
		$correctIst 	= 0;
		$w1 = '';
		$w2 = '';
		if($fromLR == 1):
			$w1 = 'width: 160px';
			$w2 = 'width: 190px';
		endif;
		if($answers):
			$html .= '<td style="'.$w1.';">';
			$html .= '<table style="width: 100%;">';
			foreach($answers as $answer):
				if($answer->correct == 1):
					$bgS = 'bg-success';
				else:
					$bgS = 'bg-danger';
				endif;
				$html .= '<tr>';
				$html .= '<td style=""><span class="" style="text-align: left;font-size: 8pt;">' .$answer->title.'</span></td>';
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		$html .= '<td style="'.$w2.';">';
		$html .= '<table style="width: 100%;">';
		
		foreach($answers as $answer):
			$setCorrect = $answer->correct;
			$theTD 		= '<td style="height: 13px;line-height: 8pt;"></td>';
			$theAnswer 	= '';
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$r 	= JTable::getInstance('Quizz_results','JclassroomTable',array());
			$load = array(
				'theResultID' 	=> $publishedQuizzID,
				'questionID' 	=> $questionID,
				'answerID' 		=> $answer->id
			);
			$check = $r->load($load);
			if($check):
				$theTD = '<td style="padding: 3px;"><span class="" style="font-size: 8pt;line-height: 8pt;text-align: left;">'.$r->content.' '.$theID.'</span></td>';
			else:

			endif;
			/*foreach($cart as $part):
				$eparts 	= $part['content'];
				if($eparts):
					foreach($eparts as $epart):
						$EPS = explode('_', $epart);
						if($EPS[0] == $answer->id):
							if($answer->correct == 1):
								$bgS = 'bg-success';
								$correctIst++;
								$thePoints += $points;
							else:	
								$bgS = 'bg-danger';
							endif;
							$theTD = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$EPS[1].' '.$theID.'</span></td>';
						endif;
					endforeach;
				endif;
			endforeach;*/
			$html .= '<tr>';
			$html .= $theTD;
			$html .= '</tr>';
		endforeach;
		$html .= '</table>';
		$html .= '</td>';
		if($correctSoll > $correctIst):
			$thePoints = ($correctIst * $points / $correctSoll);
		endif;
		if($calculate == 2):
			$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Checkbox-Fragen
	function loadType13($questionID, $publishedQuizzID, $calculate, $fromLR) {
		$html = '';
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$answers = $db->loadObjectList();
		//Count correct answers
		$query 	= $db->getQuery(true);
		$query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->where($db->quoteName('a.correct').' = '.$db->quote(1));
		$db->setQuery($query);
		$correctSoll 	= $db->loadResult();
		$correctIst 	= 0;
		$w1 = '';
		$w2 = '';
		if($fromLR == 1):
			$w1 = 'width: 160px';
			$w2 = 'width: 190px';
		endif;
		if($answers):
			$html .= '<td width="'.$w1.'">';
			$html .= '<table style="width: 100%;">';
			foreach($answers as $answer):
				if($answer->correct == 1):
					//$bgS = 'lightgreen';
				else:
					//$bgS = 'firebrick';
				endif;
				$html .= '<tr>';
				$html .= '<td style="background-color:'.$bgS.';"><span style="text-align: left;font-size: 8pt;line-height: 8pt;">'.$answer->title.'</span></td>';
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		$html .= '<td width="'.$w2.'">';
		$html .= '<table style="width: 100%;">';
		foreach($answers as $answer):
			$setCorrect = $answer->correct;
			$theTD 		= '<td style="height: 13px;line-height: 8pt;"></td>';
			$theAnswer 	= '';
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$r 	= JTable::getInstance('Quizz_results','JclassroomTable',array());
			$load = array(
				'theResultID' 	=> $publishedQuizzID,
				'questionID' 	=> $questionID,
				'answerID' 		=> $answer->id
			);
			$check = $r->load($load);
			if($check):
				$theTD = '<td style="padding: 3px;"><span class="" style="font-size: 8pt;line-height: 8pt;text-align: left;">'.$r->content.' '.$theID.'</span></td>';
			else:

			endif;
			/*foreach($cart as $part):
				$eparts 	= $part['content'];
				if($eparts):
					foreach($eparts as $epart):
						$EPS = explode('_', $epart);
						if($EPS[0] == $answer->id):
							if($answer->correct == 1):
								$bgS = 'bg-success';
								$correctIst++;
								$thePoints += $points;
							else:	
								$bgS = 'bg-danger';
							endif;
							$theTD = '<td style="padding: 3px;"><span class="" style="justify-content: center;">'.$EPS[1].' '.$theID.'</span></td>';
						endif;
					endforeach;
				endif;
			endforeach;*/
			$html .= '<tr>';
			$html .= $theTD;
			$html .= '</tr>';
		endforeach;
		$html .= '</table>';
		$html .= '</td>';
		if($correctSoll > $correctIst):
			$thePoints = ($correctIst * $points / $correctSoll);
		endif;
		if($calculate == 2):
			$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	
	public function getVorwort($rID) {
		$session = JFactory::getSession();
		$remove = array('<p>','</p>');
		$auditierteS = '';
		//GET AUDIT-KUNDE
		$this->SetMargins(20,40,10,false);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Quizz','JclassroomTable',array());
		$result->load($rID);
		$createdBy 		= $result->created_by;
		$preface 		= $result->preface;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$table->load($createdBy);
		$auditBy 	= $table->name;
		/*
		//if(count($auditierteS) == 1) {
		$len = strlen($auditierteS);
		$auditierteS = substr($auditierteS, 0,$len -2);
		//}
		$tblAuditorenID = $table->tblAuditorenID;
		$y = 40;
		//Globales Vorwort laden
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Fragenkatalog','AuditumTable',array());
		$table->load($tblFragenkatalogeID);
		$vorwort 	= $table->vorwort;
      	//GET UNTERNEHMEN-DATA
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Company','AuditumTable',array());
		$table->load($tblCompaniesID);
		$unternehmen_name 	= $table->name;
      	$unternehmen_beschreibung = $table->beschreibung;
      	$unternehmen_beschreibung = str_replace($remove,'',$unternehmen_beschreibung);
		//GET AUDITOR
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_auditum/tables');
		$table = JTable::getInstance('Auditor','AuditumTable',array());
		$table->load($tblAuditorenID);
		$auditorname = $table->vorname.' '.$table->nachname;*/
		//$this->SetTextColor(182,201,49);
		$this->SetTextColor(0,0,0);
		$this->SetFillColor(255,255,255);
		$this->SetDrawColor(255,255,255);
		$this->SetFont('helvetica', 'R', 10);
		$html = '';
		$html .= '<h2>1. Vorwort</h2>';
		
		$this->writeHTMLCell(170, 200, 20,40, $html, 0, 1, 1, true, 'J', true);
		$y = $this->getY() + 4;

		$currentPage = $this->PageNo();
		return $currentPage;
	}
	function get_string_between($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}
	public function getErgebnis($rID, $fromLR, $userID, $completeEvaluation, $classroomID) {
		//Load the Quizz-Data
		if($fromLR == 1):
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$quizz 	= JTable::getInstance('Unit','JclassroomTable',array());
			$quizz->load($rID);
			$quizzID 	= 'UID'.str_pad($quizz->id,8,'0',STR_PAD_LEFT);
			$quizzName 	= $quizz->title;
			$preface 	= '';
		else:
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$quizz 	= JTable::getInstance('Quizz','JclassroomTable',array());
			$quizz->load($rID);
			$quizzID 	= 'QID'.str_pad($quizz->id,8,'0',STR_PAD_LEFT);
			$quizzName 	= $quizz->title;
			$preface 	= $quizz->preface;
		endif;
		$this->SetTextColor(0,0,0);
		$this->SetFillColor(255,255,255);
		$this->SetDrawColor(255,255,255);
		$this->SetFont('helvetica', 'R', 10);
		$html = '';
		$html .= '<h2>3. Quizzergebnis</h2>';
		if($fromLR == 0):
			// rID is publishedQuizzID -> transform to rID
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
			$quizz 		= JTable::getInstance('Theresults','JclassroomTable',array());
			$load 		= array('publishedQuizzID' => $rID); 
			$quizz->load($load);
			$rID 	= $quizz->id;
		endif;
		JLoader::register('StageResultHelper',JPATH_COMPONENT_SITE.'/helpers/stageResult.php');
      	$result     = new StageResultHelper();
      	$html       .= $result->getResultPDF($rID, 1, $userID, $completeEvaluation, $classroomID, 0, $quizz->id);
		//$html .= $this->getResult($rID, $fromLR, $userID, $completeEvaluation, $classroomID);

		$this->writeHTMLCell(180, '', 20, $y, $html, 0, 1, 1, true, 'J', true);

	}
	public function getInhaltV($ad, $page) {
		//GET AUDIT-KUNDE
		$this->SetMargins(10,30,10,false);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Theresults','JclassroomTable',array());
		$table->load($ad);
		$this->SetMargins(20,30,10,false);		
		$this->SetTextColor(0,0,0);
		$y = 40;
      	// add a new page for TOC
        $this->addTOCPage();

        // write the TOC title and/or other elements on the TOC page
        $this->SetTextColor(120,120,120);
		$this->SetFont('helvetica', 'B', 14);
       	$this->MultiCell(150, 6, '2. Inhaltsverzeichnis', '0', 'L', 0, 1, '20', '30', true);
       	//$this->Bookmark('2. Inhaltsverzeichnis', 1, 0, '', '', array(128,0,0));
      	$this->SetTextColor(0,0,0);
        $this->SetFont('helvetica', 'R', 10);

        // define styles for various bookmark levels
        $bookmark_templates = array();
        // A monospaced font for the page number is mandatory to get the right alignment
        $bookmark_templates[0] = '<table border="0" cellpadding="0" cellspacing="5" style="background-color:#FFFFFF">
        <tr>
		<td width="145mm">
			<span style="font-family:helvetica;font-weight:bold;font-size:8pt;color:black;">#TOC_DESCRIPTION#</span>
		</td>
        <td width="28mm"><span style="font-family:helvetica;font-weight:bold;font-size:10pt;color:black;" align="right">#TOC_PAGE_NUMBER#</span></td>
        </tr>
        </table>';
        $bookmark_templates[1] = '<table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td width="10mm">&nbsp;</td>
        <td width="140mm"><span style="font-size: 8pt;color:black;">#TOC_DESCRIPTION#</span></td>
        <td width="26mm"><span style="font-size: 8pt;font-weight:bold;color:white;" align="right">#TOC_PAGE_NUMBER#</span></td>
        </tr>
        </table>';
        // add other bookmark level templates here ...

        // add table of content at page 1
        // (check the example n. 45 for a text-only TOC
        $this->addHTMLTOC($page, 'INDEX', $bookmark_templates, true, 'B', array(128,0,0));

        // end of TOC page
        $this->endTOCPage();
	}
}
?>