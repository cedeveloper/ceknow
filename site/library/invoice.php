<?php
defined('_JEXEC') or die;

class printPDF {
	
	public function invoice($id) {
		require_once(JPATH_SITE.'/components/com_jclassroom/controllers/tcpdf_include.php');
		require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('dats | Druckagentur Torsten Scheel');
		$pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE,PDF_HEADER_STRING);
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		// set margins
		$pdf->SetMargins(25, 10, 10, true);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->SetFont('helvetica', 'R', 14, '', false);
		$pdf->AddPage();
		//$pdf->Rect(0, 0, 40, 90, 'DF', '', array(220, 220, 200));
		$adress = $this->getAdress($id);
		$pdf->writeHTMLCell(85, 5, 25, 50, $adress, 0,0, 0, 1,false, 'left', true);
		$company = $this->getCompany($id);
		$pdf->writeHTMLCell(85, 5, 125, 50, $company, 0,0, 0, 1,false, 'left', true);
		$html = $this->getResult($id);
		$pdf->writeHTMLCell(165, 100, 25, 97, $html, 0,0, 0, 1,false, 'left', true);
		//$pdf->writeHTML($html, true, false, true, false, '');
		ob_end_clean();
		$pdf->Output('components/com_inclure/pdf/Dispo1_BLG'.$id.'.pdf', 'I');
	}
	function getAdress($id) {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Invoice','JclassroomTable',array());
		$table->load($id);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$student 	= JTable::getInstance('Customer','JclassroomTable',array());
		$load 		= array('id' => $table->customerID);
		$student->load($load);
		$html = '<div style="font-size:12px;">';
		$html .= '<small style="font-size: 6px;">ce - corporate education GmbH | Bonner Str. 178 | 50968 Köln</small><br/>';
		$html .= '<span>'.$student->company_name.'</span><br/>';
		$html .= '<span>'.$student->address.'</span><br/>';
		$html .= '<span>'.$student->postcode.' '.$student->city.'</span>';
		$html .= '</div>';
		return $html;
	}
	function getCompany($id) {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('Invoice','JclassroomTable',array());
		$table->load($id);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$student 	= JTable::getInstance('Customer','JclassroomTable',array());
		$load 		= array('id' => $table->customerID);
		$student->load($load);
		$html = '<div style="font-size:10px;">';
		$html .= '<span>ce - corporate education GmbH</span><br/>';
		$html .= '<span>Bonner Str. 178</span><br/>';
		$html .= '<span>50968 Köln</span><br/>';
		$html .= '<span>Telefon: +49 221 | 29 211 6 - 0</span><br/>';
		$html .= '<span>E-Mail: training@ce.de</span><br/>';
		$html .= '<span>Rechnung-Nr.: RE'.str_pad($id, 8, '0',STR_PAD_LEFT).'</span><br/>';
		$html .= '<span>Kundennummer: '.$student->customer_number.'</span><br/>';
		$html .= '<span>Datum: '.date('d.m.Y').'</span><br/>';
		$html .= '</div>';
		return $html;
	}
	function getResult($id) {
		$user 			= JFactory::getUser();
		$input 			= JFactory::getApplication()->input;
		$learningroomID = $input->get('id',0,'INT');
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		// Load the result of the quizz
		$query 		= $db->getQuery(true);
		$query->select(array('a.*,b.*'));
        $query->from($db->quoteName('#__jclassroom_invoices','a'));
        $query->join('INNER', $db->quoteName('#__jclassroom_invoice_positions', 'b') . ' ON (' . $db->quoteName('b.invoiceID') . ' = ' . $db->quoteName('a.id') . ')');
		$query->where($db->quoteName('a.id').' = '.$db->quote($id));
		$query->order('b.id ASC');
		$db->setQuery($query);
		$positions 	= $db->loadObjectList();
		$html = '';
		$html .= '<table cellpadding="2" border="0" width="100%" style="font-size: 10px;text-align: left;">';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td colspan="4"><br/><p>Sehr geehrte Damen und Herren,<br/><br/>wir stellen Ihnen wie folgt in Rechnung: <br/></p></td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<table cellpadding="2" border="1" width="100%" style="font-size: 10px;text-align: left;">';
		$html .= '<tbody>';
		$html .= '<tr style="background-color: #e1e1e1;">';
		$html .= '<td width="30">Pos.</td>';
		$html .= '<td width="270">Artikel</td>';
		$html .= '<td width="70" align="right">Netto</td>';
		$html .= '<td width="50" align="right">Menge</td>';
		$html .= '<td width="70" align="right">Gesamt</td>';
		$html .= '</tr>';
		if($positions):
			$i = 1;
			foreach($positions as $position):
				$html .= '<tr>';
				$html .= '<td>'.$i.'</td>';
				$html .= '<td>'.$position->plan.'</td>';
				$html .= '<td align="right">'.number_format($position->ep,2,',','.').' €</td>';
				$html .= '<td align="right">'.$position->amount.'</td>';
				$html .= '<td align="right">'.number_format($position->gp,2,',','.').' €</td>';
				$html .= '</tr>';
				$i++;
				$gp += $position->gp;
			endforeach;
		endif;
		$mwst 	= ($gp * 16) / 100;
		$brutto = ($gp * 116) / 100;
		$html .= '<tr>';
		$html .= '<td colspan="4">Gesamt netto</td>';
		$html .= '<td align="right">'.number_format($gp,2,',','.').' €</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td colspan="4">zzgl. 16% MwSt.</td>';
		$html .= '<td align="right">'.number_format($mwst,2,',','.').' €</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td colspan="4">Gesamt brutto</td>';
		$html .= '<td align="right">'.number_format($brutto,2,',','.').' €</td>';
		$html .= '</tr>';
		$html .= '</table>';
		return $html;
	}
}
require_once(JPATH_SITE.'/components/com_jclassroom/controllers/tcpdf_include.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');

class MYPDF extends TCPDF {
	public function Header() {
		$this->Image('images/logo_ceknow1.png', 10, 10, 40, 15, 'PNG','' , '', true, 300, '', false, false, 0, false, false, false);
		$this->writeHTMLCell(90, 30, 110,11.5, '<h1 style="font-size: 20pt;">Rechnung</h1>', 0, 0, 0, 0, 'R', true, true, 0, false, true, 30, 'M', true);
	}
	public function Footer() {
		$now = JFactory::getDate();
		$date = new JDate($now);
		$now = $date->setTimezone(new DateTimeZone('Europe/Berlin'));
		$datumSTR = $now->format('d.m.Y');
		$datum = explode(' ',$now);

		$html = '';
		$html .= '<table>';
		$html .= '<tr>';
		$html .= '<td width="33%">';
		$html .= 'ce - corporate education GmbH<br/>';
		$html .= 'Bonner Str. 178<br/>';
		$html .= '50968 Köln';
		$html .= '</td>';
		$html .= '<td width="34%">';
		$html .= 'Telefon: +49 221 | 29 211 6-0';
		$html .= 'E-Mail: info@ce.de<br/>';
		$html .= 'Web: www.ce.de';
		$html .= '</td>';
		$html .= '<td width="33%">';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</table>';


		$this->writeHTMLCell(165, 20, 25, 270, $html, 0,0, 0, 1,false, 'left', true);
		/*$this->SetXY(10,-15);
		// Set font
		$this->SetFont('helvetica', '', 7);
		// Page number
		$this->Cell(0, 10, $mandant, 0, false, 'L', 0, '', 0, false, 'T', 'M');
		$this->MultiCell(50, 5, 'ceKnow (c) '.date('Y'), '0', 'L', 0, 1, '10', '285', true);
		$this->MultiCell(50, 5, 'Druck am: '.$datumSTR.' '.$datum[1], '', 'L', 0, 1, '10', '288', true);
		$this->MultiCell(50, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), '0', 'R', 0, 1, '160', '288', true);*/
	}
}

