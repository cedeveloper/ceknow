<?php
defined('_JEXEC') or die;

if (!class_exists('TCPDF')) {
	require_once(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tcpdf/tcpdf.php');
}
$aktSite = 0;

class printAuditEngine {
	
	public function printAudit($rID) {
		$pdf = new MYPDF('P', 'mm', array(297,210), true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('dats | Druckagentur Torsten Scheel');
		$pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE,PDF_HEADER_STRING);
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
		
		// set margins
		$pdf->SetFillColor(255,255,255);
		$pdf->SetDrawColor(0,0,0);
		// set auto page breaks
		$pdf->SetAutoPageBreak(true, 18);
		$pdf->SetMargins(10,50,10,false);
		$pdf->SetFont('helvetica', 'B', 12, '', false);
		$pdf->AddPage();
		$aktSite = $pdf->getAliasNumPage();
		if($aktSite = 1) {
			$pdf->getFooterFirst($rID);
			//$pdf->getAuditiert($ad);	
		} 
		$pdf->SetFont('helvetica', 'R', 48, '', false);
		$pdf->SetTextColor(120,120,120);
		$pdf->MultiCell(190, 25,'Umfragebericht', '', 'L', 0, 1, 10, 120, true,0,false,false,25,'M',false);
		$pdf->SetFont('helvetica', 'R', 18, '', false);
		
		$pdf->AddPage();
      	
      	JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Quizz','JclassroomTable',array());
		$result->load($rID);
      	if($result->preface):
      		$pdf->Bookmark('1. Vorwort', 0, 0, '', '', array(0,0,0));
			$IV = $pdf->getVorwort($rID);
			$pdf->AddPage();
		else:
			$IV = 1;
		endif;
		$pdf->Bookmark('2. Umfragedetails', 0, 0,'', '', array(0,0,0));
		$html = $this->getUmfragedetails($rID);
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->AddPage();
      	/*$pdf->Bookmark('3. Management Summary', 0, 0, '', '', array(128,0,0));
		$pdf->getSummary($ad,$IV);
		$pdf->AddPage();
      	$pdf->Bookmark('4. Auditergebnis', 0, 0, '', '', array(128,0,0));
		$pdf->getErgebnis($ad);
		$pdf->AddPage();
      	$pdf->Bookmark('5. Maßnahmenkatalog', 0, 0, '', '', array(128,0,0));
		$pdf->getMassnahmen($ad);*/
		$html = $this->getResult($rID);
		$pdf->SetTextColor(0,0,0);
		$pdf->Bookmark('3. Auswertung', 0, 0,'', '', array(0,0,0));
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->getInhaltV($rID, $IV);
		ob_end_clean();
		$pdf->Output('Umfragebericht_'.$rID.'.pdf', 'I');
	}
	function getUmfragedetails($publishedQuizzID) {
		$html = '';
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('Quizz','JclassroomTable',array());
		$table->load($publishedQuizzID);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$company = JTable::getInstance('Company','JclassroomTable',array());
		$company->load($table->companyID);
		$db 		= JFactory::getDbo();
		$query 		= $db->getQuery(true);
		$query->select(array('
			COUNT(a.id) as participations
		'));
        $query->from($db->quoteName('#__jclassroom_theresults','a'));
		$query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
		$db->setQuery($query);
		$participations 	= $db->loadResult();
		$html .= '<h2>Umfragedetails</h2>';
		$html .= '<table border="1" cellpadding="2" style="font-size: 10pt;">';
		$html .= '<tr>';
		$html .= '<td>Quizz-ID:</td>';
		$html .= '<td>QID'.str_pad($table->quizzID, 6, '0', STR_PAD_LEFT).'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Veröffentlichung-ID:</td>';
		$html .= '<td>PID'.str_pad($table->id, 6, '0', STR_PAD_LEFT).'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>zu Learningroom:</td>';
		$html .= '<td>LR'.str_pad($table->classroomID, 8, '0', STR_PAD_LEFT).'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Art des Quizz:</td>';
		$html .= '<td>Umfrage</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Für Firma:</td>';
		$html .= '<td>'.$company->name.'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Anzahl Teilnehmer:</td>';
		$html .= '<td>'.$participations.'</td>';
		$html .= '</tr>';
		$html .= '</table>';
		return $html;
	}
	function getResult($publishedQuizzID) {
		$html = '';
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('
			a.*,
			b.id as positionID,
			b.title,
			b.type
		'));
        $query->from($db->quoteName('#__jclassroom_quizzes','a'));
        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'b') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('b.quizzID') . ')');
		$query->where($db->quoteName('a.id').'='.$db->quote($publishedQuizzID));
		$db->setQuery($query);
		$quizzPositions = $db->loadObjectList();
		if($quizzPositions):
			$path = JPATH_SITE.'/images/evaluates/eva'.$publishedQuizzID;
			$files = glob($path.'/*', GLOB_BRACE);
			$html .= '<h1>Auswertung für Umfrage</h1>';
			$html .= '<table border="1" cellpadding="2">';
			$html .= '<tr style="font-size: 10pt;">';
			$html .= '<th>Frage</th>';
			$html .= '<th>Antwortmöglichkeiten</th>';
			$html .= '<th>Antworten</th>';
			$html .= '<th>Diagramm</th>';
			$html .= '</tr>';
			foreach($quizzPositions as $quizzPosition):
				$html .= '<tr>';
				$html .= '<td style="font-size: 9pt;">'.$quizzPosition->title.'</td>';
				switch($quizzPosition->type):
					case '3':
						// WWM-Fragen
						$return = $this->loadType3($quizzPosition->positionID, $cart, $quizzPosition->points);
						$html .= $return['html'];
						$points += $return['points'];
						break;
					case '5':
						// Ja/Nein-Fragen
						$return = $this->loadType5($quizzPosition->positionID, $publishedQuizzID);
						$html .= $return['html'];
						$points += $return['points'];
						break;
					case '10':
						// Ja/Nein-Fragen
						$return = $this->loadType10($quizzPosition->positionID, $cart, $quizzPosition->points);
						$html .= $return['html'];
						$points += $return['points'];
					case '11':
						// Textarea-Fragen
						$return = $this->loadType11($quizzPosition->positionID, $publishedQuizzID);
						$html .= $return['html'];
						$points += $return['points'];
				endswitch;
				$html .= '<td style="font-size: 9pt;">';
				foreach($files as $file) {
					if(basename($file) == 'quizzID_'.$quizzPosition->positionID.'.png'):
						$pathT = '/images/evaluates/eva'.$publishedQuizzID.'/'.basename($file);
						$html .= '<img src="'.$pathT.'" />';
					endif;
				}
				$html .= '</td>';	
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
		endif;
		//$pdf->Bookmark('Auswertung für Umfrage', 1, 0, '', '', array(0,0,0));
		
		return $html;
	}
		// for WWM-Fragen
	function loadType3($questionID, $cart, $points) {

		$html = '';
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$answers = $db->loadObjectList();
		//Count correct answers
		$query 	= $db->getQuery(true);
		$query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->where($db->quoteName('a.correct').' = '.$db->quote(1));
		$db->setQuery($query);
		$correctSoll 	= $db->loadResult();
		$correctIst 	= 0;
		if($answers):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			foreach($answers as $answer):
				if($answer->correct == 1):
					$bgS = 'bg-success';
				else:
					$bgS = 'bg-danger';
				endif;
				$html .= '<tr>';
				$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$answer->title.' '.$answer->id.'</span></td>';
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		//if cart is not empty
		if($cart):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			// 
			foreach($answers as $answer):
				$setCorrect = $answer->correct;
				$theTD 		= '<td style="height: 27px;"></td>';
				$theAnswer 	= '';
				foreach($cart as $part):
					$eparts 	= $part['content'];
					if($eparts):
						foreach($eparts as $epart):
							$EPS = explode('_', $epart);
							if($EPS[0] == $answer->id):
								if($answer->correct == 1):
									$bgS = 'bg-success';
									$correctIst++;
									$thePoints += $points;
								else:	
									$bgS = 'bg-danger';
								endif;
								$theTD = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$EPS[1].' '.$theID.'</span></td>';
							endif;
						endforeach;
					endif;
				endforeach;
				$html .= '<tr>';
				$html .= $theTD;
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
			if($correctSoll > $correctIst):
				$thePoints = ($correctIst * $points / $correctSoll);
			endif;
			$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Ja/Nein-Fragen
	function loadType5($questionID, $publishedQuizzID) {
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('
			COUNT(a.id ) as countAnswers,
			a.content
		'));
        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
		$query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->group('a.content');
		$db->setQuery($query);
		$quizzPositions 	= $db->loadObjectList();

		$thePoints = 0;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quest 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$quest->load($questionID);
		if($quest->correctAnswers == 0):
			$bgsJ = 'bg-success';
			$bgsN = 'bg-danger';
		else:
			$bgsJ = 'bg-danger';
			$bgsN = 'bg-success';
		endif;
		$html = '';
		$db 	= JFactory::getDbo();
		$html .= '<td style="font-size: 9pt;padding:5px;">';
		$html .= '<table style="width: 100%;">';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '</td>';
		//if cart is not empty
		if($quizzPositions):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;font-size: 9pt;">';
			foreach($quizzPositions as $part):
				if($part->content == 'answerJ'):
					$ATJ = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">'.$part->countAnswers.' x Ja</span></td>';
				endif;
				if($part->content == 'answerN'):
					$ATN = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">'.$part->countAnswers.' x Nein</span></td>';
				endif;
			endforeach;
			$html .= '<tr>';
			$html .= $ATJ;
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= $ATN;
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '</td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Textfeld-Fragen
	function loadType10($questionID, $cart, $points) {
		$thePoints = 0;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quest 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$quest->load($questionID);
		if($quest->correctAnswers == 0):
			$bgsJ = 'bg-success';
			$bgsN = 'bg-danger';
		else:
			$bgsJ = 'bg-danger';
			$bgsN = 'bg-success';
		endif;
		$html = '';
		$db 	= JFactory::getDbo();
		$html .= '<td style="padding:5px;">';
		$html .= '<table style="width: 100%;">';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '</td>';
		//if cart is not empty
		if($cart):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			foreach($cart as $part):
				if($part['questionID'] = $questionID):
					$content = explode('_', $part['content'][0]);
					if($content[1] == 'answerJ'):
						$ATJ = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
						$ATN = '<td style="height: 27px;"></td>';
						if($quest->correctAnswers == 0):
							$thePoints = $points;
						endif;
					endif;
					if($content[1] == 'answerN'):
						$ATJ = '<td style="height: 27px;"></td>';
						$ATN = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
						if($quest->correctAnswers == 1):
							$thePoints = $points;
						endif;
					endif;
				endif;
			endforeach;
			$html .= '<tr>';
			$html .= $ATJ;
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= $ATN;
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '</td>';
			$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Textarea-Fragen
	function loadType11($questionID, $publishedQuizzID) {
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('
			COUNT(a.id ) as countAnswers,
			a.content
		'));
        $query->from($db->quoteName('#__jclassroom_quizzresults','a'));
		$query->where($db->quoteName('a.publishedQuizzID').' = '.$db->quote($publishedQuizzID));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->group('a.content');
		$db->setQuery($query);
		$quizzPositions 	= $db->loadObjectList();

		$thePoints = 0;
		$html = '';
		$html .= '<td style="padding:5px;">';
		$html .= '</td>';
		//if cart is not empty
		if($quizzPositions):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="font-size: 9pt;" class="table table-striped table-sm">';
			foreach($quizzPositions as $part):
				if($part->content != ''):
					$html .= '<tr>';
					$html .= '<td>'.$part->content.'</td>';
					$html .= '</tr>';
				endif;
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
}

class MYPDF extends TCPDF {

	public function Header() {
		$image_file = JPATH_SITE.'/images/logo-black.png';
		$this->Image($image_file, 10, 10, 50, 16, 'PNG', '', 'T', true, 300, '', false, false, 0, false, false, false);
	}	
	public function Footer() {
		$aktSite = $this->getAliasNumPage();
			$this->SetTextColor(0,0,0);
			$this->SetFont('helvetica', '', 7);
			$this->MultiCell(170, 6, "(c) 2020 | ceLearning", '0', 'L', 0, 1, '10', '281', true);
			$this->MultiCell(170, 3, date('d.m.Y H:i'), '0', 'L', 0, 1, '10', '284', true);
	}
	public function getFooterFirst($rID) {
		$user 	= JFactory::getUser();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$audit 	= JTable::getInstance('Quizz','JclassroomTable',array());
		$audit->load($rID);
		$createdBy 		= $audit->created_by;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table 	= JTable::getInstance('User','JclassroomTable',array());
		$table->load($createdBy);
		$auditCreator = $table->name;
		//GET THE AUDIT
		$this->SetTextColor(0,0,0);
		$this->SetFont('helvetica', '', 12);
		$this->MultiCell(170, 6, "Erstellt von: ".$auditCreator, '0', 'L', 0, 1, '10', '150', true);
		$this->SetFont('helvetica', '', 12);
		$this->MultiCell(170, 3, 'Erstellt am:' .date('d.m.Y H:i'), '0', 'L', 0, 1, '10', '156', true);
	}
	public function getVorwort($rID) {
		$session = JFactory::getSession();
		$remove = array('<p>','</p>');
		$auditierteS = '';
		//GET AUDIT-KUNDE
		$this->SetMargins(10,40,10,false);
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$result = JTable::getInstance('Quizz','JclassroomTable',array());
		$result->load($rID);
		$createdBy 		= $result->created_by;
		$preface 		= $result->preface;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$table = JTable::getInstance('User','JclassroomTable',array());
		$table->load($createdBy);
		$auditBy 	= $table->name;
		$this->SetTextColor(0,0,0);
		$this->SetFillColor(255,255,255);
		$this->SetDrawColor(255,255,255);
		$this->SetFont('helvetica', 'R', 10);
		$html = '';
		$html .= '<h2>1. Vorwort</h2>';
		$html .= $preface;
		/*$preface = str_replace('{unternehmen_name}',$unternehmen_name,$preface);
		$preface = str_replace('{unternehmen_beschreibung}',$unternehmen_beschreibung,$preface);

		//$vorwort = str_replace('{audit_start}',$beginn->format('d.m.Y H:i'),$vorwort);
		//$vorwort = str_replace('{audit_ende}',$ende->format('d.m.Y H:i'),$vorwort);
		$preface = str_replace('{auditor_name}',$auditorname,$preface);
		$preface = str_replace('{auditierte}',$auditierteS,$preface);
		$preface = str_replace('{unterlagen}',$unterlagen,$preface);
		$preface = explode('<p>--</p>',$preface);
		if($preface) {
			foreach($preface as $teilstring) {
				$parsed = $this->get_string_between($teilstring, '**', '**');
				$this->Bookmark($parsed, 1, 0, '', '', array(0,0,0));
				$teilstring = str_replace('**','',$teilstring);
				$html .= $teilstring;
			}
		}
		/*if($indVorwort) {
			$indVorwort = str_replace('{unternehmen_name}',$unternehmen_name,$indVorwort);
			$indVorwort = str_replace('{unternehmen_beschreibung}',$unternehmen_beschreibung,$indVorwort);

			$indVorwort = str_replace('{audit_start}',$beginn->format('d.m.Y H:i'),$indVorwort);
			$indVorwort = str_replace('{audit_ende}',$ende->format('d.m.Y H:i'),$indVorwort);
			$indVorwort = str_replace('{auditor_name}',$auditorname,$indVorwort);
			$indVorwort = str_replace('{auditierte}',$auditierteS,$indVorwort);
			$indVorwort = explode('<p>--</p>',$indVorwort);
			if($indVorwort) {
				foreach($indVorwort as $teilstring) {
					$parsed = $this->get_string_between($teilstring, '**', '**');
					$this->Bookmark($parsed, 1, 0, '', '', array(128,0,0));
					$teilstring = str_replace('**','',$teilstring);
					$html .= $teilstring;
				}
			}
		}*/
		$this->writeHTMLCell(170, 200, 10,40, $html, 0, 1, 1, true, 'J', true);
		$y = $this->getY() + 4;

		$currentPage = $this->PageNo();
		return $currentPage;
	}
	function get_string_between($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}
	public function getInhaltV($ad, $page) {
		//GET AUDIT-KUNDE
		$this->SetMargins(10,30,10,false);		
		$this->SetTextColor(0,0,0);
		$y = 40;
      	// add a new page for TOC
        $this->addTOCPage();

        // write the TOC title and/or other elements on the TOC page
        $this->SetTextColor(120,120,120);
		$this->SetFont('helvetica', 'B', 14);
       	$this->MultiCell(150, 6, 'Inhaltsverzeichnis', '0', 'L', 0, 1, '10', '30', true);
       	//$this->Bookmark('2. Inhaltsverzeichnis', 1, 0, '', '', array(128,0,0));
      	$this->SetTextColor(0,0,0);
        $this->SetFont('helvetica', 'R', 10);

        // define styles for various bookmark levels
        $bookmark_templates = array();
        // A monospaced font for the page number is mandatory to get the right alignment
        $bookmark_templates[0] = '<table border="0" cellpadding="0" cellspacing="5" style="background-color:#FFFFFF">
        <tr>
		<td width="145mm">
			<span style="font-family:helvetica;font-weight:bold;font-size:12pt;color:black;">#TOC_DESCRIPTION#</span>
		</td>
        <td width="28mm"><span style="font-family:helvetica;font-weight:bold;font-size:12pt;color:black;" align="right">#TOC_PAGE_NUMBER#</span></td>
        </tr>
        </table>';
        $bookmark_templates[1] = '<table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td width="10mm">&nbsp;</td>
        <td width="140mm"><span style="color:black;">#TOC_DESCRIPTION#</span></td>
        <td width="26mm"><span style="font-weight:bold;color:black;" align="right">#TOC_PAGE_NUMBER#</span></td>
        </tr>
        </table>';
        // add other bookmark level templates here ...

        // add table of content at page 1
        // (check the example n. 45 for a text-only TOC
        $this->addHTMLTOC($page + 1, 'INDEX', $bookmark_templates, true, 'B', array(128,0,0));

        // end of TOC page
        $this->endTOCPage();
	}
}
?>