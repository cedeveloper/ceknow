<?php
/**
 *  	DATS Torsten Scheel
 * 	  	JClassroom 1.0
 * 		LMS-System
 * 	 	(C) 2020. All Rights reserved.
 * 		05. April 2020
 *		Development: Torsten Scheel
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Get an instance of the controller prefixed by HelloWorld
$controller = JControllerLegacy::getInstance('jclassroom');

// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));

// Redirect if set by the controller
$controller->redirect();