<?php
define( '_JEXEC',1);
define( 'DS', DIRECTORY_SEPARATOR );
define( 'JPATH_BASE',dirname(__FILE__));
ini_set('default_socket_timeout', 600); // 900 Seconds = 15 Minutes Added by CS

require_once( JPATH_BASE. DS .'includes' . DS . 'defines.php' );
require_once( JPATH_BASE. DS .'includes' . DS . 'framework.php' );
require_once( JPATH_BASE. DS .'libraries' . DS . 'src' . DS . 'Factory.php' );
$mainframe = JFactory::getApplication('site');
$mainframe->initialise();
doSomething();

   	function doSomething() {
		$input   	= JFactory::getApplication()->input;
		$quizzID   	= $input->get('qID', 0, 'INT');
		$q 			= $input->get('q', 0, 'INT');
		$lid 		= $input->get('lid', 0, 'INT');
		if($lid == '10'):
			result();
		else:
			questions($quizzID, $q);
		endif;
      exit();
   	}  
   	function questions($quizzID, $q) {
   		$quizz 		= getQuizzFromStageing($quizzID, $q, 1);
   		echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">';
		echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>';
		echo '<script src="https://www.ceknow.de/expath.js"></script>';
		echo '<style>';
		echo '.hidden {
        	display: none;
    	}';
		echo '</style>';
		echo '<div class="stage row">';
			echo '<div class="col-12">';
        		echo '<h3><i class="fa fa-commenting"></i> '.$quizz['title'].'</h3>';
        		if($q == -1):
        			echo $quizz['description'];
        		endif;
        	echo '</div>';
        	if($q != -1):
	           	echo '<div class="col-12">
	           		<div class="row">
	                    <div class="col-12 "> 
	                        <div class="card">
	                            <div class="card-body">';
	                            $answered = 0;
                                    if($quizz['quizzType'] != 1 && $quizzt['quizzType'] != 14 ):
                                        echo '<h2>Frage '.($q + 1).' von '. $quizz['countSections'].'</h2>';
                                        echo '<h1>'.$quizz['question'].'</h1>';
                                    endif;
                                    if($quizz['quizzType'] == 1):
                                        echo '<h2>Karte '.($q + 1).' von '. $quizz['countSections'].'</h2>';
                                        if($quizz['type'] == 14):
                                            echo '<h1><span class="badge bg-success text-white">Prüffeld: '.$quizz['question'].'</span></h1>';
                                        else:
                                            echo '<h1>'.$quizz['question'].'</h1>';
                                        endif;
                                    endif;
                                    if(!$quizz['type']):
	                                    echo '<p>Dieses Quizz beinhaltet keine Fragen.</p>';
	                                endif;
	                                if($quizz['correctAnswers'] > 1):
	                                    echo '<p>Mehrere Antworten sind möglich.</p>';
	                                endif;
	                                echo html_entity_decode($quizz['content']);
	                                // WWM
	                                if($quizz['type'] == 3):
	                                    if($quizz['answers']):
	                                        foreach($quizz['answers'] as $answer):
	                                            $currentAnswerID;
	                                            $aColor = 'lightsteelblue';
	                                            $aClass ='';
	                                            if($cart):
	                                                foreach($cart as $item):
	                                                    if($item['questionID'] == $quizz['questionID']):
	                                                        foreach($item['content'] as $content):
	                                                            $currentAnswers = explode('_',$content);
	                                                            $currentAnswerID = $currentAnswers[0];
	                                                            if($answer->id == $currentAnswerID):
	                                                                $aColor = 'forestgreen';
	                                                                $aClass = 'choosen';
	                                                                $answered = 1;
	                                                            else:
	                                                                $aColor = 'lightsteelblue';
	                                                                $aClass = '';
	                                                            endif;
	                                                        endforeach;
	                                                    endif;
	                                                endforeach;
	                                            endif;
	                                            if($quizz['correctAnswers'] == 2):
	                                                echo '<h2><a id="answer'.$answer->id.'" onclick="chooseAnswer('.$answer->id.',0);" class="'.$aClass.' answer btn w-100" style="text-align: left;background-color:'.$aColor.';"><b>'.$answer->title.'</b></a></h2>';
	                                            else:  
	                                                echo '<h2><a id="answer'.$answer->id.'" onclick="checkQuestion('.$answer->id.');" class="'.$aClass.' answer btn w-100" style="text-align: left;background-color:'.$aColor.';"><b>'.$answer->title.'</b></a></h2>';
	                                            endif;
	                                        endforeach;
	                                    endif;
	                                endif;
	                                // Smiley
	                                if($quizz['type'] == 4):
	                                    if($quizz['existingAnswer']['answer']):
	                                        $valueA = $quizz['existingAnswer']['answer'];
	                                        $s1     = '';
	                                        $s2     = '';
	                                        $s3     = '';
	                                        $s4     = '';
	                                        $s5     = '';
	                                        switch($valueA):
	                                            case 1:
	                                                 $s1 = 'smileyActive';
	                                                break;
	                                            case 2:
	                                                 $s2 = 'smileyActive';
	                                                break;
	                                            case 3:
	                                                $s3 = 'smileyActive';
	                                                break;
	                                            case 4:
	                                                 $s4 = 'smileyActive';
	                                                break;
	                                            case 5:
	                                                 $s5 = 'smileyActive';
	                                                break;
	                                        endswitch;
	                                    endif;
	                                    echo '<div class="smileys">';
	                                    echo '<div id="smiley1" class="'.$s1.' smiley" onclick="nextQuestion(1);">
	                                        <img src="images/jclassroom/smiley1.png" />
	                                        </div>';
	                                    echo '<div id="smiley2" class="'.$s2.' smiley" onclick="nextQuestion(2);">
	                                        <img src="images/jclassroom/smiley2.png" />
	                                        </div>';
	                                    echo '<div id="smiley3" class="'.$s3.' smiley" onclick="nextQuestion(3);">
	                                        <img src="images/jclassroom/smiley3.png" />
	                                        </div>';
	                                    echo '<div id="smiley4" class="'.$s4.' smiley"onclick="nextQuestion(4);">
	                                        <img src="images/jclassroom/smiley4.png" />
	                                        </div>';
	                                    echo '<div id="smiley5" class="'.$s5.' smiley" onclick="nextQuestion(5);">
	                                        <img src="images/jclassroom/smiley5.png" />
	                                        </div>'; 
	                                    echo '</div>';
	                                endif;
	                                // Ja/Nein
	                                if($quizz['type'] == 5):
	                                    $aColor = 'lightsteelblue';
	                                    $class  = '';
	                                    if($quizz['existingAnswer']['answer']):
	                                        $valueA = $quizz['existingAnswer']['answer'];
	                                        $valueA = explode('_', $valueA);
	                                        $value  = $valueA[1];
	                                        if($value == 'answerJ'):
	                                            $class = 'choosen';
	                                        endif;
	                                    endif;
	                                    echo '<h2><a id="answerJ" onclick="JNAnswer(1);" class="'.$class.' answer yes btn w-100" style="text-align: left;background-color:'.$aColor.';"><b>Ja</b></a></h2>';
	                                    $class  = '';
	                                    if($quizz['existingAnswer']['answer']):
	                                        $valueA = $quizz['existingAnswer']['answer'];
	                                        $valueA = explode('_', $valueA);
	                                        $value  = $valueA[1];
	                                        if($value == 'answerN'):
	                                            $class = 'choosen';
	                                        endif;
	                                    endif;
	                                    echo '<h2><a id="answerN" onclick="JNAnswer(0);" class="'.$class.' answer no btn w-100" style="text-align: left;background-color:'.$aColor.';"><b>Nein</b></a></h2>';
	                                endif;
	                                // Audit
	                                if($quizz['type'] == 6):
	                                    echo '<p style="font-size: 24px;">';
	                                    if($quizz['testfield']):
	                                        echo '<span class="badge bg-success text-white mr-1">'.$quizz['testfield'].'</span>';
	                                    endif;
	                                    if($quizz['theme']):
	                                        echo '<span class="badge bg-primary text-white"> '.$quizz['theme'].'</span>';
	                                    endif;
	                                    echo '</p>';
	                                    $class  = '';
	                                    $exeption       = 'none';
	                                    $exeptionText   = '';
	                                    $exeptionPoints = '';
	                                    if($quizz['existingAnswer']['answer']):
	                                        $value = $quizz['existingAnswer']['answer'];
	                                        if($value == 'A_'):
	                                            $class = 'choosen';
	                                        endif;
	                                    endif;
	                                    echo '<p>'.$quizz['infotext'].'</p>';
	                                    echo '<h2><a id="answerA" onclick="auditAnswer(&quot;A&quot;);" class="'.$class.' answer a hover-blue btn w-100" style="text-align: left;background-color: #e1e1e1;"><b>Ja</b></a></h2>';
	                                    $class  = '';
	                                    if($quizz['existingAnswer']['answer']):
	                                        $valueA = $quizz['existingAnswer']['answer'];
	                                        $value = explode('_',$valueA);
	                                        if($value[0] == 'AA'):
	                                            $class = 'choosen';
	                                            $exeption = 'block';
	                                            $exeptionText = $value[2];
	                                            switch($value[1]):
	                                                case 1:
	                                                    $selected1 = 'selected';
	                                                    $selected2 = '';
	                                                    $selected3 = '';
	                                                    $selected5 = '';
	                                                    break;
	                                                case 2:
	                                                    $selected1 = '';
	                                                    $selected2 = 'selected';
	                                                    $selected3 = '';
	                                                    $selected5 = '';
	                                                    break;
	                                                case 3:
	                                                    $selected1 = '';
	                                                    $selected2 = '';
	                                                    $selected3 = 'selected';
	                                                    $selected5 = '';
	                                                    break;
	                                                case 5:
	                                                    $selected1 = '';
	                                                    $selected2 = '';
	                                                    $selected3 = '';
	                                                    $selected5 = 'selected';
	                                                    break;
	                                            endswitch;
	                                        else:
	                                            $exeption = 'none';
	                                        endif;
	                                    endif;
	                                    echo '<h2><a id="answerAA" onclick="auditAnswer(&quot;AA&quot;);" class="'.$class.' answer aa hover-blue btn w-100" style="text-align: left;background-color: #e1e1e1;"><b>Ja, mit Ausnahme</b></a></h2>';
	                                    $class  = '';
	                                    if($quizz['existingAnswer']['answer']):
	                                        $value = $quizz['existingAnswer']['answer'];
	                                        if($value == 'N_'):
	                                            $class = 'choosen';
	                                            $exeption = 'none';
	                                        endif;
	                                    endif;
	                                    echo '<h2><a id="answerN" onclick="auditAnswer(&quot;N&quot;);" class="'.$class.' answer n hover-blue btn w-100" style="text-align: left;background-color: #e1e1e1;"><b>Nein</b></a></h2>';
	                                    echo '<div id="exeption" style="display: '.$exeption.';">';
	                                    echo '<h5>Grund für die Ausnahme</h5>';
	                                    echo '<textarea id="exeptionText" class="mb-2">'.$exeptionText.'</textarea>';
	                                    echo '<h5>Abweichende Punkte</h5>';
	                                    echo '<small class="d-block">Wenn Sie abweichende Punkte vergeben möchten, wählen Sie diese bitte hier aus.</small>';
	                                    echo '<select id="exeptionPoints" class="select2">';
	                                    echo '<option selected value="">Keine abweichenden Punkte</option>';
	                                    echo '<option '.$selected1.' value="1">1</option>';
	                                    echo '<option '.$selected2.' value="2">2</option>';
	                                    echo '<option '.$selected3.' value="3">3</option>';
	                                    echo '<option '.$selected5.' value="5">5</option>';
	                                    echo '</select>';
	                                    echo '</div>';
	                                endif;
	                                if($quizz['type'] == 10):
	                                    if($quizz['existingAnswer']['answer']):
	                                        $valueA = $quizz['existingAnswer']['answer'];
	                                        $valueA = explode('_', $valueA);
	                                        $value  = $valueA[1];
	                                    else:
	                                        $value = '';
	                                    endif;
	                                    echo '<input type="text" id="textFeld" value="'.$value.'" placeholder="Bitte geben Sie hier Ihre Antwort ein."/>';
	                                endif;
	                                if($quizz['type'] == 11):
	                                    if($quizz['existingAnswer']['answer']):
	                                        $valueA = $quizz['existingAnswer']['answer'];
	                                        $valueA = explode('_', $valueA);
	                                        $value  = $valueA[1];
	                                    else:
	                                        $value = '';
	                                    endif;
	                                    echo '<textarea style="width: 100%; min-height: 100px;" id="textArea">'.$value.'</textarea>';
	                                endif;
	                                // Auswahlliste
	                                if($quizz['type'] == 12):
	                                    if($quizz['correctAnswers'] == 2):
	                                        $multiple   = 'multiple';
	                                        $first      = '';
	                                    else:
	                                        $multiple   = '';
	                                        $first      = '<option value="" selected disabled>Bitte auswählen</option>';
	                                    endif;
	                                    echo '<select class="select2-max" id="auswahlliste" '.$multiple.'>';
	                                    echo $first;
	                                    if($quizz['options']):
	                                        foreach($quizz['options'] as $option):
	                                            if($quizz['existingAnswer']['answer']):
	                                                $valueA = $quizz['existingAnswer']['answer'];
	                                                $valueA = explode('_', $valueA);
	                                                if($valueA[0] == $option->id):
	                                                    $value  = 'selected';
	                                                else:
	                                                    $value = '';
	                                                endif;
	                                            else:
	                                                $value = '';
	                                            endif;
	                                            echo '<option '.$value.' value="'.$option->id.'">'.$option->title.'</option>';
	                                        endforeach;
	                                    endif;
	                                    echo '</select>';
	                                endif;
	                                // Checkboxen
	                                if($quizz['type'] == 13):
	                                    if($quizz['checkboxes']):
	                                        foreach($quizz['checkboxes'] as $checkbox):
	                                            if($quizz['existingAnswer']['answer']):
	                                                $valueA = $quizz['existingAnswer']['answer'];
	                                                $valueA = explode('_', $valueA);
	                                                if($valueA[0] == $checkbox->id):
	                                                    $value  = 'checked';
	                                                else:
	                                                    $value = '';
	                                                endif;
	                                            else:
	                                                $value = '';
	                                            endif;
	                                            echo '<div class="checkbox_container d-flex" style="border-bottom: 1px solid #a1a1a1;width: 100%;align-items: center;">';
	                                            echo '<span class="theCheckboxText d-inline-block">'.$checkbox->title.'</span>';
	                                            echo '<input class="aCheckbox ml-2" '.$value.' name="theCheckboxes[]" type="checkbox" data-answerID="'.$checkbox->id.'" value="'.$checkbox->title.'" />';
	                                            echo '</div>';
	                                        endforeach;
	                                    endif;
	                                endif;
	                        	echo '</div>
	                        </div>
	                    </div>
	                </div>';
	                echo '<div class="panel-footer mt-3">';
	                    echo '<a id="quitQuizz" onclick="quitQuizz();" class="hidden float-left btn btn-danger btn-lg text-white">Quizz beenden</a>';
	                    echo '<a id="breakQuizz" onclick="breakQuizz();" class="hidden float-left btn btn-danger btn-lg text-white mr-1">Quizz unterbrechen</a>';
	                    echo '<a id="restartQuizz" onclick="restartQuizz();" class="hidden float-left btn btn-warning btn-lg text-white mr-1">Quizz neu starten</a>';
	                    echo '<a id="prevQuestion" onclick="prevQuestion();" class="hidden float-left btn btn-info btn-lg text-white">Zurück</a>';
	                    echo '<a id="nextQuestion" onclick="nextQuestion();" class="hidden float-right btn btn-success btn-lg text-white">Weiter</a>';
	                    echo '<a id="moveNextQuestion" onclick="moveNextQuestion();" class="hidden float-right btn btn-success btn-lg text-white">Weiter</a>';
                    	// if the first question in quizz, it could be finished
	                    if($q == 0):
	                        
	                    endif;
	                    if($q >= 1):
	                        
	                    endif;
                    	//if(($this->q + 1) != $this->unit['countSections']):
                        if(($quizz['type'] == 3 && $quizz['correctAnswers'] == 2) || $quizz['type'] == 12 || $quizz['type'] == 10 || $quizz['type'] == 11 || $quizz['type'] == 1 || $quizz['type'] == 13): 
                            
                        endif;
                    	//endif;
                	echo '</div>';
	            echo '</div>';
        	else:
        		echo '<div class="col-12">
	                <div class="panel-footer mt-3">
	                    <a id="quitQuizz" onclick="quitQuizz();" class="hidden float-left btn btn-danger btn-lg text-white">Quizz beenden</a>
	                    <a onclick="nextQuestion();" class="float-right btn btn-success btn-lg text-white">Starten</a>
	                </div>
	            </div>';
        	endif;
      	echo '</div>';
      	echo '<input type="hidden" id="quizzID" value="'.$quizz['quizzID'].'" />
		<input type="hidden" id="publishedQuizzID" value="'.$quizz['publishedQuizzID'].'" />
		<input type="hidden" id="clr" value="'.$quizz['clr'].'" />
		<input type="hidden" id="q" value="'.$q.'" />
		<input type="hidden" id="type" value="'.$quizz['type'].'" />
		<input type="hidden" id="showResultAfter" value="'.$quizz['showResultAfter'].'" />
		<input type="hidden" id="allowRestart" value="'.$quizz['allowRestart'].'" />
		<input type="hidden" id="stageType" value="'.$quizz['stageType'].'" />
		<input type="hidden" id="questionID" value="'.$quizz['questionID'].'" />
		<input type="hidden" id="classroomID" value="0" />
		<input type="hidden" id="theResultID" value="0" />
		<input type="hidden" id="correctAnswers" value="'.$quizz['correctAnswers'].'" />
		<input type="hidden" id="answered" value="<?php echo $answered;?>" />
		<input type="hidden" id="preview" value="<?php echo $this->preview;?>" />
		<input type="hidden" id="countSections" value="'.$quizz['countSections'].'" />
		<input type="hidden" id="mustBeAnswered" value="'.$quizz['mustBeAnswered'].'" />';
   	}
   	function result() {
   		echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">';
		echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>';
		echo '<script src="https://www.ceknow.de/expath.js"></script>';
		echo '<style>';
		echo '.hidden {
        	display: none;
    	}';
		echo '</style>';
   		$input 		= JFactory::getApplication()->input;
		$resultID 	= $input->get('rID', 0, 'INT');
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		if($resultID):
			// Load the result of the quizz, if exist
			$query 		= $db->getQuery(true);
			$query->select(array('
				a.*,
				a.id as theResultID,
				b.id as resultPositionID,
				b.questionID
				'));
	        $query->from($db->quoteName('#__jclassroom_theresults','a'));
	        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzresults', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.theResultID') . ')');
	        $query->join('LEFT', $db->quoteName('#__jclassroom_quizzpositions', 'c') . ' ON (' . $db->quoteName('b.questionID') . ' = ' . $db->quoteName('c.id') . ')');
			$query->where($db->quoteName('a.id').' = '.$db->quote($resultID));
			$query->group('b.questionID');
			$query->order('c.ordering');
			$db->setQuery($query);
			$questions 	= $db->loadObjectList();
			$quizzID 	= $questions[0]->quizzID;
			$theResultID= $questions[0]->theResultID;
		else:
			$quizzID = $input->get('qID', 0, 'INT');
		endif;
		//Load the quizz
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quizz 		= JTable::getInstance('Unit','JclassroomTable',array());
		$quizz->load($quizzID);
		$html 		= false;
		if($quizz->type == 0):
			$html .= '<p>Auswertung für Quizz</p>';
			$html .= getQuizzResult($quizz->id);
		endif;
		if($quizz->type == 1):
			$html .= '<p>Auswertung für Audit</p>';
			if($questions):
				$html .= getAuditResult($quizz, $theResultID);
			endif;
		endif;

		echo $html;
   	}
   	function getQuizzResult($quizzID) {
		$session 	= JFactory::getSession();
		$cart 		= $session->get('quizzes');
		// Load the quizz
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quizz 		= JTable::getInstance('Unit','JclassroomTable',array());
		$quizz->load($quizzID);
		$counter 	= 1;
		$points 	= 0;
		$maxPoints 	= 0;
		$html = '';
		$html .= '<table class="table table-striped">';
		$html .= '<thead>';
		$html .= '<tr>';
		$html .= '<th width="50%">Frage</th>';
		//if($quizz->type != 1 && $quizz->calculate == 2):
		$html .= '<th width="30%" class="text-center">Antwortmöglichkeiten</th>';
		//endif;
		$html .= '<th width="30%" class="text-center">Ihre Antwort</th>';
		if($quizz->calculate == 2):
			$html .= '<th width="10%" class="text-center">Punkte</th>';
		endif;
		$html .= '</tr>';
		$html .= '</thead>';
		//Load the quizzPositions 
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
		$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
		$query->where($db->quoteName('a.type').' <> '.$db->quote(1));
		$query->order('a.ordering asc');
		$db->setQuery($query);
		$quizzPositions 	= $db->loadObjectList();
		if($quizzPositions):
			$html .= '<tbody>';
			foreach($quizzPositions as $quizzPosition):
				$colspan 	= 1;
				$theBG 		= '';
				if($quizzPosition->type == 14):
					$theBG = 'style="background-color: lightskyblue;"';
					if($quizz->calculate == 2):
						$colspan = 4;
					else:
						$colspan = 3;
					endif;
				endif;
				$maxPoints += $quizzPosition->points;
				// Questiontitle for all questiontypes
				$html .= '<tr>';
				$html .= '<td '.$theBG.' colspan="'.$colspan.'"><h5><b>'.$counter.'. '.$quizzPosition->title.'</b></h5></td>';
				
				switch($quizzPosition->type):
					case '3':
						// WWM-Fragen
						$return = loadType3($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
						$html .= $return['html'];
						$points += $return['points'];
						break;
					case '5':
						// Ja/Nein-Fragen
						$return = loadType5($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
						$html .= $return['html'];
						$points += $return['points'];
						break;
					case '10':
						// Textfeld-Fragen
						$return = loadType10($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
						$html .= $return['html'];
						$points += $return['points'];
						break;
					case '11':
						// Textarea-Fragen
						$return = loadType11($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
						$html .= $return['html'];
						$points += $return['points'];
						break;
					case '12':
						// Auswahlliste-Fragen
						$return = loadType12($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
						$html .= $return['html'];
						$points += $return['points'];
						break;
					case '13':
						// Checkbox-Fragen
						$return = loadType13($quizzPosition->id, $cart, $quizzPosition->points, $quizz->calculate);
						$html .= $return['html'];
						$points += $return['points'];
						break;
				endswitch;
				$html .= '</tr>';
				$counter++;
			endforeach;
			$html .= '</tbody>';
		endif;
		if($quizz->calculate == 2):
			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td><b>Erreichte Gesamtpunktzahl</b></td>';
			$html .= '<td colspan="2"></td>';
			$html .= '<td style="text-align: center;"><b><span style="color: forestgreen;">'.$points.'</span>/<span style="color: steelblue;">'.$maxPoints.'</span></b></td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td><b>Dies entspricht einem Score von</b></td>';
			$html .= '<td colspan="2"></td>';
			$score = 0;
			if($points && $maxPoints):
				$score = round($points * 100 / $maxPoints);
			endif;
			$html .= '<td style="text-align: center;"><b>'.$score.'%</b></td>';
			$html .= '</tr>';
			$html .= '</tfoot>';
		endif;
		$html .= '</table>';

		return $html;
	}
	// for WWM-Fragen
	function loadType3($questionID, $cart, $points, $calculate) {

		$html = '';
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$answers = $db->loadObjectList();
		//Count correct answers
		$query 	= $db->getQuery(true);
		$query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->where($db->quoteName('a.correct').' = '.$db->quote(1));
		$db->setQuery($query);
		$correctSoll 	= $db->loadResult();
		$correctIst 	= 0;
		if($answers):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			foreach($answers as $answer):
				if($answer->correct == 1):
					$bgS = 'bg-success';
				else:
					$bgS = 'bg-danger';
				endif;
				$html .= '<tr>';
				$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$answer->title.' '.$answer->id.'</span></td>';
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		//if cart is not empty
		if($cart):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			// 
			foreach($answers as $answer):
				$setCorrect = $answer->correct;
				$theTD 		= '<td style="height: 27px;"></td>';
				$theAnswer 	= '';
				foreach($cart as $part):
					$eparts 	= $part['content'];
					if($eparts):
						foreach($eparts as $epart):
							$EPS = explode('_', $epart);
							if($EPS[0] == $answer->id):
								if($answer->correct == 1):
									$bgS = 'bg-success';
									$correctIst++;
									$thePoints += $points;
								else:	
									$bgS = 'bg-danger';
								endif;
								$theTD = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$EPS[1].' '.$theID.'</span></td>';
							endif;
						endforeach;
					endif;
				endforeach;
				$html .= '<tr>';
				$html .= $theTD;
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
			if($correctSoll > $correctIst):
				$thePoints = ($correctIst * $points / $correctSoll);
			endif;
			if($calculate == 2):
				$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
			endif;
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Ja/Nein-Fragen
	function loadType5($questionID, $cart, $points, $calculate) {
		$thePoints = 0;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quest 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$quest->load($questionID);
		if($quest->correctAnswers == 0):
			$bgsJ = 'bg-success';
			$bgsN = 'bg-danger';
		else:
			$bgsJ = 'bg-danger';
			$bgsN = 'bg-success';
		endif;
		$html = '';
		$db 	= JFactory::getDbo();
		$html .= '<td style="padding:5px;">';
		$html .= '<table style="width: 100%;">';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '</td>';
		//if cart is not empty
		if($cart):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			foreach($cart as $part):
				if($part['questionID'] == $questionID):
					$content = explode('_', $part['content'][0]);
					if($content[1] == 'answerJ'):
						$ATJ = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
						$ATN = '<td style="height: 27px;"></td>';
						if($quest->correctAnswers == 0):
							$thePoints = $points;
						endif;
					endif;
					if($content[1] == 'answerN'):
						$ATJ = '<td style="height: 27px;"></td>';
						$ATN = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
						if($quest->correctAnswers == 1):
							$thePoints = $points;
						endif;
					endif;
				endif;
			endforeach;
			$html .= '<tr>';
			$html .= $ATJ;
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= $ATN;
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '</td>';
			if($calculate == 2):
				$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
			endif;
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Textfeld-Fragen
	function loadType10($questionID, $cart, $points, $calculate) {
		$thePoints = 0;
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jclassroom/tables');
		$quest 		= JTable::getInstance('Quizz_positions','JclassroomTable',array());
		$quest->load($questionID);
		if($quest->correctAnswers == 0):
			$bgsJ = 'bg-success';
			$bgsN = 'bg-danger';
		else:
			$bgsJ = 'bg-danger';
			$bgsN = 'bg-success';
		endif;
		$html = '';
		$db 	= JFactory::getDbo();
		$html .= '<td style="padding:5px;">';
		$html .= '<table style="width: 100%;">';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '</td>';
		//if cart is not empty
		if($cart):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			foreach($cart as $part):
				if($part['questionID'] == $questionID):
					$content = explode('_', $part['content'][0]);
					if($content[1] == 'answerJ'):
						$ATJ = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsJ.'" style="justify-content: center;">Ja</span></td>';
						$ATN = '<td style="height: 27px;"></td>';
						if($quest->correctAnswers == 0):
							$thePoints = $points;
						endif;
					endif;
					if($content[1] == 'answerN'):
						$ATJ = '<td style="height: 27px;"></td>';
						$ATN = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgsN.'" style="justify-content: center;">Nein</span></td>';
						if($quest->correctAnswers == 1):
							$thePoints = $points;
						endif;
					endif;
				endif;
			endforeach;
			$html .= '<tr>';
			$html .= $ATJ;
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= $ATN;
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '</td>';
			if($calculate == 2):
				$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
			endif;
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Textarea-Fragen
	function loadType11($questionID, $cart, $points, $calculate) {
		$thePoints = 0;
		$html = '';
		$db 	= JFactory::getDbo();
		$html .= '<td style="padding:5px;">';
		$html .= '</td>';
		//if cart is not empty
		if($cart):
			$html .= '<td style="padding:5px;">';
			foreach($cart as $part):
				if($part['questionID'] == $questionID):
					$content = explode('_', $part['content'][0]);
					if($content[1] != ''):
						$ATJ = $content[1];
					else:
						$ATJ = '';
					endif;
				endif;
			endforeach;
			$html .= $ATJ;
			$html .= '</td>';
			if($calculate == 2):
				$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
			endif;
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Auswahlliste-Fragen
	function loadType12($questionID, $cart, $points, $calculate) {
		$html = '';
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$answers = $db->loadObjectList();
		//Count correct answers
		$query 	= $db->getQuery(true);
		$query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->where($db->quoteName('a.correct').' = '.$db->quote(1));
		$db->setQuery($query);
		$correctSoll 	= $db->loadResult();
		$correctIst 	= 0;
		if($answers):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			foreach($answers as $answer):
				if($answer->correct == 1):
					$bgS = 'bg-success';
				else:
					$bgS = 'bg-danger';
				endif;
				$html .= '<tr>';
				$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$answer->title.'</span></td>';
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		//if cart is not empty
		if($cart):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			// 
			foreach($answers as $answer):
				$setCorrect = $answer->correct;
				$theTD 		= '<td style="height: 27px;"></td>';
				$theAnswer 	= '';
				foreach($cart as $part):
					$eparts 	= $part['content'];
					if($eparts):
						foreach($eparts as $epart):
							$EPS = explode('_', $epart);
							if($EPS[0] == $answer->id):
								if($answer->correct == 1):
									$bgS = 'bg-success';
									$correctIst++;
									$thePoints += $points;
								else:	
									$bgS = 'bg-danger';
								endif;
								$theTD = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$EPS[1].' '.$theID.'</span></td>';
							endif;
						endforeach;
					endif;
				endforeach;
				$html .= '<tr>';
				$html .= $theTD;
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
			if($correctSoll > $correctIst):
				$thePoints = ($correctIst * $points / $correctSoll);
			endif;
			if($calculate == 2):
				$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
			endif;
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
	// for Checkbox-Fragen
	function loadType13($questionID, $cart, $points, $calculate) {
		$html = '';
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select(array('a.*'));
        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$db->setQuery($query);
		$answers = $db->loadObjectList();
		//Count correct answers
		$query 	= $db->getQuery(true);
		$query->select(array('COUNT(correct)'));
        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
		$query->where($db->quoteName('a.questionID').' = '.$db->quote($questionID));
		$query->where($db->quoteName('a.correct').' = '.$db->quote(1));
		$db->setQuery($query);
		$correctSoll 	= $db->loadResult();
		$correctIst 	= 0;
		if($answers):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			foreach($answers as $answer):
				if($answer->correct == 1):
					$bgS = 'bg-success';
				else:
					$bgS = 'bg-danger';
				endif;
				$html .= '<tr>';
				$html .= '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$answer->title.'</span></td>';
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
		endif;
		//if cart is not empty
		if($cart):
			$html .= '<td style="padding:5px;">';
			$html .= '<table style="width: 100%;">';
			// 
			foreach($answers as $answer):
				$setCorrect = $answer->correct;
				$theTD 		= '<td style="height: 27px;"></td>';
				$theAnswer 	= '';
				foreach($cart as $part):
					$eparts 	= $part['content'];
					if($eparts):
						foreach($eparts as $epart):
							$EPS = explode('_', $epart);
							if($EPS[0] == $answer->id):
								if($answer->correct == 1):
									$bgS = 'bg-success';
									$correctIst++;
									$thePoints += $points;
								else:	
									$bgS = 'bg-danger';
								endif;
								$theTD = '<td style="padding: 3px;"><span class="d-flex p-1 d-block text-white w-100 badge '.$bgS.'" style="justify-content: center;">'.$EPS[1].' '.$theID.'</span></td>';
							endif;
						endforeach;
					endif;
				endforeach;
				$html .= '<tr>';
				$html .= $theTD;
				$html .= '</tr>';
			endforeach;
			$html .= '</table>';
			$html .= '</td>';
			if($correctSoll > $correctIst):
				$thePoints = ($correctIst * $points / $correctSoll);
			endif;
			if($calculate == 2):
				$html .= '<td style="text-align: center; font-weight: bold;"><span style="color:forestgreen">'.$thePoints.'</span>/<span style="color: steelblue;">'.$points.'</span></td>';
			endif;
		endif;
		$return = array('html' => $html, 'points' => $thePoints);
		return $return;
	}
   	//If quizz will be called by staging
	function getQuizzFromStageing($quizzID, $q, $preview) {
		$app 		= JFactory::getApplication();
		$user 		= JFactory::getUser();
		$db 		= JFactory::getDbo();
		
		//The quizz is preview
		if($preview == 1):
			$query = $db->getQuery(true);
			$query->select(array('
				a.*
			'));
	        $query->from($db->quoteName('#__jclassroom_units','a'));
			$query->where($db->quoteName('a.id').' = '.$db->quote($quizzID));
			$db->setQuery($query);
			$result 	= $db->loadObject();
			//Count Quizzpositions
			$query = $db->getQuery(true);
			$query->select(array('count(a.id)'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
			$db->setQuery($query);
			$countQuestions 	= $db->loadResult();
			//Check if there are groups
			$query = $db->getQuery(true);
			$query->select(array('a.id'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
			$query->where($db->quoteName('a.type').' = 14');
			$db->setQuery($query);
			$groups 	= $db->loadObjectList();
			//Get Quizzpositions
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
			$query->where($db->quoteName('a.quizzID').' = '.$db->quote($quizzID));
			$query->where($db->quoteName('a.ordering').' = '.$db->quote($q));
			$db->setQuery($query);
			$theQuestions 	= $db->loadObject();
			//Get Answers
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theAnswers 	= $db->loadObjectList();
			//Get Options
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theOptions 	= $db->loadObjectList();
			//Get Checkboxes
			$query = $db->getQuery(true);
			$query->select(array('a.*'));
	        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->order('a.ordering ASC');
			$db->setQuery($query);
			$theCheckboxes 	= $db->loadObjectList();
			//Get Answers correct
			$query = $db->getQuery(true);
			$query->select(array('count(a.id)'));
	        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
			$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
			$query->where($db->quoteName('a.correct').' = 1');
			$db->setQuery($query);
			$theAnswersCorrect 	= $db->loadResult();
			$existingAnswer 	= getExistingAnswer($theQuestions->id);
			$return = array(
				'quizzID' 		=> $quizzID, 
				'quizzType' 		=> $result->quizzType,
				'publishedQuizzID' 	=> 0,
				'title' 		=> $result->title, 
				'description' 	=> $result->description,
				'question' 		=> $theQuestions->title,
				'questionID' 	=> $theQuestions->id,
				'type' 			=> $theQuestions->type,
				'content' 		=> $theQuestions->content,
				'infotext' 		=> $theQuestions->infotext,
				'testfield' 	=> $theQuestions->testfield,
				'theme' 		=> $theQuestions->theme,
				'answers' 		=> $theAnswers,
				'options' 		=> $theOptions,
				'checkboxes' 	=> $theCheckboxes,
				'countCorrect'	=> $theAnswersCorrect,
				'correctAnswers'=> $theQuestions->correctAnswers,
				'countSections' => $countQuestions,
				'stageType'		=> 'quizz',
				'showResultAfter'=> $result->showResultAfter,
				'allowRestart' 	=> $result->allowRestart,
				'groups' 		=> $groups,
				'groupTitle' 	=> $theQuestions->title,
				'existingAnswer'=> $existingAnswer,
				'mustBeAnswered'=> $result->mustBeAnswered
			);
		else:
			if($quizzID):
				if(!$q):
					$q = 0;
				endif;
				$db 	= JFactory::getDbo();
				$query 	= $db->getQuery(true);
				$query->select(array('a.*,b.type as quizzType'));
		        $query->from($db->quoteName('#__jclassroom_quizzes','a'));
		        $query->join('INNER', $db->quoteName('#__jclassroom_units', 'b') . ' ON (' . $db->quoteName('a.quizzID') . ' = ' . $db->quoteName('b.id') . ')');
				$query->where($db->quoteName('a.id').' = '.$db->quote($quizzID));
				$db->setQuery($query);
				$result 	= $db->loadObject();
				// Stop, if quizz is private and no user is logged in
				if($result->openQuizz == 0 && !$user->id):
					JFactory::getApplication()->enqueueMessage('Dies ist ein geschlossenes Quizz. Bitte melden sie sich an.', 'Message');
					$app->redirect(JRoute::_(JURI::Root().'demo', false));
				endif;
				// Stop, if the quizz is not published
				if($result->publish_up != '1970-01-01 01:00:00' && $result->publish_down != '1970-01-01 01:00:00'):
					if($result->publish_up > date('Y-m-d H:i:s')):
						JFactory::getApplication()->enqueueMessage('Dieses Quizz ist erst ab dem '.date('d.m.Y H:i', strtotime($result->publish_up)).' freigegeben.', 'Message');
						$app->redirect(JRoute::_(JURI::Root().'demo', false));
					endif;
					if($result->publish_down < date('Y-m-d H:i:s')):
						JFactory::getApplication()->enqueueMessage('Dieses Quizz ist nur bis zum '.date('d.m.Y H:i', strtotime($result->publish_down)).' freigegeben.', 'Message');
						$app->redirect(JRoute::_(JURI::Root().'demo', false));
					endif;
				endif;
				//Get Quizzpositions
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->quizzID));
				$query->where($db->quoteName('a.ordering').' = '.$db->quote($q));
				$db->setQuery($query);
				$theQuestions 	= $db->loadObject();
				//Count Quizzpositions
				$query = $db->getQuery(true);
				$query->select(array('count(a.id)'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->quizzID));
				$db->setQuery($query);
				$countQuestions 	= $db->loadResult();
				//Check if there are groups
				$query = $db->getQuery(true);
				$query->select(array('a.id'));
		        $query->from($db->quoteName('#__jclassroom_quizzpositions','a'));
				$query->where($db->quoteName('a.quizzID').' = '.$db->quote($result->quizzID));
				$query->where($db->quoteName('a.type').' = 14');
				$db->setQuery($query);
				$groups 	= $db->loadObjectList();
				//Get Answers
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
				$query->order('a.ordering ASC');
				$db->setQuery($query);
				$theAnswers 	= $db->loadObjectList();
				//Get Options
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzoptions','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
				$query->order('a.ordering ASC');
				$db->setQuery($query);
				$theOptions 	= $db->loadObjectList();
				//Get Checkboxes
				$query = $db->getQuery(true);
				$query->select(array('a.*'));
		        $query->from($db->quoteName('#__jclassroom_quizzcheckboxes','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
				$query->order('a.ordering ASC');
				$db->setQuery($query);
				$theCheckboxes 	= $db->loadObjectList();
				//Get Answers correct
				$query = $db->getQuery(true);
				$query->select(array('count(a.id)'));
		        $query->from($db->quoteName('#__jclassroom_quizzanswers','a'));
				$query->where($db->quoteName('a.questionID').' = '.$db->quote($theQuestions->id));
				$query->where($db->quoteName('a.correct').' = 1');
				$db->setQuery($query);
				$theAnswersCorrect 	= $db->loadResult();
				$existingAnswer 	= getExistingAnswer($theQuestions->id);
				if($result):
					// the quizzID is the ID of the published quizz.
					// the unitID is the ID of the quizz, which is connected with the published quizz
					$return = array('quizzID' => $result->quizzID, 
						'quizzType' 	=> $result->quizzType,
						'publishedQuizzID' 	=> $quizzID,
						'title' 		=> $result->title, 
						'description' 	=> $result->description,
						'question' 		=> $theQuestions->title,
						'questionID' 	=> $theQuestions->id,
						'type' 			=> $theQuestions->type,
						'content' 		=> $theQuestions->content,
						'infotext' 		=> $theQuestions->infotext,
						'testfield' 	=> $theQuestions->testfield,
						'theme' 		=> $theQuestions->theme,
						'answers' 		=> $theAnswers,
						'options' 		=> $theOptions,
						'checkboxes' 	=> $theCheckboxes,
						'countCorrect'	=> $theAnswersCorrect,
						'correctAnswers'=> $theQuestions->correctAnswers,
						'countSections' => $countQuestions,
						'stageType'		=> 'quizz',
						'showResultAfter'=> $result->showResultAfter,
						'allowRestart' 	=> $result->allowRestart,
						'groups' 		=> $groups,
						'existingAnswer'=> $existingAnswer,
						'groupTitle' 	=> $theQuestions->title
					);
				endif;	
			endif;
		endif;
	return $return;
	}
	function getExistingAnswer($questionID) {
		$session 	= JFactory::getSession();
		$cart 		= $session->get('quizzes');
		$input 		= JFactory::getApplication()->input;
		$stageType 	= $input->get('sT','','STR');
		$unitID 	= $input->get('unitID',0,'INT');
		$answer 	= '';
		if($cart):
			foreach($cart as $item):
				//$answer .= "A".$item['questionID'].' '.$questionID.'<br/>';
				if($item['questionID'] == $questionID):
					if($item['type'] != 6):
						$answer 	= $item['content'][0];
					else:
						$answer 	= $item['content'][0].$item['exeptionPoints'].'_'.$item['exeptionText'];
					endif;
				endif;
			endforeach;
		endif;
		$return 	= array(
			'classroomID' 	=> $input->get('clr', 0, 'INT'),
			'quizzID' 		=> $input->get('unitID',0,'INT'),
			'questionID' 	=> $questionID,
			'q' 			=> $input->get('q',0, 'INT'),
			'answer' 		=> $answer
		);

		return $return;
	}
?>