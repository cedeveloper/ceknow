<?php	
   function searchEndingDemoPlans() {
      echo '<span>====== Check for Demoplans, ending within 7 days =====</span><br>';
      $now  = date('Y-m-d');
      echo '<span>====== Today is '.$now.' =====</span><br>';
      $db   = JFactory::getDbo(); 
      $query   = $db->getQuery(true);
      $query->select('a.*');
      $query->from($db->quoteName('#__jclassroom_customer','a'));
      $query->where($db->quotename('a.plan_to').' <= '.$db->quote($now));
      $query->where($db->quotename('a.planID').' = '.$db->quote(4));
      $db->setQuery($query);
      $result = $db->loadObjectList();
      if($result):
         foreach($result as $item):
            echo '<span style="background-color: red; color: #fff;">====== DEMOPLAN for'.$item->customer_number.' is ending on '.$item->plan_to.' =====</span><br>';
         endforeach;
      endif;

   }
?>