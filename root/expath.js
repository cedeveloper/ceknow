function nextQuestion(choosenAnswer) {   
    if($('#answer' + choosenAnswer).hasClass('closed')) {
        $('#message .modal-body').html('<h4>Sie haben diese Frage bereits beantwortet.</h4>');
        $('#message').modal('show');
        return false;
    }
    
    var quizzID     = $('#quizzID').val();
    var publishedQuizzID     = $('#publishedQuizzID').val();
    var questionID  = $('#questionID').val();
    var stageType   = $('#stageType').val();
    var q           = $('#q').val();
    var clr         = $('#clr').val();
    var preview     = 1;
    var answers     = [];
    var indAnswers  = [];
    var options     = [];
    var checkboxes  = [];
    var smiley      = [];
    var type        = $('#type').val();
    var exeptionText    = $('#exeptionText').val();
    var exeptionPoints  = $('#exeptionPoints option:selected').val();
    if(type == 4) {
        var text = '';
        if(choosenAnswer) {
            if(choosenAnswer == 1) {
                text = 'Sehr schlecht';
            }
            if(choosenAnswer == 2) {
                text = 'Schlecht';
            }
            if(choosenAnswer == 3) {
                text = 'Nicht schlecht';
            }
            if(choosenAnswer == 4) {
                text = 'Gut';
            }
            if(choosenAnswer == 5) {
                text = 'Sehr gut';
            }
            smiley = choosenAnswer + '_' + text;
        } else {
            $('.smileyActive').each(function() {
                var id  = $(this).attr('id');
                id      = id.replace('smiley','');
                if(id == 1) {
                    text    = 'Sehr schlecht';
                }
                if(id == 2) {
                    text    = 'Schlecht';
                }
                if(id == 3) {
                    text    = 'Nicht schlecht';
                }
                if(id == 4) {
                    text    = 'Gut';
                }
                if(id == 5) {
                    text    = 'Sehr gut';
                }
                smiley = id + '_' + text;
            });
        }      
    }
    if(type == 5) {
        $('.choosen').each(function() {
            var id  = $(this).attr('id');
            answers.push('0_' + id);
        });
    }
    if(type == 10) {
        if($('#textFeld').val()) {
            indAnswers.push('0_' + $('#textFeld').val());
        }
    }

    if(type == 11) {
        indAnswers.push('0_' + $('#textArea').val());
    }
    if(type == 12) {
        $.each($('#auswahlliste option:selected'), function() {
            options.push($(this).val() + '_' + $(this).text());
        });
    }
    if(type == 13) {
        $('.aCheckbox').each(function() {
            if($(this).prop('checked') == true) {
                checkboxes.push($(this).attr('data-answerID') + '_' + $(this).val());
            }
        });
    }
    var areThereChoosen = $('.choosen').length;
    if(type == 3 || type == 6) {
        if(choosenAnswer) {
            var answer  = getAnswer(choosenAnswer);
            $('.choosen').each(function() {
                 $(this).css('background-color', 'lightsteelblue').removeClass('choosen');
            });
            if($('#answer' + choosenAnswer).hasClass('choosen')) {
                $('#answer' + choosenAnswer).css('background-color', 'lightsteelblue').removeClass('choosen');
            } else {
                $('#answer' + choosenAnswer).css('background-color', 'forestgreen').addClass('choosen');
            }
            $('.choosen').each(function() {
                answers.push(choosenAnswer + '_' + answer);
            });
        } else {
            if(areThereChoosen > 0) {
                $('.choosen').each(function() {
                    var id      = $(this).attr('id');
                    id          = id.replace('answer', '');
                    var answer  = getAnswer(id);
                    answers.push(id + '_' + answer);
                });
            }
        }
    }
    areThereChoosen = $('.choosen').length;
    if(type != 1 && type != 10 && type != 11 && q != -1 && $('#mustBeAnswered').val() == 1) {
        if(areThereChoosen == 0) {
            alert('Die Frage muss beantwortet werden, bevor Sie die nächste Frage laden können.');
            return false;
        }
    }
    if(type == 10 && q != -1 && $('#mustBeAnswered').val() == 1) {
        if(indAnswers.length == 0) {
            alert('Die Frage muss beantwortet werden, bevor Sie die nächste Frage laden können.');
            return false;
        }
    }
    if(type == 11 && q != -1 && $('#mustBeAnswered').val() == 1) {
        if(indAnswers.length == 0) {
            alert('Die Frage muss beantwortet werden, bevor Sie die nächste Frage laden können.');
            return false;
        }
    }
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_jclassroom&task=expath.nextQuestion",
        data: {
            stageType:stageType,
            type:type,
            quizzID:quizzID, 
            publishedQuizzID:publishedQuizzID,
            questionID:questionID, 
            answers:answers,
            q:q,
            clr:clr,
            preview:preview,
            indAnswers:indAnswers,
            options:options,
            checkboxes:checkboxes,
            smiley:smiley,
            exeptionText:exeptionText,
            exeptionPoints:exeptionPoints
        },
        //dataType: 'json',
        success: function( data ) {
            var path = data;
            path = path.split('_');
            if(path[0] == 'SRA') {
                $('#questionResponse').html(path[2]);
                $('#theResultID').val(path[1]);
                $('#moveNextQuestion').fadeIn(200);
                // remove the links of the questions
                $('.answer').each(function() {
                    $(this).addClass('closed');
                })
            } else {
                window.location.href = data;
            }
        }
    });
};
function JNAnswer(choosenAnswer) {
    $('.answer').each(function() {
        $(this).css('background-color', '#e1e1e1').removeClass('choosen');
    });
    if(choosenAnswer == '1') {
        $('#answerJ').css('background-color', 'forestgreen').addClass('choosen');
    } 
    if(choosenAnswer == '0') {
        $('#answerN').css('background-color', 'indianred').addClass('choosen');
    }
}
$(document).ready(function() {
    var q           = $('#q').val();
    var answered    = $('#answered').val();
    var showResultAfter = $('#showResultAfter').val();
    var allowRestart    = $('#allowRestart').val();
    var countSections   = $('#countSections').val();
    if(countSections == 0) {
        $('#nextQuestion').fadeOut(200);
        $('#quitQuizz').fadeIn(200);
    }
    if(q == -1) {
        $('#quitQuizz').fadeIn(200);
    }
    if(q == 0 && allowRestart == 1) {
        $('#restartQuizz').fadeIn(200);
    }
    if(countSections != 0 && q == 0 && showResultAfter == 0) {
        $('#nextQuestion').fadeIn(200);
    }
    if(q > 0) {
        $('#prevQuestion').fadeIn(200);
        if(allowRestart == 1) {
            $('#restartQuizz').fadeIn(200);
        }
        if(showResultAfter == 0) {
            $('#nextQuestion').fadeIn(200);
        }
    }
    if(answered == 1) {
        //$('#moveNextQuestion').fadeIn(200);
        //$('#nextQuestion').fadeOut(0);
    }
});